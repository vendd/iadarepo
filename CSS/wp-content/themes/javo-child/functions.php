<?php
//
// Recommended way to include parent theme styles.
//  (Please see http://codex.wordpress.org/Child_Themes#How_to_Create_a_Child_Theme)
//  
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array('parent-style')
    );
}
//
// Your code goes below
//
// Pull Dynamic Data on Assignment Form

function my_get_current_user($atts){
	extract(shortcode_atts(array(
		'key' => 'user_login',
'key' => 'display_name',
	), $atts));
	
	global $current_user;
	get_currentuserinfo();

	$val = $current_user->$key;
	return $val;
}
add_shortcode('my_get_current_user', 'my_get_current_user');

/**
 * Set up custom arguments using wpmem_login_form_args
 *
 *  * add a wrapper for rows (row_before & row_after)
 *  * remove the remember me checkbox (just login button)
 */
add_filter( 'wpmem_login_form_args', 'my_login_form' );
function my_login_form() {
 
    $args = array(
        'row_before' => '<div class="login-form-row">',
        'row_after'  => '</div>',
        'remember_check' => false
    );
 
    return $args;
}

/**
 * Custom CSS for custom login form elements
 *
 * Note: this just echoes the styles to the head.
 * This is not the "best" way to do it - the best
 * would be to include these styles in a stylesheet,
 * either custom for the plugin, or added to the
 * theme.  But it is the quickest way to just apply
 * some custom CSS for this example.
 */
add_filter('wp_head', 'my_login_form_styles');
function my_login_form_styles() {
    echo '
    <style>
        /* my custom login form styles */
        .login-form-row { float:left; }
        #wpmem_login .button_div {
            width: 18%;
            float: right;
            margin: 12px 0 0 0;
        }
    </style>';
}
