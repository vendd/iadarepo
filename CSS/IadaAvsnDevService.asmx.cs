﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;

namespace CSS
{
    /// <summary>
    /// Summary description for IadaAvsnDevService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class IadaAvsnDevService : System.Web.Services.WebService
    {

        [WebMethod]
        public string TestMethod()
        {
            try
            {
                #region Authorization
                string AuthorizationHeader = HttpContext.Current.Request.Headers["Authorization"];
                bool isUserAuthorized = false;
                if (AuthorizationHeader != null && AuthorizationHeader.StartsWith("Basic"))  //if has header
                {
                    string encodedUserPass = AuthorizationHeader.Substring(6).Trim();  //remove the "Basic"
                    Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                    string userPass = encoding.GetString(Convert.FromBase64String(encodedUserPass));
                    string[] credentials = userPass.Split(':');
                    string UserName = credentials[0];
                    string Password = credentials[1];

                    if (UserName == System.Configuration.ConfigurationManager.AppSettings["AVSNServiceUID"].ToString() && Password == System.Configuration.ConfigurationManager.AppSettings["AVSNServicePWD"].ToString())
                    {
                        isUserAuthorized = true;
                    }
                    else
                    {
                        isUserAuthorized = false;
                    }
                }
                else
                {
                    isUserAuthorized = false;
                }
                #endregion

                #region Return String
                if (isUserAuthorized)
                {
                    return "Response from IADAPro Dev enviroment ";
                }
                else
                {
                    return "Unauthorized";
                }
                #endregion
            }
            catch (Exception ex)
            {
                return "Error "+ex.Message.ToString();
            }

        }
    }
}
