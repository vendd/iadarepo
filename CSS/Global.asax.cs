﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace CSS
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        //Application Cofiguration  parameters
        public static string ReportPath = ConfigurationManager.AppSettings["ReportPath"];
        public static string WebAppURL = ConfigurationManager.AppSettings["WebAppURL"];
        public static string ClaimServiceProviderDocPath = ConfigurationManager.AppSettings["ClaimServiceProviderDocPath"];
        public static string UseAzureCloudStorage = ConfigurationManager.AppSettings["UseAzureCloudStorage"];
        public static string CompanyDocsContainerName = ConfigurationManager.AppSettings["CompanyDocsContainerName"];
        public static string CompanyThumbnailContainerName = ConfigurationManager.AppSettings["CompanyThumbnailContainerName"];
        public static string AutoAssignmentDocsPath = ConfigurationManager.AppSettings["AutoAssignmentDocsPath"];
        public static string AutoAssignDocsContainerName = ConfigurationManager.AppSettings["AutoAssignDocsContainerName"];

        public static string BulkInvoiceSummaryPDF = ConfigurationManager.AppSettings["BulkInvoiceSummaryPDF"];

        public static string AdminMail = ConfigurationManager.AppSettings["SupportEmailAddress"];
        public static string NotFoundFileName = ConfigurationManager.AppSettings["NotFoundFileName"];

        public static string TestEmailAddress = ConfigurationManager.AppSettings["TestEmailAddress"];
        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            //if (HttpContext.Current.Request.IsLocal.Equals(false) && (HttpContext.Current.Request.IsSecureConnection.Equals(false)
            //    || (!Request.Url.Host.StartsWith("www") && !Request.Url.IsLoopback)))
            //{
            //    if (!Request.Url.Host.StartsWith("www"))
            //    {
            //        Response.Redirect("https://www." + Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.RawUrl);
            //    }
            //    else
            //    {
            //        Response.Redirect("https://" + Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.RawUrl);
            //    }
            //}

            //if ((HttpContext.Current.Request.IsSecureConnection.Equals(false)))
            //{
            //    if (!Request.Url.Host.StartsWith("www"))
            //    {
            //        Response.Redirect("https://www." + Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.RawUrl);
            //    }
            //    else
            //    {
            //        Response.Redirect("https://" + Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.RawUrl);
            //    }
            //}
            //string DevelopmentMode = System.Configuration.ConfigurationManager.AppSettings["DevelopmentMode"].ToString();

            //if (DevelopmentMode == "Yes")
            //{
            //    if (HttpContext.Current.Request.IsSecureConnection.Equals(true))
            //    {
            //        Response.Redirect("http://" + Request.ServerVariables["HTTP_HOST"]
            //    + HttpContext.Current.Request.RawUrl);
            //    }
            //}
            //else
            //{
            //    if (HttpContext.Current.Request.IsSecureConnection.Equals(false))
            //    {
            //        Response.Redirect("https://" + Request.ServerVariables["HTTP_HOST"]
            //    + HttpContext.Current.Request.RawUrl);
            //    }
            //} 

        }
       
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new System.Web.Mvc.AuthorizeAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

         }

        protected void Application_Start()
        {
           // AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
        }
        void Session_Start(object sender, EventArgs e)
        {
            //If the session object expires, redirect the user to the homepage
            //if (HttpContext.Current.Session["LoggedInUser"] == null && HttpContext.Current.User.Identity.IsAuthenticated == true)  
            //{
            //    FormsAuthentication.SignOut();
            //    Session.Abandon();
                
            //    HttpContext.Current.Response.Redirect("~/Account/Login");
            
            //}

        }

        void Session_End(object sender, EventArgs e)
        {
            Session.Abandon(); 

        }
        
    }
}