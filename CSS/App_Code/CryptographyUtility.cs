﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.IO;
namespace CSS
{
    public class CryptographyUtility
    {
        public static string Decrypt(string cipherText)
        {
            RijndaelManaged RijndaelCipher = new RijndaelManaged();

            string password = System.Configuration.ConfigurationManager.AppSettings["WebServiceRijndaelSecretKey"].ToString();
            string decryptedData;

            try
            {
                byte[] encryptedData = Convert.FromBase64String(cipherText);

                byte[] salt = Encoding.ASCII.GetBytes(password.Length.ToString());
                //Making of the key for decryption 
                PasswordDeriveBytes secretKey = new PasswordDeriveBytes(password, salt);
                //Creates a symmetric Rijndael decryptor object. 
                ICryptoTransform decryptor = RijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16));

                MemoryStream memoryStream = new MemoryStream(encryptedData);
                //Defines the cryptographics stream for decryption.THe stream contains decrpted data 
                CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);

                //Converting to string 
                byte[] byteArr = new byte[encryptedData.Length - 1];
                int decryptedCount = cryptoStream.Read(byteArr, 0, encryptedData.Length - 1);
                decryptedData = Encoding.Unicode.GetString(byteArr, 0, decryptedCount);

                memoryStream.Close();
                cryptoStream.Close();
            }
            catch
            {
                decryptedData = "";
            }
            return decryptedData;
        }

        public static string Encrypt(string plainText)
        {
            RijndaelManaged RijndaelCipher = new RijndaelManaged();
            string strPassword = System.Configuration.ConfigurationManager.AppSettings["WebServiceRijndaelSecretKey"].ToString();
            byte[] plainTextBytes = System.Text.Encoding.Unicode.GetBytes(plainText);
            byte[] salt = Encoding.ASCII.GetBytes(strPassword.Length.ToString());
            PasswordDeriveBytes secretKey = new PasswordDeriveBytes(strPassword, salt);

            //Creates a symmetric encryptor object. 
            ICryptoTransform encryptor = RijndaelCipher.CreateEncryptor(secretKey.GetBytes(32), secretKey.GetBytes(16));
            MemoryStream memoryStream = new MemoryStream();

            //Defines a stream that links data streams to cryptographic transformations 
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);

            //Writes the final state and clears the buffer 
            cryptoStream.FlushFinalBlock();
            byte[] cipherBytes = memoryStream.ToArray();

            memoryStream.Close();
            cryptoStream.Close();

            string encryptedData = Convert.ToBase64String(cipherBytes);

            return encryptedData;
        }
    }
}