﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using BLL;
using System.Net.Mail;
using System.Net;
using System.IO;

namespace CSS
{
   public class ExceptionLogDAL
    {
        public DataSet GetExceptionLogDetails()
        {
            DataSet ds = new DataSet();
            SqlConnection sqlCon;
            try
            {
                string connstring = ConfigurationManager.ConnectionStrings["ChoiceSolutionsEntities"].ToString();
                sqlCon = new SqlConnection(connstring);
                SqlCommand scmd = new SqlCommand();
                scmd.Connection = sqlCon;
                scmd.CommandType = CommandType.StoredProcedure;
                scmd.CommandText = "GetExceptionLogDetails";
              
                SqlDataAdapter sqlda = new SqlDataAdapter(scmd);
                sqlCon.Open();
                sqlda.Fill(ds);
                sqlCon.Close();
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                //if (sqlCon.State == ConnectionState.Open)
                //{
                //    sqlCon.Close();
                //}
            }
            return ds;
        }
        public DataSet GetQBFailedPayments()
        {
            DataSet ds = new DataSet();
            SqlConnection sqlCon = new SqlConnection();
            try
            {
                string connstring = ConfigurationManager.ConnectionStrings["ChoiceSolutionsEntities"].ToString();
                sqlCon.ConnectionString = connstring;
                SqlCommand scmd = new SqlCommand();
                scmd.Connection = sqlCon;
                scmd.CommandType = CommandType.StoredProcedure;
                scmd.CommandText = "GetQBBridgeFailedPayments";

                SqlDataAdapter sqlda = new SqlDataAdapter(scmd);
                sqlCon.Open();
                sqlda.Fill(ds);
                sqlCon.Close();
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                if (sqlCon.State == ConnectionState.Open)
                {
                    sqlCon.Close();
                }
            }
            return ds;
        }

        public int UpdateExceptionLog(Int32 elid,Int16 flg)
        {
            SqlConnection sqlCon = new SqlConnection();
            int Qbflagvalue=4;
            try
            {
               
                string connstring = ConfigurationManager.ConnectionStrings["ChoiceSolutionsEntities"].ToString();
                sqlCon.ConnectionString = connstring;
                SqlCommand scmd = new SqlCommand();
                scmd.Connection = sqlCon;
                scmd.CommandType = CommandType.StoredProcedure;
                scmd.CommandText = "usp_ExceptionLogUpdate";
                //scmd.Parameters.Add("@Elid", DbType.Int32);
                //scmd.Parameters["@Elid"].Value = elid;
                //scmd.Parameters.Add("@flg", DbType.Int16);                         
                //scmd.Parameters["@flg"].Value = flg;

                scmd.Parameters.AddWithValue("@Elid", elid);
                scmd.Parameters.AddWithValue("@flg", flg);
                scmd.Parameters["@flg"].Direction = ParameterDirection.InputOutput;
                sqlCon.Open();
                scmd.ExecuteNonQuery();
                Qbflagvalue = Convert.ToInt16(scmd.Parameters["@flg"].Value);
                sqlCon.Close();
            }
            catch (Exception ex)
            {
                
            }
            finally
            {
                if (sqlCon.State == ConnectionState.Open)
                {
                    sqlCon.Close();
                }
            }
            return Qbflagvalue;
        }

        public List<usp_PayrollSummaryDetailReport_Result> GetProcessedPayrollDetailReportQB(Int64 SPId, DateTime PaymentDate)
       {
            DataSet ds = new DataSet();
            SqlConnection sqlCon = new SqlConnection();
            try
            {
                string connstring = ConfigurationManager.ConnectionStrings["ChoiceSolutionsEntities"].ToString();
                sqlCon.ConnectionString = connstring;
                SqlCommand scmd = new SqlCommand();
                scmd.Connection = sqlCon;
                scmd.CommandType = CommandType.StoredProcedure;
                scmd.CommandText = "usp_ProcessedPayrollDetailReportQB";
                //scmd.Parameters.Add("@SPId", DbType.Int64);
                //scmd.Parameters["@SPId"].Value = SPId;
                //scmd.Parameters.Add("@PaymentDate", DbType.DateTime);
                //scmd.Parameters["@PaymentDate"].Value = PaymentDate;

                scmd.Parameters.AddWithValue("@SPId", SPId);
                scmd.Parameters.AddWithValue("@PaymentDate", PaymentDate);
                SqlDataAdapter sqlda = new SqlDataAdapter(scmd);
                sqlCon.Open();
                sqlda.Fill(ds);
                sqlCon.Close();
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                if (sqlCon.State == ConnectionState.Open)
                {
                    sqlCon.Close();
                }
            }
            return ConvertToCS(ds);
       }

        public List<usp_PayrollSummaryDetailReport_Result> ConvertToCS(DataSet ds)
        {
            List<usp_PayrollSummaryDetailReport_Result> list = new List<usp_PayrollSummaryDetailReport_Result>();

            usp_PayrollSummaryDetailReport_Result detail;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                detail = new usp_PayrollSummaryDetailReport_Result();
                detail.PAId = Convert.ToInt64(ds.Tables[0].Rows[i]["PAId"].ToString());
                detail.ClaimId = Convert.ToInt64(ds.Tables[0].Rows[i]["ClaimId"].ToString());

                detail.AssignmentId = Convert.ToInt64(ds.Tables[0].Rows[i]["AssignmentId"].ToString());
                detail.ClaimNumber = ds.Tables[0].Rows[i]["ClaimNumber"].ToString();
                detail.HeadCompanyId = Convert.ToInt16(ds.Tables[0].Rows[i]["HeadCompanyId"].ToString());
                detail.InvoiceNo = ds.Tables[0].Rows[i]["InvoiceNo"].ToString();
                detail.TotalSPPay = Convert.ToDecimal(ds.Tables[0].Rows[i]["TotalSPPay"].ToString());
                detail.TotalSPPayPercent = Convert.ToDouble(ds.Tables[0].Rows[i]["TotalSPPayPercent"].ToString());
                detail.AmountPaidToSP = Convert.ToDecimal(ds.Tables[0].Rows[i]["AmountPaidToSP"].ToString());
                detail.AmountHoldBack = Convert.ToDecimal(ds.Tables[0].Rows[i]["AmountHoldBack"].ToString());

                detail.PaymentDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["PaymentDate"].ToString());
               
                    DateTime date1;
                    bool val = DateTime.TryParse(ds.Tables[0].Rows[i]["HoldBackPaymentDate"].ToString(),out date1);
                    if (val)
                    {
                        detail.HoldBackPaymentDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["HoldBackPaymentDate"].ToString());
                    }
                    else
                    {
                        detail.HoldBackPaymentDate = null;
                    }
               
                detail.IsPaymentReceived = Convert.ToInt16(ds.Tables[0].Rows[i]["IsPaymentReceived"].ToString());
                detail.PaymentType = Convert.ToInt16(ds.Tables[0].Rows[i]["PaymentType"].ToString());

                detail.PaymentTypeDesc = ds.Tables[0].Rows[i]["PaymentTypeDesc"].ToString();
                detail.PaymentAmount = Convert.ToDecimal(ds.Tables[0].Rows[i]["PaymentAmount"].ToString());
                list.Add(detail);
            }
            return list;

        }

        //public void CreateLogFile(string lslogFileName)
        //{
        //    string FileName = string.Empty;
        //    string PassedFilename = lslogFileName;
        //    ExceptionLogDAL dal = new ExceptionLogDAL();
        //    string activityLogsPath = System.Configuration.ConfigurationManager.AppSettings["ActivityLogsPath"].ToString();
        //    System.IO.FileStream loFileStream = null;

        //    if (!string.IsNullOrEmpty(lslogFileName))
        //    {
        //        if (System.IO.Directory.Exists(activityLogsPath) == false)
        //        {
        //            System.IO.Directory.CreateDirectory(activityLogsPath);
        //        }
        //        lslogFileName = activityLogsPath + "" + lslogFileName;

        //        var directory = new DirectoryInfo(activityLogsPath);
        //        DateTime creation = DateTime.Now;

        //        creation = directory.GetFiles()
        //    .OrderByDescending(f => f.LastWriteTime).Select(x => x.CreationTime)
        //    .FirstOrDefault<DateTime>();



        //        if ((DateTime.Now - creation).TotalDays > 7)
        //        {
        //            if (!(System.IO.File.Exists(lslogFileName) == true))
        //            {
        //                loFileStream = System.IO.File.Create(lslogFileName);
        //                FileName = PassedFilename;
        //            }
        //        }
        //        else
        //        {
        //            var oldfile = directory.GetFiles()
        //   .OrderByDescending(f => f.LastWriteTime)
        //   .FirstOrDefault();
        //            FileName = oldfile.ToString();
        //        }
        //        if ((loFileStream != null))
        //        {
        //            dal.WriteLog("Log File Created : ", lslogFileName);
        //            loFileStream.Close();
        //            loFileStream.Dispose();
        //        }
        //    }
        //    return FileName;
        //}
        public void CreateLogFile(string lslogFileName)
        {

            string activityLogsPath = System.Configuration.ConfigurationManager.AppSettings["ActivityLogsPath"].ToString();
            System.IO.FileStream loFileStream = null;

            if (!string.IsNullOrEmpty(lslogFileName))
            {
                if (System.IO.Directory.Exists(activityLogsPath) == false)
                {
                    System.IO.Directory.CreateDirectory(activityLogsPath);
                }
                lslogFileName = activityLogsPath + "" + lslogFileName;
                if (!(System.IO.File.Exists(lslogFileName) == true))
                {
                    loFileStream = System.IO.File.Create(lslogFileName);
                }
                if ((loFileStream != null))
                {
                    loFileStream.Close();
                    loFileStream.Dispose();
                }
            }

        }


        public void WriteExceptionLog(string userMessage, Exception ex, string lsFileName)
        {
            string logMessage = "User Message: " + userMessage + System.Environment.NewLine;
            logMessage += "System Exception Message: " + ex.Message + System.Environment.NewLine;
            if (ex.InnerException != null)
            {
                logMessage += "Inner Exception Message: " + ex.InnerException.Message + System.Environment.NewLine;
            }
            WriteLog(logMessage, lsFileName);
        }
        public void WriteLog(string messageToWrite, string lsFileName)
        {
            string activityLogsPath = System.Configuration.ConfigurationManager.AppSettings["ActivityLogsPath"].ToString();
            string lsLogFullFileName = "";
            System.IO.TextWriter loTextWriter = null;
            try
            {
                lsLogFullFileName = activityLogsPath + "" + lsFileName;
                if (System.IO.File.Exists(lsLogFullFileName) == true)
                {
                    loTextWriter = System.IO.File.AppendText(lsLogFullFileName);

                    loTextWriter.WriteLine();
                    loTextWriter.WriteLine("--------------------------------------------------------------");
                    loTextWriter.WriteLine(DateTime.Now.ToString());
                    loTextWriter.WriteLine();
                    loTextWriter.WriteLine(messageToWrite);
                    loTextWriter.WriteLine("--------------------------------------------------------------");
                    loTextWriter.Flush();
                    loTextWriter.Close();
                }
            }
            catch (Exception ex)
            {
            }
        }

       


    }
}
