﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSS.WeGoLookDocUpload
{
    [Serializable]
    public class PropertyLookPutRequest
    {
        public string reference_number { get; set; }
        public string property_information_address { get; set; }
        public string property_information_city { get; set; }
        public string property_information_state { get; set; }
        public string property_information_zip_code { get; set; }
        public Property_Information_Coordinates property_information_coordinates { get; set; }
        public string property_information_map_or_identifying_site_image_information { get; set; }
        public string primary_on_site_contact_name { get; set; }
        public string primary_on_site_contact_email { get; set; }
        public string primary_on_site_contact_phone { get; set; }
        public string primary_on_site_contact_alternate_phone { get; set; }
        public string secondary_on_site_contact_name { get; set; }
        public string secondary_on_site_contact_email { get; set; }
        public string secondary_on_site_contact_phone { get; set; }
        public string description_of_damage_to_be_verified_including_any_possible_cause_of_loss { get; set; }
        public string measurements_to_be_taken_choice { get; set; }
        public int delivery_options { get; set; }

        public class Property_Information_Coordinates
        {
            public string type { get; set; }
            public int[] coordinates { get; set; }
        }
    }
}