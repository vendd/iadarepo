﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSS.WeGoLookDocUpload
{
    [Serializable]
    public class WGLUser
    {
        public string email { get; set; }
        public string password { get; set; }
    }
}