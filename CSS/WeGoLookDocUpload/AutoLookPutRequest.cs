﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CSS.WeGoLookDocUpload
{
    [Serializable]
    public class AutoLookPutRequest
    {
        public string reference_number { get; set; }
        public string reference_number_2 { get; set; }
        public string reference_number_3 { get; set; }
        public string on_site_contact_name { get; set; }
        public string on_site_contact_email { get; set; }
        public string on_site_contact_phone { get; set; }
        public string on_site_contact_alternate_phone { get; set; }
        public string vehicle_location_location_type { get; set; }
        public string vehicle_location_address_or_intersection { get; set; }
        public string vehicle_location_city { get; set; }
        public string vehicle_location_state { get; set; }
        public string vehicle_location_zip_code { get; set; }
        public Vehicle_Location_Coordinates vehicle_location_coordinates { get; set; }
        public int vehicle_information_year { get; set; }
        public string vehicle_information_make { get; set; }
        public string vehicle_information_model { get; set; }
        public string vehicle_information_color { get; set; }
        public string vehicle_information_vin { get; set; }
        public string vehicle_information_license_plate_number { get; set; }
        public string vehicle_information_vehicle_owner { get; set; }
        public string vehicle_information_is_an_estimate_needed { get; set; }
        public string vehicle_information_was_the_vehicle_subject_to_possible_flood_damage { get; set; }
        public string vehicle_information_does_this_vehicle_inspection_require_tire_tread_depth_measurement { get; set; }
        public string vehicle_information_do_photos_of_documents_need_to_be_taken { get; set; }
        public string vehicle_information_what_documents_need_to_be_captured { get; set; }
        public string document_upload_completed_estimate { get; set; }
        public string vehicle_information_order_comments { get; set; }
        public int delivery_options { get; set; }

        public class Vehicle_Location_Coordinates
        {
            public string type { get; set; }
            //public int[] coordinates { get; set; }
            public string coordinates { get; set; }
        }
    }
}