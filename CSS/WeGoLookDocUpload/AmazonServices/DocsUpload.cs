﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
namespace CSS.WeGoLookDocUpload.AmazonServices
{
    public static class DocsUpload
    {
        static IAmazonS3 s3Client;
        
        static string response = "";
        static HttpWebResponse _httpWebResponse;

        public static string GetAbsoluteFileURL(string containerName, string storedFileName)
        {
            if (string.IsNullOrEmpty(containerName))
            {
                throw new Exception("Container Name Null or Empty");
            }
            if (String.IsNullOrEmpty(storedFileName))
            {
                throw new Exception("Stored File Name Null or Empty");
            }
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["StorageConnectionString"].ConnectionString);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);

            return container.Uri.ToString() + "/" + storedFileName;
        }

        public static string UploadDocs(string url, string filePath)
        {
            try
            {
                _httpWebResponse = UploadObject(url, filePath);
                response = (_httpWebResponse.ResponseUri.AbsoluteUri).Replace(_httpWebResponse.ResponseUri.Query, "");
                //using (s3Client = new AmazonS3Client(Amazon.RegionEndpoint.APNortheast1)) { UploadObject(url); }
                //using (s3Client = new AmazonS3Client(Amazon.RegionEndpoint.APNortheast2)) { UploadObject(url); }
                //using (s3Client = new AmazonS3Client(Amazon.RegionEndpoint.APSoutheast1)) { UploadObject(url); }
                //using (s3Client = new AmazonS3Client(Amazon.RegionEndpoint.APSoutheast2)) { UploadObject(url); }
                //using (s3Client = new AmazonS3Client(Amazon.RegionEndpoint.CNNorth1)) { UploadObject(url); }
                //using (s3Client = new AmazonS3Client(Amazon.RegionEndpoint.EUCentral1)) { UploadObject(url); }
                //using (s3Client = new AmazonS3Client(Amazon.RegionEndpoint.EUWest1)) { UploadObject(url); }
                //using (s3Client = new AmazonS3Client(Amazon.RegionEndpoint.SAEast1)) { UploadObject(url); }
                //using (s3Client = new AmazonS3Client(Amazon.RegionEndpoint.USEast1)) { UploadObject(url); }
                //using (s3Client = new AmazonS3Client(Amazon.RegionEndpoint.USGovCloudWest1)) { UploadObject(url); }
                //using (s3Client = new AmazonS3Client(Amazon.RegionEndpoint.USWest1)) { UploadObject(url); }
                //using (s3Client = new AmazonS3Client(Amazon.RegionEndpoint.USWest2)) { UploadObject(url); }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    response = "Error: Check the provided AWS Credentials.";
                    response += "To sign up for service, go to http://aws.amazon.com/s3";
                }
                else
                {
                    response = string.Format("Error occurred. Message:'{0}' when listing objects", amazonS3Exception.Message);
                }
            }
            catch (Exception e)
            {
                response = "Error: " + e.Message;
            }

            return response;
        }


        static HttpWebResponse UploadObject(string url, string filePath)
        {
            //HttpWebRequest httpRequest = WebRequest.Create(url) as HttpWebRequest;
            //httpRequest.Method = "PUT";
            //using (Stream dataStream = httpRequest.GetRequestStream())
            //{
            //    byte[] buffer = new byte[8000];

            //    using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            //    {
            //        int bytesRead = 0;
            //        while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) > 0)
            //        {
            //            dataStream.Write(buffer, 0, bytesRead);
            //        }
            //    }
            //}
            //HttpWebResponse response = httpRequest.GetResponse() as HttpWebResponse;
            //return response;

            HttpWebResponse response = null;
            
            try
            {
                byte[] fileData;
                HttpWebRequest httpRequest = WebRequest.Create(url) as HttpWebRequest;

                using (WebClient client = new WebClient())
                {
                    fileData = client.DownloadData(filePath);

                    if (fileData.Length > 0)
                    {
                        httpRequest.Method = "PUT";
                        using (Stream dataStream = httpRequest.GetRequestStream())
                        {
                            int bytesRead = 0;
                            MemoryStream fileStream = new MemoryStream(fileData);
                            byte[] buffer = new byte[fileData.Length];                        
                            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                dataStream.Write(buffer, 0, bytesRead);
                            }
                        }                        
                        response = httpRequest.GetResponse() as HttpWebResponse;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return response;
        }
    }
}