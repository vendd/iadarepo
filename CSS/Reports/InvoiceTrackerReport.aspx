﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvoiceTrackerReport.aspx.cs" Inherits="CSS.Reports.InvoiceTrackerReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <div class='alert alert-info display-hide' runat="server" id="divErrorMSG">
                <button class='close' data-close='alert'></button>
                <i class='glyphicon glyphicon-info-sign'></i><strong>Error</strong>
                <span runat="server" id='alerterrorMessage'>&nbsp&nbsp&nbsp
             
                </span>
            </div>

            <rsweb:ReportViewer ID="RptViewer" runat="server" AsyncRendering="false" Font-Names="Verdana" Font-Size="8pt" Style="margin-left: 0px; margin-top: 0px; width:98%; " WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" HyperlinkTarget="_blank" Height="100%" Width="99%" SizeToReportContent="true"></rsweb:ReportViewer>
        </div>

    </form>
</body>
</html>

