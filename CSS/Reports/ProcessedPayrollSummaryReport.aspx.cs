﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using Microsoft.Reporting;
using BLL;


namespace CSS.Reports
{
    public partial class ProcessedPayrollSummaryReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("PayrollDate", Request.QueryString["PayrollDate"].ToString()));

            //TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("SPId", Request.QueryString["SPId"] != null ? Request.QueryString["SPId"].ToString() : "0"));

            //string spid = Request.QueryString["SPId"];
            //TrptViewer.RefreshReport();
            string reportPath = MvcApplication.ReportPath; //System.Configuration.ConfigurationManager.AppSettings["ReportFilePath"];
            divErrorMSG.Visible = false;
            RptViewer.Visible = true;
            if (!IsPostBack)
            {
            if (Session["LoggedInUser"] != null)
            {
                //Cypher cypher = new Cypher();
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                //Int32 JobTypeId = Convert.ToInt32(Request.QueryString["JobTypeId"]);
                string JobTypeId = Request.QueryString["JobTypeId"];
                if (loggedInUser.UserTypeId == 1 || loggedInUser.UserTypeId == 2 || loggedInUser.UserTypeId == 12)
                {
                    //if (Request.QueryString["SPId"] != null)
                    //{
                    //    string SPID = Request.QueryString["SPId"].ToString();
                    //    //string SPID = Cypher.DecryptString(Request.QueryString["SPId"].ToString());

                    //    if (Convert.ToInt64(SPID) == loggedInUser.UserId)
                    //    {
                    //        TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("PayrollDate", Request.QueryString["PayrollDate"].ToString()));
                    //        TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("SPId", SPID != null ? SPID : "0"));
                    //        TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("JobTypeId", JobTypeId != null ? JobTypeId : "0"));
                    //        TrptViewer.RefreshReport();
                    //    }

                    //}
                    //else
                    //{
                    //    TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("PayrollDate", Request.QueryString["PayrollDate"].ToString()));
                    //    TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("SPId", Request.QueryString["SPId"] != null ? Request.QueryString["SPId"].ToString() : "0"));
                    //    TrptViewer.ReportSource.Parameters.Add(new Telerik.Reporting.Parameter("JobTypeId", JobTypeId != null ? JobTypeId : "0"));
                    //    TrptViewer.RefreshReport();
                    //}
               
                   
                        try
                        {
                            RptViewer.Reset();
                            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
                            RptViewer.LocalReport.ReportPath = Server.MapPath(reportPath + "ProcessedPayrollSummaryReport.rdlc");

                            RptViewer.LocalReport.DataSources.Clear();
                            ReportDataSource ds = new ReportDataSource("DataSet2", GetReportData());
                            RptViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                            RptViewer.LocalReport.DataSources.Add(ds);
                            RptViewer.LocalReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter("PayrollDate", Request.QueryString["PayrollDate"]));
                            RptViewer.LocalReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter("ServiceTypeId", "0"));
                            
                            RptViewer.LocalReport.Refresh();
                            // RptViewer.DataBind();
                        }
                        catch (Exception ex)
                        {
                            RptViewer.Visible = true;
                            divErrorMSG.Visible = true;
                            alerterrorMessage.InnerText = "Unexpected error while Generating report";
                        }

                    }
                    else
                    {
                    }

                }
            }
        }
        public IEnumerable<usp_ProcessedPayrollSummaryReport_Result> GetReportData()
        {
            Int64? SPid = IsValidInt(Request.QueryString["SPId"]);
            DateTime? PayrollDate = IsValidDate(Request.QueryString["PayrollDate"]);
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            IEnumerable<usp_ProcessedPayrollSummaryReport_Result> result = css.usp_ProcessedPayrollSummaryReport(PayrollDate, SPid).ToList();
            return result;
        }
        private DateTime? IsValidDate(object DT)
        {
            DateTime validdate;
            if (DT == null) return null;

            if (DateTime.TryParse(DT.ToString(), out validdate))
            {
                return validdate;
            }
            return null;
        }

        private Int32? IsValidInt(object Value)
        {
            Int32 validVal;
            if (Value == null) return null;

            if (Int32.TryParse(Value.ToString(), out validVal))
            {
                return validVal;
            }
            return null;
        }
        protected void btnShowReport_Click(object sender, EventArgs e)
        {

        }


    }




}