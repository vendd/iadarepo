﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using Microsoft.Reporting;
using BLL;


namespace CSS.Reports
{
    public partial class ProcessedBulkInvoiceSummaryReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
            string reportPath = MvcApplication.ReportPath; //System.Configuration.ConfigurationManager.AppSettings["ReportFilePath"];
            divErrorMSG.Visible = false;
            RptViewer.Visible = true;
            if (!IsPostBack)
            {
            if (Session["LoggedInUser"] != null)
            {
                //Cypher cypher = new Cypher();
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
           
                if (loggedInUser.UserTypeId == 1 || loggedInUser.UserTypeId == 2 || loggedInUser.UserTypeId == 12)
                {
                  
               
                   
                        try
                        {
                            RptViewer.Reset();
                            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
                            RptViewer.LocalReport.ReportPath = Server.MapPath(reportPath + "ProcessedBulkInvoiceSummaryReport.rdlc");

                            RptViewer.LocalReport.DataSources.Clear();
                            ReportDataSource ds = new ReportDataSource("DataSet1", GetReportData());
                            RptViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                            RptViewer.LocalReport.DataSources.Add(ds);
                            RptViewer.LocalReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter("BulkInvoiceDate", Request.QueryString["BulkInvoiceDate"]));
                            RptViewer.LocalReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter("ServiceTypeId", "0"));
                            
                            RptViewer.LocalReport.Refresh();
                            // RptViewer.DataBind();
                        }
                        catch (Exception ex)
                        {
                            RptViewer.Visible = true;
                            divErrorMSG.Visible = true;
                            alerterrorMessage.InnerText = "Unexpected error while Generating report";
                        }

                    }
                    else
                    {
                    }

                }
            }
        }
        public IEnumerable<usp_ProcessedBulkInvoiceSummaryReport_Result> GetReportData()
        {
            Int64? SPid = IsValidInt(Request.QueryString["SPId"]);
            DateTime? BulkInvoiceDate = IsValidDate(Request.QueryString["BulkInvoiceDate"]);
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            IEnumerable<usp_ProcessedBulkInvoiceSummaryReport_Result> result = css.usp_ProcessedBulkInvoiceSummaryReport(BulkInvoiceDate, SPid).ToList();
            return result;
        }
        private DateTime? IsValidDate(object DT)
        {
            DateTime validdate;
            if (DT == null) return null;

            if (DateTime.TryParse(DT.ToString(), out validdate))
            {
                return validdate;
            }
            return null;
        }

        private Int32? IsValidInt(object Value)
        {
            Int32 validVal;
            if (Value == null) return null;

            if (Int32.TryParse(Value.ToString(), out validVal))
            {
                return validVal;
            }
            return null;
        }
        protected void btnShowReport_Click(object sender, EventArgs e)
        {

        }


    }




}