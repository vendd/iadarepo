﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProcessedPayrollSummaryReport.aspx.cs" Inherits="CSS.Reports.ProcessedPayrollSummaryReport" %>


<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>


<!DOCTYPE html>
<script src="../Scripts/DateTimePicker/jquery-1.8.0.min.js" type="text/javascript"></script>
<script src="../Scripts/DateTimePicker/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
<link href="../Content/JQueryUI/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    <script type="text/javascript">
        $(function () {

            $(".DatePickerTextBox").datepicker({
                showOn: "button",
                buttonImage: "../Content/Images/calendar.gif",
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+20",
                minDate: new Date(1999, 10 - 1, 25),
                maxDate: '+30Y',
                inline: true
            });

            //var waitDiv = document.getElementById("TrptViewer_ReportArea_WaitControl");
            //var tcell = waitDiv.getElementsByTagName("TD")[0];
            //tcell.style.verticalAlign = "Top";
            //tcell.style.textAlign = "Center";
            //tcell.style.padding = "100px";
        });


    </script>
</head>
<body>
    <form id="Form1" runat="server">
         <div class='alert alert-info display-hide' runat="server" id="divErrorMSG">
                <button class='close' data-close='alert'></button>
                <i class='glyphicon glyphicon-info-sign'></i><strong>Error</strong>
                <span runat="server" id='alerterrorMessage'>&nbsp&nbsp&nbsp
             
                </span>
            </div>
        <table style="width: 100%">
           
            <tr>
                <td style="text-align: center">
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
           
                    <rsweb:ReportViewer ID="RptViewer" runat="server" AsyncRendering="false" Font-Names="Verdana" Font-Size="8pt" Style="margin-left: 0px; margin-top: 0px; width:98%; " WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" HyperlinkTarget="_blank" Height="100%" Width="99%" SizeToReportContent="true"></rsweb:ReportViewer>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
