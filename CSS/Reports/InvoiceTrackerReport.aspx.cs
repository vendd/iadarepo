﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting;
using Microsoft.Reporting.WebForms;
using BLL;
namespace CSS.Reports
{
    public partial class InvoiceTrackerReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            string reportPath = MvcApplication.ReportPath; //System.Configuration.ConfigurationManager.AppSettings["ReportFilePath"];
            divErrorMSG.Visible = false;
            RptViewer.Visible = true;
            if (!IsPostBack)
            {
                try
                {
                    RptViewer.Reset();
                    ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
                    RptViewer.LocalReport.ReportPath = Server.MapPath(reportPath + "InvoiceTrackerReport.rdlc");
                   
                    RptViewer.LocalReport.DataSources.Clear();
                    ReportDataSource ds = new ReportDataSource("DataSet1", GetReportData());
                    RptViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                    RptViewer.LocalReport.DataSources.Add(ds);
                    RptViewer.LocalReport.Refresh();
                    // RptViewer.DataBind();
                }
                catch (Exception ex)
                {
                    RptViewer.Visible = false;
                    divErrorMSG.Visible = true;
                    alerterrorMessage.InnerText = "Unexpected error while Generating report";
                }
            }
        }
        public IEnumerable<usp_GetInvoiceTrackerReport_Result> GetReportData()
        {
            DateTime? StartDate = IsValidDate(Request.QueryString["StartDate"]);
            DateTime? EndDate = IsValidDate(Request.QueryString["EndDate"]);
             ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
             IEnumerable<usp_GetInvoiceTrackerReport_Result> result = css.usp_GetInvoiceTrackerReport(IsValidInt(Request.QueryString["InsuranceCompany"]), IsValidInt(Request.QueryString["ClaimRepresentativeID"]), StartDate, EndDate, IsValidInt(Request.QueryString["AssignmentMonth"]), IsValidInt(Request.QueryString["AssignmentYear"])).ToList();
             return result;
        }
        private DateTime? IsValidDate(object DT)
        {
            DateTime validdate;
            if (DT == null) return null;
            
            if (DateTime.TryParse(DT.ToString(),out validdate))
            {
                return validdate;          
            }
            return null;
         }

        private Int32? IsValidInt(object Value)
        {
            Int32 validVal;
            if (Value == null) return null;

            if (Int32.TryParse(Value.ToString(), out validVal))
            {
                return validVal;
            }
            return null;
        }
    }
}