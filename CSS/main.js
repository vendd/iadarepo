﻿/// <reference path="Scripts/ui-bootstrap-tpls-0.11.0.js" />
/// <reference path="Scripts/ui-bootstrap-tpls-0.11.0.js" />
/// <reference path="Scripts/ui-bootstrap-tpls-0.11.0.js" />
require.config({
   
    //baseUrl: "",

    // alias libraries paths
    paths: {
        'app-config': 'scripts/app-config',
        'angular': 'scripts/angular',
        'angular-route': 'scripts/angular-route',
        'angularAMD': 'scripts/angularAMD',
        'ui-bootstrap': 'scripts/ui-bootstrap-tpls-1.3.3',
        'blockUI': 'scripts/angular-block-ui',
        'angular-animate': 'scripts/angular-animate',
        'ngload': 'scripts/ngload',
        'angular-sanitize': 'scripts/angular-sanitize',

        //'ajaxService': 'Services/AjaxService',
        //'EmployeeService': 'Services/EmployeeService',
        //'UpBoardService': 'Services/UpboardService',
        //'CustomerService': 'Services/CustomerService',
        //'AlertService': 'Services/AlertService',
        //'UtilityService': 'Services/UtilityService',
        // 'OpportunityController': 'Views/UpBoard/OpportunityController',
        //'CustomerNewController': 'Views/Customer/CustomerNewController',
        //'OpenOpportunityController': 'Views/UpBoard/OpenOpportunityController'
    },

    // Add angular modules that does not support AMD out of the box, put it in a shim
    shim: {
        'angularAMD': ['angular'],
        'angular-route': ['angular'],
        'blockUI': ['angular'],
        'angular-sanitize': ['angular'],
        'ui-bootstrap': ['angular'],
        'angular-animate': ['angular']
    },

    // kick start application
    deps: ['app-config'],

     
    waitSeconds: 0
});
