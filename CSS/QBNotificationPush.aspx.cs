﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CSS.Controllers
{
    public partial class QBNotificationPush : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnQBVendorPush_Click(object sender, EventArgs e)
        {
           //Utility.QBUpdateVendor(Convert.ToInt64(txtOPTSP.Text));

        }

        protected void btnQBInvoicePush_Click(object sender, EventArgs e)
        {
            foreach (string optInvoiceId in txtOPTInvoice.Text.Split(','))
            {
                if (!String.IsNullOrEmpty(optInvoiceId))
                {
                    Utility.QBUpdateInvoice(Convert.ToInt64(optInvoiceId.Trim()));
                }
            }
        }

        protected void btnQBBillPush_Click(object sender, EventArgs e)
        {
            foreach (string spId in txtBillUserId.Text.Split(','))
            {
                List<BLL.usp_PayrollSummaryDetailReport_Result> payrollDetails = new List<BLL.usp_PayrollSummaryDetailReport_Result>();
                BLL.ChoiceSolutionsEntities css = new BLL.ChoiceSolutionsEntities();



                List<usp_PayrollSummaryDetailReport_Result> processedSPPayrollSummaryReport = GetProcessedPayrollDetailReportQB(Convert.ToInt64(spId), Convert.ToDateTime(txtBillPayrollDateTime.Text));
                //List<usp_ProcessedPayrollSummaryReport_Result> processedSPPayrollSummaryReport = css.usp_ProcessedPayrollSummaryReport(Convert.ToDateTime(txtBillPayrollDateTime.Text)).Where(x => x.SPId == Convert.ToInt64(spId.Trim())).ToList();
                string strAccessToken = "";
                foreach (var pa in processedSPPayrollSummaryReport)
                {
                    usp_PayrollSummaryDetailReport_Result toAdd = new usp_PayrollSummaryDetailReport_Result();
                    toAdd.AmountHoldBack = pa.AmountHoldBack;
                    toAdd.AmountPaidToSP = pa.AmountPaidToSP;
                    toAdd.AssignmentId = pa.AssignmentId;
                    toAdd.ClaimId = pa.ClaimId;
                    toAdd.ClaimNumber = pa.ClaimNumber;
                    //toAdd.HeadCompanyId = css.Claims.Find(pa.ClaimId).HeadCompanyId;
                    toAdd.HeadCompanyId = pa.HeadCompanyId;
                    toAdd.HoldBackPaymentDate = pa.HoldBackPaymentDate;
                    toAdd.InvoiceNo = pa.InvoiceNo;
                    toAdd.IsPaymentReceived = pa.IsPaymentReceived;
                    toAdd.PAId = pa.PAId;
                    toAdd.PaymentAmount = pa.PaymentAmount;
                    toAdd.PaymentDate = pa.PaymentDate;
                    toAdd.PaymentType = pa.PaymentType;
                    toAdd.PaymentTypeDesc = pa.PaymentTypeDesc;
                    toAdd.TotalSPPay = pa.TotalSPPay;
                    toAdd.TotalSPPayPercent = pa.TotalSPPayPercent;

                    payrollDetails.Add(toAdd);

                    QBManageTokens_Result objQBManageToken = css.QBManageTokens(false, null, null).FirstOrDefault();
                    if (objQBManageToken != null)
                    {
                        if (string.IsNullOrEmpty(objQBManageToken.AccessToken))
                        {
                            AccountController obj = new AccountController();
                            strAccessToken = obj.setQuickBookDetail();

                            if (strAccessToken.Contains("Error"))
                            {
                                Utility.LogException("Reports/PayrollSummaryReportProcess", new Exception(), spId + "|" + pa.PaymentAmount + "|" + strAccessToken);
                            }

                            //if (HttpContext.Session["QuickBookDetails"] != null)
                            //{
                            //    QuickBookDetails obj_QuickBookDetail = new QuickBookDetails();
                            //    obj_QuickBookDetail = (QuickBookDetails)System.Web.HttpContext.Current.Session["QuickBookDetails"];
                            //    strAccessToken = obj_QuickBookDetail.accessToken;
                            //    HttpContext.Session["QuickBookDetails"] = obj_QuickBookDetail;
                            //}
                        }
                        else
                        {
                            strAccessToken = objQBManageToken.AccessToken;
                        }
                    }

                    decimal totalPayAmount = processedSPPayrollSummaryReport.Sum(x => x.PaymentAmount);
                    //string qbBillId = Utility.QBUpdateBill(Convert.ToInt64(spId.Trim()), totalPayAmount, Convert.ToDateTime(txtBillPayrollDateTime.Text), Convert.ToDateTime(txtBillPayrollEndDate.Text), payrollDetails);
                    string qbBillId = Utility.QBUpdateBill(Convert.ToInt64(spId.Trim()), totalPayAmount, Convert.ToDateTime(txtBillPayrollDateTime.Text), Convert.ToDateTime(txtBillPayrollEndDate.Text), payrollDetails, strAccessToken);

                    payrollDetails.Remove(toAdd); 

                    //foreach (var detail in payrollDetails)
                    //{
                    //    css.usp_SPPayrollAndAdjUpdateQBBillId(detail.PAId, detail.PaymentType, qbBillId);
                    //}


                }
                
            }

        }

        protected void btnQBPaymentPush_Click(object sender, EventArgs e)
        {
            
             foreach (string optPaymentId in txtPaymentId.Text.Split(','))
            {
                if (!String.IsNullOrEmpty(optPaymentId))
                {
                    Utility.QBUpdatePayment(Convert.ToInt64(optPaymentId.Trim()));
                }
            }
            
        }

        public List<usp_PayrollSummaryDetailReport_Result> GetProcessedPayrollDetailReportQB(Int64 SPId, DateTime PaymentDate)
        {
            DataSet ds = new DataSet();
            SqlConnection sqlCon = new SqlConnection();
            try
            {
                string connstring = ConfigurationManager.ConnectionStrings["ChoiceSolutionsReports"].ToString();
                sqlCon.ConnectionString = connstring;
                SqlCommand scmd = new SqlCommand();
                scmd.Connection = sqlCon;
                scmd.CommandType = CommandType.StoredProcedure;
                scmd.CommandText = "usp_ProcessedPayrollDetailReportQB";
                //scmd.Parameters.Add("@SPId", DbType.Int64);
                //scmd.Parameters["@SPId"].Value = SPId;
                //scmd.Parameters.Add("@PaymentDate", DbType.DateTime);
                //scmd.Parameters["@PaymentDate"].Value = PaymentDate;

                scmd.Parameters.AddWithValue("@SPId", SPId);
                scmd.Parameters.AddWithValue("@PaymentDate", PaymentDate);
                SqlDataAdapter sqlda = new SqlDataAdapter(scmd);
                sqlCon.Open();
                sqlda.Fill(ds);
                sqlCon.Close();
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                if (sqlCon.State == ConnectionState.Open)
                {
                    sqlCon.Close();
                }
            }
            return ConvertToCS(ds);
        }

        public List<usp_PayrollSummaryDetailReport_Result> ConvertToCS(DataSet ds)
        {
            List<usp_PayrollSummaryDetailReport_Result> list = new List<usp_PayrollSummaryDetailReport_Result>();

            usp_PayrollSummaryDetailReport_Result detail;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                detail = new usp_PayrollSummaryDetailReport_Result();
                detail.PAId = Convert.ToInt64(ds.Tables[0].Rows[i]["PAId"].ToString());
                detail.ClaimId = Convert.ToInt64(ds.Tables[0].Rows[i]["ClaimId"].ToString());

                detail.AssignmentId = Convert.ToInt64(ds.Tables[0].Rows[i]["AutoAssignmentId"].ToString());
                detail.ClaimNumber = ds.Tables[0].Rows[i]["ClaimNumber"].ToString();
                detail.HeadCompanyId = Convert.ToInt16(ds.Tables[0].Rows[i]["HeadCompanyId"].ToString());
                detail.InvoiceNo = ds.Tables[0].Rows[i]["AutoInvoiceNo"].ToString();
                detail.TotalSPPay = Convert.ToDecimal(ds.Tables[0].Rows[i]["TotalSPPay"].ToString());
                detail.TotalSPPayPercent = Convert.ToDouble(ds.Tables[0].Rows[i]["TotalSPPayPercent"].ToString());
                detail.AmountPaidToSP = Convert.ToDecimal(ds.Tables[0].Rows[i]["AmountPaidToSP"].ToString());
                detail.AmountHoldBack = Convert.ToDecimal(ds.Tables[0].Rows[i]["AmountHoldBack"].ToString());

                detail.PaymentDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["PaymentDate"].ToString());

                DateTime date1;
                bool val = DateTime.TryParse(ds.Tables[0].Rows[i]["HoldBackPaymentDate"].ToString(), out date1);
                if (val)
                {
                    detail.HoldBackPaymentDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["HoldBackPaymentDate"].ToString());
                }
                else
                {
                    detail.HoldBackPaymentDate = null;
                }

                detail.IsPaymentReceived = Convert.ToInt16(ds.Tables[0].Rows[i]["IsPaymentReceived"].ToString());
                detail.PaymentType = Convert.ToInt16(ds.Tables[0].Rows[i]["PaymentType"].ToString());

                detail.PaymentTypeDesc = ds.Tables[0].Rows[i]["PaymentTypeDesc"].ToString();
                detail.PaymentAmount = Convert.ToDecimal(ds.Tables[0].Rows[i]["PaymentAmount"].ToString());
                list.Add(detail);
            }
            return list;

        }
    }
}