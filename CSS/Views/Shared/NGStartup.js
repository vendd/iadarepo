﻿//
//  angular bootup and routing table
//



(function () {

    var app = angular.module("CSSMVCApp", ['ngRoute', 'ui.router']);

    app.config(['$controllerProvider', '$provide', function ($controllerProvider, $provide) {
        app.register =
          {
              controller: $controllerProvider.register,
              service: $provide.service
          };
    }]);

})();






