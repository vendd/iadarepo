﻿"use strict";

var app = angular.module('CSSMVCApp');

app.service('SPMapService', ['ajaxService', function (ajaxService) {
    this.GetSPMapDetails = function (data, successFunction, errorFunction) {
        ajaxService.AjaxGetWithData(data, "SPMapSearch/GetSPServiceRateDetails", successFunction, errorFunction);
    };

    this.updateSPServiceRates = function (data, successFunction, errorFunction) {
        ajaxService.AjaxPost(data, "SPMapSearch/updateSPServiceRates", successFunction, errorFunction);
    };
   
}]);
app.controller('ManageSPServiceRateController', ['$location', '$scope', '$rootScope', '$filter', '$document', '$compile', 'SPMapService', 'IADArootUrl', function ($location, $scope, $rootScope, $filter, $document, $compile, SPMapService, IADArootUrl) {

    //Parameters
    $scope.initializeController = function () {
      
        var postData = { SPID: loggedInUser };
        SPMapService.GetSPMapDetails(postData, $scope.GetSPMapDetailsCompleted, $scope.GetSPMapDetailsError);

    }
    $scope.GetSPMapDetailsCompleted = function (response, status) {
         $scope.SPServiceRate = response.SPServiceRate;
    
       //  $("#ManageServiceRatePopup").modal("show");
    }
  
    $scope.GetSPMapDetailsError = function (response, status) {
        //  window.location = "/index.html";
    }
    $scope.SPServiceRate = null;

    $scope.updateServiceRate = function () {

        var postData = { SPServiceRate: $scope.SPServiceRate, UserID: loggedInUser };
        SPMapService.updateSPServiceRates(postData, $scope.updateSPServiceRatesCompleted, $scope.updateSPServiceRatesError);
    }
    $scope.updateSPServiceRatesCompleted = function (response, status) {
        $("#ManageServiceRatePopup").modal('hide');
    }
    $scope.updateSPServiceRatesError = function (response, status) {
        //   window.location = "/index.html";
    }
}]);//end controller
