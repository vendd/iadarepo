﻿"use strict";

var app = angular.module('CSSMVCApp', ['ngAnimate']);

//test
app.config(function ($httpProvider, $provide) {
    var baseSiteUrlPath = $("base").first().attr("href");
    $provide.constant('IADArootUrl', baseSiteUrlPath);
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    $httpProvider.interceptors.push(function ($q, $rootScope) {
        return {
            'request': function (config) {
                $rootScope.IsSpinnerVisible = false;
                $rootScope.$broadcast('loading-started');
                return config || $q.when(config);
            },
            'response': function (response) {
                $rootScope.IsSpinnerVisible = false;
                $rootScope.$broadcast('loading-complete');
                return response || $q.when(response);
            },
            'responseError': function (rejection) {
                $rootScope.IsSpinnerVisible = false;
                $rootScope.$broadcast('loading-complete');
                return $q.reject(rejection);
            }
        };
    });
});

app.factory('loadingCounts', function () {
    return {
        enable_count: 0,
        disable_count: 0
    }
});

app.directive("loadingIndicator", function (loadingCounts, $timeout, $rootScope) {
    return {
        restrict: "A",
        link: function (scope, element, attrs) {
            scope.$on("loading-started", function (e) {
                loadingCounts.enable_count++;
                //console.log("displaying indicator " + loadingCounts.enable_count);
                //only show if longer than one sencond
                $timeout(function () {
                    if (loadingCounts.enable_count > loadingCounts.disable_count) {
                        $rootScope.IsSpinnerVisible = true;
                        element.css({ "display": "" });
                       
                    }
                }, 100);
            });
            scope.$on("loading-complete", function (e) {
                loadingCounts.disable_count++;
               // console.log("hiding indicator " + loadingCounts.disable_count);
                if (loadingCounts.enable_count == loadingCounts.disable_count) {
                    $rootScope.IsSpinnerVisible = false;
                    element.css({ "display": "none" });
                }
            });
        }
    };
});
//test
app.service('ajaxService', ['$http', function ($http, BlockUI) {

    // setting timeout of 1 second to simulate a busy server.

    this.AjaxPost = function (data, route, successFunction, errorFunction) {
     
        setTimeout(function () {
            $http.post(route, data).success(function (response, status, headers, config) {
         
                successFunction(response, status);
            }).error(function (response) {
              
                if (response.IsAuthenicated == false) { window.location = "/index.html"; }
                errorFunction(response);
            });
        }, 100);

    }
    this.AjaxGetWithDataResponseType = function (data, route,ResponseType, successFunction, errorFunction) {

        setTimeout(function () {
            $http({ method: 'GET', url: route, params: data, responseType: ResponseType }).success(function (response, status, headers, config) {

                successFunction(response, status, headers);
            }).error(function (response) {

                if (response.IsAuthenicated == false) { window.location = "/index.html"; }
                errorFunction(response);
            });
        }, 100);

    }

    this.AjaxGet = function (route, successFunction, errorFunction) {
     
        setTimeout(function () {
            $http({ method: 'GET', url: route }).success(function (response, status, headers, config) {
             
                successFunction(response, status);
            }).error(function (response) {
              
                if (response.IsAuthenicated == false) { window.location = "/index.html"; }
                errorFunction(response);
            });
        }, 100);

    }

    this.AjaxGetWithData = function (data, route, successFunction, errorFunction) {
       
        setTimeout(function () {
            $http({ method: 'GET', url: route, params: data }).success(function (response, status, headers, config) {
                
                successFunction(response, status);
            }).error(function (response) {
               
                if (response.IsAuthenicated == false) { window.location = "/index.html"; }
                errorFunction(response);
            });
        }, 100);

    }
    //ajax post with file data
    this.AjaxPostwithFile = function (data, route, successFunction, errorFunction) {
     
        setTimeout(function () {
            $http.post(route, data, {
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity

            }).success(function (response, status, headers, config) {
             
                successFunction(response, status);
            }).error(function (response) {
              
                if (response.IsAuthenicated == false) { window.location = "/index.html"; }
                errorFunction(response);
            });
        }, 100);

    }
}]);