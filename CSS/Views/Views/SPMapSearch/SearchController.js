﻿"use strict";

var app = angular.module('CSSMVCApp');


app.directive('cssMap', function (IADArootUrl) {
    // directive link function
    var link = function ($scope, element, attrs) {
        var gmap, infoWindow;
        var markerCluster;

        // map config
        var mapOptions = {
            center: new google.maps.LatLng(37.09024, -95.712891),
            zoom: 3,

            mapTypeId: google.maps.MapTypeId.ROADMAP,

            //scrollwheel: false
        };

        // init the map
        function initMap() {
            if (gmap === void 0) {

                gmap = new GMaps({
                    div: element[0],
                    zoom: 4,
                    maxZoom: 15,
                    center: new google.maps.LatLng(37.09024, -95.712891),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    panControl: false,
                    markerClusterer: function (gmap) {
                        markerCluster = new MarkerClusterer(gmap, [], {
                            title: 'Location Cluster',
                            maxZoom: 15
                        });

                        // onClick OVERRIDE
                        markerCluster.onClick = function (clickedClusterIcon) {
                            //  return multiChoice(clickedClusterIcon.cluster_);
                        }

                        return markerCluster;
                    }
                });
            }
        }
        var marker;
        var LOClatlng, stuDistances, inBetween, labelMarker;
        //   var markerBounds = new google.maps.LatLngBounds();
        function setMarker(position, title, content,iconUrl) {
            
            if (title == "LOC")
            {
                LOClatlng = new google.maps.LatLng(position.lat(), position.lng());
            }
            marker= gmap.addMarker({
                position: position,
                title: title,

                panControl: false,

                infoWindow: {
                    content: content
                },
               
                icon: iconUrl
        
            });
//Commented Line and Distance
            //if (title != "LOC") {
            //    gmap.drawPolyline({
            //        path: [LOClatlng, position],
            //        strokeColor: "#FF0000",
            //        strokeOpacity: 0.5,
            //        strokeWeight: 2,
            //        geodesic: true,
            //    });
            //    // calculate the distance between home and this marker
            //    stuDistances = calculateDistances(LOClatlng, position);

            //    // get the point half-way between this marker and the home marker
            //    inBetween = google.maps.geometry.spherical.interpolate(LOClatlng, position, 0.5);
            //    // Label specific
            //    var span  = document.createElement('span');
            //    span.style.cssText = 'position: relative; left: -50%; top: -8px; ' +
            //                         'white-space: nowrap; border: 1px solid blue; ' +
            //                         'padding: 2px; background-color: white';
            //    span.innerHTML = stuDistances.miles + ' miles';
            //    gmap.drawOverlay({
            //        lat: inBetween.lat(),
            //        lng: inBetween.lng(),
            //        content: span,

            //    });
            //    //// create an invisible marker
            //    //labelMarker = gmap.addMarker({
            //    //    position: position,
            //    //    visible: true
            //    //});

            //    //var label = new Label();
            //    //label.bindTo('position', labelMarker, 'position');
            //    //label.set(stuDistances.miles + ' miles');
            //}
        }

        initMap();
        function calculateDistances(start, end) {
            var stuDistances = {};

            stuDistances.metres = google.maps.geometry.spherical.computeDistanceBetween(start, end);	// distance in metres rounded to 1dp
            stuDistances.km = Math.round(stuDistances.metres / 1000 * 10) / 10;							// distance in km rounded to 1dp
            stuDistances.miles = Math.round(stuDistances.metres / 1000 * 0.6214 * 10) / 10;				// distance in miles rounded to 1dp

            return stuDistances;
        }
        $scope.$watch('marker', function (values) {
            if (values != null && values.length>0) {

                gmap.markerClusterer.removeMarkers(gmap.markers)
                gmap.removeMarkers();
                gmap.setZoom(4);
              //  gmap.removeOverlays();
              //  gmap.removePolylines();
                angular.forEach(values, function (value, key) {
                    // var markPin= "http://localhost:50090/Content/Images/red-dot.png"
                   
                    var markPin = IADArootUrl + "Content/Images/red-dot.png";

                    if (value.title == "LOC") {
                      //  markPin = "http://localhost:50090/Content/Images/green-dot.png";
                        markPin = IADArootUrl + "Content/Images/green-dot.png";
                    }
                    else if (value.title == "YELLOW") {
                        //markPin = "http://localhost:50090/Content/Images/yellow-dot.png";
                        markPin = IADArootUrl + "Content/Images/yellow-dot.png";
                    }
                    setMarker(value.position, value.title, value.content, markPin)

                })
                gmap.markerClusterer.fitMapToMarkers();
                gmap.markerClusterer.setMinimumClusterSize(160934);
            }
            else {
                gmap.markerClusterer.removeMarkers(gmap.markers)
                gmap.removeMarkers();
                gmap.setZoom(4);
            }

        });
    };

    return {
        restrict: 'E',
        scope: {
            marker: '=',
        },
        template: '<div style="height: 100%;background-color: GrayText"></div>',
        replace: true,
        link: link
    };
})

app.filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        if (input !== null && angular.isDefined(input)) {
            return input.slice(start);
        }
    }
});

app.directive('cssFocus', ['$rootScope', function ($rootScope) {
    return {
        require: 'ngModel',
        scope: {
            ngModel: '=',
            details: '=?'
        },
        link: function (scope, element, attrs, model) {
            var options = {
                types: ['geocode'],
                componentRestrictions: {}
            };

            scope.gPlace = new google.maps.places.Autocomplete(element[0], options);


            google.maps.event.addListener(scope.gPlace, 'place_changed', function () {
                scope.$apply(function () {
                    scope.details = scope.gPlace.getPlace();
                    model.$setViewValue(element.val());
                    $rootScope.$broadcast('place_changed', scope.details);
                });
            });

            //  google.maps.event.addListener(scope.gPlace, 'place_changed', function () {
            //element.onChange(function () {

            //});

            //var geoComponents = scope.gPlace.getPlace();
            //var latitude = geoComponents.geometry.location.lat();
            //var longitude = geoComponents.geometry.location.lng();
            //var addressComponents = geoComponents.address_components;
            //var returnAddress = [];

            //var obj = new Object()
            //angular.forEach(addressComponents, function (value, key) {

            //    var typeVal = '';
            //    switch (value.types[0]) {
            //        case "locality": // city
            //            obj.City = value.long_name;
            //            break;
            //        case "administrative_area_level_1": // state
            //            obj.StateLong = value.long_name;
            //            obj.State = value.short_name;
            //            break;

            //        case "administrative_area_level_2": // state
            //            obj.administrative_area_level_2 = value.long_name;
            //            break;
            //        case "country": // country
            //            obj.country = value.long_name;
            //            obj.countryShort = value.short_name;
            //            break;
            //        case "postal_code": // postal_code
            //            obj.Zip = value.long_name;
            //            break;
            //        default:
            //            typeVal = '';
            //    }

            //});


            //obj.latitude = latitude;
            //obj.longitude = longitude;
            //returnAddress.push(obj);


            //scope.$apply(function () {
            //   // scope.details = returnAddress; // array containing each location component
            //    model.$setViewValue(element.val());
            //});

            //   });
            //   prevent submitting form on enter
            //google.maps.event.addDomListener(element[0], 'keydown', function (e) {
            //    if (e.keyCode == 13) {
            //        model.$setViewValue(element[0].value);
            //    }
            //});
        }
    };
}]);

app.directive('ionslider', function ($timeout) {
    return {
        require: 'ngModel',
        restrict: 'E',
        scope: {
            min: '=',
            max: '=',
            ngModel: '=',
            type: '@',
            prefix: '@',
            maxPostfix: '@',
            prettify: '@',
            grid: '@',

            gridMargin: '@',
            postfix: '@',
            step: '@',
            hideMinMax: '@',
            hideFromTo: '@',
            from: '=',
            disable: '=',
            onChange: '=',
            onFinish: '=',
            onStart: '=',
            onUpdate: '='

        },
        template: '<div></div>',
        replace: true,
        link: function ($scope, $element, attrs, model) {
            (function init() {
                $element.ionRangeSlider({
                    min: $scope.min,
                    max: $scope.max,
                    type: $scope.type,
                    prefix: $scope.prefix,
                    maxPostfix: $scope.maxPostfix,
                    prettify: $scope.prettify,
                    grid: $scope.grid,
                    gridMargin: $scope.gridMargin,
                    postfix: $scope.postfix,
                    step: $scope.step,
                    hideMinMax: $scope.hideMinMax,
                    hideFromTo: $scope.hideFromTo,
                    from: $scope.from,
                    disable: $scope.disable,
                    //onChange: $scope.onChange,
                    //onFinish: $scope.onFinish,
                    onStart: $scope.onStart,
                    onUpdate: $scope.onUpdate,

                    onChange: function (a) {
                        $scope.$apply(function () {

                            model.$setViewValue(a.from);
                            //$scope.to = a.to;
                            $scope.onChange && $scope.onChange({
                                a: a
                            });
                        });
                    },
                    onFinish: function () {
                        $scope.$apply($scope.onFinish);
                    },
                });
            })();
            $scope.$watch('min', function (value) {
                $timeout(function () { $element.data("ionRangeSlider").update({ min: value }); });
            }, true);
            $scope.$watch('max', function (value) {
                $timeout(function () { $element.data("ionRangeSlider").update({ max: value }); });
            });
            $scope.$watch('from', function (value) {
                $timeout(function () { $element.data("ionRangeSlider").update({ from: value }); });
            });
        }
    }
});
app.directive('cdatePicker', function ($timeout) {
    return {
        restrict: "A",
        require: "^?ngModel",
        link: function (scope, element, attrs, ngModelCtrl) {
            var parent = $(element).parent();
            var dtp = parent.datepicker({
                format: "mm-dd-yyyy",
                showTodayButton: true
            });
            dtp.on("change", function (e) {
                //ngModelCtrl.$setViewValue(moment(e.date).format("mm-dd-yyyy"));
                //scope.$apply();
                parent.datepicker('hide');
            });
           
        }
    };
});

app.directive('cautoComplete', function ($timeout) {
    return {

        restrict: "A",
        require: "^?ngModel",
        scope: {
          
            companyList: '=?'
        },
        link: function (scope, element, attrs, ngModelCtrl) {
            var elem = $(element);
           //var eledata= elem.select2({
           //     data: scope.companyList
           //})


            scope.$watch('companyList', function (value) {
                CompList.source = value;
            })
        }
    };
});

app.directive('automapHeight', function () {
    return {
        restrict: "A",
        link: function ($scope, element, attrs) {
            //var eleHeight = element.height();
            var eleAttrs = attrs.automapHeight;
            $scope.$watch('__height', function (newHeight, oldHeight) {
                //  $('#' + eleAttrs).height = newHeight;
                var ele = element;
                $('#' + eleAttrs).attr('style', 'height: ' + newHeight);
            });

            $scope.$watch(function () {
                $scope.__height = window.getComputedStyle(element[0], null).getPropertyValue("height");
            });
        }
    };
});

app.service('SPMapService', ['ajaxService', function (ajaxService) {
    this.GetSPMapDetails = function (data,successFunction, errorFunction) {
        ajaxService.AjaxGetWithData(data,"SPMapSearch/GetSPMapDetails", successFunction, errorFunction);
    };

    this.initialize = function (successFunction, errorFunction) {
        ajaxService.AjaxGet("SPMapSearch/InitSPMapSearch", successFunction, errorFunction);
    };
    this.searchSPMap = function (data, successFunction, errorFunction) {
        ajaxService.AjaxPost(data, "SPMapSearch/SPMapSearch", successFunction, errorFunction);
    };
    this.populateZipData = function (data, successFunction, errorFunction) {
        ajaxService.AjaxPost(data, "Utility/GetZipCodeInfo", successFunction, errorFunction);
    };

    this.GetZipCode = function (data, successFunction, errorFunction) {
        ajaxService.AjaxPost(data, "SPMapSearch/SelectServiceProvider", successFunction, errorFunction);
    };

    this.AutoAssignServiceProvider = function (data, successFunction, errorFunction) {
        ajaxService.AjaxPost(data, "NewAssignment/AutoAssignServiceProvider", successFunction, errorFunction);
    };
    
    this.updateUserfavourite = function (data, successFunction, errorFunction) {
        ajaxService.AjaxPost(data, "SPMapSearch/updateUserfavourite", successFunction, errorFunction);
    };
    this.updateSPStatus = function (data, successFunction, errorFunction) {
        ajaxService.AjaxPost(data, "SPMapSearch/Reject", successFunction, errorFunction);
    };
    
    this.CreateAutoAssignment = function (data, successFunction, errorFunction) {
        ajaxService.AjaxGetWithData(data, "AutoAssignment/MapAutoAssignment", successFunction, errorFunction);
    };
}]);



app.controller('SPMapSearchController', ['$location', '$scope', '$rootScope', '$filter', '$document', '$compile', 'SPMapService', 'IADArootUrl', function ($location, $scope, $rootScope, $filter, $document, $compile, SPMapService, IADArootUrl) {

    //Parameters
    $scope.initializeController = function () {
        $scope.hdnSpidVal = "";
        $scope.hdnASGIdVal = "";
        $scope.Distance = 50;
        $scope.hdnIsQuickAssignVal = 0;
        $scope.hdnsubmitTypeVal = "";
        $scope.ActionActTab = 1
        $scope.hdnsubmitTypeVal = "LOC";
        $scope.SPMapSearchObject();
        $scope.searchFilter = true;
        $scope.SPMapVisible = true;
        $scope.gMapSearch = null;
        $scope.markerModel = null;
        $scope.getPropertyZip();
        $scope.ServiceType = '0';
        $scope.placeDetails = null;
        $scope.DistanceFilter = 200;
        $scope.SPActiveStatus = 'true';

        $scope.autoClaimSelect = true;
        $scope.preventDBLClick = false;
        $scope.autoSPDelete = false;

        $scope.sliderVM = {
            min: 10,
            max: 500,
            fromVal: 200
        }
    }
    $scope.getPropertyZip = function () {
        // var path = $location.absUrl();
        var path = location.search;

        var parsePath = path.split("?");
        if (parsePath.length > 1) {
            //  var parentPath = parsePath[1];
            var queryParam = parsePath[1].split("=");
            var paraname = queryParam[0];
            var paraVal = queryParam[1];
            if (paraname != 'sptype') {
                var postData = { assignmentId: paraVal }
                SPMapService.GetZipCode(postData, $scope.getPropertyZipCompleted, $scope.getPropertyZipError)
            }
        }
    };
    $scope.getPropertyZipCompleted = function (response, status) {
        $scope.initSPData(response);
        $scope.gMapSearch = response.assignmentSearchDet;
    }
    $scope.getPropertyZipError = function (response, status) {
       // window.location = "/index.html";
    }
    $scope.ResetFilter = function () {
        $scope.sliderVM.fromVal = 10;
        $scope.SPName = null;
        $scope.Rank = null;
        $scope.Distance = 50;
        $scope.DistanceFilter = 50;
    }
    $scope.slideChange = function () {
        var s = 0;
    }
    //Google MAP Search

    $scope.gPlace;
    $scope.details;




    //ACTION ACTIVE TAB
    $scope.setActiveActionTB = function (tabVal) {
        $scope.ActionActTab = tabVal;
        if ($scope.ActionActTab === 1) {
            $scope.hdnsubmitTypeVal = "LOC";
        }
        else if ($scope.ActionActTab === 2) {
            $scope.hdnsubmitTypeVal = "SA";
        }
    };

    $scope.SPMapSearchObject = function () {

        SPMapService.initialize($scope.SPMapSearchCompleted, $scope.SPMapSearchError)
    };

    $scope.initSearchFilter = function (response) {
        $scope.ServiceOfferings = response.ServiceOfferings;
        $scope.HeadCompanyList = response.HeadCompanyList;
        $scope.ServiceType = '0';
        $scope.initSPData(response);
    }
    $scope.initContent = function (value, type) {
        var div12 = ' <table class="table table-bordered table-striped table-condensed flip-content">';
        if (type == 1) {
            //$scope.spInfoData = value;
            var div = '<tbody>' +
                 '<tr><td style="font-weight: 800">' + value.CompanyName + '</td></tr><tr><td>' + value.City + ', ' + value.State + ' ' + value.Zip + '</td></tr>' +
                 '<tr><td>' + (value.Distance?value.Distance:0) + ' Miles</td></tr></tbody><tfoot><tr><td>  <button type="button" style="width:90px;" ng-show="autoClaimSelect" ng-click="AssignSP(' + value.UserId + ')" class="btn green-meadow">Select</button>' +
               '  <button type="button" width="90px" ng-click="spMoreInfo(' + value.UserId + ')" class="btn blue">More Info</button>'+
               ' <button type="button" ng-if="autoSPDelete &&'+!(value.MgrId==null)+'" data-toggle="modal" ng-click="SPMapDelete(' + value.UserId + ')" style="width:90px;margin-top: 5px;" class="btn red">Delete</button></tr></td></tfoot></table> ';
        }
        else {
            var div = '<tbody>' +
             '<tr><td style="font-weight: 800">' + value.CompanyName + '</td></tr><tr><td></td></tr>' +
             '<tr><td>' + value.City + '</td></tr></tbody><tfoot><tr><td>  ' +
             ' </tr></td></tfoot></table> ';
        }
        return div12 + div
    }
    $scope.SetActiveSPSearchTab = function (SPSearchType)
    {
        if (SPSearchType == 1)
        {
            $scope.autoSPDelete = true;
        }
    }
    $scope.SPMapDelete = function (spID) {
        if(confirm("Are you sure want to delete this Service Provider"))
        {
            var PostData = { hdnRejectUserId: spID ,txtRejectRemarks:""};
            SPMapService.updateSPStatus(PostData, $scope.SPStatusUpdateCompleted, $scope.SPStatusUpdateError)
        }
    }
    $scope.SPStatusUpdateCompleted = function (response,status) {
        $scope.initSPData(response);
    }
    $scope.SPStatusUpdateError = function (response, status) {

    }
    $scope.initSPData = function (response) {
        $scope.Splist = response.Splist;
        $scope.autoClaimSelect = response.IsActiveSP;
        $scope.SetActiveSPSearchTab(response.SPMapSearchtype);
        if (!angular.isUndefined($scope.Splist) && $scope.Splist!=null && $scope.Splist.length > 0)
        {
            $scope.SPClass = 'active';
        }
        //    $scope.Addmark();
        $scope.markerModel = [];
        if ($scope.SearchedLoC.City != "") {
            $scope.markerModel.push({
                position: $scope.SearchedLoC.position,
                title: "LOC",
                content: $scope.initContent($scope.SearchedLoC, 2)
            })
        }
        angular.forEach($scope.Splist, function (value, key) {
            $scope.markerModel.push({
                position: new google.maps.LatLng(value.Lat, value.Lng),
                title: value.MapPinColor,
                content:$compile($scope.initContent(value,1))($scope)[0]
            })
        })
       
    }

    $scope.SearchedLoC = {
        position: "",
        title: "text",
        City: "",
        CompanyName: "Searched location"
    }

    $scope.SPMapSearchCompleted = function (response, status) {
        $scope.initSearchFilter(response);
    }
    $scope.SPMapSearchError = function (response, status) {
       // window.location = "/index.html";
    }

    //on click select button
    //$scope.SPMapVisible = true;
    //$scope.AssignSP = function () {
    //    debugger;
    //    var url = location.search;

    //    if (url == "")
    //    {
    //        //window.location = "AutoAssignment/AutoAssignment";
    //        $scope.SPMapVisible = false;
    //    }
    //    else {
    //        window.location = "AutoAssignment/AutoSearch";
    //    }        
    //}
    //form search click
    $scope.searchSPMap = function () {
        $scope.searchFilter = false;
        var postData = { search: $scope.createSPMapSearchVM() }
        SPMapService.searchSPMap(postData, $scope.searchSPMapCompleted, $scope.searchSPMapError)
    };
    $scope.searchSPMapclick = function () {
        $scope.getAddressDetails($scope.gMapSearch);
    }
    $scope.searchSPMapPopupclick = function () {
        $scope.Distance = $scope.DistanceFilter;
        $scope.getAddressDetails($scope.gMapSearch);
     
    }
    $scope.retriveSpaceDetails = function (results,searchedResult) {
        var geoComponents = results;
        var latitude = geoComponents.geometry.location.lat();
        var longitude = geoComponents.geometry.location.lng();
        var addressComponents = geoComponents.address_components;
        var returnAddress = [];

        var obj = new Object()
        angular.forEach(addressComponents, function (value, key) {

            var typeVal = '';
            switch (value.types[0]) {
                case "locality": // city
                    obj.City = value.long_name;
                    break;
                case "administrative_area_level_1": // state
                    obj.StateLong = value.long_name;
                    obj.State = value.short_name;
                    break;

                case "administrative_area_level_2": // state
                    obj.administrative_area_level_2 = value.long_name;
                    break;
                case "country": // country
                    obj.country = value.long_name;
                    obj.countryShort = value.short_name;
                    break;
                case "postal_code": // postal_code
                    obj.Zip = value.long_name;
                    break;
                default:
                    typeVal = '';
            }

        });


        obj.latitude = latitude;
        obj.longitude = longitude;
        returnAddress.push(obj);
        $scope.SearchedLoC.position = new google.maps.LatLng(latitude, longitude);
        $scope.SearchedLoC.City = searchedResult;
        if (returnAddress.length > 0) {
            $scope.placeDetails = returnAddress;
            $scope.searchSPMap();
        }
    }

    $scope.$on('place_changed', function (e, place) {
        if (place.address_components != null) {
            $scope.retriveSpaceDetails(place, $scope.gMapSearch);
        }
        else {
            $scope.getAddressDetails($scope.gMapSearch);
        }
    });
    $scope.getAddressDetails = function (firstResult) {
        //   var firstResult = document.getElementsByClassName(".pac-container .pac-item:first"); //$(".pac-container .pac-item:first").text();
        // var result = document.getElementById('gmap_geocoding_address');//document.getElementsByClassName("pac-container pac-item");

        var modelR = $scope.gMapSearch;
        //if (firstResult=="") {
        //firstResult = angular.element(document.getElementById('gmap_geocoding_address'))[0].value;
        //}
        if (firstResult != null && firstResult != "") {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ "address": firstResult }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    //var lat = results[0].geometry.location.lat(),
                    //    lng = results[0].geometry.location.lng(),
                    //    placeName = results[0].address_components[0].long_name;
                    //commented
                    $scope.retriveSpaceDetails(results[0], firstResult);
                    //var geoComponents = results[0];
                    //var latitude = geoComponents.geometry.location.lat();
                    //var longitude = geoComponents.geometry.location.lng();
                    //var addressComponents = geoComponents.address_components;
                    //var returnAddress = [];

                    //var obj = new Object()
                    //angular.forEach(addressComponents, function (value, key) {

                    //    var typeVal = '';
                    //    switch (value.types[0]) {
                    //        case "locality": // city
                    //            obj.City = value.long_name;
                    //            break;
                    //        case "administrative_area_level_1": // state
                    //            obj.StateLong = value.long_name;
                    //            obj.State = value.short_name;
                    //            break;

                    //        case "administrative_area_level_2": // state
                    //            obj.administrative_area_level_2 = value.long_name;
                    //            break;
                    //        case "country": // country
                    //            obj.country = value.long_name;
                    //            obj.countryShort = value.short_name;
                    //            break;
                    //        case "postal_code": // postal_code
                    //            obj.Zip = value.long_name;
                    //            break;
                    //        default:
                    //            typeVal = '';
                    //    }

                    //});


                    //obj.latitude = latitude;
                    //obj.longitude = longitude;
                    //returnAddress.push(obj);
                    //$scope.SearchedLoC.position = new google.maps.LatLng(latitude, longitude);
                    //$scope.SearchedLoC.City = firstResult;
                    //if (returnAddress.length > 0) {
                    //    $scope.placeDetails = returnAddress;
                    //    $scope.searchSPMap();
                    //}
                }
            });
        }
        else {
            $scope.placeDetails = undefined;
            $scope.SearchedLoC.City = "";
            $scope.searchSPMap();
        }
    }
    $scope.enterClick = function (event) {
        if (event.which === 13) {
            //   $rootScope.$broadcast('getPlacesDetails', {});
            //   event.preventDefault();
            // google.maps.event.trigger($scope.gPlace, 'place_changed');
            $scope.getAddressDetails();

        }
    }

    $scope.searchSPMapCompleted = function (response, status) {
        $scope.currentPage = 0;
        if (response.IsLoggedIN == true) {
            $scope.initSPData(response);
        }
        else {
            window.location =IADArootUrl+'Account/Login';
        }
    }
    $scope.searchSPMapError = function (response, status) {
       // window.location = "/index.html";
    }

    $scope.populateZipData = function () {
        var postData = { ZipCode: ($scope.Zip || '') }
        SPMapService.populateZipData(postData, $scope.populateZipDataCompleted, $scope.populateZipDataError)
    }
    $scope.populateZipDataCompleted = function (response, status) {
        if (response != -1) {
            $scope.State = response.State
            $scope.City = response.City
        }
    }
    $scope.populateZipDataError = function (response, status) {
      //  window.location = "/index.html";
    }
    //create Search Filter SPMapSearchViewModel
    $scope.createSPMapSearchVM = function () {

        var SPMapSearchVM = new Object();
        //SPMapSearchVM.AlphSelected = $scope.AlphSelected;
        //SPMapSearchVM.City = $scope.City;
        //SPMapSearchVM.State = $scope.State;
        //SPMapSearchVM.Zip = $scope.Zip;
        SPMapSearchVM.Distance = $scope.Distance;
        SPMapSearchVM.SPName = $scope.SPName;
        SPMapSearchVM.Rank = $scope.Rank;
        SPMapSearchVM.ServiceType = $scope.ServiceType;
        SPMapSearchVM.SPActiveStatus = $scope.SPActiveStatus;
        //SPMapSearchVM.SeverityLevel = $scope.SeverityLevel;
        //SPMapSearchVM.LossType = $scope.LossType;
        //SPMapSearchVM.CatCode = $scope.CatCode;
        //SPMapSearchVM.InsuranceCompany = $scope.InsuranceCompany;
        SPMapSearchVM.AddressDetails = (angular.isUndefined($scope.placeDetails) || $scope.placeDetails==null) ? null : $scope.placeDetails[0];
        //Hidden fields
        //SPMapSearchVM.hdnSpidVal = $scope.hdnSpidVal ;
        //SPMapSearchVM.hdnASGIdVal = $scope.hdnASGIdVal ;
        //SPMapSearchVM.hdnIsQuickAssignVal = $scope.hdnIsQuickAssignVal;
        //SPMapSearchVM.hdnsubmitTypeVal = $scope.hdnsubmitTypeVal;

        return SPMapSearchVM;

    };



    //pagination

    $scope.currentPage = 0;
    $scope.pageSize = 6;

    $scope.q = '';
    $scope.getData = function () {
        // needed for the pagination calc
        // https://docs.angularjs.org/api/ng/filter/filter

        return $filter('filter')($scope.Splist, $scope.q)



    }
    $scope.numberOfPages = function () {
        if ($scope.getData() !== null && angular.isDefined($scope.getData())) {
            return Math.ceil($scope.getData().length / $scope.pageSize);
        }
    }

    $scope.dataSetLength = function () {
        if ($scope.getData() !== null && angular.isDefined($scope.getData())) {
            return $scope.getData().length;
        }
        else {
            return 1;
        }
    }

    $scope.IsSplistExist = function () {
        if ($scope.Splist == null || angular.isUndefined($scope.Splist) || $scope.Splist.length==0) {
            return true;
        }
        else {
            return false;
        }
    }

    $scope.ratingSet = function (a, b) {
        return a >= b;
    }

   // $scope.modalTarget ='none';
    //SP More Info Modal
    $scope.spMoreInfo = function (UserID) {

        //if (angular.isNumber(SPInfo))
        //{
        //    var countloop = false;
        //    angular.forEach($scope.Splist, function (item) {
        //        if (!countloop&&item.UserId === SPInfo) {
        //            SPInfo = item;
        //            countloop = true;
        //        }
        //    });
        //}
      //  $scope.SPInfoMiles = Distance;
        var postData = { SPID: UserID };
        SPMapService.GetSPMapDetails(postData, $scope.GetSPMapDetailsCompleted, $scope.GetSPMapDetailsError)

        //$scope.SPInfo = {
        //    UserName:SPInfo.Firstname+' '+SPInfo.LastName,
        //    SpName: SPInfo.CompanyName,
        //    Address:SPInfo.City+', '+SPInfo.State+' '+SPInfo.Zip,
        //    Phone: SPInfo.MobilePhone,
        //    Email: SPInfo.Email,
        //    Website: '',
        //    Category: SPInfo.ServiceTypes,
        //    Location:SPInfo.City+', '+SPInfo.State+' '+SPInfo.Zip,
        //    Tags: SPInfo.City + ', ' + SPInfo.State + ' ' + SPInfo.Zip,
        //    Rank: SPInfo.OverdueRank,
        //    OpenClaims: SPInfo.OpenClaimsCount,
        //    Miles: SPInfo.Distance+' Miles'
        //}

        //$("#spMoreInfoModal").modal("show");
       // angular.element('#spMoreInfoModal').modal("show");
    }
    $scope.GetSPMapDetailsCompleted = function (response, status) {

        var SPInfo = response.result;
        $scope.SPInfo = {
            spID:SPInfo.UserId,
            UserName: SPInfo.Firstname + ' ' + SPInfo.LastName,
            SpName: SPInfo.CompanyName,
            Address:SPInfo.City + ', ' + SPInfo.State + ' ' + SPInfo.Zip,
            Phone: SPInfo.MobilePhone,
            Email: SPInfo.Email,
            Website: SPInfo.WebsiteURL,
            Category: SPInfo.ServiceTypes,
            Location: SPInfo.City + ', ' + SPInfo.State + ' ' + SPInfo.Zip,
            Tags: SPInfo.City + ', ' + SPInfo.State + ' ' + SPInfo.Zip,
            Rank: SPInfo.Rank,
            OpenClaims: SPInfo.OpenClaimsCount,
           // Miles: $scope.SPInfoMiles,
            BusinessDesc: SPInfo.BusinessDesc,
            ImagePath: SPInfo.imagePath,
            userfavourite: SPInfo.IsFavourite,
            internalContact: SPInfo.internalContact,
            StreetAddress: SPInfo.StreetAddress,
            LinkedInAddress: SPInfo.LinkedInAddress

        }
        $("#spMoreInfoModal").modal("show");
    }
    $scope.nullEmptyCheck = function (value)
    {
        if (value)
            return true;
        else
            return false;
    }
    $scope.GetSPMapDetailsError = function (response, status) {
      //  window.location = "/index.html";
    }
    //Create Assignment View
    $scope.setSelectedItemPage = function (Userid)
    {
        //filter the array
        var foundItem = $filter('filter')($scope.Splist, { UserId: Userid }, true)[0];
        var itemIndex = ($scope.Splist.indexOf(foundItem)+1)/6;
        var calval= Math.ceil( ($scope.Splist.indexOf(foundItem)+1) / $scope.pageSize);
        $scope.currentPage = calval-1;

        //$scope.currentPage = (itemIndex.indexOf('.') > -1 ? (parseInt(itemIndex, 10)) : itemIndex-1);
    }

    $scope.CntrlMD = 'col-md-12';
    //on click select button
    $scope.SPMapVisible = true;
    $scope.AssignSP = function (UserId) {
        debugger;
        $scope.preventDBLClick = true;
        $scope.setSelectedItemPage(UserId);
        angular.element(document.getElementById("CAutoAssignmentID")).val(UserId);
        $scope.assignmentID ={item:UserId};
        var url = location.search;

        if (url == "" || url == "?sptype=fav") {
            //window.location = "AutoAssignment/AutoAssignment";
            $scope.SPMapVisible = false;
            var postData = { Claimid: -1 ,SPID : UserId };
            SPMapService.CreateAutoAssignment(postData, $scope.CreateAutoAssignmentCompleted, $scope.CreateAutoAssignmentError);
        }
        else {
            var ID = url.split("=");
            var AssignmentID = ID[1];
            //alert("Want to Assign Service Provider");
           
                $scope.AutoAssignServiceProvider(AssignmentID, UserId);
                window.location = "AutoAssignment/AutoSearch";
        }

    }

    $scope.CreateAutoAssignmentCompleted = function (response, status) {
        $("#spmapautoassignment").html(response);
    };
    $scope.CreateAutoAssignmentError = function (response, status) {

    }

    $scope.AutoAssignServiceProvider = function (assignmentId, OAUserId) {
        var postData = { assignmentId: assignmentId, OAUserId: OAUserId };
        SPMapService.AutoAssignServiceProvider(postData, $scope.AutoAssignServiceProviderCompleted, $scope.AutoAssignServiceProviderError)
    };
    $scope.AutoAssignServiceProviderCompleted = function (response, status) {
        
    }
    $scope.AutoAssignServiceProviderError = function (response, status) {
     //   window.location = "/index.html";
    }

    //Active User Guid
    $scope.userGuid = {
        type: "",
        active: false,
        done:false
    }
    $scope.validateFilter = function (type) {
        if (type == 'sType') {
            if (angular.isUndefined($scope.ServiceType) || $scope.ServiceType == '0') {
                $scope.sTypeClass = 'active';
                $scope.PlaceClass = '';
            } else {
                $scope.sTypeClass = 'done';
                $scope.PlaceClass = 'active';
            }
        }
        else (!angular.isUndefined($scope.ServiceType) && $scope.ServiceType !== "0" && type == 'Place')
        {
            if ($scope.gMapSearch!=null && $scope.gMapSearch.length > 0) {
                $scope.PlaceClass = 'done';
            } 
        }
    }
    $scope.popup2 = {
        opened: false
    };

    //Update User Userfavourite
    $scope.updateUserfavourite = function (spID)
    {
        var postData = { SPid: spID, isFavourite: $scope.SPInfo.userfavourite };
        SPMapService.updateUserfavourite(postData, $scope.updateUserfavouriteCompleted, $scope.updateUserfavouriteError)
    }
    $scope.updateUserfavouriteCompleted = function (response, status) {

    }
    $scope.updateUserfavouriteError = function (response, status) {
        //   window.location = "/index.html";
    }

}]);//end controller

    //app.controller('SPMapSearchController12', ['$scope', function ($scope) {
    //    //alert('sss');
    //    $scope.CntrlMD = 'col-md-12';
    //}]);
