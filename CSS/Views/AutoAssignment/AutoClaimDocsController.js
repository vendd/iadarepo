﻿"use strict";

var app = angular.module('CSSMVCApp');
app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
     
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
            scope.$watch(
                      attrs.fileModel,
                      function (newValue, oldValue) {
                          if (newValue==null)
                          element[0].value = '';

                      }
                  );

        }
    };
}]);
app.filter('Dateform', function ($filter) {

    return function (jsonDate) {
        var tempdate = parseInt(jsonDate.substr(6));
        var date = $filter('date')(tempdate, "MM/dd/yyyy");
        return date;
    };

});
app.service('AutoClaimDocService', ['ajaxService', function (ajaxService) {
    this.GetDocumentLink = function (data,responseType, successFunction, errorFunction) {
        ajaxService.AjaxGetWithDataResponseType(data, "AutoAssignment/DocumentAcessFilePath", responseType, successFunction, errorFunction);
    };
    this.InitializeDocuments = function (data, successFunction, errorFunction) {
        ajaxService.AjaxGetWithData(data, "AutoAssignment/InitializeDocuments", successFunction, errorFunction);
    };
    this.initialize = function (data,successFunction, errorFunction) {
        ajaxService.AjaxGetWithData(data, "AutoAssignment/InitAutoClaimDocuments", successFunction, errorFunction);
    };
    //Customer Image upload
    this.DocUpload = function (postData, successFunction, errorFunction) {
        ajaxService.AjaxPostwithFile(postData, "AutoAssignment/UploadClaimDocument", successFunction, errorFunction);
    };
    this.updateDocStatus = function (data, successFunction, errorFunction) {
        ajaxService.AjaxPost(data, "AutoAssignment/ChangeDocumentStatus", successFunction, errorFunction);
    };
    this.initEmailDoc = function (data, successFunction, errorFunction) {
        ajaxService.AjaxGetWithData(data, "AutoAssignment/ClaimEmailDocuments", successFunction, errorFunction);
    };
    this.sendEmailDoc = function (data, successFunction, errorFunction) {
        ajaxService.AjaxPost(data, "AutoAssignment/EmailDocuments", successFunction, errorFunction);
    };
    this.deleteDoc = function (data, successFunction, errorFunction) {
        ajaxService.AjaxPost(data, "AutoAssignment/DeleteClaimDocument", successFunction, errorFunction);
    };
}]);



app.controller('AutoClaimDocsController', ['$location', '$scope', '$rootScope', '$filter', '$document', '$compile', 'AutoClaimDocService', '$sce', function ($location, $scope, $rootScope, $filter, $document, $compile, AutoClaimDocService, $sce) {

    //Parameters
    $scope.initializeController = function () {
        debugger;
        $rootScope.IsSpinnerVisible = false;
        $scope.autoClaim.AutoClaimId = angular.element(document.getElementById("AutoClaimId")).val();
        $scope.autoClaim.AutoAssignmentId = angular.element(document.getElementById("AutoAssignmentId")).val();
        var postData = { claimid: $scope.autoClaim.AutoClaimId, AssingmentID: $scope.autoClaim.AutoAssignmentId, documentTypeId: $scope.DocumentTypeId.item }
       // $scope.initAutoClaimDocs(postData);
        AutoClaimDocService.InitializeDocuments(postData, $scope.InitializeDocumentsCompleted, $scope.InitializeDocumentsError)
    }
    $scope.InitializeDocumentsCompleted = function (response, status) {
        $scope.ClaimDocumentTypeList = response.ClaimDocumentTypeList;
        $scope.DocumentsList = response.DocumentsList;
        $scope.DocumentsPreviousList = response.DocumentsPreviousList;
        $scope.DocumentsRejectedList = response.DocumentsRejectedList;
        $scope.totalDocs.totalCount = 0;
        angular.forEach($scope.ClaimDocumentTypeList, function (value, key) {
            $scope.totalDocs.totalCount += value.DocCount;
        });
        $scope.StatusList = response.StatusList;
        $scope.selectedStatus.item = '4';
    }
    $scope.InitializeDocumentsError = function (response, status) {
        //  window.location = "/index.html";
    }
    $scope.DocumentTypeId = {item:0}

    $scope.autoClaim = {
        AutoClaimId:0,AutoAssignmentId:0
    }
    $scope.totalDocs = {
        totalCount:0
    }
    $scope.documentTypeClick = function (DocumentTypeId) {
        $scope.DocumentTypeId.item = DocumentTypeId;
        $scope.CheckedId = undefined;
        var postData = { claimid: $scope.autoClaim.AutoClaimId, AssingmentID: $scope.autoClaim.AutoAssignmentId, documentTypeId: $scope.DocumentTypeId.item }
        $scope.initAutoClaimDocs(postData);
    }
    $scope.initAutoClaimDocs = function (postData) {
       
        AutoClaimDocService.initialize(postData, $scope.AutoClaimDocsCompleted, $scope.AutoClaimDocsError)
    }
    $scope.AutoClaimDocsCompleted = function (response, status) {
        $scope.initDocumentModel(response);
    }
    $scope.AutoClaimDocsError = function (response, status) {
      //  window.location = "/index.html";
    }
    $scope.filterDocList = function () {
            var postData = { claimid: $scope.autoClaim.AutoClaimId, AssingmentID: $scope.autoClaim.AutoAssignmentId, documentTypeId: $scope.DocumentTypeId.item, statusId: $scope.selectedStatus.item }
            $scope.initAutoClaimDocs(postData);
    }

    $scope.getUploadedFile = function () {
        var fd = new FormData();
        fd.append('uploadFile', $scope.imagefile);
        fd.append('AutoDocument.Title', $scope.DocTitle);
        fd.append('AutoDocument.AutoAssignmentId', $scope.autoClaim.AutoAssignmentId);
        fd.append('AutoDocument.DocumentTypeId', $scope.DocumentTypeId.item);
        fd.append('AutoDocument.AutoDocumentUploadedByUserId', 13);
        fd.append('ClaimId', $scope.autoClaim.AutoClaimId);
        return fd;
    };
    $scope.initDocumentModel = function (response) {

        $scope.ClaimDocumentTypeList = response.ClaimDocumentTypeList;
        $scope.DocumentsList = response.DocumentsList;
        $scope.DocumentsPreviousList = response.DocumentsPreviousList;
        $scope.DocumentsRejectedList = response.DocumentsRejectedList;
        $scope.totalDocs.totalCount = 0;
        angular.forEach($scope.ClaimDocumentTypeList, function (value, key) {
            $scope.totalDocs.totalCount += value.DocCount;
        });
        $scope.AllDocumentsCount = totalDocs;
       // $scope.StatusList = response.StatusList;
       // $scope.selectedStatus.item = '4';
    }

    //UPLOAD Customer image
    $scope.saveDocument = function () {
        if ($scope.uploadDocsForm.$valid) {
            var PostData = $scope.getUploadedFile();
            $('#uploadDocsPopup').modal('hide');
            AutoClaimDocService.DocUpload(PostData, $scope.saveDocumentCompleted, $scope.saveDocumentError)
        }
    };


    $scope.saveDocumentCompleted = function (response, status) {
        $('#uploadDocsPopup').modal('hide');
        $scope.imagefile = null;
        $scope.DocTitle = '';
        $scope.initDocumentModel(response);
    }
    $scope.saveDocumentError = function (response, status) {
       // window.location = "/index.html";
    }

    $scope.DocChecked = function () {
        $scope.Checkedval = "";
        $scope.CheckedId = "";
        $scope.spn = false;
        angular.forEach($scope.DocumentsList, function (value, key) {
            if (value.checked) {
                $scope.spn = true;
                if ($scope.CheckedId.length == 0) {
             
                    $scope.CheckedId = value.AutoDocumentId;
                }
                else {
                
                    $scope.CheckedId += ", " + value.AutoDocumentId;
                }
            }
        });
        //Previously uploaded Docs
        angular.forEach($scope.DocumentsPreviousList, function (value, key) {
            if (value.checked) {
                $scope.spn = true;
                if ($scope.CheckedId.length == 0) {

                    $scope.CheckedId = value.AutoDocumentId;
                }
                else {

                    $scope.CheckedId += ", " + value.AutoDocumentId;
                }
            }
        });

        //Rejected uploaded Docs
        angular.forEach($scope.DocumentsRejectedList, function (value, key) {
            if (value.checked) {
                $scope.spn = true;
                if ($scope.CheckedId.length == 0) {

                    $scope.CheckedId = value.AutoDocumentId;
                }
                else {

                    $scope.CheckedId += ", " + value.AutoDocumentId;
                }
            }
        });
    }

    //Update Doc Status

    //UPLOAD Customer image
    $scope.statusUpdate = '';
    $scope.changeStatus = function (statusUpdate) {
        if (angular.isUndefined($scope.CheckedId) || $scope.CheckedId.length == 0) {
            alert("Select Document(s) from List");
        }
        else {
            var statutext = statusUpdate == 1 ? 'Approve' : 'Reject';
            var r = confirm("Are you sure want to change status to " + statutext);
            if (r == true) {

                var doc = new Object();

                doc.AutoAssignmentId = $scope.autoClaim.AutoAssignmentId;
                doc.DocumentTypeId = $scope.DocumentTypeId.item;
                doc.ClaimId = $scope.autoClaim.AutoClaimId;
                doc.documentIds = $scope.CheckedId;
                doc.newStatusId = statusUpdate;
                var PostData = doc;
                AutoClaimDocService.updateDocStatus(PostData, $scope.updateDocStatusCompleted, $scope.updateDocStatusError)
            }
        }
      };


    $scope.updateDocStatusCompleted = function (response, status) {
       
        $scope.initDocumentModel(response);
    }
    $scope.updateDocStatusError = function (response, status) {
        // window.location = "/index.html";
    }

       
    $scope.selectedStatus ={ item:'4'};


    $scope.initEmailpopup = function () {
        if (angular.isUndefined($scope.CheckedId) || $scope.CheckedId.length == 0) {
            alert("Select Document(s) from List");
        }
        else {
            var postData = { claimId: $scope.autoClaim.AutoClaimId, assignmentId: $scope.autoClaim.AutoAssignmentId, documentIds: $scope.CheckedId, documentTypeId: $scope.DocumentTypeId.item, statusId: $scope.selectedStatus.item }

            AutoClaimDocService.initEmailDoc(postData, $scope.initEmailDocCompleted, $scope.initEmailDocError)
        }
     }
    $scope.DocEmailList = function () {
      
        $scope.docEmailids = "";
        $scope.spn = false;
        angular.forEach($scope.DocumentsListMail, function (value, key) {
        
                if ($scope.docEmailids.length == 0) {

                    $scope.docEmailids = value.DocumentId;
                }
                else {

                    $scope.docEmailids += ", " + value.DocumentId;
                }
         
        });
    }
    $scope.initEmailDocCompleted = function (response, status) {
        $scope.DocumentsListMail = response.DocumentsListMail;
        $scope.docEmail = {
            ToAdd:response.ClaimParticipantsEmailAddresses,
            Subject: response.subject,
            Body: '',
         
            //ReplayTo: response.authReplayUser, //INPSW-8 - Change Default Send Reply To Email Address used on Documents Email
            ReplayTo: 'support@iada.org',
            Ismergedoc: false,
            //IsReplayTo:false,
            IsReplayTo: true,
        }
        $('#emailDocsPopup').modal('show');
    }
    $scope.initEmailDocError = function (response, status) {
        // window.location = "/index.html";
    }

    $scope.sendDocEmail = function () {
        var mergeval = "0";
        var replayval = 0;

        if ($scope.docEmail.Ismergedoc) mergeval = "1";
        if ($scope.docEmail.IsReplayTo) replayval = 1;
        $scope.DocEmailList();
        var postData = {
            assignmentId: $scope.autoClaim.AutoAssignmentId, documentIds: $scope.docEmailids, setReplyToEmail: replayval, toEmails: $scope.docEmail.ToAdd, subject: $scope.docEmail.Subject,
            body: $scope.docEmail.Body, mergedoc: mergeval
        }

        AutoClaimDocService.sendEmailDoc(postData, $scope.sendEmailDocCompleted, $scope.sendEmailDocError)
    }
    $scope.sendEmailDocCompleted = function (response, status) {
        
            $('#emailDocsPopup').modal('hide');
      
    }
    $scope.sendEmailDocError = function (response, status) {
        // window.location = "/index.html";
    }

    //Doc Delete

    $scope.deleteDoc = function (docId) {
        var r = confirm("Are you sure want to delete this document!");
        if (r == true) {
            var postData = { claimid: $scope.autoClaim.AutoClaimId, claimDocumentId: docId }

            AutoClaimDocService.deleteDoc(postData, $scope.deleteDocCompleted, $scope.deleteDocError)
        }
    }
    $scope.deleteDocCompleted = function (response, status) {
        $scope.initDocumentModel(response);
    }
    $scope.deleteDocError = function (response, status) {
        // window.location = "/index.html";
    }


    //doc Download
    $scope.createdoclk = function (docID) {
        return '/AutoAssignment/DocumentAcess?docTypeId=' + docID;
    }
    $scope.FileOpen = function (docID) {
        debugger;
        var postData = { docTypeId: docID }
        AutoClaimDocService.GetDocumentLink(postData, 'arraybuffer', $scope.GetDocumentLinkCompleted, $scope.GetDocumentLinkError)
     
      // $("#ifrmDocView").attr("src", $scope.FilePathURL);
        //alert($scope.FilePathURL);
        //return '/AutoAssignment/DocumentAcessFilePath?docTypeId=' + docID;
        //$scope.FilePathURL = '/AutoAssignment/DocumentAcessFilePath?docTypeId=' + docID;
        //alert($scope.FilePathURL);
        //return 
    }

    $scope.GetDocumentLinkCompleted = function (response, status, headers) {
        //  console.log(response);
        var contentType = headers('Content-Type');
        var file = new Blob([response], { type: contentType });
        var fileURL = URL.createObjectURL(file);
        $scope.FilePathURL = $sce.trustAsResourceUrl(fileURL);
        //$("#ifrmDocView").attr("src", response);
        $("#ifrmdocview").attr("src", "" + $scope.FilePathURL);
        $("#loadinglist").hide();
        $("#divViewDoc").modal("show");
       // window.open(fileURL);
    }
    $scope.GetDocumentLinkError = function (response, status) {
        // window.location = "/index.html";
    }

}]);//end controller
