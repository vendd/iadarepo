﻿"use strict";

var app = angular.module('CSSMVCApp');

app.directive('imageModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',

        link: function (scope, element, attrs) {
            var model = $parse(attrs.imageModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
            scope.$watch(
                      attrs.imageModel,
                      function (newValue, oldValue) {
                          if (newValue == null)
                              element[0].value = '';

                      }
                  );

        }
    };
}]);
app.directive('useruidModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',

        link: function (scope, element, attrs) {
            var model = $parse(attrs.useruidModel);
            var modelSetter = model.assign;
          
            scope.$watch(
                      attrs.useruidModel,
                      function (newValue, oldValue) {
                          modelSetter(scope, element[0].value);

                      }
                  );

        }
    };
}]);
app.directive("passwordVerify", function () {
    return {
        require: "ngModel",
        scope: {
            passwordVerify: '='
        },
        link: function (scope, element, attrs, ctrl) {
            scope.$watch(function () {
                var combined;

                if (scope.passwordVerify || ctrl.$viewValue) {
                    combined = scope.passwordVerify + '_' + ctrl.$viewValue;
                }
                return combined;
            }, function (value) {
                if (value) {
                    ctrl.$parsers.unshift(function (viewValue) {
                        var origin = scope.passwordVerify;
                        if (origin !== viewValue) {
                            ctrl.$setValidity("passwordVerify", false);
                            return undefined;
                        } else {
                            ctrl.$setValidity("passwordVerify", true);
                            return viewValue;
                        }
                    });
                }
            });
        }
    };
});

app.service('UserProfileService', ['ajaxService', function (ajaxService) {
   
    //Customer Image upload
    this.DocUpload = function (postData, successFunction, errorFunction) {
        ajaxService.AjaxPostwithFile(postData, "Account/UploadDocument", successFunction, errorFunction);
    };
    //Customer Image upload
    this.updatePassword = function (data, successFunction, errorFunction) {
        ajaxService.AjaxPost(data, "Account/ChangePassword", successFunction, errorFunction);
    };

    //Customer Image upload
    this.InitializeUserProfile = function ( successFunction, errorFunction) {
        ajaxService.AjaxGet("Account/InitializeUserProfile", successFunction, errorFunction);
    };

    //update User Profile
    this.updateUserProfile = function (data, successFunction, errorFunction) {
        ajaxService.AjaxPost(data, "Account/updateUserProfile", successFunction, errorFunction);
    };
  
}]);
app.controller('UserProfileController', ['$location', '$scope', '$rootScope', '$filter', '$document', '$compile', 'UserProfileService', function ($location, $scope, $rootScope, $filter, $document, $compile, UserProfileService) {

    $scope.initializeController = function () {
        debugger;
        $rootScope.IsSpinnerVisible = false;

        ////Initailize User Basic info for edit
        //$scope.profileInfo = {
        //    FirstName: "'@Model.user.FirstName'",
        //    LastName: "'@Model.user.LastName'",
        //    MobNumber: '@Model.user.MobilePhone',
        //    Email: '@Model.user.Email',
        //    Address: '@Model.user.AddressLine1',
        //    zip: '@Model.user.Zip',
        //    City: '@Model.user.City',
        //    State: '@Model.user.State',
        //}
        $scope.InitailizeUserProfile();
      }
    $scope.InitailizeUserProfile = function ()
    {
        UserProfileService.InitializeUserProfile($scope.InitailizeUserProfileCompleted, $scope.InitailizeUserProfileError)
    }
    $scope.InitailizeUserProfileCompleted = function (response, status) {
        $scope.profileInfo = response.UserProfile;
        $scope.StateList = response.StateList;
      
    }
    $scope.InitailizeUserProfileError = function (response, status) {
        // window.location = "/index.html";
    }

    $scope.checkHeadCompanyExist = function (company) {
        if(company)
        {
            return true;
        }
        return false;
    }
  
    $scope.UserData = { UserProfile: null }
    $scope.updateProfileInfo = function () {
        if ($scope.frmProfileInfo.$valid) {
            $scope.UserData.UserProfile = $scope.profileInfo;
            UserProfileService.updateUserProfile($scope.UserData, $scope.updateUserProfileCompleted, $scope.updateUserProfileError)
        }
    }
   
    $scope.updateUserProfileCompleted = function (response, status) {
        $scope.profileInfo = response.UserProfile;
        $scope.IsErrorExist = true;
        $scope.errorMessage = response.Error;
    }
    $scope.updateUserProfileError = function (response, status) {
        // window.location = "/index.html";
    }
    $scope.getUploadedFile = function () {
        var fd = new FormData();
        fd.append('uploadFile', $scope.imagefile);
      
        fd.append('UserId', $scope.userID);
        return fd;
    };
    //UPLOAD Customer image
    $scope.saveImage = function () {
        if ($scope.uploadImageForm.$valid) {
            var PostData = $scope.getUploadedFile();
         
            UserProfileService.DocUpload(PostData, $scope.saveDocumentCompleted, $scope.saveDocumentError)
        }
    };
    $scope.saveDocumentCompleted = function (response, status) {
     
        $scope.profileImage = response.RelativePath;
        $scope.IsErrorExist = true;
        $scope.errorMessage = response.errorList;
    }
    $scope.saveDocumentError = function (response, status) {
        // window.location = "/index.html";
    }

    //Update Password
    $scope.FormData = null;
    $scope.IsErrorExist = false;
    $scope.updatePassword = function () {
        if ($scope.frmUpdatePassword.$valid) {
            var PostData = $scope.FormData;
             UserProfileService.updatePassword(PostData, $scope.updatePasswordCompleted, $scope.updatePasswordError)
        }
    };
    $scope.updatePasswordCompleted = function (response, status) {
        $scope.FormData = null;
        $scope.IsErrorExist = true;
        $scope.errorMessage = response;
    }
    $scope.updatePasswordError = function (response, status) {
        // window.location = "/index.html";
    }
  
}]);//end controller
