﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CSS
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
            //);
            routes.MapRoute(
                  name: "faq",
                  url: "faq/{*.}",
                  defaults: new { controller = "Home", action = "faq", id = UrlParameter.Optional }
              );
            routes.MapRoute(
                 name: "newsEvent",
                 url: "iada-news-and-events/{*.}",
                 defaults: new { controller = "Home", action = "newsEvent", id = UrlParameter.Optional }
             );
            routes.MapRoute(
               name: "Contact",
               url: "contact/{*.}",
               defaults: new { controller = "Home", action = "contact", id = UrlParameter.Optional }
           );

            routes.MapRoute(
           name: "helpSaveLife",
           url: "help-iada-save-a-life/{*.}",
           defaults: new { controller = "Home", action = "helpSaveLife", id = UrlParameter.Optional }
            );
            routes.MapRoute(
           name: "newIADALogo",
           url: "introducing-the-new-iada-logo/{*.}",
           defaults: new { controller = "Home", action = "newIADALogo", id = UrlParameter.Optional }
            );
            routes.MapRoute(
           name: "vehicleRepairConf",
           url: "the-47th-iada-vehicle-repair-conference-was-a-huge-success/{*.}",
           defaults: new { controller = "Home", action = "vehicleRepairConf", id = UrlParameter.Optional }
            );

            routes.MapRoute(
             name: "Default",
             url: "{controller}/{action}/{id}",
             defaults: new { controller = "Home", action = "Home", id = UrlParameter.Optional }
                );
        }
    }
}