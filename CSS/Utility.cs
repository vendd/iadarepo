﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.IO;
using System.Text;
using System.Configuration;
using System.Drawing;
using System.Web.Mvc;
using System.Net;
using BLL;
using BLL.Models;
using BLL.ViewModels;
using System.Xml;
using Winnovative.WnvHtmlConvert;
using System.Data;
using System.Data.Objects;
//using CSS.SymbilityService;
using System.Web.UI;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Web.SessionState;
using Microsoft.Reporting.WebForms;
using CSS.Controllers;

namespace CSS
{
    public class Utility
    {
        public static BLL.ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();



        /// <summary>
        /// The method create a Base64 encoded string from a normal string.
        /// </summary>
        /// <param name="toEncode">The String containing the characters to encode.</param>
        /// <returns>The Base64 encoded string.</returns>
        public static string EncodeTo64(string toEncode)
        {

            byte[] toEncodeAsBytes

                  = System.Text.Encoding.Unicode.GetBytes(toEncode);

            string returnValue

                  = System.Convert.ToBase64String(toEncodeAsBytes);

            return returnValue;

        }
        /// <summary>
        /// The method to Decode your Base64 strings.
        /// </summary>
        /// <param name="encodedData">The String containing the characters to decode.</param>
        /// <returns>A String containing the results of decoding the specified sequence of bytes.</returns>
        public static string DecodeFrom64(string encodedData)
        {

            byte[] encodedDataAsBytes

                = System.Convert.FromBase64String(encodedData);

            string returnValue =

               System.Text.Encoding.Unicode.GetString(encodedDataAsBytes);

            return returnValue;

        }
        public static bool sendEmail(string to, string from, string subject, string body)
        {
            List<string> toList = new List<string>();
            toList.Add(to);
            bool successFlag = sendEmail(toList, from, subject, body);
            return successFlag;
        }
        public static bool sendEmail(string to, string from, string replyTo, string subject, string body)
        {
            List<string> toList = new List<string>();
            toList.Add(to);
            bool successFlag = sendEmail(toList, from, replyTo, subject, body);
            return successFlag;
        }
        public static bool sendEmail(string to, string from, string replyTo, string subject, string body,Attachment attachment)
        {
            List<string> toList = new List<string>();
            toList.Add(to);
            bool successFlag = sendEmail(toList, from, replyTo, subject, body, attachment);
            return successFlag;
        }
        public static bool sendEmail(List<string> to, string from, string subject, string body)
        {
            return sendEmail(to, from, null, subject, body);
        }
        public static bool sendEmail(List<string> to, string from, string replyTo, string subject, string body)
        {
            if (String.IsNullOrEmpty(replyTo))
            {
                replyTo = ConfigurationManager.AppSettings["SupportEmailAddress"].ToString();
            }
            bool successFlag = false;
            try
            {
                MailMessage mail = new MailMessage();
                mail.BodyEncoding = Encoding.Default;
                mail.IsBodyHtml = true;
                if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["EmailToOverride"].ToString()))
                {
                    if (to.Count > 0)
                    {
                        foreach (string emailAddress in to)
                        {
                            mail.To.Add(emailAddress);
                        }
                    }
                }
                else
                {
                    mail.To.Add(ConfigurationManager.AppSettings["EmailToOverride"].ToString());
                }

                mail.ReplyTo = new MailAddress(replyTo);
                mail.From = new MailAddress(ConfigurationManager.AppSettings["SupportEmailAddress"].ToString());
                mail.Subject = subject;
                mail.Body = body;


                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["SMTPHost"].ToString();
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"].ToString());
                NetworkCredential basicAuthenticationInfo = new NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"].ToString(), ConfigurationManager.AppSettings["SMTPPassword"].ToString());
                // smtp.EnableSsl = true;

                smtp.UseDefaultCredentials = false;
                smtp.Credentials = basicAuthenticationInfo;


                // smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"].ToString(), ConfigurationManager.AppSettings["SMTPPassword"].ToString());
                //   smtp.ServicePoint.MaxIdleTime = 1; 
                smtp.Send(mail);
                successFlag = true;
            }
            catch (Exception ex)
            {
                Utility.LogException("Method sendEmail", ex, "An error occured while sending mail to SP:" + subject+" "+body);
                //css.usp_ExceptionLogInsert("Error:" + subject, ex.Message, (ex.InnerException != null ? ex.InnerException.Message : ""), ex.StackTrace, body);
            }

            return successFlag;
        }
        public static bool sendEmail(List<string> to, string from, string replyTo, string subject, string body, Attachment attachment)
        {
            if (String.IsNullOrEmpty(replyTo))
            {
                replyTo = ConfigurationManager.AppSettings["SupportEmailAddress"].ToString();
            }
            bool successFlag = false;
            try
            {
                MailMessage mail = new MailMessage();
                mail.BodyEncoding = Encoding.Default;
                mail.IsBodyHtml = true;
                if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["EmailToOverride"].ToString()))
                {
                    if (to.Count > 0)
                    {
                        foreach (string emailAddress in to)
                        {
                            mail.To.Add(emailAddress);
                        }
                    }
                }
                else
                {
                    mail.To.Add(ConfigurationManager.AppSettings["EmailToOverride"].ToString());
                }

                if(!String.IsNullOrEmpty(MvcApplication.TestEmailAddress))
                {
                   // mail.CC.Add(new MailAddress(MvcApplication.TestEmailAddress));
                }
                mail.ReplyTo = new MailAddress(replyTo);
                mail.From = new MailAddress(ConfigurationManager.AppSettings["SupportEmailAddress"].ToString());
                mail.Subject = subject;
                mail.Body = body;
                if (attachment != null)
                {
                    mail.Attachments.Add(attachment);
                }

                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["SMTPHost"].ToString();
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"].ToString());
                NetworkCredential basicAuthenticationInfo = new NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"].ToString(), ConfigurationManager.AppSettings["SMTPPassword"].ToString());
               // smtp.EnableSsl = true;

                smtp.UseDefaultCredentials = false;
                smtp.Credentials = basicAuthenticationInfo;

               
               // smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"].ToString(), ConfigurationManager.AppSettings["SMTPPassword"].ToString());
             //   smtp.ServicePoint.MaxIdleTime = 1; 
                smtp.Send(mail);
                successFlag = true;
            }
            catch (Exception ex)
            {
            // css.usp_ExceptionLogInsert("Error:" + subject, ex.Message, (ex.InnerException != null ? ex.InnerException.Message : ""), ex.StackTrace, body);
                Utility.LogException("Method sendEmail list to", ex, "An error occured while sending mail to SP:" + subject + " " + body);
            }

            return successFlag;
        }
        public static bool sendEmail(string to, string from, string subject, string body, List<string> attachmentList)
        {
            List<string> toList = new List<string>();
            toList.Add(to);
            bool successFlag = sendEmail(toList, from, subject, body, attachmentList);
            return successFlag;
        }
        public static bool sendEmail(List<string> to, string from, string replyTo, string subject, string body, List<string> attachmentList)
        {
            //body = Regex.Replace(body, "\n", "<br />");
            MailMessage mail = new MailMessage();
            System.Net.Mail.Attachment attachment = null;
            SmtpClient smtp = new SmtpClient();
            if (String.IsNullOrEmpty(replyTo))
            {
                replyTo = ConfigurationManager.AppSettings["SupportEmailAddress"].ToString();
            }
            bool successFlag = false;
            try
            {
                // MailMessage mail = new MailMessage();
                mail.BodyEncoding = Encoding.Default;
                mail.IsBodyHtml = true;
                if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["EmailToOverride"].ToString()))
                {
                    if (to.Count > 0)
                    {
                        foreach (string emailAddress in to)
                        {
                            mail.To.Add(emailAddress);
                        }
                    }
                }
                else
                {
                    mail.To.Add(ConfigurationManager.AppSettings["EmailToOverride"].ToString());
                }
                mail.ReplyTo = new MailAddress(replyTo);
                mail.From = new MailAddress(ConfigurationManager.AppSettings["SupportEmailAddress"].ToString());
                mail.Subject = subject;
                mail.Body = body;


                foreach (string filePath in attachmentList)
                {
                    //  System.Net.Mail.Attachment attachment;
                    if ((new Uri(filePath)).IsFile == false)
                    {
                        //Is Remote file(Cloud Storate URL)
                        try
                        {
                            Stream fileStream = new WebClient().OpenRead(filePath);
                            string fileName = filePath.Substring(filePath.LastIndexOf("/") + 1, filePath.Length - 1 - filePath.LastIndexOf("/"));
                            attachment = new Attachment(fileStream, fileName);
                            mail.Attachments.Add(attachment);
                        }
                        catch (Exception e)
                        {
                        }
                    }
                    else
                    {
                        //Is Local File System File
                        if (System.IO.File.Exists(filePath))
                        {
                            attachment = new Attachment(filePath);
                            mail.Attachments.Add(attachment);
                        }
                    }

                }

                // SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["SMTPHost"].ToString();
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"].ToString());
                NetworkCredential basicAuthenticationInfo = new NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"].ToString(), ConfigurationManager.AppSettings["SMTPPassword"].ToString());
              //  smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = basicAuthenticationInfo;


                smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"].ToString(), ConfigurationManager.AppSettings["SMTPPassword"].ToString());
           //     smtp.ServicePoint.MaxIdleTime = 1; 
                smtp.Send(mail);
                //  smtp.Dispose();
                //   mail.Dispose();
                successFlag = true;
            }
            catch (SmtpException ex)
            {
                
                throw new TimeoutException();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                smtp.Dispose();
                mail.Dispose();
                attachment.Dispose();
            }
            return successFlag;
        }
        public static bool sendEmail(List<string> to, string from, string subject, string body, List<string> attachmentList)
        {
            return sendEmail(to, from, null, subject, body, attachmentList);
        }
        public static  System.Drawing.Image generateThumbnail(System.Drawing.Image originalImage)
        {
            float aspectRatio = (float)originalImage.Width / originalImage.Height;
            float height = 80;
            float newWidth = height * aspectRatio;
            System.Drawing.Image.GetThumbnailImageAbort myCallback = new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback);
            return originalImage.GetThumbnailImage((int)newWidth, (int)height, myCallback, IntPtr.Zero);
        }
        public static string getThumbnailFileName(string originalFilename)
        {
            string thumbnailFileName = originalFilename.Split('.')[0] + "_S." + originalFilename.Split('.')[1];
            return thumbnailFileName;
        }

        private static bool ThumbnailCallback() { return false; }
        public static string getDefaultDateFormat()
        {
            return System.Configuration.ConfigurationManager.AppSettings["DefaultDateFormat"].ToString();
        }
        public static string getDateInDefaultFormat(DateTime date)
        {

            return date.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultDateFormat"].ToString());
        }
        public static int getProfileProgressValue(BLL.User user)
        {
            ProfileProgressInfo profileProgressInfo;
            int percentageCompleted = getProfileProgressInfo(user, out profileProgressInfo);

            return percentageCompleted;
        }
        public static int getProfileProgressInfo(BLL.User user, out ProfileProgressInfo progressInfo)
        {
            progressInfo = new ProfileProgressInfo();
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            ServiceProviderDetail spDetail = null;
            spDetail = css.ServiceProviderDetails.Find(user.UserId);

            int totalScore = 0;
            int completedScore = 0;
            int percentageCompleted = 0;
            int type1Score = 2;
            int type2Score = 1;
            int type3Score = 15;

            #region Type 1 Field

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.FirstName))
            {
                completedScore += type1Score;
                progressInfo.HasFirstName = true;
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.LastName))
            {
                completedScore += type1Score;
                progressInfo.HasLastName = true;
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.UserName))
            {
                completedScore += type1Score;
                progressInfo.HasUserName = true;
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.Password))
            {
                completedScore += type1Score;
                progressInfo.HasPassword = true;
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.SSN))
            {
                completedScore += type1Score;
                progressInfo.HasSSN = true;
            }

            //Social Security Card
            totalScore += type1Score;
            if (user.ServiceProviderDocuments.Where(x => x.DTId == 1).ToList().Count != 0)
            {
                completedScore += type1Score;
                progressInfo.HasSSNDocument = true;

            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.DriversLicenceNumber))
            {
                completedScore += type1Score;
                progressInfo.HasDriversLicenseNumber = true;
            }

            //Drivers License
            totalScore += type1Score;
            if (user.ServiceProviderDocuments.Where(x => x.DTId == 3).ToList().Count != 0)
            {
                completedScore += type1Score;
                progressInfo.HasDriversLicenseDoc = true;
            }


            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.DriversLicenceState))
            {
                if (user.ServiceProviderDetail.DriversLicenceState != "0")
                {
                    completedScore += type1Score;
                    progressInfo.HasDriversLicenceState = true;
                }
            }

            totalScore += type1Score;
            if (user.ServiceProviderDetail.DOB != null)
            {
                completedScore += type1Score;
                progressInfo.HasDOB = true;
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.PlaceOfBirthState))
            {
                if (user.ServiceProviderDetail.PlaceOfBirthState != "0")
                {
                    completedScore += type1Score;
                    progressInfo.HasPlaceOfBirth = true;
                }
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.StreetAddress))
            {
                completedScore += type1Score;
                progressInfo.HasStreetAddress = true;
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.Zip))
            {
                completedScore += type1Score;
                progressInfo.HasZip = true;
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.State))
            {
                if (user.State != "0")
                {
                    completedScore += type1Score;
                    progressInfo.HasState = true;
                }
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.City))
            {
                if (user.City != "0") //probably would be shifted to a dropdown list
                {
                    completedScore += type1Score;
                    progressInfo.HasCity = true;
                }
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.MobilePhone))
            {
                completedScore += type1Score;
                progressInfo.HasMobileNumber = true;
            }

            totalScore += type1Score;
            if (user.ServiceProviderAddresses.Count != 0)
            {
                ServiceProviderAddress spa = user.ServiceProviderAddresses.FirstOrDefault();
                if (spa != null)
                {
                    if (spa.DateFrom.HasValue && spa.DateTo.HasValue && !String.IsNullOrEmpty(spa.Address) && !String.IsNullOrEmpty(spa.State) && !String.IsNullOrEmpty(spa.City))
                    {
                        if (spa.State != "0")
                        {
                            completedScore += type1Score;
                            progressInfo.HasServiceProviderAddress = true;
                        }
                    }
                }

            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.PrimaryLanguage))
            {
                completedScore += type1Score;
                progressInfo.HasPrimaryLanguage = true;
            }

            string[] spseType1List = { "Xactimate", "Symbility" };
            foreach (string software in spseType1List)
            {
                totalScore += type1Score;
                IEnumerable<ServiceProviderSoftwareExperience> spseList = user.ServiceProviderSoftwareExperiences.Where(x => x.Software == software);
                if (spseList != null)
                {
                    ServiceProviderSoftwareExperience spse = spseList.FirstOrDefault();
                    if (spse != null)
                    {
                        if (spse.YearsOfExperience != null) //keep incase the field is made nullable in future
                        {
                            if (spse.YearsOfExperience > 0)
                            {
                                completedScore += type1Score;
                                progressInfo.HasSoftwareExperience = true;
                            }
                        }
                    }
                }
            }

            List<BLL.ClaimType> claimTypeExpList = css.ClaimTypes.ToList();
            foreach (BLL.ClaimType claimType in claimTypeExpList)
            {
                totalScore += type1Score;
                IEnumerable<ServiceProviderExperience> speList = user.ServiceProviderExperiences.Where(x => x.ClaimTypeId == claimType.ClaimTypeId);
                if (speList != null)
                {
                    ServiceProviderExperience spe = speList.FirstOrDefault();
                    if (spe != null)
                    {
                        if (spe.YearsOfExperience != null) //keep incase the field is made nullable in future
                        {
                            if (spe.YearsOfExperience > 0)
                            {
                                completedScore += type1Score;
                                progressInfo.HasClaimTypeExperience = true;
                            }
                        }
                    }
                }
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.CapabilityToClimb))
            {
                if (user.ServiceProviderDetail.CapabilityToClimb != "0")
                {
                    completedScore += type1Score;
                    progressInfo.HasCapabilityToClimb = true;
                }
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.CapabilityToClimbSteep))
            {
                if (user.ServiceProviderDetail.CapabilityToClimbSteep != "0")
                {
                    completedScore += type1Score;
                    progressInfo.HasCapabilityToClimbSteep = true;
                }
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.AutoCarrier))
            {
                completedScore += type1Score;
                progressInfo.HasAutoCarrier = true;
            }

            totalScore += type1Score;
            if (user.ServiceProviderDetail.AutoLimit.HasValue)
            {
                if (user.ServiceProviderDetail.AutoLimit > 0) //0 is the default value being populated, hence avoid counting it. CHECK if null can be the default value instead of 0
                {
                    completedScore += type1Score;
                    progressInfo.HasAutoLimit = true;
                }
            }

            totalScore += type1Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.AutoPolicyNumber))
            {
                completedScore += type1Score;
                progressInfo.HasAutoPolicyNumber = true;
            }

            totalScore += type1Score;
            if (user.ServiceProviderDetail.AutoExpiry.HasValue)
            {
                completedScore += type1Score;
                progressInfo.HasAutoExpiry = true;
            }

            totalScore += type1Score;
            if (user.ServiceProviderLicenses.Count != 0)
            {
                ServiceProviderLicens spl = user.ServiceProviderLicenses.FirstOrDefault();
                if (spl != null)
                {
                    if (!String.IsNullOrEmpty(spl.Type) && !String.IsNullOrEmpty(spl.State) && !String.IsNullOrEmpty(spl.LicenseNumber) && spl.ExpirationDate.HasValue)
                    {
                        if (spl.State != "0") //Unselected. Do not merge with the above "if" statement as if the string is null an exception would be raised
                        {
                            completedScore += type1Score;
                            progressInfo.HasServiceProviderLicenses = true;
                        }
                    }
                }
            }

            //Background Authorization Form
            totalScore += type1Score;
            if (user.ServiceProviderDocuments.Where(x => x.DTId == 10).ToList().Count != 0)
            {
                completedScore += type1Score;
                progressInfo.HasBGAuthorizationFormDoc = true;
            }

            ////Direct Deposit Form
            //totalScore += type1Score;
            //if (user.ServiceProviderDocuments.Where(x => x.DTId == 11).ToList().Count != 0)
            //{
            //    completedScore += type1Score;
            //}

            //Direct Deposit Form
            totalScore += type1Score;
            if (css.ServiceProviderDocuments.Where(x => (x.DTId == 11)).ToList().Count != 0)
            {
                ServiceProviderDocument spDoc = css.ServiceProviderDocuments.Where(x => (x.DTId == 11)).First();
                if (spDetail != null && spDoc != null)
                {
                    if (isSertifiDocumentSigned(spDetail.SertifiFileId, spDoc.SertifiDocumentId, spDoc.SPDId))
                    {
                        completedScore += type1Score;
                        progressInfo.HasDDFDoc = true;
                    }
                }
            }
            #endregion

            #region Type 2 Field
            //Service Provider Photo
            totalScore += type2Score;
            if (user.ServiceProviderDocuments.Where(x => x.DTId == 7).ToList().Count != 0)
            {
                completedScore += type2Score;
                progressInfo.HasSPPhoto = true;
            }

            totalScore += type2Score;
            if (!String.IsNullOrEmpty(user.HomePhone))
            {
                completedScore += type2Score;
                progressInfo.HasHomePhone = true;
            }

            totalScore += type2Score;
            if (!String.IsNullOrEmpty(user.Email))
            {
                completedScore += type2Score;
                progressInfo.HasEmail = true;
            }

            totalScore += type2Score;
            if (!String.IsNullOrEmpty(user.Facebook))
            {
                completedScore += type2Score;
                progressInfo.HasFacebook = true;
            }

            totalScore += type2Score;
            if (!String.IsNullOrEmpty(user.Twitter))
            {
                completedScore += type2Score;
                progressInfo.HasTwitter = true;
            }

            totalScore += type2Score;
            if (!String.IsNullOrEmpty(user.GooglePlus))
            {
                completedScore += type2Score;
                progressInfo.HasGooglePlus = true;
            }

            totalScore += type2Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.OtherLanguages))
            {
                completedScore += type2Score;
                progressInfo.HasOtherLanguages = true;
            }

            totalScore += type2Score;
            if (user.ServiceProviderDetail.ShirtSizeTypeId.HasValue)
            {
                if (user.ServiceProviderDetail.ShirtSizeTypeId != 0)//Unselected
                {
                    completedScore += type2Score;
                    progressInfo.HasShirtSize = true;
                }
            }

            totalScore += type2Score;
            if (user.ServiceProviderReferences.Count == 2)
            {
                bool hasSufficientDataFlag = true;
                foreach (ServiceProviderReference spr in user.ServiceProviderReferences)
                {
                    if (spr != null)
                    {
                        if (String.IsNullOrEmpty(spr.Name) || String.IsNullOrEmpty(spr.Company))
                        {
                            hasSufficientDataFlag = false;
                            break;
                        }
                    }
                }

                if (hasSufficientDataFlag)
                {
                    completedScore += type2Score;
                    progressInfo.HasReference = true;

                }
            }

            string[] spEducationList = { "College", "GraduateStudies", "Others" };
            foreach (string education in spEducationList)
            {
                totalScore += type2Score;
                IEnumerable<ServiceProviderEducation> speList = user.ServiceProviderEducations.Where(x => x.Education == education);
                ServiceProviderEducation spe = speList.FirstOrDefault();
                if (spe != null)
                {
                    if (spe.Date.HasValue && !String.IsNullOrEmpty(spe.Degree))
                    {
                        completedScore += type2Score;
                        progressInfo.HasEducation = true;
                    }
                }
            }



            string[] spseType2List = { "MSB", "Simsol", "Other" };
            foreach (string software in spseType2List)
            {
                totalScore += type2Score;
                IEnumerable<ServiceProviderSoftwareExperience> spseList = user.ServiceProviderSoftwareExperiences.Where(x => x.Software == software);
                if (spseList != null)
                {
                    ServiceProviderSoftwareExperience spse = spseList.FirstOrDefault();
                    if (spse != null)
                    {
                        if (spse.YearsOfExperience != null) //keep incase the field is made nullable in future
                        {
                            if (spse.YearsOfExperience > 0)
                            {
                                completedScore += type2Score;
                                progressInfo.HasSoftwareExperience = true;
                            }
                        }
                    }
                }
            }


            totalScore += type2Score;
            if (user.ServiceProviderDesignations.Count != 0)
            {
                ServiceProviderDesignation spd = user.ServiceProviderDesignations.FirstOrDefault();
                if (spd != null)
                {
                    if (spd.Date.HasValue && !String.IsNullOrEmpty(spd.Designation))
                    {
                        completedScore += type2Score;
                        progressInfo.HasSPDesignations = true;
                    }
                }
            }

            totalScore += type2Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.TypeOfClaimsPref))
            {
                completedScore += type2Score;
                progressInfo.HasTypeOfClaimPref = true;
            }

            totalScore += type2Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.TypeOfClaimsPrefOthers))
            {
                completedScore += type2Score;
                progressInfo.HasTypeOfClaimPrefOthers = true;
            }

            totalScore += type2Score;
            if (user.ServiceProviderDetail.ConsiderWorkingInCat.HasValue)
            {
                completedScore += type2Score;
                progressInfo.HasConsiderWorkingInCat = true;
            }

            totalScore += type2Score;
            if (user.ServiceProviderDetail.HasExperienceWorkingInCat.HasValue)
            {
                completedScore += type2Score;
                progressInfo.HasExpWorkingInCat = true;
            }

            totalScore += type2Score;
            if (user.ServiceProviderDetail.ExperienceWorkingInCatWhen.HasValue)
            {
                completedScore += type2Score;
                progressInfo.HasExpWorkingInCatWhen = true;
            }


            totalScore += type2Score;
            if (!String.IsNullOrEmpty(user.ServiceProviderDetail.CatCompany))
            {
                completedScore += type2Score;
                progressInfo.HasCatCompany = true;
            }
            #endregion

            #region Type 3


            //Agreement
            totalScore += type3Score;

            if (css.ServiceProviderDocuments.Where(x => ((x.DTId == 8) || (x.DTId == 12)) && (x.UserId == user.UserId)).ToList().Count != 0)
            {
                ServiceProviderDocument spDoc = css.ServiceProviderDocuments.Where(x => ((x.DTId == 8) || (x.DTId == 12)) && (x.UserId == user.UserId)).First();
                if (spDetail != null && spDoc != null)
                {
                    if (isSertifiDocumentSigned(spDetail.SertifiFileId, spDoc.SertifiDocumentId, spDoc.SPDId))
                    {
                        completedScore += type3Score;
                        progressInfo.HasAgreementDoc = true;
                    }
                }

            }

            //W9
            totalScore += type3Score;
            if (css.ServiceProviderDocuments.Where(x => (x.DTId == 9) && (x.UserId == user.UserId)).ToList().Count != 0)
            {
                ServiceProviderDocument spDoc = css.ServiceProviderDocuments.Where(x => (x.DTId == 9) && (x.UserId == user.UserId)).First();
                if (spDetail != null && spDoc != null)
                {
                    if (isSertifiDocumentSigned(spDetail.SertifiFileId, spDoc.SertifiDocumentId, spDoc.SPDId))
                    {
                        completedScore += type3Score;
                        progressInfo.HasW9Doc = true;
                    }
                }
            }

            #endregion
            #region Calculate Percentage Completed
            percentageCompleted = (int)(((float)completedScore / totalScore) * 100);
            #endregion

            return percentageCompleted;
        }

        public static string getESigningDocumentLink(Int64 userId, int documentTypeID)
        {
            string valueToReturn = "";
            try
            {
                ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
                DocumentType docType = css.DocumentTypes.Find(documentTypeID);
                BLL.User user = css.Users.Find(userId);
                string _APICode = ConfigurationManager.AppSettings["SertifiAPICode"].ToString();
                string senderEmail = ConfigurationManager.AppSettings["SertifiSenderEmailAddress"].ToString();
                string senderName = ConfigurationManager.AppSettings["SertifiSenderName"].ToString(); ;
                string filename = user.UserName;
                string signers = user.Email;
                string fileID = "";
                string documentID = "";
                SertifiAPI.Gateway sertifiAPIRequest = new SertifiAPI.Gateway();
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                #region Identify FileId

                ServiceProviderDetail spDetails = css.ServiceProviderDetails.Find(userId);
                if (!String.IsNullOrEmpty(spDetails.SertifiFileId))
                {
                    //A FileId already exists for this Service Provider, reuse it
                    fileID = spDetails.SertifiFileId;

                }
                else
                {
                    //create a request by calling the sertifi API
                    fileID = sertifiAPIRequest.CreateSignatureRequest(_APICode, senderEmail, senderName, filename, signers, "", "", "", "", "", "", "", "", "", "");
                    spDetails.SertifiFileId = fileID;
                    css.SertifiFileIdUpdate(userId, fileID);
                    //css.Entry(spDetails).State = System.Data.EntityState.Modified;
                    //css.SaveChanges();
                }
                #endregion

                #region Identify DocumentId
                IQueryable<ServiceProviderDocument> spDocSearchResult = css.ServiceProviderDocuments.Where(x => (x.UserId == userId) && (x.DTId == documentTypeID));
                ServiceProviderDocument spDoc = null;
                if (spDocSearchResult.ToList().Count > 0)
                {
                    spDoc = spDocSearchResult.First();
                }
                else
                {
                    spDoc = new ServiceProviderDocument();
                    spDoc.UserId = userId;
                    spDoc.DTId = documentTypeID;
                    spDoc.Title = docType.DocumentDesc;
                    spDoc.Path = "";
                    spDoc.UploadedDate = DateTime.Now;
                    //css.ServiceProviderDocuments.Add(spDoc);
                    //css.SaveChanges();
                    ObjectParameter outSPDId = new ObjectParameter("SPDId", DbType.Int64);
                    css.ServiceProviderDocumentsInsert(outSPDId, spDoc.DTId, spDoc.UserId, spDoc.Title, spDoc.Path, spDoc.UploadedDate, null, false);
                    spDoc.SPDId = Convert.ToInt64(outSPDId.Value);
                }

                if (!String.IsNullOrEmpty(spDoc.SertifiDocumentId))
                {
                    documentID = spDoc.SertifiDocumentId;
                }
                else
                {
                    if (documentTypeID == 8)
                    {
                        #region Service Provider Agreement (Approved)

                        string sertifiPDFId = ConfigurationManager.AppSettings["SertifiPDFIdSPAgreementApproved"].ToString();
                        string name = user.FirstName + " " + user.LastName;
                        string fullname = user.LastName + " " + user.FirstName + " " + user.MiddleInitial;

                        string presentAddress = (!String.IsNullOrEmpty(user.AddressPO) ? user.AddressPO + ", " : "") + (!String.IsNullOrEmpty(user.AddressLine1) ? user.AddressLine1 + ", " : "") + (!String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress + ", " : "");
                        string cityStateZip = "";
                        DateTime cstDateTime = css.usp_GetLocalDateTime().First().Value;
                        string currentDate = cstDateTime.ToString("MM/dd/yyyy");
                        //string spinitial = user.FirstName.Substring(0,1) +"" + user.LastName.Substring(0,1);
                        cityStateZip += user.City;

                        if (!String.IsNullOrEmpty(user.State))
                        {
                            StateProvince objStateProvince = null;
                            if (css.StateProvinces.Where(p => p.StateProvinceCode == user.State).ToList().Count > 0)
                            {
                                objStateProvince = css.StateProvinces.Where(p => p.StateProvinceCode == user.State).First();
                            }
                            if (objStateProvince != null)
                            {
                                cityStateZip += " ," + objStateProvince.Name;

                            }
                        }
                        cityStateZip += !String.IsNullOrEmpty(user.Zip) ? " ," + user.Zip : "";
                        string fullAddress = (!String.IsNullOrEmpty(user.AddressPO) ? user.AddressPO + ", " : "") + (!String.IsNullOrEmpty(user.AddressLine1) ? user.AddressLine1 + ", " : "") + (!String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress + ", " : "") + cityStateZip;
                        string agreementDate = cstDateTime.ToString("dddd, MMMM dd, yyyy ");
                        string prepopulateXMLData = "";
                        prepopulateXMLData += "<?xml version=\"1.0\" encoding=\"us-ascii\"?>";
                        prepopulateXMLData += "<record>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].SertifiDate_1[0]\" Text=\"" + agreementDate + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].Name[0]\" Text=\"" + name + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page5[0].SPFullName[0]\" Text=\"" + name + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page5[0].SPAddress[0]\" Text=\"" + fullAddress + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page5[0].SPSignDate[0]\" Text=\"" + currentDate + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].FullName[0]\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].Date[0]\" Text=\"" + currentDate + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].SSN[0]\" Text=\"" + user.SSN + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].DOB[0]\" Text=\"" + (user.ServiceProviderDetail.DOB.HasValue ? user.ServiceProviderDetail.DOB.Value.ToString("MM/dd/yyyy") : "") + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].PresentAddress[0]\" Text=\"" + presentAddress + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].CityStateZip[0]\" Text=\"" + cityStateZip + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].DriverLicense[0]\" Text=\"" + user.ServiceProviderDetail.DriversLicenceNumber + "\"/>";
                        prepopulateXMLData += "</record>";
                        documentID = sertifiAPIRequest.AddPrepopulatedPdfToRequest(_APICode, fileID, sertifiPDFId, docType.DocumentDesc, prepopulateXMLData);

                        spDoc.SertifiDocumentId = documentID;
                        css.SertifiDocumentIdUpdate(spDoc.SPDId, spDoc.SertifiDocumentId);
                        //css.Entry(spDoc).State = System.Data.EntityState.Modified;
                        //css.SaveChanges();

                        #endregion
                    }
                    else if (documentTypeID == 12)
                    {
                        #region Service Provider Agreement

                        string sertifiPDFId = ConfigurationManager.AppSettings["SertifiPDFIdSPAgreement"].ToString();
                        string name = user.FirstName + " " + user.LastName;
                        string fullname = user.LastName + " " + user.FirstName + " " + user.MiddleInitial;

                        string presentAddress = (!String.IsNullOrEmpty(user.AddressPO) ? user.AddressPO + ", " : "") + (!String.IsNullOrEmpty(user.AddressLine1) ? user.AddressLine1 + ", " : "") + (!String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress + ", " : "");
                        string cityStateZip = "";
                        DateTime cstDateTime = css.usp_GetLocalDateTime().First().Value;
                        string currentDate = cstDateTime.ToString("MM/dd/yyyy");
                        //string spinitial = user.FirstName.Substring(0, 1) + "" + user.LastName.Substring(0, 1);
                        cityStateZip += user.City;

                        if (!String.IsNullOrEmpty(user.State))
                        {
                            StateProvince objStateProvince = null;
                            if (css.StateProvinces.Where(p => p.StateProvinceCode == user.State).ToList().Count > 0)
                            {
                                objStateProvince = css.StateProvinces.Where(p => p.StateProvinceCode == user.State).First();
                            }
                            if (objStateProvince != null)
                            {
                                cityStateZip += " ," + objStateProvince.Name;

                            }
                        }
                        cityStateZip += !String.IsNullOrEmpty(user.Zip) ? " ," + user.Zip : "";
                        string fullAddress = (!String.IsNullOrEmpty(user.AddressPO) ? user.AddressPO + ", " : "") + (!String.IsNullOrEmpty(user.AddressLine1) ? user.AddressLine1 + ", " : "") + (!String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress + ", " : "") + cityStateZip;
                        string agreementDate = cstDateTime.ToString("dddd, MMMM dd, yyyy ");
                        string prepopulateXMLData = "";
                        prepopulateXMLData += "<?xml version=\"1.0\" encoding=\"us-ascii\"?>";
                        prepopulateXMLData += "<record>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].SertifiDate_1[0]\" Text=\"" + agreementDate + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].Name[0]\" Text=\"" + name + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page5[0].SPFullName[0]\" Text=\"" + name + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page5[0].SPAddress[0]\" Text=\"" + fullAddress + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page5[0].SPSignDate[0]\" Text=\"" + currentDate + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].FullName[0]\" Text=\"" + fullname + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].Date[0]\" Text=\"" + currentDate + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].SSN[0]\" Text=\"" + user.SSN + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].DOB[0]\" Text=\"" + (user.ServiceProviderDetail.DOB.HasValue ? user.ServiceProviderDetail.DOB.Value.ToString("MM/dd/yyyy") : "") + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].PresentAddress[0]\" Text=\"" + presentAddress + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].CityStateZip[0]\" Text=\"" + cityStateZip + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page11[0].DriverLicense[0]\" Text=\"" + user.ServiceProviderDetail.DriversLicenceNumber + "\"/>";
                        prepopulateXMLData += "</record>";
                        documentID = sertifiAPIRequest.AddPrepopulatedPdfToRequest(_APICode, fileID, sertifiPDFId, docType.DocumentDesc, prepopulateXMLData);

                        spDoc.SertifiDocumentId = documentID;
                        css.SertifiDocumentIdUpdate(spDoc.SPDId, spDoc.SertifiDocumentId);
                        //css.Entry(spDoc).State = System.Data.EntityState.Modified;
                        //css.SaveChanges();


                        #endregion
                    }
                    else if (documentTypeID == 9)
                    {
                        #region W9

                        string sertifiPDFId = ConfigurationManager.AppSettings["SertifiPDFIdW9"].ToString();
                        string printedname = user.FirstName + " " + user.LastName;
                        string presentAddress = (!String.IsNullOrEmpty(user.AddressPO) ? user.AddressPO + ", " : "") + (!String.IsNullOrEmpty(user.AddressLine1) ? user.AddressLine1 + ", " : "") + (!String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress + ", " : "");
                        string cityStateZip = "";
                        string FederalTaxClassification = "";
                        string individual = "";
                        string ccorporation = "";
                        string scorporation = "";
                        string partnership = "";
                        string trust = "";
                        string llc = "";
                        string corp = "";

                        cityStateZip += user.City;


                        if (!String.IsNullOrEmpty(user.State))
                        {
                            StateProvince objStateProvince = null;
                            if (css.StateProvinces.Where(p => p.StateProvinceCode == user.State).ToList().Count > 0)
                            {
                                objStateProvince = css.StateProvinces.Where(p => p.StateProvinceCode == user.State).First();
                            }
                            if (objStateProvince != null)
                            {
                                cityStateZip += " ," + objStateProvince.Name;
                            }
                        }
                        cityStateZip += !String.IsNullOrEmpty(user.Zip) ? " ," + user.Zip : "";

                        if (user.ServiceProviderDetail.FederalTaxClassification != null)
                        {
                            if (user.ServiceProviderDetail.FederalTaxClassification != "")
                            {
                                FederalTaxClassification = user.ServiceProviderDetail.FederalTaxClassification.Trim();
                                switch (FederalTaxClassification)
                                {
                                    case "I":
                                        individual = "1";
                                        break;
                                    case "C":
                                        ccorporation = "2";
                                        break;
                                    case "S":
                                        scorporation = "3";
                                        break;
                                    case "P":
                                        partnership = "4";
                                        break;
                                    case "T":
                                        trust = "5";
                                        break;
                                    case "L":
                                        llc = "6";
                                        if (user.ServiceProviderDetail.LLCTaxClassification == "C")
                                        {
                                            //corp = "C Corporation";
                                            corp = "C";
                                        }
                                        else if (user.ServiceProviderDetail.LLCTaxClassification == "S")
                                        {
                                            //corp = "S Corporation";
                                            corp = "S";
                                        }
                                        else if (user.ServiceProviderDetail.LLCTaxClassification == "P")
                                        {
                                            //corp = "Partnership";
                                            corp = "P";
                                        }
                                        break;

                                }
                            }
                        }






                        string ssn = "";
                        string ssn1 = "";
                        string ssn2 = "";
                        string ssn3 = "";
                        try
                        {

                            if (!String.IsNullOrEmpty(user.SSN))
                            {
                                ssn = user.SSN;
                                ssn1 = ssn.Substring(0, 3);
                                ssn2 = ssn.Substring(3, 2);
                                ssn3 = ssn.Substring(5, 4);
                            }
                        }
                        catch (Exception ex)
                        {
                        }

                        string taxid1 = "", taxid2 = "";
                        string taxid = "";
                        int taxidlength = 0;
                        if (!String.IsNullOrEmpty(user.ServiceProviderDetail.TaxID))
                        {
                            taxidlength = user.ServiceProviderDetail.TaxID.Length;


                            if (taxidlength == 9)
                            {
                                taxid = user.ServiceProviderDetail.TaxID;
                                taxid1 = taxid.Substring(0, 2);
                                taxid2 = taxid.Substring(2, 7);
                            }
                            if (taxidlength == 10)
                            {
                                taxid = user.ServiceProviderDetail.TaxID;
                                taxid1 = taxid.Substring(0, 2);
                                taxid2 = taxid.Substring(3, taxid.Length - 3);
                            }
                        }

                        string prepopulateXMLData = "";
                        prepopulateXMLData += "<?xml version=\"1.0\" encoding=\"us-ascii\"?>";
                        prepopulateXMLData += "<record>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].f1_01_0_[0]\" Text=\"" + printedname + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].f1_02_0_[0]\" Text=\"" + WebUtility.HtmlEncode(user.ServiceProviderDetail.DBA_LLC_Firm_Corp) + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].Address[0].f1_04_0_[0]\" Text=\"" + presentAddress + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].Address[0].f1_05_0_[0]\" Text=\"" + cityStateZip + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].FedClassification[0].c1_01[0]\" Text=\"" + individual + "\" />";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].FedClassification[0].c1_01[1]\" Text=\"" + ccorporation + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].FedClassification[0].c1_01[2]\" Text=\"" + scorporation + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].FedClassification[0].c1_01[3]\" Text=\"" + partnership + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].FedClassification[0].c1_01[4]\" Text=\"" + trust + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].FedClassification[0].c1_01[5]\" Text=\"" + llc + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].FedClassification[0].f1_18_0_[0]\" Text=\"" + corp + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].social[0].f1_07[0]\" Text=\"" + ssn1 + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].social[0].f1_08[0]\" Text=\"" + ssn2 + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].social[0].f1_09[0]\" Text=\"" + ssn3 + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].Employeridentifi[0].f1_10[0]\" Text=\"" + taxid1 + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].Employeridentifi[0].f1_11[0]\" Text=\"" + taxid2 + "\"/>";
                        prepopulateXMLData += "<field Name=\"SignatureDate\" Text=\"" + DateTime.Now.ToString("MM/dd/yyyy") + "\"/>";
                        prepopulateXMLData += "</record>";
                        documentID = sertifiAPIRequest.AddPrepopulatedPdfToRequest(_APICode, fileID, sertifiPDFId, docType.DocumentDesc, prepopulateXMLData);

                        spDoc.SertifiDocumentId = documentID;
                        css.SertifiDocumentIdUpdate(spDoc.SPDId, spDoc.SertifiDocumentId);
                        //css.Entry(spDoc).State = System.Data.EntityState.Modified;
                        //css.SaveChanges();

                        #endregion
                    }
                    else if (documentTypeID == 10)
                    {
                        #region Background Authorization

                        string sertifiPDFId = ConfigurationManager.AppSettings["SertifiPDFIdBGAuthorization"].ToString();
                        string presentAddress = (!String.IsNullOrEmpty(user.AddressPO) ? user.AddressPO + ", " : "") + (!String.IsNullOrEmpty(user.AddressLine1) ? user.AddressLine1 + ", " : "") + (!String.IsNullOrEmpty(user.StreetAddress) ? user.StreetAddress + ", " : "");
                        string cityStateZip = "";
                        cityStateZip += user.City;

                        if (!String.IsNullOrEmpty(user.State))
                        {
                            StateProvince objStateProvince = null;
                            if (css.StateProvinces.Where(p => p.StateProvinceCode == user.State).ToList().Count > 0)
                            {
                                objStateProvince = css.StateProvinces.Where(p => p.StateProvinceCode == user.State).First();
                            }
                            if (objStateProvince != null)
                            {
                                cityStateZip += " ," + objStateProvince.Name;
                            }
                        }
                        cityStateZip += !String.IsNullOrEmpty(user.Zip) ? " ," + user.Zip : "";

                        string prepopulateXMLData = "";
                        prepopulateXMLData += "<?xml version=\"1.0\" encoding=\"us-ascii\"?>";
                        prepopulateXMLData += "<record>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page3[0].First[0]\" Text=\"" + user.FirstName + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page3[0].Applicant_Last_Name[0]\" Text=\"" + user.LastName + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page3[0].Middle[0]\" Text=\"" + user.MiddleInitial + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page3[0].Date[0]\" Text=\"" + DateTime.Now.ToString("MM/dd/yyyy") + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page3[0].Social_Security[0]\" Text=\"" + user.SSN + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page3[0].Date_of_Birth_for_ID_purposes_only[0]\" Text=\"" + (user.ServiceProviderDetail.DOB.HasValue ? user.ServiceProviderDetail.DOB.Value.ToString("MM/dd/yyyy") : "") + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page3[0].Present_Address[0]\" Text=\"" + presentAddress + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page3[0].CityStateZip[0]\" Text=\"" + cityStateZip + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page3[0].Driver_s_License[0]\" Text=\"" + user.ServiceProviderDetail.DriversLicenceNumber + "\"/>";
                        prepopulateXMLData += "</record>";
                        documentID = sertifiAPIRequest.AddPrepopulatedPdfToRequest(_APICode, fileID, sertifiPDFId, docType.DocumentDesc, prepopulateXMLData);

                        spDoc.SertifiDocumentId = documentID;
                        css.SertifiDocumentIdUpdate(spDoc.SPDId, spDoc.SertifiDocumentId);
                        //css.Entry(spDoc).State = System.Data.EntityState.Modified;
                        //css.SaveChanges();
                        //sertifiAPIRequest.AddLocation(_APICode, fileID, documentID, 3, 800, 400, 0, 0, user.Email, "", 7);

                        #endregion
                    }
                    else if (documentTypeID == 11)
                    {
                        #region Direct Deposit Form

                        string sertifiPDFId = ConfigurationManager.AppSettings["SertifiPDFIdDirectDeposit"].ToString();
                        string printedname = user.FirstName + " " + user.LastName;


                        string prepopulateXMLData = "";
                        prepopulateXMLData += "<?xml version=\"1.0\" encoding=\"us-ascii\"?>";
                        prepopulateXMLData += "<record>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].Printed_Name[0]\" Text=\"" + printedname + "\"/>";
                        prepopulateXMLData += "<field Name=\"topmostSubform[0].Page1[0].Date[0]\" Text=\"" + DateTime.Now.ToString("MM/dd/yyyy") + "\"/>";
                        prepopulateXMLData += "</record>";
                        documentID = sertifiAPIRequest.AddPrepopulatedPdfToRequest(_APICode, fileID, sertifiPDFId, docType.DocumentDesc, prepopulateXMLData);

                        spDoc.SertifiDocumentId = documentID;
                        css.SertifiDocumentIdUpdate(spDoc.SPDId, spDoc.SertifiDocumentId);
                        //css.Entry(spDoc).State = System.Data.EntityState.Modified;
                        //css.SaveChanges();

                        #endregion
                    }
                }

                #endregion

                #region Generate URL to Sign or View Document
                //identify whether the current documentID has been signed and submitted or not
                //string strFileSigners = sertifiAPIRequest.GetFileSigners(_APICode, fileID);
                //XmlDocument myXmlDoc = new XmlDocument();
                //myXmlDoc.LoadXml(strFileSigners);
                //XmlNode node = myXmlDoc.SelectSingleNode(@"/Signers/Signer[DocumentID='" + documentID + "']");
                //string docSignStatus = node["Status"].InnerText;

                bool docIsSigned = isSertifiDocumentSigned(fileID, documentID, spDoc.SPDId);

                SertifiAPI.LinkParameter[] linkParams = new SertifiAPI.LinkParameter[1];
                linkParams[0] = new SertifiAPI.LinkParameter();
                linkParams[0].FileId = fileID;
                linkParams[0].DocumentId = documentID;
                if (!docIsSigned)
                {
                    linkParams[0].LinkType = SertifiAPI.LinkParameterType.SigningPageLink;
                }
                else
                {
                    linkParams[0].LinkType = SertifiAPI.LinkParameterType.DocumentSignedLink;
                }
                linkParams[0].SignerEmail = signers;

                SertifiAPI.UrlQueryResult[] url;
                url = sertifiAPIRequest.GetLink(_APICode, linkParams);

                valueToReturn = url[0].Link;
                #endregion
            }
            catch (Exception ex)
            {
            }
            return valueToReturn;
        }
        //public static bool isSertifiDocumentSigned(string fileId, string documentId)
        //{


        //   bool valueToReturn = false;
        //    SertifiAPI.Gateway sertifiAPIRequest = new SertifiAPI.Gateway();

        //    //identify whether the current documentID has been signed and submitted or not
        //    string strFileSigners = sertifiAPIRequest.GetFileSigners(_APICode, fileId);
        //    XmlDocument myXmlDoc = new XmlDocument();
        //    myXmlDoc.LoadXml(strFileSigners);
        //    XmlNode node = myXmlDoc.SelectSingleNode(@"/Signers/Signer[DocumentID='" + documentId + "']");
        //    string docSignStatus = node["Status"].InnerText;

        //    if (docSignStatus.ToLower() == "signed")
        //    {
        //        valueToReturn = true;
        //    }
        //    return valueToReturn;

        //}
        public static bool isSertifiDocumentSigned(string fileId, string documentId, Int64 SPDId)
        {
            bool valueToReturn = false;
            try
            {
                string _APICode = ConfigurationManager.AppSettings["SertifiAPICode"].ToString();
                SertifiAPI.Gateway sertifiAPIRequest = new SertifiAPI.Gateway();
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                //identify whether the current documentID has been signed and submitted or not
                string strFileSigners = sertifiAPIRequest.GetFileSigners(_APICode, fileId);
                XmlDocument myXmlDoc = new XmlDocument();
                myXmlDoc.LoadXml(strFileSigners);
                XmlNode node = myXmlDoc.SelectSingleNode(@"/Signers/Signer[DocumentID='" + documentId + "']");
                if (node != null)
                {
                    if (node["Status"] != null)
                    {
                        string docSignStatus = node["Status"].InnerText;
                        if (!String.IsNullOrEmpty(docSignStatus))
                        {
                            if (docSignStatus.ToLower() == "signed")
                            {
                                css.ServiceProviderDocumentsUpdate(SPDId, true);
                                css.SaveChanges();
                                valueToReturn = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return valueToReturn;

        }
        public static byte[] ConvertHTMLStringToPDF(string htmlData)
        {
            string htmlString = htmlData;
            PdfConverter pdfConverter = new PdfConverter();
            // set the license key
            pdfConverter.LicenseKey = "OgmRqJy4kkQWgihnkILiGC3ofMK83tlFSiAVpzjJv9Fa6+ZMh+8fNIirK6M+dxWT";
            // set the converter "P38cBx6AWW7b9c81TjEGxnrazP+J7rOjs+9omJ3TUycauK+cLWdrITM5T59hdW5r"
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.BelowNormal;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = false;

            // set to generate selectable pdf or a pdf with embedded image
            pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
            // set the embedded fonts option - optional, by default is false
            pdfConverter.PdfDocumentOptions.EmbedFonts = true;
            // enable the live HTTP links option - optional, by default is true
            pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = true;
            // enable the support for right to left languages , by default false
            pdfConverter.RightToLeftEnabled = false;
            // set PDF security options - optional
            pdfConverter.PdfSecurityOptions.CanPrint = true;
            pdfConverter.PdfSecurityOptions.CanEditContent = true;
            pdfConverter.PdfSecurityOptions.UserPassword = "";
            // set PDF document description - optional
            pdfConverter.PdfDocumentInfo.AuthorName = "IADA Claim Solutions Inc.";

            byte[] pdfBytes = null;
            pdfBytes = pdfConverter.GetPdfBytesFromHtmlString(htmlString);//, baseURL);


            return pdfBytes;
        }
        public static byte[] ConvertHTMLStringToPDF(string htmlData, int marginleft, int marginright)
        {
            string htmlString = htmlData;
            PdfConverter pdfConverter = new PdfConverter();
            // set the license key
            pdfConverter.LicenseKey = "OgmRqJy4kkQWgihnkILiGC3ofMK83tlFSiAVpzjJv9Fa6+ZMh+8fNIirK6M+dxWT";
            // set the converter "P38cBx6AWW7b9c81TjEGxnrazP+J7rOjs+9omJ3TUycauK+cLWdrITM5T59hdW5r"
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = false;
            pdfConverter.PdfDocumentOptions.LeftMargin = marginleft;
            pdfConverter.PdfDocumentOptions.RightMargin = marginright;


            // set to generate selectable pdf or a pdf with embedded image
            pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
            // set the embedded fonts option - optional, by default is false
            pdfConverter.PdfDocumentOptions.EmbedFonts = true;
            // enable the live HTTP links option - optional, by default is true
            pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = true;
            // enable the support for right to left languages , by default false
            pdfConverter.RightToLeftEnabled = false;
            // set PDF security options - optional
            pdfConverter.PdfSecurityOptions.CanPrint = true;
            pdfConverter.PdfSecurityOptions.CanEditContent = true;
            pdfConverter.PdfSecurityOptions.UserPassword = "";
            // set PDF document description - optional
            pdfConverter.PdfDocumentInfo.AuthorName = "IADA Claim Solutions Inc.";

            byte[] pdfBytes = null;
            try
            {
                pdfBytes = pdfConverter.GetPdfBytesFromHtmlString(htmlString);//, baseURL);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " " + ex.InnerException != null ? ex.InnerException.Message : "");
            }


            return pdfBytes;
        }
        public static byte[] ConvertHTMLStringToPDF(string htmlData, int lMargin, int rMargin, int tMargin, int bMargin, bool showHeader, string headerHtml, int headerHeight, bool showFooter, string footerHtml)
        {
            string htmlString = htmlData;
            PdfConverter pdfConverter = new PdfConverter();
            // set the license key
            pdfConverter.LicenseKey = "OgmRqJy4kkQWgihnkILiGC3ofMK83tlFSiAVpzjJv9Fa6+ZMh+8fNIirK6M+dxWT";
            // set the converter "P38cBx6AWW7b9c81TjEGxnrazP+J7rOjs+9omJ3TUycauK+cLWdrITM5T59hdW5r"
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.BelowNormal;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
            pdfConverter.PdfDocumentOptions.ShowHeader = showHeader;
            pdfConverter.PdfHeaderOptions.HeaderText = headerHtml;
            pdfConverter.PdfHeaderOptions.HeaderHeight = headerHeight;
            pdfConverter.PdfHeaderOptions.HeaderTextAlign = TextAlign.Left;
            pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
            pdfConverter.PdfHeaderOptions.HeaderSubtitleText = "";
            pdfConverter.PdfHeaderOptions.HeaderTextFontSize = 8;
            pdfConverter.PdfDocumentOptions.ShowFooter = showFooter;
            pdfConverter.PdfFooterOptions.FooterText = footerHtml;
            pdfConverter.PdfFooterOptions.FooterTextFontSize = 8;

            pdfConverter.PdfFooterOptions.DrawFooterLine = false;

            pdfConverter.PdfDocumentOptions.LeftMargin = lMargin;
            pdfConverter.PdfDocumentOptions.RightMargin = rMargin;
            pdfConverter.PdfDocumentOptions.TopMargin = tMargin;
            pdfConverter.PdfDocumentOptions.BottomMargin = bMargin;

            // set to generate selectable pdf or a pdf with embedded image
            pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
            // set the embedded fonts option - optional, by default is false
            pdfConverter.PdfDocumentOptions.EmbedFonts = true;
            // enable the live HTTP links option - optional, by default is true
            pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = true;
            // enable the support for right to left languages , by default false
            pdfConverter.RightToLeftEnabled = false;
            // set PDF security options - optional
            pdfConverter.PdfSecurityOptions.CanPrint = true;
            pdfConverter.PdfSecurityOptions.CanEditContent = true;
            pdfConverter.PdfSecurityOptions.UserPassword = "";
            // set PDF document description - optional
            pdfConverter.PdfDocumentInfo.AuthorName = "IADA Claim Solutions Inc.";

            byte[] pdfBytes = null;
            pdfBytes = pdfConverter.GetPdfBytesFromHtmlString(htmlString);//, baseURL);


            return pdfBytes;
        }
        public static byte[] ConvertHTMLStringToPDFwithImages(string htmlData, string thisPageURL)
        {
            string htmlString = htmlData;

            PdfConverter pdfConverter = new PdfConverter();
            // set the license key
            pdfConverter.LicenseKey = "OgmRqJy4kkQWgihnkILiGC3ofMK83tlFSiAVpzjJv9Fa6+ZMh+8fNIirK6M+dxWT";
            // set the converter "P38cBx6AWW7b9c81TjEGxnrazP+J7rOjs+9omJ3TUycauK+cLWdrITM5T59hdW5r"
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = false;

            // set to generate selectable pdf or a pdf with embedded image
            pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
            // set the embedded fonts option - optional, by default is false
            pdfConverter.PdfDocumentOptions.EmbedFonts = true;

            // enable the live HTTP links option - optional, by default is true
            pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = true;
            // enable the support for right to left languages , by default false
            pdfConverter.RightToLeftEnabled = false;
            // set PDF security options - optional
            pdfConverter.PdfSecurityOptions.CanPrint = true;
            pdfConverter.PdfSecurityOptions.CanEditContent = true;
            pdfConverter.PdfSecurityOptions.UserPassword = "";
            // set PDF document description - optional
            pdfConverter.PdfDocumentInfo.AuthorName = "IADA Claim Solutions Inc.";
            ControllerContext controllerContext = new ControllerContext();

            // Performs the conversion and get the pdf document bytes that you can further 
            // save to a file or send as a browser response
            //
            // The baseURL parameter helps the converter to get the CSS files and images
            // referenced by a relative URL in the HTML string. This option has efect only if the HTML string
            // contains a valid HEAD tag. The converter will automatically inserts a <BASE HREF="baseURL"> tag. 
            byte[] pdfBytes = null;

            //string baseUrl = thisPageURL.Substring(0, thisPageURL.LastIndexOf('/')) + "/";

            pdfBytes = pdfConverter.GetPdfBytesFromHtmlString(htmlString, thisPageURL);//, baseURL);

            //Please Write the following file checking on the top before calling all store procedures generatepdf() function

            //string PdfPath = ".pdf";
            //FileInfo fFile = new FileInfo(PdfPath);
            //If Not fFile.Exists Then
            //pdfConverter.SavePdfFromHtmlStringToFile(htmlString, Server.MapPath("\") & "uploads\TotalLossFeeCalculation\Summary Reports\" & strclaim & "_" & calculationid & ".pdf")
            //pdfConverter.SavePdfFromHtmlStringToFile(htmlString, Server.MapPath(PdfPath));
            //End If

            //if (!fFile.Exists)
            //  {
            // MessageBox.Show("File Not Found")
            //========================== Generating pdf ======================================
            //System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            //response.Clear();
            //response.AddHeader("Content-Type", "binary/octet-stream");
            //response.AddHeader("Content-Disposition", "attachment; filename=" + strDate + "_" + DateTime.Now.ToString("ddMMyyyy") + ".pdf; size=" + pdfBytes.Length.ToString());
            //response.Flush();
            //response.BinaryWrite(pdfBytes);
            //response.Flush();
            //response.End();
            // ================================== End here ==================================
            return pdfBytes;
        }
        public static byte[] ConvertHTMLStringToPDFwithImages(string htmlData, string thisPageURL, int lMargin, int rMargin, int tMargin, int bMargin, bool showHeader, string headerHtml, int headerHeight, bool showFooter, string footerHtml)
        {
            string htmlString = htmlData;

            PdfConverter pdfConverter = new PdfConverter();
            // set the license key
            pdfConverter.LicenseKey = "OgmRqJy4kkQWgihnkILiGC3ofMK83tlFSiAVpzjJv9Fa6+ZMh+8fNIirK6M+dxWT";
            // set the converter "P38cBx6AWW7b9c81TjEGxnrazP+J7rOjs+9omJ3TUycauK+cLWdrITM5T59hdW5r"
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
            pdfConverter.PdfDocumentOptions.ShowHeader = showHeader;
            pdfConverter.PdfDocumentOptions.ShowFooter = showFooter;

            pdfConverter.PdfDocumentOptions.LeftMargin = lMargin;
            pdfConverter.PdfDocumentOptions.RightMargin = rMargin;
            pdfConverter.PdfDocumentOptions.TopMargin = tMargin;
            pdfConverter.PdfDocumentOptions.BottomMargin = bMargin;


            // set to generate selectable pdf or a pdf with embedded image
            pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
            // set the embedded fonts option - optional, by default is false
            pdfConverter.PdfDocumentOptions.EmbedFonts = true;

            // enable the live HTTP links option - optional, by default is true
            pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = true;
            // enable the support for right to left languages , by default false
            pdfConverter.RightToLeftEnabled = false;
            // set PDF security options - optional
            pdfConverter.PdfSecurityOptions.CanPrint = true;
            pdfConverter.PdfSecurityOptions.CanEditContent = true;
            pdfConverter.PdfSecurityOptions.UserPassword = "";
            // set PDF document description - optional
            pdfConverter.PdfDocumentInfo.AuthorName = "IADA Claim Solutions Inc.";
            ControllerContext controllerContext = new ControllerContext();

            // Performs the conversion and get the pdf document bytes that you can further 
            // save to a file or send as a browser response
            //
            // The baseURL parameter helps the converter to get the CSS files and images
            // referenced by a relative URL in the HTML string. This option has efect only if the HTML string
            // contains a valid HEAD tag. The converter will automatically inserts a <BASE HREF="baseURL"> tag. 
            byte[] pdfBytes = null;

            //string baseUrl = thisPageURL.Substring(0, thisPageURL.LastIndexOf('/')) + "/";

            pdfBytes = pdfConverter.GetPdfBytesFromHtmlString(htmlString, thisPageURL);//, baseURL);

            //Please Write the following file checking on the top before calling all store procedures generatepdf() function

            //string PdfPath = ".pdf";
            //FileInfo fFile = new FileInfo(PdfPath);
            //If Not fFile.Exists Then
            //pdfConverter.SavePdfFromHtmlStringToFile(htmlString, Server.MapPath("\") & "uploads\TotalLossFeeCalculation\Summary Reports\" & strclaim & "_" & calculationid & ".pdf")
            //pdfConverter.SavePdfFromHtmlStringToFile(htmlString, Server.MapPath(PdfPath));
            //End If

            //if (!fFile.Exists)
            //  {
            // MessageBox.Show("File Not Found")
            //========================== Generating pdf ======================================
            //System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            //response.Clear();
            //response.AddHeader("Content-Type", "binary/octet-stream");
            //response.AddHeader("Content-Disposition", "attachment; filename=" + strDate + "_" + DateTime.Now.ToString("ddMMyyyy") + ".pdf; size=" + pdfBytes.Length.ToString());
            //response.Flush();
            //response.BinaryWrite(pdfBytes);
            //response.Flush();
            //response.End();
            // ================================== End here ==================================
            return pdfBytes;
        }
        public static bool StoreBytesAsFile(byte[] data, string pathWithFileName)
        {
            bool valueToReturn = true;
            try
            {
                System.IO.File.WriteAllBytes(pathWithFileName, data);
            }
            catch (Exception ex)
            {
                valueToReturn = false;
            }
            return valueToReturn;
        }
        public static void ExportClaim(BLL.Claim claim, int? headcompanyid)
        {
            try
            {
                //ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
                //SymbilityService.SymbilityClaimsService symbilityClaimService = new SymbilityClaimsService();
                //ClaimIDSpecification claimidSpecification = new ClaimIDSpecification();
                //ClaimSpecification claimspecification = new ClaimSpecification();
                //CompanyIDSpecification compIdSpecification = new CompanyIDSpecification();
                ////sCustomField[] customFields = new CustomField[5];
                //SymbilityService.Claim claimsysmbility = new SymbilityService.Claim();
                //ClaimFilterSpecification claimFilterSpecification = new ClaimFilterSpecification();
                //symbilityClaimService.AuthenticationHeaderValue = new AuthenticationHeader();
                //symbilityClaimService.Url = System.Configuration.ConfigurationManager.AppSettings["AuthenticationURl"];  // "https://staging.symbility.net/Api/v01_18/Claims.asmx";
                //symbilityClaimService.AuthenticationHeaderValue.AccountNumber = System.Configuration.ConfigurationManager.AppSettings["AuthenticationHeaderAccountNumber"];// "ChoiceSSAPI";
                //symbilityClaimService.AuthenticationHeaderValue.Password = System.Configuration.ConfigurationManager.AppSettings["AuthenticationHeaderPassword"];// "Choice123";
                //UserIDSpecification userIdSpecification = new UserIDSpecification();
                //string HeadCompanyOfficeid = css.Companies.Where(x => x.CompanyId == headcompanyid).First().SymbilityHeadOfficeCompanyId;

                //claimidSpecification.ClaimID = claim.ClaimNumber;
                //claimidSpecification.ClaimIDType = SymbilityService.ClaimIDType.ClaimNumber;
                //compIdSpecification.CompanyID = HeadCompanyOfficeid;
                //claimFilterSpecification.IncludeOriginator = true;
                //claimFilterSpecification.IncludeInternalAssignees = true;
                //claimFilterSpecification.IncludeOriginatorUsers = true;
                //symbilityClaimService.GetClaim(claimidSpecification, compIdSpecification, claimFilterSpecification, out claimsysmbility);

                //////claimspecification.CatastropheNumber = claim.CatCode;
                ////claimspecification.Number = claimsysmbility.Number;
                ////claimspecification.PolicyNumber = claimsysmbility.PolicyNumber;
                ////claimspecification.LossDate = claimsysmbility.LossDate;
                ////claimspecification.InsuredFirstName = claimsysmbility.InsuredFirstName;
                ////claimspecification.InsuredLastName = claimsysmbility.InsuredLastName;
                ////symbilityClaimService.CreateClaim(claimspecification, null);

                //claimidSpecification.ClaimIDType = ClaimIDType.ClaimNumber;
                //claimidSpecification.ClaimID = claimsysmbility.Number;
                //compIdSpecification.CompanyIDType = CompanyIDType.CompanyID;
                //compIdSpecification.CompanyID = claimsysmbility.Originator.HeadOfficeCompanyID;
                //symbilityClaimService.SetClaimStatus(claimidSpecification, compIdSpecification, ClaimStatus.InspectionPerformed, userIdSpecification);

                //customFields[0] = new CustomField();
                //customFields[0].Name = "CatastropheNumber";
                //customFields[0].Value = claim.CatCode;

                //symbilityClaimService.SetClaimCustomFields(claimidSpecification, compIdSpecification, customFields, userIdSpecification);
            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
        }
        public static void QBUpdateVendor(Int64 userId, string strAccessToken)
        {
            css = new ChoiceSolutionsEntities();
            BLL.User user = css.Users.Find(userId);
            BLL.ServiceProviderDetail spd = css.ServiceProviderDetails.Find(user.UserId);
            QBUpdateVendor(user, spd, strAccessToken);

        }
        public static void QBUpdateVendor(BLL.User user, BLL.ServiceProviderDetail spDetail, string srtAccessToken)
        {
            try
            {
                if (ConfigurationManager.AppSettings["IADAQBServiceSendNotification"].ToString() == "1")
                {
                    css = new ChoiceSolutionsEntities();
                    //bool areDocumentsSigned = true;
                    //Agreement
                    //List<ServiceProviderDocument> spdocument = css.ServiceProviderDocuments.Where(x => x.UserId == user.UserId).ToList();
                    
                    //if (spdocument.Where(x => x.DTId == 8).ToList().Count > 0)
                    //{
                    //    ServiceProviderDocument spd = spdocument.Where(x => x.DTId == 8).First();
                    //    if (!isSertifiDocumentSigned(spDetail.SertifiFileId, spd.SertifiDocumentId, spd.SPDId))
                    //    {
                    //        areDocumentsSigned = false;
                    //    }
                    //}
                    //else if (spdocument.Where(x => x.DTId == 12).ToList().Count > 0)
                    //{
                    //    ServiceProviderDocument spd = spdocument.Where(x => x.DTId == 12).First();
                    //    if (!isSertifiDocumentSigned(spDetail.SertifiFileId, spd.SertifiDocumentId, spd.SPDId))
                    //    {
                    //        areDocumentsSigned = false;
                    //    }
                    //}
                    //else
                    //{
                    //    areDocumentsSigned = false;
                    //}

                    ////W9
                    //if (spdocument.Where(x => x.DTId == 9).ToList().Count > 0)
                    //{
                    //    ServiceProviderDocument spd = spdocument.Where(x => x.DTId == 9).First();
                    //    if (!isSertifiDocumentSigned(spDetail.SertifiFileId, spd.SertifiDocumentId, spd.SPDId))
                    //    {
                    //        areDocumentsSigned = false;
                    //    }
                    //}
                    //else
                    //{
                    //    areDocumentsSigned = false;
                    //}
                    
                    //DDF
                    //if (spdocument.Where(x => x.DTId == 11).ToList().Count > 0)
                    //{
                    //    ServiceProviderDocument spd = spdocument.Where(x => x.DTId == 11).First();
                    //    if (!isSertifiDocumentSigned(spDetail.SertifiFileId, spd.SertifiDocumentId, spd.SPDId))
                    //    {
                    //        areDocumentsSigned = false;
                    //    }
                    //}
                    //else
                    //{
                    //    areDocumentsSigned = false;
                    //}


                    //if (user.Active == true && areDocumentsSigned == true)
                    if (user.Active == true)
                    {
                        IADAQBVendorService.VendorServiceClient vendorService = new IADAQBVendorService.VendorServiceClient();
                        vendorService.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["CSSQBServiceUID"];
                        vendorService.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["CSSQBServicePWD"];
                        IADAQBVendorService.Vendor vedorobj = new IADAQBVendorService.Vendor();

                        //vedorobj.fullyQualifiedNameField = user.FirstName + " " + user.MiddleInitial + " " + user.LastName;
                        string strCompanyName = css.Companies.Where(c => c.CompanyId == user.HeadCompanyId).FirstOrDefault().CompanyName;

                        vedorobj.fullyQualifiedNameField = user.FirstName + " " + user.LastName;


                        vedorobj.givenNameField = user.FirstName;
                        vedorobj.middleNameField = user.MiddleInitial;
                        vedorobj.familyNameField = user.LastName;
                        if ((spDetail.IsDBA ?? false) == true)
                        {
                            /* vedorobj.dBANameField = spDetail.DBA_LLC_Firm_Corp;
                             vedorobj.showAsField = spDetail.DBA_LLC_Firm_Corp;*/
                            vedorobj.companyNameField = spDetail.DBA_LLC_Firm_Corp;
                            vedorobj.displayNameField = spDetail.DBA_LLC_Firm_Corp;
                        }

                        if (string.IsNullOrEmpty(vedorobj.companyNameField) && !string.IsNullOrEmpty(strCompanyName))
                        {
                            vedorobj.companyNameField = strCompanyName;
                        }



                        //  vedorobj.taxIdentifierField = spDetail.TaxID; /*when taxid is empty take SSN as Taxid- commented by Pratik */
                        if ((spDetail.IsDBA ?? false) == true)
                        {
                            vedorobj.taxIdentifierField = spDetail.TaxID;
                        }
                        else
                        {
                            vedorobj.taxIdentifierField = user.SSN;
                        }
                        //vedorobj.acctNumField = "12345";
                        if (!String.IsNullOrEmpty(user.Email))
                        {
                            //vedorobj.emailField = new CSSQBVendorService.EmailAddress[1];
                            vedorobj.primaryEmailAddrField = new IADAQBVendorService.EmailAddress();
                            vedorobj.primaryEmailAddrField.addressField = user.Email;
                        }

                        // vedorobj.phoneField = new CSSQBVendorService.TelephoneNumber[2];
                        vedorobj.primaryPhoneField = new IADAQBVendorService.TelephoneNumber();
                        vedorobj.primaryPhoneField.deviceTypeField = "LandLine";
                        if (!String.IsNullOrEmpty(user.HomePhone))
                        {
                            /*vedorobj.phoneField[0].freeFormNumberField = user.HomePhone;*/
                            vedorobj.primaryPhoneField.freeFormNumberField = user.HomePhone;

                        }

                        vedorobj.mobileField = new IADAQBVendorService.TelephoneNumber();

                        vedorobj.mobileField.deviceTypeField = "Mobile";
                        if (!String.IsNullOrEmpty(user.MobilePhone))
                        {
                            /*vedorobj.phoneField[1].freeFormNumberField = user.MobilePhone;*/
                            vedorobj.mobileField.freeFormNumberField = user.MobilePhone;

                        }
                        //vedorobj.webSiteField[0].uRIField = "http://www.google.com";
                        /*
                        vedorobj.addressField = new CSSQBVendorService.PhysicalAddress[1];
                        vedorobj.addressField[0] = new CSSQBVendorService.PhysicalAddress();
                        vedorobj.addressField[0].cityField = user.City;
                        vedorobj.addressField[0].countrySubDivisionCodeField = user.State;
                        vedorobj.addressField[0].countryField = "USA";
                        vedorobj.addressField[0].postalCodeField = user.Zip;
                        vedorobj.addressField[0].line1Field = user.AddressPO;
                        vedorobj.addressField[0].line2Field = user.AddressLine1;
                        vedorobj.addressField[0].line3Field = user.StreetAddress;
                        */
                        vedorobj.billAddrField = new IADAQBVendorService.PhysicalAddress();
                        vedorobj.billAddrField.cityField = user.City;
                        vedorobj.billAddrField.countrySubDivisionCodeField = user.State;
                        vedorobj.billAddrField.countryField = "USA";
                        vedorobj.billAddrField.postalCodeField = user.Zip;
                        vedorobj.billAddrField.line1Field = user.AddressPO;
                        vedorobj.billAddrField.line2Field = user.AddressLine1;
                        vedorobj.billAddrField.line3Field = user.StreetAddress;


                        
                        //ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                        if (String.IsNullOrEmpty(spDetail.QBVendorId))
                        {
                            string id;

                            id = vendorService.AddVendor(vedorobj, srtAccessToken);
                            css.usp_UpdateQBVendorId(user.HeadCompanyId, id);
                            //css.usp_SPQBVendorIdUpdate(user.UserId, id);

                        }
                        else
                        {
                            /*
                            vedorobj.idField = new CSSQBVendorService.IdType();
                            vedorobj.idField.valueField = spDetail.QBVendorId;
                            vendorService.UpdateVendor(vedorobj);
                              */
                            vedorobj.idField = spDetail.QBVendorId;
                            vendorService.UpdateVendor(vedorobj, srtAccessToken);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogException("QBUpdateVendor", ex, user.UserId + "");

            }
        }
        public static void QBUpdateCustomer(int companyId, string strAccessToken)
        {
            css = new ChoiceSolutionsEntities();
            BLL.Company company = css.Companies.Find(companyId);
            QBUpdateCustomer(company, strAccessToken);
        }
        public static void QBUpdateCustomer(BLL.Company company, string strAccessToken)
        {
            try
            {
                if (ConfigurationManager.AppSettings["IADAQBServiceSendNotification"].ToString() == "1")
                {
                    css = new ChoiceSolutionsEntities();
                    IADAQBCustomerService.CustomerServiceClient customerService = new IADAQBCustomerService.CustomerServiceClient();
                    customerService.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["CSSQBServiceUID"];
                    customerService.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["CSSQBServicePWD"];
                    IADAQBCustomerService.Customer customerObject = new IADAQBCustomerService.Customer();
                    customerObject.companyNameField = company.CompanyName;
                    customerObject.displayNameField = company.CompanyName;

                    customerObject.givenNameField = company.CompanyName;
                    customerObject.fullyQualifiedNameField = company.CompanyName;
                    //customerObject.paymentMethodIdField = new CSSQBCustomerService.IdType();
                    //customerObject.paymentMethodIdField.valueField
                    //Address
                    /*
                    customerObject.addressField = new CSSQBCustomerService.PhysicalAddress[1];
                    customerObject.addressField[0] = new CSSQBCustomerService.PhysicalAddress();
                    customerObject.addressField[0].line1Field = company.Address;
                    customerObject.addressField[0].cityField = company.City;
                    customerObject.addressField[0].countrySubDivisionCodeField = company.State;
                    customerObject.addressField[0].countryField = "USA";
                    customerObject.addressField[0].postalCodeField = company.Zip;
                    */

                    customerObject.billAddrField = new IADAQBCustomerService.PhysicalAddress();
                    customerObject.billAddrField.line1Field = company.Address;
                    customerObject.billAddrField.cityField = company.City;
                    customerObject.billAddrField.countrySubDivisionCodeField = company.State;
                    customerObject.billAddrField.countryField = "USA";
                    customerObject.billAddrField.postalCodeField = company.Zip;

                    if (!String.IsNullOrEmpty(company.WebsiteURL))
                    {
                        //Website
                        //customerObject.webSiteField = new CSSQBCustomerService.WebSiteAddress[1];
                        customerObject.webAddrField = new IADAQBCustomerService.WebSiteAddress();

                        if (company.WebsiteURL.Contains("http://") || company.WebsiteURL.Contains("https://"))
                            customerObject.webAddrField.uRIField = company.WebsiteURL;
                        else
                        {
                            string websiteuri = string.Concat("http://", company.WebsiteURL);
                            customerObject.webAddrField.uRIField = websiteuri;
                        }
                    }

                    //Conatact Details

                    customerObject.primaryPhoneField = new IADAQBCustomerService.TelephoneNumber();

                    if (!String.IsNullOrEmpty(company.PhoneNumber))
                    {
                        customerObject.primaryPhoneField.freeFormNumberField = company.PhoneNumber;
                    }
                    customerObject.primaryPhoneField.deviceTypeField = "Landline";

                    customerObject.faxField = new IADAQBCustomerService.TelephoneNumber();
                    if (!String.IsNullOrEmpty(company.Fax))
                    {
                        customerObject.faxField.freeFormNumberField = company.Fax;
                    }
                    customerObject.faxField.deviceTypeField = "Fax";

                    //Email
                    if (!String.IsNullOrEmpty(company.Email))
                    {
                        customerObject.primaryEmailAddrField = new IADAQBCustomerService.EmailAddress();
                        customerObject.primaryEmailAddrField.addressField = company.Email;
                    }

                   
                    //ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                    if (String.IsNullOrEmpty(company.QBCustomerId))
                    {
                        string id;
                        id = customerService.AddCustomer(customerObject, strAccessToken);
                        css.usp_CompanyQBCustomerIdUpdate(company.CompanyId, id);
                    }
                    else
                    {
                        /*
                        customerObject.idField = new CSSQBCustomerService.IdType();
                        customerObject.idField.valueField = company.QBCustomerId;
                        customerService.UpdateCustomer(customerObject);
                          */
                        customerObject.idField = company.QBCustomerId;
                        customerService.UpdateCustomer(customerObject, strAccessToken);
                    }


                }
            }
            catch (Exception ex)
            {
                LogException("QBUpdateCustomer", ex, company.CompanyId + "");

            }

        }

        public static void QBUpdateInvoice(Int64 AutoInvoiceId)
        {
            css = new ChoiceSolutionsEntities();
            BLL.AutoInvoice invoice = css.AutoInvoices.Find(AutoInvoiceId);
            BLL.AutoAssignment Autoassignments = css.AutoAssignments.Find(invoice.AutoAssignmentId);
            BLL.Claim claims = css.Claims.Find(Autoassignments.ClaimId);
            int custid = Convert.ToInt16(claims.HeadCompanyId);
            string AccessToken = getStrAccessToken(AutoInvoiceId);
            QBUpdateInvoice(invoice, custid, AccessToken);

            //BLL.PropertyInvoice invoice = css.PropertyInvoices.Find(AutoInvoiceId);
            //BLL.PropertyAssignment propertyAssign = css.PropertyAssignments.Find(invoice.AssignmentId);
            //BLL.Claim claim = css.Claims.Find(propertyAssign.ClaimId);
            //int custid = Convert.ToInt16(claim.HeadCompanyId);
            //QBUpdateInvoice(invoice, custid);

            
        }
        public static string getStrAccessToken(Int64 AutoInvoiceId)
        {
            string strAccessToken = "";
            try
            {
                //Check for OAuth acceessKeyToken

                //Set first parameter flat QBManageTokens stored procedure as
                //0. false -> Get
                //1. true  -> Update
               
                QBManageTokens_Result objQBManageToken = css.QBManageTokens(false, null, null).FirstOrDefault();
                if (objQBManageToken != null)
                {
                    if (string.IsNullOrEmpty(objQBManageToken.AccessToken))
                    {
                        AccountController obj = new AccountController();
                        strAccessToken = obj.setQuickBookDetail();

                        if (strAccessToken.Contains("Error"))
                        {
                            Utility.LogException("QBUpdateInvoice/getStrAccessToken", new Exception(), AutoInvoiceId + "|" + strAccessToken);
                        }
                 
                    }
                    else
                    {
                        strAccessToken = objQBManageToken.AccessToken;
                       
                    }
                }
                                
            }
            catch
            {

            }
            return strAccessToken;
        }
        public static void QBUpdateInvoice(BLL.AutoInvoice invoice, int custid, string strAccessToken)
        {
            try
            {
                if (ConfigurationManager.AppSettings["CSSQBServiceSendNotification"].ToString() == "1")
                {
                    css = new ChoiceSolutionsEntities();
                    //CSSQBInvoiceService.InvoiceServiceClient invoiceService = new CSSQBInvoiceService.InvoiceServiceClient();
                    IADAQBInvoiceService.InvoiceServiceClient invoiceService = new IADAQBInvoiceService.InvoiceServiceClient();
                    invoiceService.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["CSSQBServiceUID"];
                    invoiceService.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["CSSQBServicePWD"];
                    BLL.Company company = css.Companies.Find(custid);
                    
                    //if the company/customer is not bridged to QB then create the customer on QB first
            
                    if (String.IsNullOrEmpty(company.QBCustomerId))
                    {
                        QBUpdateCustomer(company, strAccessToken);
                        company = css.Companies.Find(custid);//Get the updated QBCustomerId
                    }
                  
                    //ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                    if (!String.IsNullOrEmpty(company.QBCustomerId))
                    {
                        string id=""; //chnage for INPSW -32
                        if (String.IsNullOrEmpty(invoice.QBInvoiceId))
                        {
                            id = invoiceService.AddInvoice(invoice.AutoInvoiceId, company.QBCustomerId, strAccessToken);
                            css.usp_AutoInvoiceQBInvoiceIdUpdate(invoice.AutoInvoiceId, id);

                        }
                        else
                        {
                            id = invoiceService.UpdateInvoice(invoice.AutoInvoiceId, company.QBCustomerId, strAccessToken);
                        }
                    }


                }
            }

            catch (Exception ex)
            {
                LogException("QBUpdateInvoice", ex, invoice.AutoInvoiceId + "|" + custid);

            }

        }

        public static string QBUpdateBill(Int64 userId, decimal paymentAmount, DateTime payrollDateTime, DateTime endDate, List<usp_PayrollSummaryDetailReport_Result> payrollDetails, string strAccessToken)
        {
            //SP Payroll and Adjustments
            string valueToReturn = null;
            try
            {
                if (ConfigurationManager.AppSettings["CSSQBServiceSendNotification"].ToString() == "1")
                {
                    css = new ChoiceSolutionsEntities();
                    ServiceProviderDetail spd = css.ServiceProviderDetails.Find(userId);

                    //if the company/customer is not bridged to QB then create the customer on QB first
                    var lineItemByCompany = payrollDetails.GroupBy(x => x.HeadCompanyId).ToList();
                    foreach (var payrollCompany in lineItemByCompany)
                    {
                        Int32 headCompanyId = payrollCompany.First().HeadCompanyId.Value;
                        string QBCustomerId = css.Companies.Find(headCompanyId).QBCustomerId;
                        if (String.IsNullOrEmpty(QBCustomerId))
                        {
                            QBUpdateCustomer(headCompanyId, strAccessToken);
                        }
                    }

                    //if SP/Vendor is not bridged to QB then first create the vendor on QB
                    if (String.IsNullOrEmpty(spd.QBVendorId))
                    {
                        QBUpdateVendor(userId, strAccessToken);
                        spd = css.ServiceProviderDetails.Find(userId);//Get the updated QBVendorId
                    }


                    if (!String.IsNullOrEmpty(spd.QBVendorId))
                    {
                        string qbVendorId = spd.QBVendorId;

                        IADAQBBillService.BillServiceClient billService = new IADAQBBillService.BillServiceClient();
                        billService.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["CSSQBServiceUID"];
                        billService.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["CSSQBServicePWD"];

                      
                        //ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                        //List<IADAQBBillService.usp_PayrollSummaryDetailReport_Result> qbPayrollDetails = new List<IADAQBBillService.usp_PayrollSummaryDetailReport_Result>();
                        foreach (var detail in payrollDetails)
                        {
                            List<IADAQBBillService.usp_PayrollSummaryDetailReport_Result> qbPayrollDetails = new List<IADAQBBillService.usp_PayrollSummaryDetailReport_Result>();
                            IADAQBBillService.usp_PayrollSummaryDetailReport_Result obj = new IADAQBBillService.usp_PayrollSummaryDetailReport_Result();
                            //obj.AmountHoldBack = detail.AmountHoldBack;
                            //obj.AmountPaidToSP = detail.AmountPaidToSP;
                            //obj.AssignmentId = detail.AssignmentId;
                            //obj.ClaimId = detail.ClaimId;
                           //obj.ClaimNumber = detail.ClaimNumber;
                            
                            obj.HeadCompanyIdk__BackingField = detail.HeadCompanyId;

                            //obj.HoldBackPaymentDate = detail.HoldBackPaymentDate;
                            obj.InvoiceNok__BackingField = detail.InvoiceNo;
                            obj.SpInvoiceNumberk__BackingField = detail.SpInvoiceNumber;//INPSW -40
                            //obj.IsPaymentReceived = detail.IsPaymentReceived;
                            obj.PAIdk__BackingField = detail.PAId;
                            obj.PaymentAmountk__BackingField = detail.PaymentAmount;
                            //obj.PaymentDate = detail.PaymentDate;
                            obj.PaymentTypek__BackingField = detail.PaymentType.Value;
                            //obj.PaymentTypeDesc = detail.PaymentTypeDesc;
                            //obj.TotalSPPay = detail.TotalSPPay;
                            //obj.TotalSPPayPercent = detail.TotalSPPayPercent;
                            
                            qbPayrollDetails.Add(obj);

                            //valueToReturn = billService.AddBill(userId, paymentAmount, payrollDateTime, endDate, qbPayrollDetails.ToArray(), strAccessToken);
                            valueToReturn = billService.AddBill(userId, detail.PaymentAmount, payrollDateTime, endDate, qbPayrollDetails.ToArray(), strAccessToken);

                            css.usp_SPPayrollAndAdjUpdateQBBillId(detail.PAId, detail.PaymentType, valueToReturn);
                            qbPayrollDetails = null;
                        }

                        //valueToReturn = billService.AddBill(userId, paymentAmount, payrollDateTime, endDate, qbPayrollDetails.ToArray(), strAccessToken);
                    }
                }
            }
            catch (Exception ex)
            {
                LogException("QBUpdateBill", ex, userId + "|" + paymentAmount + "|" + payrollDateTime + "|" + endDate);

            }
            return valueToReturn;

        }
        public static void QBUpdatePayment(Int64 paymentId)
        {
            string id = string.Empty;
            try
            {
                if (ConfigurationManager.AppSettings["CSSQBServiceSendNotification"].ToString() == "1")
                {
                    css = new ChoiceSolutionsEntities();

                    BLL.Payment optPayment = css.Payments.Find(paymentId);
                    BLL.PropertyInvoice optPropertyInvoice = css.PropertyInvoices.Find(optPayment.InvoiceId);

                    string qbInvoiceId = optPropertyInvoice.QBInvoiceId;
                    Int64 claimId = css.PropertyAssignments.Find(optPropertyInvoice.AssignmentId.Value).ClaimId.Value;
                    int headCompanyId = css.Claims.Find(claimId).HeadCompanyId.Value;
                    string qbCustomerId = css.Companies.Find(headCompanyId).QBCustomerId;

                    //if the company/customer is not bridged to QB then create the customer on QB first
                    if (String.IsNullOrEmpty(qbCustomerId))
                    {
                        //QBUpdateCustomer(headCompanyId);
                    }

                    //if the invoice is not bridged to QB then create the invoice on QB first
                    if (String.IsNullOrEmpty(qbInvoiceId))
                    {
                        QBUpdateInvoice(optPropertyInvoice.InvoiceId);
                    }

                    CSSQBPaymentService.PaymentServiceClient paymentService = new CSSQBPaymentService.PaymentServiceClient();
                    paymentService.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["CSSQBServiceUID"];
                    paymentService.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["CSSQBServicePWD"];

                  
                    //ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                    //id = paymentService.AddPayment(paymentId);
                    //css.usp_PaymentQBPaymentIdUpdate(paymentId, id);
                }
            }
            catch (Exception ex)
            {
                //LogException("QBUpdatePayment", ex, paymentId + ""); commented for capturing Qb Payment id

                if (id == null && id == "")
                {
                    id = string.Empty;

                }

                LogException("QBUpdatePayment", ex, paymentId + "|" + id);

            }
        }
        public static void QBDeletePayment(string qbPaymentId)
        {
            try
            {
                if (ConfigurationManager.AppSettings["CSSQBServiceSendNotification"].ToString() == "1")
                {
                    if (!String.IsNullOrEmpty(qbPaymentId))
                    {
                        css = new ChoiceSolutionsEntities();
                        CSSQBPaymentService.PaymentServiceClient paymentService = new CSSQBPaymentService.PaymentServiceClient();
                        paymentService.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["CSSQBServiceUID"];
                        paymentService.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["CSSQBServicePWD"];
                        paymentService.RemovePayment(qbPaymentId);
                    }
                }
            }
            catch (Exception ex)
            {
                LogException("QBUpdateDeletePayment", ex, qbPaymentId);

            }
        }

        public static bool isZipCodeValid(string zipCode)
        {
            bool valueToReturn = true;
            string _usZipRegEx = @"^\d{5}(?:[-\s]\d{4})?$";
            if (!Regex.Match(zipCode, _usZipRegEx).Success)
            {
                valueToReturn = false;
            }
            return valueToReturn;
        }
        public static void LogException(string source, Exception ex, string customMessage = "")
        {
            try
            {
                css.usp_ExceptionLogInsert(source, ex.Message, ex.InnerException != null ? ex.InnerException.Message : "", ex.StackTrace, customMessage);
            }
            catch { }
        }

        public static string GetCurrentSessionId()
        {
            HttpSessionState ss = HttpContext.Current.Session;
            HttpContext.Current.Session["test"] = "test";
            return ss.SessionID;

        }

        #region VirtualPath
        public static string GetClaimServiceProviderAbsoluteDocPath(Int64 HeadCompanyID, String ImagePath)
        {
            string baseFolder = MvcApplication.ClaimServiceProviderDocPath;
            String RelativePath = String.Empty;
            if (MvcApplication.UseAzureCloudStorage == "1")
            {
              if (IsCloudPathExist(CloudStorageUtility.GetAbsoluteFileURL(MvcApplication.CompanyThumbnailContainerName, HeadCompanyID + "/" + ImagePath)))
             { 
                RelativePath = CloudStorageUtility.GetAbsoluteFileURL(MvcApplication.CompanyThumbnailContainerName, HeadCompanyID + "/" + ImagePath);
             }
             else
             {
                 RelativePath = Path.Combine((VirtualPathUtility.ToAbsolute(baseFolder)), "avatar.jpg");
             }
            }
            else
            {
                if (!String.IsNullOrEmpty(ImagePath))
                {
                    RelativePath = Path.Combine((VirtualPathUtility.ToAbsolute(baseFolder) + "ThumbnailImage/" + HeadCompanyID), ImagePath);

                }
                else
                {
                    RelativePath = Path.Combine((VirtualPathUtility.ToAbsolute(baseFolder)), "avatar.jpg");
                }
            }
            return RelativePath;
        }

        //check if cloud url ix exist
        public static bool IsCloudPathExist(string Path)
        {
            HttpWebResponse response = null;
            var request = (HttpWebRequest)WebRequest.Create(Path);
            request.Method = "HEAD";
            bool isExist = false;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
                isExist = true;
            }
            catch (WebException ex)
            {
                /* A WebException will be thrown if the status of the response is not `200 OK` */
                isExist = false;
            }
            finally
            {
                // Don't forget to close your response.
                if (response != null)
                {
                    response.Close();
                }
            }
            return isExist;
        }

        private static DateTime? IsValidDate(object DT)
        {
            DateTime validdate;
            if (DT == null) return null;

            if (DateTime.TryParse(DT.ToString(), out validdate))
            {
                return validdate;
            }
            return null;
        }

        public static Int32? IsValidInt32(object Value)
        {
            Int32 validVal;
            if (Value == null) return null;

            if (Int32.TryParse(Value.ToString(), out validVal))
            {
                return validVal;
            }
            return null;
        }
        public static Int64? IsValidInt64(object Value)
        {
            Int64 validVal;
            if (Value == null) return null;

            if (Int64.TryParse(Value.ToString(), out validVal))
            {
                return validVal;
            }
            return null;
        }
        public static bool IsEmptyFormField(object Value)
        {
           
            if (Value == null) return true;

            if (string.IsNullOrEmpty(Value.ToString()))
            {
                return true;
            }
            return false;
        }
        public static string GetContentType(string filename)
        {
            string contentType = string.Empty;
            filename = filename.ToLower();
            if (filename.Contains(".pdf"))
            {
                contentType = "application/pdf";
            }

            else if (filename.Contains(".txt"))
            {
                contentType = "text/plain";
            }
            else if (filename.Contains(".docx"))
            {
                contentType = "application/docx";
            }
            else if (filename.Contains(".png"))
            {
                contentType = "image/png";
            }
            else if (filename.Contains(".jpeg"))
            {
                contentType = "image/jpeg";
            }
            else if (filename.Contains(".jpg"))
            {
                contentType = "image/jpg";
            }
            else if (filename.Contains(".xlsx"))
            {
                contentType = "application/xlsx";
            }
            return contentType;
        }

        public static byte[] ConvertRDLCToByteArray(Int64 AssignmentClaimID, out string mimeTypeOut,Int32 ServiceType)
        {
            LocalReport localReport = new LocalReport();
            ReportDataSource reportDataSource;
            byte[] renderedBytes;
            String ReportPath = String.Empty;
            try
            {
                if (ServiceType == ((Int32)BLL.ViewModels.AutoAssignmentViewModel.AssignmentType.PropertyAssignment)) {
                    ReportPath = HttpContext.Current.Server.MapPath(MvcApplication.ReportPath + "PropertyClaimDetailsPrint.rdlc");
                    IEnumerable<usp_GetPropertyClaimInfoPrint_Result> result = css.usp_GetPropertyClaimInfoPrint(AssignmentClaimID).ToList();
                    reportDataSource = new ReportDataSource("DataSet1", result);
                }
                else
                {
                    ReportPath = HttpContext.Current.Server.MapPath(MvcApplication.ReportPath + "ClaimDetailsPrint.rdlc");
                    IEnumerable<usp_GetClaimInfoPrint_Result> result = css.usp_GetClaimInfoPrint(AssignmentClaimID).ToList();
                    reportDataSource = new ReportDataSource("DataSet1", result);
                }
              
                localReport.ReportPath = ReportPath;
                

                localReport.DataSources.Add(reportDataSource);
                string reportType = "PDF";
                string mimeType;
                string encoding;
                string fileNameExtension;

                //The DeviceInfo settings should be changed based on the reportType
                //http://msdn.microsoft.com/en-us/library/ms155397.aspx

                Warning[] warnings;
                string[] streams;
              

                //Render the report
                renderedBytes = localReport.Render(
                    reportType,
                    null,
                    out mimeType,
                    out encoding,
                    out fileNameExtension,
                    out streams,
                    out warnings);
                mimeTypeOut = mimeType;
            }
            finally
            {
                localReport.Dispose();
                
            }
            return renderedBytes;
         }


        public static Size GetThumbnailSize(System.Drawing.Image original)
        {
            // Maximum size of any dimension.
            const int maxPixels = 140;

            // Width and height.
            int originalWidth = original.Width;
            int originalHeight = original.Height;

            // Compute best factor to scale entire image based on larger dimension.
            double factor;
            if (originalWidth > originalHeight)
            {
                factor = (double)maxPixels / originalWidth;
            }
            else
            {
                factor = (double)maxPixels / originalHeight;
            }

            // Return thumbnail size.
            return new Size((int)(originalWidth * factor), (int)(originalHeight * factor));
        }
       public static void ThumbnailImage(string inputPath, string outPutPath)
       {


           // Load image.
           Image image = Image.FromFile(inputPath);

           // Compute thumbnail size.
           Size thumbnailSize = GetThumbnailSize(image);

           // Get thumbnail.
           Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
               thumbnailSize.Height, null, IntPtr.Zero);

           // Save thumbnail.
           thumbnail.Save(outPutPath);
       }
       public static Decimal? isDecimal(object value)
       {
           Decimal RValue;
           if (value != null && Decimal.TryParse(value.ToString(), out RValue))
           {
               return RValue;
           }
           return null;
       }
       public static Int16? isInt16(object value)
       {
           Int16 RValue;
           if (value != null && Int16.TryParse(value.ToString(), out RValue))
           {
               return RValue;
           }
           return null;
       }
        #endregion
         
       public static void defineGridViewColumns(System.Web.UI.WebControls.GridView GV)
       {
           System.Web.UI.WebControls.BoundField bf1 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Claim Number",DataField="ClaimNumber" };
         
           System.Web.UI.WebControls.BoundField bf2 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Policy Number", DataField = "PolicyNumber" };
          
           System.Web.UI.WebControls.BoundField bf3 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Claim Name",DataField="ClaimName"};
          
           System.Web.UI.WebControls.BoundField bf4 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Claim City", DataField = "ClaimCity" };
           System.Web.UI.WebControls.BoundField bf5 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Claim State", DataField = "ClaimState" };
           System.Web.UI.WebControls.BoundField bf6 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Company Name", DataField = "Company_Name" };
           System.Web.UI.WebControls.BoundField bf7 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Client Contact", DataField = "Client_Contact" };
           System.Web.UI.WebControls.BoundField bf8 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Service Type", DataField = "Service_Type" };
           System.Web.UI.WebControls.BoundField bf9 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Service Provider", DataField = "Service_Provider" };
           System.Web.UI.WebControls.BoundField bf10 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Received Date", DataField = "Received_Date" };
           System.Web.UI.WebControls.BoundField bf11 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Closed Date", DataField = "Closed_Date" };
           System.Web.UI.WebControls.BoundField bf12 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Duration Days", DataField = "Duration_Days" };
           System.Web.UI.WebControls.BoundField bf13 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Estimate", DataField = "Estimate" };
           System.Web.UI.WebControls.BoundField bf14 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Status", DataField = "Status" };
           System.Web.UI.WebControls.BoundField bf15 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Invoice Amount", DataField = "InvoiceAmount" };

           bf1.ItemStyle.Width = 150;
           bf2.ItemStyle.Width = 150;
           bf3.ItemStyle.Width = 150;
           bf4.ItemStyle.Width = 150;
           bf5.ItemStyle.Width = 150;
           bf6.ItemStyle.Width = 150;
           bf7.ItemStyle.Width = 150;
           bf8.ItemStyle.Width = 150;
           bf9.ItemStyle.Width = 150;
           bf10.ItemStyle.Width = 150;
           bf11.ItemStyle.Width = 150;
           bf12.ItemStyle.Width = 150;
           bf13.ItemStyle.Width = 150;
           bf14.ItemStyle.Width = 150;
           bf15.ItemStyle.Width = 150;

           GV.Columns.Add(bf1);
           GV.Columns.Add(bf2);
           GV.Columns.Add(bf3);
           GV.Columns.Add(bf4);
           GV.Columns.Add(bf5);
           GV.Columns.Add(bf6);
           GV.Columns.Add(bf7);
           GV.Columns.Add(bf8);
           GV.Columns.Add(bf9);
           GV.Columns.Add(bf10);
           GV.Columns.Add(bf11);
           GV.Columns.Add(bf12);
           GV.Columns.Add(bf13);
           GV.Columns.Add(bf14);
           GV.Columns.Add(bf15);
                      
       }
       public static void NugenGridViewColumns(System.Web.UI.WebControls.GridView GV)
       {
           System.Web.UI.WebControls.BoundField bf1 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Claim Number", DataField = "ClaimNumber" };

           System.Web.UI.WebControls.BoundField bf2 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Company Name", DataField = "CompanyName" };

           System.Web.UI.WebControls.BoundField bf3 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Insured Name", DataField = "ReferrerName" };

           System.Web.UI.WebControls.BoundField bf4 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Type", DataField = "ReferrerType" };
           System.Web.UI.WebControls.BoundField bf5 = new System.Web.UI.WebControls.BoundField() { HeaderText = "IADA SP Name", DataField = "SPCompanyName" };
           System.Web.UI.WebControls.BoundField bf6 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Year", DataField = "Year" };
           System.Web.UI.WebControls.BoundField bf7 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Make", DataField = "Make" };
           System.Web.UI.WebControls.BoundField bf8 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Model", DataField = "AutoModel" };
           System.Web.UI.WebControls.BoundField bf9 = new System.Web.UI.WebControls.BoundField() { HeaderText = "DateOfLoss", DataField = "DateOfLoss" };
           System.Web.UI.WebControls.BoundField bf10 = new System.Web.UI.WebControls.BoundField() { HeaderText = "AssignDate", DataField = "AutoAssignmentDate" };
           System.Web.UI.WebControls.BoundField bf11 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Status", DataField = "StatusDescription" };
           System.Web.UI.WebControls.BoundField bf12 = new System.Web.UI.WebControls.BoundField() { HeaderText = "Invoice Amount", DataField = "GrandTotal" };
        

           bf1.ItemStyle.Width = 150;
           bf2.ItemStyle.Width = 150;
           bf3.ItemStyle.Width = 150;
           bf4.ItemStyle.Width = 150;
           bf5.ItemStyle.Width = 150;
           bf6.ItemStyle.Width = 150;
           bf7.ItemStyle.Width = 150;
           bf8.ItemStyle.Width = 150;
           bf9.ItemStyle.Width = 150;
           bf10.ItemStyle.Width = 150;
           bf11.ItemStyle.Width = 150;
           bf12.ItemStyle.Width = 150;
         

           GV.Columns.Add(bf1);
           GV.Columns.Add(bf2);
           GV.Columns.Add(bf3);
           GV.Columns.Add(bf4);
           GV.Columns.Add(bf5);
           GV.Columns.Add(bf6);
           GV.Columns.Add(bf7);
           GV.Columns.Add(bf8);
           GV.Columns.Add(bf9);
           GV.Columns.Add(bf10);
           GV.Columns.Add(bf11);
           GV.Columns.Add(bf12);
       

       }
    }
}
