﻿var SPRegistration = function () {
     
    //Service Provider Details
    var handleSPRegistration = function () {
        
        //Submit SP Registration 
        $("#frmRegistration").submit(function (event) {
            SPRegistration.ShowProgress();
            if (compare() == false) {
                SPRegistration.StopProgress();
                event.preventDefault();
            }

        });

        //Get data from server if Headcompany Dropdown Change
        $('#ddlHeadCompany').change(
        function () {
            debugger;
            var headcompanyid = $('#ddlHeadCompany option:selected').val();
            //alert(headcompanyid);
            if (headcompanyid != null || headcompanyid != 0) {
                SPRegistration.progressCallBack();
                $.post(HeadCompanyDetailsUrl,
                { HeadCompanyId: headcompanyid })
                .done(function (data) {
                    //    var comdetails = JSON.parse(data);
                    if (data != null) {
                        $("#txtBusinessDesc").val(data.BusinessDesc);
                        $("#imgheadcompany").attr("src", data.imagePath);
                        $("#txtAddress").val(data.StreetAddress);
                        $("#txtEmail").val(data.Email);
                        $("input[name='user.FirstName'").val(data.FirstName);
                        $("input[name='user.MiddleInitial'").val(data.MiddleInitial);
                        $("input[name='user.LastName'").val(data.LastName);
                        $("input[name='user.JobTitle'").val(data.JobTitle);
                        $("input[name='user.MobilePhone'").val(data.MobilePhone);
                        $("input[name='user.HomePhone'").val(data.HomePhone);
                        $("input[name='user.LinkedInAddress'").val(data.LinkedInAddress);
                        $("input[name='user.HeadCompany.Website'").val(data.WebsiteURL);
                        $("input[name='user.Company1.CompanyName'").val(data.CompanyName);
                        $("input[name='user.Company1.imagePath'").val(data.imagePath.split("/").pop());
                    }
                    SPRegistration.StopProgress();
                })
               .fail(function () {
                   SPRegistration.StopProgress();
               });
            }
        });
    }
   
    var startupScript = function () {
        if (ISDBA != null) {
            if (ISDBA != "") {
                if (ISDBA == 'False') {

                    $('#ddlFederalTaxClassification').empty();
                    $("#ddlFederalTaxClassification").append($("<option>").val('I').text('Individual/Sole Proprietor'));
                }
                if (ISDBA == 'True') {
                    $('#ddlFederalTaxClassification').empty();
                    $("#ddlFederalTaxClassification").append($("<option>").val('0').text('--Select--'));
                    $("#ddlFederalTaxClassification").append($("<option>").val('C').text('C Corporation'));
                    $("#ddlFederalTaxClassification").append($("<option>").val('S').text('S Corporation'));
                    $("#ddlFederalTaxClassification").append($("<option>").val('P').text('Partnership'));
                    $("#ddlFederalTaxClassification").append($("<option>").val('T').text('Trust/estate'));
                    $("#ddlFederalTaxClassification").append($("<option>").val('L').text('LLC'));



                    $("#ddlFederalTaxClassification option[value=" + federal + "]").attr("selected", "selected");
                }
            }
            else {
                $('#ddlFederalTaxClassification').empty();
                $("#ddlFederalTaxClassification").append($("<option>").val('I').text('Individual/Sole Proprietor'));
            }
        }
    }
    return {
        //main function to initiate the module
        init: function () {
            startupScript();
            handleSPRegistration();
      
        },


        //populate City And state based on ZipCode
        populateZipData: function (zipControl, stateControl, cityControl) {
            $.post(populateZipUrl,
           { ZipCode: $("#" + zipControl).val() },
           function (data) {
               if (data != -1) {

                   $("#" + stateControl).val(data.State);
                   $("#" + cityControl).val(data.City);
               }
               else {
                   $("#" + stateControl).val("");
                   $("#" + cityControl).val("");
               }

           });
        },
        //LLC Check
        Radio_ClickLLC: function (rdbNo, txtTobeDisabled, autoFocus) {
            debugger;
            var rdb = document.getElementById(rdbNo);
            var textBox = document.getElementById(txtTobeDisabled);

            $('#rdbIsDBAYes').click(function () {
                textBox.disabled = false;
                $('#hdnIsDBA').val("true");
                $('#ddlFederalTaxClassification').empty();

                $("#ddlFederalTaxClassification").append($("<option>").val('0').text('--Select--'));
                $("#ddlFederalTaxClassification").append($("<option>").val('C').text('C Corporation'));
                $("#ddlFederalTaxClassification").append($("<option>").val('S').text('S Corporation'));
                $("#ddlFederalTaxClassification").append($("<option>").val('P').text('Partnership'));
                $("#ddlFederalTaxClassification").append($("<option>").val('T').text('Trust/estate'));
                $("#ddlFederalTaxClassification").append($("<option>").val('L').text('LLC'));
            });
            $('#rdbIsDBANo').click(function () {
                $('#hdnIsDBA').val("false");
                $('#ddlFederalTaxClassification').empty();
                $('#txtLLCFirm').val("");
                textBox.disabled = rdb.checked;
                $("#ddlFederalTaxClassification").append($("<option>").val('I').text('Individual/Sole Proprietor'));
                document.getElementById("trLLCTaxClassification").style.display = "none";
                $("#ddlLLCTaxClassification").val("0");
            });

            if (autoFocus == true) {
                textBox.focus();
            }
        },

        //Check Password and Confirm Password
        compare:function() {
            //Perform validation when all the previous required fields have data else let the controller handle those validations.
            debugger;
            var valueToReturn = true;
            if ($("#txtFirstName").val().length != 0 && $("#txtLastName").val().length != 0 && $("#txtUserName").val().length != 0) {
                if ($("#tdConfirmPassword") != "") {
                    var pass = $('#txtPassword').val();
                    var confPass = $('#txtConfPassword').val();
                    var letters;
                    var numbers;
                    var alpha = /^[a-zA-Z]+$/; //PATTERN FOR ALPHABETS
                    var number = /^[0-9]+$/; //PATTERN FOR NUMBERS
                    if ($('#txtPassword').val() != $('#txtConfPassword').val()) {
                        passwordNotMatch("Passwords do not match.");
                        valueToReturn = false;
                    }
                    else if (pass.length < 6) {
                        passwordNotMatch("Password should be at least 6 characters.");
                        valueToReturn = false;
                    }
                    for (i = 0; i < pass.length; i++) {
                        if (pass.substr(i, 1).match(alpha)) {
                            letters = true; //AT LEAST ONE LETTER EXISTS
                        }
                        else if (pass.substr(i, 1).match(number)) {
                            numbers = true; //AT LEAST ONE NUMBER EXISTS
                        }
                    }
                    //IF BOTH LETTERS AND NUMBERS ARE PRESENT...
                    if (letters == true && numbers == true) {
                    }
                    else {
                        passwordNotMatch("Password should have at least one alphabet and one numeric character.");
                        valueToReturn = false;
                    }
                }
            }
            return valueToReturn;
        },

        //Spinner

          ShowProgress:function() {
              setTimeout(function () {
                  var modal = $('<div />');
                  modal.addClass("waiting-box");
                  $('body').append(modal);
                  var loading = $(".loading");
                  loading.show();
                  var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                  var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                  loading.css({ top: top, left: left });
              }, 200);
          },
                
           progressCallBack:function() {
               SPRegistration.ShowProgress();
                 },
           StopProgressCallBack: function() {
               SPRegistration.StopProgress();
                 },
           StopProgress:function() {
                     setTimeout(function () {
                         //var modal = $('<div />');
                         ////modal.addClass("waiting-box");
                         //$('body').removeClass("waiting-box");
                         var loading = $(".loading");
                         var waiting = $(".waiting-box");
                         //loading.show();
                         loading.css({ 'display': 'none' });
                         waiting.css({ 'display': 'none' });
                     }, 200);
               },
        

      //Internal Contact
         CloseAllPopup:function() {
             debugger;
             $("#CreateInternalContactPopup").modal("hide");
             $("#InternalContactListPopup").modal("hide");
         },
        
         InternalContactValidation:function() {
             debugger;
             $('#internalContactErrorList').html('');
             var valueToReturn = true;
             if ($("#txtIntContFirstName").val().length != 0 && $("#txtIntContLastName").val().length != 0 && $("#txtIntContEmail").val().length != 0 && $("#txtIntContMobile").val().length != 0) {
                 valueToReturn = true;
             }
             else {
                 $('#internalContactErrorList').html("Please enter the First Name, Last Name, Mobile Number and Email.");
        
                 valueToReturn = false;
             }
             return valueToReturn;
         },
        
        
         InternalContactReject:function(UserID) {
             debugger;
             $.ajax({
                 url: InternalContactRejectUrl
                 , type: 'POST'
                 , data: { UserID: UserID }
                 , success: function (result) {
                     if (result == 1) {
                         alert("Internal Contact is Used");
                     }
                     else {
                         alert("Rejected Successfully");
                         var CompanyID = $("#hidCompanyID").val();
                         $("#InternalContactListPopup").modal("hide");
                         SPRegistration.InternalContactList(CompanyID);
                     }
                 }
             });
         },
        
         SaveInternalContact:function(UserID) {
             $('#internalContactErrorList').html('');
             if (SPRegistration.InternalContactValidation()) {
                 var CompanyID = $("#hdnHeadCompanyId").val();
                 $("#frmCreateInternalContact").serialize();
                 debugger;
                 $.ajax({
                     url: SaveInternalContact,
                     type: 'POST',
                     data: $('#frmCreateInternalContact').serialize(),
                     success: function (result) {
                         debugger;
                         if (result.success == "1") {
                             $("#CreateInternalContactPopup").modal("hide");
                             $("#InternalContactListPopup").modal("hide");
                             SPRegistration.InternalContactList(CompanyID);
                         }
                         else if (result.success == "0") {
                             $('#internalContactErrorList').html(result.Errorlist);
                         }
                     }
                 });
             }
         },
        
         CreateInternalContactPopup:function(UserID) {
             debugger;
             var CompanyID = $("#hidCompanyID").val();
              
             $('#internalContactErrorList').html('');
             $.post(CreateInternalContactPopup,
                 { UserID: UserID },
             function (data) {
                 debugger;
                 $("#divInternalContactSave").empty().append(data);
                 $("#CreateInternalContactPopup").modal("show");
                 $("#hdnHeadCompanyId").val(CompanyID);
             });
         },
        
        
         InternalContactList:function(CompanyID) {
        
             debugger;
             $.post(InternalContactList,
             { CompanyID: CompanyID },
             function (data) {
                 debugger;
                 $("#divInternalContactList").empty().append(data);
                 $("#InternalContactListPopup").modal("show");
                 $("#hidCompanyID").val(CompanyID);
             });
         },

        // Orther Details Settings
          HasEstSystemChanged:function(ESTtype) {
              if (ESTtype == "AudatexEST") {
                  $("#txtAudatexEST").prop("disabled", false);
                  $("#txtMitchellEST").val("");
                  $("#txtMitchellEST").prop("disabled", true);
                  $("#txtCCCEST").val("");
                  $("#txtCCCEST").prop("disabled", true);
                  $("#txtOtherEST").val("");
                  $("#txtOtherEST").prop("disabled", true);
                  $("#txtOtherDesc").val("");
                  $("#txtOtherDesc").prop("disabled", true);
              }
              else if (ESTtype == "MitchellEST") {
                  $("#txtAudatexEST").val("");
                  $("#txtAudatexEST").prop("disabled", true);
                  $("#txtMitchellEST").prop("disabled", false);
                  $("#txtCCCEST").val("");
                  $("#txtCCCEST").prop("disabled", true);
                  $("#txtOtherEST").val("");
                  $("#txtOtherEST").prop("disabled", true);
                  $("#txtOtherDesc").val("");
                  $("#txtOtherDesc").prop("disabled", true);
              }
              else if (ESTtype == "CCCEST") {
                  $("#txtAudatexEST").val("");
                  $("#txtAudatexEST").prop("disabled", true);
                  $("#txtMitchellEST").val("");
                  $("#txtMitchellEST").prop("disabled", true);
                  $("#txtCCCEST").prop("disabled", false);
                  $("#txtOtherEST").val("");
                  $("#txtOtherEST").prop("disabled", true);
                  $("#txtOtherDesc").val("");
                  $("#txtOtherDesc").prop("disabled", true);
              }
              else if (ESTtype == "OtherEST") {
                  $("#txtAudatexEST").val("");
                  $("#txtAudatexEST").prop("disabled", true);
                  $("#txtMitchellEST").val("");
                  $("#txtMitchellEST").prop("disabled", true);
                  $("#txtCCCEST").val("");
                  $("#txtCCCEST").prop("disabled", true);
                  $("#txtOtherEST").prop("disabled", false);
                  $("#txtOtherDesc").prop("disabled", false);
              }
          },
          OfficeReviewCompletedChange:function(isDisable) {
              if (isDisable == 1) {
                  $("#txtReviewedPercentage").prop("disabled", false);
              }
              else {
                  $("#txtReviewedPercentage").val("");
                  $("#txtReviewedPercentage").prop("disabled", true);
              }
          }
    };
}();

jQuery(document).ready(function () {
   
    SPRegistration.init();
});