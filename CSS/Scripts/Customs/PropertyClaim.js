﻿
function addCoverageRow(tableID) {
    var table = document.getElementById(tableID);
    var chkSingledeductible = document.getElementById("chkSingledeductible");

    var rowCount = table.rows.length;
    var newRowIndex = rowCount - 1;
    var row = table.insertRow(rowCount);
    row.vAlign = "top";
    var hdnRowCount = document.getElementById("hdnCoveragesRowCount");
    hdnRowCount.value = parseInt(hdnRowCount.value) + 1;

    //CoverageName
    var cell1 = row.insertCell(0);
    var element1 = document.createElement("input");
    element1.type = "text";
    element1.name = "Claim.ClaimCoverages[" + newRowIndex + "].CoverageName";
    cell1.appendChild(element1);

    //CoverageType
    var cell2 = row.insertCell(1);
    var element2 = document.getElementById("ddlCoverageTypesReplica").cloneNode(true);
    element2.name = "Claim.ClaimCoverages[" + newRowIndex + "].CoverageType";
    cell2.appendChild(element2);

    //CoverageLimit
    var cell3 = row.insertCell(2);
    var element3 = document.createElement("input");
    element3.type = "text";
    element3.name = "Claim.ClaimCoverages[" + newRowIndex + "].CoverageLimit";
    element3.style.width = "100px";

    cell3.appendChild(element3);

    //CoverageDeductible
    var cell4 = row.insertCell(3);
    var element4 = document.createElement("input");
    element4.type = "text";
    element4.className = "deductible";
    element4.style.width = "100px";

    if (chkSingledeductible.checked == true && rowCount > 1) {
        element4.disabled = true;
        element4.value = "0";
    }

    element4.name = "Claim.ClaimCoverages[" + newRowIndex + "].CoverageDeductible";
    cell4.appendChild(element4);

    //COV_Reserves
    var cell5 = row.insertCell(4);
    var element5 = document.createElement("input");
    element5.type = "text";
    element5.style.width = "100px";
    element5.name = "Claim.ClaimCoverages[" + newRowIndex + "].COV_Reserves";
    cell5.appendChild(element5);

    //ExpenseReserve
    var cell6 = row.insertCell(5);
    var element6 = document.createElement("input");
    element6.type = "text";
    element6.style.width = "100px";

    element6.name = "Claim.ClaimCoverages[" + newRowIndex + "].ExpenseReserve";
    cell6.appendChild(element6);

    //Delete
    var cell7 = row.insertCell(6);
    var element7 = document.getElementById("imgCoverageDeleteRowReplica").cloneNode(true);
    element7.id = "imgCoverageDeleteRow" + newRowIndex;
    cell7.appendChild(element7);
    if ((rowCount - 2) >= 0) {
        var deleteRowIcon = document.getElementById("imgCoverageDeleteRow" + (rowCount - 2));
        deleteRowIcon.style.display = "none";
    }

}
function deleteRow(tableID) {
    try {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        var hdnRowCount = document.getElementById("hdnCoveragesRowCount");
        hdnRowCount.value = parseInt(hdnRowCount.value) - 1;
        table.deleteRow(rowCount - 1);
        if ((rowCount - 3) >= 0) {
            var deleteRowIcon = document.getElementById("imgCoverageDeleteRow" + (rowCount - 3));
            deleteRowIcon.style.display = "inline";
        }


    } catch (e) {
        alert(e);
    }
}
function addMortgageRow(tableID) {
    var table = document.getElementById(tableID);

    var rowCount = table.rows.length;
    var newRowIndex = rowCount - 1;
    var row = table.insertRow(rowCount);
    row.vAlign = "top";
    var hdnRowCount = document.getElementById("hdnMortgagesRowCount");
    hdnRowCount.value = parseInt(hdnRowCount.value) + 1;

    //Mortgage Holder
    var cell1 = row.insertCell(0);
    var element1 = document.createElement("input");
    element1.type = "text";
    element1.name = "Claim.ClaimMortgages[" + newRowIndex + "].MortgageHolder";
    element1.style.width = "300px";
    cell1.appendChild(element1);

    //Loan Number
    var cell2 = row.insertCell(1);
    var element2 = document.createElement("input");
    element2.name = "Claim.ClaimMortgages[" + newRowIndex + "].LoanNumber";
    element2.style.width = "200px";
    cell2.appendChild(element2);

    //Delete
    var cell3 = row.insertCell(2);
    var element3 = document.getElementById("imgMortgageDeleteRowReplica").cloneNode(true);
    element3.id = "imgMortgageDeleteRow" + newRowIndex;
    cell3.appendChild(element3);
    if ((rowCount - 2) >= 0) {
        var deleteRowIcon = document.getElementById("imgMortgageDeleteRow" + (rowCount - 2));
        deleteRowIcon.style.display = "none";
    }

}

function deleteMortgageRow(tableID) {
    try {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        var hdnRowCount = document.getElementById("hdnMortgagesRowCount");
        hdnRowCount.value = parseInt(hdnRowCount.value) - 1;
        table.deleteRow(rowCount - 1);
        if ((rowCount - 3) >= 0) {
            var deleteRowIcon = document.getElementById("imgMortgageDeleteRow" + (rowCount - 3));
            deleteRowIcon.style.display = "inline";
        }

    } catch (e) {
        alert(e);
    }
}

function disableDeductible(tableID) {

    var table = document.getElementById(tableID);
    var chkSingledeductible = document.getElementById("chkSingledeductible");

    var rowCount = table.rows.length;

    if (chkSingledeductible.checked == true && rowCount > 1) {
        for (var i = 0; i < rowCount; i++) {
            $('.deductible').attr("disabled", "disabled");
            $('.deductible').val("0");
        }
    }
    else {
        $('.deductible').removeAttr("disabled");
    }

}



function copyInsuredToLossOnBlur() {
    if (document.getElementById('cbCopyInsuredToLoss').checked) {
        copyInsuredToLoss();
    }
}




function copyInsuredToLoss() {
    try {
        document.getElementById('txtInsuredAddress1').value = document.getElementById('txtPropertyAddress1').value;
        document.getElementById('txtInsuredAddress2').value = document.getElementById('txtPropertyAddress2').value;
        document.getElementById('txtInsuredZip').value = document.getElementById('txtPropertyZip').value;
        document.getElementById('ddlInsuredState').value = document.getElementById('ddlPropertyState').value;
        document.getElementById('txtInsuredCity').value = document.getElementById('txtPropertyCity').value;
    }
    catch (err) {
    }
}


function GetLineOfBussinessList() {
    var selectedCategoryId = $('#ddlInsuranceCompany').val();
    $("#imgLOBListLoading").show();
    $('#ddlLineOfBussiness').empty();
    $('#ddlLineOfBussiness').append($('<option/>').attr('value', "0").text("--Select--"));
    $.getJSON(GetLineOfBussinessListUrl,
               { companyId: selectedCategoryId }).
               done(
               function (subcategories) {
                   var subsSelect = $('#ddlLineOfBussiness');
                   $('#ddlLineOfBussiness').empty();
                   $.each(subcategories, function (index, subcat) {
                       subsSelect.append(
                           $('<option/>')
                               .attr('value', subcat.Value)
                               .text(subcat.Text)
                       );
                   });
                   $("#imgLOBListLoading").hide();
               })
    .fail(function (jqxhr, textStatus, error) {
        $("#imgLOBListLoading").hide();
    });
    
}

function populateClientPOCDLL(SelectClientPOCFromList) {
    var selectedCategoryId = $('#ddlInsuranceCompany').val();
    $("#imgClientPOCListLoading").show();
    $('#ddlLineOfBussiness').empty();
    $('#ddlLineOfBussiness').append($('<option/>').attr('value', "0").text("--Select--"));
    $.getJSON(GetClientPointOfContactListUrl,
            { companyId: selectedCategoryId },
            function (subcategories) {
                var subsSelect = $('#ddlClientPointOfContact');
                subsSelect.empty();
                $.each(subcategories, function (index, subcat) {
                    subsSelect.append(
                        $('<option/>')
                            .attr('value', subcat.Value)
                            .text(subcat.Text)
                    );
                });

                try {
                    $('#ddlClientPointOfContact').val(SelectClientPOCFromList);
                }
                catch (e) {

                }
                $("#imgClientPOCListLoading").hide();
            });
}





function loadPopupNewCatCode() {

    $("#modalCatCodeEdit").modal("show");
    return false;
}

function createNewClaimContact(event) {
    event.preventDefault();
    var txtCatCode = $('#txtCatCode').val();
    var txtcatCodeDescription = $("#txtcatCodeDescription").val();

    var isValid = true;
    var isValidErrorMsg = '';

    if (txtCatCode.length == 0) {
        isValid = false;
        isValidErrorMsg += "Cat Code is required.<br/>";
    }

    if (txtcatCodeDescription.length == 0) {
        isValid = false;
        isValidErrorMsg += "Description is required.<br/>";
    }

    if (isValid) {
        $("#imgProcessingNewCatCodeSubmit").show();


        $.getJSON(PropertyAssignmentAddCatCodeUrl,
       {
           catcode: txtCatCode, Description: txtcatCodeDescription
       },
        function (data) {
            if (data == 1) {
                $("#modalCatCodeEdit").modal("hide");

                populateCatCodesDDL(txtCatCode);
            }
            else if (data == 2) {
                //CatCode Already Exists
                $("#imgProcessingNewCatCodeSubmit").hide();
                document.getElementById("divCatCodeError").innerHTML = "Cat Code already exists.";
            }
        });
    }
    else {
        document.getElementById("divCatCodeError").innerHTML = isValidErrorMsg;
    }

}
function createNewClientPOC() {

    var hdnHeadCompanyId = $('#hdnHeadCompanyId').val();
    var txtFirstName = $('#txtFirstName').val();
    var txtMiddleInitial = $('#txtMiddleInitial').val();
    var txtLastName = $("#txtLastName").val();
    var txtEmail = $("#txtEmail").val();

    var isValid = true;
    var isValidErrorMsg = '';

    if (txtFirstName.length == 0) {
        isValid = false;
        isValidErrorMsg += "First Name is required.<br/>";
    }

    if (txtLastName.length == 0) {
        isValid = false;
        isValidErrorMsg += "Last Name is required.<br/>";
    }

    if (txtEmail.length == 0) {
        isValid = false;
        isValidErrorMsg += "Email Address is required.<br/>";
    }


    if (isValid) {
        $("#imgProcessingNewClientPOCSubmit").show();


        $.getJSON(PropertyAssignmentAddClientPOCUrl,
      {
          headCompanyId: hdnHeadCompanyId, firstName: txtFirstName, middleInitial: txtMiddleInitial, lastName: txtLastName, email: txtEmail
      },
       function (data) {
           if (data > 0) {
               $("#imgProcessingNewClientPOCSubmit").hide();
               disablePopupNewClientPOC();
               populateClientPOCDLL(data);
           }
           else if (data == -2) {
               //User Name Already Exists
               $("#imgProcessingNewClientPOCSubmit").hide();
               document.getElementById("divClientPOCError").innerHTML = "User Name already exists.";
           }
           else if (data == -3) {
               //Email Already Exists
               $("#imgProcessingNewClientPOCSubmit").hide();
               document.getElementById("divClientPOCError").innerHTML = "Email already exists.";
           }
       });
    }
    else {
        document.getElementById("divClientPOCError").innerHTML = isValidErrorMsg;
    }

}
function populateCatCodesDDL(SelectCatCodeFromList) {
    $(function () {


        $.getJSON(GetCatCodesListUrl,
                function (subcategories) {
                    var subsSelect = $('#ddlCatCodesList');
                    subsSelect.empty();
                    $.each(subcategories, function (index, subcat) {
                        subsSelect.append(
                            $('<option/>')
                                .attr('value', subcat.Value)
                                .text(subcat.Text)
                        );

                    });
                    //alert('External Participants List Loaded.');
                    $("#ddlCatCodesList").val(SelectCatCodeFromList);
                });

    });

}
function populateCSSPOCDDL(SelectCSSPOCFromList) {
    $(function () {


        $.getJSON(GetCSSPOCListUrl,
        function (subcategories) {
            var subsSelect = $('#ddlCSSPOC');
            subsSelect.empty();
            $.each(subcategories, function (index, subcat) {
                subsSelect.append(
                    $('<option/>')
                        .attr('value', subcat.Value)
                        .text(subcat.Text)
                );

            });
            //alert('External Participants List Loaded.');
            $("#ddlCSSPOC").val(SelectCSSPOCFromList);
        });

    });

}


function populateZipData(zipControl, stateControl, cityControl) {
 
    try {
        $.post(GetZipCodeInfoUrl,
    { ZipCode: $("#" + zipControl).val() },
    function (data) {
        if (data != -1) {

            $("#" + stateControl).val(data.State);
            $("#" + cityControl).val(data.City);
        }
        else {
            $("#" + stateControl).val("");
            $("#" + cityControl).val("");
        }

    });
    copyInsuredToLossOnBlur();
} catch (err) {
}
}



function GetLineOfBussinessList() {
    var selectedCategoryId = $('#ddlInsuranceCompany').val();

    $("#imgLOBListLoading").show();
    $('#ddlLOB').empty();
    $('#ddlLOB').append($('<option/>').attr('value', "0").text("--Select--"));
    $.getJSON(GetLOBListUrl,
               { companyId: selectedCategoryId })
        .done(
               function (subcategories) {
                   var subsSelect = $('#ddlLOB');
                   $('#ddlLOB').empty();
                   $.each(subcategories, function (index, subcat) {
                       subsSelect.append(
                           $('<option/>')
                               .attr('value', subcat.Value)
                               .text(subcat.Text)
                       );
                   });
                   $("#imgLOBListLoading").hide();
               })
      .fail(function (jqxhr, textStatus, error) {
          $("#imgLOBListLoading").hide();
      });

}

function populateClientPOCDLL(SelectClientPOCFromList) {
    var selectedCategoryId = $('#ddlInsuranceCompany').val();

    $("#imgClientPOCListLoading").show();
    //$('#ddlLOB').empty();
    //$('#ddlLOB').append($('<option/>').attr('value', "0").text("--Select--"));
    $.getJSON(GetClientPointOfContactListUrl,
            { companyId: selectedCategoryId },
            function (subcategories) {
                var subsSelect = $('#ddlClientPointOfContact');
                subsSelect.empty();
                $.each(subcategories, function (index, subcat) {
                    subsSelect.append(
                        $('<option/>')
                            .attr('value', subcat.Value)
                            .text(subcat.Text)
                    );
                });

                try {
                    $('#ddlClientPointOfContact').val(SelectClientPOCFromList);
                }
                catch (e) {

                }
                $("#imgClientPOCListLoading").hide();
            });
}



//POC Insert /Update 


function btnNewCPOC_Click() {
    if ($("#ddlInsuranceCompany").val() != "0") {
        $("#spnCPOCHeading").html("Add ");
        $("#btnSaveCPOC").val("Add ");

        $("#imgClientPOCListLoading").show();
        $.ajax({
            url: ClientPOCEditUrl
    , type: 'GET'
        , data: { UserId: 0 }
        , cache: false
        }).done(function (result) {
            $("#divClientPOC").empty();
            $("#divClientPOC").html(result);
            $("#hdnCPOCHeadCompanyId").val($("#ddlInsuranceCompany").val());
            $("#modalClientPOCDetailEdit").modal("show");
            $("#imgClientPOCListLoading").hide();
        }).fail(function (result) {
        });
    }
    else {
        alert("Select an Insurance Company.");
    }
    return false;
}

function btnSaveCPOC_Click(event) {
    event.preventDefault();
    $("#modalClientPOCDetailEdit").modal("hide");
    $('body').removeClass().removeAttr('style'); $('.modal-backdrop').remove();
    var data = $("#frmCPOC").serialize();

    $("#imgProcessingNewClientPOCSubmit").show();
    $.ajax({
        url: ClientPOCEditSaveUrl
      , type: 'POST'
      , data: data
    }).done(function (result) {

        if (result.Code == "1") {
         
            $("#divClientPOC").empty();
            $("#imgProcessingNewClientPOCSubmit").hide();
       
            populateClientPOCDLL(result.Data);
        }
        else if (result.Code == "-1") {
         
            $("#divClientPOC").empty();
            $("#divClientPOC").html(result.Data);
            $("#modalClientPOCDetailEdit").modal("show");
            $("#imgProcessingNewClientPOCSubmit").hide();
        }
    }).fail(function (result) {
        //alert(result.responseText);
    });
}



$(document).ready(function () {
    $('div').on("change",'#ddlInsuranceCompany',function () {
        populateClientPOCDLL('0');
        GetLineOfBussinessList();
    });

   

    $('div').on("blur",'#txtPolicynumber',function () {
  
        var policynumber = $(this).val();
        $.getJSON(checkPolicyIDUrl,
        { policynumber: policynumber },
        function (result) {
            if (result > 0) {
                alert('Warning: This Policy Number already exists');
                //$('#txtPolicynumber').val('');
                return false;
            }
        });
    });



    //  $("#txtPropertyContactPhone").mask("(999) 999-9999");
    $('div').on("keydown", '#txtPropertyZip', function (event) {

        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });

    //  $("#HomePhone").mask("(999) 999-9999");
    //   $("#BusinessPhone").mask("(999) 999-9999");
    //   $("#MobilePhone").mask("(999) 999-9999");
    $('div').on("keydown", '#txtInsuredZip', function (event) {
    
        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });

   
    $('div').on("blur",'#txtClaimNumber',function () {
   
        var claimNumber = $(this).val();

       
        $.getJSON(checkClaimIDUrl,
        { ClaimNumber: claimNumber },
        function (result) {
            if (result > 0) {
                alert('Warning: This Claim Number already exists');
                //$('#txtClaimNumber').val('');
                return false;
            }
        });
    });
})


function SubmitPropertyAssignment(event) {
    event.preventDefault();
    var sss = $("#CAutoAssignmentID").val();
    $("#selectedassignmentID").val($("#CAutoAssignmentID").val())
    $('.waiting-box1').css("display", "block");
    $.ajax({
        url: PropertyAssignmentSubmitUrl
    , type: 'POST'
    , data: $("form").serialize()
    }).done(function (data) {
        $('.waiting-box1').css("display", "none");
        if (data.id == 1) {
            window.location = AutoSearchUrl;
        }
        else {
            var Data1 = "<ul>"
            for (i = 0; i < data.errorList.length; i++) {

                var Error = data.errorList[i].Message;
                Data1 = Data1 + "<li>" + Error + "</li>"
            }
            Data1 = Data1 + "</ul>"
            $("#Errorlog").html(Data1);
            $("#ErrorPopup").modal("show");
        }
    }).fail(function (result) {
        $('.waiting-box1').css("display", "none");
    });


}
