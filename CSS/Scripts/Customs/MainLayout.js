﻿var MainLayout = function () {

    var handleMainLayout = function () {
        if (contrName != undefined && action != undefined) {

            var activeTab = "";
            $(".page-sidebar-menu li").removeClass('active open');
            if (contrName.toLowerCase() == "home" && action.toLowerCase() == "index") {
                activeTab = 'liHome';
                $('#liHome').addClass('active open');
            }

            else if (contrName.toLowerCase() == "spmapsearch" && action.toLowerCase() == "search" && para != '') {
                activeTab = 'liFavouriteSP';
                $('#liFavouriteSP').addClass('active open');
            }
            else if (contrName.toLowerCase() == "spmapsearch" && action.toLowerCase() == "search") {
                activeTab = 'liSearchAssign';
                $('#liSearchAssign').addClass('active open');
            }
            else if (contrName.toLowerCase() == "autoassignment" && action.toLowerCase() == "autosearch" && para != '') {
                activeTab = 'liQAReview';
                $('#liQAReview').addClass('active open');

            }
            else if (contrName.toLowerCase() == "autoassignment" && action.toLowerCase() == "autosearch") {
                activeTab = 'liClaimManagement';
                $('#liClaimManagement').addClass('active open');
            }
            else if (contrName.toLowerCase() == "serviceproviders" && action.toLowerCase() == "search") {
                activeTab = 'liAdmin';
                $('#liAdmin').addClass('active open');
            }
            else if (contrName.toLowerCase() == "insurancesetup" && action.toLowerCase() == "companieslist") {
                activeTab = 'liAdmin';
                $('#liAdmin').addClass('active open');
            }
        }

        //var urlString = document.referrer;
        //var isLoginPage = urlString.includes("Login");

        var cookieval = sessionStorage.getItem('Linkidname');
        if (activeTab != "") {
            MainLayout.StoreLinkID(activeTab);
        }
        if (!$(".page-sidebar-menu li").hasClass('active open')) {
            if (cookieval != null) {
                $("#" + cookieval).addClass('active open');
            }

        }
    }

    return {
        //main function to initiate the module
        init: function () {
            handleMainLayout();
        },

        //Spinner

        ShowProgress: function () {
            setTimeout(function () {
                var modal = $('<div />');
                modal.addClass("waiting-box");
                $('body').append(modal);
                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        },

        progressCallBack: function () {
            SPRegistration.ShowProgress();
        },
        StopProgressCallBack: function () {
            SPRegistration.StopProgress();
        },
        StopProgress: function () {
            setTimeout(function () {
                //var modal = $('<div />');
                ////modal.addClass("waiting-box");
                //$('body').removeClass("waiting-box");
                var loading = $(".loading");
                var waiting = $(".waiting-box");
                //loading.show();
                loading.css({ 'display': 'none' });
                waiting.css({ 'display': 'none' });
            }, 200);
        },

        HighlightLink:function(Linkid) {
            $(".page-sidebar-menu li").removeClass('active open');
            $('#' + Linkid).addClass('active open');
            MainLayout.StoreLinkID(Linkid);
        },
     
        ClearCookie:function() {
            //document.cookie = 'Linkidname' + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            sessionStorage.removeItem("Linkidname");
        },
     
        StoreLinkID:function(LinkID) {
            MainLayout.ClearCookie();
            sessionStorage.setItem('Linkidname', LinkID);
     
        },

        NotificationPopup:function() {
            debugger;
            $.ajax({
                url: NotificationPopupUrl
                    , type: 'POST'
                , success: function (result) {
                    debugger;
                    $("#FormNotification").empty().html(result);
                    $("#divNotification").modal('show');
                }
            });
      
        },
        ShowNotification:function(AutoNoteID1) {
            debugger;
            $.ajax({
                url: ShowNotificationUrl
                , type: 'POST'
                , data: { AutoNoteID: AutoNoteID1 }
            , success: function (result) {
                debugger;
                $("#FormNotificationView").empty().html(result);
                $("#divNotification").modal('hide');
                $("#divNotificationView").modal('show');
            }
            });
        },
      
      
        CloseNotification:function() {
            debugger;
            var AutoNoteID1 = $("#hidAutoNoteID").val();
            $.ajax({
                url: CloseNotificationUrl
                    , type: 'POST'
                , data: { AutoNoteID: AutoNoteID1 }
                , success: function (result) {
                    if (result == 1) {
                        debugger;
                        $("#divNotificationView").modal('hide');
                        location.reload();
                        MainLayout.NotificationPopup();
      
                    }
                }
            });
      
        },
        ManageServiceRate: function (SPID) {
            MainLayout.ShowProgress();
            debugger;
            loggedInUser = SPID;
            $.ajax({
                url: ManageServiceRateUrl
                , type: 'POST'
                , data: { UserID: SPID }
            , success: function (result) {
                debugger;
                $("#divManageServiceRate").empty().html(result);
                MainLayout.StopProgress();
                $("#ManageServiceRatePopup").modal('show');
                var ngElement = document.getElementById("ManageServiceRatePopup");
                angular.bootstrap(ngElement, ['CSSMVCApp']);
            }, error: function (jqXHR, exception) {
                MainLayout.StopProgress();
            }

            });
        }
    };

}();

jQuery(document).ready(function () {
    //Init Action
    MainLayout.init();
});