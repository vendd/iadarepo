﻿
define(['angularAMD', 'angular-route', 'ui-bootstrap', 'angular-sanitize', 'blockUI', 'angular-animate'], function (angularAMD) {
    var app = angular.module("CSSMVCApp", ['ngRoute', 'ui.bootstrap', 'ngSanitize', 'blockUI', 'ngAnimate']);


    //app.config(function (blockUIConfigProvider) {

    //    // Change the default overlay message
    //    blockUIConfigProvider.message("Loading...");
    //    // Change the default delay to 100ms before the blocking is visible
    //    blockUIConfigProvider.delay(1);
    //    // Disable automatically blocking of the user interface
    //    blockUIConfigProvider.autoBlock(false);

    //});

    
    app.config(['$routeProvider', '$locationProvider', '$sceDelegateProvider', function ($routeProvider, $locationProvider, $sceDelegateProvider) {
    
        var baseSiteUrlPath = $("base").first().attr("href");

    
        
    
        $routeProvider.
              when("/", angularAMD.route({
                         
                  templateUrl: function (rp) { return 'Views/SPMapSearch/Search.html'; },
                  controllerUrl: "Views/SPMapSearch/SearchController"

              }))
      
             .when("/:section/:tree", angularAMD.route({

                 templateUrl: function (rp) { return baseSiteUrlPath + 'views/' + rp.section + '/' + rp.tree + '.html'; },

                 resolve: {

                     load: ['$q', '$rootScope', '$location', function ($q, $rootScope, $location) {

                         var path = $location.path();
                         var parsePath = path.split("/");
                         var parentPath = parsePath[1];
                         var controllerName = parsePath[2];

                         var loadController = "Views/" + parentPath + "/" + controllerName + "Controller";

                         var deferred = $q.defer();
                         require([loadController], function () {
                             $rootScope.$apply(function () {
                                 deferred.resolve();
                             });
                         });
                         return deferred.promise;
                     }]
                 }

             }))

            .when("/:section/:tree/:id", angularAMD.route({

                templateUrl: function (rp) { return baseSiteUrlPath + 'views/' + rp.section + '/' + rp.tree + '.html'; },

                resolve: {

                    load: ['$q', '$rootScope', '$location', function ($q, $rootScope, $location) {

                        var path = $location.path();
                        var parsePath = path.split("/");
                        var parentPath = parsePath[1];
                        var controllerName = parsePath[2];

                        var loadController = "Views/" + parentPath + "/" + controllerName + "Controller";

                        var deferred = $q.defer();
                        require([loadController], function () {
                            $rootScope.$apply(function () {
                                deferred.resolve();
                            });
                        });
                        return deferred.promise;
                    }]
                }

            }))

            .otherwise({ redirectTo: '/' })

        $locationProvider.html5Mode(true);

    }]);

    var indexController = function ($scope, $rootScope, $http, $location, blockUI) {

        $scope.initializeController = function () {
         
        }
    };
    indexController.$inject = ['$scope', '$rootScope', '$http', '$location', 'blockUI'];
    app.controller("indexController", indexController);

    // Bootstrap Angular when DOM is ready
    angularAMD.bootstrap(app);


    return app;

})