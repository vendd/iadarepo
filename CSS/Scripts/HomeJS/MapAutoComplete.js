﻿function initMap() {
  
    var input = /** @type {!HTMLInputElement} */(
        document.getElementById('txtGeoloc'));
    var options = {
            types: []
    };
  
    var autocomplete = new google.maps.places.Autocomplete(input, options);
  

    autocomplete.addListener('place_changed', function () {
           
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
        }

    var address = '';
          if (place.address_components) {
          
            var addressComponents = place.address_components;
          
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
           

                var typeVal = '';
                var value = place.address_components[i];
                    switch (addressType) {
                        case "locality": // city
                            document.getElementById('plLocality').value = value.long_name;
                            break;
                        case "administrative_area_level_1": // state
                            document.getElementById('plAdministrative_area_level_1').value = value.long_name;
                            document.getElementById('plSHadministrative_area_level_1').value = value.short_name;
                            break;

                        case "administrative_area_level_2": // state
                            document.getElementById('pladministrative_area_level_2').value = value.long_name;
                            break;
                        case "country": // country
                            document.getElementById('plcountry').value = value.long_name;
                            document.getElementById('plSHcountry').value = value.short_name;
                            break;
                        case "postal_code": // postal_code
                            document.getElementById('plpostal_code').value = value.long_name;
                            break;
                        default:
                            typeVal = '';
                  
                }
            }
          
          }
        
         
    });

}