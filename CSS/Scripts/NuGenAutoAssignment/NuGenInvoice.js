﻿var app = angular.module("NuGenInvoice", []);
app.controller('NuGenInvoiceController', function ($scope) {
    $scope.ngbaseServiceFee;
    $scope.ngTotalNOHours;
    $scope.ngHourlyRate;
    $scope.ngTotalNOHoursCalculated;
    $scope.ngTotalNOMiles;
    $scope.ngFreeMiles;
    $scope.ngDollarPerMile;
    $scope.ngTotalNOMilesCalculated;
    $scope.ngTotalLossFee;
    $scope.ngTotalInvoiceAmountCalculated;
    $scope.ngTotalInvoiceAmountCalculated = $scope.ngbaseServiceFee + $scope.ngTotalNOHoursCalculated + $scope.ngTotalNOMilesCalculated + $scope.ngTotalLossFee;
});