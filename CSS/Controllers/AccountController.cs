﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using CSS.Filters;
using CSS.Models;
using BLL;
using BLL.Models;
using BLL.ViewModels;
using System.Data.Objects;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using Newtonsoft.Json;
using System.Text;
using System.Configuration;
using System.Net;

namespace CSS.Controllers
{
    public class AccountController : Controller
    {
        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        [AllowAnonymous]
        public ActionResult Login()
        {
            LoginViewModel loginViewModel = new LoginViewModel();
            string cookieName = FormsAuthentication.FormsCookieName; //Find cookie name
            HttpCookie authCookie = HttpContext.Request.Cookies.Get("IADAAuthUser"); //Get the cookie by it's name
            if (authCookie != null && !String.IsNullOrEmpty(authCookie.Value))
            {
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value); //Decrypt it
                loginViewModel.UserName = ticket.Name;
            }
            //loginViewModel.UserName = HttpContext.Current.User.Identity.Name;
            return View(loginViewModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel loginViewModel, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                ObjectParameter outLoginStatus = new ObjectParameter("LoginStatus", DbType.Int32);
                ObjectParameter outUserId = new ObjectParameter("UserId", DbType.Int32);
                css.ValidateUserCredential(outLoginStatus, outUserId, loginViewModel.UserName, Cypher.EncryptString(loginViewModel.Password));
                    switch (Convert.ToInt32(outLoginStatus.Value))
                {
                    case -1:
                        ModelState.AddModelError("", "Invalid User Name or Password");
                        break;
                    case 1:
                            //OAuth get accessToken using refresh Token.
                            //setQuickBookDetail();
                            string EncryptPassword = Cypher.EncryptString(loginViewModel.Password);
                            BLL.User user = css.Users.Where(u => u.UserName == loginViewModel.UserName && u.Password == EncryptPassword).First();
                            byte[] userAssignedRightsList = css.usp_UserAssignedRightsGetList(user.UserId).Select(x => x.UserRightId).ToArray();
                        //If user is a CSS POC or has CSS POC Rights then for the time being the user is an Admin
                        //if (user.UserTypeId == 3 || userAssignedRightsList.Contains((byte)2))
                        //{
                        //    user.UserTypeId = 2;
                        //}
                            if (userAssignedRightsList.Contains((byte)2))
                            {
                                user.UserTypeId = 2;
                            }
                        CSS.Models.User loggedInUser = new Models.User();
                        loggedInUser.UserId = user.UserId;
                        loggedInUser.UserName = user.UserName;
                        loggedInUser.AssignedRights = userAssignedRightsList;
                        loggedInUser.UserTypeId = user.UserTypeId.GetValueOrDefault(0);
                        loggedInUser.HeadCompanyId = user.HeadCompanyId;
                        loggedInUser.Email = user.Email;
                        Session["LoggedInUser"] = loggedInUser;
                        if (loginViewModel.RememberMe)
                        {
                            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, loginViewModel.UserName, DateTime.Now, DateTime.Now.AddDays(30), true, loginViewModel.UserName);
                            // Encrypt the ticket.
                            string encTicket = FormsAuthentication.Encrypt(ticket);
                            // Create the cookie.
                            Response.Cookies.Add(new HttpCookie("IADAAuthUser", encTicket));

                           
                        }
                        FormsAuthentication.RedirectFromLoginPage(loginViewModel.UserName, loginViewModel.RememberMe);
                        //return RedirectToAction("AutoSearch", "AutoAssignment");
                        return RedirectToAction("Index", "Home");
                 }

            }
            else
            {
                ModelState.AddModelError("", "Invalid User Name or Password");
            }
            ViewBag.ReturnUrl = returnUrl;
            return View(loginViewModel);
        }


        //public async Task<string> setQuickBookDetail()
        public string  setQuickBookDetail()
        {
            string strResult = "";
            BLL.Models.QuickBookDetails obj = new BLL.Models.QuickBookDetails();

            obj.clientId = ConfigurationManager.AppSettings["clientid"].ToString();
            obj.clientSecret = ConfigurationManager.AppSettings["clientsecret"].ToString();
            obj.callbackURL = "";
            obj.authorizationCode = "";
            obj.realmId = ConfigurationManager.AppSettings["realmId"].ToString();
            obj.refreshToken = ConfigurationManager.AppSettings["refreshToken"].ToString();
            obj.accessToken = "";
            obj.discoveryAuthority = "";
            obj.discoveryUrl = "";
            obj.qboBaseUrl = ConfigurationManager.AppSettings["QBOBaseUrl"].ToString();
            obj.state = "";

            obj.error = "";

            QBManageTokens_Result objQBManageToken = css.QBManageTokens(false, null, null).FirstOrDefault();

            strResult = performRefreshToken(objQBManageToken.RefreshToken, obj.clientId, obj.clientSecret);

            if ((!string.IsNullOrEmpty(strResult) && strResult.Contains('~')) && (!strResult.Contains("Error") || !strResult.Contains("Exception")))
            {
                //0. Refresh Token
                //1. Access Token
                string[] tokensArray = strResult.Split('~');
                if (tokensArray.Length == 2)
                {
                    obj.refreshToken = tokensArray[0];
                    obj.accessToken = tokensArray[1];

                    //0. Get
                    //1. Update
                    objQBManageToken = css.QBManageTokens(true, obj.accessToken, obj.refreshToken).FirstOrDefault();
                    strResult = obj.accessToken;

                    //System.Web.HttpContext.Current.Session["QuickBookDetails"] = obj;
                }
            }
            else
            {
                strResult = "Error while generating access token";
            }

            return strResult;
        }

        private string performRefreshToken(string refresh_token, string clientID, string clientSecret)
        {
            //output("Exchanging refresh token for access token.");//refresh token is valid for 100days and access token for 1hr
            string access_token = "", strTokensResult = "";
            string cred = string.Format("{0}:{1}", clientID, clientSecret);
            string enc = Convert.ToBase64String(Encoding.ASCII.GetBytes(cred));
            string basicAuth = string.Format("{0} {1}", "Basic", enc);
            string tokenEndPoint = ConfigurationManager.AppSettings["tokenEndPoint"].ToString();

            // build the  request
            string refreshtokenRequestBody = string.Format("grant_type=refresh_token&refresh_token={0}",
                refresh_token
                );

            // send the Refresh Token request
            HttpWebRequest refreshtokenRequest = (HttpWebRequest)WebRequest.Create(tokenEndPoint);
            refreshtokenRequest.Method = "POST";
            refreshtokenRequest.ContentType = "application/x-www-form-urlencoded";
            refreshtokenRequest.Accept = "application/json";
            //Adding Authorization header
            refreshtokenRequest.Headers[HttpRequestHeader.Authorization] = basicAuth;

            byte[] _byteVersion = Encoding.ASCII.GetBytes(refreshtokenRequestBody);
            refreshtokenRequest.ContentLength = _byteVersion.Length;
            Stream stream = refreshtokenRequest.GetRequestStream();
            stream.Write(_byteVersion, 0, _byteVersion.Length);
            stream.Close();

            try
            {
                //get response
                HttpWebResponse refreshtokenResponse = (HttpWebResponse)refreshtokenRequest.GetResponse();
                using (var refreshTokenReader = new StreamReader(refreshtokenResponse.GetResponseStream()))
                {
                    //read response
                    string responseText = refreshTokenReader.ReadToEnd();

                    // decode response
                    Dictionary<string, string> refreshtokenEndpointDecoded = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseText);

                    if (refreshtokenEndpointDecoded.ContainsKey("error"))
                    {
                        // Check for errors.
                        if (refreshtokenEndpointDecoded["error"] != null)
                        {
                            //output(String.Format("OAuth token refresh error: {0}.", refreshtokenEndpointDecoded["error"]));
                            strTokensResult = "Error: OAuth token refresh error: " + refreshtokenEndpointDecoded["error"];
                        }
                    }
                    else
                    {
                        //if no error
                        if (refreshtokenEndpointDecoded.ContainsKey("refresh_token"))
                        {

                            refresh_token = refreshtokenEndpointDecoded["refresh_token"];
                            if (refreshtokenEndpointDecoded.ContainsKey("access_token"))
                            {
                                //save both refresh token and new access token in permanent store
                                access_token = refreshtokenEndpointDecoded["access_token"];
                                strTokensResult = refresh_token + "~" + access_token;
                            }
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = ex.Response as HttpWebResponse;
                    if (response != null)
                    {

                        //output("HTTP Status: " + response.StatusCode);
                        var exceptionDetail = response.GetResponseHeader("WWW-Authenticate");
                        if (exceptionDetail != null && exceptionDetail != "")
                        {
                            //return exceptionDetail;
                            strTokensResult = "Exception";
                        }
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            // read response body
                            string responseText = reader.ReadToEnd();
                            if (responseText != null && responseText != "")
                            {
                                //return responseText;
                                strTokensResult = "Exception";
                            }
                        }
                    }

                }
            }
            return strTokensResult;
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session["LoggedInUser"] = null;

            Session.Abandon();
            Session.Clear();
            return RedirectToAction("Login", "Account");
        }


        [HttpPost]
        public JsonResult ForgotPassword(String UserName)
        {
            if (string.IsNullOrEmpty(UserName))
            { return Json(new {flag=0,errorMsg="UserName Is Required" },JsonRequestBehavior.AllowGet); }

            BLL.ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            ObjectParameter Pwupdstatus = new ObjectParameter("Pwupdstatus", SqlDbType.Int);
         
            String resetKey = Guid.NewGuid().ToString();
            String outEmail=css.usp_resetPassword(Pwupdstatus,"", resetKey, UserName).FirstOrDefault();
            if (string.IsNullOrEmpty(outEmail))
            {
                //TempData["errorMsg"] = "Some error occurred Please try again";
                return Json(new { flag = 0, errorMsg = "Some error occurred" }, JsonRequestBehavior.AllowGet); 
            }
            if (Convert.ToInt32(Pwupdstatus.Value) == 1)
            {
             
                if(sendPasswordRecoveryMail(resetKey, outEmail))
                    return Json(new { flag = 1, errorMsg = "Password reset link has been sent on your registered email address successfully" }, JsonRequestBehavior.AllowGet); 
                else
                    return Json(new { flag = 0, errorMsg = "Some error occurred" }, JsonRequestBehavior.AllowGet); 

            }
            else
            {
                return Json(new { flag = 0, errorMsg = "Some error occurred" }, JsonRequestBehavior.AllowGet); 
            }
        }
        [HttpPost]
        public ActionResult ResetPassword(LoginViewModel loginViewModel)
        {

            BLL.ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            ObjectParameter Pwupdstatus = new ObjectParameter("Pwupdstatus", SqlDbType.Int);
           
            String resetKey = Guid.NewGuid().ToString();
            String outEmail = css.usp_resetPassword(Pwupdstatus, loginViewModel.UserEmail, resetKey, null).FirstOrDefault(); ;
            if (string.IsNullOrEmpty(outEmail))
            {
                return Json(new { flag = 0, errorMsg = "Contact Administrator!" }, JsonRequestBehavior.AllowGet); 
            }

            if (Convert.ToInt32(Pwupdstatus.Value) == 1)
            {
             
                if(sendPasswordRecoveryMail(resetKey, outEmail))
                     return Json(new { flag = 1, errorMsg = "Password reset link has been sent on your registered email address Successfully" }, JsonRequestBehavior.AllowGet);
                else
                     return Json(new { flag = 0, errorMsg = "Some error occurred" }, JsonRequestBehavior.AllowGet); 
            }
            else
            {
                return Json(new { flag = 0, errorMsg = "Contact Administrator!" }, JsonRequestBehavior.AllowGet); 
            }
        }
        [NonAction]
        private bool sendPasswordRecoveryMail(String resetKey, string outEmail)
        {
            bool result = false;
            StringBuilder htmlsend = new StringBuilder();
            htmlsend.Append("Click on the following link to reset your password: - ");
            htmlsend.Append("<br/> <br/>");
            string webUrl = ConfigurationManager.AppSettings["WebAppURL"].ToString();
            htmlsend.Append(webUrl + "Account/NewPasswordSetup?newid=" + resetKey);
            htmlsend.Append("<br/> <br/>");
            htmlsend.Append("IADA");
            htmlsend.Append("<br/>Damage Appraisers");
            result=Utility.sendEmail(outEmail, "", "Password Recovery", htmlsend.ToString());
            return result;
        }
        [AllowAnonymous]
        public ActionResult NewPasswordSetup(String newid)
        {
            ResetPasswordViewModel ViewModel = new ResetPasswordViewModel();
            TempData["errorMsg"] = null;
            ViewModel.resetKey = newid;
            return View(ViewModel);
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult NewPasswordSetup(ResetPasswordViewModel ViewModel)
        {
            if (ModelState.IsValid)
            {
                BLL.ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
                ObjectParameter Pwupdstatus = new ObjectParameter("Pwupdstatus", DbType.Int32);
                if (string.IsNullOrEmpty(ViewModel.resetKey))
                {
                    TempData["errorMsg"] = "Your Link is not valid.Please reclick on Forgot Password Link to Reset The Password";

                    return RedirectToAction("Login");
                }
                string EncryptPassword = Cypher.EncryptString(ViewModel.Password);
                css.usp_updateNewPassword(Pwupdstatus, ViewModel.resetKey, EncryptPassword);

                if (Convert.ToInt32(Pwupdstatus.Value) == 1)
                {
                    TempData["errorMsg"] = "Your password has been updated successfully.";
                    return RedirectToAction("Login");
                    //  return Json(1);
                }
                else
                {
                    TempData["errorMsg"] = "Your Link has expired.Please reclick on Forgot Password Link to Reset The Password";

                    return RedirectToAction("Login");
                }
            }
          
           return View();
        }
        [HttpPost]
        public JsonResult IsUserNameAvailable(string userName)
        {


            BLL.ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            ObjectParameter outStatus = new ObjectParameter("statusFlag", DbType.Binary);
            css.IsUserNameInUse(outStatus, userName);

            if (Convert.ToBoolean(outStatus.Value) == true)
            {
                return Json(0);
            }
            else
            {
                return Json(1);
            }
        }



        #region Users Profile
        [Authorize]
        public ActionResult UserProfile()
        {
            UserProfileViewModel viewModel = new UserProfileViewModel();
            //CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            //if (loggedInUser != null)
            //{
            //    viewModel = new UserProfileViewModel(loggedInUser.UserId);
            //    if (!String.IsNullOrEmpty(viewModel.user.imagePath))
            //    {
            //        string baseFolder = System.Configuration.ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
            //        String RelativePath = Path.Combine((VirtualPathUtility.ToAbsolute(baseFolder) + "ThumbnailImage/" + viewModel.user.UserId), viewModel.user.imagePath);
            //        viewModel.user.imagePath = RelativePath;
            //    }
            //}
            return View(viewModel);
        }
        [Authorize]
        public ActionResult InitializeUserProfile()
        {
            UserProfileViewModel viewModel = new UserProfileViewModel();
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            if (loggedInUser != null)
            {
                viewModel = new UserProfileViewModel(loggedInUser.UserId);
                if (!String.IsNullOrEmpty(viewModel.user.imagePath))
                {
                    string baseFolder = System.Configuration.ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
                    String RelativePath = Path.Combine((VirtualPathUtility.ToAbsolute(baseFolder) + "ThumbnailImage/" + viewModel.user.UserId), viewModel.user.imagePath);
                    viewModel.user.imagePath = RelativePath;
                }
            }
            // return View(viewModel);

            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [Authorize]
        public ActionResult updateUserProfile(UserProfileViewModel viewModel)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            if (loggedInUser != null)
            {
                if (!String.IsNullOrEmpty(viewModel.UserProfile.FirstName))
                {
                    css.usp_UpdateUserProfile(loggedInUser.UserId, viewModel.UserProfile.FirstName, viewModel.UserProfile.LastName, viewModel.UserProfile.StreetAddress, viewModel.UserProfile.City, viewModel.UserProfile.State,
                        viewModel.UserProfile.Zip, viewModel.UserProfile.Email, viewModel.UserProfile.MobilePhone, loggedInUser.UserId, DateTime.Now);
                    viewModel.Error = "Profile Details Updated Successfully!";
                }
                else {
                    viewModel.Error = "UserName is Required!";
                }
                // return View(viewModel);
            }
            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [Authorize]
        public ActionResult ChangePassword(UserProfileViewModel viewModel)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            String errorMsg = "";
            if (!String.IsNullOrEmpty(viewModel.Password))
            {
                if (loggedInUser != null)
                {
                    viewModel.CurrentPassword = Cypher.EncryptString(viewModel.CurrentPassword);
                    if (css.Users.Where(x => x.Password == viewModel.CurrentPassword).Count() > 0)
                    {
                        css.usp_changeUserPassword(loggedInUser.UserId, Cypher.EncryptString(viewModel.Password));
                        errorMsg = "New Password has been changed Successfully";
                    }
                    else
                    {
                        errorMsg = "Enter Valid Current Password";
                    }

                }
            }
            else {
                errorMsg = "Password Field is Required!";
            }
            return Json(errorMsg,JsonRequestBehavior.AllowGet);
        }
     
        [HttpPost]
        public ActionResult UploadDocument(HttpPostedFileBase uploadFile, UserProfileViewModel viewModel)
        {
            string imagePath = "", LogoPathI="";
            String RelativePath = "", LogoPath = "";;
            try
            {
                if (ModelState.IsValid)
                {
                    if (uploadFile != null && uploadFile.ContentLength > 0)
                    {
                        if (viewModel.UserId != null)
                        {
                            UploadDocument(viewModel, uploadFile, out imagePath);

                          //  string baseFolder = System.Configuration.ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
                          //  RelativePath = Path.Combine((VirtualPathUtility.ToAbsolute(baseFolder) + "ThumbnailImage/" + viewModel.UserId), imagePath);

                        }
                    }
                    //if (uploadLogo != null && uploadLogo.ContentLength > 0)
                    //{
                    //    if (viewModel.UserId != null)
                    //    {
                    //        UploadLogo(viewModel, uploadLogo, out LogoPathI);
                    //        string baseFolder = System.Configuration.ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
                    //        LogoPath = Path.Combine((VirtualPathUtility.ToAbsolute(baseFolder) + "ThumbnailImage/" + viewModel.UserId), LogoPathI);
                    //    }
                    //}
                    return Json(new { RelativePath = RelativePath, LogoPath = LogoPath, errorList = "Profile Image has been updated Successfully" }, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                return Json(new { RelativePath = RelativePath, LogoPath = LogoPath, errorList = "Error" }, JsonRequestBehavior.AllowGet);

            }
            
            return Json(new { RelativePath = "", errorList = "Error occured while uploading image" }, JsonRequestBehavior.AllowGet);
        }
        public void UploadDocument(UserProfileViewModel viewModel, HttpPostedFileBase uploadFile,out string imagePath)
        {
            imagePath = "";
            if (uploadFile != null && uploadFile.ContentLength > 0)
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                if (MvcApplication.UseAzureCloudStorage == "1")
                {
                    #region Cloud Storage

                    // extract only the fielname
                    string fileName = new FileInfo(uploadFile.FileName).Name;
                    string storeAsFileName = Guid.NewGuid() + "" + Regex.Replace(fileName, "[^0-9a-zA-Z-_.]+", "");
                    string HeadCompanyId = viewModel.HeadCompanyId + "";
                    //check whether the folder with user's userid exists

                    // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                    string relativeFileName = HeadCompanyId + "/" + storeAsFileName;

                    MemoryStream bufferStream = new System.IO.MemoryStream();
                    uploadFile.InputStream.CopyTo(bufferStream);
                    byte[] buffer = bufferStream.ToArray();
                    string containerName = MvcApplication.CompanyDocsContainerName;
                    string ThumbnailcontainerName = MvcApplication.CompanyThumbnailContainerName;
                    //remove existing file
                    try {
                        int companyid = viewModel.HeadCompanyId ?? 0;
                        String imagePathDB = css.Companies.Where(x => x.CompanyId == companyid).Select(y => y.imagePath).FirstOrDefault();
                        if (!String.IsNullOrEmpty(imagePathDB))
                        {
                            string deleteFileName = HeadCompanyId + "/" + imagePathDB;
                            CloudStorageUtility.DeleteFile(containerName, deleteFileName);
                            CloudStorageUtility.DeleteFile(ThumbnailcontainerName, deleteFileName);
                        }
                    }
                    catch (Exception ex) { }


               //Original Image storage 
                  
                    CloudStorageUtility.StoreFile(containerName, relativeFileName, buffer);
                    bufferStream.Close();
                    bufferStream.Dispose();
               //Thumbnail Image storage  
                   
                   
                    MemoryStream TbufferStream = new System.IO.MemoryStream();
                    // Load image.
                    Image image = Image.FromStream(uploadFile.InputStream);

                    // Compute thumbnail size.
                    Size thumbnailSize = GetThumbnailSize(image);

                    // Get thumbnail.
                    Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                        thumbnailSize.Height, null, IntPtr.Zero);
                    ImageFormat Iformat = new ImageFormat(image.RawFormat.Guid);
                    thumbnail.Save(TbufferStream,Iformat);

                    // If you're going to read from the stream, you may need to reset the position to the start
                    TbufferStream.Position = 0;
                    byte[] Tbuffer = TbufferStream.ToArray();
                    CloudStorageUtility.StoreFile(ThumbnailcontainerName, relativeFileName, Tbuffer);
                    TbufferStream.Close();
                    TbufferStream.Dispose();
                    #endregion
                    imagePath = storeAsFileName;
                    //Update Company Logo Image
                    if (viewModel.HeadCompanyId != null)
                    {
                        css.usp_companyProfilePictureUpload(viewModel.UserId, viewModel.HeadCompanyId, storeAsFileName);
                    }
                }
                else
                {
                    #region Local File System

                    // extract only the fielname
                    string fileName = new FileInfo(uploadFile.FileName).Name;
                    string storeAsFileName = Guid.NewGuid() + "" + Regex.Replace(fileName, "[^0-9a-zA-Z-_.]+", "");
                    string baseFolder = MvcApplication.ClaimServiceProviderDocPath;
                    string HeadCompanyId = viewModel.HeadCompanyId + "";

                    //check whether the folder with user's userid exists
                    if (!Directory.Exists(Server.MapPath(baseFolder + "OriginalImage/" + HeadCompanyId)))
                    {
                        Directory.CreateDirectory(Server.MapPath(baseFolder + "OriginalImage/" + HeadCompanyId));
                    }

                    if (!Directory.Exists(Server.MapPath(baseFolder + "ThumbnailImage/" + HeadCompanyId)))
                    {
                        Directory.CreateDirectory(Server.MapPath(baseFolder + "ThumbnailImage/" + HeadCompanyId));
                    }

                    // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                    string path = Path.Combine(Server.MapPath(baseFolder + "OriginalImage/" + HeadCompanyId), storeAsFileName);
                    uploadFile.SaveAs(path);
                    ThumbnailImage(path, Path.Combine(Server.MapPath(baseFolder + "ThumbnailImage/" + HeadCompanyId), storeAsFileName));

                    //   loadFormData(uploadFile, path);


                    //  css.usp_userProfilePictureUpload(viewModel.UserId, storeAsFileName);
                    //Update Company Logo Image
                    if (viewModel.HeadCompanyId != null)
                    {
                        css.usp_companyProfilePictureUpload(viewModel.UserId, viewModel.HeadCompanyId, storeAsFileName);
                    }
                    #endregion
                    imagePath = storeAsFileName;
                }
             
            }
        }
      
        public void UploadLogo(UserProfileViewModel viewModel, HttpPostedFileBase uploadLogo, out string LogoPath)
        {
            LogoPath = "";
            if (uploadLogo != null && uploadLogo.ContentLength > 0)
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                //#region Cloud Storage

                //// extract only the fielname
                //string fileName = new FileInfo(uploadFile.FileName).Name;
                //string storeAsFileName = Regex.Replace(fileName, "[^0-9a-zA-Z-_.]+", "");
                //string assignmentId = document.AssignmentId + "";
                //string documentTypeId = document.DocumentTypeId + "";
                ////check whether the folder with user's userid exists

                //// store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                //string relativeFileName = assignmentId + "/" + documentTypeId + "/" + storeAsFileName;

                //MemoryStream bufferStream = new System.IO.MemoryStream();
                //uploadFile.InputStream.CopyTo(bufferStream);
                //byte[] buffer = bufferStream.ToArray();

                //string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                //CloudStorageUtility.StoreFile(containerName, relativeFileName, buffer);



                //#endregion

                #region Local File System

                // extract only the fielname
                string fileName = new FileInfo(uploadLogo.FileName).Name;
                string storeAsFileName = Guid.NewGuid() + "" + Regex.Replace(fileName, "[^0-9a-zA-Z-_.]+", "");
                string baseFolder = System.Configuration.ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
                string UserId = viewModel.UserId + "";

                //check whether the folder with user's userid exists
                if (!Directory.Exists(Server.MapPath(baseFolder + "OriginalImage/Logo/" + UserId)))
                {
                    Directory.CreateDirectory(Server.MapPath(baseFolder + "OriginalImage/Logo/" + UserId));
                }
                if (!Directory.Exists(Server.MapPath(baseFolder + "ThumbnailImage/Logo/" + UserId)))
                {
                    Directory.CreateDirectory(Server.MapPath(baseFolder + "ThumbnailImage/Logo/" + UserId));
                }

                // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                string path = Path.Combine(Server.MapPath(baseFolder + "OriginalImage/Logo/" + UserId), storeAsFileName);
                uploadLogo.SaveAs(path);
               ThumbnailImage(path,Path.Combine(Server.MapPath(baseFolder + "ThumbnailImage/Logo/" + UserId), storeAsFileName));
                // loadFormData(uploadLogo, path);


                css.usp_userLogoUpload(viewModel.UserId, storeAsFileName);
                #endregion
                LogoPath = storeAsFileName;

            }
        }
     
        [NonAction]
        private void ThumbnailImage(string inputPath,string outPutPath)
        {
         

            // Load image.
            Image image = Image.FromFile(inputPath);

            // Compute thumbnail size.
            Size thumbnailSize = GetThumbnailSize(image);

            // Get thumbnail.
            Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                thumbnailSize.Height, null, IntPtr.Zero);
         
            // Save thumbnail.
            thumbnail.Save(outPutPath);
        }
       [NonAction]
        static Size GetThumbnailSize(Image original)
        {
            // Maximum size of any dimension.
            const int maxPixels = 140;

            // Width and height.
            int originalWidth = original.Width;
            int originalHeight = original.Height;

            // Compute best factor to scale entire image based on larger dimension.
            double factor;
            if (originalWidth > originalHeight)
            {
                factor = (double)maxPixels / originalWidth;
            }
            else
            {
                factor = (double)maxPixels / originalHeight;
            }

            // Return thumbnail size.
            return new Size((int)(originalWidth * factor), (int)(originalHeight * factor));
        }

        [NonAction]
        public string loadFormData(HttpPostedFileBase file,String strPath)
        {
            //retrive image from form
            string strfilename = string.Empty;
            if (file.ContentLength > 0)
            {
            
                try
                {
                    Bitmap ImageOrg = new Bitmap(file.InputStream);

                    SaveImage(ImageOrg, 390, 300, strPath);
                }
                catch
                {
                    strfilename = "";
                }
            }
            return strfilename;
        }
        [NonAction]
        public void SaveImage(Bitmap image, int maxWidth, int maxHeight, string filePath)
        {
            // Get the image's original width and height
            int originalWidth = image.Width;
            int originalHeight = image.Height;
            float ratio = 1;
            // To preserve the aspect ratio
            if (originalHeight > maxHeight)
            {
                //float ratioX = (float)maxWidth / (float)originalWidth;
                float ratioX = (float)originalWidth;
                float ratioY = (float)maxHeight / (float)originalHeight;
                ratio = Math.Min(ratioX, ratioY);

            }

            // New width and height based on aspect ratio
            int newWidth = (int)(originalWidth * ratio);
            int newHeight = (int)(originalHeight * ratio);

            // Convert other formats (including CMYK) to RGB.
            Bitmap newImage = new Bitmap(newWidth, newHeight, PixelFormat.Format24bppRgb);

            // Draws the image in the specified size with quality mode set to HighQuality
            using (Graphics graphics = Graphics.FromImage(newImage))
            {
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);
            }

            newImage.Save(filePath, image.RawFormat);

        }
        #endregion
    }

}

