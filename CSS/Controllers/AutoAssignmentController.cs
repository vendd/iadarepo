﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ViewModels;
using BLL;
using System.Data.Objects;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using CSS.CssXactAssignmentService;
using BLL.Models;
using System.Text.RegularExpressions;
using System.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting;
using Microsoft.Reporting.WebForms;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Drawing;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using CSS.WeGoLookDocUpload;
using CSS.WeGoLookDocUpload.AmazonServices;

namespace CSS.Controllers
{
    [Authorize]
    public class AutoAssignmentController : CustomController
    {
        //
        // GET: /AutoAssignment/


        public AutoAssignmentController()
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        BLL.ChoiceSolutionsEntities css = new BLL.ChoiceSolutionsEntities();

        public ActionResult AutoAssignment(Int64 Claimid = -1, bool isNuGenClaims = false ,bool isManualInvoiceClaim = false) //INPSW -32
        {
            AutoAssignmentViewModel viewModel;
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            BLL.User LoggedInUser = css.Users.Find(loggedInUser.UserId);
            int UserID = Convert.ToInt32(LoggedInUser.UserId);
            int UserTypeID = Convert.ToInt32(LoggedInUser.UserTypeId);
            int CompanyID = Convert.ToInt32(LoggedInUser.HeadCompanyId);
            if (Claimid == -1)
            {
                viewModel = new AutoAssignmentViewModel();
                viewModel.UseMasterPageLayout = true;
                viewModel.claim.IsSingleDeductible = false;//Default to SingeDeductible   

                viewModel.ClaimRepresentativeList = ClaimRepresentativeList(UserTypeID, CompanyID);
                viewModel.InsuranceCompaniesList = InsuranceCompanies(UserTypeID, CompanyID);
            }
            else
            {
                viewModel = new AutoAssignmentViewModel(Claimid);
                viewModel.UseMasterPageLayout = false;
                viewModel.LineOfBusinessList = GetLOBDDL(viewModel.claim.HeadCompanyId);
                viewModel.ClientPointofContactList = GetClientContactDDL(viewModel.claim.HeadCompanyId);
                CompanyID = Convert.ToInt32(viewModel.claim.HeadCompanyId);
                viewModel.ClaimRepresentativeList = ClaimRepresentativeList(UserTypeID, CompanyID);
                viewModel.InsuranceCompaniesList = InsuranceCompanies(UserTypeID, CompanyID);
                viewModel.claim.ServiceTypeID = (from c in css.Claims
                                               where c.ClaimId == Claimid
                                               select c.ServiceTypeID).First();
                viewModel.ServiceOfferingsList.Clear();
                viewModel.ServiceOfferingsList = CompanyServiceOfferingsList(CompanyID,viewModel.claim.ServiceTypeID);

                BLL.User user = new BLL.User();
                if (viewModel.autoassignment.OAUserID != null)
                {
                    user = css.Users.Find(viewModel.autoassignment.OAUserID);
                    viewModel.InternalContactList = InternalContactList(user.HeadCompanyId);
                }
            }

            if (isNuGenClaims)
            {
                viewModel.isNuGenClaim = true;
            }
            else
            {
                viewModel.isNuGenClaim = false;
            }

            //...INPSW -32..Start
            if (isManualInvoiceClaim)
            {
                viewModel.isManualInvoiceClaim = true;
            }
            else
            {
                viewModel.isManualInvoiceClaim = false;
            }
            //INPSW -32 end

            /////////////////////////////////////////////////////////////////////////////
            viewModel.IsEditOrSearchAssign = true;
            ////////////////////////////////////////////////////////////////////////////
            return View(viewModel);
        }

        //////////////////////////////////////////////////////////////////////////////////////////
        [HttpPost]
        public ActionResult AutoAssignmentClaimType(FormCollection fc, Int64 Claimid = -1, bool isNuGenClaims = false)
        {
           // string test = fc["ddlClaimType"]; comment for task INPSW -32
            AutoAssignmentViewModel viewModel;
            string test = "";
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            BLL.User LoggedInUser = css.Users.Find(loggedInUser.UserId);
            int UserID = Convert.ToInt32(LoggedInUser.UserId);
            int UserTypeID = Convert.ToInt32(LoggedInUser.UserTypeId);
            int CompanyID = Convert.ToInt32(LoggedInUser.HeadCompanyId);
            if (Claimid == -1)
            {
                viewModel = new AutoAssignmentViewModel();
                viewModel.UseMasterPageLayout = true;
                viewModel.claim.IsSingleDeductible = false;//Default to SingeDeductible   

                viewModel.ClaimRepresentativeList = ClaimRepresentativeList(UserTypeID, CompanyID);
                viewModel.InsuranceCompaniesList = InsuranceCompanies(UserTypeID, CompanyID);
                //INPSW-32 start
                if (fc["isManualInvoiceClaim"] != null && fc["isManualInvoiceClaim"].ToString() == "1")
                {
                    viewModel.isManualInvoiceClaim = true;//INPSW -32
                    test = "Automobile";
                }
                else
                {
                    test = fc["ddlClaimType"];
                    viewModel.isManualInvoiceClaim = false;//INPSW -32
                }
                //INPSW -32 End
            }
            else
            {
                viewModel = new AutoAssignmentViewModel(Claimid);
                viewModel.UseMasterPageLayout = false;
                viewModel.LineOfBusinessList = GetLOBDDL(viewModel.claim.HeadCompanyId);
                viewModel.ClientPointofContactList = GetClientContactDDL(viewModel.claim.HeadCompanyId);
                CompanyID = Convert.ToInt32(viewModel.claim.HeadCompanyId);
                viewModel.ClaimRepresentativeList = ClaimRepresentativeList(UserTypeID, CompanyID);
                viewModel.InsuranceCompaniesList = InsuranceCompanies(UserTypeID, CompanyID);

                BLL.User user = new BLL.User();
                if (viewModel.autoassignment.OAUserID != null)
                {
                    user = css.Users.Find(viewModel.autoassignment.OAUserID);
                    viewModel.InternalContactList = InternalContactList(user.HeadCompanyId);
                }
                //if (fc.AllKeys.Length <= 0)
                //{
                //    viewModel.isManualInvoiceClaim = true;//INPSW -32
                //    test = "Automobile";
                //}
                //else
                //{
                //    test = fc["ddlClaimType"];
                //    viewModel.isManualInvoiceClaim = false;//INPSW -32
                //}
            }

            if (isNuGenClaims)
            {
                viewModel.isNuGenClaim = true;
            }
            else
            {
                viewModel.isNuGenClaim = false;
            }
            if (test == "Automobile")
            {
                viewModel.autoassignment.ServiceId = 1;
                viewModel.ServiceTypeId = 2;
            }
            else
            {
                viewModel.autoassignment.ServiceId = 9;
                viewModel.ServiceTypeId = 1;
            }

            if (CompanyID != null && CompanyID != 0)
            {
                viewModel.ServiceOfferingsList.Clear();
                viewModel.ServiceOfferingsList = CompanyServiceOfferingsList(CompanyID, viewModel.ServiceTypeId);

            }
            else
            {
                viewModel.ServiceOfferingsList.Clear();
                foreach (ServiceOffering serviceoffering in css.ServiceOfferings.Where(x => x.ServiceTypeId == viewModel.ServiceTypeId && x.IsDeleted == false).ToList())
                {
                    viewModel.ServiceOfferingsList.Add(new SelectListItem { Text = serviceoffering.ServiceDescription, Value = Convert.ToString(serviceoffering.ServiceId) });
                }
            }
            viewModel.IsEditOrSearchAssign = false;
            return View("AutoAssignment",viewModel);
        }
        /// //////////////////////////////////////////////////////////////////////////////////////////////////

        private List<SelectListItem> CompanyServiceOfferingsList(int? CompanyId, int? ServiceTypeId)//task2-swapnil
        {
            List<SelectListItem> ServiceOfferingsList = new List<SelectListItem>();
            List<GetServiceOfferingsList_Result> ClaimServList = css.GetServiceOfferingsList(CompanyId, ServiceTypeId).ToList();
            foreach (var Services in ClaimServList)
            {

                ServiceOfferingsList.Add(new SelectListItem { Text = Services.ServiceDescription, Value = Services.ServiceId + "" });
            }
            if (ClaimServList.Count == 0)
            {
                ServiceOfferingsList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            }
            return ServiceOfferingsList;
        }
        public ActionResult MapAutoAssignment(Int64 Claimid = -1, Int64 SPID = -1, string ServiceID = "")
        {
            AutoAssignmentViewModel viewModel;
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            BLL.User LoggedInUser = css.Users.Find(loggedInUser.UserId);
            int UserID = Convert.ToInt32(LoggedInUser.UserId);
            int UserTypeID = Convert.ToInt32(LoggedInUser.UserTypeId);
            int CompanyID = Convert.ToInt32(LoggedInUser.HeadCompanyId);
            if (Claimid == -1)
            {
                viewModel = new AutoAssignmentViewModel();
                viewModel.UseMasterPageLayout = true;
                viewModel.claim.IsSingleDeductible = false;//Default to SingeDeductible   

                viewModel.ClaimRepresentativeList = ClaimRepresentativeList(UserTypeID, CompanyID);
                viewModel.InsuranceCompaniesList = InsuranceCompanies(UserTypeID, CompanyID);
                viewModel.autoassignment.OAUserID = SPID;
                viewModel.autoassignment.ServiceId = ServiceID == "" ? 0 : Convert.ToInt32(ServiceID);
                BLL.User user = new BLL.User();
                if (SPID != -1)
                {
                    user = css.Users.Find(viewModel.autoassignment.OAUserID);
                    viewModel.InternalContactList = InternalContactList(user.HeadCompanyId);
                }
                //svp 10
                if (ServiceID == "9" && ServiceID == "11" && ServiceID == "12")
                {
                    viewModel.ServiceTypeId = 1;
                }
                else
                {
                    viewModel.ServiceTypeId = 2;
                }
                /////////////////////////////////////////////////////////////////////////////////////
                viewModel.IsEditOrSearchAssign = true;
                /////////////////////////////////////////////////////////////////////////////////////
            }
            else
            {

                viewModel = new AutoAssignmentViewModel(Claimid);
                viewModel.UseMasterPageLayout = false;
                viewModel.LineOfBusinessList = GetLOBDDL(viewModel.claim.HeadCompanyId);
                viewModel.ClientPointofContactList = GetClientContactDDL(viewModel.claim.HeadCompanyId);
                CompanyID = Convert.ToInt32(viewModel.claim.HeadCompanyId);
                viewModel.ClaimRepresentativeList = ClaimRepresentativeList(UserTypeID, CompanyID);
                viewModel.InsuranceCompaniesList = InsuranceCompanies(UserTypeID, CompanyID);
                viewModel.autoassignment.ServiceId = ServiceID == "" ? 0 : Convert.ToInt32(ServiceID);
                BLL.User user = new BLL.User();
                if (viewModel.autoassignment.OAUserID != null)
                {
                    user = css.Users.Find(viewModel.autoassignment.OAUserID);
                    viewModel.InternalContactList = InternalContactList(user.HeadCompanyId);
                }
            }
            return PartialView("_CreateAutoAssignment", viewModel);
        }

        [HttpPost]
        public ActionResult BulkMarkAutoNotesBillable(string noteIds)
        {
            string returnCode = "0";
            string returnData = String.Empty;

            try
            {
                foreach (string noteId in noteIds.Split(','))
                {
                    css.AutoNoteIsBillableUpdate(Convert.ToInt64(noteId), true);
                }
                returnCode = "1";
            }
            catch (Exception ex)
            {
                returnCode = "-1";
                returnData = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(new { Code = returnCode, Data = returnData }, JsonRequestBehavior.AllowGet);
        }
        [NonAction]
        public dynamic getUserSet(Int64? ToUserID)
        {
            try
            {
                var userObj = (from USR in css.Users
                               join Mngr in css.Users on USR.MgrId equals Mngr.UserId
                                   into subRS
                               from RS in subRS.DefaultIfEmpty()
                               where (USR.UserId == ToUserID)
                               select new
                               {
                                   Email = (string.IsNullOrEmpty(USR.Email) ? (RS == null ? String.Empty : RS.Email) : USR.Email),
                                   FirstName = USR.FirstName,
                                   LastName = USR.LastName
                               }).FirstOrDefault();

                return userObj;
            }
            catch (Exception ex)
            {
                Utility.LogException("Method getUserSet Controller:AutoAssignment", ex, "An error occured while retriving data for SP:" + ToUserID);
                return null;
            }
        }

        public ActionResult AutoNotesInsert(Int64 assignmentid, string Subject, string Comment, bool Isprivate, bool isBillable, double? noOfHours)
        {
            #region Auto Notes Insert
            CSS.Models.User user = CSS.AuthenticationUtility.GetUser();
            ObjectParameter outAutoNoteId = new ObjectParameter("AutoNoteId", DbType.Int64);
            DateTime NoteCreatedDate = DateTime.Now;
            var autoassignment = css.AutoAssignments.Where(x => x.AutoAssignmentId == assignmentid).FirstOrDefault();
            Int64? ToUserID;
            if (user.UserId == autoassignment.RepresentativeID)
            {
                ToUserID = autoassignment.OAUserID;
            }
            else
            {
                ToUserID = autoassignment.RepresentativeID;
            }
            css.usp_AutoNotesInsert(outAutoNoteId, assignmentid, Subject, Comment, NoteCreatedDate, Isprivate, false, user.UserId, ToUserID, isBillable, noOfHours, null, null, null);

            
            // start INPSW - 9
            //#region Email Notification to SP or Claim Representative


            //try
            //{
            //    List<string> toEmails = new List<string>();
            //    //var userObj = css.Users.Where(x => x.UserId == ToUserID).
            //    //    Select(x => new { Email = x.Email, FirstName = x.FirstName, LastName=x.LastName }).FirstOrDefault();
            //    var userObj = getUserSet(ToUserID);
            //    if (userObj != null)
            //    {
            //        toEmails.Add(userObj.Email);
            //    }
            //    if (toEmails.Count > 0)
            //    {

            //        string mailBody = "Dear " + userObj.FirstName + " " + userObj.LastName;
            //        mailBody += "<br />";
            //        mailBody += "<br />";
            //        mailBody += "<br /><b>Claim Number: " + autoassignment.Claim.ClaimNumber + "</b>";
            //        mailBody += "<br />";
            //        mailBody += "<br />Note:" + Comment;
            //        mailBody += "<br />";
            //        mailBody += "<br />";
            //        mailBody += "<br />";
            //        mailBody += "<br />";
            //        mailBody += "<br />Thanking you,";
            //        mailBody += "<br />";
            //        mailBody += "<br />Support,";
            //        mailBody += "<br />IADA Claim Solutions Inc.";
            //        Utility.sendEmail(toEmails, MvcApplication.AdminMail, Subject, mailBody);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Utility.LogException("method AutoNotesInsert", ex, "An Error occured while sending email notification:" + assignmentid);
            //}
            //#endregion

            // end INPSW - 9

            if (user.UserTypeId == 2||  user.UserTypeId == 12)
            {
                List<Int64> ID = new List<Int64>();
                ID.Add((Int64) autoassignment.OAUserID);
                ID.Add((Int64)autoassignment.RepresentativeID);
                for (int i = 0; i < ID.Count(); i++ )
                {
                    NoteInsertMailSend(ID[i], autoassignment.Claim.ClaimNumber, Comment, Subject, assignmentid); //INPSW - 9
                }
                    
            }

            else
            {
                NoteInsertMailSend(ToUserID, autoassignment.Claim.ClaimNumber, Comment, Subject, assignmentid); //INPSW - 9
            }
            
            
            //return Json(1);
            //return PartialView("_AutoNotes");
            return Json(1, JsonRequestBehavior.AllowGet);
            #endregion
        }

        //INPSW - 9
        public void NoteInsertMailSend(Int64 ?ToUserID, String ClaimNumber, String Comment, string Subject, Int64 assignmentid)
        {
            #region Email Notification to SP or Claim Representative
            CSS.Models.User user = CSS.AuthenticationUtility.GetUser();
           var UserInfo = (from USR in css.Users                           
                           where (USR.UserId == user.UserId)
                           select new
                           {                               
                               FirstName = USR.FirstName,
                               LastName = USR.LastName
                           }).FirstOrDefault();
            
            try
            {
                List<string> toEmails = new List<string>();
                //var userObj = css.Users.Where(x => x.UserId == ToUserID).
                //    Select(x => new { Email = x.Email, FirstName = x.FirstName, LastName=x.LastName }).FirstOrDefault();
                var userObj = getUserSet(ToUserID);
                if (userObj != null)
                {
                    toEmails.Add(userObj.Email);
                }
                if (toEmails.Count > 0)
                {
                    string mailBody = "<br />";
                    mailBody += "DO NOT REPLY TO THIS EMAIL. THIS IS A SYSTEM GENERATED EMAIL. THE EMAIL MAILBOX IS NOT MONITORED.";
                    mailBody += "<br /><br />";   
                    mailBody += "Dear " + userObj.FirstName + " " + userObj.LastName+",";
                    mailBody += "<br />";
                    mailBody += "<br />";
                    mailBody += "The following note has been added to:";
                    mailBody += "<br />";
                    mailBody += "<br /> Claim Number: " + ClaimNumber;
                    mailBody += "<br /> From: "+ UserInfo.FirstName + " " + UserInfo.LastName;                 
                    mailBody += "<br />";
                    mailBody += "<br />Note:  " + Comment;
                    mailBody += "<br />";
                    mailBody += "<br />";
                    mailBody += "To review or respond to this Note, log into <a href=\"" + ConfigurationManager.AppSettings["IadaLoginUrl"] + "\">www.IADApro.com</a>.";
                    mailBody += "<br />";
                    mailBody += "<br />";
                    mailBody += "Thank you,";
                    mailBody += "<br />IADA Customer Support Team";
                    Utility.sendEmail(toEmails, MvcApplication.AdminMail, "Claim #: " + ClaimNumber + " - File Note Notification", mailBody);
                }
            }
            catch (Exception ex)
            {
                Utility.LogException("method AutoNotesInsert", ex, "An Error occured while sending email notification:" + assignmentid);
            }
            #endregion
        }


        [HttpPost]
        public ActionResult AutoAssignment(AutoAssignmentViewModel viewmodel, FormCollection Form, Int64 claimid = -1)
        {
            // string claim = form["claim.ClaimNumber"].ToString();
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                BLL.User LoggedInUser = css.Users.Find(loggedInUser.UserId);
                ModelState.Clear();
                string SPID = Form["selectedassignmentID"];
                if (IsFormValid(viewmodel, claimid))
                {
                    string PlateState = "";
                    if (viewmodel.autoassignment.ServiceId == 9|| viewmodel.autoassignment.ServiceId == 11|| viewmodel.autoassignment.ServiceId == 12)
                    {
                        viewmodel.claim.OwnerFirstName = "";
                        viewmodel.claim.OwnerLastName = "";
                        viewmodel.claim.OwnerAddress1 = "";
                        viewmodel.claim.OwnerCity = "";
                        viewmodel.claim.OwnerState = "";
                        viewmodel.claim.OwnerZip = "";
                        viewmodel.claim.OwnerHomePhone = "";
                        viewmodel.claim.OwnerBusinessPhone = "";
                        viewmodel.claim.OwnerMobilePhone = "";
                        viewmodel.claim.OwnerContactEmail = "";
                        viewmodel.claim.AutoModel = "";
                        viewmodel.claim.Make = "";
                        viewmodel.claim.Year = "";
                        viewmodel.claim.LicensePlate = "";
                        viewmodel.claim.VIN_ = "";
                        viewmodel.claim.AutoInstructions = "";
                        viewmodel.claim.AutoDamage = "";
                        viewmodel.claim.AutoColor = "";
                        viewmodel.autoassignment.IsDriveable = "";
                        viewmodel.claim.AutoColor = "";
                        viewmodel.claim.VehicleContactEmail = "";
                        
                    }
                    else
                    {
                        //PlateState = viewmodel.vehicleClaimDetail.PlateState;
                        PlateState = viewmodel.claim.LicensePlate;
                    }
                    if (claimid == -1)
                    {
                        #region Auto Claims Insert
                        if (Request.IsAjaxRequest())
                        {
                           // string ClaimTypeId = Form["claim.ClaimTypeId"];
                           // viewmodel.ServiceTypeId = Convert.ToInt32(ClaimTypeId);
                        }
                        ObjectParameter outAutoClaimId = new ObjectParameter("claimId", DbType.Int64);

                        int? headCompanyId = null;

                        if (viewmodel.claim.HeadCompanyId.HasValue)
                        {
                            if (viewmodel.claim.HeadCompanyId.Value != 0)
                            {
                                headCompanyId = viewmodel.claim.HeadCompanyId.Value;
                            }
                        }
						//INPSW-32 start
                        ObjectParameter outAutoAssignmentId = new ObjectParameter("AutoAssignmentId", DbType.Int64);
                        if (viewmodel.isManualInvoiceClaim)
                        {
                            viewmodel.isManualInvoiceClaim = true;

                            css.usp_AVSN_Manual_ClaimAssignmentInsertUpdate
                                (
                                    null,
                                    viewmodel.claim.ClaimNumber,
                                    null,
                                    true,
                                    false,
                                    false,
                                    viewmodel.claim.PolicyNumber,
                                    viewmodel.claim.CauseTypeOfLoss,
                                    viewmodel.claim.DateofLoss,
                                    viewmodel.claim.InsuredFirstName,
                                    viewmodel.claim.InsuredLastName,
                                    viewmodel.claim.InsuredAddress1,
                                    "-",
                                    viewmodel.claim.InsuredCity,
                                    viewmodel.claim.InsuredState,
                                    viewmodel.claim.InsuredZip,
                                    viewmodel.claim.InsuredEmail,
                                    viewmodel.claim.PropertyAddress1,
                                    "-",
                                    viewmodel.claim.PropertyCity,
                                    viewmodel.claim.PropertyState,
                                    viewmodel.claim.PropertyZip,
                                    viewmodel.claim.TotalDeductible,
                                    null,
                                    null,
                                    "USA",
                                    viewmodel.claim.InsuredBusinessPhone,
                                    viewmodel.claim.InsuredMobilePhone,
                                    viewmodel.claim.insured_FAX,
                                    viewmodel.claim.FAXExtension,
                                    viewmodel.claim.OwnerFirstName,
                                    viewmodel.claim.OwnerLastName,
                                    viewmodel.claim.OwnerAddress1,
                                    viewmodel.claim.OwnerAddress2,
                                    viewmodel.claim.OwnerCity,
                                    viewmodel.claim.OwnerState,
                                    viewmodel.claim.OwnerZip,
                                    "USA",
                                    viewmodel.claim.OwnerMobilePhone,
                                    viewmodel.claim.OwnerBusinessPhone,
                                    viewmodel.claim.OwnerFax,
                                    viewmodel.claim.OwnerFaxExtension,
                                    viewmodel.claim.OwnerContactEmail,
                                    viewmodel.claim.InsuredContactEmail,
                                    viewmodel.claim.VehicleContactEmail,
                                    viewmodel.claim.PropertyContactName,
                                    viewmodel.claim.PropertyContactPhone,
                                    viewmodel.claim.Code,
                                    viewmodel.claim.LicensePlate,
                                    viewmodel.claim.VIN_,
                                    viewmodel.claim.Year,
                                    viewmodel.claim.Make,
                                    viewmodel.claim.AutoModel,
                                    viewmodel.claim.AutoColor,
                                    viewmodel.claim.AutoDamage,
                                    viewmodel.claim.AVSNIsTotalLoss,
                                    viewmodel.claim.AVSNPolicyAgentFN,
                                    viewmodel.claim.AVSNPolicyAgentLN,
                                    viewmodel.claim.AVSNPolicyAgentLicence,
                                    viewmodel.claim.AVSNPolicyAgentPh,
                                    viewmodel.claim.AVSNPolicyAgentPhExt,
                                    DateTime.UtcNow,
                                    viewmodel.autoassignment.AutoAssignmentDescription,
                                    null
                                );
                        }
						//INPSW-32 end
                        else
                        {
                            //viewmodel.isManualInvoiceClaim = false;
                            //viewmodel.isNuGenClaim = false;
                            //viewmodel.IsAvsnAssignment = false;


                            DateTime ClaimCreatedDate = DateTime.UtcNow;
                            css.AutoClaimsInsert_Test(outAutoClaimId, viewmodel.claim.ClaimNumber, viewmodel.claim.HeadCompanyId, viewmodel.claim.LOBId, viewmodel.claim.ClientPointOfContactUserId, viewmodel.claim.PolicyNumber, ClaimCreatedDate,
                                                viewmodel.claim.TotalDeductible, viewmodel.claim.CauseTypeOfLoss, viewmodel.claim.DateofLoss, viewmodel.claim.InsuredFirstName, viewmodel.claim.PropertyAddress1,
                                                viewmodel.claim.PropertyCity, viewmodel.claim.PropertyState, viewmodel.claim.PropertyZip, viewmodel.claim.PropertyContactName, viewmodel.claim.PropertyContactPhone, viewmodel.claim.InsuredEmail,
                                                viewmodel.claim.OwnerFirstName, viewmodel.claim.OwnerLastName, viewmodel.claim.OwnerAddress1, viewmodel.claim.OwnerCity, viewmodel.claim.OwnerState, viewmodel.claim.OwnerZip,
                                                viewmodel.claim.OwnerHomePhone, viewmodel.claim.OwnerBusinessPhone, viewmodel.claim.OwnerMobilePhone, viewmodel.claim.OwnerContactEmail, viewmodel.claim.InsuredContactEmail, viewmodel.claim.VehicleContactEmail, viewmodel.claim.AutoModel, viewmodel.claim.Make, viewmodel.claim.Year,
                                                viewmodel.claim.LicensePlate, viewmodel.claim.VIN_, viewmodel.claim.AutoInstructions, viewmodel.claim.AutoDamage, viewmodel.claim.AutoColor, viewmodel.claim.ReferrerTypeId,
                                                viewmodel.claim.ClaimTypeId, viewmodel.claim.VehicleLocationID, viewmodel.claim.RCV, viewmodel.claim.InsuredLastName, viewmodel.claim.InsuredAddress1,
                                                viewmodel.claim.InsuredCity, viewmodel.claim.InsuredState, viewmodel.claim.InsuredZip, viewmodel.claim.InsuredHomePhone, viewmodel.claim.InsuredBusinessPhone, viewmodel.claim.InsuredMobilePhone, viewmodel.ServiceTypeId);
                            claimid = Convert.ToInt64(outAutoClaimId.Value);

                        #endregion

                        #region Auto Assignment Insert

                        DateTime AutoAssignmentDate = DateTime.UtcNow;
                            //ObjectParameter outAutoAssignmentId = new ObjectParameter("AutoAssignmentId", DbType.Int64);//INPSW-32
                        Int64 AutoAssignmentId;
                        viewmodel.autoassignment.FileStatus = 2; //2 is the unassigned 
                        viewmodel.autoassignment.CSSQAAgentUserId = 1; //set default for Invoice Validation
                        AutoAssignmentId = css.AutoAssignmentInsert(outAutoAssignmentId, Convert.ToInt64(claimid), AutoAssignmentDate, viewmodel.autoassignment.AutoAssignmentDescription, viewmodel.autoassignment.CSSPointofContactUserId, viewmodel.autoassignment.CSSQAAgentUserId, viewmodel.autoassignment.FileStatus, viewmodel.autoassignment.ServiceId, viewmodel.autoassignment.RepresentativeID);
                        css.SaveChanges();

                            #endregion

                        }

                        if (SPID != null)
                        {
                            NewAssignmentController controller = new NewAssignmentController();
                            controller.ControllerContext = this.ControllerContext;
                            controller.AutoAssignServiceProvider(Convert.ToInt64(outAutoAssignmentId.Value), Convert.ToInt64(SPID));
                        }
                        if (viewmodel.autoassignment.ServiceId == 9 || viewmodel.autoassignment.ServiceId == 11 || viewmodel.autoassignment.ServiceId == 12)
                        {
                           // CopyAssignmentToOPT(Convert.ToInt64(outAutoAssignmentId.Value));
                        }
                        
                    }
                    else
                    {
                        #region Auto Claim Update
                        css.AutoClaimUpdate_Test(claimid, viewmodel.claim.ClaimNumber, viewmodel.claim.HeadCompanyId, viewmodel.claim.PolicyNumber, viewmodel.claim.ClaimCreatedDate,
                                            viewmodel.claim.TotalDeductible, viewmodel.claim.CauseTypeOfLoss, viewmodel.claim.DateofLoss, viewmodel.claim.InsuredFirstName,
                                            viewmodel.claim.PropertyAddress1, viewmodel.claim.PropertyCity, viewmodel.claim.PropertyState, viewmodel.claim.PropertyZip,
                                            viewmodel.claim.PropertyContactName, viewmodel.claim.PropertyContactPhone, viewmodel.claim.OwnerFirstName, viewmodel.claim.OwnerLastName,
                                            viewmodel.claim.OwnerAddress1, viewmodel.claim.OwnerCity, viewmodel.claim.OwnerState, viewmodel.claim.OwnerZip, viewmodel.claim.OwnerHomePhone, viewmodel.claim.InsuredEmail,
                                            viewmodel.claim.OwnerBusinessPhone, viewmodel.claim.OwnerMobilePhone, viewmodel.claim.OwnerContactEmail, viewmodel.claim.InsuredContactEmail, viewmodel.claim.VehicleContactEmail, viewmodel.claim.AutoModel, viewmodel.claim.Make, viewmodel.claim.Year,
                                            viewmodel.claim.LicensePlate, viewmodel.claim.VIN_, viewmodel.claim.AutoInstructions, viewmodel.claim.AutoDamage, viewmodel.claim.AutoColor,
                                            viewmodel.claim.ReferrerTypeId, viewmodel.claim.ClaimTypeId, viewmodel.claim.VehicleLocationID, viewmodel.claim.LOBId, viewmodel.claim.ClientPointOfContactUserId, viewmodel.claim.RCV,
                                            viewmodel.autoassignment.IsDriveable, viewmodel.claim.Code, viewmodel.claimReferrer == null ? null : viewmodel.claimReferrer.ReferrerDeductibleStatus, PlateState, viewmodel.claim.InsuredLastName, viewmodel.claim.InsuredAddress1,
                                            viewmodel.claim.InsuredCity, viewmodel.claim.InsuredState, viewmodel.claim.InsuredZip, viewmodel.claim.InsuredHomePhone, viewmodel.claim.InsuredBusinessPhone, viewmodel.claim.InsuredMobilePhone);
                        css.SaveChanges();
                        #endregion


                        #region Auto Assignment Update
                        viewmodel.autoassignment.CSSQAAgentUserId = 1; //set default for Invoice Validation
                        css.AutoAssignmentUpdate(viewmodel.autoassignment.AutoAssignmentId, claimid, viewmodel.autoassignment.AutoAssignmentDate, viewmodel.autoassignment.AutoAssignmentDescription,
                                                    viewmodel.autoassignment.CSSPointofContactUserId, viewmodel.autoassignment.ServiceId, viewmodel.autoassignment.RepresentativeID);
                        #endregion

                    }
                    if (Request.IsAjaxRequest())
                    {

                        var successmsg = new { id = 1, errorList = "" };
                        return Json(successmsg, JsonRequestBehavior.AllowGet);


                    }
                    TempData["UseMasterPageLayout"] = viewmodel.UseMasterPageLayout;
                    //if(viewmodel.autoassignment.ServiceId == 9|| viewmodel.autoassignment.ServiceId == 11|| viewmodel.autoassignment.ServiceId == 12)
                    //    return RedirectToAction("PropertySearch");
					//Inpsw-32 
                    if (viewmodel.isManualInvoiceClaim)
                    {
                        return RedirectToAction("InvoiceAutoSearch");
                    }
                        return RedirectToAction("AutoSearch");
                }
                else
                {
                    //Bind Ins Company and Claim rep 
                    int UserTypeID = Convert.ToInt32(LoggedInUser.UserTypeId);
                    //int CompanyID = Convert.ToInt32(LoggedInUser.HeadCompanyId);
                    int CompanyID = Convert.ToInt32(viewmodel.claim.HeadCompanyId);
                   // int UserID = Convert.ToInt32(LoggedInUser.UserId);
     
                    viewmodel.ClaimRepresentativeList = ClaimRepresentativeList(UserTypeID, CompanyID);
                    viewmodel.InsuranceCompaniesList = InsuranceCompanies(UserTypeID, CompanyID);


                    bool RedirectToSpMap = Convert.ToBoolean(Form["RedirectToSpMap"]);
                    if (Request.IsAjaxRequest())
                    {
                        List<object> errorlist = new List<object>();
                        foreach (var modelStateKey in ViewData.ModelState.Keys)
                        {
                            var modelStateVal = ViewData.ModelState[modelStateKey];

                            foreach (var error in modelStateVal.Errors)
                            {

                                var newError = new { key = modelStateKey.Substring(modelStateKey.IndexOf(".") + 1), Message = error.ErrorMessage };
                                errorlist.Add(newError);
                                // You may log the errors if you want
                            }
                        }
                        var returnObj = new { id = 0, errorList = errorlist };
                        return Json(returnObj, JsonRequestBehavior.AllowGet);


                    }
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }

            return View(viewmodel);
        }
        //private bool CopyAssignmentToOPT(long AssignmentId)
        //{
        //    #region web api
        //    //#01. TPA to Opt ------------------------------------------------------

        //    List<IAssignmnetGetDetails_Result> LstIAssignment = new List<IAssignmnetGetDetails_Result>();
        //    bool IsProcessed = false;
        //    LstIAssignment = css.IAssignmnetGetDetails(AssignmentId).ToList();

        //    foreach (IAssignmnetGetDetails_Result lst in LstIAssignment)
        //    {
        //        lst.HeadCompanyId = Convert.ToInt32(ConfigurationManager.AppSettings["IADAToOpHeadComapnyID"]);
        //        lst.ClientPointOfContactUserId = Convert.ToInt32(ConfigurationManager.AppSettings["IADAToOpClientPointOfContactUserId"]);

        //        HttpClient client = new HttpClient();
        //        client.BaseAddress = new Uri(ConfigurationManager.AppSettings["IADAToOptWebApiBaseUrl"].ToString());

        //        client.DefaultRequestHeaders.Accept.Add(
        //           new MediaTypeWithQualityHeaderValue("application/json"));
        //        var response = client.PostAsJsonAsync("InsertUpdateAssignment", LstIAssignment).Result;

        //        if (response.IsSuccessStatusCode)
        //        {
        //            IsProcessed = true;
        //        }
        //        else
        //        {
        //            IsProcessed = false;
        //        }
        //    }
        //    #endregion
        //    return IsProcessed;
        //}

        public bool IsFormValid(AutoAssignmentViewModel viewmodel, Int64 claimid)
        {
            bool IsValid = true;

            //claiminfo Insurance Comp
            if (viewmodel.claim.HeadCompanyId == 0 || viewmodel.claim.HeadCompanyId == null)
            {
                IsValid = false;
                ModelState.AddModelError("HeadCompanyId", "Insurance Company is required.");
            }

            #region ClaimInfo validation
            //Service Type
            if (viewmodel.autoassignment.ServiceId == 0 || viewmodel.autoassignment.ServiceId == null)
            {
                IsValid = false;
                ModelState.AddModelError("Service Type", "Service Type is required.");
            }

            //claiminfo DateofLoss
            if (String.IsNullOrEmpty(viewmodel.claim.ClaimNumber))
            {
                IsValid = false;
                ModelState.AddModelError("ClaimNumber", "Claim Number is required.");
            }
            //claimInfo Duplicate check
            if (!String.IsNullOrEmpty(viewmodel.claim.ClaimNumber) && claimid == -1)
            {
                if (css.Claims.Where(x => x.ClaimNumber.Trim() == viewmodel.claim.ClaimNumber.Trim()).Count() > 0)
                {
                    IsValid = false;
                    ModelState.AddModelError("ClaimNumber", "Claim Number is already Exists.");
                }
            }
            //claiminfo Policy Number
            if (String.IsNullOrEmpty(viewmodel.claim.PolicyNumber))
            {
                IsValid = false;
                ModelState.AddModelError("PolicyNumber", "Policy Number is required.");
            }

            //claimInfo Claim for
            if (viewmodel.claim.ReferrerTypeId == 0 || viewmodel.claim.ReferrerTypeId == null)
            {
                IsValid = false;
                ModelState.AddModelError("ReferrerTypeId", "Claim For is required.");
            }

            //claimInfo Claim for
            if (viewmodel.claim.CauseTypeOfLoss == 0 || viewmodel.claim.CauseTypeOfLoss == null)
            {
                IsValid = false;
                ModelState.AddModelError("CauseTypeOfLoss", "Type of Loss is required.");
            }

            //claimInfo Claim for
            if (viewmodel.claim.ClaimTypeId == 0 || viewmodel.claim.ClaimTypeId == null)
            {
                IsValid = false;
                ModelState.AddModelError("ClaimTypeId", "Claim Type is required.");
            }

            //claiminfo Insured Name
            if (String.IsNullOrEmpty(viewmodel.claim.InsuredFirstName))
            {
                IsValid = false;
                ModelState.AddModelError("InsuredFirstName", "Insured Name is required.");
            }

            //claiminfo DateofLoss
            if (viewmodel.claim.DateofLoss != null)
            {
                if (viewmodel.claim.DateofLoss.Value.Date > DateTime.Now.Date)
                {
                    IsValid = false;
                    ModelState.AddModelError("DateofLoss", "Loss Date cannot be a future date.");
                }
            }
            else
            {
                IsValid = false;
                ModelState.AddModelError("DateofLoss", "Loss Date is required.");
            }
            #endregion

            #region OwnerInfo validation
            //owner First name
            if (String.IsNullOrEmpty(viewmodel.claim.OwnerFirstName) && (viewmodel.autoassignment.ServiceId != 9 && viewmodel.autoassignment.ServiceId != 11 && viewmodel.autoassignment.ServiceId != 12))
            {
                IsValid = false;
                ModelState.AddModelError("OwnerFirstName", "Owner First Name is required.");
            }

            //should contain atleast one owner phone number.
            if (String.IsNullOrEmpty(viewmodel.claim.OwnerBusinessPhone) && String.IsNullOrEmpty(viewmodel.claim.OwnerHomePhone) && String.IsNullOrEmpty(viewmodel.claim.OwnerMobilePhone) && (viewmodel.autoassignment.ServiceId != 9 && viewmodel.autoassignment.ServiceId != 11 && viewmodel.autoassignment.ServiceId != 12))
            {
                IsValid = false;
                ModelState.AddModelError("OwnerMobilePhone", "One Owner Phone is required.");
            }

            //owner address
            if (String.IsNullOrEmpty(viewmodel.claim.OwnerAddress1) && (viewmodel.autoassignment.ServiceId != 9 && viewmodel.autoassignment.ServiceId != 11 && viewmodel.autoassignment.ServiceId != 12))
            {
                IsValid = false;
                ModelState.AddModelError("OwnerAddress1", "Owner Address is required.");
            }

            //Owner Zip
            if (!String.IsNullOrEmpty(viewmodel.claim.OwnerZip) && (viewmodel.autoassignment.ServiceId != 9 && viewmodel.autoassignment.ServiceId != 11 && viewmodel.autoassignment.ServiceId != 12))
            {
                if (!Utility.isZipCodeValid(viewmodel.claim.OwnerZip))
                {
                    IsValid = false;
                    ModelState.AddModelError("OwnerZip", "Invalid Owner Zip.");
                }
            }
            else if(String.IsNullOrEmpty(viewmodel.claim.OwnerZip) && (viewmodel.autoassignment.ServiceId != 9 && viewmodel.autoassignment.ServiceId != 11 && viewmodel.autoassignment.ServiceId != 12))
            {
                IsValid = false;
                ModelState.AddModelError("OwnerZip", "Owner Zip is required.");
            }

            //owner state
            if ((viewmodel.claim.OwnerState == "0" || viewmodel.claim.OwnerState == null) && (viewmodel.autoassignment.ServiceId != 9 && viewmodel.autoassignment.ServiceId != 11 && viewmodel.autoassignment.ServiceId != 12))
            {
                IsValid = false;
                ModelState.AddModelError("OwnerState", "Owner State is required.");
            }
            #endregion

            #region Vehicle Loc validation
            //Vehicle Location
            if (viewmodel.claim.VehicleLocationID == 0 && (viewmodel.autoassignment.ServiceId != 9 && viewmodel.autoassignment.ServiceId != 11 && viewmodel.autoassignment.ServiceId != 12))
            {
                IsValid = false;
                ModelState.AddModelError("VehicleLocationID", "Vehicle Location is required.");
            }

            //Vehicle Address
            if (String.IsNullOrEmpty(viewmodel.claim.PropertyAddress1) && (viewmodel.autoassignment.ServiceId != 9 && viewmodel.autoassignment.ServiceId != 11 && viewmodel.autoassignment.ServiceId != 12))
            {
                IsValid = false;
                ModelState.AddModelError("PropertyContactPhone", "Vehicle Address is required.");
            }

            //Vehicle Zip
            if (!String.IsNullOrEmpty(viewmodel.claim.PropertyZip) && (viewmodel.autoassignment.ServiceId != 9 && viewmodel.autoassignment.ServiceId != 11 && viewmodel.autoassignment.ServiceId != 12))
            {
                if (!Utility.isZipCodeValid(viewmodel.claim.PropertyZip))
                {
                    IsValid = false;
                    ModelState.AddModelError("PropertyZip", "Invalid Vehicle Zip.");
                }
            }
            else if (String.IsNullOrEmpty(viewmodel.claim.PropertyZip) && (viewmodel.autoassignment.ServiceId != 9 && viewmodel.autoassignment.ServiceId != 11 && viewmodel.autoassignment.ServiceId != 12))
            {
                IsValid = false;
                ModelState.AddModelError("PropertyZip", "Vehicle Zip is required.");
            }

            //Vehicle State
            if ((viewmodel.claim.PropertyState == "0" || viewmodel.claim.PropertyState == null) && (viewmodel.autoassignment.ServiceId != 9 && viewmodel.autoassignment.ServiceId != 11 && viewmodel.autoassignment.ServiceId != 12))
            {
                IsValid = false;
                ModelState.AddModelError("PropertyState", "Vehicle State is required.");
            }

            //Vehicle Contact Phone
            if (String.IsNullOrEmpty(viewmodel.claim.PropertyContactPhone) && (viewmodel.autoassignment.ServiceId != 9 && viewmodel.autoassignment.ServiceId != 11 && viewmodel.autoassignment.ServiceId != 12))
            {
                IsValid = false;
                ModelState.AddModelError("PropertyContactPhone", "Contact Phone is required.");
            }
            #endregion

            if ((viewmodel.autoassignment.ServiceId == 9 || viewmodel.autoassignment.ServiceId == 11 || viewmodel.autoassignment.ServiceId == 12))
            {
                #region Insured validation
                //Insured First name
                if (String.IsNullOrEmpty(viewmodel.claim.InsuredFirstName))
                {
                    IsValid = false;
                    ModelState.AddModelError("InsuredFirstName", "Insured First Name is required.");
                }

                //should contain atleast one Insured phone number.
                if (String.IsNullOrEmpty(viewmodel.claim.InsuredBusinessPhone) && String.IsNullOrEmpty(viewmodel.claim.InsuredHomePhone) && String.IsNullOrEmpty(viewmodel.claim.InsuredMobilePhone) )
                {
                    IsValid = false;
                    ModelState.AddModelError("InsuredMobilePhone", "One Insured Phone is required.");
                }

                //Insured address
                if (String.IsNullOrEmpty(viewmodel.claim.InsuredAddress1) )
                {
                    IsValid = false;
                    ModelState.AddModelError("InsuredAddress1", "Insured Address is required.");
                }

                //Insured Zip
                if (!String.IsNullOrEmpty(viewmodel.claim.InsuredZip))
                {
                    if (!Utility.isZipCodeValid(viewmodel.claim.InsuredZip))
                    {
                        IsValid = false;
                        ModelState.AddModelError("InsuredZip", "Invalid Insured Zip.");
                    }
                }
                else if (String.IsNullOrEmpty(viewmodel.claim.InsuredZip) )
                {
                    IsValid = false;
                    ModelState.AddModelError("InsuredZip", "Insured Zip is required.");
                }

                //Insured state
                if ((viewmodel.claim.InsuredState == "0" || viewmodel.claim.InsuredState == null))
                {
                    IsValid = false;
                    ModelState.AddModelError("InsuredState", "Insured State is required.");
                }
                #endregion

                #region Property Loc validation
                            
                //Property Address
                if (String.IsNullOrEmpty(viewmodel.claim.PropertyAddress1))
                {
                    IsValid = false;
                    ModelState.AddModelError("PropertyAddress1", "Property Address is required.");
                }

                //Property Zip
                if (!String.IsNullOrEmpty(viewmodel.claim.PropertyZip))
                {
                    if (!Utility.isZipCodeValid(viewmodel.claim.PropertyZip))
                    {
                        IsValid = false;
                        ModelState.AddModelError("PropertyZip", "Invalid Property Zip.");
                    }
                }
                else if (String.IsNullOrEmpty(viewmodel.claim.PropertyZip))
                {
                    IsValid = false;
                    ModelState.AddModelError("PropertyZip", "Property Zip is required.");
                }

                //Property State
                if ((viewmodel.claim.PropertyState == "0" || viewmodel.claim.PropertyState == null) )
                {
                    IsValid = false;
                    ModelState.AddModelError("PropertyState", "Property State is required.");
                }

                //Property Contact Phone
                if (String.IsNullOrEmpty(viewmodel.claim.PropertyContactPhone))
                {
                    IsValid = false;
                    ModelState.AddModelError("PropertyContactPhone", "Contact Phone is required.");
                }
                #endregion
            }

            if (!ModelState.IsValid)
            {
                IsValid = false;
            }

            if (IsValid == true && viewmodel.claim.ClientPointOfContactUserId == 0)
            {
                viewmodel.claim.ClientPointOfContactUserId = null;
            }
            return IsValid;

        }

        public ActionResult AutoSubmitSuccess()
        {
            return View();
        }

        public ActionResult AutoSearch(string reviewType)
        {
            XactAutoAssignmentSearchViewModel viewmodel = new XactAutoAssignmentSearchViewModel();
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();


            TempData["reviewType"] = reviewType;
            if (viewmodel.Pager == null)
            {
                viewmodel.Pager = new BLL.Models.Pager();
                viewmodel.Pager.Page = 1;
                viewmodel.Pager.RecsPerPage = 15;
                viewmodel.Pager.FirstPageNo = 1;
                viewmodel.Pager.IsAjax = false;
                viewmodel.Pager.FormName = "formAutoSearch";
            }

            BLL.User user = css.Users.Find(loggedInUser.UserId);

            if (user.UserTypeId == 3) // 3 is Claim Representative
            {
                viewmodel.InsuranceCompany = Convert.ToInt32(user.HeadCompanyId);
            }
            viewmodel.ServiceId = 0;
            viewmodel.AutoClaimsInfo = GetAutoAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewmodel).ToList();

            //viewmodel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.GENERAL;
            //viewmodel.AutoClaimsInfo = GetAutoAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewmodel).ToList();
            //viewmodel.Pager.TotalCount = viewmodel.AutoClaimsInfo[0].TotalRecords.ToString();
            //Session["XactAssignmentSearch"] = null;
            if (viewmodel.AutoClaimsInfo.Count != 0)
            {
                viewmodel.Pager.TotalCount = viewmodel.AutoClaimsInfo[0].TotalRecords.ToString(); // getAssignmentSearchResult(loggedInUser, viewModel).Count().ToString();
            }
            else
            {
                viewmodel.Pager.TotalCount = "0";
            }


            viewmodel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewmodel.Pager.TotalCount) / Convert.ToInt32(viewmodel.Pager.RecsPerPage)))).ToString();


            return View(viewmodel);
        }
        public ActionResult PropertySearch(string reviewType)
        {
            XactAutoAssignmentSearchViewModel viewmodel = new XactAutoAssignmentSearchViewModel();
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();


            TempData["reviewType"] = reviewType;
            if (viewmodel.Pager == null)
            {
                viewmodel.Pager = new BLL.Models.Pager();
                viewmodel.Pager.Page = 1;
                viewmodel.Pager.RecsPerPage = 15;
                viewmodel.Pager.FirstPageNo = 1;
                viewmodel.Pager.IsAjax = false;
                viewmodel.Pager.FormName = "formPropertySearch";
            }

            BLL.User user = css.Users.Find(loggedInUser.UserId);

            if (user.UserTypeId == 3) // 3 is Claim Representative
            {
                viewmodel.InsuranceCompany = Convert.ToInt32(user.HeadCompanyId);
            }
            viewmodel.ServiceId = 9;
            viewmodel.AutoClaimsInfo = GetAutoAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewmodel).ToList();

            //viewmodel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.GENERAL;
            //viewmodel.AutoClaimsInfo = GetAutoAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewmodel).ToList();
            //viewmodel.Pager.TotalCount = viewmodel.AutoClaimsInfo[0].TotalRecords.ToString();
            //Session["XactAssignmentSearch"] = null;
            if (viewmodel.AutoClaimsInfo.Count != 0)
            {
                viewmodel.Pager.TotalCount = viewmodel.AutoClaimsInfo[0].TotalRecords.ToString(); // getAssignmentSearchResult(loggedInUser, viewModel).Count().ToString();
            }
            else
            {
                viewmodel.Pager.TotalCount = "0";
            }


            viewmodel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewmodel.Pager.TotalCount) / Convert.ToInt32(viewmodel.Pager.RecsPerPage)))).ToString();


            return View(viewmodel);
        }

        public ActionResult Accepted(Int64 AssignmentId, bool IsAccepted, Int64 claimid)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            css.AutoAssignmentIsAccepted(AssignmentId, 15, IsAccepted, loggedInUser.UserId);
            return RedirectToAction("Details", new { claimid = Cypher.EncryptString(claimid.ToString()) });
        }

        [HttpPost]
        public ActionResult AutoSearch(XactAutoAssignmentSearchViewModel viewmodel, FormCollection form, string SubmitButton = null)
        {
            DateTime From = Convert.ToDateTime(viewmodel.DateFrom);
            DateTime To = Convert.ToDateTime(viewmodel.DateTo);
            string hdnSearchcriteria = "";
            if ((form["hdnSearchcriteria"]) != "" && (form["hdnSearchcriteria"]) != null)
            {

                hdnSearchcriteria = form["hdnSearchcriteria"].ToString();

            }

            //if (From > To)
            //{
            //    return new JavascriptResult() { Script = "alert('Successfully registered');" };
            //}

            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

            viewmodel.Pager = new BLL.Models.Pager();
            viewmodel.Pager.IsAjax = false;
            viewmodel.Pager.FormName = "formAutoSearch";

            if ((form["hdnCurrentPage"]) != "" && (form["hdnCurrentPage"]) != null)
            {

                viewmodel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);

            }
            else
            {

                viewmodel.Pager.Page = 1;
            }
            viewmodel.Pager.RecsPerPage = 15;
            if (form["hdnstartPage"] != null)
            {
                if (!String.IsNullOrEmpty(form["hdnstartPage"]))
                {
                    viewmodel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
                }
                else
                {
                    viewmodel.Pager.FirstPageNo = 1;
                }
            }
            else
            {
                viewmodel.Pager.FirstPageNo = 1;
            }

            if ((form["hdnFlagClaim"] == "0" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"]) || (form["hdnlastOrderByValue"] != form["hdnOrderByValue"]) && (form["hdnOrderByValue"] != ""))
            {
                viewmodel.OrderDirection = true;
                form["hdnFlagClaim"] = "1";
            }
            else if (form["hdnFlagClaim"] == "1" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"])
            {
                viewmodel.OrderDirection = false;
                form["hdnFlagClaim"] = "0";
            }

            viewmodel.OrderByField = form["hdnOrderByValue"];
            if (!String.IsNullOrEmpty(viewmodel.AutoClaimNumber))
            {
                viewmodel.AutoPolicyNumber = null;
                viewmodel.LastName = null;
                viewmodel.City = null;
                viewmodel.Zip = null;
                viewmodel.Distance = null;
                viewmodel.State = "0";
                viewmodel.CatCode = null;
                viewmodel.LossType = "0";
                viewmodel.FileStatus = 0;
                viewmodel.DateFrom = null;
                viewmodel.DateTo = null;
                viewmodel.InsuranceCompany = 0;
                viewmodel.CSSPOCUserId = 0;
                viewmodel.CSSQAAgentUserId = 0;
                viewmodel.DateReceivedFrom = null;
                viewmodel.DateReceivedTo = null;
                viewmodel.IsTriageAvailable = false;
                viewmodel.SPName = null;
            }
            viewmodel.SearchOrExport = 0;
            if (!String.IsNullOrEmpty(hdnSearchcriteria))
            {
                if (hdnSearchcriteria == "QAAgent_Review")
                {
                    viewmodel.SearchMode = BLL.ViewModels.XactAutoAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE;
                    bool hasright = loggedInUser.AssignedRights.Where(x => x == 1).ToList().Count == 1 ? true : false;
                    viewmodel.FileStatus = 6;
                    //if ((loggedInUser.UserTypeId != 2 && loggedInUser.UserTypeId != 8) && hasright == false)
                    //{
                    //    return View("AuthorizationError");
                    //}
                }
                else
                {
                    if (viewmodel.SearchMode == XactAutoAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE)
                    {
                        //Reset the file status to select incase the user is coming back to claim search from QA Agent review queue
                        viewmodel.FileStatus = 0;
                    }
                    viewmodel.SearchMode = BLL.ViewModels.XactAutoAssignmentSearchViewModel.ViewMode.GENERAL;
                }
            }

            BLL.User user = css.Users.Find(loggedInUser.UserId);

            if (user.UserTypeId == 3) // 3 is Claim Representative
            {
                viewmodel.InsuranceCompany = Convert.ToInt32(user.HeadCompanyId);
            }

            if (String.IsNullOrEmpty(SubmitButton))
            {
                viewmodel.SearchOrExport = 0;
                viewmodel.AutoClaimsInfo = GetAutoAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewmodel).ToList();

            }
            else
            {
                viewmodel.SearchOrExport = 1;
                viewmodel.AutoClaimsInfo = GetAutoAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewmodel).ToList();


                GridView gv = new GridView();

                var data = from p in viewmodel.AutoClaimsInfo.ToList()
                           select new
                           {
                               ClaimNumber = p.ClaimNumber,
                               PolicyNumber = p.PolicyNumber,
                               ClaimName = p.InsuredName,
                               ClaimCity = p.ClaimCity,
                               ClaimState = p.ClaimState,
                               Company_Name = p.Company,
                               Client_Contact = p.Client_Contact,
                               Service_Type = p.ServiceDescription,
                               Service_Provider = p.Service_Provider,
                               Received_Date = p.Received_Date == null ? "" : p.Received_Date.Value.ToShortDateString(),
                               Closed_Date = p.Closed_Date == null ? "" : p.Closed_Date.Value.ToShortDateString(),
                               Duration_Days = p.OlDDays,
                               Estimate = p.RCV == null ? p.RCV : Math.Round(p.RCV.Value, 2),
                               Status = p.FileStatus,
                               InvoiceAmount = Math.Round(p.GrandTotal, 2)

                           };

                Utility.defineGridViewColumns(gv);

                gv.DataSource = data.ToList();
                gv.AllowPaging = false;
                gv.AutoGenerateColumns = false;
                gv.DataBind();
                gv.HeaderStyle.BackColor = Color.LightGray;
                if (SubmitButton == "Excel")
                {
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment; filename=ClaimList.xls");
                    Response.ContentType = "application/ms-excel";
                    //Response.Charset = "";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    gv.RenderControl(htw);
                    Response.Write(sw.ToString());
                    //Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }
                else if (SubmitButton == "CSV")
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition",
                     "attachment;filename=ClaimList.csv");
                    Response.Charset = "";
                    Response.ContentType = "application/text";


                    StringBuilder sb = new StringBuilder();
                    for (int k = 0; k < gv.Columns.Count; k++)
                    {
                        //add separator
                        sb.Append(gv.Columns[k].HeaderText + ',');
                    }
                    //append new line
                    sb.Append("\r\n");
                    for (int i = 0; i < gv.Rows.Count; i++)
                    {
                        for (int k = 0; k < gv.Columns.Count; k++)
                        {
                            //add separator
                            sb.Append(Regex.Replace(gv.Rows[i].Cells[k].Text, @"<[^>]+>|&nbsp;", "").Trim() + ',');
                        }
                        //append new line
                        sb.Append("\r\n");
                    }
                    Response.Output.Write(sb.ToString());
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    //using (StringWriter sw = new StringWriter())
                    //{
                    //    using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                    //    {
                    //        //To Export all pages
                    //        gv.AllowPaging = false;


                    //        gv.RenderControl(hw);
                    //        StringReader sr = new StringReader(sw.ToString());
                    //        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A2, 10f, 10f, 10f, 0f);

                    //        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                    //        PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                    //        pdfDoc.Open();
                    //        htmlparser.Parse(sr);
                    //        pdfDoc.Close();

                    //        Response.ContentType = "application/pdf";
                    //        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.pdf");
                    //        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    //        Response.Write(pdfDoc);
                    //        Response.End();
                    //    }
                    //}
                    //Create a table
                    using (StringWriter sw = new StringWriter())
                    {
                        using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                        {
                            iTextSharp.text.pdf.PdfPTable table = new iTextSharp.text.pdf.PdfPTable(gv.Columns.Count);
                            //table.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                            BaseFont bf = BaseFont.CreateFont("c:\\\\windows\\\\fonts\\\\tahoma.ttf", BaseFont.IDENTITY_H, true);
                            iTextSharp.text.Font f2 = new iTextSharp.text.Font(bf, 8f, iTextSharp.text.Font.NORMAL);
                            table.TotalWidth = 1150f;
                            //fix the absolute width of the table
                            table.LockedWidth = true;

                            //relative col widths in proportions - 1/3 and 2/3
                            //float[] widths = new float[] { 1f, 2f };
                            //table.SetWidths(widths);
                            table.HorizontalAlignment = 0;
                            //leave a gap before and after the table
                            //table.SpacingBefore = 20f;
                            //table.SpacingAfter = 30f;
                            //Transfer column from GridView to table
                            for (int j = 0; j <= gv.Columns.Count - 1; j++)
                            {
                                string cellText = gv.Columns[j].HeaderText;
                                //  iTextSharp.text.pdf.PdfPCell cell = new iTextSharp.text.pdf.PdfPCell(new Phrase(cellText));
                                PdfPCell cell = new PdfPCell(new Phrase(cellText));
                                cell.BackgroundColor = new iTextSharp.text.BaseColor(192, 192, 192);
                                table.AddCell(cell);
                            }
                            //Transfer rows from GridView to table
                            for (int i = 0; i <= gv.Rows.Count - 1; i++)
                            {
                                if (gv.Rows[i].RowType == DataControlRowType.DataRow)
                                {
                                    for (int j = 0; j <= gv.Columns.Count - 1; j++)
                                    {
                                        string cellText = gv.Rows[i].Cells[j].Text;
                                        // iTextSharp.text.pdf.PdfPCell cell = new iTextSharp.text.pdf.PdfPCell(new Phrase(100, cellText));

                                        table.AddCell(Regex.Replace(cellText, @"<[^>]+>|&nbsp;", "").Trim());
                                    }
                                }
                            }

                            //Create the PDF Document
                            //Create the PDF Document
                            //iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A2, 10f, 10f, 10f, 0f);
                            iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A2, 10f, 10f, 10f, 0f);
                            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                            pdfDoc.Open();

                            //Chunk c = new Chunk("Export GridView to PDF \n", FontFactory.GetFont("Verdana", 15));
                            //Paragraph p = new Paragraph();
                            //p.Alignment = Element.ALIGN_CENTER;
                            //p.Add(c);
                            //pdfDoc.Add(p); // add title            
                            pdfDoc.Add(table); // add the table
                            pdfDoc.Close();

                            Response.ContentType = "application/pdf";
                            Response.AddHeader("content-disposition", "attachment;" +
                                                           "filename=ClaimList.pdf");
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDoc);
                            Response.End();
                        }
                    }


                }

            }



            if (viewmodel.AutoClaimsInfo.Count != 0)
            {
                viewmodel.Pager.TotalCount = viewmodel.AutoClaimsInfo[0].TotalRecords.ToString(); // getAssignmentSearchResult(loggedInUser, viewModel).Count().ToString();
            }
            else
            {
                viewmodel.Pager.TotalCount = "0";
            }

            if (Convert.ToInt32(viewmodel.Pager.TotalCount) % Convert.ToInt32(viewmodel.Pager.RecsPerPage) != 0)
            {
                viewmodel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewmodel.Pager.TotalCount) / Convert.ToInt32(viewmodel.Pager.RecsPerPage)))).ToString();
            }
            else
            {
                viewmodel.Pager.NoOfPages = ((Convert.ToInt32(viewmodel.Pager.TotalCount) / Convert.ToInt32(viewmodel.Pager.RecsPerPage))).ToString();
            }



            // return View(viewmodel);
            if (Request.IsAjaxRequest())
            {
                return Json(RenderPartialViewToString("_AutoClaimSearchList", viewmodel), JsonRequestBehavior.AllowGet);
            }
            viewmodel.autoClaimSearchResult = @"" + RenderPartialViewToString("_AutoClaimSearchList", viewmodel);
            return View(viewmodel);

        }

        [HttpPost]
        public ActionResult PropertySearch(XactAutoAssignmentSearchViewModel viewmodel, FormCollection form, string SubmitButton = null)
        {
            DateTime From = Convert.ToDateTime(viewmodel.DateFrom);
            DateTime To = Convert.ToDateTime(viewmodel.DateTo);
            viewmodel.ServiceId = 9;
            string hdnSearchcriteria = "";
            if ((form["hdnSearchcriteria"]) != "" && (form["hdnSearchcriteria"]) != null)
            {

                hdnSearchcriteria = form["hdnSearchcriteria"].ToString();

            }

            //if (From > To)
            //{
            //    return new JavascriptResult() { Script = "alert('Successfully registered');" };
            //}

            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

            viewmodel.Pager = new BLL.Models.Pager();
            viewmodel.Pager.IsAjax = false;
            viewmodel.Pager.FormName = "formPropertySearch";

            if ((form["hdnCurrentPage"]) != "" && (form["hdnCurrentPage"]) != null)
            {

                viewmodel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);

            }
            else
            {

                viewmodel.Pager.Page = 1;
            }
            viewmodel.Pager.RecsPerPage = 15;
            if (form["hdnstartPage"] != null)
            {
                if (!String.IsNullOrEmpty(form["hdnstartPage"]))
                {
                    viewmodel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
                }
                else
                {
                    viewmodel.Pager.FirstPageNo = 1;
                }
            }
            else
            {
                viewmodel.Pager.FirstPageNo = 1;
            }

            if ((form["hdnFlagClaim"] == "0" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"]) || (form["hdnlastOrderByValue"] != form["hdnOrderByValue"]) && (form["hdnOrderByValue"] != ""))
            {
                viewmodel.OrderDirection = true;
                form["hdnFlagClaim"] = "1";
            }
            else if (form["hdnFlagClaim"] == "1" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"])
            {
                viewmodel.OrderDirection = false;
                form["hdnFlagClaim"] = "0";
            }

            viewmodel.OrderByField = form["hdnOrderByValue"];
            if (!String.IsNullOrEmpty(viewmodel.AutoClaimNumber))
            {
                viewmodel.AutoPolicyNumber = null;
                viewmodel.LastName = null;
                viewmodel.City = null;
                viewmodel.Zip = null;
                viewmodel.Distance = null;
                viewmodel.State = "0";
                viewmodel.CatCode = null;
                viewmodel.LossType = "0";
                viewmodel.FileStatus = 0;
                viewmodel.DateFrom = null;
                viewmodel.DateTo = null;
                viewmodel.InsuranceCompany = 0;
                viewmodel.CSSPOCUserId = 0;
                viewmodel.CSSQAAgentUserId = 0;
                viewmodel.DateReceivedFrom = null;
                viewmodel.DateReceivedTo = null;
                viewmodel.IsTriageAvailable = false;
                viewmodel.SPName = null;
            }
            viewmodel.SearchOrExport = 0;
            if (!String.IsNullOrEmpty(hdnSearchcriteria))
            {
                if (hdnSearchcriteria == "QAAgent_Review")
                {
                    viewmodel.SearchMode = BLL.ViewModels.XactAutoAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE;
                    bool hasright = loggedInUser.AssignedRights.Where(x => x == 1).ToList().Count == 1 ? true : false;
                    viewmodel.FileStatus = 6;
                    //if ((loggedInUser.UserTypeId != 2 && loggedInUser.UserTypeId != 8) && hasright == false)
                    //{
                    //    return View("AuthorizationError");
                    //}
                }
                else
                {
                    if (viewmodel.SearchMode == XactAutoAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE)
                    {
                        //Reset the file status to select incase the user is coming back to claim search from QA Agent review queue
                        viewmodel.FileStatus = 0;
                    }
                    viewmodel.SearchMode = BLL.ViewModels.XactAutoAssignmentSearchViewModel.ViewMode.GENERAL;
                }
            }

            BLL.User user = css.Users.Find(loggedInUser.UserId);

            if (user.UserTypeId == 3) // 3 is Claim Representative
            {
                viewmodel.InsuranceCompany = Convert.ToInt32(user.HeadCompanyId);
            }

            if (String.IsNullOrEmpty(SubmitButton))
            {
                viewmodel.SearchOrExport = 0;
                viewmodel.AutoClaimsInfo = GetAutoAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewmodel).ToList();

            }
            else
            {
                viewmodel.SearchOrExport = 1;
                viewmodel.AutoClaimsInfo = GetAutoAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewmodel).ToList();


                GridView gv = new GridView();

                var data = from p in viewmodel.AutoClaimsInfo.ToList()
                           select new
                           {
                               ClaimNumber = p.ClaimNumber,
                               PolicyNumber = p.PolicyNumber,
                               ClaimName = p.InsuredName,
                               ClaimCity = p.ClaimCity,
                               ClaimState = p.ClaimState,
                               Company_Name = p.Company,
                               Client_Contact = p.Client_Contact,
                               Service_Type = p.ServiceDescription,
                               Service_Provider = p.Service_Provider,
                               Received_Date = p.Received_Date == null ? "" : p.Received_Date.Value.ToShortDateString(),
                               Closed_Date = p.Closed_Date == null ? "" : p.Closed_Date.Value.ToShortDateString(),
                               Duration_Days = p.OlDDays,
                               Estimate = p.RCV == null ? p.RCV : Math.Round(p.RCV.Value, 2),
                               Status = p.FileStatus,
                               InvoiceAmount = Math.Round(p.GrandTotal, 2)

                           };

                Utility.defineGridViewColumns(gv);

                gv.DataSource = data.ToList();
                gv.AllowPaging = false;
                gv.AutoGenerateColumns = false;
                gv.DataBind();
                gv.HeaderStyle.BackColor = Color.LightGray;
                if (SubmitButton == "Excel")
                {
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment; filename=ClaimList.xls");
                    Response.ContentType = "application/ms-excel";
                    //Response.Charset = "";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    gv.RenderControl(htw);
                    Response.Write(sw.ToString());
                    //Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }
                else if (SubmitButton == "CSV")
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition",
                     "attachment;filename=ClaimList.csv");
                    Response.Charset = "";
                    Response.ContentType = "application/text";


                    StringBuilder sb = new StringBuilder();
                    for (int k = 0; k < gv.Columns.Count; k++)
                    {
                        //add separator
                        sb.Append(gv.Columns[k].HeaderText + ',');
                    }
                    //append new line
                    sb.Append("\r\n");
                    for (int i = 0; i < gv.Rows.Count; i++)
                    {
                        for (int k = 0; k < gv.Columns.Count; k++)
                        {
                            //add separator
                            sb.Append(Regex.Replace(gv.Rows[i].Cells[k].Text, @"<[^>]+>|&nbsp;", "").Trim() + ',');
                        }
                        //append new line
                        sb.Append("\r\n");
                    }
                    Response.Output.Write(sb.ToString());
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    //using (StringWriter sw = new StringWriter())
                    //{
                    //    using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                    //    {
                    //        //To Export all pages
                    //        gv.AllowPaging = false;


                    //        gv.RenderControl(hw);
                    //        StringReader sr = new StringReader(sw.ToString());
                    //        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A2, 10f, 10f, 10f, 0f);

                    //        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                    //        PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                    //        pdfDoc.Open();
                    //        htmlparser.Parse(sr);
                    //        pdfDoc.Close();

                    //        Response.ContentType = "application/pdf";
                    //        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.pdf");
                    //        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    //        Response.Write(pdfDoc);
                    //        Response.End();
                    //    }
                    //}
                    //Create a table
                    using (StringWriter sw = new StringWriter())
                    {
                        using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                        {
                            iTextSharp.text.pdf.PdfPTable table = new iTextSharp.text.pdf.PdfPTable(gv.Columns.Count);
                            //table.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                            BaseFont bf = BaseFont.CreateFont("c:\\\\windows\\\\fonts\\\\tahoma.ttf", BaseFont.IDENTITY_H, true);
                            iTextSharp.text.Font f2 = new iTextSharp.text.Font(bf, 8f, iTextSharp.text.Font.NORMAL);
                            table.TotalWidth = 1150f;
                            //fix the absolute width of the table
                            table.LockedWidth = true;

                            //relative col widths in proportions - 1/3 and 2/3
                            //float[] widths = new float[] { 1f, 2f };
                            //table.SetWidths(widths);
                            table.HorizontalAlignment = 0;
                            //leave a gap before and after the table
                            //table.SpacingBefore = 20f;
                            //table.SpacingAfter = 30f;
                            //Transfer column from GridView to table
                            for (int j = 0; j <= gv.Columns.Count - 1; j++)
                            {
                                string cellText = gv.Columns[j].HeaderText;
                                //  iTextSharp.text.pdf.PdfPCell cell = new iTextSharp.text.pdf.PdfPCell(new Phrase(cellText));
                                PdfPCell cell = new PdfPCell(new Phrase(cellText));
                                cell.BackgroundColor = new iTextSharp.text.BaseColor(192, 192, 192);
                                table.AddCell(cell);
                            }
                            //Transfer rows from GridView to table
                            for (int i = 0; i <= gv.Rows.Count - 1; i++)
                            {
                                if (gv.Rows[i].RowType == DataControlRowType.DataRow)
                                {
                                    for (int j = 0; j <= gv.Columns.Count - 1; j++)
                                    {
                                        string cellText = gv.Rows[i].Cells[j].Text;
                                        // iTextSharp.text.pdf.PdfPCell cell = new iTextSharp.text.pdf.PdfPCell(new Phrase(100, cellText));

                                        table.AddCell(Regex.Replace(cellText, @"<[^>]+>|&nbsp;", "").Trim());
                                    }
                                }
                            }

                            //Create the PDF Document
                            //Create the PDF Document
                            //iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A2, 10f, 10f, 10f, 0f);
                            iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A2, 10f, 10f, 10f, 0f);
                            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                            pdfDoc.Open();

                            //Chunk c = new Chunk("Export GridView to PDF \n", FontFactory.GetFont("Verdana", 15));
                            //Paragraph p = new Paragraph();
                            //p.Alignment = Element.ALIGN_CENTER;
                            //p.Add(c);
                            //pdfDoc.Add(p); // add title            
                            pdfDoc.Add(table); // add the table
                            pdfDoc.Close();

                            Response.ContentType = "application/pdf";
                            Response.AddHeader("content-disposition", "attachment;" +
                                                           "filename=ClaimList.pdf");
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDoc);
                            Response.End();
                        }
                    }


                }

            }



            if (viewmodel.AutoClaimsInfo.Count != 0)
            {
                viewmodel.Pager.TotalCount = viewmodel.AutoClaimsInfo[0].TotalRecords.ToString(); // getAssignmentSearchResult(loggedInUser, viewModel).Count().ToString();
            }
            else
            {
                viewmodel.Pager.TotalCount = "0";
            }

            if (Convert.ToInt32(viewmodel.Pager.TotalCount) % Convert.ToInt32(viewmodel.Pager.RecsPerPage) != 0)
            {
                viewmodel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewmodel.Pager.TotalCount) / Convert.ToInt32(viewmodel.Pager.RecsPerPage)))).ToString();
            }
            else
            {
                viewmodel.Pager.NoOfPages = ((Convert.ToInt32(viewmodel.Pager.TotalCount) / Convert.ToInt32(viewmodel.Pager.RecsPerPage))).ToString();
            }



            // return View(viewmodel);
            if (Request.IsAjaxRequest())
            {
                return Json(RenderPartialViewToString("_AutoClaimSearchList", viewmodel), JsonRequestBehavior.AllowGet);
            }
            viewmodel.autoClaimSearchResult = @"" + RenderPartialViewToString("_AutoClaimSearchList", viewmodel);
            return View(viewmodel);

        }

        public ActionResult InvoiceAutoSearch(string reviewType)
        {
            NuGenAutoAssignmentSearchViewModel viewmodel = new NuGenAutoAssignmentSearchViewModel();
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

            TempData["reviewType"] = reviewType;
            if (viewmodel.Pager == null)
            {
                viewmodel.Pager = new BLL.Models.Pager();
                viewmodel.Pager.Page = 1;
                viewmodel.Pager.RecsPerPage = 15;
                viewmodel.Pager.FirstPageNo = 1;
                viewmodel.Pager.IsAjax = false;
                viewmodel.Pager.FormName = "formNugenInvoicesSearch";
            }

            BLL.User user = css.Users.Find(loggedInUser.UserId);


            //This need to be calculated later

            
            viewmodel.NuGenAutoAssignmentSearch = css.NuGenClaimsGetList(
                                                                            viewmodel.Pager.Page - 1, 
                                                                            viewmodel.Pager.RecsPerPage,
                                                                            null,
                                                                            true,
                                                                            null,
                                                                            null,
                                                                            null,
                                                                            null,
                                                                            null,
                                                                            null,
                                                                            null,
                                                                            null,
                                                                            null,
                                                                            null,
                                                                            null,
                                                                            null,
                                                                            loggedInUser.UserId
                                                                        ).ToList();

            if (viewmodel.NuGenAutoAssignmentSearch.Count != 0)
            {
                viewmodel.Pager.TotalCount = viewmodel.NuGenAutoAssignmentSearch[0].TotalCount.ToString();
            }
            else
            {
                viewmodel.Pager.TotalCount = "0";
            }

            viewmodel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewmodel.Pager.TotalCount) / Convert.ToInt32(viewmodel.Pager.RecsPerPage)))).ToString();

            viewmodel.strNuGenAutoAssignmentResult = RenderPartialViewToString("_NuGenInvoicesSearchList", viewmodel);
            ViewData["Visible"] = "false";

            return View();
        }

        [HttpPost]
        public ActionResult InvoiceAutoSearch(NuGenAutoAssignmentSearchViewModel viewmodel, FormCollection form, string SubmitButton = null)
        {
            ModelState.Clear();
            string hdnSearchcriteria = "";
            if ((form["hdnSearchcriteria"]) != "" && (form["hdnSearchcriteria"]) != null)
            {

                hdnSearchcriteria = form["hdnSearchcriteria"].ToString();

            }

            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

            viewmodel.Pager = new BLL.Models.Pager();
            viewmodel.Pager.IsAjax = false;
            viewmodel.Pager.FormName = "formNugenInvoicesSearch";

            if ((form["hdnCurrentPage"]) != "" && (form["hdnCurrentPage"]) != null)
            {

                viewmodel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);

            }
            else
            {

                viewmodel.Pager.Page = 1;
            }
            viewmodel.Pager.RecsPerPage = 15;
            if (form["hdnstartPage"] != null)
            {
                if (!String.IsNullOrEmpty(form["hdnstartPage"]))
                {
                    viewmodel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
                }
                else
                {
                    viewmodel.Pager.FirstPageNo = 1;
                }
            }
            else
            {
                viewmodel.Pager.FirstPageNo = 1;
            }

            //********************************************** SEARCH HERE **************************************************
            if (string.IsNullOrEmpty(SubmitButton))
            {
                viewmodel.NuGenAutoAssignmentSearch = css.NuGenClaimsGetList(
                                                                                viewmodel.Pager.Page - 1,        //PageNo            
                                                                                viewmodel.Pager.RecsPerPage,            //RecsPerPage
                                                                                null, //IsExport
                                                                                true, //IsSearch ///////////
                                                                                viewmodel.objXactAutoAssignmentSearchViewModel.InsureName,        //ClaimName
                                                                                viewmodel.objXactAutoAssignmentSearchViewModel.AutoClaimNumber,   //ClaimNumber
                                                                                viewmodel.objXactAutoAssignmentSearchViewModel.SPName,            //ServiceProviderName
                                                                                viewmodel.objXactAutoAssignmentSearchViewModel.Zip,               //ZIP Code
                                                                                viewmodel.objXactAutoAssignmentSearchViewModel.City,              //City
                                                                                Convert.ToInt32(viewmodel.objXactAutoAssignmentSearchViewModel.State),             //State
                                                                                viewmodel.objXactAutoAssignmentSearchViewModel.DateReceivedFrom,  //DateReceivedFrom
                                                                                viewmodel.objXactAutoAssignmentSearchViewModel.DateReceivedTo,    //DateReceivedTo
                                                                                viewmodel.objXactAutoAssignmentSearchViewModel.AutoPolicyNumber,  //PolicyNumber
                                                                                Convert.ToInt32(viewmodel.objXactAutoAssignmentSearchViewModel.InsuranceCompany),  //InsuranceComapny
                                                                                viewmodel.objXactAutoAssignmentSearchViewModel.ClaimType,         //ClaimType
                                                                                viewmodel.objXactAutoAssignmentSearchViewModel.FileStatus,        //FileStatus
                                                                                 loggedInUser.UserId //LoggedInUserId
                                                                            ).ToList();
            }
            else
            {
                viewmodel.NuGenAutoAssignmentSearch = css.NuGenClaimsGetList(
                                                                                viewmodel.Pager.FirstPageNo - 1,        //PageNo            
                                                                                viewmodel.Pager.RecsPerPage,            //RecsPerPage
                                                                                true, //IsExport
                                                                                null, //IsSearch
                                                                                viewmodel.objXactAutoAssignmentSearchViewModel.InsureName,        //ClaimName
                                                                                viewmodel.objXactAutoAssignmentSearchViewModel.AutoClaimNumber,   //ClaimNumber
                                                                                viewmodel.objXactAutoAssignmentSearchViewModel.SPName,            //ServiceProviderName
                                                                                viewmodel.objXactAutoAssignmentSearchViewModel.Zip,               //ZIP Code
                                                                                viewmodel.objXactAutoAssignmentSearchViewModel.City,              //City
                                                                                Convert.ToInt32(viewmodel.objXactAutoAssignmentSearchViewModel.State),             //State
                                                                                viewmodel.objXactAutoAssignmentSearchViewModel.DateReceivedFrom,  //DateReceivedFrom
                                                                                viewmodel.objXactAutoAssignmentSearchViewModel.DateReceivedTo,    //DateReceivedTo
                                                                                viewmodel.objXactAutoAssignmentSearchViewModel.AutoPolicyNumber,  //PolicyNumber
                                                                                Convert.ToInt32(viewmodel.objXactAutoAssignmentSearchViewModel.InsuranceCompany),  //InsuranceComapny
                                                                                viewmodel.objXactAutoAssignmentSearchViewModel.ClaimType,         //ClaimType
                                                                                viewmodel.objXactAutoAssignmentSearchViewModel.FileStatus,        //FileStatus
                                                                                 loggedInUser.UserId//LoggedInUserId
                                                                            ).ToList();

                GridView gv = new GridView();

                //var data = from p in viewmodel.NuGenAutoAssignmentSearch.ToList()
                //           select new
                //           {
                //               ClaimNumber = p.ClaimNumber,
                //               PolicyNumber = p.PolicyNumber,
                //               ClaimName = p.ReferrerName,
                //               ClaimCity = p.ClaimCity,
                //               ClaimState = p.ClaimState,
                //               Company_Name = p.CompanyName,
                //               Client_Contact = p.Client_Contact,
                //               Service_Type = p.ServiceDescription,
                //               Service_Provider = p.SPCompanyName,
                //               Received_Date = p.Received_Date == null ? "" : p.Received_Date.ToShortDateString(),
                //               Closed_Date = p.Closed_Date == null ? "" : p.Closed_Date.ToShortDateString(),
                //               Duration_Days = p.OlDDays,
                //               Estimate = p.RCV == null ? p.RCV : Math.Round(p.RCV.Value, 2),
                //               Status = p.FileStatus,
                //               InvoiceAmount = Math.Round(p.GrandTotal.Value, 2)
                //           };

                Utility.NugenGridViewColumns(gv);

                gv.DataSource = viewmodel.NuGenAutoAssignmentSearch;
                gv.AllowPaging = false;
                gv.AutoGenerateColumns = false;
                gv.DataBind();
                gv.HeaderStyle.BackColor = Color.LightGray;
                if (SubmitButton == "Excel")
                {
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment; filename=ClaimList.xls");
                    Response.ContentType = "application/ms-excel";
                    //Response.Charset = "";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    gv.RenderControl(htw);
                    Response.Write(sw.ToString());
                    //Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }
                else if (SubmitButton == "CSV")
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition",
                     "attachment;filename=ClaimList.csv");
                    Response.Charset = "";
                    Response.ContentType = "application/text";


                    StringBuilder sb = new StringBuilder();
                    for (int k = 0; k < gv.Columns.Count; k++)
                    {
                        //add separator
                        sb.Append(gv.Columns[k].HeaderText + ',');
                    }
                    //append new line
                    sb.Append("\r\n");
                    for (int i = 0; i < gv.Rows.Count; i++)
                    {
                        for (int k = 0; k < gv.Columns.Count; k++)
                        {
                            //add separator
                            sb.Append(Regex.Replace(gv.Rows[i].Cells[k].Text, @"<[^>]+>|&nbsp;", "").Trim() + ',');
                        }
                        //append new line
                        sb.Append("\r\n");
                    }
                    Response.Output.Write(sb.ToString());
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    using (StringWriter sw = new StringWriter())
                    {
                        using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                        {
                            iTextSharp.text.pdf.PdfPTable table = new iTextSharp.text.pdf.PdfPTable(gv.Columns.Count);
                            //table.RunDirection = PdfWriter.RUN_DIRECTION_LTR;
                            BaseFont bf = BaseFont.CreateFont("c:\\\\windows\\\\fonts\\\\tahoma.ttf", BaseFont.IDENTITY_H, true);
                            iTextSharp.text.Font f2 = new iTextSharp.text.Font(bf, 8f, iTextSharp.text.Font.NORMAL);
                            table.TotalWidth = 1150f;
                            //fix the absolute width of the table
                            table.LockedWidth = true;

                            //relative col widths in proportions - 1/3 and 2/3
                            //float[] widths = new float[] { 1f, 2f };
                            //table.SetWidths(widths);
                            table.HorizontalAlignment = 0;
                            //leave a gap before and after the table
                            //table.SpacingBefore = 20f;
                            //table.SpacingAfter = 30f;
                            //Transfer column from GridView to table
                            for (int j = 0; j <= gv.Columns.Count - 1; j++)
                            {
                                string cellText = gv.Columns[j].HeaderText;
                                //  iTextSharp.text.pdf.PdfPCell cell = new iTextSharp.text.pdf.PdfPCell(new Phrase(cellText));
                                PdfPCell cell = new PdfPCell(new Phrase(cellText));
                                cell.BackgroundColor = new iTextSharp.text.BaseColor(192, 192, 192);
                                table.AddCell(cell);
                            }
                            //Transfer rows from GridView to table
                            for (int i = 0; i <= gv.Rows.Count - 1; i++)
                            {
                                if (gv.Rows[i].RowType == DataControlRowType.DataRow)
                                {
                                    for (int j = 0; j <= gv.Columns.Count - 1; j++)
                                    {
                                        string cellText = gv.Rows[i].Cells[j].Text;
                                        // iTextSharp.text.pdf.PdfPCell cell = new iTextSharp.text.pdf.PdfPCell(new Phrase(100, cellText));

                                        table.AddCell(Regex.Replace(cellText, @"<[^>]+>|&nbsp;", "").Trim());
                                    }
                                }
                            }

                            //Create the PDF Document
                            //Create the PDF Document
                            //iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A2, 10f, 10f, 10f, 0f);
                            iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A2, 10f, 10f, 10f, 0f);
                            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                            pdfDoc.Open();

                            //Chunk c = new Chunk("Export GridView to PDF \n", FontFactory.GetFont("Verdana", 15));
                            //Paragraph p = new Paragraph();
                            //p.Alignment = Element.ALIGN_CENTER;
                            //p.Add(c);
                            //pdfDoc.Add(p); // add title            
                            pdfDoc.Add(table); // add the table
                            pdfDoc.Close();

                            Response.ContentType = "application/pdf";
                            Response.AddHeader("content-disposition", "attachment;" +
                                                           "filename=ClaimList.pdf");
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDoc);
                            Response.End();
                        }
                    }
                }
            }


            if (viewmodel.NuGenAutoAssignmentSearch.Count != 0)
            {            
                viewmodel.Pager.TotalCount = viewmodel.NuGenAutoAssignmentSearch[0].TotalCount.ToString();            
            }
            else
            {
                viewmodel.Pager.TotalCount = "0";
            }

            if (Convert.ToInt32(viewmodel.Pager.TotalCount) % Convert.ToInt32(viewmodel.Pager.RecsPerPage) != 0)
            {
                viewmodel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewmodel.Pager.TotalCount) / Convert.ToInt32(viewmodel.Pager.RecsPerPage)))).ToString();
            }
            else
            {
                viewmodel.Pager.NoOfPages = ((Convert.ToInt32(viewmodel.Pager.TotalCount) / Convert.ToInt32(viewmodel.Pager.RecsPerPage))).ToString();
            }

            // return View(viewmodel);
            if (Request.IsAjaxRequest())
            {
                return Json(RenderPartialViewToString("_NuGenInvoicesSearchList", viewmodel), JsonRequestBehavior.AllowGet);
            }
            viewmodel.strNuGenAutoAssignmentResult = @"" + RenderPartialViewToString("_NuGenInvoicesSearchList", viewmodel);
            return View(viewmodel);
        }

        public ActionResult BulkInvoiceCreationList(string reviewType)
        {
            NuGenAutoAssignmentSearchViewModel viewmodel = new NuGenAutoAssignmentSearchViewModel();
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

            TempData["reviewType"] = reviewType;
            if (viewmodel.Pager == null)
            {
                viewmodel.Pager = new BLL.Models.Pager();
                viewmodel.Pager.Page = 1;
                viewmodel.Pager.RecsPerPage = 200;
                viewmodel.Pager.FirstPageNo = 1;
                viewmodel.Pager.IsAjax = false;
                viewmodel.Pager.FormName = "formNugenInvoicesSearch";
            }

            BLL.User user = css.Users.Find(loggedInUser.UserId);


            //This need to be calculated later

            viewmodel.Pager.TotalCount = (viewmodel.NuGenAutoAssignmentSearch != null ? viewmodel.NuGenAutoAssignmentSearch.Count : 10).ToString();
            viewmodel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewmodel.Pager.TotalCount) / Convert.ToInt32(viewmodel.Pager.RecsPerPage)))).ToString();
            viewmodel.NuGenAutoAssignmentSearch = css.NuGenClaimsGetList(
                                                                            viewmodel.Pager.Page - 1,
                                                                            viewmodel.Pager.RecsPerPage,
                                                                            null, //IsExport
                                                                            true, //IsSearch
                                                                            null, //ClaimName
                                                                            null, //ClaimNumber
                                                                            null, //ServiceProviderName
                                                                            null, //ZIP Code
                                                                            null, //City
                                                                            null, //State
                                                                            null, //DateReceivedFrom
                                                                            null, //DateReceivedTo
                                                                            null, //PolicyNumber
                                                                            null, //InsuranceComapny
                                                                            null, //ClaimType
                                                                            18, //FileStatus
                                                                            null //LoggedInUserId
                                                                        ).ToList(); //.Where(x => x.FileStatus == 18)
            viewmodel.strNuGenAutoAssignmentResult = RenderPartialViewToString("_NuGenInvoicesSearchList", viewmodel);
            ViewData["Visible"] = "true";

            return View("InvoiceAutoSearch");
        }


        public ActionResult FinalizeBulkInvoice(string AssignmentIds)
        {
            try
			              
						{CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            ObjectResult<DateTime?> cstLocalTime = css.usp_GetLocalDateTime();
            DateTime bulkInvoiceDateTime = cstLocalTime.First().Value;
            bulkInvoiceDateTime = new DateTime(bulkInvoiceDateTime.Year, bulkInvoiceDateTime.Month, bulkInvoiceDateTime.Day, bulkInvoiceDateTime.Hour, bulkInvoiceDateTime.Minute, 0);//retain time info till minute

            css.FinalizeBulkInvoiceList(AssignmentIds, bulkInvoiceDateTime);

            if (!String.IsNullOrEmpty(AssignmentIds))
                {
                    String[] assignmentList = AssignmentIds.Split(',');
                    foreach (string assignmentID in assignmentList)
                    {
                        GenInvoicePDF(Convert.ToInt64(assignmentID), loggedInUser);
                    }
                }

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex) { return Json(false, JsonRequestBehavior.AllowGet); }
        }

        public ActionResult BulkInvoicePdf(DateTime SubmittedDate)
        {
            string Fileurl = "",filename="";
            string str =SubmittedDate.ToString("yyyy-MM-dd HH:mm:ss.fff");
            DateTime date = Convert.ToDateTime(str);
            //SubmittedDate.ToString("yyyy-mm-ddhh:mm:ss");
            try
            {
                BulkInvoiceSummary BulkInvoiceSummary = css.BulkInvoiceSummaries.Where(s => s.SubmittedDate == date).FirstOrDefault();
               
                string baseFolder = System.Configuration.ConfigurationManager.AppSettings["AutoAssignmentDocsPath"].ToString() + "/BulkInvoiceSummary";
                //string assignmentId = document.AutoAssignmentId + "";
                //string documentTypeId = document.DocumentTypeId + "";
                ////check whether the folder with user's userid exists
                //string filename = document.AutoDocumentPath;
                //check whether the destination folder depending on the type of document being uploaded exists
                if (BulkInvoiceSummary!=null)
                {
                    filename = BulkInvoiceSummary.PdfFileName;
                    if (MvcApplication.UseAzureCloudStorage == "1")
                    {
                        Fileurl = CloudStorageUtility.GetAbsoluteFileURL(MvcApplication.BulkInvoiceSummaryPDF, filename);
                    }
                    else
                    {
                        if (Directory.Exists(Server.MapPath(baseFolder)))
                        {
                            Fileurl = Path.Combine(Server.MapPath(baseFolder),filename);
                        }
                        else
                        {
                            Fileurl = Path.Combine(Server.MapPath(baseFolder), "NotFound.txt");
                            filename = "NotFound.txt";
                        }
                    }
                }
                else
                {
                    Fileurl = Path.Combine(Server.MapPath(baseFolder), "NotFound.txt");
                    filename = "NotFound.txt";
                }
                // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                
                string contentType = string.Empty;

                if (filename.Contains(".pdf"))
                {
                    contentType = "application/pdf";
                }

                else if (filename.Contains(".txt"))
                {
                    contentType = "text/plain";
                }
                else if (filename.Contains(".docx"))
                {
                    contentType = "application/docx";
                }
                else if (filename.Contains(".png"))
                {
                    contentType = "application/png";
                }
                else if (filename.Contains(".jpeg"))
                {
                    contentType = "application/jpeg";
                }
                else if (filename.Contains(".jpg"))
                {
                    contentType = "application/jpg";
                }
                else if (filename.Contains(".xlsx"))
                {
                    contentType = "application/xlsx";
                }

                return new ReportResult(Fileurl, filename, contentType);
                //  return FilePath(Fileurl, contentType, filename);  

            }
            catch (Exception ex)
            {

            }
            return File("", "application/pdf");
        }
        public ActionResult NugenInvoiceDetails(string claimid, bool IsQBInvoice = true)
        {            
            NuGenPricingScheduleViewModel viewmodel = new NuGenPricingScheduleViewModel(Convert.ToInt32(Cypher.DecryptString(Cypher.ReplaceCharcters(claimid))), false);
            ViewData["ClaimId"] = Cypher.DecryptString(Cypher.ReplaceCharcters(claimid));
            return View(viewmodel);
        }

        [HttpPost]
        public ActionResult NugenInvoiceDetails(NuGenPricingScheduleViewModel viewmodel, FormCollection form, string SubmitButton = null)
        {
            String ChkSubmittbutton = form["SubmitStatus"];
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            if (SubmitButton == "BACK")
            {
                return RedirectToAction("InvoiceAutoSearch");
            }
                
            else if (SubmitButton == "Submit" || ChkSubmittbutton == "Submit")
            {
                Int64 ClaimId = Convert.ToInt64(viewmodel.pricingClaimId);

                //INPSW -40 if spinvoice number is blank then set claimnumber
                string ClaimNumber = (from c in css.Claims                                         
                                          where c.ClaimId == ClaimId
                                          select c.ClaimNumber).FirstOrDefault();
                string SpInvoiceNumber = viewmodel.SpInvoiceNumber;

                if (viewmodel.SpInvoiceNumber == null || viewmodel.SpInvoiceNumber == "")
                {
                    SpInvoiceNumber = ClaimNumber;
                }
              
                //Int64 AutoAssignmentId = (from c in css.AutoAssignments
                //                          where c.ClaimId == ClaimId
                //                          select c.AutoAssignmentId).FirstOrDefault();
                Int64 AutoAssignmentId = (from c in css.AutoAssignments orderby c.AutoAssignmentId descending
                                          where c.ClaimId == ClaimId
                                          select c.AutoAssignmentId).FirstOrDefault();
                Int64 AutoInvoiceId = -1;
                ServiceLOBPricing obj = new ServiceLOBPricing();
                Int32 pricingServiceTypeId = Convert.ToInt32(viewmodel.pricingServiceTypeId);
                Int32 pricingLineofBusinessId= Convert.ToInt32(viewmodel.pricingLineofBusinessId);
                Double? GradTotal= Convert.ToDouble(viewmodel.AutoInvoiceDetails.GrandTotal);

                obj = (from c in css.ServiceLOBPricings
                       where c.ServiceTypeId == pricingServiceTypeId && c.LOBId == pricingLineofBusinessId
                       && (GradTotal >= c.StartRange && GradTotal <= c.EndRange)
                       select c).FirstOrDefault();

                if (obj != null)
                {
                    double totalSPPay = 0;
                    double iadaServiceFee = 0;
                    if (obj.SPServiceFee.HasValue)
                    {
                        iadaServiceFee = obj.SPServiceFee.Value;
                        if(viewmodel.AutoInvoiceDetails.GrandTotal == 0)
                        {
                            totalSPPay = 0;
                        }
                        else
                        {
                            totalSPPay = Convert.ToDouble(viewmodel.AutoInvoiceDetails.GrandTotal) - obj.SPServiceFee.Value;
                        }                        
                    }
                    else if (obj.SPServicePercent.HasValue)
                    {
                        iadaServiceFee = (Convert.ToDouble(viewmodel.AutoInvoiceDetails.GrandTotal) * (obj.SPServicePercent.Value / 100));
                        totalSPPay = Convert.ToDouble(viewmodel.AutoInvoiceDetails.GrandTotal) - iadaServiceFee;
                    }
                    else 
					{
                        totalSPPay = Convert.ToDouble(viewmodel.AutoInvoiceDetails.GrandTotal);
                    }
                    viewmodel.AutoInvoiceDetails.SPServiceFee =Convert.ToDecimal(iadaServiceFee);
                    viewmodel.AutoInvoiceDetails.SPServiceFeePercent =Convert.ToByte(obj.SPServicePercent);
                    viewmodel.AutoInvoiceDetails.TotalSPPay =Convert.ToDecimal(totalSPPay);
                }
                if (css.AutoInvoices.Where(a => a.AutoAssignmentId == AutoAssignmentId).Any())
                {
                    //This is for Edit
                    Int64 intAutoInvoiceId = (from c in css.AutoInvoices
                                              where c.AutoAssignmentId == AutoAssignmentId
                                              select c.AutoInvoiceId).FirstOrDefault();
                    AutoInvoiceId = intAutoInvoiceId;
                    css.NuGenInvoiceUpdate(intAutoInvoiceId,
                        Convert.ToInt64(viewmodel.pricingClaimId),
                        viewmodel.pricingSPReferenceInvoiceNo,
                        AutoAssignmentId,
                        DateTime.Now,
                        null,                   //AutoInvoiceStatus
                        null,                   //OriginalInvoiceDate
                        null,
                        Convert.ToInt32(viewmodel.pricingServiceTypeId),
                        Convert.ToInt32(viewmodel.pricingLineofBusinessId),
                        Convert.ToDecimal(viewmodel.AutoInvoiceDetails.RCV),
                        Convert.ToDecimal(viewmodel.AutoInvoiceDetails.ServicesCharges),
                        Convert.ToDouble(viewmodel.AutoInvoiceDetails.TE1NoOfHours),
                        Convert.ToDecimal(viewmodel.AutoInvoiceDetails.TE1SPHourlyRate),
                        Convert.ToDecimal(viewmodel.AutoInvoiceDetails.TotalHourCalculated),
                        viewmodel.AutoInvoiceDetails.ActualMiles,
                        viewmodel.AutoInvoiceDetails.IncludedMiles,
                        Convert.ToDecimal(viewmodel.AutoInvoiceDetails.MileageCharges),
                        viewmodel.AutoInvoiceDetails.TotalMileage,
                        viewmodel.pricingIsTotalLoss == "1" ? true : false,
                        Convert.ToDecimal(viewmodel.AutoInvoiceDetails.TotalLossFee),
                        Convert.ToDecimal(viewmodel.AutoInvoiceDetails.Misc),
                        viewmodel.AutoInvoiceDetails.MiscComment,
                        Convert.ToDouble(viewmodel.AutoInvoiceDetails.SalesCharges),
                        Convert.ToDecimal(viewmodel.AutoInvoiceDetails.Tax),
                        Convert.ToDecimal(viewmodel.AutoInvoiceDetails.GrandTotal),
                        viewmodel.AutoInvoiceDetails.SPServiceFee,
                        viewmodel.AutoInvoiceDetails.SPServiceFeePercent,
                        viewmodel.AutoInvoiceDetails.TotalSPPay,
                        SpInvoiceNumber //INPSW -40
                        );


                    viewmodel.AutoInvoiceDetails = css.usp_AutoInvoiceGetDetails(AutoAssignmentId, intAutoInvoiceId).FirstOrDefault();
                    ViewBag.Message = "Invoice changes has been updated successfully.View Invoice on Claim Details Documents Screen.";
                    // if total invoice ammount is 0 than invoice not bridge to QB
                    if(viewmodel.AutoInvoiceDetails.GrandTotal > 0)
                    {
                        Utility.QBUpdateInvoice(AutoInvoiceId);//INPSW -29
                    }
                    
                  
                }
                else
                {
                    //this is for Insert


                    ObjectParameter outAutoInvoiceId = new ObjectParameter("AutoInvoiceId", DbType.Int64);

                    css.NuGenInvoiceInsert(outAutoInvoiceId,
                        Convert.ToInt64(viewmodel.pricingClaimId),
                        viewmodel.pricingSPReferenceInvoiceNo,
                        AutoAssignmentId,
                        DateTime.Now,
                        null,                   //AutoInvoiceStatus
                        null,                   //OriginalInvoiceDate
                        null,
                        Convert.ToInt32(viewmodel.pricingServiceTypeId),
                        Convert.ToInt32(viewmodel.pricingLineofBusinessId),
                        Convert.ToDecimal(viewmodel.AutoInvoiceDetails.RCV),
                        Convert.ToDecimal(viewmodel.AutoInvoiceDetails.ServicesCharges),
                        Convert.ToDouble(viewmodel.AutoInvoiceDetails.TE1NoOfHours),
                        Convert.ToDecimal(viewmodel.AutoInvoiceDetails.TE1SPHourlyRate),
                        Convert.ToDecimal(viewmodel.AutoInvoiceDetails.TotalHourCalculated),
                        viewmodel.AutoInvoiceDetails.ActualMiles,
                        viewmodel.AutoInvoiceDetails.IncludedMiles,
                        Convert.ToDecimal(viewmodel.AutoInvoiceDetails.MileageCharges),
                        viewmodel.AutoInvoiceDetails.TotalMileage,
                        viewmodel.pricingIsTotalLoss == "1" ? true : false,
                        Convert.ToDecimal(viewmodel.AutoInvoiceDetails.TotalLossFee),
                        Convert.ToDecimal(viewmodel.AutoInvoiceDetails.Misc),
                        viewmodel.AutoInvoiceDetails.MiscComment,
                        Convert.ToDouble(viewmodel.AutoInvoiceDetails.SalesCharges),
                        Convert.ToDecimal(viewmodel.AutoInvoiceDetails.Tax),
                        Convert.ToDecimal(viewmodel.AutoInvoiceDetails.GrandTotal),
                        viewmodel.AutoInvoiceDetails.SPServiceFee,
                        viewmodel.AutoInvoiceDetails.SPServiceFeePercent,
                        viewmodel.AutoInvoiceDetails.TotalSPPay,
                        SpInvoiceNumber //INPSW -40
                        );
                        AutoInvoiceId = Convert.ToInt64(outAutoInvoiceId.Value);
                        //ViewBag.Message = "Invoice detail has been saved successfully...!";
                        ViewBag.Message = "Invoice Successfully Created. View Invoice on Claim Details Documents Screen.";
                        
                    // if total invoice ammount is 0 than invoice not bridge to QB
                        if (viewmodel.AutoInvoiceDetails.GrandTotal > 0)
                        {
                            Utility.QBUpdateInvoice(AutoInvoiceId);//INPSW -29
                        }
                         
                       
                }
             //   if (!viewmodel.IsQBInvoice)
             //   {
                GenInvoicePDFAutoClaims(AutoAssignmentId, AutoInvoiceId, loggedInUser, viewmodel.IsQBInvoice);
              //  }
                viewmodel.NuGenLOBPricingDetail(Convert.ToInt32(viewmodel.pricingServiceTypeId), Convert.ToInt32(ViewData["ClaimId"]));
                viewmodel.populateLists(Convert.ToInt32(viewmodel.pricingLineofBusinessId));
                viewmodel.populateLOBAndServiceTypes(!string.IsNullOrEmpty(viewmodel.pricingClaimId)? Convert.ToInt32(viewmodel.pricingClaimId) : 0);

            }
            else if (viewmodel.pricingServiceTypeId != "0" && !string.IsNullOrEmpty(viewmodel.pricingLineofBusinessId) && viewmodel.pricingLineofBusinessId != "0" && viewmodel.pricingPrevServiceTypeId == viewmodel.pricingServiceTypeId)
            {
                //Pull detailas based on LOB has selected   
                //ModelState.Clear();
                viewmodel.NuGenLOBPricingDetail(Convert.ToInt32(viewmodel.pricingServiceTypeId), Convert.ToInt32(ViewData["ClaimId"]));
                viewmodel.populateLists(Convert.ToInt32(viewmodel.pricingLineofBusinessId));
            }
            else if (viewmodel.pricingServiceTypeId != "0")
            {
                //ModelState.Clear();
                viewmodel.populateLists(Convert.ToInt32(viewmodel.pricingServiceTypeId));  
                viewmodel.getNuGenClaimDetailResult(viewmodel.pricingClaimId);
            }
            else
            {
                viewmodel.getNuGenClaimDetailResult(viewmodel.pricingClaimId);
            }
            
            viewmodel.pricingPrevServiceTypeId = viewmodel.pricingServiceTypeId;

            return View(viewmodel);
        }

        [HttpPost]
        public ActionResult getServiceLOBTypes(NuGenPricingScheduleViewModel viewmodel, FormCollection formData)
        {
            
            return Json(formData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetLobDetail(NuGenPricingScheduleViewModel viewmodel)
        {
         //   double rcvPrice = double.Parse(viewmodel.AutoInvoiceDetails.RCV.Value.ToString());
            Int32 servicetypeid = Convert.ToInt32(viewmodel.pricingServiceTypeId);
            int lobId = Convert.ToInt32(viewmodel.pricingLineofBusinessId);
            bool IsFound = false;

            ServiceLOBPricing obj = new ServiceLOBPricing();
            //obj = (from c in css.ServiceLOBPricings
            //       where c.ServiceTypeId == servicetypeid && c.LOBId == lobId && (rcvPrice >= c.StartRange && rcvPrice <= c.EndRange)
            //       select c).FirstOrDefault();

            obj = (from c in css.ServiceLOBPricings
                   where c.ServiceTypeId == servicetypeid && c.LOBId == lobId 
                   select c).FirstOrDefault();
            if(obj != null)
            {
                IsFound = true;
            }
            viewmodel.getNuGenClaimDetailResult(viewmodel.pricingClaimId);
            
            if (!IsFound)
                return Json(new { _MatchFound = IsFound });

            return Json(new {
                _MatchFound = IsFound,
                _BaseServiceCharge = obj.BaseServiceFee,
                _HourlyRate = obj.HourlyRate,
                _FreeMiles = obj.FreeMile,
                _DollarPerMile = obj.DollarPerMile,
                _TotalLossFee = obj.TotalLossFee
            });
        }

        public ActionResult AutoSearchNew(XactAutoAssignmentSearchViewModel viewmodel, FormCollection form)
        {
            DateTime From = Convert.ToDateTime(viewmodel.DateFrom);
            DateTime To = Convert.ToDateTime(viewmodel.DateTo);

            //if (From > To)
            //{
            //    return new JavascriptResult() { Script = "alert('Successfully registered');" };
            //}

            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

            viewmodel.Pager = new BLL.Models.Pager();
            viewmodel.Pager.IsAjax = false;
            viewmodel.Pager.FormName = "formAutoSearch";

            if ((form["hdnCurrentPage"]) != "" && (form["hdnCurrentPage"]) != null)
            {

                viewmodel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);

            }
            else
            {
                viewmodel.Pager.Page = 1;
            }
            viewmodel.Pager.RecsPerPage = 10;
            if (form["hdnstartPage"] != null)
            {
                if (!String.IsNullOrEmpty(form["hdnstartPage"]))
                {
                    viewmodel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
                }
                else
                {
                    viewmodel.Pager.FirstPageNo = 1;
                }
            }
            if ((form["hdnFlagClaim"] == "0" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"]) || (form["hdnlastOrderByValue"] != form["hdnOrderByValue"]) && (form["hdnOrderByValue"] != ""))
            {
                viewmodel.OrderDirection = true;
                form["hdnFlagClaim"] = "1";
            }
            else if (form["hdnFlagClaim"] == "1" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"])
            {
                viewmodel.OrderDirection = false;
                form["hdnFlagClaim"] = "0";
            }

            viewmodel.OrderByField = form["hdnOrderByValue"];
            if (!String.IsNullOrEmpty(viewmodel.AutoClaimNumber))
            {
                viewmodel.AutoPolicyNumber = null;
                viewmodel.LastName = null;
                viewmodel.City = null;
                viewmodel.Zip = null;
                viewmodel.Distance = null;
                viewmodel.State = "0";
                viewmodel.CatCode = null;
                viewmodel.LossType = "0";
                viewmodel.FileStatus = 0;
                viewmodel.DateFrom = null;
                viewmodel.DateTo = null;
                viewmodel.InsuranceCompany = 0;
                viewmodel.CSSPOCUserId = 0;
                viewmodel.CSSQAAgentUserId = 0;
                viewmodel.DateReceivedFrom = null;
                viewmodel.DateReceivedTo = null;
                viewmodel.IsTriageAvailable = false;
                viewmodel.SPName = null;
            }
            viewmodel.SearchOrExport = 0;

            BLL.User user = css.Users.Find(loggedInUser.UserId);

            if (user.UserTypeId == 3) // 3 is Claim Representative
            {
                viewmodel.InsuranceCompany = Convert.ToInt32(user.HeadCompanyId);
                //InsuranceCompanies
            }


            viewmodel.AutoClaimsInfo = GetAutoAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewmodel).ToList();


            viewmodel.Pager.TotalCount = viewmodel.AutoClaimsInfo[0].TotalRecords.ToString(); // getAssignmentSearchResult(loggedInUser, viewModel).Count().ToString();


            if (Convert.ToInt32(viewmodel.Pager.TotalCount) % Convert.ToInt32(viewmodel.Pager.RecsPerPage) != 0)
            {
                viewmodel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewmodel.Pager.TotalCount) / Convert.ToInt32(viewmodel.Pager.RecsPerPage)))).ToString();
            }
            else
            {
                viewmodel.Pager.NoOfPages = ((Convert.ToInt32(viewmodel.Pager.TotalCount) / Convert.ToInt32(viewmodel.Pager.RecsPerPage))).ToString();
            }


            // return View(viewmodel);
            return Json(RenderPartialViewToString("_AutoClaimSearchList", viewmodel), JsonRequestBehavior.AllowGet);
        }

        public IEnumerable<AutoClaimsSearch_Result> GetAutoAssignmentSearchResult(User SearchPerformedByUser, XactAutoAssignmentSearchViewModel viewModel)
        {

            IEnumerable<AutoClaimsSearch_Result> valueToReturn = null;
            Int64? headCompanyId = null;
            Int64? OAUserId = null;
            Int64? CSSQAAgentUserId = null;

            bool hasQAAgentRights = false;

            if (SearchPerformedByUser != null)
            {
                List<usp_UserAssignedRightsGetList_Result> userAssignedRights = css.usp_UserAssignedRightsGetList(SearchPerformedByUser.UserId).ToList();
                if (userAssignedRights.Where(x => x.UserRightId == 1).ToList().Count == 1)
                {
                    hasQAAgentRights = true;
                }

                if (SearchPerformedByUser.UserTypeId.HasValue)
                {
                    if (SearchPerformedByUser.UserTypeId.Value == 3)
                    {
                        headCompanyId = SearchPerformedByUser.HeadCompanyId;
                    }
                    else if (SearchPerformedByUser.UserTypeId.Value == 11)
                    {
                        //Client POC User can search claims assigned only to their company. 17-01-2014
                        headCompanyId = SearchPerformedByUser.HeadCompanyId;
                        if (headCompanyId.HasValue)
                        {
                            viewModel.InsuranceCompany = headCompanyId.Value;
                        }
                    }
                    else if (SearchPerformedByUser.UserTypeId.Value == 1 && !hasQAAgentRights && viewModel.SearchMode == BLL.ViewModels.XactAutoAssignmentSearchViewModel.ViewMode.GENERAL)
                    {
                        OAUserId = SearchPerformedByUser.UserId;
                    }
                    else if (SearchPerformedByUser.UserTypeId.Value == 8 || hasQAAgentRights)
                    {
                        CSSQAAgentUserId = SearchPerformedByUser.UserId;
                    }
                }
                if (CSSQAAgentUserId > 0 && viewModel.FileStatus == 6)
                {
                    CSSQAAgentUserId = null;
                    viewModel.OrderByField = "Returned";
                    viewModel.OrderDirection = false;
                }
                else
                {
                    //kept in else for better readibility
                    CSSQAAgentUserId = null;
                }
                if (viewModel.CSSQAAgentUserId != 0)
                {
                    CSSQAAgentUserId = viewModel.CSSQAAgentUserId;
                }
                if (viewModel.DateFrom == DateTime.MinValue)
                {
                    viewModel.DateFrom = Convert.ToDateTime(null);
                }
                if (viewModel.DateTo == DateTime.MinValue)
                {
                    viewModel.DateTo = Convert.ToDateTime(null); ;
                }

            }
            //valueToReturn = css.SearchClaims(viewModel.InsuranceCompany, OAUserId, viewModel.CSSQAAgentUserId, viewModel.ClaimNumber, viewModel.PolicyNumber, viewModel.CatCode, viewModel.TransactionId, viewModel.Zip, viewModel.State, viewModel.LossType, viewModel.WorkFlowStatus, viewModel.FileStatus, viewModel.JobType, viewModel.City, viewModel.LastName, Convert.ToInt32(viewModel.Distance));
             valueToReturn = css.AutoClaimsSearch(
			 viewModel.InsuranceCompany,
			 OAUserId, 
			 viewModel.CSSPOCUserId, 
			 CSSQAAgentUserId, 
			 viewModel.AutoClaimNumber, 
			 viewModel.AutoPolicyNumber, 
			 viewModel.CatCode, 
			 viewModel.Zip, 
			 viewModel.State,
			 viewModel.LossType,
			 Convert.ToByte(viewModel.FileStatus),
			 viewModel.City, 
			 viewModel.LastName, 
			 Convert.ToInt32(viewModel.Distance),
			 viewModel.OrderDirection,
			 viewModel.OrderByField,
			 viewModel.Pager.Page,
			 viewModel.Pager.RecsPerPage,					 
			 viewModel.DateFrom,
			 viewModel.DateTo,
			 SearchPerformedByUser.UserId,
		     viewModel.IsTriageAvailable,
			 viewModel.DateReceivedFrom,
			 viewModel.DateReceivedTo, 
			 viewModel.SPName, 
			 viewModel.AutoInvoiceNumber,
			 viewModel.SearchOrExport,
		     viewModel.InsureName, 
			 viewModel.ClaimType, 
			 viewModel.ServiceId,
			 viewModel.ClientContactName
			 );
            return valueToReturn;




            //valueToReturn = css.AutoClaimsGetListNew(viewmodel.AutoClaimNumber,viewmodel.InsuranceCompany,viewmodel.AutoPolicyNumber,viewmodel.Zip,viewmodel.State,viewmodel.City,viewmodel.ClaimType,viewmodel.DateReceivedFrom,viewmodel.DateReceivedTo,viewmodel.Pager.Page,viewmodel.Pager.RecsPerPage);

            //return valueToReturn;
        }

        public ActionResult Details(string claimid)
        {
            Int64 ClaimID = 0;
            ClaimID = Convert.ToInt64(Cypher.DecryptString(Cypher.ReplaceCharcters(claimid)));
            XACTAutoClaimsInfoViewModel viewmodel = new XACTAutoClaimsInfoViewModel(ClaimID);
            //INPSW - 27 12-10-2018 chk if serviceprovider login then hide cancel status - start   
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            viewmodel.FileStatusList = new List<SelectListItem>();
            viewmodel.FileStatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (FileStatusGetList_Result filestatus in css.FileStatusGetList())
            {
                if (loggedInUser.UserTypeId == 1 && filestatus.StatusId == 21)
                {
                    continue;
                }

                viewmodel.FileStatusList.Add(new SelectListItem { Text = filestatus.StatusDescription, Value = filestatus.StatusId.ToString() });
            }
           

            //INPSW -27 -End
            viewmodel.IsNuGenClaim = false;
            return View(viewmodel);
        }

        public ActionResult NuGenClaimDetails(string claimid)
        {
            Int64 ClaimID = 0;
            ClaimID = Convert.ToInt64(Cypher.DecryptString(Cypher.ReplaceCharcters(claimid)));
            XACTAutoClaimsInfoViewModel viewmodel = new XACTAutoClaimsInfoViewModel(ClaimID);
            viewmodel.IsNuGenClaim = true;
            viewmodel.getNugetVehicleDetails(ClaimID);
            return View("Details", viewmodel);
        }



        #region Auto Claim Documents

        public ActionResult DocumentAcess(Int64 docTypeId = 0)
        {
            string Fileurl = "";

            try
            {
                BLL.AutoDocument document = css.AutoDocuments.Find(docTypeId);
                string baseFolder = System.Configuration.ConfigurationManager.AppSettings["AutoAssignmentDocsPath"].ToString();

                //string assignmentId = document.AutoAssignmentId + "";

                string assignmentId = document.AutoAssignmentId + "";
                bool IsNugen = Convert.ToBoolean((from c in css.Claims
                                                  where c.ClaimId == ((from ass in css.AutoAssignments
                                                                       where ass.AutoAssignmentId == document.AutoAssignmentId
                                                                       select ass.ClaimId).FirstOrDefault())
                                                  select c.IsNugeAssignment).FirstOrDefault());

                if (document.Title.ToLower() == "service invoice" || IsNugen == false)
                {
                    assignmentId = document.AutoAssignmentId.ToString();
                }
                else
                {
                    assignmentId = ((
                                           from a in css.AutoInvoices
                                           where a.AutoAssignmentId == document.AutoAssignmentId
                                           select a.AutoInvoiceId
                                         ).FirstOrDefault()).ToString();
                }




                string documentTypeId = document.DocumentTypeId + "";
                //check whether the folder with user's userid exists
                string filename = document.AutoDocumentPath;
                //check whether the destination folder depending on the type of document being uploaded exists
                if (MvcApplication.UseAzureCloudStorage == "1")
                {
                    Fileurl = CloudStorageUtility.GetAbsoluteFileURL(MvcApplication.AutoAssignDocsContainerName, assignmentId + "/" + documentTypeId + "/" + document.AutoDocumentPath);
                }
                else
                {
                    if (System.IO.File.Exists(Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), document.AutoDocumentPath)))
                    {
                        Fileurl = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), document.AutoDocumentPath);
                    }
                    else
                    {
                        Fileurl = Path.Combine(Server.MapPath(baseFolder), "NotFound.txt");
                        filename = "NotFound.txt";
                    }
                }
                // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder



                string contentType = string.Empty;

                if (filename.Contains(".pdf"))
                {
                    contentType = "application/pdf";
                }

                else if (filename.Contains(".txt"))
                {
                    contentType = "text/plain";
                }
                else if (filename.Contains(".docx"))
                {
                    contentType = "application/docx";
                }
                else if (filename.Contains(".png"))
                {
                    contentType = "application/png";
                }
                else if (filename.Contains(".jpeg"))
                {
                    contentType = "application/jpeg";
                }
                else if (filename.Contains(".jpg"))
                {
                    contentType = "application/jpg";
                }
                else if (filename.Contains(".xlsx"))
                {
                    contentType = "application/xlsx";
                }

                return new ReportResult(Fileurl, filename, contentType);
                //  return FilePath(Fileurl, contentType, filename);  

            }
            catch (Exception ex)
            {

            }
            return File("", "application/pdf");

        }
        public ActionResult DocumentAcessFilePath(Int64 docTypeId = 0)
        {
            string Fileurl = "", assignmentId="";

            try
            {
                BLL.AutoDocument document = css.AutoDocuments.Find(docTypeId);
                string baseFolder = MvcApplication.AutoAssignmentDocsPath;
                bool IsNugen = Convert.ToBoolean((from c in css.Claims
                                                  where c.ClaimId == ((from ass in css.AutoAssignments
                                                                       where ass.AutoAssignmentId == document.AutoAssignmentId
                                                                       select ass.ClaimId).FirstOrDefault())
                                                  select c.IsNugeAssignment).FirstOrDefault());

                //string assignmentId = document.AutoAssignmentId + "";
                if (document.Title.ToLower() == "service invoice" || IsNugen == false)
                {
                    assignmentId = document.AutoAssignmentId.ToString();
                }
                else
                {
                    assignmentId = ((
                                           from a in css.AutoInvoices
                                           where a.AutoAssignmentId == document.AutoAssignmentId
                                           select a.AutoInvoiceId
                                         ).FirstOrDefault()).ToString();
                }
                string documentTypeId = document.DocumentTypeId + "";
                //check whether the folder with user's userid exists
                string filename = document.AutoDocumentPath;

                string contentType = Utility.GetContentType(filename);
                if (MvcApplication.UseAzureCloudStorage == "1")
                {
                    return File(CloudStorageUtility.GetByteStramFromAbsoluteFileURL(MvcApplication.AutoAssignDocsContainerName, assignmentId + "/" + documentTypeId + "/" + document.AutoDocumentPath), contentType);

                }
                else
                {
                    if (System.IO.File.Exists(Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), document.AutoDocumentPath)))
                    {
                        Fileurl = Path.Combine((VirtualPathUtility.ToAbsolute(baseFolder) + "" + assignmentId + "/" + documentTypeId), document.AutoDocumentPath);
                    }
                    else
                    {
                        Fileurl = Path.Combine(VirtualPathUtility.ToAbsolute(baseFolder), MvcApplication.NotFoundFileName);
                    }
                }
                return File(Fileurl, contentType);
            }
            catch (Exception ex)
            {

            }
            return File(Path.Combine(VirtualPathUtility.ToAbsolute(MvcApplication.AutoAssignmentDocsPath), MvcApplication.NotFoundFileName), "text/plain");

        }

        public ActionResult GetFirstDocumentFilePath(Int64 Assignmentid = 0)
        {
            string Fileurl = "", assignmentId = "";


            try
            {
                BLL.AutoDocument document = css.AutoDocuments.Where(x => x.AutoAssignmentId == Assignmentid).OrderByDescending(x => x.AutoDocumentUploadedDate).FirstOrDefault();
                string baseFolder = System.Configuration.ConfigurationManager.AppSettings["AutoAssignmentDocsPath"].ToString();
                bool IsNugen = Convert.ToBoolean((from c in css.Claims
                                                  where c.ClaimId == ((from ass in css.AutoAssignments
                                                                       where ass.AutoAssignmentId == Assignmentid
                                                                       select ass.ClaimId).FirstOrDefault())
                                                  select c.IsNugeAssignment).FirstOrDefault());

                //string assignmentId = document.AutoAssignmentId + "";
                if (document.Title.ToLower() == "service invoice" || IsNugen == false)
                {
                    assignmentId = Assignmentid.ToString();
                }
                else
                {
                    assignmentId = ((
                                           from a in css.AutoInvoices
                                           where a.AutoAssignmentId == Assignmentid
                                           select a.AutoInvoiceId
                                         ).FirstOrDefault()).ToString();
                }

                
                string documentTypeId = document.DocumentTypeId + "";
                //check whether the folder with user's userid exists
                string filename = document.AutoDocumentPath;

                //  Fileurl = Path.Combine((baseFolder + "" + assignmentId + "/" + documentTypeId), document.AutoDocumentPath);
                // check whether the destination folder depending on the type of document being uploaded exists
                string contentType = Utility.GetContentType(filename);
                if (MvcApplication.UseAzureCloudStorage == "1")
                {

                    return File(CloudStorageUtility.GetByteStramFromAbsoluteFileURL(MvcApplication.AutoAssignDocsContainerName, assignmentId + "/" + documentTypeId + "/" + document.AutoDocumentPath), contentType);
                }
                else
                {
                    if (System.IO.File.Exists(Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), document.AutoDocumentPath)))
                    {
                        Fileurl = Path.Combine((VirtualPathUtility.ToAbsolute(baseFolder) + "" + assignmentId + "/" + documentTypeId), document.AutoDocumentPath);
                    }
                    else
                    {
                        Fileurl = Path.Combine(VirtualPathUtility.ToAbsolute(baseFolder), MvcApplication.NotFoundFileName);
                    }
                }
                return File(Fileurl, contentType);
            }
            catch (Exception ex)
            {

            }
            return File(Path.Combine(VirtualPathUtility.ToAbsolute(MvcApplication.AutoAssignmentDocsPath), MvcApplication.NotFoundFileName), "text/plain");


        }
        //public ActionResult InitAutoClaimDocuments(Int32 claimid, Int32 AssingmentID)
        //{
        //    DocumentsListViewModel viewmodel = new DocumentsListViewModel();
        //    return Json(viewmodel,JsonRequestBehavior.AllowGet);
        //}
        [NonAction]
        public DocumentsListViewModel InitAutoDocumentModel(Int64 claimid, Int64 AssingmentID, short documentTypeId = 0, byte? statusId = 4)
        {
            
            DocumentsListViewModel viewModel = new BLL.ViewModels.DocumentsListViewModel(claimid, AssingmentID, documentTypeId);


            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            //int? headCompanyId = css.Claims.Find(claimId).HeadCompanyId;
            //if (headCompanyId.HasValue)
            //{
            //    viewModel.IsFTPDocExportEnabled = css.Companies.Find(headCompanyId.Value).EnableFTPDocExport ?? false;
            //}

            //viewModel.DocumentsList = css.DocumentsList(AssingmentID, documentTypeId).ToList();
            viewModel.DocumentsList = css.DocumentsList(claimid,documentTypeId).ToList();
            //viewModel.DocumentsPreviousList = css.usp_DocumentsPreviousGetList(AssingmentID, documentTypeId).ToList();
            viewModel.DocumentsPreviousList = css.usp_DocumentsPreviousGetList(claimid,documentTypeId).ToList();
            //viewModel.DocumentsRejectedList = css.RejectedDocumentsList(AssingmentID, documentTypeId).ToList();
            viewModel.DocumentsRejectedList = css.RejectedDocumentsList(claimid,documentTypeId).ToList();

            //if (loggedInUser.UserTypeId == 1 || loggedInUser.UserTypeId == 8)
            //{
            //    viewModel.DocumentsList.RemoveAll(x => x.DocumentTypeId == 10);
            //    viewModel.DocumentsPreviousList.RemoveAll(x => x.DocumentTypeId == 10);
            //    viewModel.DocumentsRejectedList.RemoveAll(x => x.DocumentTypeId == 10);



            //}
            //  CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            //Client POC should see only the approved documents -17-01-2014
            //if (loggedInUser.UserTypeId == 11)
            //{
            //    List<DocumentsList_Result> tempList = viewModel.DocumentsList.ToList();
            //    tempList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);

            //    viewModel.DocumentsList = tempList.ToList();
            //    viewModel.DocumentsPreviousList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
            //}


            //Search Filter
            if (statusId == 1)//Doc Status: Approved 
            {
                List<DocumentsList_Result> tempList = viewModel.DocumentsList.ToList();
                tempList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);

                viewModel.DocumentsList = tempList.ToList();
                viewModel.DocumentsPreviousList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
                //viewModel.DocumentsRejectedList.ToList().RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
                //  viewModel.DocumentsRejectedList.ToList().RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
                viewModel.DocumentsRejectedList = null;
            }
            else if (statusId == 0)//Doc Status: Rejected
            {
                List<DocumentsList_Result> tempList = viewModel.DocumentsList.ToList();
                tempList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : true) == true);

                viewModel.DocumentsList = tempList.ToList();
                viewModel.DocumentsPreviousList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : true) == true);
                //  viewModel.DocumentsRejectedList.ToList().RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
            }
            else if (statusId == 2)//Doc Status: Pending Approval
            {
                // IEnumerable<DocumentsList_Result> obj = (IEnumerable<DocumentsList_Result>)viewModel.DocumentsList;
                List<DocumentsList_Result> tempList = (List<DocumentsList_Result>)viewModel.DocumentsList.ToList();
                tempList.RemoveAll(x => x.Status.HasValue == true);

                viewModel.DocumentsList = tempList.ToList();
                viewModel.DocumentsPreviousList.RemoveAll(x => x.Status.HasValue == true);
                viewModel.DocumentsRejectedList = null;
                //viewModel.DocumentsRejectedList.ToList().RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
                //viewModel.DocumentsRejectedList.ToList().RemoveAll(x => x.Status.HasValue == true);
            }

            viewModel.ClaimDocumentType = css.ClaimDocumentTypes.Find(documentTypeId);
            AutoAssignment pa = css.AutoAssignments.Find(AssingmentID);

            //List<GetClaimParticipants_Result> claimParticipantsList = null;
            //if (pa != null)
            //{
            //    claimParticipantsList = css.GetClaimParticipants(pa.ClaimId).ToList();
            //}
            //string participantsEmails = "";
            //foreach (GetClaimParticipants_Result participant in claimParticipantsList)
            //{
            //    if (!String.IsNullOrEmpty(participant.Email))
            //    {
            //        if (participant.UserType != "Insured")
            //        {
            //            if (participantsEmails.Length != 0)
            //            {
            //                participantsEmails += ", ";
            //            }
            //            participantsEmails += participant.Email;
            //        }
            //    }
            //}
            //viewModel.ClaimParticipantsEmailAddresses = participantsEmails;
            if (viewModel.ClaimDocumentType == null)
            {
                viewModel.ClaimDocumentType = new ClaimDocumentType();
            }
            return viewModel;
        }
        public ActionResult InitAutoClaimDocuments(Int32 claimid, Int32 AssingmentID, short documentTypeId = 0, byte? statusId = 4)
        {
            DocumentsListViewModel viewModel = new DocumentsListViewModel();

            viewModel = InitAutoDocumentModel(claimid, AssingmentID, documentTypeId, statusId);

            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }
        public ActionResult InitializeDocuments(Int32 claimid, Int32 AssingmentID, short documentTypeId = 0, byte? statusId = 4)
        {
            DocumentsListViewModel viewModel = new DocumentsListViewModel();

            viewModel = InitAutoDocumentModel(claimid, AssingmentID, documentTypeId, statusId);
            viewModel.StatusList = new List<SelectListItem>();
            viewModel.StatusList.Add(new SelectListItem() { Text = "--Select--", Value = "4" });//Default
            viewModel.StatusList.Add(new SelectListItem() { Text = "Approved", Value = "1" });
            viewModel.StatusList.Add(new SelectListItem() { Text = "Rejected", Value = "0" });
            viewModel.StatusList.Add(new SelectListItem() { Text = "Pending Approval", Value = "2" });//Database value for Pending Approval is NULL
            viewModel.StatusId = (byte)4;
            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UploadClaimDocument(HttpPostedFileBase uploadFile, AutoClaimDocumentViewModel viewModel)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            try
            {
                if (ModelState.IsValid)
                {
                    if (uploadFile != null && uploadFile.ContentLength > 0)
                    {
                        if (viewModel.AutoDocument != null)
                        {
                            UploadDocument(viewModel.AutoDocument, uploadFile);
                            if(viewModel.AutoDocument.DocumentTypeId ==1)
                            {
                                css.UpdateNewAutoAssignmentStatus(viewModel.AutoDocument.AutoAssignmentId, 6, loggedInUser.UserId, null, 0,false);
                                AssignmentStatu assignmentStatu = css.AssignmentStatus.Find(6);
                                ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                                css.usp_NotesInsert(outNoteId, viewModel.AutoDocument.AutoAssignmentId, "Status changed to " + assignmentStatu.StatusDescription, "Status changed to " + assignmentStatu.StatusDescription, DateTime.Now, false, true, loggedInUser.UserId, null, null, null, null, null, null);
                               
                                
                                AutoAssignment _autoassignments = null;
                                Claim _claim = null;
                                Int64? ToUserID;
                                _autoassignments = (from Auto_Assignments in css.AutoAssignments
                                                    where Auto_Assignments.AutoAssignmentId == viewModel.AutoDocument.AutoAssignmentId
                                                    select Auto_Assignments).FirstOrDefault();
                                _claim = (from c in css.Claims
                                          where c.ClaimId == viewModel.ClaimId
                                          select c).FirstOrDefault();
                                
                                //INPSW -11 strat comment
                                //if (loggedInUser.UserId == _autoassignments.RepresentativeID)
                                //    {
                                //        ToUserID = _autoassignments.OAUserID;
                                //    }
                                //else
                                //    {
                                //        ToUserID = _autoassignments.RepresentativeID;
                                //    }
                                //INPSw-11 end comment

                                //INPSW -11
                                List<Int64> ID = new List<Int64>();                                                            
                                if (loggedInUser.UserTypeId == 3 || loggedInUser.UserTypeId == 12)
                                {
                                    ID.Add((Int64) _autoassignments.OAUserID);
                                   // ToUserID = _autoassignments.OAUserID;
                                }
                                else if(loggedInUser.UserTypeId == 2)
                                {
                                     ID.Add((Int64) _autoassignments.OAUserID);
                                     ID.Add((Int64)_autoassignments.RepresentativeID);
                                }
                                else
                                {
                                    //ToUserID = _autoassignments.RepresentativeID;
                                    ID.Add((Int64)_autoassignments.RepresentativeID);
                                }

                               
                                var UserInfo = (from USR in css.Users
                                                where (USR.UserId == loggedInUser.UserId)
                                                select new
                                                {
                                                    FirstName = USR.FirstName,
                                                    LastName = USR.LastName
                                                }).FirstOrDefault();
                                #region Status changed notification email
                                for (int i = 0; i < ID.Count(); i++)
                                {

                                    try
                                    {
                                        string documentName = "";
                                        if(uploadFile.FileName.Length>0)
                                        {
                                            documentName = uploadFile.FileName;
                                        }                                       
                                        String DocumentList = "<ul>";                                       
                                        DocumentList = DocumentList + "<li>" + documentName + "</li>";                                     
                                        DocumentList = DocumentList + "</ul>";

                                        List<string> toEmails = new List<string>();
                                        var userObj = getUserSet(ID[i]);
                                        if (userObj != null)
                                        {
                                            toEmails.Add(userObj.Email);
                                        }
                                        if (toEmails.Count > 0)
                                        {
                                            string mailBody = "<br />"; 
                                            mailBody += "DO NOT REPLY TO THIS EMAIL. THIS IS A SYSTEM GENERATED EMAIL. THE EMAIL MAILBOX IS NOT MONITORED.";
                                            mailBody += "<br />";
                                            mailBody += "<br />";
                                            //mailBody += "Dear " + userObj.FirstName + " " + userObj.LastName;
                                            mailBody += "The following document(s) has been added to:<br /><br />";
                                            mailBody += "Claim Number: "+_claim.ClaimNumber;
                                            mailBody += "<br />Uploaded By: " + UserInfo.FirstName + " " + UserInfo.LastName;
                                            mailBody += "<br />";
                                            mailBody += "<br />";
                                            mailBody += "Documents:" + DocumentList;
                                            mailBody += "<br />";                                            
                                            mailBody += "To review these Documents, log into <a href=\"" + ConfigurationManager.AppSettings["IadaLoginUrl"] + "\">www.IADApro.com</a><br />";
                                            mailBody += "<br />Thank you,";
                                            mailBody += "<br />";
                                            mailBody += "IADA Customer Support Team";
                                            Utility.sendEmail(toEmails, MvcApplication.AdminMail, "Claim #: " + _claim.ClaimNumber + " - Documents Uploaded Notification", mailBody);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Utility.LogException("method uploaddocuments ", ex, "An Error occured while sending email notification:" + viewModel.AutoAssignmentId);
                                    }



                                    try
                                    {
                                        List<string> toEmails = new List<string>();
                                        var userObj = getUserSet(ID[i]);
                                        if (userObj != null)
                                        {
                                            toEmails.Add(userObj.Email);
                                        }
                                        if (toEmails.Count > 0)
                                        {
                                            string mailBody = "<br />";
                                            mailBody += "DO NOT REPLY TO THIS EMAIL. THIS IS A SYSTEM GENERATED EMAIL. THE EMAIL MAILBOX IS NOT MONITORED.";
                                            mailBody += "<br />";
                                            mailBody += "<br />";
                                            mailBody += "Dear " + userObj.FirstName + " " + userObj.LastName +",";
                                            mailBody += "<br />";
                                            mailBody += "<br /> The status of claim " + _claim.ClaimNumber + " has changed to " + assignmentStatu.StatusDescription + ". If a Note was entered at the time of Status Change it has been provided below.Log into <a href=\"" + ConfigurationManager.AppSettings["IadaLoginUrl"] + "\">www.IADApro.com</a> to view the claim.";
                                            mailBody += "<br />";
                                            mailBody += "<br />";
                                            mailBody += "<br />Thank you,";
                                            mailBody += "<br />";
                                            mailBody += "IADA Customer Support Team";                                     
                                            Utility.sendEmail(toEmails, MvcApplication.AdminMail, "Claim #: " + _claim.ClaimNumber+ " - Assignment Status Change Notification", mailBody);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Utility.LogException("Method uploaddocuments ", ex, "An Error occured while sending email notification:" + viewModel.AutoAssignmentId);
                                    }
                                }
                                #endregion
                            
                            };

                            //   return RedirectToAction("UploadClaimDocumentSuccess");
                            DocumentsListViewModel Model = InitAutoDocumentModel(viewModel.ClaimId, viewModel.AutoDocument.AutoAssignmentId, (short)viewModel.AutoDocument.DocumentTypeId, 4);
                            return Json(Model, JsonRequestBehavior.AllowGet);

                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return View(viewModel);
        }
        public void UploadDocument(BLL.AutoDocument document, HttpPostedFileBase uploadFile)
        {
            string strPartialUploadedFilePath = "";
            if (uploadFile != null && uploadFile.ContentLength > 0)
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                string fileName = string.Empty, storeAsFileName = string.Empty;
                if (MvcApplication.UseAzureCloudStorage == "1")
                {

                    //#region Cloud Storage

                    // extract only the fielname
                    fileName = new FileInfo(uploadFile.FileName).Name;
                    storeAsFileName = Regex.Replace(fileName, "[^0-9a-zA-Z-_.]+", "");

                    string assignmentId = document.AutoAssignmentId + "";
                    string documentTypeId = document.DocumentTypeId + "";
                    //check whether the folder with user's userid exists

                    // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                    string relativeFileName = assignmentId + "/" + documentTypeId + "/" + storeAsFileName;
                    strPartialUploadedFilePath = relativeFileName;
                    MemoryStream bufferStream = new System.IO.MemoryStream();
                    uploadFile.InputStream.CopyTo(bufferStream);
                    byte[] buffer = bufferStream.ToArray();

                    //Original doc storage 
                    string containerName = MvcApplication.AutoAssignDocsContainerName;
                    CloudStorageUtility.StoreFile(containerName, relativeFileName, buffer);
                    bufferStream.Close();
                    bufferStream.Dispose();

                    //#endregion
                }
                else
                {
                    #region Local File System

                    // extract only the fielname
                    fileName = new FileInfo(uploadFile.FileName).Name;
                    storeAsFileName = Regex.Replace(fileName, "[^0-9a-zA-Z-_.]+", "");
                    string baseFolder = MvcApplication.AutoAssignmentDocsPath;
                    string assignmentId = document.AutoAssignmentId + "";
                    string documentTypeId = document.DocumentTypeId + "";
                    //check whether the folder with user's userid exists
                    if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId)))
                    {
                        Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId));
                    }
                    //check whether the destination folder depending on the type of document being uploaded exists
                    if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId)))
                    {
                        Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId));
                    }

                    // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                    string path = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), storeAsFileName);
                    uploadFile.SaveAs(path);

                    #endregion
                }

                ObjectParameter outDocumentId = new ObjectParameter("DocumentId", DbType.Int64);
                css.AutoDocumentsInsert(outDocumentId, document.AutoAssignmentId, DateTime.Now, document.Title, fileName, storeAsFileName, loggedInUser.UserId, document.DocumentTypeId);


                //------------------- WeGoLook Upload Docs ----------------------

                //Token
                var _WGLClaim = (from c in css.Claims
                                join a in css.AutoAssignments on c.ClaimId equals a.ClaimId
                                where a.AutoAssignmentId == document.AutoAssignmentId
                                select new
                                {
                                    WGLClaimId = c.ClaimId,
                                    WGLClaimNumber = c.ClaimNumber,
                                    WGLJobId = c.WGLJobId,
                                    WGLJobNumber = c.WGLJobNumber,
                                    WGLDocUrl = c.WGLSSignedURL,
                                    WGLServiceId = a.ServiceId
                                }).FirstOrDefault();

                if (!string.IsNullOrEmpty(_WGLClaim.WGLJobId) && !string.IsNullOrEmpty(_WGLClaim.WGLJobNumber) && !string.IsNullOrEmpty(_WGLClaim.WGLDocUrl))
                {
                    WGLUser user = null;
                    WGLToken token = new WGLToken();
                    string baseURL = ConfigurationManager.AppSettings["WGLBaseUrl"];
                    string userName = ConfigurationManager.AppSettings["WGLUsername"];
                    string password = ConfigurationManager.AppSettings["WGLPassword"];

                    user = new WGLUser()
                    {
                        email = userName,
                        password = password
                    };

                    try
                    {
                        using (var client = new HttpClient())
                        {
                            client.BaseAddress = new Uri(baseURL);
                            client.DefaultRequestHeaders.Clear();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("applicatinon/json"));
                            string strbody = Newtonsoft.Json.JsonConvert.SerializeObject(user);
                            StringContent stringContent = new StringContent(strbody, System.Text.Encoding.UTF8, "application/json");
                            HttpResponseMessage Res = client.PostAsync("tokens", stringContent).Result;

                            //Check if sucesses
                            if (Res.IsSuccessStatusCode)
                            {
                                var tokenResponse = Res.Content.ReadAsStringAsync().Result;
                                token = Newtonsoft.Json.JsonConvert.DeserializeObject<WGLToken>(tokenResponse);
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }


                    if (!string.IsNullOrEmpty(token.token))
                    {
                        //Docs upload to Amazon
                        string wglFileUrl = DocsUpload.GetAbsoluteFileURL(MvcApplication.AutoAssignDocsContainerName, strPartialUploadedFilePath);
                        string wglAWSS3UploadedFilePath = DocsUpload.UploadDocs(_WGLClaim.WGLDocUrl, wglFileUrl);


                        if (!wglAWSS3UploadedFilePath.Contains("Error"))
                        {
                            long tmpDocId = Convert.ToInt64(outDocumentId.Value);
                            AutoDocument _AutoDocument = (
                                                            from c in css.AutoDocuments
                                                            where c.AutoDocumentId == tmpDocId
                                                            select c
                                                          ).FirstOrDefault();

                            _AutoDocument.WGLS3DocUrl = wglAWSS3UploadedFilePath;
                            css.SaveChanges();

                            //_AutoDocument

                            //Property
                            if (_WGLClaim.WGLServiceId == 9 || _WGLClaim.WGLServiceId == 11 || _WGLClaim.WGLServiceId == 12)
                            {
                                usp_WGLPropertyDocUpload_Result item = css.usp_WGLPropertyDocUpload(_WGLClaim.WGLClaimId).FirstOrDefault();

                                PropertyLookPutRequest _LookPropertyPutRequest = new PropertyLookPutRequest()
                                {
                                    reference_number = _WGLClaim.WGLClaimNumber,
                                    property_information_address = item.PropertyAddress1,
                                    property_information_city = item.PropertyCity,
                                    property_information_state = item.PropertyState,
                                    property_information_zip_code = item.PropertyZip,
                                    //property_information_coordinates=null,
                                    property_information_coordinates = new PropertyLookPutRequest.Property_Information_Coordinates()
                                    {
                                        type = "Point",
                                        coordinates = new int[] { 1, 2 }
                                    },
                                    property_information_map_or_identifying_site_image_information = "https://example.com/test.pdf",
                                    primary_on_site_contact_name = item.PropertyContactName,
                                    primary_on_site_contact_email = "user@example.com",
                                    primary_on_site_contact_phone = item.PropertyContactPhone,
                                    primary_on_site_contact_alternate_phone = "8552655665",
                                    secondary_on_site_contact_name = "Johnny User",
                                    secondary_on_site_contact_email = "user@example.com",
                                    secondary_on_site_contact_phone = "8552655665",
                                    description_of_damage_to_be_verified_including_any_possible_cause_of_loss = "dummy value",
                                    measurements_to_be_taken_choice = "Yes",
                                    delivery_options = 3

                                };
                            }

                            //Auto
                            if (_WGLClaim.WGLServiceId != 9 || _WGLClaim.WGLServiceId != 11 || _WGLClaim.WGLServiceId != 12)
                            {

                                usp_WGLUGetAutoDetailsForDocUpload_Result item = css.usp_WGLUGetAutoDetailsForDocUpload(_WGLClaim.WGLClaimId).FirstOrDefault();
                                AutoLookPutRequest _LookPutRequest = new AutoLookPutRequest()
                                {
                                    reference_number = _WGLClaim.WGLClaimNumber,
                                    reference_number_2 = "NA",
                                    reference_number_3 = "NA",
                                    on_site_contact_name = item.PropertyContactName,
                                    on_site_contact_email = "user@example.com",
                                    on_site_contact_phone = item.PropertyContactPhone,
                                    on_site_contact_alternate_phone = "8552655665",
                                    vehicle_location_location_type = item.VehicleLocationName,
                                    vehicle_location_address_or_intersection = item.PropertyAddress1,
                                    vehicle_location_city = item.PropertyCity,
                                    vehicle_location_state = item.PropertyState,
                                    vehicle_location_zip_code = item.PropertyZip,
                                    vehicle_location_coordinates = new AutoLookPutRequest.Vehicle_Location_Coordinates()
                                    {
                                        type = "Point",
                                        coordinates = "[40, 70]"
                                    },
                                    vehicle_information_year = string.IsNullOrEmpty(item.Year) ? 0 : int.Parse(item.Year),
                                    vehicle_information_make = item.Make,
                                    vehicle_information_model = item.AutoModel,
                                    vehicle_information_color = item.AutoColor,
                                    vehicle_information_vin = item.VIN_,
                                    vehicle_information_license_plate_number = item.LicensePlate,
                                    vehicle_information_vehicle_owner = item.OwnerFirstName,
                                    vehicle_information_is_an_estimate_needed = "Yes",
                                    vehicle_information_was_the_vehicle_subject_to_possible_flood_damage = "Yes",
                                    vehicle_information_does_this_vehicle_inspection_require_tire_tread_depth_measurement = "Yes",
                                    vehicle_information_do_photos_of_documents_need_to_be_taken = "Yes",
                                    vehicle_information_what_documents_need_to_be_captured = "dummy value",
                                    document_upload_completed_estimate = "",
                                    vehicle_information_order_comments = "dummy value",
                                    delivery_options = 3
                                };
                            }
                        }
                    }
                }


                //Post Docs



                //if (document.DocumentTypeId == 1)
                //{
                //    PropertyAssignment pa = css.PropertyAssignments.Find(document.AssignmentId);
                //    Claim claim = css.Claims.Find(pa.ClaimId);
                //    if (pa.CSSPointofContactUserId.HasValue)
                //    {
                //        if (pa.CSSPointofContactUserId.Value != 0)
                //        {
                //            if (pa.FileStatus.HasValue)
                //            {
                //                if (pa.FileStatus.Value != 6)
                //                {
                //                    DateTime cstDateTime = css.usp_GetLocalDateTime().First().Value;
                //                    ObjectParameter outDiaryId = new ObjectParameter("DiaryId", DbType.Int64);
                //                    css.DiaryItemsInsert(outDiaryId, pa.AssignmentId, Convert.ToByte(ConfigurationManager.AppSettings["GenLossRptAddedClaimNotReturnedDiaryCategoryId"]), pa.CSSPointofContactUserId.Value, pa.CSSPointofContactUserId.Value, System.DateTime.Now, cstDateTime, null, null, "Estimate not yet returned", "Loss Report Document was uploaded. The claim status has not yet been changed to Estimate Returned.", (byte)1, null, true, false, false, null);
                //                }
                //            }
                //        }
                //    }
                //}
            }
        }
        [HttpPost]
        public ActionResult ChangeDocumentStatus(AutoClaimDocumentViewModel viewModel)
        {
            int valueToReturn = 0;
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            try
            {
                string[] documentIdList = viewModel.documentIds.Split(',');

                foreach (string documentId in documentIdList)
                {
                    css.usp_DocumentsStatusUpdate(Convert.ToInt64(documentId), viewModel.newStatusId == 1 ? true : false, loggedInUser.UserId);
                }


                valueToReturn = 1;
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }

            if (valueToReturn == 1)
            {
                DocumentsListViewModel Model = InitAutoDocumentModel(viewModel.ClaimId, viewModel.AutoAssignmentId, (short)viewModel.DocumentTypeId, 4);

                #region Status changed notification email
                List<string> toEmails = new List<string>();
                var autoassignment = css.AutoAssignments.Find(viewModel.AutoAssignmentId);
                try
                {
                    if (autoassignment != null && autoassignment.User != null)
                    {
                        if (!string.IsNullOrEmpty(autoassignment.User.Email))
                            toEmails.Add(autoassignment.User.Email);
                        else if (autoassignment.User.User1 != null && !string.IsNullOrEmpty(autoassignment.User.User1.Email))
                            toEmails.Add(autoassignment.User.User1.Email);
                    }
                    if (toEmails.Count > 0)
                    {
                        string[] documentIdList = viewModel.documentIds.Split(',');
                        String DocumentList = "<ul>";
                        foreach (string documentId in documentIdList)
                        {
                            Int64? docId = Convert.ToInt64(documentId);
                            DocumentList = DocumentList + "<li>" + css.AutoDocuments.Where(x => x.AutoDocumentId == (docId ?? 0)).Select(y => y.Title + " " + y.OriginalFileName).FirstOrDefault() + " - Status changed to " + (viewModel.newStatusId == 1 ? "Approved" : "Rejected")+"</li>";
                        }
                        DocumentList = DocumentList + "</ul>";
                        string mailBody = "<br />DO NOT REPLY TO THIS EMAIL.  THIS IS A SYSTEM GENERATED EMAIL.  THE EMAIL MAILBOX IS NOT MONITORED.<br /><br />";
                        mailBody += "Dear " + autoassignment.User.FirstName + " " + autoassignment.User.LastName+",";
                        mailBody += "<br />";
                        mailBody += "<br /> The status of the following documents on Claim Number " + autoassignment.Claim.ClaimNumber + " has changed. Log into <a href=\"" + ConfigurationManager.AppSettings["IadaLoginUrl"] + "\">www.IADApro.com</a> to view the documents.";
                        mailBody += "<br />";
                        mailBody += "<br />";
                        mailBody += "<br />Document(s):" + DocumentList;
                       // mailBody += "<br />Status changed to " + (viewModel.newStatusId == 1 ? "Approved" : "Rejected");
                        mailBody += "<br />";
                        mailBody += "<br />Thanking you,";
                        mailBody += "<br />";
                        mailBody += "IADA Customer Support Team";
                        Utility.sendEmail(toEmails, MvcApplication.AdminMail, "Claim #: " + autoassignment.Claim.ClaimNumber + " - Document Status Update", mailBody);
                    }
                }
                catch (Exception ex)
                {
                    Utility.LogException("method ChangeDocumentStatus ", ex, "An Error occured while sending email notification:" + viewModel.AutoAssignmentId);
                }
                #endregion
                return Json(Model, JsonRequestBehavior.AllowGet);
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ClaimEmailDocuments(Int64 claimId, Int64 assignmentId, string documentIds, short documentTypeId = 0, byte? statusId = 4)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            string[] documentIdList = documentIds.Split(',');

            DocumentsListViewModel viewModel = new BLL.ViewModels.DocumentsListViewModel(claimId, assignmentId, documentTypeId);

            List<DocumentsListResult> doclist = new List<DocumentsListResult>();

            foreach (string documentId in documentIdList)
            {
                DocumentsListResult docselected = new DocumentsListResult();
                BLL.AutoDocument document = css.AutoDocuments.Find(Convert.ToInt64(documentId));
                docselected.DocumentId = document.AutoDocumentId;
                docselected.AssignmentId = document.AutoAssignmentId;
                docselected.Title = document.Title + "_" + document.AutoDocumentPath;
                doclist.Add(docselected);
            }
            viewModel.DocumentsListMail = doclist;

            AutoAssignment pa = css.AutoAssignments.Find(assignmentId);

            //List<GetClaimParticipants_Result> claimParticipantsList = null;
            //if (pa != null)
            //{
            //    claimParticipantsList = css.GetClaimParticipants(pa.ClaimId).ToList();
            //}
            string RepresentativeEmailId = "";
            if (pa != null)
            {
               RepresentativeEmailId= css.Users.Where(x => x.UserId == pa.RepresentativeID).Select(x => x.Email).FirstOrDefault();
            }
            //string participantsEmails = "";

            string participantsEmails = RepresentativeEmailId != null?RepresentativeEmailId : "";
            //Comment because by default set claim rep emailid 

            //foreach (GetClaimParticipants_Result participant in claimParticipantsList)
            //{
            //    if (!String.IsNullOrEmpty(participant.Email))
            //    {
            //        if (participant.UserType != "Insured")
            //        {
            //            if (participantsEmails.Length != 0)
            //            {
            //                participantsEmails += ", ";
            //            }
            //            participantsEmails += participant.Email;
            //        }
            //    }
            //}

            viewModel.ClaimParticipantsEmailAddresses = participantsEmails;
            BLL.Claim claim = css.Claims.Find(viewModel.ClaimId);

            //viewModel.subject = "Document(s) - Insured Name: " + claim.InsuredFirstName + " " + claim.InsuredLastName + " - Claim #: " + claim.ClaimNumber;
            viewModel.subject = "Claim #: " + claim.ClaimNumber + " - Documents";
            viewModel.Body = "<br />aagsgs<br />sddsds<br />";
            viewModel.authReplayUser = CSS.AuthenticationUtility.GetUser().Email;
            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EmailDocuments(Int64 assignmentId, string documentIds, int setReplyToEmail, string toEmails, string subject, string body, string mergedoc)
        {
            int valueToReturn = 0;
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            string fromEmail = System.Configuration.ConfigurationManager.AppSettings["DocumentAttachmentFromEmailAddress"].ToString();
            Int64? claimid = css.AutoAssignments.FirstOrDefault(x => x.AutoAssignmentId == assignmentId).ClaimId;
            string claimNumber = css.Claims.FirstOrDefault(x => x.ClaimId == claimid).ClaimNumber;
            string newFile1 = "";
            List<string> files = null;



            string replyToEmail = String.Empty;
            if (setReplyToEmail == 1)
            {
               // replyToEmail = loggedInUser.Email; //INPSW-8 - Change Default Send Reply To Email Address used on Documents Email
                replyToEmail = "support@iada.org";
            }

                string[] documentIdList = documentIds.Split(',');
                string[] tempToEmailList = toEmails.Split(',');
                string emailbody = "TO: " + toEmails + "\nSUBJECT: " + subject + "\n\nMESSAGE: " + body;
                List<string> attachmentFilePathList = new List<string>();
                List<string> toEmailList = new List<string>();
                List<PdfReader> readerList = new List<PdfReader>();


                string containerName = "";

                long attachedfileslenth = 0;
                bool flag = false;
                string faileddocumentids = "";
                long f = 0;
                string docpath = "";
                try
                {

                    if (mergedoc == "1")
                    {
                        files = new List<string>();
                        foreach (string documentId in documentIdList)
                        {
                            BLL.AutoDocument document = css.AutoDocuments.Find(Convert.ToInt64(documentId));
                            //#region Local File
                            //  string filePath = Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["AutoAssignmentDocsPath"].ToString() + "" + document.AutoAssignmentId + "/" + document.DocumentTypeId + "/" + document.AutoDocumentPath);
                            #region Cloud File

                            containerName = MvcApplication.AutoAssignDocsContainerName;//ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                            string relativeFileName = CloudStorageUtility.GetAbsoluteFileURL(containerName, document.AutoAssignmentId + "/" + document.DocumentTypeId + "/" + document.AutoDocumentPath);

                            try
                            {

                                f = getDocFileSize(relativeFileName);
                                attachedfileslenth += f / 1024;
                                docpath = document.AutoDocumentPath.Substring(document.AutoDocumentPath.LastIndexOf('.') + 1);

                                if (docpath == "pdf" || docpath == "PDF")
                                {
                                    if (attachedfileslenth <= Convert.ToInt32(ConfigurationManager.AppSettings["TotalEmailSize"].ToString()))
                                    {
                                        PdfReader pdfReader = new PdfReader(FileToByteArray(relativeFileName, true));
                                        readerList.Add(pdfReader);
                                        faileddocumentids += documentId + ',';
                                    }
                                    else
                                    {
                                        flag = true;
                                        break;
                                    }

                                }
                                else
                                {
                                    if (attachedfileslenth <= Convert.ToInt32(ConfigurationManager.AppSettings["TotalEmailSize"].ToString()))
                                    {

                                        //attachmentFilePathList.Add(relativeFileName);
                                        docpath = document.AutoDocumentPath.Substring(document.AutoDocumentPath.LastIndexOf('/') + 1);
                                        ReadCloudFile(relativeFileName, docpath, ref attachmentFilePathList, ref files);
                                        faileddocumentids += documentId + ',';
                                    }
                                    else
                                    {
                                        flag = true;
                                        break;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                            }
                            #endregion
                        }
                        if (flag == true)
                        {
                            return Json(2);
                        }
                        newFile1 = Server.MapPath(ConfigurationManager.AppSettings["DocumentTempPath"].ToString() + claimid.ToString() + DateTime.Now.ToString("MMddyyymmss") + ".pdf");
                        iTextSharp.text.Document Idoc = new iTextSharp.text.Document(PageSize.A4, 0, 0, 0, 0);
                        PdfCopy pdfcopyprovider = new PdfCopy(Idoc, new FileStream(newFile1, FileMode.Create));
                        Idoc.Open();

                        foreach (PdfReader reader in readerList)
                        {
                            for (int i = 1; i <= reader.NumberOfPages; i++)
                            {
                                PdfImportedPage page = pdfcopyprovider.GetImportedPage(reader, i);

                                pdfcopyprovider.AddPage(page);
                            }
                        }
                        Idoc.Close();
                        attachmentFilePathList.Add(newFile1);

                    }
                    else
                    {
                        files = new List<string>();
                        foreach (string documentId in documentIdList)
                        {
                            BLL.AutoDocument document = css.AutoDocuments.Find(Convert.ToInt64(documentId));
                            #region Cloud File
                            containerName = MvcApplication.AutoAssignDocsContainerName;
                            string relativeFileName = CloudStorageUtility.GetAbsoluteFileURL(containerName, document.AutoAssignmentId + "/" + document.DocumentTypeId + "/" + document.AutoDocumentPath);
                            try
                            {
                                f = getDocFileSize(relativeFileName);

                                attachedfileslenth += f / 1024;
                            #endregion

                                docpath = document.AutoDocumentPath.Substring(document.AutoDocumentPath.LastIndexOf('/') + 1);
                                if (attachedfileslenth <= Convert.ToInt32(ConfigurationManager.AppSettings["TotalEmailSize"].ToString()))
                                {

                                    ReadCloudFile(relativeFileName, docpath, ref attachmentFilePathList, ref files);
                                    faileddocumentids += documentId + ',';
                                }
                                else
                                {
                                    flag = true;
                                    break;
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                            // attachmentFilePathList.Add(newFile);
                        }
                        if (flag == true)
                        {
                            return Json(2);
                        }
                    }
                    foreach (string email in tempToEmailList)
                    {
                        toEmailList.Add(email);
                    }

            
                var UserInfo = (from USR in css.Users
                                where (USR.UserId == loggedInUser.UserId)
                                select new
                                {
                                    FirstName = USR.FirstName,
                                    LastName = USR.LastName
                                }).FirstOrDefault();
                body = Regex.Replace(body, "\n", "<br>");
                string mailBody = "<br />";
                mailBody += "DO NOT REPLY TO THIS EMAIL. THIS IS A SYSTEM GENERATED EMAIL. THE EMAIL MAILBOX IS NOT MONITORED.";
                mailBody += "<br />";
                mailBody += "<br />";
                mailBody += "The attached document(s) have been emailed to you:";
                mailBody += "<br />";
                mailBody += "<br />";
                mailBody += "From: " + UserInfo.FirstName + " " + UserInfo.LastName;
                mailBody += "<br />";
                mailBody += "<br />";
                mailBody += "Note: "+ body;
                mailBody += "<br />";
                mailBody += "<br />";
                mailBody += "To review these Documents, log into <a href=\"" + ConfigurationManager.AppSettings["IadaLoginUrl"] + "\">www.IADApro.com</a><br />";
                mailBody += "<br />Thank you,";
                mailBody += "<br />";
                mailBody += "IADA Customer Support Team";
                if (attachmentFilePathList.Count > 0)
                {

                    Utility.sendEmail(toEmailList, fromEmail, replyToEmail, subject, mailBody, attachmentFilePathList);
                }








                //17-03-2015 gaurav
                // deletinf files from tempdoc
                if (System.IO.File.Exists(newFile1) && newFile1 != "")
                {

                        System.IO.File.Delete(newFile1);
                    }
                    foreach (var file in files)
                    {
                        if (System.IO.File.Exists(file))
                        {
                            System.IO.File.Delete(file);
                        }

                    }
                    ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                    css.usp_NewAutoNotesInsert(outNoteId, assignmentId, subject, emailbody, DateTime.Now, false, false, loggedInUser.UserId, null, null, null, null, toEmails, documentIds);

                    valueToReturn = 1;
                }
                catch (TimeoutException ex)
                {
                    Utility.LogException("DocEmail", ex, ex.Message);
                    bool merge = false;
                    bool replytoemail = false;
                    string attachmentlist = "";
                    string[] attachmentslist = attachmentFilePathList.ToArray();
                    if (mergedoc == "1")
                    {
                        merge = true;

                    }
                    if (setReplyToEmail == 1)
                    {
                        replytoemail = true;
                    }
                    attachmentlist = String.Join(",", attachmentslist);
                    css.FailedEmailLogInsert(toEmails, faileddocumentids.Remove(faileddocumentids.LastIndexOf(','), 1), replyToEmail, subject, body, merge, claimNumber, assignmentId, attachmentlist, loggedInUser.UserId, replytoemail);
                    if (System.IO.File.Exists(newFile1) && newFile1 != "")
                    {

                        System.IO.File.Delete(newFile1);
                    }

                    foreach (var file in files)
                    {
                        if (System.IO.File.Exists(file))
                        {
                            System.IO.File.Delete(file);
                        }

                    }

                    //  valueToReturn = -1;
                }
                catch (Exception ex)
                {
                   
                    if (System.IO.File.Exists(newFile1) && newFile1 != "")
                    {

                        System.IO.File.Delete(newFile1);
                    }
                    foreach (var file in files)
                    {
                        if (System.IO.File.Exists(file))
                        {
                            System.IO.File.Delete(file);
                        }

                    }
                    if (!String.IsNullOrEmpty(loggedInUser.Email))
                    {
                        Utility.LogException("DocEmail", ex, ex.Message);
                        Utility.sendEmail(loggedInUser.Email, fromEmail, loggedInUser.Email, "Email Sending Failed", "Email sending failed due to some error for claim " + claimNumber);

                    }
                //valueToReturn = -1;
            }
            return Json(valueToReturn + "");
        }
        [HttpPost]
        public ActionResult DeleteClaimDocument(Int64 claimid, Int64 claimDocumentId = 0)
        {
            int valueToReturn = 0;//1-Success
            try
            {
                if (claimDocumentId != 0)
                {
                    BLL.AutoDocument claimDoc = css.AutoDocuments.Find(claimDocumentId);
                    css.AutoDocumentsDelete(claimDocumentId);

                    //delete physical file 
                    string fileName = new FileInfo(claimDoc.AutoDocumentPath).Name;
                    if (MvcApplication.UseAzureCloudStorage == "1")
                    {
                        #region Cloud Delete
                        string assignmentId = claimDoc.AutoAssignmentId + "";
                        string documentTypeId = claimDoc.DocumentTypeId + "";
                        string relativeFileName = assignmentId + "/" + documentTypeId + "/" + fileName;
                        string containerName = MvcApplication.AutoAssignDocsContainerName;
                        CloudStorageUtility.DeleteFile(containerName, relativeFileName);
                        #endregion
                    }
                    else
                    {
                        #region local file system
                        string baseFolder = System.Configuration.ConfigurationManager.AppSettings["AutoAssignmentDocsPath"].ToString();
                        string assignmentId = claimDoc.AutoAssignmentId + "";
                        string documentTypeId = claimDoc.DocumentTypeId + "";
                        string filePath = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), fileName);

                        if (System.IO.File.Exists(filePath))
                        {
                            try
                            {
                                System.IO.File.Delete(filePath);
                            }
                            catch (IOException ex)
                            {
                            }
                        }
                        #endregion
                    }
                    valueToReturn = 1;
                    if (valueToReturn == 1)
                    {
                        DocumentsListViewModel Model = InitAutoDocumentModel(claimid, claimDoc.AutoAssignmentId, (short)claimDoc.DocumentTypeId, 4);
                        return Json(Model, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn);
        }
        [NonAction]
        private long getDocFileSize(string urlpath)
        {
            Stream f = null;
            MemoryStream ms = null;
            long filesize = 0;
            try
            {
                f = new WebClient().OpenRead(urlpath);
                ms = new MemoryStream();

                f.CopyTo(ms);

                filesize = ms.Length;
            }
            catch (Exception ex)
            {
                filesize = 0;
                throw;

            }
            finally
            {

                f.Dispose();
                ms.Dispose();
            }



            return filesize;

        }
        [NonAction]
        public byte[] FileToByteArray(string fileName, bool fromcloud = false)
        {
            byte[] fileContent = null;
            try
            {
                if (fromcloud)
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(fileName);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream ReceiveStream = response.GetResponseStream();
                    System.IO.BinaryReader binaryReader = new System.IO.BinaryReader(ReceiveStream);
                    long byteLength = response.ContentLength;
                    fileContent = binaryReader.ReadBytes((Int32)byteLength);
                    ReceiveStream.Close();
                    ReceiveStream.Dispose();
                    binaryReader.Close();
                }
                else
                {
                    System.IO.FileStream fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                    System.IO.BinaryReader binaryReader = new System.IO.BinaryReader(fs);
                    long byteLength = new System.IO.FileInfo(fileName).Length;
                    fileContent = binaryReader.ReadBytes((Int32)byteLength);
                    fs.Close();
                    fs.Dispose();
                    binaryReader.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return fileContent;
        }
        [NonAction]
        public void ReadCloudFile(string relativefilepath, string docpath, ref List<string> attachmentFilePathList, ref List<string> files)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(relativefilepath);

                using (var response = request.GetResponse())
                {

                    using (Stream responseStream = response.GetResponseStream())
                    {

                        string saveTo = Server.MapPath(ConfigurationManager.AppSettings["DocumentTempPath"].ToString() + docpath.ToString());

                        // create a write stream
                        FileStream writeStream = new FileStream(saveTo, FileMode.Create, FileAccess.Write);
                        // write to the stream
                        ReadWriteStream(responseStream, writeStream);
                        attachmentFilePathList.Add(saveTo);
                        files.Add(saveTo);


                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }


        }
        private static void ReadWriteStream(Stream readStream, Stream writeStream)
        {
            int Length = 25600000;
            Byte[] buffer = new Byte[Length];
            int bytesRead = readStream.Read(buffer, 0, Length);
            // FileStream outFile = null;
            // write the required bytes
            while (bytesRead > 0)
            {
                writeStream.Write(buffer, 0, bytesRead);
                bytesRead = readStream.Read(buffer, 0, Length);
            }
            readStream.Dispose();
            //  outFile = (FileStream)writeStream;
            //  writeStream.Close();
            writeStream.Dispose();

            //    return outFile;

        }
        [HttpPost]
        public ActionResult UploadClaimDocument1(IEnumerable<HttpPostedFileBase> uploadFile, Int64 ClaimDocumentType, Int64 hdnAssignmentID, string txtTitle)
        {

            //var files= Request.Files;

            AutoClaimDocumentViewModel viewModel = new AutoClaimDocumentViewModel();  //viewModel.ClaimId
            try
            {
                if (ModelState.IsValid)
                {
                    if (uploadFile != null && uploadFile.Count() > 0)
                    {
                        //if (viewModel.AutoDocument != null)
                        //{
                        //    UploadDocument(viewModel.AutoDocument, uploadFile);
                        //    //   return RedirectToAction("UploadClaimDocumentSuccess");
                        //    DocumentsListViewModel Model = InitAutoDcumentModel(viewModel.ClaimId, viewModel.AutoDocument.AutoAssignmentId, (short)viewModel.AutoDocument.DocumentTypeId, 4);
                        //    return Json(Model, JsonRequestBehavior.AllowGet);

                        //}
                        CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();     //logged user

                        Int64 autoAssignmentId = Convert.ToInt64(hdnAssignmentID);
                        Int64? ClaimID = css.AutoAssignments.Where(x => x.AutoAssignmentId == autoAssignmentId).Select(x => x.ClaimId).FirstOrDefault();



                        viewModel.AutoDocument.AutoAssignmentId = autoAssignmentId;
                        viewModel.AutoDocument.DocumentTypeId = Convert.ToByte(ClaimDocumentType);
                        viewModel.ClaimId = Convert.ToInt64(ClaimID);
                        viewModel.AutoDocument.Title = txtTitle;
                        viewModel.AutoDocument.AutoDocumentUploadedByUserId = loggedInUser.UserId;

                        if (viewModel.AutoDocument != null)
                        {
                            foreach (var uploadedFile in uploadFile)
                            {
                                UploadDocument(viewModel.AutoDocument, uploadedFile);
                            }
                            if (viewModel.AutoDocument.DocumentTypeId == 1)
                            {
                                css.UpdateNewAutoAssignmentStatus(viewModel.AutoDocument.AutoAssignmentId, 6, loggedInUser.UserId, null, 0,false);
                                AssignmentStatu assignmentStatu = css.AssignmentStatus.Find(6);
                                ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                                css.usp_NotesInsert(outNoteId, viewModel.AutoDocument.AutoAssignmentId, "Status changed to " + assignmentStatu.StatusDescription, "Status changed to " + assignmentStatu.StatusDescription, DateTime.Now, false, true, loggedInUser.UserId, null, null, null, null, null, null);

                                AutoAssignment _autoassignments = null;
                                Claim _claim = null;
                                Int64? ToUserID;
                                _autoassignments = (from Auto_Assignments in css.AutoAssignments
                                                    where Auto_Assignments.AutoAssignmentId == viewModel.AutoDocument.AutoAssignmentId
                                                    select Auto_Assignments).FirstOrDefault();
                                _claim = (from c in css.Claims
                                          where c.ClaimId == viewModel.ClaimId
                                          select c).FirstOrDefault();
                                //Inpsw -11 cooment start
                                //if (loggedInUser.UserId == _autoassignments.RepresentativeID)
                                //{
                                //    ToUserID = _autoassignments.OAUserID;
                                //}
                                //else
                                //{
                                //    ToUserID = _autoassignments.RepresentativeID;
                                //}
                                //Inpsw-11 Comment end
                                List<Int64> ID = new List<Int64>();
                                if (loggedInUser.UserTypeId == 3 || loggedInUser.UserTypeId == 12)
                                {
                                    ID.Add((Int64)_autoassignments.OAUserID);
                                    // ToUserID = _autoassignments.OAUserID;
                                }
                                else if (loggedInUser.UserTypeId == 2)
                                {
                                    ID.Add((Int64)_autoassignments.OAUserID);
                                    ID.Add((Int64)_autoassignments.RepresentativeID);
                                }
                                else
                                {
                                    //ToUserID = _autoassignments.RepresentativeID;
                                    ID.Add((Int64)_autoassignments.RepresentativeID);
                                }


                                var UserInfo = (from USR in css.Users
                                                where (USR.UserId == loggedInUser.UserId)
                                                select new
                                                {
                                                    FirstName = USR.FirstName,
                                                    LastName = USR.LastName
                                                }).FirstOrDefault();
                                for (int i = 0; i < ID.Count(); i++)
                                {
                                    #region Status changed notification email


                                    try
                                    {
                                        String DocumentList = "<ul>";
                                        foreach (var uploadedFile in uploadFile)
                                        {
                                            DocumentList = DocumentList + "<li>" + uploadedFile.FileName + "</li>";
                                        }
                                        DocumentList = DocumentList + "</ul>"; 

                                        List<string> toEmails = new List<string>();
                                        var userObj = getUserSet(ID[i]);
                                        if (userObj != null)
                                        {
                                            toEmails.Add(userObj.Email);
                                        }
                                        if (toEmails.Count > 0)
                                        {
                                            string mailBody = "<br />";
                                            mailBody += "DO NOT REPLY TO THIS EMAIL.THIS IS A SYSTEM GENERATED EMAIL.THE EMAIL MAILBOX IS NOT MONITORED.";
                                            mailBody += "<br />";
                                            mailBody += "<br />";
                                            //mailBody += "Dear " + userObj.FirstName + " " + userObj.LastName;
                                            mailBody += "The following document(s) has been added to:<br /><br />";
                                            mailBody += "Claim Number: " + _claim.ClaimNumber;
                                            mailBody += "<br />From: " + UserInfo.FirstName + " " + UserInfo.LastName;
                                            mailBody += "<br />";
                                            mailBody += "<br />";
                                            mailBody += "Documents: " + DocumentList;
                                            mailBody += "<br />";
                                            mailBody += "To review these Documents, log into <a href=\"" + ConfigurationManager.AppSettings["IadaLoginUrl"] + "\">www.IADApro.com</a><br />";
                                            mailBody += "<br />Thank you,";
                                            mailBody += "<br />";
                                            mailBody += "IADA Customer Support Team";
                                            Utility.sendEmail(toEmails, MvcApplication.AdminMail, "Claim #: " + _claim.ClaimNumber + " - Assignment Status Change Notification", mailBody);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Utility.LogException("method uploaddocuments ", ex, "An Error occured while sending email notification:" + viewModel.AutoAssignmentId);
                                    }
                                    try
                                    {
                                        List<string> toEmails = new List<string>();
                                        var userObj = getUserSet(ID[i]);
                                        if (userObj != null)
                                        {
                                            toEmails.Add(userObj.Email);
                                        }
                                        if (toEmails.Count > 0)
                                        {
                                            string mailBody = "Dear " + userObj.FirstName + " " + userObj.LastName;
                                            mailBody += "<br />";
                                            mailBody += "<br /> The status of claim " + _claim.ClaimNumber + " has changed to " + assignmentStatu.StatusDescription + ". If a Note was entered at the time of Status Change it has been provided below.Log into <a href=\"" + ConfigurationManager.AppSettings["IadaLoginUrl"] + "\">www.IADApro.com</a> to view the claim.";
                                            mailBody += "<br />";
                                            mailBody += "<br />";
                                            mailBody += "<br />Thank you,";
                                            mailBody += "<br />";
                                            mailBody += "IADA Customer Support Team";
                                            Utility.sendEmail(toEmails, MvcApplication.AdminMail, "Claim # " + _claim.ClaimNumber + " Assignment Status Change Notification", mailBody);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Utility.LogException("Method uploaddocuments ", ex, "An Error occured while sending email notification:" + viewModel.AutoAssignmentId);
                                    }

                                    #endregion
                                }
                            };
                            //   return RedirectToAction("UploadClaimDocumentSuccess");
                            DocumentsListViewModel Model = InitAutoDocumentModel(viewModel.ClaimId, viewModel.AutoDocument.AutoAssignmentId, (short)viewModel.AutoDocument.DocumentTypeId, 4);
                            return Json("1", JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return Json("0", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult BulkAssignInternalContact(string AutoAssignmentIds, Int64 InternalContactID)
        {
            string valueToReturn = "0";
            try
            {
                foreach (string assignmentId in AutoAssignmentIds.Split(','))
                {
                    css.AutoAssignCSSInternalContact(Convert.ToInt64(assignmentId), InternalContactID);
                }
                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UserDetails(Int32 UserID)
        {
            return PartialView("_UserDetails", css.GetUserDetails(UserID).ToList());
        }


        public ActionResult GetInternalContactList(int companyId)
        {
            List<SelectListItem> ClientPointofContactList = GetClientContactDDL(companyId);
            return Json(ClientPointofContactList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetLOBList(Int32? companyId)
        {
            List<SelectListItem> LineOfBussinessList = GetLOBDDL(companyId);
            return Json(LineOfBussinessList, JsonRequestBehavior.AllowGet);
        }

        private List<SelectListItem> InsuranceCompanies(int UserTypeId, int? CompanyId)
        {
            List<SelectListItem> InsuranceCompaniesList = new List<SelectListItem>();
            if (UserTypeId != 3 && UserTypeId != 12) //12 is ClaimRepManager  
            {
                InsuranceCompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            }
            if (UserTypeId != null)
            {
                List<CompaniesGetList_Result> CompanyList = css.CompaniesGetList(UserTypeId, CompanyId, 1).ToList();
                foreach (var Company in CompanyList)
                {
                    InsuranceCompaniesList.Add(new SelectListItem { Text = Company.CompanyName, Value = Company.CompanyId + "" });
                }
            }
            return InsuranceCompaniesList;
        }
        private List<SelectListItem> GetClientContactDDL(int? companyId)
        {
            List<SelectListItem> ClientPointofContactList = new List<SelectListItem>();
            ClientPointofContactList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            if (companyId.HasValue)
            {
                if (companyId != 0)
                {

                    List<GetClientPointOfContactList_Result> clientPOCList = css.GetClientPointOfContactList(companyId.Value).ToList();
                    foreach (var client in clientPOCList)
                    {
                        ClientPointofContactList.Add(new SelectListItem { Text = client.UserFullName, Value = client.UserId + "" });
                    }
                }

            }
            return ClientPointofContactList;
        }

        //onchange Insurance Company
        [HttpPost]
        public ActionResult GetClaimRepresentativeList(int? CompanyId)
        {
            int UserTypeId = 2; //2 is admin 
            List<SelectListItem> ClaimRepresentativeList = new List<SelectListItem>();
            //ClaimRepresentativeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            if (CompanyId != null)
            {
                List<ClaimRepresentativeGetList_Result> ClaimRepreList = css.ClaimRepresentativeGetList(UserTypeId, CompanyId).ToList();
                foreach (var ClaimRepresentative in ClaimRepreList)
                {
                    ClaimRepresentativeList.Add(new SelectListItem { Text = ClaimRepresentative.UserFullName, Value = ClaimRepresentative.UserId + "" });
                }
                if (ClaimRepreList.Count == 0)
                {
                    ClaimRepresentativeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                }
            }
            return Json(ClaimRepresentativeList);
        }

        //onchange Insurance Company
        [HttpPost]
        public ActionResult GetServiceOfferingsList(int? CompanyId, int? ServiceTypeId)
        {
            
            List<SelectListItem> ClaimServiceList = new List<SelectListItem>();
            if (CompanyId != null)
            {           
                List<GetServiceOfferingsList_Result> ClaimServList = css.GetServiceOfferingsList(CompanyId,ServiceTypeId).ToList();
                foreach (var Services in ClaimServList)
                {
                    ClaimServiceList.Add(new SelectListItem { Text = Services.ServiceDescription, Value = Services.ServiceId + "" });
                }
                if (ClaimServList.Count == 0)
                {
                    ClaimServiceList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                }
            }
            return Json(ClaimServiceList);
        }

        private List<SelectListItem> InternalContactList(int? CompanyId)
        {
            List<SelectListItem> InternalContactList = new List<SelectListItem>();
            //ClaimRepresentativeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            if (CompanyId != null && CompanyId != 0)
            {
                List<InternalContactList_Result> InternalConList = css.InternalContactList(CompanyId).ToList();
                foreach (var InternalContact in InternalConList)
                {
                    InternalContactList.Add(new SelectListItem { Text = InternalContact.UserFullName, Value = InternalContact.UserId + "" });
                }
            }
            return InternalContactList;
        }
        private List<SelectListItem> ClaimRepresentativeList(int UserId, int? CompanyId)
        {
            List<SelectListItem> ClaimRepresentativeList = new List<SelectListItem>();
            //ClaimRepresentativeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            if (CompanyId != null && CompanyId != 0)
            {
                List<ClaimRepresentativeGetList_Result> ClaimRepreList = css.ClaimRepresentativeGetList(UserId, CompanyId).ToList();
                foreach (var ClaimRepresentative in ClaimRepreList)
                {
                    ClaimRepresentativeList.Add(new SelectListItem { Text = ClaimRepresentative.UserFullName, Value = ClaimRepresentative.UserId + "" });
                }
                
            }
            return ClaimRepresentativeList;
        }
        private List<SelectListItem> GetLOBDDL(int? companyId)
        {
            List<SelectListItem> LineOfBusinessList = new List<SelectListItem>();
            LineOfBusinessList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            if (companyId.HasValue)
            {
                if (companyId != 0)
                {
                    List<LineOfBusinessGetList_Result> LObList = css.LineOfBusinessGetList(companyId.Value).ToList();
                    foreach (var lob in LObList)
                    {
                        LineOfBusinessList.Add(new SelectListItem { Text = lob.LOBDescription, Value = lob.LOBId + "" });
                    }
                }
            }
            return LineOfBusinessList;
        }

        #endregion

        public ActionResult AutoNotesGetList(Int64? ClaimID, int displayType = 0)
        {
            XACTAutoClaimsInfoViewModel viewmodel = new BLL.ViewModels.XACTAutoClaimsInfoViewModel();
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            if (displayType == 1)
            {
                viewmodel.note.IsSystemGenerated = false;
            }
            else
            {
                viewmodel.note.IsSystemGenerated = false;
            }

            viewmodel.AutoNoteHistory = css.AutoNotesHistoryGetList(ClaimID, null).ToList();

            return PartialView("_AutoNotes", viewmodel);
        }


        public ActionResult SetupAutoSPPayPercentage(Int64 autoassignmentId, byte spPayPercentage, byte holdbackPercentage)
        {

            string valueToReturn = "0";
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                NewAssignmentController controller = new NewAssignmentController();
                controller.ControllerContext = this.ControllerContext;
                Int64? spUserId = css.AutoAssignments.Find(autoassignmentId).OAUserID;
                controller.SetupAutoSPPayPercentage(autoassignmentId, spUserId, spPayPercentage, holdbackPercentage);
                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + "" + ex.InnerException != null ? "  " + ex.InnerException.Message + "" : "";
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AssignmentStatusUpdate(Int64 assignmentId, Int64 claimId, short statusId, string statusChangeNote, bool retainSP, int SPRank, bool RejectionMark, string InspectionTime, string InspectionDate, bool ISSprejected = false)//INPSW-37 (Add new parameter ISSprejected)
        {
            try
            {
                string Note = "";
                string MailNote = "";

                if (InspectionTime != "" && InspectionTime!= null)
                {
                    DateTime timeValue = Convert.ToDateTime(InspectionTime);
                    InspectionTime = timeValue.ToString("hh:mm tt");
                    Note = "Inspection Time: " + InspectionTime + "<br />";
                }
                if (InspectionDate != "" && InspectionDate != null)
                {
                    Note += "Inspection Date: " + InspectionDate + "<br />";
                }
                if(statusChangeNote !="" && statusChangeNote !=null)
                {
                    MailNote = Note + "Note:  " + statusChangeNote;
                    Note+= statusChangeNote;
                }
                if(!String.IsNullOrEmpty(InspectionTime) || !String.IsNullOrEmpty(InspectionDate)) 
                {
                    AutoAssignment _AutoAssignment = null;
                    _AutoAssignment = (
                                                    from a in css.AutoAssignments
                                                    where a.AutoAssignmentId == assignmentId
                                                    select a
                                                ).FirstOrDefault();

                    if (InspectionTime != "" && InspectionTime != null)
                    {
                        _AutoAssignment.SpInspectionTime = Convert.ToDateTime(InspectionTime);
                    }
                    if (InspectionDate != "" && InspectionDate != null)
                    {
                        _AutoAssignment.SpInspectionDate = Convert.ToDateTime(InspectionDate);
                    }
                    css.SaveChanges();
                }
                

                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                Int64 spid = 0;
                ClaimCycleTimeDetail c = null;
                if (SPRank != 0)
                {

                    spid = css.AutoAssignments.Single(x => x.AutoAssignmentId == assignmentId && x.ClaimId == claimId).OAUserID.Value;
                    c = css.ClaimCycleTimeDetails.Single(x => x.SPId == spid && x.ClaimId == claimId);
                }

                if (statusId == 7)
                {
                    css.usp_GenerateSPRank(spid, claimId, Note, SPRank, null, statusId);//statusChangeNote replace with Note parameter
                }
               
                if (statusId == 8)
                {
                    css.usp_GenerateSPRank(spid, claimId, null, null, RejectionMark, statusId);
                }

                css.UpdateNewAutoAssignmentStatus(assignmentId, statusId, loggedInUser.UserId, null, 0, ISSprejected);

                //Add Note Entry
                AssignmentStatu assignmentStatu = css.AssignmentStatus.Find(statusId);
                ObjectParameter outNoteId = new ObjectParameter("AutoNoteId", DbType.Int32);
                if (statusId != 7)
                {
                    var autoassignment = css.AutoAssignments.Where(x => x.AutoAssignmentId == assignmentId).FirstOrDefault();
                    Int64? ToUserID;
                    //if (assignmentStatu.StatusDescription == "Estimate Hold" || assignmentStatu.StatusDescription == "Approved" || assignmentStatu.StatusDescription == "UnApproved")
                    //{
                    //    ToUserID = autoassignment.OAUserID;
                    //}
                    //else if (assignmentStatu.StatusDescription == "Accepted")
                    //{
                    //    ToUserID = autoassignment.RepresentativeID;
                    //}
                    //else if (assignmentStatu.StatusDescription == "Returned")
                    //{
                    //    if(loggedInUser.UserId == autoassignment.RepresentativeID)
                    //    {
                    //        ToUserID = autoassignment.OAUserID;
                    //    }
                    //    else
                    //    {
                    //        ToUserID = autoassignment.RepresentativeID;
                    //    }
                    //}
                    //else
                    //{
                    //    ToUserID = null;
                    //}
                    if (loggedInUser.UserId == autoassignment.RepresentativeID)
                    {
                        ToUserID = autoassignment.OAUserID;
                    }
                    else
                    {
                        ToUserID = autoassignment.RepresentativeID;
                    }
                    //else if (assignmentStatu.StatusDescription == "Contacted" || assignmentStatu.StatusDescription == "Inspected" || assignmentStatu.StatusDescription == "Returned")
                    //{
                    //    ToUserID = null;
                    //}
                    string NoteSubject = "Status changed to " + assignmentStatu.StatusDescription;
                    if(ISSprejected == true)
                    {
                        NoteSubject = "Service Provider Rejected Assignment";
                    }
                    css.usp_AutoNotesInsert(outNoteId, assignmentId, NoteSubject, Note, DateTime.Now, false, true, loggedInUser.UserId, ToUserID, null, null, null, null, null);//statusChangeNote replace with Note parameter

                    #region Status changed notification email

                    List<string> toEmails = new List<string>();
                    var SpInfo = css.Users.Find(loggedInUser.UserId);
                    string SpName = "";
                    if (SpInfo != null)
                    {
                        SpName = SpInfo.FirstName + " " + SpInfo.LastName;
                    }

                    var userObj = getUserSet(ToUserID);
                    if (userObj != null)
                    {
                        toEmails.Add(userObj.Email);
                    }

                    if (ISSprejected == true)
                    {

                        try
                        {
                            //List<string> toEmails = new List<string>();

                            //autoassignment.Claim.InsuredEmail

                            //if (autoassignment.User != null && !string.IsNullOrEmpty(autoassignment.User.Email))
                            //{
                            //    toEmails.Add(autoassignment.User.Email);

                            //}
                            //var userObj = css.Users.Where(x => x.UserId == ToUserID).
                            //  Select(x => new { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName }).FirstOrDefault();

                            //var userObj = getUserSet(ToUserID);
                            //if (userObj != null)
                            //{
                            //    toEmails.Add(userObj.Email);
                            //}
                            if (toEmails.Count > 0)
                            {
                                string mailBody = "<br/>";
                                mailBody += "DO NOT REPLY TO THIS EMAIL. THIS IS A SYSTEM GENERATED EMAIL. THE EMAIL MAILBOX IS NOT MONITORED.";
                                mailBody += "<br />";
                                mailBody += "<br />";
                                mailBody += "The following claim assignment was rejected by the selected service provider.";
                                mailBody += "<br />";
                                mailBody += "<br />";
                                mailBody += "Please log in to <a href=\"" + ConfigurationManager.AppSettings["IadaLoginUrl"] + "\">www.IADApro.com</a> immediately and assign the claim to a different service provider.";
                                mailBody += "<br />";
                                mailBody += "<br />";
                                mailBody += "Claim Number: "+ autoassignment.Claim.ClaimNumber;
                                mailBody += "<br />";
                                mailBody += "Assignment Rejected by: " + SpName;
                                mailBody += "<br />";
                                mailBody += "<br />";
                                mailBody += "Thank you,";
                                mailBody += "<br />";
                                mailBody += "IADA Customer Support Team";
                                Utility.sendEmail(toEmails, MvcApplication.AdminMail, "Claim #: " + autoassignment.Claim.ClaimNumber + " - Claim Assignment Rejected", mailBody);
                            }
                        }
                        catch (Exception ex)
                        {
                            Utility.LogException("method AssignmentStatusUpdate ", ex, "An Error occured while sending email notification:" + assignmentId);
                        }
                    }
                    else
                    {
                        try
                        {
                            //List<string> toEmails = new List<string>();
                            //autoassignment.Claim.InsuredEmail

                            //if (autoassignment.User != null && !string.IsNullOrEmpty(autoassignment.User.Email))
                            //{
                            //    toEmails.Add(autoassignment.User.Email);

                            //}
                            //var userObj = css.Users.Where(x => x.UserId == ToUserID).
                            //  Select(x => new { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName }).FirstOrDefault();

                            //var userObj = getUserSet(ToUserID);
                            //if (userObj != null)
                            //{
                            //    toEmails.Add(userObj.Email);
                            //}
                            if (toEmails.Count > 0)
                            {
                                string mailBody = "<br/>";
                                mailBody += "DO NOT REPLY TO THIS EMAIL. THIS IS A SYSTEM GENERATED EMAIL. THE EMAIL MAILBOX IS NOT MONITORED.";
                                mailBody += "<br />";
                                mailBody += "<br />";
                                mailBody += "Dear " + userObj.FirstName + " " + userObj.LastName + ",";
                                mailBody += "<br />";
                                mailBody += "<br /> The status of claim " + autoassignment.Claim.ClaimNumber + " has changed to " + assignmentStatu.StatusDescription + ". If a Note was entered at the time of Status Change it has been provided below. Log into <a href=\"" + ConfigurationManager.AppSettings["IadaLoginUrl"] + "\">www.IADApro.com</a> to view the claim.";
                                mailBody += "<br />";
                                mailBody += "<br />";
                                mailBody += "<br />" + MailNote;
                                mailBody += "<br />";
                                mailBody += "<br />";
                                mailBody += "<br />";
                                mailBody += "Thank you,";
                                mailBody += "<br />";
                                mailBody += "IADA Customer Support Team";
                                Utility.sendEmail(toEmails, MvcApplication.AdminMail, "Claim #: " + autoassignment.Claim.ClaimNumber + " - Assignment Status Change Notification", mailBody);
                            }
                        }
                        catch (Exception ex)
                        {
                            Utility.LogException("method AssignmentStatusUpdate ", ex, "An Error occured while sending email notification:" + assignmentId);
                        }
                    }
                    
                    #endregion
//INPSW-31
                    #region Send mail When status = unapproved
                    try
                    {
                        if (statusId == 8)
                        {
                            var assignment = css.AutoAssignments.Where(x => x.AutoAssignmentId == assignmentId).FirstOrDefault();
                            var SpDetail = css.Users.Where(x => x.UserId == assignment.OAUserID).FirstOrDefault();

                            List<string> toEmailsList = new List<string>();
                            toEmailsList.Add("support@iada.org");
                            if (SpDetail.Email != null)
                            {
                                toEmailsList.Add(SpDetail.Email);
                            }

                            string mailBody = "<br/>";
                            mailBody += "DO NOT REPLY TO THIS EMAIL. THIS IS A SYSTEM GENERATED EMAIL. THE EMAIL MAILBOX IS NOT MONITORED.";
                            mailBody += "<br />";
                            mailBody += "<br />";
                            mailBody += "Please log in to <a href=\"" + ConfigurationManager.AppSettings["IadaLoginUrl"] + "\">www.IADApro.com</a>, " + autoassignment.Claim.ClaimNumber + " requires your attention.";
                            mailBody += "<br />";
                            mailBody += "<br />Thank you,";
                            mailBody += "<br />";
                            mailBody += "IADA Customer Support Team";
                            Utility.sendEmail(toEmailsList, "", "Claim #: " + autoassignment.Claim.ClaimNumber + " Unapproved Notification", mailBody);
                            AutoAssignment _AutoAssignment = null;
                            _AutoAssignment = (
                            from AA in css.AutoAssignments
                            where AA.AutoAssignmentId == assignmentId
                            select AA
                            ).FirstOrDefault();
                            _AutoAssignment.IsNotificationEmail = true;
                            css.SaveChanges();
                        }
                    }

                    catch (Exception ex)
                    {
                        Utility.LogException("method AssignmentStatusUpdate:UnapprovedMail ", ex, "An Error occured while sending email notification:" + assignmentId);
                    }
                    #endregion

                } //commented this bracket for removing the errorby shubham
                if (retainSP && statusId == 11)//Consider Retain SP only if the claim is being reopened
                {
                    //When a reopen is performed a new PropertyAssignment record is created without the SP
                    Int64? existingSPUserId = css.AutoAssignments.Find(assignmentId).OAUserID;

                    //Find the most recent PropertyAssignment for the claim which got created after a reopen
                    Int64 newAssignmentId = css.AutoAssignments.Where(x => x.ClaimId == claimId).OrderByDescending(x => x.AutoAssignmentId).First().AutoAssignmentId;
                    if (existingSPUserId.HasValue)
                    {
                        css.AssignServiceProviderToAssignment(newAssignmentId, existingSPUserId, loggedInUser.UserId);
                    }
                }
                //INPSW - 27 Start
                if (retainSP ==false && statusId == 11)//Consider Retain SP only if the claim is being reopened with 
                {

                    AutoAssignment _Autoassignment = (
                                                       from AA in css.AutoAssignments
                                                       where AA.AutoAssignmentId == assignmentId
                                                       select AA
                                                   ).FirstOrDefault();
                    _Autoassignment.OAUserID = null;
                    _Autoassignment.SPPayPercentUpdatedOn = null;
                    _Autoassignment.DateAssigned = null;
                    _Autoassignment.UpdatedOn = null;
                    css.SaveChanges();
                }

                
                //}svp
                
                //IPNsw- 27 End

                #region Invoice pdf Creation
                if (statusId == 19)
                {
                    GenInvoicePDF(assignmentId, loggedInUser);
                }
                #endregion

            }
            catch (Exception ex)
            {
            }
            //return RedirectToAction("Details", new { claimId = claimId });
            return Json(1);
        }

        public void GenInvoicePDF(Int64 nugenassignmentId, CSS.Models.User loggedInUser)
        {
            var assignment = css.AutoAssignments.Where(x => x.AutoAssignmentId == nugenassignmentId).FirstOrDefault();
            
            if (assignment != null && assignment.FileStatus == 19 && assignment.NuGenGUId.HasValue)
            {
                #region Export to Pdf
                string originalFileName = "Invoice.pdf";
                int revisionCount = css.AutoDocuments.Where(x => (x.AutoAssignmentId == assignment.AutoAssignmentId) && (x.DocumentTypeId == 8) && (x.OriginalFileName == originalFileName)).ToList().Count;

                string thisPageURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
                thisPageURL = thisPageURL.Replace("SubmitPropertyInvoice", "GeneratePdfInvoice");
                NuGenInvoiceViewModel ViewModel = new NuGenInvoiceViewModel();
                ViewModel.invoiceDetail = css.usp_AutoInvoiceGetDetails(assignment.AutoAssignmentId, -1).FirstOrDefault();
                ViewModel.TotalAmountReceived = css.usp_PropertyInvoiceTotalSum(ViewModel.invoiceDetail.AutoInvoiceId).FirstOrDefault();
            //    ViewModel.viewType = "READ";
                ObjectParameter outDocumentid = new ObjectParameter("Documentid", DbType.Int64);

                string str = RenderViewToString("NugenGeneratePdfInvoice", ViewModel);

                byte[] pdfarray = Utility.ConvertHTMLStringToPDFwithImages(str, thisPageURL);
                string fileName = string.Empty;
                if (MvcApplication.UseAzureCloudStorage == "1")
                {
                    #region Cloud Storage

                    string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                    string assignmentId = ViewModel.invoiceDetail.AutoInvoiceId + "";
                    string documentTypeId = 8 + "";

                    fileName = "Invoice" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
                    string relativeFileName = assignmentId + "/" + documentTypeId + "/" + fileName;

                    string containerName = MvcApplication.AutoAssignDocsContainerName;
                    CloudStorageUtility.StoreFile(containerName, relativeFileName, pdfarray);

                    #endregion
                }
                else
                {
                    #region Local File System

                    string baseFolder = System.Configuration.ConfigurationManager.AppSettings["AutoAssignmentDocsPath"].ToString();
                    string assignmentId = ViewModel.invoiceDetail.AutoAssignmentId + "";
                    string documentTypeId = 8 + "";


                    fileName = "Invoice" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
                    if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId)))
                    {
                        Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId));
                    }
                    string path = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), fileName);

                    Utility.StoreBytesAsFile(pdfarray, path);
                    #endregion

                }

                //Generation of a monthly T&E Invoice created an incorrect note which would read as Revised Invoice. Make use of a generalised note
                css.AutoDocumentsInsert(outDocumentid, ViewModel.invoiceDetail.AutoAssignmentId, DateTime.Now, "Invoice", originalFileName, fileName, loggedInUser.UserId, 8);
         #endregion
            }
        }

        public void GenInvoicePDFAutoClaims(Int64 AutoassignmentId, Int64 AutoInvoiceId, CSS.Models.User loggedInUser, bool IsQBInvoice)
        {
            var assignment = css.AutoAssignments.Where(x => x.AutoAssignmentId == AutoassignmentId).FirstOrDefault();
            if (assignment != null)
            {
                #region Export to Pdf
                string originalFileName = "Invoice.pdf";
                int revisionCount = css.AutoDocuments.Where(x => (x.AutoAssignmentId == assignment.AutoAssignmentId) && (x.DocumentTypeId == 8) && (x.OriginalFileName == originalFileName)).ToList().Count;
                string thisPageURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
                thisPageURL = thisPageURL.Replace("SubmitPropertyInvoice", "GeneratePdfInvoice");
                NuGenInvoiceViewModel ViewModel = new NuGenInvoiceViewModel();
                ViewModel.invoiceDetail = css.usp_AutoInvoiceGetDetails(assignment.AutoAssignmentId, AutoInvoiceId).FirstOrDefault();
                ViewModel.TotalAmountReceived = css.usp_PropertyInvoiceTotalSum(AutoInvoiceId).FirstOrDefault();
                ViewModel.IsQBInvoice = false;
                //    ViewModel.viewType = "READ";
                ObjectParameter outDocumentid = new ObjectParameter("Documentid", DbType.Int64);

                string str = RenderViewToString("NugenGeneratePdfInvoice", ViewModel);

                byte[] pdfarray = Utility.ConvertHTMLStringToPDFwithImages(str, thisPageURL);
                string fileName = string.Empty;
                if (MvcApplication.UseAzureCloudStorage == "1")
                {
                    #region Cloud Storage

                    string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                    string assignmentId;
                    if(IsQBInvoice == true)
                    {
                         assignmentId = ViewModel.invoiceDetail.AutoInvoiceId + "";
                    }
                    else
                    {
                        assignmentId = ViewModel.invoiceDetail.AutoAssignmentId + "";
                    }
                    
                    string documentTypeId = 8 + "";

                    fileName = "Invoice" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
                    string relativeFileName = assignmentId + "/" + documentTypeId + "/" + fileName;

                    string containerName = MvcApplication.AutoAssignDocsContainerName;
                    CloudStorageUtility.StoreFile(containerName, relativeFileName, pdfarray);

                    #endregion
                }
                else
                {
                    #region Local File System

                    string baseFolder = System.Configuration.ConfigurationManager.AppSettings["AutoAssignmentDocsPath"].ToString();
                   // string assignmentId = ViewModel.invoiceDetail.AutoAssignmentId + "";
                    string assignmentId;
                    if (IsQBInvoice == true)
                    {
                        assignmentId = ViewModel.invoiceDetail.AutoInvoiceId + "";
                    }
                    else
                    {
                        assignmentId = ViewModel.invoiceDetail.AutoAssignmentId + "";
                    }
                    string documentTypeId = 8 + "";


                    fileName = "Invoice" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
                    if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId)))
                    {
                        Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId));
                    }
                    string path = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), fileName);

                    Utility.StoreBytesAsFile(pdfarray, path);
                    #endregion

                }

                //Generation of a monthly T&E Invoice created an incorrect note which would read as Revised Invoice. Make use of a generalised note
                css.AutoDocumentsInsert(outDocumentid, ViewModel.invoiceDetail.AutoAssignmentId, DateTime.Now, "Invoice", originalFileName, fileName, loggedInUser.UserId, 8);
                #endregion
            }
        }
        public JsonResult UpdateAssignmentStatuspopup(Int64 claimId)
        {

            XACTAutoClaimsInfoViewModel viewmodel = new XACTAutoClaimsInfoViewModel(claimId);

            return Json(RenderPartialViewToString("_UpdateAssignmentStatuspopup", viewmodel), JsonRequestBehavior.AllowGet);
        }


        public ActionResult InvoiceDocumentViewer(Int64 assignmentID)
        {
            AutoAssignment assignment = css.AutoAssignments.Find(assignmentID);
            DocumentsListViewModel viewModel = new DocumentsListViewModel();
            if (assignment != null)
            {
                viewModel = InitAutoDocumentModel((Int64)assignment.ClaimId, assignment.AutoAssignmentId);
                var docpath = viewModel.DocumentsList.Where(x => x.DocumentTypeId == 8).OrderByDescending(x => x.AutoDocumentId).FirstOrDefault();
                if (docpath != null)
                {
                    viewModel.FirstDocument = FirstDoc(docpath.AutoDocumentId);
                }
            }
            return Json(RenderPartialViewToString("_InvoiceView", viewModel), JsonRequestBehavior.AllowGet);
            // return PartialView("_AutoClaimDocuments", viewmodel);
        }
        public ActionResult GetFirstInvoiceDocument(Int64 Assignmentid)
        {
            string Fileurl = "", assignmentId="";
           

            string baseFolder = MvcApplication.AutoAssignmentDocsPath;
            try
            {
                BLL.AutoDocument document = css.AutoDocuments.Where(x => x.AutoAssignmentId == Assignmentid && x.DocumentTypeId == 8).OrderByDescending(x => x.AutoDocumentId).FirstOrDefault();
                if (document != null)
                {

                    bool IsNugen =Convert.ToBoolean((from c in css.Claims
                                    where c.ClaimId == ((from ass in css.AutoAssignments
                                                         where ass.AutoAssignmentId == Assignmentid
                                                         select ass.ClaimId).FirstOrDefault())
                                    select c.IsNugeAssignment).FirstOrDefault());
                                  
                    //string assignmentId = document.AutoAssignmentId + "";
                    if (document.Title.ToLower() == "service invoice" || IsNugen==false)
                    {
                        assignmentId = Assignmentid.ToString();
                    }
                    else
                    {
                        assignmentId = ((
                                               from a in css.AutoInvoices
                                               where a.AutoAssignmentId == Assignmentid
                                               select a.AutoInvoiceId
                                             ).FirstOrDefault()).ToString();
                    }
                    string documentTypeId = document.DocumentTypeId + "";
                    //check whether the folder with user's userid exists
                    string filename = document.AutoDocumentPath;
                    if (MvcApplication.UseAzureCloudStorage == "1")
                    {
                        string containerName = MvcApplication.AutoAssignDocsContainerName;//ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                        // Fileurl = CloudStorageUtility.GetAbsoluteFileURL(containerName, document.AutoAssignmentId + "/" + document.DocumentTypeId + "/" + document.AutoDocumentPath);
                        return File(CloudStorageUtility.GetByteStramFromAbsoluteFileURL(containerName, assignmentId + "/" + document.DocumentTypeId + "/" + document.AutoDocumentPath), Utility.GetContentType(filename));
                    }
                    else
                    {
                        //  Fileurl = Path.Combine((baseFolder + "" + assignmentId + "/" + documentTypeId), document.AutoDocumentPath);
                        // check whether the destination folder depending on the type of document being uploaded exists
                        if (System.IO.File.Exists(Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), document.AutoDocumentPath)))
                        {
                            Fileurl = Path.Combine((VirtualPathUtility.ToAbsolute(baseFolder) + "" + assignmentId + "/" + documentTypeId), document.AutoDocumentPath);
                        }
                        else
                        {
                            Fileurl = Path.Combine(VirtualPathUtility.ToAbsolute(baseFolder), "NotFound.txt");
                        }
                        return File(Fileurl, "application/pdf");
                    }
                }

            }
            catch (Exception ex)
            {

            }
            return File(Path.Combine(VirtualPathUtility.ToAbsolute(MvcApplication.AutoAssignmentDocsPath), MvcApplication.NotFoundFileName), "text/plain");
        }
        public ActionResult multiDocUploader(Int64 assignmentID)
        {
            return PartialView("_MultiDocUploader");
        }
        public ActionResult InitmultiDocUploader(Int64 assignmentID)
        {
            MultiFileUploadViewModel viewModel = new MultiFileUploadViewModel();
            viewModel.AssignmentID = assignmentID;
            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DocumentViewer(Int64 assignmentID)
        {
            AutoAssignment assignment = css.AutoAssignments.Find(assignmentID);
            DocumentsListViewModel viewModel = new DocumentsListViewModel();
            if (assignment != null)
            {
                viewModel = InitAutoDocumentModel((Int64)assignment.ClaimId, assignment.AutoAssignmentId);
                DocumentsList_Result docpath = null;
                usp_DocumentsPreviousGetList_Result PdocResult = null;
                // if (viewModel.DocumentsList.Count > 0)
                // {
                //   docpath = viewModel.DocumentsList.FirstOrDefault();
                // }
                // else if(viewModel.DocumentsPreviousList.Count>0)
                // {
                //     PdocResult = viewModel.DocumentsPreviousList.FirstOrDefault();
                // }
                // if (docpath != null || PdocResult!=null)
                // {
                ////    viewModel.FirstDocument = FirstDoc(docpath.AutoDocumentId);   
                //     viewModel.FirstFilebyte = GetFirstDocumentByteArray(docpath.AutoDocumentId);
                // }
            }
            return Json(RenderPartialViewToString("_AutoClaimDocumentViewer", viewModel), JsonRequestBehavior.AllowGet);
            // return PartialView("_AutoClaimDocuments", viewmodel);
        }
        public byte[] GetFirstDocumentByteArray(Int64 DocID)
        {
            string Fileurl = "";

            try
            {
                BLL.AutoDocument document = css.AutoDocuments.Find(DocID);
                string baseFolder = System.Configuration.ConfigurationManager.AppSettings["AutoAssignmentDocsPath"].ToString();
                string assignmentId = document.AutoAssignmentId + "";
                string documentTypeId = document.DocumentTypeId + "";
                //check whether the folder with user's userid exists
                string filename = document.AutoDocumentPath;
                if (MvcApplication.UseAzureCloudStorage == "1")
                {
                    string containerName = MvcApplication.AutoAssignDocsContainerName;//ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                    // Fileurl = CloudStorageUtility.GetAbsoluteFileURL(containerName, document.AutoAssignmentId + "/" + document.DocumentTypeId + "/" + document.AutoDocumentPath);
                    return CloudStorageUtility.GetByteStramFromAbsoluteFileURL(containerName, document.AutoAssignmentId + "/" + document.DocumentTypeId + "/" + document.AutoDocumentPath);
                }

            }
            catch (Exception ex)
            {

            }
            return null;
        }
        public ActionResult PrintAssignment(Int64 AssignmentClaimID, Int32 ServiceType)
        {
            string ReportPath = String.Empty;
            //      Response.AddHeader("content-disposition", "attachment; filename=ClaimInfo." + fileNameExtension);
            string mimeType;
            byte[] renderedBytes;
            //if ((Int32)BLL.ViewModels.AutoAssignmentViewModel.AssignmentType.PropertyAssignment == ServiceType)
            //{
            //    ReportPath = Server.MapPath(MvcApplication.ReportPath + "PropertyClaimDetailsPrint.rdlc");
            //    renderedBytes = Utility.ConvertRDLCToByteArray(AssignmentClaimID, ReportPath, out mimeType, ServiceType);
            //}
            //else
            //{
            //    ReportPath = Server.MapPath(MvcApplication.ReportPath + "ClaimDetailsPrint.rdlc");
            //    renderedBytes = Utility.ConvertRDLCToByteArray(AssignmentClaimID, ReportPath, out mimeType, ServiceType);
            //}
            renderedBytes = Utility.ConvertRDLCToByteArray(AssignmentClaimID, out mimeType, ServiceType);
            return File(renderedBytes, mimeType);

        }
        public string FirstDoc(Int64 DocID)
        {
            string Fileurl = "";

            try
            {
                BLL.AutoDocument document = css.AutoDocuments.Find(DocID);
                string baseFolder = System.Configuration.ConfigurationManager.AppSettings["AutoAssignmentDocsPath"].ToString();
                string assignmentId = document.AutoAssignmentId + "";
                string documentTypeId = document.DocumentTypeId + "";
                //check whether the folder with user's userid exists
                string filename = document.AutoDocumentPath;
                if (MvcApplication.UseAzureCloudStorage == "1")
                {
                    string containerName = MvcApplication.AutoAssignDocsContainerName;//ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                    Fileurl = CloudStorageUtility.GetAbsoluteFileURL(containerName, document.AutoAssignmentId + "/" + document.DocumentTypeId + "/" + document.AutoDocumentPath);

                }
                else
                {
                    //  Fileurl = Path.Combine((baseFolder + "" + assignmentId + "/" + documentTypeId), document.AutoDocumentPath);
                    // check whether the destination folder depending on the type of document being uploaded exists
                    if (System.IO.File.Exists(Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), document.AutoDocumentPath)))
                    {
                        Fileurl = Path.Combine((VirtualPathUtility.ToAbsolute(baseFolder) + "" + assignmentId + "/" + documentTypeId), document.AutoDocumentPath);
                    }
                    else
                    {
                        Fileurl = Path.Combine(VirtualPathUtility.ToAbsolute(baseFolder), "NotFound.txt");
                    }
                }
                return Fileurl;


            }
            catch (Exception ex)
            {

            }
            return Fileurl;
        }
    }
}
