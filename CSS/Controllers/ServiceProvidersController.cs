﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BLL.Models;
using BLL.ViewModels;
using System.Data;
using System.Data.SqlClient;
using System.Data.Linq.SqlClient;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Data.Objects;

namespace CSS.Controllers
{

    public class ServiceProvidersController : CustomController
    {

        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        ServiceProviderDetailViewModel viewModel;

        public ActionResult Index()
        {
            return View(new AssignmentServiceProviderSearchViewModel());
        }

        public ActionResult InactiveList()
        {
            css = new ChoiceSolutionsEntities();
            List<User> serviceProviders = css.Users.Where(p => p.Active == false).ToList();

            return View(serviceProviders);
        }

        public ActionResult Search()
        {

            ServiceProviderSearchViewModel viewModel = new ServiceProviderSearchViewModel();
            try
            {
               if (Session["ServiceProviderSearch"] != null)
               {
                   viewModel = JsonConvert.DeserializeObject<ServiceProviderSearchViewModel>(Session["ServiceProviderSearch"].ToString());
                    viewModel.ServiceProvidersList = getSPSearchResultNew(viewModel, new FormCollection(),false);
               }
               else
                {
                    viewModel.ServiceProvidersList = getSPSearchResultNew(viewModel, null,true);
                    viewModel.IsIADAMember = "ActiveIADA";
               }

              //  viewModel.MapViewModel = getMapViewModel(viewModel);//map to be processed before paging
                viewModel = setPager(viewModel, null);
                //viewModel.populateLists();

                Session["sortingid"] = "";
            }
            catch (Exception ex)
            {

            }

            return View(viewModel);
        }
        [HttpPost]
       public ActionResult Search(ServiceProviderSearchViewModel viewModel, FormCollection form, string SubmitButton="Search")
        {
            switch (SubmitButton)
            {
                case "Search":
                    //original code
                    //List<User> serviceProviders = css.Users.ToList();
                    //viewModel.ServiceProviders = getSPSearchResult(viewModel, form);
                    //viewModel.MapViewModel = getMapViewModel(viewModel);//map to be processed before paging
                    viewModel.ServiceProvidersList = getSPSearchResultNew(viewModel, form,true);
                    viewModel = setPager(viewModel, form);
                    viewModel.populateLists();
                    //Session["ServiceProviderSearch"] = viewModel;
                    Session["ServiceProviderSearch"] = JsonConvert.SerializeObject(viewModel);
                    //Session["ServiceProviderSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                    return View(viewModel);
                //                                   State=p.User.State=="0"?"":S.Name,
                case "Export":

                    var state1 = (from S in css.StateProvinces
                                  where S.StateProvinceCode.Equals("Ak")
                                  select new { StateName = S.Name });
                    
                    GridView gv = new GridView();
                    viewModel.ServiceProviders = getSPSearchResult(viewModel, form);
                    var data = from p in viewModel.ServiceProviders.ToList()
                               join S in css.StateProvinces
                               on p.User.State equals S.StateProvinceCode into tg
                               from S in tg.DefaultIfEmpty()                               
                               select new
                               {
                                   Full_Name = p.User.FirstName + " " + p.User.MiddleInitial + " " + p.User.LastName,
                                   LLC_Corp_Partnership_Name = p.DBA_LLC_Firm_Corp,
                                   State = p.User.State == "0" ? "" : S.Name,
                                   City = p.User.City,
                                   Zip = p.User.Zip,
                                   Mobile_Phone = p.User.MobilePhone,
                                   Email_Address = p.User.Email,
                                   Software_Experience = string.Join(",", (from A in p.User.ServiceProviderSoftwareExperiences
                                                                          where A.YearsOfExperience > 0
                                                                          select A.Software).ToArray()),
                                   Rank = p.Rank,                            
                                   
                                   Status = (p.User.Active != null ? (p.User.Active.Value == true ? "Approved" : "Rejected") : "")
                               };
                    
                    gv.DataSource = data.ToList();
                    gv.DataBind();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment; filename=ServiceProviderList.xls");
                    Response.ContentType = "application/ms-excel";
                    //Response.Charset = "";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    gv.RenderControl(htw);
                    Response.Write(sw.ToString());
                    //Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();

                    viewModel.ServiceProviders = getSPSearchResult(viewModel, form);
                    viewModel.MapViewModel = getMapViewModel(viewModel);//map to be processed before paging
                    viewModel = setPager(viewModel, form);
                    viewModel.populateLists();
                    //Session["ServiceProviderSearch"] = viewModel;
                    Session["ServiceProviderSearch"] = JsonConvert.SerializeObject(viewModel);
                    //Session["ServiceProviderSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                    return View(viewModel);
        }

            return View(viewModel);
        }

        private List<usp_ServiceProviderSearch_Result> getSPSearchResultNew(ServiceProviderSearchViewModel viewModel, FormCollection form,bool isPostRequest)
        {
          //Difault Initialization
            if(isPostRequest)
             viewModel.Pager = new BLL.Models.Pager();

            if (isPostRequest)
            {
                if (form == null)
                {
                    viewModel.Pager.Page = 1;
                    viewModel.Pager.FirstPageNo = 1;
                }
                else
                {
                    if (form["hdnCurrentPage"] != null && !String.IsNullOrEmpty(form["hdnCurrentPage"]) && (form["hdnCurrentPage"]) != "0")
                    {

                        viewModel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);
                    }
                    else
                    {
                        viewModel.Pager.Page = 1;
                    }
                    if (form["hdnstartPage"] != null)
                    {
                        if (!String.IsNullOrEmpty(form["hdnstartPage"]))
                        {
                            viewModel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
                        }
                        else
                        {
                            viewModel.Pager.FirstPageNo = 1;
                        }
                    }
                    else { viewModel.Pager.FirstPageNo = 1; }
                }
            }
            viewModel.Distance = 100;
            viewModel.Pager.RecsPerPage = 20;

            ObjectParameter outRowCount = new ObjectParameter("TotalRowCount", DbType.Int64);
            List<usp_ServiceProviderSearch_Result> spList = css.usp_ServiceProviderSearch(viewModel.Status, viewModel.Rank, viewModel.UserName, viewModel.ContactName,
               viewModel.HeadCompanyID, viewModel.Email, viewModel.MobilePhone, viewModel.Zip, viewModel.Distance, viewModel.State, viewModel.City, viewModel.IsIADAMember,
               viewModel.sortString, viewModel.sortBy, viewModel.RegisteredDateFrom, viewModel.RegisteredDateTo, viewModel.ApprovedDateFrom, viewModel.ApprovedDateTo, viewModel.Pager.Page, viewModel.Pager.RecsPerPage, outRowCount).ToList();
            
            viewModel.RecordCount =Convert.ToInt64(outRowCount.Value);
            
            return spList;
        }

        private List<ServiceProviderDetail> getSPSearchResult(ServiceProviderSearchViewModel viewModel, FormCollection form)
        {
            IEnumerable<ServiceProviderDetail> spList = css.ServiceProviderDetails.Where(x => ((x.User.UserTypeId == 1) || (x.User.UserTypeId == 8)));

            if (viewModel.Status == "Inactive")
            {
                spList = spList.Where(p => (p.User.Active == false) || (p.User.Active == null));
            }
            else if (viewModel.Status == "Active")
            {
                spList = spList.Where(p => p.User.Active == true);
            }
            else
            {
                spList = spList.Where(p => (p.User.Active == true) || (p.User.Active == false) || (p.User.Active == null));
            }  

            if (viewModel.Rank > 0)
            {
                spList = spList.Where(x => x.Rank >= viewModel.Rank);
            }
            if (viewModel.UserName != null)
            {
                if (!String.IsNullOrEmpty(viewModel.UserName))
                    spList = spList.Where(x => x.User.UserName.ToLower().Contains(viewModel.UserName.ToLower()));
            }
            if (viewModel.ContactName != null)
            {
                if (!String.IsNullOrEmpty(viewModel.ContactName))
                {
                    string contactname = viewModel.ContactName.Trim().ToLower();
                    spList = spList.Where(x => (x.User.FirstName ?? "").ToLower().Contains(contactname) || (x.User.LastName ?? "").ToLower().Contains(contactname));
                }

            }
            //if (!String.IsNullOrEmpty(viewModel.DBA_LLC_Firm_Corp))
            //{
            //    spList = spList.Where(x => (x.DBA_LLC_Firm_Corp ?? "").ToLower().Contains(viewModel.DBA_LLC_Firm_Corp.ToLower()));
            //}

            if (viewModel.HeadCompanyID!=0)
            {
                spList = spList.Where(x => (x.User.HeadCompanyId??0)==viewModel.HeadCompanyID);
            }
            if (!String.IsNullOrEmpty(viewModel.Email))
            {
                spList = spList.Where(x => (x.User.Email ?? "").ToLower().Contains(viewModel.Email.ToLower()));
            }
            if (!String.IsNullOrEmpty(viewModel.MobilePhone))
            {
                spList = spList.Where(x => (x.User.MobilePhone ?? "").ToLower().Contains(viewModel.MobilePhone.ToLower()));
            }

            //if (!String.IsNullOrEmpty(viewModel.Zip))
            //{
            //    spList = spList.Where(x => (!String.IsNullOrEmpty(css.usp_GetDistance(viewModel.Zip, x.User.Zip).First().Distance)) && (Convert.ToDouble(css.usp_GetDistance(viewModel.Zip, x.User.Zip).First().Distance) <= viewModel.Distance));
            //}
            viewModel.Distance = 100;
            if (!String.IsNullOrEmpty(viewModel.Zip))
            {
                spList = spList.Where(x => (!String.IsNullOrEmpty(css.usp_GetDistance(viewModel.Zip, x.User.Zip).First().Distance))  && (Convert.ToDouble(css.usp_GetDistance(viewModel.Zip, x.User.Zip).First().Distance) <= viewModel.Distance));
            }
            else
            {
                if (!String.IsNullOrEmpty(viewModel.State))
                {
                    if (viewModel.State != "0")
                    {
                        spList = spList.Where(x => (x.User.State ?? "") == viewModel.State);
                    }
                }
                if (!String.IsNullOrEmpty(viewModel.City))
                {
                    spList = spList.Where(x => (x.User.City ?? "").ToLower().Contains(viewModel.City.ToLower()));
                }
            }

            if (viewModel.IsIADAMember == "InactiveIADA")
            {
                spList = spList.Where(x => ((x.User.Company1.IsIadaMember??false) == false));
            }
            else if (viewModel.IsIADAMember == "ActiveIADA")
            {
                spList = spList.Where(x => ((x.User.Company1.IsIadaMember ?? false) == true));
            }
            

            //sorting
            //viewModel.Sorting = form["hdnsorting"];
            //var HeaderName = form["hdnHeader"];
            //Session["HeaderName"] = form["hdnHeader"];
            var HeaderName = viewModel.sortBy;
            var sorting = form["hdnsorting"];
            if (HeaderName == "CompanyName")
            {
                if (viewModel.sortString == "DSC")
                {
                    spList = spList.OrderByDescending(x => x.User.Company1.CompanyName);
                 
                }
                else if (viewModel.sortString == "ASC")
                {
                    spList = spList.OrderBy(x => x.User.Company1.CompanyName);                   
                } 
            }
            else if (HeaderName == "StateName")
            {
                if (viewModel.sortString == "DSC")
                {
                    spList = spList.OrderByDescending( x => x.User.State);

                }
                else if (viewModel.sortString == "ASC")
                {
                    spList = spList = spList.OrderBy(x => x.User.State);

                } 
            }
            else if (HeaderName == "CityName")
            {
                if (viewModel.sortString == "DSC")
                {
                    spList = spList.OrderByDescending(x => x.User.City);

                }
                else if (viewModel.sortString == "ASC")
                {
                    spList = spList.OrderBy(x => x.User.City);

                } 
            }

            #region
            //if (viewModel.Sorting == "comasc" || viewModel.Sorting == "comdes")
            //{
            //    if (viewModel.Sorting == "comasc")
            //    {
            //        spList = from sp in spList
            //                 orderby sp.User.Company1.CompanyName ascending
            //                 select sp;                        
            //    }
            //    else
            //    {
            //        spList = spList.OrderByDescending(x => x.User.Company1.CompanyName);
            //    }

            //}
            //else if (viewModel.Sorting == "stateasc" || viewModel.Sorting == "statedes")
            //{
            //    if (viewModel.Sorting == "stateasc")
            //    {
            //        spList = from sp in spList
            //                 orderby sp.User.Company1.State ascending
            //                 select sp;
            //    }
            //    else
            //    {
            //        spList = spList.OrderByDescending(x => x.User.Company1.State);
            //    }
            //}
            //else if (viewModel.Sorting == "cityasc" || viewModel.Sorting == "citydes")
            //{
            //    if (viewModel.Sorting == "cityasc")
            //    {
            //        spList = from sp in spList
            //                 orderby sp.User.Company1.City ascending
            //                 select sp;
            //    }
            //    else
            //    {
            //        spList = spList.OrderByDescending(x => x.User.Company1.City);
            //    }
            //}
            #endregion

            //if (viewModel.IsIADAMember   == "InactiveIADA")
            //{
            //    spList = spList.Where(x => (x.User.Company1.IsIadaMember == false ) || (x.User.Company1.IsIadaMember == null));
            //}
            //else if (viewModel.IsIADAMember == "ActiveIADA")
            //{
            //    spList = spList.Where(x => (x.User.Company1.IsIadaMember == true));
            //}

            //Check IS IADA Member
            //if (viewModel.IsIADAMember)
            //{
            //    spList = spList.Where(x => (x.User.Company1.IsIadaMember ?? false) == viewModel.IsIADAMember);
            //}
            //int count = spList.Count();


//Commented fot IADA
          //  if (viewModel.EQCertified == true)
          //  {
          //      spList = spList.Where(x => x.EQCertified == viewModel.EQCertified);
          //  }
          //  if (viewModel.HAAGCertified == true)
          //  {
          //      spList = spList.Where(x => x.HAAGCertified == viewModel.HAAGCertified);
          //  }
          //  if (viewModel.IsFloodCertified == true)
          //  {
          //      spList = spList.Where(x => x.IsFloodCertified == viewModel.IsFloodCertified);
          //  }
          //  if (viewModel.EOHasInsurance == true)
          //  {
          //      spList = spList.Where(x => x.EOHasInsurance == viewModel.EOHasInsurance);
          //  }


          //  if (viewModel.IsBilingual)
          //  {
          //      spList = spList.Where(x => String.IsNullOrEmpty(x.OtherLanguages) == false);
          //  }

          //  if (!String.IsNullOrEmpty(viewModel.CapabilityToClimbSteep))
          //  {
          //      if (viewModel.CapabilityToClimbSteep != "0")
          //      {
          //          spList = spList.Where(x => (x.CapabilityToClimbSteep ?? "") == viewModel.CapabilityToClimbSteep);
          //      }
          //  }
          //  if (!String.IsNullOrEmpty(viewModel.CapabilityToClimb))
          //  {
          //      if (viewModel.CapabilityToClimb != "0")
          //      {
          //          spList = spList.Where(x => (x.CapabilityToClimb ?? "") == viewModel.CapabilityToClimb);
          //      }
          //  }
          //  if (viewModel.HasRopeHarnessExp != false)
          //  {
          //      spList = spList.Where(x => x.HasRopeAndHarnessExp == true);
          //  }
          //  if (viewModel.HasRopeHarnessEquip != false)
          //  {
          //      spList = spList.Where(x => x.HasRopeHarnessEquip == true);
          //  }
          //  if (viewModel.HasRoofAssistProgram != false)
          //  {
          //      spList = spList.Where(x => x.HasRoofAssistProgram == true);
          //  }
          //  //JIRA OPT-64: Filter added on 27-05-2013
          //  if (viewModel.HasExaminerRights != false)
          //  {
          //      spList = spList.Where(x => x.User.UserAssignedRights.Any(y => y.UserRightId == 1 && (y.IsActive ?? false) == true));
          //  }

            viewModel.ServiceProviders = spList.ToList(); //Send Request to DB and fetch Data

          ////  if (viewModel.HasW9Signed != false || viewModel.HasContractSigned != false)
          //  //if (viewModel.HasIgnoreSigned != true)
          //  if (viewModel.HasIgnoreSigned != false)
          //  {
          //  if (viewModel.HasW9Signed != false && viewModel.HasContractSigned==false)
          //  {

          //      List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();

          //      foreach (ServiceProviderDetail serviceProvider in viewModel.ServiceProviders.ToList())
          //      {
          //          bool SPW9Exists = false;
          //          bool SPContractExists = false;
          //          if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == 9 && (x.IsSigned ?? false) == true).Count() > 0)
          //          {
          //              SPW9Exists = true;
          //          }

          //           if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == 8 && (x.IsSigned ?? false) == true).Count() > 0)
          //          {
          //              SPContractExists = true;
          //          }
          //          if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == 12 && (x.IsSigned ?? false) == true).Count() > 0)
          //          {
          //              SPContractExists = true;
          //          }
          //          if (SPW9Exists == true && SPContractExists == false)
          //          {
          //              tempSP.Add(serviceProvider);
          //          }

          //      }
          //      viewModel.ServiceProviders = tempSP;


          //  }

          //  if (viewModel.HasContractSigned != false && viewModel.HasW9Signed==false)
          //  {

          //      List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();

          //      foreach (ServiceProviderDetail serviceProvider in viewModel.ServiceProviders.ToList())
          //      {
          //          bool SPW9Exists = false;
          //          bool SPContractExists = false;
          //          //if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == 12 && (x.IsSigned ?? false) == true).Count() > 0)
          //          if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == 8 && (x.IsSigned ?? false) == true).Count() > 0)
          //          {
          //              SPContractExists = true;
          //          }
          //          if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == 12 && (x.IsSigned ?? false) == true).Count() > 0)
          //          {
          //              SPContractExists = true;
          //          }

          //          if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == 9 && (x.IsSigned ?? false) == true).Count() == 0)
          //          {
          //              SPW9Exists =true;
          //          }
          //          if (SPContractExists == true && SPW9Exists == true)
          //          {
          //              tempSP.Add(serviceProvider);
          //          }
                   
          //      }
          //      viewModel.ServiceProviders = tempSP;


          //  }

          //  //Gaurav 09-06-2014
          //  if (viewModel.HasContractSigned != false && viewModel.HasW9Signed!=false)
          //  {

          //     List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();

          //  foreach (ServiceProviderDetail serviceProvider in viewModel.ServiceProviders.ToList())
          //  {
          //      bool SPW9Exists=false;
          //      bool SPContractExists=false;
          //  //    if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && (x.DTId == 12) && (x.IsSigned ?? false) == true).Count() > 0)
          //      if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId ==8 && (x.IsSigned ?? false) == true).Count() > 0)
          //      {
          //          //tempSP.Add(serviceProvider);
          //          SPContractExists = true;
          //      }
          //      if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId ==12 && (x.IsSigned ?? false) == true).Count() > 0)
          //      {
          //          //tempSP.Add(serviceProvider);
          //          SPContractExists = true;
          //      }
          //      if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && (x.DTId == 9) && (x.IsSigned ?? false) == true).Count() > 0)
          //      {
          //        //  tempSP.Add(serviceProvider);
          //          SPW9Exists = true;
          //      }

          //      if (SPW9Exists && SPContractExists)
          //      {
          //          tempSP.Add(serviceProvider);
          //      }

          //  }
          //  viewModel.ServiceProviders = tempSP;


          //  }

          //  //Percy 18-06-2014  If both are false we want those SP's that have not signed.
          //  if (viewModel.HasContractSigned == false && viewModel.HasW9Signed == false)
          //  {

          //      List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();

          //      foreach (ServiceProviderDetail serviceProvider in viewModel.ServiceProviders.ToList())
          //      {
          //          bool SPW9Exists = false;
          //          bool SPContractExists = false;
          //          //if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == (serviceProvider.EOHasInsurance == true ? 8 : 12) && (x.IsSigned ?? false) == true).Count() > 0)
          //         // if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && (x.DTId == 12) && (x.IsSigned ?? false) == true).Count() > 0)
          //          if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == 8  && (x.IsSigned ?? false) == true).Count() > 0)
          //          {
          //              //tempSP.Add(serviceProvider);
          //              SPContractExists = true;
          //          }
          //          if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && x.DTId == 12 && (x.IsSigned ?? false) == true).Count() > 0)
          //          {
          //              //tempSP.Add(serviceProvider);
          //              SPContractExists = true;
          //          }
          //          if (css.ServiceProviderDocuments.Where(x => x.UserId == serviceProvider.UserId && (x.DTId == 9) && (x.IsSigned ?? false) == true).Count() > 0)
          //          {
          //              //  tempSP.Add(serviceProvider);
          //              SPW9Exists = true;
          //          }

          //          if (!SPW9Exists && !SPContractExists)
          //          {
          //              tempSP.Add(serviceProvider);
          //          }

          //      }
          //      viewModel.ServiceProviders = tempSP;


          //  }
          //  }
          //  if (!String.IsNullOrEmpty(viewModel.ExperienceClaimTypeId))
          //  {
          //      if (viewModel.ExperienceClaimTypeId != "0")
          //      {
          //          List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();

          //          foreach (ServiceProviderDetail serviceProvider in viewModel.ServiceProviders.ToList())
          //          {
          //              if (serviceProvider.User.ServiceProviderExperiences.Where(exp => exp.ClaimTypeId == Convert.ToInt32(viewModel.ExperienceClaimTypeId)).ToList().Count > 0)
          //              {
          //                  tempSP.Add(serviceProvider);
          //              }
          //          }
          //          viewModel.ServiceProviders = tempSP;
          //      }
          //  }
          //  if (viewModel.ExperienceYears != 0)
          //  {
          //      List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();

          //      foreach (ServiceProviderDetail serviceProvider in viewModel.ServiceProviders.ToList())
          //      {
          //          if (serviceProvider.User.ServiceProviderExperiences.Where(exp => exp.YearsOfExperience >= viewModel.ExperienceYears).ToList().Count > 0)
          //          {
          //              tempSP.Add(serviceProvider);
          //          }
          //      }
          //      viewModel.ServiceProviders = tempSP;
          //  }

          //  if ((viewModel.FloodClaimTypeID??"0") != "0")
          //  {
          //      List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();

          //      foreach (ServiceProviderDetail spd in viewModel.ServiceProviders.ToList())
          //      {
          //          if (spd.User.ServiceProviderFloodExperiences.Where(exp => exp.ClaimTypeId == Convert.ToInt32(viewModel.FloodClaimTypeID)).ToList().Count > 0)
          //          {
          //              tempSP.Add(spd);
          //          }
          //      }
          //      viewModel.ServiceProviders = tempSP;
          //  }
          //  if (viewModel.FloodExperienceYears != 0)
          //  {
          //      List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();

          //      foreach (ServiceProviderDetail spd in viewModel.ServiceProviders.ToList())
          //      {
          //          if (spd.User.ServiceProviderFloodExperiences.Where(exp => exp.YearsOfExperience >= viewModel.FloodExperienceYears).ToList().Count > 0)
          //          {
          //              tempSP.Add(spd);
          //          }
          //      }
          //      viewModel.ServiceProviders = tempSP;
          //  }




          //  if (viewModel.HasLicense)
          //  {
          //      viewModel.ServiceProviders = viewModel.ServiceProviders.Where(x => x.User.ServiceProviderLicenses.Count != 0).ToList();
          //      if (viewModel.LicenseState != "0")
          //      {
          //          List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();
          //          foreach (ServiceProviderDetail spd in viewModel.ServiceProviders.ToList())
          //          {
          //              if (spd.User.ServiceProviderLicenses.Where(exp => (exp.State ?? "") == viewModel.LicenseState).ToList().Count > 0)
          //              {
          //                  tempSP.Add(spd);
          //              }

          //          }
          //          viewModel.ServiceProviders = tempSP;
          //      }

          //      if (viewModel.LicenseStateFrom.HasValue && viewModel.LicenseStateTo.HasValue)
          //      {
          //          List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();
          //          foreach (ServiceProviderDetail spd in viewModel.ServiceProviders.ToList())
          //          {

          //              if (spd.User.ServiceProviderLicenses.Where(exp => exp.ExpirationDate >= viewModel.LicenseStateFrom && exp.ExpirationDate <= viewModel.LicenseStateTo).Count() > 0)
          //              {
          //                  tempSP.Add(spd);
          //              }
          //          }
          //          viewModel.ServiceProviders = tempSP;
          //      }
          //  }

          if (viewModel.RegisteredDateFrom.HasValue && viewModel.RegisteredDateTo.HasValue)
            {
                List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();
                foreach (ServiceProviderDetail spd in viewModel.ServiceProviders.ToList())
                {
                    if (spd.User.RegisteredOnDate >= viewModel.RegisteredDateFrom && spd.User.RegisteredOnDate <= viewModel.RegisteredDateTo)
                    {
                        tempSP.Add(spd);
                    }

                }
                viewModel.ServiceProviders = tempSP;
                

            }

            if (viewModel.ApprovedDateFrom.HasValue && viewModel.ApprovedDateTo.HasValue)
            {
                List<ServiceProviderDetail> tempSP = new List<ServiceProviderDetail>();
                foreach (ServiceProviderDetail spd in viewModel.ServiceProviders.ToList())
                {
                    if ((spd.User.Active ?? false) == true && (spd.User.ApprovedDate >= viewModel.ApprovedDateFrom && spd.User.ApprovedDate <= viewModel.ApprovedDateTo))
                    {
                        tempSP.Add(spd);
                    }

                }
                viewModel.ServiceProviders = tempSP;


            }

            //string[] SoftwareUsedSelectedList = null;
            //if (form["SoftwareUsed"] != null)
            //{
            //    SoftwareUsedSelectedList = form["SoftwareUsed"].Split(',');
            //    viewModel.SoftwareUsed = form["SoftwareUsed"];//will enable to reselect the selected checkboxes after this View is loaded again
            //    if (SoftwareUsedSelectedList.ToList().Count != 0)
            //    {
            //        foreach (string software in SoftwareUsedSelectedList)
            //        {
            //            List<ServiceProviderDetail> tempSPD = new List<ServiceProviderDetail>();
            //            foreach (ServiceProviderDetail spd in viewModel.ServiceProviders.ToList())
            //            {
            //                if (spd.User.ServiceProviderSoftwareExperiences.Where(exp => ((exp.Software ?? "") == software) && (exp.YearsOfExperience > 0)).ToList().Count > 0)
            //                {
            //                    tempSPD.Add(spd);
            //                }
            //            }
            //            viewModel.ServiceProviders = tempSPD;
            //        }
            //    }
            //}
            //if (viewModel.HasDeployed==true)
            //{
            //   viewModel.ServiceProviders = viewModel.ServiceProviders.Where(x => (x.User.ServiceProviderDetail.IsDeployed ?? false) == true).ToList();

            //}
            return viewModel.ServiceProviders;

        }
        private SPSearchMapViewModel getMapViewModel(ServiceProviderSearchViewModel viewModel)
        {
            List<SPSearchMapMaker> markerList = new List<SPSearchMapMaker>();
            for (int i = 0; i < viewModel.ServiceProviders.Count; i++)
            {
                if (!String.IsNullOrEmpty(viewModel.ServiceProviders[i].User.Zip))
                {
                    string spZipCode = viewModel.ServiceProviders[i].User.Zip;
                    List<tbl_ZipUSA> zipList = css.tbl_ZipUSA.Where(x => x.ZipCode == spZipCode).ToList();
                    if (zipList.Count != 0)
                    {
                        tbl_ZipUSA zip = zipList.First();
                        string spName = viewModel.ServiceProviders[i].User.FirstName + " " + viewModel.ServiceProviders[i].User.LastName;
                        SPSearchMapMaker location = new SPSearchMapMaker(i, spName, zip.Lat.Value, zip.Lng.Value);
                        location.SPId = viewModel.ServiceProviders[i].User.UserId + "";
                        location.State = viewModel.ServiceProviders[i].User.State;
                        location.City = viewModel.ServiceProviders[i].User.City;
                        location.Zip = viewModel.ServiceProviders[i].User.Zip;
                        location.Mobile = viewModel.ServiceProviders[i].User.MobilePhone;
                        location.Email = viewModel.ServiceProviders[i].User.Email;


                        markerList.Add(location);
                    }
                }
            }
            SPSearchMapViewModel googleMapViewModel = new SPSearchMapViewModel();
            googleMapViewModel.MarkerList = markerList;

            return googleMapViewModel;
        }
        public ServiceProviderSearchViewModel setPager(ServiceProviderSearchViewModel viewModel, FormCollection form)
        {
            viewModel.Pager.IsAjax = false;
            viewModel.Pager.FormName = "ServiceProviders";
          
            viewModel.Pager.TotalCount = viewModel.Pager.TotalCount = viewModel.RecordCount.ToString();
            viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
          
            return viewModel;
        }

        public ActionResult Edit(Int64 id)
        {
            return RedirectToAction("Submit", "ServiceProviderRegistration", new { id = id });

        }

        [HttpPost]
        public ActionResult Approve(Int64 hdnApproveUserId, Int32 Rank)
        {
            BLL.User user = css.Users.Find(hdnApproveUserId);

            user.Active = true;
			  //gaurav 09-06-2014
            //user.ApprovedDate = Convert.ToDateTime(System.DateTime.Now);
            //user.RejectionRemarks = null;
            //css.Entry(user).State = EntityState.Modified;
            //css.SaveChanges();

            //css = new ChoiceSolutionsEntities();
            //BLL.ServiceProviderDetail spd = css.ServiceProviderDetails.Find(hdnApproveUserId);
            //spd.Rank = Rank;
            //css.Entry(spd).State = EntityState.Modified;
            //css.SaveChanges();
            if (user != null)
            {
                css.usp_updateUserApproveStatus(hdnApproveUserId, true, null, Convert.ToDateTime(System.DateTime.Now), Rank);
            }
            ////Task.Factory.StartNew(() =>
            ////{
            ////    Utility.QBUpdateVendor(user, spd);
            ////}, TaskCreationOptions.LongRunning);

            return RedirectToAction("Search");
        }


        [HttpPost]
        public ActionResult Reject(Int64 hdnRejectUserId, string txtRejectRemarks, bool cbEmailSPRej)
        {
            BLL.User user = css.Users.Find(hdnRejectUserId);
            if (cbEmailSPRej)
            {
                List<string> mailTo = new List<string>();
                string mailFrom = System.Configuration.ConfigurationManager.AppSettings["SupportEmailAddress"].ToString();
                if (String.IsNullOrEmpty(user.Email) && user.User1 != null)
                {
                    mailTo.Add(user.User1.Email);
                }
                else
                {
                    mailTo.Add(user.Email);
                }
                string mailSubject = "Service Provider Registration Rejected";
                string mailBody = "";
                mailBody += "Dear " + user.FirstName + " " + (String.IsNullOrEmpty(user.MiddleInitial) ? "" : " " + user.MiddleInitial + " ") + " " + user.LastName + ",";
                mailBody += "<br />";
                mailBody += "<br />Your Service Provider Registration was reviewed by us, but has been rejected. Kindly review the cause of rejection given below.";
                mailBody += "<br />";
                mailBody += "<br />" + txtRejectRemarks;
                mailBody += "<br />";
                mailBody += "<br />";
                mailBody += "<br />";
                mailBody += "<br />Thanking you,";
                mailBody += "<br />";
                mailBody += "<br />Support,";
                mailBody += "<br />IADA Claim Solutions Inc.";
                Utility.sendEmail(mailTo, mailFrom, mailSubject, mailBody);
            }
            //user.Active = false;
            //user.RejectionRemarks = txtRejectRemarks;
            //css.Entry(user).State = EntityState.Modified;
            //css.SaveChanges();
            css.usp_updateUserIsActiveStatus(hdnRejectUserId, false, txtRejectRemarks);
            return RedirectToAction("Search");

        }

        public ActionResult SPDetails(Int32 SPID)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
           
            return PartialView("_ServiceProviderDetails", css.usp_GetSPMapDetails(SPID, loggedInUser.UserId).ToList());           
        }
       

        [Authorize]
        public ActionResult ServiceProviderDetails(string ID, FormCollection form)
        {
            Int64 id=0;
            //Cypher cypher = new Cypher();
            if (ID != null)
            {              
                id = Convert.ToInt64(Cypher.DecryptString(Cypher.ReplaceCharcters(ID)));
            }
            else
            {
                id = -1;
            }

            
            viewModel = new ServiceProviderDetailViewModel(id);
            viewModel.user = css.Users.Find(id);

            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            List<UserAssignedRight> userassignedrights = new List<UserAssignedRight>();
            userassignedrights = css.UserAssignedRights.Where(x => x.UserId == loggedInUser.UserId).ToList();

            if (loggedInUser.UserTypeId != 2 && loggedInUser.UserTypeId != 3 )
            {
                //if (userassignedrights.Where(x => x.UserRightId == 2).Count() <= 0)
                //{
                //    return View("AuthorizationError");
                //}
            
                
            }
            if (viewModel.user.NetworkProviderId > 0)
            {
                viewModel.NetworkProvider = css.MobileNetworkProviders.SingleOrDefault(x => x.NetworkProviderId == viewModel.user.NetworkProviderId).NetworkProviderName;
            }
            viewModel.UnQualifiedList = css.GetUnQualifiedList(Convert.ToInt32(id)).ToList();
            viewModel.UnQualifiedSPCompanyLOBList = css.GetUnQualifiedSPByLOBList(Convert.ToInt32(id)).ToList();
            viewModel.UnQualifiedSPCompanyClientPOCList = css.GetUnQualifiedSPByClientPOCList(Convert.ToInt32(id)).ToList();
            foreach (ClaimType claimType in css.ClaimTypes.ToList())
            {
                viewModel.ClaimTypeList.Add(claimType.ClaimTypeId, claimType.Description);
            }


            List<ServiceProviderDetail> spDetailsList = css.ServiceProviderDetails.Where(x => x.UserId == id).ToList();
            if (spDetailsList.ToList().Count > 0)
            {
                ServiceProviderDetail spdetails = css.ServiceProviderDetails.Where(x => x.UserId == id).First();

                //Agreement Signed
                int agreementDTId;
                if (viewModel.user.ServiceProviderDetail.EOHasInsurance == true)
                {
                    agreementDTId = 8;
                }
                else
                {
                    agreementDTId = 12;
                }
                if (viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == agreementDTId).Count() > 0)
                {
                    ServiceProviderDocument spdocument = viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == agreementDTId).First();
                    if (CSS.Utility.isSertifiDocumentSigned(spdetails.SertifiFileId, spdocument.SertifiDocumentId, spdocument.SPDId) == true)
                    {
                        viewModel.IsSertifiAgreementSigned = true;
                        viewModel.SertifiAgreementSignedURL = Url.Content("~/ServiceProviderRegistration/ProcessESigningDoc?userID=" + id + "&documentTypeID=" + agreementDTId); ;
                    }
                }

                //W9
                int w9DTId = 9;
                if (viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == w9DTId).Count() > 0)
                {
                    ServiceProviderDocument spdocument = viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == w9DTId).First();
                    if (CSS.Utility.isSertifiDocumentSigned(spdetails.SertifiFileId, spdocument.SertifiDocumentId, spdocument.SPDId) == true)
                    {
                        viewModel.IsSertifiW9Signed = true;
                        viewModel.SertifiW9SignedURL = Url.Content("~/ServiceProviderRegistration/ProcessESigningDoc?userID=" + id + "&documentTypeID=" + w9DTId); ;
                    }
                }

                //DDF
                int DDF_DTId = 11;
                if (viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == DDF_DTId).Count() > 0)
                {
                    ServiceProviderDocument spdocument = viewModel.user.ServiceProviderDocuments.Where(p => p.DTId == DDF_DTId).First();
                    if (CSS.Utility.isSertifiDocumentSigned(spdetails.SertifiFileId, spdocument.SertifiDocumentId, spdocument.SPDId) == true)
                    {
                        viewModel.IsSertifiDDFSigned = true;
                        viewModel.SertifiDDFSignedURL = Url.Content("~/ServiceProviderRegistration/ProcessESigningDoc?userID=" + id + "&documentTypeID=" + DDF_DTId); ;
                    }
                }

            }

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AlternateLocation(AlternetLocationViewModel ViewModel, FormCollection form)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            if (loggedInUser != null)
            {
                if (form["txtAlternateZip"] != null)
                {
                    string strZip = form["txtAlternateZip"];
                    css.usp_updateAlternateLocation(loggedInUser.UserId, strZip);
                }
                //User user = css.Users.Where(u => u.UserName == loggedInUser1.UserName).First();
                //Session["LoggedInUser"] = user;

            }
            return RedirectToAction("Index", "Home");
        }
        [Authorize]
        public ActionResult UserAssignedRights(Int64 userId)
        {
            UserRightsViewModel viewModel = new UserRightsViewModel();
            viewModel.UserId = userId;
            User user = css.Users.Find(userId);
            viewModel.UserFullName = user.FirstName + " " + user.LastName;
            List<usp_UserRightsGetList_Result> userRightsList = css.usp_UserRightsGetList().ToList();
            List<usp_UserAssignedRightsGetList_Result> userAssignedRightsList = css.usp_UserAssignedRightsGetList(userId).ToList();
            List<UserAssignedRightInfo> userAssignedRightsInfoList = new List<UserAssignedRightInfo>();
            foreach (var userRight in userRightsList)
            {
                UserAssignedRightInfo userAssignedRight = new UserAssignedRightInfo();
                userAssignedRight.UserId = userId;
                userAssignedRight.UserRightId = userRight.UserRightId;
                userAssignedRight.UserRightDesc = userRight.UserRightDesc;
                userAssignedRight.HasRight = userAssignedRightsList.Where(x => x.UserRightId == userRight.UserRightId).ToList().Count == 1 ? true : false;
                userAssignedRightsInfoList.Add(userAssignedRight);
            }
            viewModel.UserAssignedRightsInfoList = userAssignedRightsInfoList;
            return Json(RenderPartialViewToString("_UserRights", viewModel), JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public ActionResult UserAssignedRights(UserRightsViewModel viewModel)
        {
            List<usp_UserAssignedRightsGetList_Result> userAssignedRightsList = css.usp_UserAssignedRightsGetList(viewModel.UserId).ToList();
            foreach (var userRights in viewModel.UserAssignedRightsInfoList)
            {
                //Identify if rights already exists
                if (userRights.HasRight)
                {
                    if (userAssignedRightsList.Where(x => x.UserRightId == userRights.UserRightId).ToList().Count == 0)
                    {
                        css.usp_UserAssignedRightsInsert(userRights.UserId, userRights.UserRightId);
                    }
                }
            }

            //Remove rights which were unselected
            foreach (var oldUserRight in userAssignedRightsList)
            {
                if (viewModel.UserAssignedRightsInfoList.Where(x => (x.UserRightId == oldUserRight.UserRightId) && (x.HasRight == true)).ToList().Count == 0)
                {
                    css.usp_UserAssignedRightsDelete(oldUserRight.UserId, oldUserRight.UserRightId);
                }
            }
            return RedirectToAction("Search");
        }
    }
}
