﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ViewModels;
using System.Data;
using System.Data.Objects;
using BLL;

namespace CSS.Controllers
{
    public class CompanyLOBPricingController : Controller
    {
        //
        // GET: /CompanyLOBPricing/
        BLL.ChoiceSolutionsEntities css = new BLL.ChoiceSolutionsEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PricingSchedule(Int32 CompanyId, Int64 CompanyLOBPricingId = -1)
        {
            PricingScheduleViewModel viewmodel = new PricingScheduleViewModel(CompanyId);
            try
            {


                //if (CompanyLOBPricingId == -1)
                //{
                //    viewmodel = new PricingScheduleViewModel(CompanyId);
                //    viewmodel.pricingschedule = css.CompanyLineofBusinessGetList(CompanyId).ToList();
                //}
                //else
                //{
                //    viewmodel = new PricingScheduleViewModel(CompanyId, CompanyLOBPricingId);
                //    viewmodel.pricingschedule = css.CompanyLineofBusinessGetList(CompanyId).ToList();
                //}
            }
            catch (Exception ex)
            {

            }
            return View(viewmodel);


        }

        [HttpPost]
        public ActionResult PricingSchedule(Int32 CompanyId, Int64 companylobpricingid, Int32 lobid, double startrange, double endrange, double baseservicefee, double spservicefee, double rcvpercent)
        {
            int valueToReturn = 0;
            try
            {

                //if (companylobpricingid <= 0)
                //{

                //    ObjectParameter outCompanyLOBPricingId = new ObjectParameter("CompanyLOBPricingId", DbType.Int32);
                //    css.CompanyLOBPricingInsert(outCompanyLOBPricingId, CompanyId, lobid, startrange, endrange, baseservicefee, spservicefee, rcvpercent);
                //}
                //if (companylobpricingid > 0)
                //{
                //    css.CompanyLOBPricingUpdate(companylobpricingid, CompanyId, lobid, startrange, endrange, baseservicefee, spservicefee, rcvpercent);


                //}
                valueToReturn = 1;

            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }

            return Json(valueToReturn);


        }

        public ActionResult DeletePricingSchedule(Int32 CompanyId, Int64 CompanyLOBPricingId)
        {
            try
            {
                if (CompanyLOBPricingId > 0)
                {
                    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                    ObjectParameter outResult = new ObjectParameter("Result", DbType.Int32);
                    css.CompanyLOBPricingDelete(CompanyLOBPricingId, loggedInUser.UserId, outResult);
                    int companylobpricingid = Convert.ToInt32(outResult.Value);
                    if (companylobpricingid <= 0)
                    {
                        TempData["Error"] = "Cannot delete this Pricing as Invoice exists for this Pricing.";
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return RedirectToAction("PricingSchedule", "CompanyLOBPricing", new { CompanyId = CompanyId });


        }


        public ActionResult isValidRange(double startrange, double endrange, Int32 companyid, Int32 lobid, Int64 companylobpricingid)
        {
            int valueToReturn = 0;
            bool isStartRangeValid = false;
            bool isEndRangeValid = false;
            try
            {

                if (startrange >= 0 && lobid > 0 && companyid > 0)
                {
                    ObjectParameter outStartStatus = new ObjectParameter("statusFlag", DbType.Binary);
                    css.IsStartRangeInUse(outStartStatus, startrange, lobid, companyid, companylobpricingid);

                    if ((Convert.ToBoolean(outStartStatus.Value) == false))
                        isStartRangeValid = true;


                }
                if (endrange >= 0 && lobid > 0 && companyid > 0)
                {
                    ObjectParameter outEndStatus = new ObjectParameter("statusFlag", DbType.Binary);
                    css.IsEndRangeInUse(outEndStatus, endrange, lobid, companyid, companylobpricingid);

                    if ((Convert.ToBoolean(outEndStatus.Value) == false))
                        isEndRangeValid = true;

                }
                if (endrange <= startrange)
                {
                    isStartRangeValid = false;
                }
                if (isStartRangeValid && isEndRangeValid)
                {
                    valueToReturn = 1;
                }
                else if (!isStartRangeValid && !isEndRangeValid)
                {
                    valueToReturn = 0;
                }
                else if (!isStartRangeValid)
                {

                    valueToReturn = 2;
                }
                else if (!isEndRangeValid)
                {
                    valueToReturn = 3;
                }


            }
            catch (Exception ex)
            {

            }

            return Json(valueToReturn);


        }

        public ActionResult LOBPricing(int CompanyId, int LobID)
        {

            //Int32 CompanyId = Convert.ToInt32(CompanyId);
            //Int32 LobID = Convert.ToInt32(LobID);

            PricingScheduleViewModel viewmodel = new PricingScheduleViewModel(CompanyId);
            if (CompanyId != 0 && LobID != 0)
            {
                try
                {
                    viewmodel = new PricingScheduleViewModel(CompanyId);//if u delete uncomment -->//  viewmodel.InsuranceCompany = CompanyId; 
                    viewmodel.PopulateLOBlist(LobID);
                    viewmodel.LobID = LobID;
                    viewmodel.strLobDesc = viewmodel.LineofBusinessList.Where(a => a.Value == LobID.ToString()).Select(x => x.Text).SingleOrDefault();
                    //viewmodel.pricingschedule = css.CompanyLineofBusinessGetList(CompanyId).ToList();
                }
                catch (Exception ex)
                {

                }
            }

            //viewmodel = new PricingScheduleViewModel(3);//if u delete uncomment -->//  viewmodel.InsuranceCompany = CompanyId; 
            //viewmodel.PopulateLOBlist(20);
            //viewmodel.LobID = 20;
            //viewmodel.strLobDesc = viewmodel.LineofBusinessList.Where(a => a.Value == "20").Select(x => x.Text).SingleOrDefault();


            return View(viewmodel);

        }

        [HttpPost]
        public ActionResult LOBPricing(PricingScheduleViewModel viewModel, FormCollection form)
        {
            //PricingScheduleViewModel viewmodel = new PricingScheduleViewModel(viewModel.InsuranceCompany);

            viewModel.ValidateLobDetails();
            if (viewModel.ValidationSummary == "") //validate
            {
                try
                {
                    //viewmodel.pricingschedule = css.CompanyLineofBusinessGetList(viewModel.InsuranceCompany).ToList();
                    #region Uncomment once to see old logic
                    ////Uncomment once to see old logic
                    ////if (CompanyLOBPricingId <= 0)
                    ////{
                    ////    CompanyLOBPricing companylobpricing = viewModel.pricingschedule.ElementAt(0);
                    ////    bool hasCompanyLOBPricingData = false;
                    ////    if (companylobpricing != null)
                    ////    {
                    ////        if (!String.IsNullOrEmpty(companylobpricing.LineOfBusiness))
                    ////        {
                    ////            hasCompanyLOBPricingData = true;
                    ////        }

                    ////    }
                    ////    if (hasCompanyLOBPricingData)
                    ////    {

                    ////        //Code for inserting Data of dynamically created Rows into DB 
                    ////        ObjectParameter outCompanyLOBPricingId = new ObjectParameter("CompanyLOBPricingId", DbType.Int64);
                    ////        foreach (LineOfBusiness lob in viewModel.Company.LineOfBusinesses)
                    ////        {
                    ////            foreach (CompanyLOBPricing pricing in lob.CompanyLOBPricings)
                    ////            {
                    ////                css.CompanyLOBPricingInsert(outCompanyLOBPricingId, pricing.CompanyId, pricing.LOBId, pricing.StartRange, pricing.EndRange, pricing.BaseServiceFee, pricing.SPServiceFee, pricing.RCVPercent);
                    ////            }
                    ////        }
                    ////    }



                    ////}
                    ////else
                    ////{
                    ////identify whether any record exists which needs to be inserted
                    ////LineOfBusiness loblist = viewmodel.Company.LineOfBusinesses.ElementAt(0);
                    ////if (loblist.CompanyLOBPricings.Count > 0)
                    ////{
                    ////    CompanyLOBPricing companylobpricing = loblist.CompanyLOBPricings.ElementAt(0);
                    ////    bool hasCompanyLOBPricingData = false;
                    ////    if (companylobpricing != null)
                    ////    {
                    ////        if (companylobpricing.LOBId != null)
                    ////        {
                    ////            if (companylobpricing.LOBId > 0)
                    ////            {
                    ////                hasCompanyLOBPricingData = true;
                    ////            }
                    ////        }

                    ////    }
                    ////    if (hasCompanyLOBPricingData)
                    ////    {

                    ////        ObjectParameter outCompanyLOBPricingId = new ObjectParameter("CompanyLOBPricingId", DbType.Int64);
                    ////        foreach (LineOfBusiness lob in viewmodel.Company.LineOfBusinesses)
                    ////        {
                    ////            foreach (CompanyLOBPricing pricing in lob.CompanyLOBPricings)
                    ////            {
                    ////                if (pricing.CompanyLOBPricingId != 0)
                    ////                {
                    ////                    css.CompanyLOBPricingUpdate(pricing.CompanyLOBPricingId, pricing.CompanyId, pricing.LOBId, pricing.StartRange, pricing.EndRange, pricing.BaseServiceFee, pricing.SPServiceFee, pricing.RCVPercent);
                    ////                }
                    ////                else
                    ////                {
                    ////                    css.CompanyLOBPricingInsert(outCompanyLOBPricingId, pricing.CompanyId, pricing.LOBId, pricing.StartRange, pricing.EndRange, pricing.BaseServiceFee, pricing.SPServiceFee, pricing.RCVPercent);
                    ////                }


                    ////            }
                    ////        }
                    ////    }
                    ////}
                    ////}
                    ////if (companylobpricingid > 0)
                    ////{
                    ////    css.CompanyLOBPricingUpdate(companylobpricingid, CompanyId, lobid, startrange, endrange, baseservicefee, spservicefee, rcvpercent);


                    ////} 
                    #endregion
                    ObjectParameter outCompanyLOBPricingId = new ObjectParameter("CompanyLOBPricingId", DbType.Int64);
                    if (viewModel.LobPricingList != null)
                    {
                        foreach (CompanyLOBPricing pricing in viewModel.LobPricingList)
                        {
                            if (pricing.CompanyLOBPricingId != 0)
                            {
                                css.CompanyLOBPricingUpdate(pricing.CompanyLOBPricingId, viewModel.InsuranceCompany, viewModel.LobID, pricing.StartRange, pricing.EndRange, pricing.BaseServiceFee, pricing.SPServiceFee, pricing.RCVPercent, pricing.SPRCVPercent, pricing.SPXPercent, pricing.IsTimeNExpance);
                            }
                            else
                            {
                                css.CompanyLOBPricingInsert(outCompanyLOBPricingId, viewModel.InsuranceCompany, viewModel.LobID, pricing.StartRange, pricing.EndRange, pricing.BaseServiceFee, pricing.SPServiceFee, pricing.RCVPercent, pricing.SPRCVPercent, pricing.SPXPercent, pricing.IsTimeNExpance);
                            }
                        }
                    }
                    string strhdnDelLobprcIDlst = form["hdnDelLobprcIDlst"];
                    if (strhdnDelLobprcIDlst.Trim() != "")
                    {
                        strhdnDelLobprcIDlst = strhdnDelLobprcIDlst.Substring(0, strhdnDelLobprcIDlst.Length - 1);
                        string[] CompanyLOBPricingIds = strhdnDelLobprcIDlst.Split(',');
                        foreach (string strCLPID in CompanyLOBPricingIds)
                        {
                            if (strCLPID.Trim() != "")
                            {
                                //delete code here
                                try
                                {
                                    css.usp_deletecompanylobpricing(Convert.ToInt32(strCLPID.Trim()));
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                        }
                    }
                    viewModel.ValidationSummary = " Save successful";
                }
                catch (Exception ex)
                {

                }

            }
            else//add vlidattion summarry
            {
                viewModel.Delstring = form["hdnDelLobprcIDlst"];
                viewModel.ValidationSummary = " Error Summary <br/> " + viewModel.ValidationSummary;
            }
            // viewModel =  PricingScheduleViewModel(viewModel.InsuranceCompany);//if u delete uncomment -->//  viewmodel.InsuranceCompany = CompanyId;

            viewModel.FillCompanyDetails();
            viewModel.populateLists(viewModel.InsuranceCompany);
            viewModel.strLobDesc = viewModel.LineofBusinessList.Where(a => a.Value == viewModel.LobID.ToString()).Select(x => x.Text).SingleOrDefault();

            viewModel.PopulateLOBlist(viewModel.LobID);
            //viewmodel.LobID = viewModel.LobID;

            return View(viewModel);

        }


        public ActionResult ICompanyLobPricing(int? CompanyId, int? LobID)
        {
            ViewBag.CompanyId = CompanyId;
            ViewBag.LobID = LobID;

            return View();
        }



        [HttpPost]
        public ActionResult ICompanyLobPricing(FormCollection form)
        {
            return View();
        }

    }




}
