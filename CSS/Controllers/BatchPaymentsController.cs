﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ViewModels;
using BLL;
using System.IO;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Data.Objects;
using System.Data;
using System.Web.Helpers;
using BLL.Models;
using System.Threading.Tasks;

namespace CSS.Controllers
{
    public class BatchPaymentsController : CustomController
    {
        //
        // GET: /BatchPayments/+

        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

        public ActionResult AutoBatchPayments()
        {
            return View();
        }
        public JsonResult GetInvoiceList(string Claimnumber)
        {

            try
            {
                Claim claim = null;
                Claimnumber = Claimnumber.Trim();
                claim = css.Claims.Where(x => x.ClaimNumber == Claimnumber).FirstOrDefault();
                AutoAssignment Autoassignment = null;

                if (claim != null)
                {
                    Autoassignment = css.AutoAssignments.Where(x => x.ClaimId == claim.ClaimId).FirstOrDefault();
                   // AutoInvoice Autoinvoice = css.AutoInvoices.Where(x => x.AutoAssignmentId == Autoassignment.AutoAssignmentId).FirstOrDefault();

                }
                else
                {
                    return Json("No Claim", JsonRequestBehavior.AllowGet);
                }
                AutoInvoiceViewModel viewModel = new BLL.ViewModels.AutoInvoiceViewModel();

                viewModel.AutoInvoiceList = css.usp_AutoInvoiceSummaryGetList(claim.ClaimId).ToList();
                if (viewModel.AutoInvoiceList.Count == 0)
                {
                    return Json("No Invoice", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    IEnumerable<SelectListItem> InvoiceList = new List<SelectListItem>();

                    InvoiceList = (from m in viewModel.AutoInvoiceList select m).AsEnumerable().Select(m => new SelectListItem() { Text = m.AutoInvoiceNo, Value = m.AutoInvoiceNo.ToString() });
                    var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                    return Json(new SelectList(InvoiceList, "Value", "Text"), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetBatchPaymentsDetails(string InvoiceNo)
        {

          
           
            string jsonString="";

          //  PropertyInvoiceViewModel ViewModel = new PropertyInvoiceViewModel();
            try
            {
                AutoInvoice Autoinvoice = css.AutoInvoices.Where(x => x.AutoInvoiceNo == InvoiceNo).FirstOrDefault();
                AutoAssignment Autoassignment=null;
                Claim claim = null;
                
                usp_AutoInvoiceGetDetails_Result invoicedetail=null;
                if (Autoinvoice != null)
                {

                    Autoassignment = css.AutoAssignments.Where(x => x.AutoAssignmentId == Autoinvoice.AutoAssignmentId).FirstOrDefault();
                }

                if (Autoassignment != null)
                {

                    //claim = css.Claims.Where(x => x.ClaimId == Autoassignment.ClaimId).FirstOrDefault();
                    //claims = css.usp_GetClaimInfoPrint(Autoassignment.ClaimId).FirstOrDefault();
                    var claims = (from t1 in css.Claims
                             from t2 in css.ClaimReferrers.Where(x => t1.ClaimId == x.ClaimId && t1.ReferrerTypeId == x.ReferrerTypeId)
                                             .DefaultIfEmpty()
                                  where (t1.ClaimId == Autoassignment.ClaimId)
                              select new
                             {
                                 InsuredFirstName = t1.IsNugeAssignment == true ? t2.ReferrerFirstName : t1.InsuredFirstName,
                                 InsuredLastName = t1.IsNugeAssignment == true ? t2.ReferrerLastName : t1.InsuredLastName,
                                 ClaimNumber = t1.ClaimNumber
                             }).FirstOrDefault();

                    if (Autoinvoice != null)
                    {
                        invoicedetail = css.usp_AutoInvoiceGetDetails(Autoinvoice.AutoAssignmentId, Autoinvoice.AutoInvoiceId).First();
                    }
                    decimal TotalBalanceDue = css.usp_ClaimTotalBalanceDueGet(Autoassignment.ClaimId).First().Value;

                    var BatchPayment = new { InvoiceId = Autoinvoice.AutoInvoiceId, InsuredName = claims.InsuredFirstName+" "+claims.InsuredLastName, AssignmentId = Autoassignment.AutoAssignmentId, ClaimId = Autoassignment.ClaimId, claimNumber = claims.ClaimNumber, InvoiceTotal = invoicedetail.GrandTotal.HasValue ? invoicedetail.GrandTotal.Value.ToString("N2") : "0.00", InvoiceBalanceDue = invoicedetail.BalanceDue.HasValue ? invoicedetail.BalanceDue.Value.ToString("N2") : "0.00", TotalBalanceDue = TotalBalanceDue.ToString("N2") };

                    // string BatchPaymentString=javaScriptSerializer.

                    var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    jsonString = javaScriptSerializer.Serialize(BatchPayment);
                }

              
            }
            catch (Exception ex)
            {
                var BatchPayment = new { ClaimId=1};
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                jsonString = javaScriptSerializer.Serialize(BatchPayment);
                return Json(jsonString, JsonRequestBehavior.AllowGet);
            }


            return Json(jsonString, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult UploadFile()
        {



            Response.CacheControl = "no-cache";
            ClaimDocumentViewModel viewModel = new ClaimDocumentViewModel();
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            viewModel.Document.DocumentUploadedByUserId = loggedInUser.UserId;
            viewModel.Document.AssignmentId = Convert.ToInt64(Request.Form["assignmentid"]);
            viewModel.Document.DocumentTypeId = 10;
            viewModel.Document.Title = Request.Form["title"];

            HttpPostedFileBase uploadfile = Request.Files[0];

             Int64 DocumentId=UploadDocument(viewModel.Document, uploadfile);

             if (DocumentId > 0)
             {
                 return Json(DocumentId);
             }
             else
             {
                 return Json(1);
             }
            

        }
       

        public long UploadDocument(BLL.Document document, HttpPostedFileBase uploadFile)
        {
            ObjectParameter outDocumentId=null;
            if (uploadFile != null && uploadFile.ContentLength > 0)
            {
                #region Cloud Storage

                // extract only the fielname
                string fileName = new FileInfo(uploadFile.FileName).Name;
                string storeAsFileName = Regex.Replace(fileName, "[^0-9a-zA-Z-_.]+", "");
                string assignmentId = document.AssignmentId + "";
                string documentTypeId = document.DocumentTypeId + "";
                //check whether the folder with user's userid exists

                // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                string relativeFileName = assignmentId + "/" + documentTypeId + "/" + storeAsFileName;

                MemoryStream bufferStream = new System.IO.MemoryStream();
                uploadFile.InputStream.CopyTo(bufferStream);
                byte[] buffer = bufferStream.ToArray();

                string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                CloudStorageUtility.StoreFile(containerName, relativeFileName, buffer);



                #endregion

                #region Local File System

                // extract only the fielname
                //string fileName = new FileInfo(uploadFile.FileName).Name;
                //string storeAsFileName = Regex.Replace(fileName, "[^0-9a-zA-Z-_.]+", "");
                //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                //string assignmentId = document.AssignmentId + "";
                //string documentTypeId = document.DocumentTypeId + "";
                ////check whether the folder with user's userid exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId));
                //}
                ////check whether the destination folder depending on the type of document being uploaded exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId));
                //}

                //// store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                //string path = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), storeAsFileName);
                //uploadFile.SaveAs(path);

                #endregion


                outDocumentId = new ObjectParameter("DocumentId", DbType.Int64);
                css.DocumentsInsert(outDocumentId, document.AssignmentId, DateTime.Now, document.Title, fileName, storeAsFileName, document.DocumentUploadedByUserId, document.DocumentTypeId);

                if (document.DocumentTypeId == 1)
                {
                    PropertyAssignment pa = css.PropertyAssignments.Find(document.AssignmentId);
                    Claim claim = css.Claims.Find(pa.ClaimId);
                    if (pa.CSSPointofContactUserId.HasValue)
                    {
                        if (pa.CSSPointofContactUserId.Value != 0)
                        {
                            if (pa.FileStatus.HasValue)
                            {
                                if (pa.FileStatus.Value != 6)
                                {
                                    DateTime cstDateTime = css.usp_GetLocalDateTime().First().Value;
                                    ObjectParameter outDiaryId = new ObjectParameter("DiaryId", DbType.Int64);
                                    css.DiaryItemsInsert(outDiaryId, pa.AssignmentId, Convert.ToByte(ConfigurationManager.AppSettings["GenLossRptAddedClaimNotReturnedDiaryCategoryId"]), pa.CSSPointofContactUserId.Value, pa.CSSPointofContactUserId.Value, System.DateTime.Now, cstDateTime, null, null, "Estimate not yet returned", "Loss Report Document was uploaded. The claim status has not yet been changed to Estimate Returned.", (byte)1, null, true, false, false, null);
                                }
                            }
                        }
                    }
                }

                
            }
            return Convert.ToInt64(outDocumentId.Value);
        }

        public ActionResult ProcessBatchPayment(List<BatchPayment> BatchPayments)
        {
            try
            {
                if (BatchPayments.Count() > 0)
                {

                    foreach (BatchPayment batchpay in BatchPayments)
                    {
                        PaymentDetailsInsert(batchpay.InvoiceId, batchpay.ReceivedDate, batchpay.AmountReceived, batchpay.CheckNumber);

                    }

                }

            }
            catch (Exception ex)
            {
                return Json(0);
            }

          
            return Json(1);
        }
        public void PaymentDetailsInsert(Int64 InvoiceId, DateTime ReceivedDate, float AmountReceived, string CheckNumber)
        {
            string valueToReturn = "0";
            AutoInvoiceViewModel ViewModel = new AutoInvoiceViewModel();
            css = new ChoiceSolutionsEntities();
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                var Assignmentid = css.AutoInvoices.Where(x => x.AutoInvoiceId == InvoiceId).First().AutoAssignmentId;
                AutoAssignment pa = css.AutoAssignments.Find(Assignmentid);
               
                ObjectParameter outPaymentId = new ObjectParameter("PaymentId", DbType.Int64);
                css.usp_paymentInsert(outPaymentId, InvoiceId, ReceivedDate, AmountReceived, CheckNumber, loggedInUser.UserId, System.DateTime.Now);
                Int64 PaymentId = Convert.ToInt64(outPaymentId.Value);
                
                
                css.usp_balanceDueUpdate(InvoiceId, AmountReceived);
                css.usp_AutoCloseInvoice(InvoiceId);
                //ViewModel.invoiceDetail = css.usp_PropertyInvoiceGetDetails(Assignmentid, InvoiceId).First();
                //ViewModel.paymentslist = css.usp_PaymentGetList(Assignmentid).ToList();
                
                
                //Int64 taskPaymentId = PaymentId;
                //Task.Factory.StartNew(() =>
                //{
                //    Utility.QBUpdatePayment(taskPaymentId);
                //}, TaskCreationOptions.LongRunning);

                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + " " + ex.InnerException != null ? ex.InnerException.Message : "";
            }
            //return valueToReturn;
            //return PartialView("_PaymentList", ViewModel);
            //return RedirectToAction("GetPaymentDetails", new { assignmentid = ViewModel.invoiceDetail.AssignmentId});
        }

        public ActionResult DeleteClaimDocument(Int64 claimDocumentId = 0)
        {
            int valueToReturn = 0;//1-Success
            try
            {
                if (claimDocumentId != 0)
                {
                    BLL.AutoDocument claimDoc = css.AutoDocuments.Find(claimDocumentId);
                    css.DocumentsDelete(claimDocumentId);

                    //delete physical file 
                    string fileName = new FileInfo(claimDoc.AutoDocumentPath).Name;
                    #region Cloud Delete
                    string assignmentId = claimDoc.AutoAssignmentId + "";
                    string documentTypeId = claimDoc.DocumentTypeId + "";
                    string relativeFileName = assignmentId + "/" + documentTypeId + "/" + fileName;
                    string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                    CloudStorageUtility.DeleteFile(containerName, relativeFileName);
                    #endregion
                    #region local file system
                    //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                    //string assignmentId = claimDoc.AssignmentId + "";
                    //string documentTypeId = claimDoc.DocumentTypeId + "";
                    //string filePath = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), fileName);

                    //if (System.IO.File.Exists(filePath))
                    //{
                    //    try
                    //    {
                    //        System.IO.File.Delete(filePath);
                    //    }
                    //    catch (IOException ex)
                    //    {
                    //    }
                    //}
                    #endregion

                    valueToReturn = 1;
                }
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn);
        }

        public JsonResult getData(string term)
        {


            List<string> Invoices = new List<string>();

            //var invoices = (from i in css.PropertyInvoices
            //                where i.IsInvoiceClose==null && i.InvoiceNo!=null
            //                select i.InvoiceNo);

            //foreach (var i in invoices)
            //{
            //    Invoices.Add(i.ToString());
            //}
            Invoices = (from i in css.AutoInvoices
                                     where i.IsInvoiceClose == null && i.AutoInvoiceNo != null
                                     select i.AutoInvoiceNo).ToList();




           
            List<string> getValues = Invoices.Where(item => item.ToLower().Contains(term.ToLower())).ToList();
            // Return the result set as JSON
            return Json(getValues, JsonRequestBehavior.AllowGet);




            
        }


       //public JsonResult getClaimNumbers(string term)
       // {
       //     List<string> Claims = new List<string>();

       //     //Claims = (from i in css.Claims
       //     //          where i.ClaimNumber != null
       //     //          select i.ClaimNumber).ToList();

       //     Claims = (from i in css.Claims join j in css.PropertyAssignments on i.ClaimId
       //                                    equals j.ClaimId join k in css.PropertyInvoices on j.AssignmentId equals k.AssignmentId orderby i.ClaimId
                    
       //               select i.ClaimNumber).ToList();

       //     List<string> getValues = Claims.Where(item => item.ToLower().Contains(term.ToLower())).ToList();
       //     return Json(getValues, JsonRequestBehavior.AllowGet);
       // }
    }
}
