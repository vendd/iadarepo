﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ViewModels;
using BLL;
using System.Data.Objects;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using CSS.CssXactAssignmentService;
using BLL.Models;
using System.Text.RegularExpressions;
using System.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CSS.Controllers
{

    [Authorize]
    public partial class PropertyAssignmentController : CustomController
    {

        Claim Claim = new Claim();
        PropertyAssignment propertyassignment = new PropertyAssignment();

        User spUser = new User();

        public Int64 spUserid { get; set; }





        public ActionResult SetupSPPayPercentage(Int64 assignmentId, byte spPayPercentage, byte holdbackPercentage)
        {

            string valueToReturn = "0";
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                NewAssignmentController controller = new NewAssignmentController();
                controller.ControllerContext = this.ControllerContext;
                Int64? spUserId = css.PropertyAssignments.Find(assignmentId).OAUserID;
                controller.SetupSPPayPercentage(assignmentId, spUserId, spPayPercentage, holdbackPercentage);
                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + "" + ex.InnerException != null ? "  " + ex.InnerException.Message + "" : "";
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSPPayPercentage(Int64 assignmentId)
        {
            PropertyAssignment pa = css.PropertyAssignments.Find(assignmentId);
            return Json(new { SPPayPercent = pa.SPPayPercent, HoldBackPercent = pa.HoldBackPercent }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            return View();
        }


        private List<SelectListItem> getClientPointOfContactDDL(int? companyId)
        {
            List<SelectListItem> ClientPointofContactList = new List<SelectListItem>();
            ClientPointofContactList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            if (companyId.HasValue)
            {
                if (companyId != 0)
                {

                    List<GetClientPointOfContactList_Result> clientPOCList = css.GetClientPointOfContactList(companyId.Value).ToList();
                    foreach (var client in clientPOCList)
                    {
                        ClientPointofContactList.Add(new SelectListItem { Text = client.UserFullName, Value = client.UserId + "" });
                    }
                }

            }
            return ClientPointofContactList;
        }

        private List<SelectListItem> getLineOfBussinessDDL(int? companyId)
        {
            List<SelectListItem> LineOfBusinessList = new List<SelectListItem>();
            LineOfBusinessList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            if (companyId.HasValue)
            {
                if (companyId != 0)
                {
                    List<LineOfBusinessGetList_Result> LObList = css.LineOfBusinessGetList(companyId.Value).ToList();
                    foreach (var lob in LObList)
                    {
                        LineOfBusinessList.Add(new SelectListItem { Text = lob.LOBDescription, Value = lob.LOBId + "" });
                    }
                }

            }
            return LineOfBusinessList;
        }

        public ActionResult GetClientPointOfContactList(int companyId)
        {
            List<SelectListItem> ClientPointofContactList = getClientPointOfContactDDL(companyId);
            return Json(ClientPointofContactList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetLineOfBussinessList(Int32? companyId)
        {
            List<SelectListItem> LineOfBussinessList = getLineOfBussinessDDL(companyId);
            return Json(LineOfBussinessList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SubmitSuccess()
        {
            return View();
        }

        BLL.ChoiceSolutionsEntities css = new BLL.ChoiceSolutionsEntities();

        public IEnumerable<SearchClaims_Result> getAssignmentSearchResult(User searchPerformedByUser, XactAssignmentSearchViewModel viewModel)
        {
            //User loggedInUser = (User)Session["LoggedInUser"];
            IEnumerable<SearchClaims_Result> valueToReturn = null;
            Int64? headCompanyId = null;
            Int64? OAUserId = null;
            Int64? CSSQAAgentUserId = null;

            bool hasQAAgentRights = false;

            if (searchPerformedByUser != null)
            {
                List<usp_UserAssignedRightsGetList_Result> userAssignedRights = css.usp_UserAssignedRightsGetList(searchPerformedByUser.UserId).ToList();
                if (userAssignedRights.Where(x => x.UserRightId == 1).ToList().Count == 1)
                {
                    hasQAAgentRights = true;
                }

                if (searchPerformedByUser.UserTypeId.HasValue)
                {
                    if (searchPerformedByUser.UserTypeId.Value == 3)
                    {
                        headCompanyId = searchPerformedByUser.HeadCompanyId;
                    }
                    else if (searchPerformedByUser.UserTypeId.Value == 11)
                    {
                        //Client POC User can search claims assigned only to their company. 17-01-2014
                        headCompanyId = searchPerformedByUser.HeadCompanyId;
                        if (headCompanyId.HasValue)
                        {
                            viewModel.InsuranceCompany = headCompanyId.Value;
                        }
                    }
                    else if (searchPerformedByUser.UserTypeId.Value == 1 && !hasQAAgentRights && viewModel.SearchMode == BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.GENERAL)
                    {
                        OAUserId = searchPerformedByUser.UserId;
                    }
                    else if (searchPerformedByUser.UserTypeId.Value == 8 || hasQAAgentRights)
                    {
                        CSSQAAgentUserId = searchPerformedByUser.UserId;
                    }
                }
                if (CSSQAAgentUserId > 0 && viewModel.FileStatus == 6)
                {
                    CSSQAAgentUserId = null;
                    viewModel.OrderByField = "Returned";
                    viewModel.OrderDirection = false;
                }
                else
                {
                    //kept in else for better readibility
                    CSSQAAgentUserId = null;
                }
                if (viewModel.CSSQAAgentUserId != 0)
                {
                    CSSQAAgentUserId = viewModel.CSSQAAgentUserId;
                }
                if (viewModel.DateFrom == DateTime.MinValue)
                {
                    viewModel.DateFrom = Convert.ToDateTime(null);
                }
                if (viewModel.DateTo == DateTime.MinValue)
                {
                    viewModel.DateTo = Convert.ToDateTime(null); ;
                }

            }
            //valueToReturn = css.SearchClaims(viewModel.InsuranceCompany, OAUserId, viewModel.CSSQAAgentUserId, viewModel.ClaimNumber, viewModel.PolicyNumber, viewModel.CatCode, viewModel.TransactionId, viewModel.Zip, viewModel.State, viewModel.LossType, viewModel.WorkFlowStatus, viewModel.FileStatus, viewModel.JobType, viewModel.City, viewModel.LastName, Convert.ToInt32(viewModel.Distance));
            valueToReturn = css.SearchClaims(viewModel.InsuranceCompany, OAUserId, viewModel.CSSPOCUserId, CSSQAAgentUserId, viewModel.ClaimNumber, viewModel.PolicyNumber, viewModel.CatCode, viewModel.Zip, viewModel.State, viewModel.LossType, Convert.ToByte(viewModel.FileStatus), viewModel.City, viewModel.LastName, Convert.ToInt32(viewModel.Distance), viewModel.OrderDirection, viewModel.OrderByField, viewModel.Pager.Page, viewModel.Pager.RecsPerPage, viewModel.DateFrom, viewModel.DateTo, searchPerformedByUser.UserId, viewModel.IsTriageAvailable, viewModel.DateReceivedFrom, viewModel.DateReceivedTo, viewModel.SPName, viewModel.InvoiceNumber,viewModel.SearchOrExport,viewModel.InsureName);
            return valueToReturn;
        }

        public ActionResult Accepted(Int64 AssignmentId, bool IsAccepted, Int64 claimid)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            css.PropertyAssignmentIsAccepted(AssignmentId, 15, IsAccepted, loggedInUser.UserId);
            return RedirectToAction("Details", new { claimid = Cypher.EncryptString(claimid.ToString()) });
        }


        public ActionResult QAAgentReviewQueue()
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            XactAssignmentSearchViewModel viewModel = new XactAssignmentSearchViewModel();
            viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE;
            viewModel.FileStatus = 6;

            Session["XactAssignmentSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));

            return RedirectToAction("Search", new { @type = "QAAgent_Review" });
        }


        public ActionResult Search(string type = "")
        {
            XactAssignmentSearchViewModel viewModel = new XactAssignmentSearchViewModel();
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            try
            {
                //if (loggedInUser.UserTypeId == 1 || loggedInUser.UserTypeId == 2 || loggedInUser.UserTypeId == 3 || loggedInUser.UserTypeId == 8 || loggedInUser.UserTypeId == 11)
                //{

                    if (Session["XactAssignmentSearch"] != null)
                    {
                    viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["XactAssignmentSearch"].ToString());
                    //viewModel = (XactAssignmentSearchViewModel)Session["XactAssignmentSearch"];
                    Session["XactAssignmentSearch"] = null;
                }
                else
                {
                    viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.GENERAL;
                }
                if (loggedInUser.UserTypeId == 11)
                {
                    //Client POC should only see their company. 17-01-2014
                    if (loggedInUser.HeadCompanyId.HasValue)
                    {
                        viewModel.InsuranceCompanies.RemoveAll(x => x.Value != loggedInUser.HeadCompanyId + "");
                    }
                    else
                    {
                        viewModel.InsuranceCompanies.RemoveAll(x => x.Value != "0");
                    }
                }
                if (!String.IsNullOrEmpty(type))
                {
                    if (type == "QAAgent_Review")
                    {
                        viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE;
                            bool hasright = loggedInUser.AssignedRights.Where(x => x == 1).ToList().Count == 1 ? true : false;
                            //if ((loggedInUser.UserTypeId != 2 && loggedInUser.UserTypeId != 8) && hasright == false)
                            //{
                            //    return View("AuthorizationError");
                            //}
                        }
                    else
                    {
                        if (viewModel.SearchMode == XactAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE)
                        {
                            //Reset the file status to select incase the user is coming back to claim search from QA Agent review queue
                            viewModel.FileStatus = 0;
                        }
                        viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.GENERAL;
                    }
                }
                if (viewModel.Pager == null)
                {
                    viewModel.Pager = new BLL.Models.Pager();
                    viewModel.Pager.Page = 1;
                    viewModel.Pager.RecsPerPage = 20;
                    viewModel.Pager.FirstPageNo = 1;
                    viewModel.Pager.IsAjax = false;
                    viewModel.Pager.FormName = "formSearch";
                }
                viewModel.ClaimsInfo = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();

                viewModel.Pager.TotalCount = viewModel.ClaimsInfo[0].TotalRecords.ToString();
                //Session["XactAssignmentSearch"] = null;

                viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
                    Session["AuthorizeSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                //}
                //else
                //{
                //    return View("AuthorizationError");
                //}
            }
            catch (Exception ex)
            {
                string abc = ex.Message;
            }
            return View(viewModel);
        }


        [HttpPost]
        public ActionResult Search(XactAssignmentSearchViewModel viewModel, FormCollection form, string SubmitButton = "Search")
        {

            try
            {
                viewModel.Pager = new BLL.Models.Pager();
                viewModel.Pager.IsAjax = false;
                viewModel.Pager.FormName = "formSearch";
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                //form["hdnOrderDirection"] = viewModel.OrderDirection;
                if ((form["hdnCurrentPage"]) != "" && (form["hdnCurrentPage"]) != null)
                {

                    viewModel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);

                }
                else
                {

                    viewModel.Pager.Page = 1;
                }

                viewModel.Pager.RecsPerPage = 20;
                if (form["hdnstartPage"] != null)
                {
                    if (!String.IsNullOrEmpty(form["hdnstartPage"]))
                    {
                        viewModel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
                    }
                    else
                    {
                        viewModel.Pager.FirstPageNo = 1;
                    }
                }

                if ((form["hdnFlagClaim"] == "0" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"]) || (form["hdnlastOrderByValue"] != form["hdnOrderByValue"]) && (form["hdnOrderByValue"] != ""))
                {
                    viewModel.OrderDirection = true;
                    form["hdnFlagClaim"] = "1";
                }
                else if (form["hdnFlagClaim"] == "1" && form["hdnlastOrderByValue"] == form["hdnOrderByValue"])
                {
                    viewModel.OrderDirection = false;
                    form["hdnFlagClaim"] = "0";
                }

                viewModel.OrderByField = form["hdnOrderByValue"];

                //if a claim number is specified clear all other filters. JIRA Issue OPT 70
                if (!String.IsNullOrEmpty(viewModel.ClaimNumber))
                {
                    viewModel.PolicyNumber = null;
                    viewModel.LastName = null;
                    viewModel.City = null;
                    viewModel.Zip = null;
                    viewModel.Distance = null;
                    viewModel.State = "0";
                    viewModel.CatCode = null;
                    viewModel.LossType = "0";
                    viewModel.FileStatus = 0;
                    viewModel.DateFrom = null;
                    viewModel.DateTo = null;
                    viewModel.InsuranceCompany = 0;
                    viewModel.CSSPOCUserId = 0;
                    viewModel.CSSQAAgentUserId = 0;
                    viewModel.DateReceivedFrom = null;
                    viewModel.DateReceivedTo = null;
                    viewModel.IsTriageAvailable = false;
                    viewModel.SPName = null;
                }
                // Gaurav 24-04-2015 it is used to get records based on export or search performed by user and get all records in case of export and not perform paging
                if (SubmitButton == "Search")
                {
                    viewModel.SearchOrExport = 0;
                    viewModel.ClaimsInfo = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();

                }
                else
                {
                    viewModel.SearchOrExport = 1;
                    viewModel.ClaimsInfo = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();


                    GridView gv = new GridView();

                    var data = from p in viewModel.ClaimsInfo.ToList()
                               select new
                               {
                                   Claim_Number = p.ClaimNumber,
                                   RCV = p.RCV,
                                   Service_Provider = p.OAName,
                                   Insured_Name = p.InsuredName,
                                   Status = p.FileStatus,
                                   CSS_Point_Of_Contact = p.CSSPOCUserUserFullName,
                                   QA_Agent = p.QaAgentUserFullName,
                                   Client_Point_Of_Contact = p.ClientPOCUserFullName,
                                   Company_Name = p.Company,
                                   LOB = p.LobDescription,
                                   Days_Old = p.OlDDays,
                                   InvoiceYesNo = p.InvoiceId != null ? "Y" : "N",
                                   InvoiceAmount = p.GrandTotal


                               };

                    gv.DataSource = data.ToList();
                    gv.DataBind();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment; filename=ClaimList.xls");
                    Response.ContentType = "application/ms-excel";
                    //Response.Charset = "";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    gv.RenderControl(htw);
                    Response.Write(sw.ToString());
                    //Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();


                }


                viewModel.Pager.TotalCount = viewModel.ClaimsInfo.Last().TotalRecords.ToString();  // getAssignmentSearchResult(loggedInUser, viewModel).Count().ToString();


                if (Convert.ToInt32(viewModel.Pager.TotalCount) % Convert.ToInt32(viewModel.Pager.RecsPerPage) != 0)
                {
                    viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
                }
                else
                {
                    viewModel.Pager.NoOfPages = ((Convert.ToInt32(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage))).ToString();
                }

                ViewBag.LastOrderByValue = form["hdnOrderByValue"];
                ViewBag.flag = form["hdnFlagClaim"];
                form["hdnOrderByValue"] = "";

                //Session["XactAssignmentSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));
                Session["AuthorizeSearch"] = (new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(viewModel));


            }
            catch (Exception ex)
            {
            }

            return View(viewModel);
        }


        public ActionResult Details(string claimid, string ActiveMainTab = "Details")
        {

            Int64 ClaimID = 0; 
            ClaimID = Convert.ToInt64(Cypher.DecryptString(Cypher.ReplaceCharcters(claimid)));
            
            XACTClaimsInfoViewModel claiminfoviewModel = new XACTClaimsInfoViewModel(ClaimID);
            claiminfoviewModel.ActiveMainTab = ActiveMainTab;
            //if (Session["AuthorizeSearch"] != null)
            //{
            //    XactAssignmentSearchViewModel viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["AuthorizeSearch"].ToString());
            //    int claimcnt = 0;
            //    claimcnt = viewModel.ClaimsInfo.Where(x => x.ClaimId == ClaimID).Count();
            //    if (viewModel.ClaimsInfo.Where(x => x.ClaimId == ClaimID).Count() <= 0)
            //    {

            //        return View("AuthorizationError");
            //    }


            //}
            //else
            //{
            //    return View("AuthorizationError");
            //}
            return View(claiminfoviewModel);
        }

        [HttpPost]
        public ActionResult SubmitNotes(Int64 assignmentid, string Subject, string Comment, bool Isprivate, bool isBillable, double? noOfHours)
        {
            CSS.Models.User user = CSS.AuthenticationUtility.GetUser();
            int NoteId;
            Int64 claimid = 0;
            ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
            NoteId = css.usp_NotesInsert(outNoteId, assignmentid, (Subject == null ? "" : Subject), (Comment == null ? "" : Comment), DateTime.Now, Isprivate, false, user.UserId, null, null, null, null,null,null);
            css.usp_NoteUpdate(Convert.ToInt64(outNoteId.Value), isBillable, noOfHours);
            return Json(1);
        }

        public ActionResult DeleteClaimDiariesDetails(Int64 DiaryId)
        {
            if (DiaryId != -1 && DiaryId != null)
            {
                css.DiaryItemsDelete(DiaryId);
            }
            return Json(1);
        }

        public ActionResult ApproveClaimDiariesDetails(Int64 DiaryId)
        {
            if (DiaryId != -1 && DiaryId != null)
            {
                css.ApproveStatusDiaryItems(DiaryId);
            }
            return Json(1);
        }

        public void UploadDocument(BLL.Document document, HttpPostedFileBase uploadFile)
        {
            if (uploadFile != null && uploadFile.ContentLength > 0)
            {
                #region Cloud Storage

                // extract only the fielname
                string fileName = new FileInfo(uploadFile.FileName).Name;
                string storeAsFileName = Regex.Replace(fileName, "[^0-9a-zA-Z-_.]+", "");
                string assignmentId = document.AssignmentId + "";
                string documentTypeId = document.DocumentTypeId + "";
                //check whether the folder with user's userid exists

                // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                string relativeFileName = assignmentId + "/" + documentTypeId + "/" + storeAsFileName;

                MemoryStream bufferStream = new System.IO.MemoryStream();
                uploadFile.InputStream.CopyTo(bufferStream);
                byte[] buffer = bufferStream.ToArray();

                string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                CloudStorageUtility.StoreFile(containerName, relativeFileName, buffer);



                #endregion

                #region Local File System

                //// extract only the fielname
                //string fileName = new FileInfo(uploadFile.FileName).Name;
                //string storeAsFileName = Regex.Replace(fileName, "[^0-9a-zA-Z-_.]+", "");
                //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                //string assignmentId = document.AssignmentId + "";
                //string documentTypeId = document.DocumentTypeId + "";
                ////check whether the folder with user's userid exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId));
                //}
                ////check whether the destination folder depending on the type of document being uploaded exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId));
                //}

                //// store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                //string path = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), storeAsFileName);
                //uploadFile.SaveAs(path);

                #endregion


                ObjectParameter outDocumentId = new ObjectParameter("DocumentId", DbType.Int64);
                css.DocumentsInsert(outDocumentId, document.AssignmentId, DateTime.Now, document.Title, fileName, storeAsFileName, document.DocumentUploadedByUserId, document.DocumentTypeId);

                if (document.DocumentTypeId == 1)
                {
                    PropertyAssignment pa = css.PropertyAssignments.Find(document.AssignmentId);
                    Claim claim = css.Claims.Find(pa.ClaimId);
                    if (pa.CSSPointofContactUserId.HasValue)
                    {
                        if (pa.CSSPointofContactUserId.Value != 0)
                        {
                            if (pa.FileStatus.HasValue)
                            {
                                if (pa.FileStatus.Value != 6)
                                {
                                    DateTime cstDateTime = css.usp_GetLocalDateTime().First().Value;
                                    ObjectParameter outDiaryId = new ObjectParameter("DiaryId", DbType.Int64);
                                    css.DiaryItemsInsert(outDiaryId, pa.AssignmentId, Convert.ToByte(ConfigurationManager.AppSettings["GenLossRptAddedClaimNotReturnedDiaryCategoryId"]), pa.CSSPointofContactUserId.Value, pa.CSSPointofContactUserId.Value, System.DateTime.Now, cstDateTime, null, null, "Estimate not yet returned", "Loss Report Document was uploaded. The claim status has not yet been changed to Estimate Returned.", (byte)1, null, true, false, false, null);
                                }
                            }
                        }
                    }
                }
            }
        }

        public ActionResult SubmitClaimDiaryItems(Int64 diaryid)
        {
            //XACTClaimsInfoViewModel viewmodel = new XACTClaimsInfoViewModel();
            //BLL.User user = (BLL.User)Session["LoggedInUser"];

            //ObjectParameter outDiaryId = new ObjectParameter("DiaryId", DbType.Int64);
            //Int64 DiaryId = css.DiaryItemsInsert(outDiaryId, assignmentid, DiaryCategoryId, AssigntoUserId, user.UserId, System.DateTime.Now, DueDate, null, null, Title, DiaryDesc, 0, null, null);
            return Json(1);
        }

        public ActionResult Requierments(Int64 assignmentId, bool? isReadOnly = false)
        {
            RequirementViewModel viewModel = new RequirementViewModel();
            viewModel.AssignmentId = assignmentId;

            Int64 claimId = css.PropertyAssignments.Find(assignmentId).ClaimId.Value;
            Claim claim = css.Claims.Find(claimId);
            List<ClaimRequirement> lstClmRequirements = css.ClaimRequirements.Where(a => (a.AssignmentId == assignmentId) && (a.HeadCompanyId == null) && (a.LOBId == null)).ToList();
            if (lstClmRequirements.Count > 0)
            {
                ClaimRequirement Requirementdetails = lstClmRequirements[0];
                viewModel.ClaimRequirementId = Convert.ToInt64(Requirementdetails.ClaimRequirementId);

                if (Requirementdetails.AssignmentId.HasValue)
                {
                    viewModel.AssignmentId = Convert.ToInt32(Requirementdetails.AssignmentId);
                }

                if (Requirementdetails.SoftwareRequired.HasValue)
                {
                    viewModel.SoftwareRequired = Convert.ToInt32(Requirementdetails.SoftwareRequired);
                }

                if (Requirementdetails.ContactWithin.HasValue)
                {
                    viewModel.ContactWithin = Convert.ToInt32(Requirementdetails.ContactWithin);
                }


                if (Requirementdetails.InspectWithin.HasValue)
                {
                    viewModel.InspectWithin = Convert.ToInt32(Requirementdetails.InspectWithin);
                }


                if (Requirementdetails.FirstReportDue.HasValue)
                {
                    viewModel.FirstReportDue = Convert.ToInt32(Requirementdetails.FirstReportDue);
                }


                if (Requirementdetails.ReservesDue.HasValue)
                {
                    viewModel.ReservesDue = Convert.ToInt32(Requirementdetails.ReservesDue);
                }


                if (Requirementdetails.SatusReports.HasValue)
                {
                    viewModel.SatusReports = Convert.ToInt32(Requirementdetails.SatusReports);
                }


                if (Requirementdetails.FinalReportDue.HasValue)
                {
                    viewModel.FinalReportDue = Convert.ToInt32(Requirementdetails.FinalReportDue);
                }


                if (Requirementdetails.BaseServiceCharges.HasValue)
                {
                    viewModel.BaseServiceCharges = Convert.ToInt32(Requirementdetails.BaseServiceCharges);
                }


                if (Requirementdetails.InsuredToValueRequired.HasValue)
                {
                    viewModel.InsuredToValueRequired = Convert.ToInt32(Requirementdetails.InsuredToValueRequired);
                }


                if (Requirementdetails.NeighborhoodCanvas.HasValue)
                {
                    viewModel.NeighborhoodCanvas = Convert.ToInt32(Requirementdetails.NeighborhoodCanvas);
                }


                if (Requirementdetails.ITELRequired.HasValue)
                {
                    viewModel.ITELRequired = Convert.ToInt32(Requirementdetails.ITELRequired);
                }


                if (Requirementdetails.MinimumCharges.HasValue)
                {
                    viewModel.MinimumCharges = Convert.ToInt32(Requirementdetails.MinimumCharges);
                }


                if (Requirementdetails.RequiredDocuments != null)
                {
                    viewModel.RequiredDocuments = Requirementdetails.RequiredDocuments;
                }


                if (Requirementdetails.EstimateContents.HasValue)
                {
                    viewModel.EstimateContents = Convert.ToInt32(Requirementdetails.EstimateContents);
                }


                if (Requirementdetails.ObtainAgreedSorP.HasValue)
                {
                    viewModel.ObtainAgreedSorP = Convert.ToInt32(Requirementdetails.ObtainAgreedSorP);
                }


                if (Requirementdetails.DiscussCoveragNScopewithClaimant.HasValue)
                {
                    viewModel.DiscussCoveragNScopewithClaimant = Convert.ToInt32(Requirementdetails.DiscussCoveragNScopewithClaimant);
                }


                if (Requirementdetails.RoofSketchRequired.HasValue)
                {
                    viewModel.RoofSketchRequired = Convert.ToInt32(Requirementdetails.RoofSketchRequired);
                }


                if (Requirementdetails.PhotoSGorPGRequired.HasValue)
                {
                    viewModel.PhotoSGorPGRequired = Convert.ToInt32(Requirementdetails.PhotoSGorPGRequired);
                }


                if (Requirementdetails.DepreciacionType.HasValue)
                {
                    viewModel.DepreciacionType = Convert.ToInt32(Requirementdetails.DepreciacionType);
                }


                if (Requirementdetails.MaxDepr.HasValue)
                {
                    viewModel.MaxDepr = Convert.ToInt32(Requirementdetails.MaxDepr);
                }


                if (Requirementdetails.AppOfOveheadNProfit.HasValue)
                {
                    viewModel.AppOfOveheadNProfit = Convert.ToInt32(Requirementdetails.AppOfOveheadNProfit);
                }


                if (Requirementdetails.Comments != null)
                {
                    viewModel.Comments = Requirementdetails.Comments;
                }

                //set detauils
            }
            else
            {

                bool fetchCompanyRequirements = true;
                if (claim.LOBId.HasValue)
                {
                    //identify whether lob requirements are specified
                    if (css.ClaimRequirements.Where(a => (a.HeadCompanyId == null) && (a.AssignmentId == null) && (a.LOBId == claim.LOBId.Value)).ToList().Count == 1)
                    {
                        viewModel = getDefaultRequirementsViewModel(null, claim.LOBId.Value);
                        fetchCompanyRequirements = false;
                    }

                }
                if (fetchCompanyRequirements)
                {

                    if (claim.HeadCompanyId.HasValue)
                    {
                        //identify whether company requirements are specified
                        if (css.ClaimRequirements.Where(a => (a.HeadCompanyId == claim.HeadCompanyId.Value) && (a.AssignmentId == null) && (a.LOBId == null)).ToList().Count == 1)
                        {
                            viewModel = getDefaultRequirementsViewModel(claim.HeadCompanyId.Value, null);
                        }
                    }
                }


            }

            viewModel.isReadOnly = isReadOnly.Value;
            return Json(RenderPartialViewToString("_Requirements", viewModel), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]

        public ActionResult Requirements(RequirementViewModel ViewModel, FormCollection Form)
        {

            try
            {
                css.usp_insertClaimRequirements(ViewModel.ClaimRequirementId, null, null, ViewModel.AssignmentId, ViewModel.SoftwareRequired, ViewModel.ContactWithin,
                    ViewModel.InspectWithin, ViewModel.FirstReportDue, ViewModel.ReservesDue, ViewModel.SatusReports, ViewModel.FinalReportDue,
                    ViewModel.BaseServiceCharges, ViewModel.InsuredToValueRequired, ViewModel.NeighborhoodCanvas, ViewModel.ITELRequired, ViewModel.MinimumCharges,
                    ViewModel.RequiredDocuments, ViewModel.EstimateContents, ViewModel.ObtainAgreedSorP, ViewModel.DiscussCoveragNScopewithClaimant,
                    ViewModel.RoofSketchRequired, ViewModel.PhotoSGorPGRequired, ViewModel.DepreciacionType, ViewModel.MaxDepr, ViewModel.AppOfOveheadNProfit,
                    ViewModel.Comments);
                ViewModel.status = "Data saved Successfully";
            }
            catch (System.Exception ex)
            {
                ViewModel.status = "Some error occoured please try again";

            }
            return Json(RenderPartialViewToString("_Requirements", ViewModel), JsonRequestBehavior.AllowGet);
            //return View(ViewModel);
        }
        public ActionResult RequiermentsMail(Int64 assignmentId, bool? isReadOnly = false)
        {
            RequirementViewModel viewModel = new RequirementViewModel();
            viewModel.AssignmentId = assignmentId;

            Int64 claimId = css.PropertyAssignments.Find(assignmentId).ClaimId.Value;
            Claim claim = css.Claims.Find(claimId);
            List<ClaimRequirement> lstClmRequirements = css.ClaimRequirements.Where(a => (a.AssignmentId == assignmentId) && (a.HeadCompanyId == null) && (a.LOBId == null)).ToList();
            if (lstClmRequirements.Count > 0)
            {
                ClaimRequirement Requirementdetails = lstClmRequirements[0];
                viewModel.ClaimRequirementId = Convert.ToInt64(Requirementdetails.ClaimRequirementId);

                if (Requirementdetails.AssignmentId.HasValue)
                {
                    viewModel.AssignmentId = Convert.ToInt32(Requirementdetails.AssignmentId);
                }

                if (Requirementdetails.SoftwareRequired.HasValue)
                {
                    viewModel.SoftwareRequired = Convert.ToInt32(Requirementdetails.SoftwareRequired);
                }

                if (Requirementdetails.ContactWithin.HasValue)
                {
                    viewModel.ContactWithin = Convert.ToInt32(Requirementdetails.ContactWithin);
                }


                if (Requirementdetails.InspectWithin.HasValue)
                {
                    viewModel.InspectWithin = Convert.ToInt32(Requirementdetails.InspectWithin);
                }


                if (Requirementdetails.FirstReportDue.HasValue)
                {
                    viewModel.FirstReportDue = Convert.ToInt32(Requirementdetails.FirstReportDue);
                }


                if (Requirementdetails.ReservesDue.HasValue)
                {
                    viewModel.ReservesDue = Convert.ToInt32(Requirementdetails.ReservesDue);
                }


                if (Requirementdetails.SatusReports.HasValue)
                {
                    viewModel.SatusReports = Convert.ToInt32(Requirementdetails.SatusReports);
                }


                if (Requirementdetails.FinalReportDue.HasValue)
                {
                    viewModel.FinalReportDue = Convert.ToInt32(Requirementdetails.FinalReportDue);
                }


                if (Requirementdetails.BaseServiceCharges.HasValue)
                {
                    viewModel.BaseServiceCharges = Convert.ToInt32(Requirementdetails.BaseServiceCharges);
                }


                if (Requirementdetails.InsuredToValueRequired.HasValue)
                {
                    viewModel.InsuredToValueRequired = Convert.ToInt32(Requirementdetails.InsuredToValueRequired);
                }


                if (Requirementdetails.NeighborhoodCanvas.HasValue)
                {
                    viewModel.NeighborhoodCanvas = Convert.ToInt32(Requirementdetails.NeighborhoodCanvas);
                }


                if (Requirementdetails.ITELRequired.HasValue)
                {
                    viewModel.ITELRequired = Convert.ToInt32(Requirementdetails.ITELRequired);
                }


                if (Requirementdetails.MinimumCharges.HasValue)
                {
                    viewModel.MinimumCharges = Convert.ToInt32(Requirementdetails.MinimumCharges);
                }


                if (Requirementdetails.RequiredDocuments != null)
                {
                    viewModel.RequiredDocuments = Requirementdetails.RequiredDocuments;
                }


                if (Requirementdetails.EstimateContents.HasValue)
                {
                    viewModel.EstimateContents = Convert.ToInt32(Requirementdetails.EstimateContents);
                }


                if (Requirementdetails.ObtainAgreedSorP.HasValue)
                {
                    viewModel.ObtainAgreedSorP = Convert.ToInt32(Requirementdetails.ObtainAgreedSorP);
                }


                if (Requirementdetails.DiscussCoveragNScopewithClaimant.HasValue)
                {
                    viewModel.DiscussCoveragNScopewithClaimant = Convert.ToInt32(Requirementdetails.DiscussCoveragNScopewithClaimant);
                }


                if (Requirementdetails.RoofSketchRequired.HasValue)
                {
                    viewModel.RoofSketchRequired = Convert.ToInt32(Requirementdetails.RoofSketchRequired);
                }


                if (Requirementdetails.PhotoSGorPGRequired.HasValue)
                {
                    viewModel.PhotoSGorPGRequired = Convert.ToInt32(Requirementdetails.PhotoSGorPGRequired);
                }


                if (Requirementdetails.DepreciacionType.HasValue)
                {
                    viewModel.DepreciacionType = Convert.ToInt32(Requirementdetails.DepreciacionType);
                }


                if (Requirementdetails.MaxDepr.HasValue)
                {
                    viewModel.MaxDepr = Convert.ToInt32(Requirementdetails.MaxDepr);
                }


                if (Requirementdetails.AppOfOveheadNProfit.HasValue)
                {
                    viewModel.AppOfOveheadNProfit = Convert.ToInt32(Requirementdetails.AppOfOveheadNProfit);
                }


                if (Requirementdetails.Comments != null)
                {
                    viewModel.Comments = Requirementdetails.Comments;
                }

                //set detauils
            }
            else
            {

                bool fetchCompanyRequirements = true;
                if (claim.LOBId.HasValue)
                {
                    //identify whether lob requirements are specified
                    if (css.ClaimRequirements.Where(a => (a.HeadCompanyId == null) && (a.AssignmentId == null) && (a.LOBId == claim.LOBId.Value)).ToList().Count == 1)
                    {
                        viewModel = getDefaultRequirementsViewModel(null, claim.LOBId.Value);

                        fetchCompanyRequirements = false;
                    }

                }
                if (fetchCompanyRequirements)
                {

                    if (claim.HeadCompanyId.HasValue)
                    {
                        //identify whether company requirements are specified
                        if (css.ClaimRequirements.Where(a => (a.HeadCompanyId == claim.HeadCompanyId.Value) && (a.AssignmentId == null) && (a.LOBId == null)).ToList().Count == 1)
                        {
                            viewModel = getDefaultRequirementsViewModel(claim.HeadCompanyId.Value, null);
                        }
                    }
                }


            }

            viewModel.isReadOnly = isReadOnly.Value;
            return Json(RenderPartialViewToString("_RequirementsMail", viewModel), JsonRequestBehavior.AllowGet);
        }

        private RequirementViewModel getDefaultRequirementsViewModel(int? companyId, int? LOBId)
        {
            //Display Default Company level Requirements
            RequirementViewModel viewModel = new RequirementViewModel();
            List<ClaimRequirement> lstClmRequirements = null;
            if (companyId.HasValue)
            {
                lstClmRequirements = css.ClaimRequirements.Where(a => (a.HeadCompanyId == companyId.Value) && (a.AssignmentId == null) && (a.LOBId == null)).ToList();
            }
            else if (LOBId.HasValue)
            {
                lstClmRequirements = css.ClaimRequirements.Where(a => (a.HeadCompanyId == null) && (a.AssignmentId == null) && (a.LOBId == LOBId.Value)).ToList();
            }
            if (lstClmRequirements.Count > 0)
            {
                ClaimRequirement Requirementdetails = lstClmRequirements[0];

                if (Requirementdetails.SoftwareRequired.HasValue)
                {
                    viewModel.SoftwareRequired = Convert.ToInt32(Requirementdetails.SoftwareRequired);
                }
                if (Requirementdetails.ContactWithin.HasValue)
                {
                    viewModel.ContactWithin = Convert.ToInt32(Requirementdetails.ContactWithin);
                }
                if (Requirementdetails.InspectWithin.HasValue)
                {
                    viewModel.InspectWithin = Convert.ToInt32(Requirementdetails.InspectWithin);
                }
                if (Requirementdetails.FirstReportDue.HasValue)
                {
                    viewModel.FirstReportDue = Convert.ToInt32(Requirementdetails.FirstReportDue);
                }
                if (Requirementdetails.ReservesDue.HasValue)
                {
                    viewModel.ReservesDue = Convert.ToInt32(Requirementdetails.ReservesDue);
                }
                if (Requirementdetails.SatusReports.HasValue)
                {
                    viewModel.SatusReports = Convert.ToInt32(Requirementdetails.SatusReports);
                }
                if (Requirementdetails.FinalReportDue.HasValue)
                {
                    viewModel.FinalReportDue = Convert.ToInt32(Requirementdetails.FinalReportDue);
                }
                if (Requirementdetails.BaseServiceCharges.HasValue)
                {
                    viewModel.BaseServiceCharges = Convert.ToInt32(Requirementdetails.BaseServiceCharges);
                }
                if (Requirementdetails.InsuredToValueRequired.HasValue)
                {
                    viewModel.InsuredToValueRequired = Convert.ToInt32(Requirementdetails.InsuredToValueRequired);
                }
                if (Requirementdetails.NeighborhoodCanvas.HasValue)
                {
                    viewModel.NeighborhoodCanvas = Convert.ToInt32(Requirementdetails.NeighborhoodCanvas);
                }
                if (Requirementdetails.ITELRequired.HasValue)
                {
                    viewModel.ITELRequired = Convert.ToInt32(Requirementdetails.ITELRequired);
                }
                if (Requirementdetails.MinimumCharges.HasValue)
                {
                    viewModel.MinimumCharges = Convert.ToInt32(Requirementdetails.MinimumCharges);
                }
                if (Requirementdetails.RequiredDocuments != null)
                {
                    viewModel.RequiredDocuments = Requirementdetails.RequiredDocuments;
                }
                if (Requirementdetails.EstimateContents.HasValue)
                {
                    viewModel.EstimateContents = Convert.ToInt32(Requirementdetails.EstimateContents);
                }
                if (Requirementdetails.ObtainAgreedSorP.HasValue)
                {
                    viewModel.ObtainAgreedSorP = Convert.ToInt32(Requirementdetails.ObtainAgreedSorP);
                }
                if (Requirementdetails.DiscussCoveragNScopewithClaimant.HasValue)
                {
                    viewModel.DiscussCoveragNScopewithClaimant = Convert.ToInt32(Requirementdetails.DiscussCoveragNScopewithClaimant);
                }
                if (Requirementdetails.RoofSketchRequired.HasValue)
                {
                    viewModel.RoofSketchRequired = Convert.ToInt32(Requirementdetails.RoofSketchRequired);
                }
                if (Requirementdetails.PhotoSGorPGRequired.HasValue)
                {
                    viewModel.PhotoSGorPGRequired = Convert.ToInt32(Requirementdetails.PhotoSGorPGRequired);
                }
                if (Requirementdetails.DepreciacionType.HasValue)
                {
                    viewModel.DepreciacionType = Convert.ToInt32(Requirementdetails.DepreciacionType);
                }
                if (Requirementdetails.MaxDepr.HasValue)
                {
                    viewModel.MaxDepr = Convert.ToInt32(Requirementdetails.MaxDepr);
                }
                if (Requirementdetails.AppOfOveheadNProfit.HasValue)
                {
                    viewModel.AppOfOveheadNProfit = Convert.ToInt32(Requirementdetails.AppOfOveheadNProfit);
                }
                if (Requirementdetails.Comments != null)
                {
                    viewModel.Comments = Requirementdetails.Comments;
                }
            }
            return viewModel;
        }

        //public List<SelectListItem> LineOfBusinessList = new List<SelectListItem>();
        public ActionResult MapPropertyAssignment(Int64 Claimid = -1, Int64 SPID = -1)
        {

            //CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            //BLL.User LoggedInUser = css.Users.Find(loggedInUser.UserId);
            //PropertyAssignmentViewModel viewModel = new PropertyAssignmentViewModel();
            PropertyAssignmentViewModel viewModel;
            if (Claimid == -1)
            {
                viewModel = new PropertyAssignmentViewModel();
                viewModel.UseMasterPageLayout = true;
                viewModel.propertyassignment.FileStatus = 2;//Default to Status Open
                viewModel.Claim.ServiceOffering = 9;
                viewModel.Claim.IsSingleDeductible = false;//Default to SingeDeductible
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                BLL.User LoggedInUser = css.Users.Find(loggedInUser.UserId);

                if (LoggedInUser.UserTypeId.HasValue)
                {
                    //If the logged user is a Client POC then display only their Company --17-01-2014
                    if (LoggedInUser.UserTypeId == 11)
                    {
                        if (LoggedInUser.HeadCompanyId.HasValue)
                        {
                            viewModel.InsuranceCompaniesList.RemoveAll(x => x.Value != LoggedInUser.HeadCompanyId.Value + "");
                        }
                        else
                        {
                            viewModel.InsuranceCompaniesList.RemoveAll(x => x.Value != "0");
                        }
                    }

                    //If the logged user is a Claims Representative, automatically select the Insurance Company
                    if (LoggedInUser.UserTypeId != 2)
                    {
                        if (LoggedInUser.HeadCompanyId.HasValue)
                        {
                            foreach (var company in viewModel.InsuranceCompaniesList)
                            {
                                if (company.Value == LoggedInUser.HeadCompanyId + "")
                                {
                                    viewModel.Claim.HeadCompanyId = LoggedInUser.HeadCompanyId;
                                    company.Selected = true;
                                    break;
                                }
                            }
                            viewModel.ClientPointofContactList = getClientPointOfContactDDL(LoggedInUser.HeadCompanyId);
                            foreach (var client in viewModel.ClientPointofContactList)
                            {
                                if (client.Value == LoggedInUser.UserId + "")
                                {
                                    client.Selected = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            else
            {

                viewModel = new PropertyAssignmentViewModel(Claimid);
                viewModel.ClientPointofContactList = getClientPointOfContactDDL(viewModel.Claim.HeadCompanyId);
                viewModel.UseMasterPageLayout = false;

                //Gaurav 13-04-2015 assign clientpoc according to externalidentifier mapping for FTPclaims.


            }

            List<LineOfBusinessGetList_Result> LObList = css.LineOfBusinessGetList(viewModel.Claim.HeadCompanyId).ToList();
            foreach (var lob in LObList)
            {
                viewModel.LineOfBusinessList.Add(new SelectListItem { Text = lob.LOBDescription, Value = lob.LOBId + "" });
            }

            return PartialView("_PropertyAssignment", viewModel);
        }
      

        public ActionResult PropertyAssignment(Int64 claimid = -1)
        {
            PropertyAssignmentViewModel viewModel;


            if (claimid == -1)
            {
                viewModel = new PropertyAssignmentViewModel();
                viewModel.UseMasterPageLayout = true;
                viewModel.propertyassignment.FileStatus = 2;//Default to Status Open
                viewModel.Claim.IsSingleDeductible = false;//Default to SingeDeductible
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                BLL.User LoggedInUser = css.Users.Find(loggedInUser.UserId);

                if (LoggedInUser.UserTypeId.HasValue)
                {
                    //If the logged user is a Client POC then display only their Company --17-01-2014
                    if (LoggedInUser.UserTypeId == 11)
                    {
                        if (LoggedInUser.HeadCompanyId.HasValue)
                        {
                            viewModel.InsuranceCompaniesList.RemoveAll(x => x.Value != LoggedInUser.HeadCompanyId.Value + "");
                        }
                        else
                        {
                            viewModel.InsuranceCompaniesList.RemoveAll(x => x.Value != "0");
                        }
                    }

                    //If the logged user is a Claims Representative, automatically select the Insurance Company
                    if (LoggedInUser.UserTypeId != 2)
                    {
                        if (LoggedInUser.HeadCompanyId.HasValue)
                        {
                            foreach (var company in viewModel.InsuranceCompaniesList)
                            {
                                if (company.Value == LoggedInUser.HeadCompanyId + "")
                                {
                                    viewModel.Claim.HeadCompanyId = LoggedInUser.HeadCompanyId;
                                    company.Selected = true;
                                    break;
                                }
                            }
                            viewModel.ClientPointofContactList = getClientPointOfContactDDL(LoggedInUser.HeadCompanyId);
                            foreach (var client in viewModel.ClientPointofContactList)
                            {
                                if (client.Value == LoggedInUser.UserId + "")
                                {
                                    client.Selected = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            else
            {

                viewModel = new PropertyAssignmentViewModel(claimid);
                viewModel.ClientPointofContactList = getClientPointOfContactDDL(viewModel.Claim.HeadCompanyId);
                viewModel.UseMasterPageLayout = false;

                //Gaurav 13-04-2015 assign clientpoc according to externalidentifier mapping for FTPclaims.
               

            }

            List<LineOfBusinessGetList_Result> LObList = css.LineOfBusinessGetList(viewModel.Claim.HeadCompanyId).ToList();
            foreach (var lob in LObList)
            {
                viewModel.LineOfBusinessList.Add(new SelectListItem { Text = lob.LOBDescription, Value = lob.LOBId + "" });
            }

            return View(viewModel);
        }

        [HttpPost]

        public ActionResult PropertyAssignment(PropertyAssignmentViewModel viewmodel, FormCollection form, Int64 claimid = -1)
        {
            try
            {
                if (isFormValid(viewmodel, claimid))
                {

                    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                    string SPID = form["selectedassignmentID"];
                    if (claimid == -1)
                    {
                        #region Claims
                        ObjectParameter outClaimId = new ObjectParameter("claimId", DbType.Int64);

                        //ClaimId = css.ClaimsInsert(outClaimId, viewmodel.Claim.ClaimNumber, 0, 0, Convert.ToInt32(viewmodel.Claim.LOBId), objClientPointOfContactUserId.UserId, viewmodel.Claim.PolicyNumber, viewmodel.Claim.PolicyType, null, null, Convert.ToByte(viewmodel.Claim.SeverityLevel), Convert.ToInt32(viewmodel.Claim.ServiceOffering), Convert.ToDouble(viewmodel.Claim.TotalDeductible), Convert.ToInt32(viewmodel.Claim.CauseTypeOfLoss), Convert.ToDateTime(viewmodel.Claim.DateofLoss), viewmodel.Claim.InsuredFirstName, viewmodel.Claim.InsuredLastName, "", "", viewmodel.Claim.InsuredAddress1, viewmodel.Claim.InsuredAddress2, viewmodel.Claim.InsuredCity, viewmodel.Claim.InsuredState, viewmodel.Claim.InsuredZip, viewmodel.Claim.PropertyAddress1, viewmodel.Claim.PropertyAddress2, viewmodel.Claim.PropertyCity, viewmodel.Claim.PropertyState, viewmodel.Claim.PropertyZip, "", viewmodel.Claim.InsuredHomePhone, viewmodel.Claim.InsuredBusinessPhone, viewmodel.Claim.InsuredMobilePhone, viewmodel.Claim.InsuredEmail, "", viewmodel.Claim.CatCode, 0, "", "", Convert.ToDateTime(viewmodel.Claim.PolicyInceptionDate), Convert.ToDateTime(viewmodel.Claim.PolicyEffectiveDate), Convert.ToDateTime(viewmodel.Claim.PolicyExpirationDate));
                        int? headCompanyId = null;
                        long? clientPointOfContactUserId = null;

                        if (viewmodel.Claim.HeadCompanyId.HasValue)
                        {
                            if (viewmodel.Claim.HeadCompanyId.Value != 0)
                            {
                                headCompanyId = viewmodel.Claim.HeadCompanyId.Value;
                            }
                        }
                        if (viewmodel.Claim.ClientPointOfContactUserId.HasValue)
                        {
                            if (viewmodel.Claim.ClientPointOfContactUserId.Value != 0)
                            {
                                clientPointOfContactUserId = viewmodel.Claim.ClientPointOfContactUserId.Value;
                            }
                        }

                        //css.ClaimsInsert(outClaimId, viewmodel.Claim.ClaimNumber, headCompanyId, viewmodel.Claim.LOBId, clientPointOfContactUserId, viewmodel.Claim.PolicyNumber, viewmodel.Claim.PolicyType, null, null, viewmodel.Claim.SeverityLevel, viewmodel.Claim.ServiceOffering, viewmodel.Claim.TotalDeductible, viewmodel.Claim.CauseTypeOfLoss, viewmodel.Claim.DateofLoss, viewmodel.Claim.InsuredFirstName, viewmodel.Claim.InsuredLastName, "", "", viewmodel.Claim.InsuredAddress1, viewmodel.Claim.InsuredAddress2, viewmodel.Claim.InsuredCity, viewmodel.Claim.InsuredState, viewmodel.Claim.InsuredZip, viewmodel.Claim.PropertyAddress1, viewmodel.Claim.PropertyAddress2, viewmodel.Claim.PropertyCity, viewmodel.Claim.PropertyState, viewmodel.Claim.PropertyZip, "", viewmodel.Claim.InsuredHomePhone, viewmodel.Claim.InsuredBusinessPhone, viewmodel.Claim.InsuredMobilePhone,
                        //    viewmodel.Claim.InsuredEmail, "", viewmodel.Claim.CatCode, 0, "", "", viewmodel.Claim.PolicyInceptionDate, viewmodel.Claim.PolicyEffectiveDate, viewmodel.Claim.PolicyExpirationDate, viewmodel.Claim.PropertyContactName,
                        //    viewmodel.Claim.PropertyContactPhone, viewmodel.Claim.IsSingleDeductible.Value, viewmodel.Claim.ClaimTypeId);
                        css.ClaimsInsert(outClaimId, viewmodel.Claim.ClaimNumber, headCompanyId, viewmodel.Claim.LOBId, clientPointOfContactUserId, viewmodel.Claim.PolicyNumber, viewmodel.Claim.PolicyType, null, null, viewmodel.Claim.SeverityLevel, viewmodel.Claim.ServiceOffering, viewmodel.Claim.TotalDeductible, viewmodel.Claim.CauseTypeOfLoss, viewmodel.Claim.DateofLoss, viewmodel.Claim.InsuredFirstName, viewmodel.Claim.InsuredLastName, "", "", viewmodel.Claim.InsuredAddress1, viewmodel.Claim.InsuredAddress2, viewmodel.Claim.InsuredCity, viewmodel.Claim.InsuredState, viewmodel.Claim.InsuredZip, viewmodel.Claim.PropertyAddress1, viewmodel.Claim.PropertyAddress2, viewmodel.Claim.PropertyCity, viewmodel.Claim.PropertyState, viewmodel.Claim.PropertyZip, "", viewmodel.Claim.InsuredHomePhone, viewmodel.Claim.InsuredBusinessPhone, viewmodel.Claim.InsuredMobilePhone,
                        viewmodel.Claim.InsuredEmail, "", viewmodel.Claim.CatCode, 0, "", "", viewmodel.Claim.PolicyInceptionDate, viewmodel.Claim.PolicyEffectiveDate, viewmodel.Claim.PolicyExpirationDate, viewmodel.Claim.PropertyContactName,
                        viewmodel.Claim.PropertyContactPhone, viewmodel.Claim.IsSingleDeductible.Value);

                        claimid = Convert.ToInt64(outClaimId.Value);

                        #endregion

                        #region PropertyAssignments
                        DateTime assignmentDate = DateTime.Now;
                        ObjectParameter outAssignmentId = new ObjectParameter("AssignmentId", DbType.Int64);
                        Int64 AssignmentId;

                        viewmodel.propertyassignment.FileStatus = 2; //2 is the unassigned 
                        viewmodel.propertyassignment.CSSQAAgentUserId = 1; //set default for Invoice Validation
                        //AssignmentId = css.PropertyAssignmentInsert(outAssignmentId, ClaimId, Convert.ToDateTime(viewmodel.propertyassignment.AssignmentDate), viewmodel.propertyassignment.AssignmentDescription, spUser.UserId, Convert.ToInt64(viewmodel.propertyassignment.CSSPointofContactUserId), Convert.ToInt64(viewmodel.propertyassignment.CSSQAAgentUserId), null, "", null, null, null, null, null, "", null, null, null, "", "", null);
                        //AssignmentId = css.PropertyAssignmentInsert(outAssignmentId, Convert.ToInt64(outClaimId.Value), assignmentDate, viewmodel.propertyassignment.AutoAssignmentDescription, viewmodel.propertyassignment.CSSPointofContactUserId, viewmodel.propertyassignment.CSSQAAgentUserId, viewmodel.propertyassignment.FileStatus, viewmodel.Claim.ServiceOffering);
                        AssignmentId = css.PropertyAssignmentInsert(outAssignmentId, Convert.ToInt64(outClaimId.Value), assignmentDate, viewmodel.propertyassignment.AutoAssignmentDescription, viewmodel.propertyassignment.CSSPointofContactUserId, viewmodel.propertyassignment.CSSQAAgentUserId, viewmodel.propertyassignment.FileStatus);
                        css.SaveChanges();
                        #endregion

                        #region ClaimCoverages Table

                        //identify whether any record exists which needs to be inserted
                        ClaimCoverage cc = viewmodel.Claim.ClaimCoverages.ElementAt(0);
                        bool hasClaimCoverageData = false;
                        if (cc != null)
                        {
                            if (!String.IsNullOrEmpty(cc.CoverageName))
                            {
                                hasClaimCoverageData = true;
                            }

                        }
                        if (hasClaimCoverageData)
                        {

                            //Code for inserting Data of dynamically created Rows into DB 
                            ObjectParameter outClaimCoverageId = new ObjectParameter("ClaimCoverageId", DbType.Int64);
                            foreach (ClaimCoverage coverage in viewmodel.Claim.ClaimCoverages)
                            {
                                css.ClaimCoveragesInsert(outClaimCoverageId, Convert.ToInt64(outClaimId.Value), coverage.CoverageName, coverage.CoverageType, coverage.CoverageLimit, coverage.CoverageDeductible, coverage.COV_Reserves, 0, 0, coverage.ExpenseReserve);
                            }
                        }

                        #endregion

                        #region ClaimMortgages Table

                        //identify whether any record exists which needs to be inserted
                        ClaimMortgage cm = viewmodel.Claim.ClaimMortgages.ElementAt(0);
                        bool hasClaimMortgageData = false;
                        if (cm != null)
                        {
                            if (!String.IsNullOrEmpty(cm.MortgageHolder))
                            {
                                hasClaimMortgageData = true;
                            }

                        }
                        if (hasClaimMortgageData)
                        {

                            //Code for inserting Data of dynamically created Rows into DB 
                            ObjectParameter outClaimCoverageId = new ObjectParameter("ClaimMortgageId", DbType.Int64);
                            foreach (ClaimMortgage mortgage in viewmodel.Claim.ClaimMortgages)
                            {
                                css.usp_ClaimMortgagesInsert(Convert.ToInt64(outClaimId.Value), mortgage.MortgageHolder, mortgage.LoanNumber);
                            }
                        }

                        #endregion

                        //return RedirectToAction("SubmitSuccess");

                        if (SPID != null)
                        {
                            NewAssignmentController controller = new NewAssignmentController();
                            controller.ControllerContext = this.ControllerContext;
                            controller.AutoAssignServiceProvider(Convert.ToInt64(outAssignmentId.Value), Convert.ToInt64(SPID));
                        }

                    }
                    else
                    {
                        int? headCompanyId = null;
                        long? clientPointOfContactUserId = null;

                        if (viewmodel.Claim.HeadCompanyId.HasValue)
                        {
                            if (viewmodel.Claim.HeadCompanyId.Value != 0)
                            {
                                headCompanyId = viewmodel.Claim.HeadCompanyId.Value;
                            }
                        }
                        if (viewmodel.Claim.ClientPointOfContactUserId.HasValue)
                        {
                            if (viewmodel.Claim.ClientPointOfContactUserId.Value != 0)
                            {
                                clientPointOfContactUserId = viewmodel.Claim.ClientPointOfContactUserId.Value;
                            }
                        }

                        //css.ClaimsUpdate(claimid, viewmodel.Claim.ClaimNumber, headCompanyId, viewmodel.Claim.LOBId, clientPointOfContactUserId, viewmodel.Claim.PolicyNumber, viewmodel.Claim.PolicyType, null, null, viewmodel.Claim.SeverityLevel, viewmodel.Claim.ServiceOffering, viewmodel.Claim.TotalDeductible, viewmodel.Claim.CauseTypeOfLoss, viewmodel.Claim.DateofLoss, viewmodel.Claim.InsuredFirstName, viewmodel.Claim.InsuredLastName, "", "", viewmodel.Claim.InsuredAddress1, viewmodel.Claim.InsuredAddress2, viewmodel.Claim.InsuredCity, viewmodel.Claim.InsuredState, viewmodel.Claim.InsuredZip, viewmodel.Claim.PropertyAddress1, viewmodel.Claim.PropertyAddress2, viewmodel.Claim.PropertyCity, viewmodel.Claim.PropertyState, viewmodel.Claim.PropertyZip, "", viewmodel.Claim.InsuredHomePhone, viewmodel.Claim.InsuredBusinessPhone, viewmodel.Claim.InsuredMobilePhone, viewmodel.Claim.InsuredEmail, "", viewmodel.Claim.CatCode, 0, "", "", viewmodel.Claim.PolicyInceptionDate, viewmodel.Claim.PolicyEffectiveDate, viewmodel.Claim.PolicyExpirationDate, viewmodel.Claim.PropertyContactName, viewmodel.Claim.PropertyContactPhone, viewmodel.Claim.IsSingleDeductible.Value,viewmodel.Claim.ClaimTypeId);
                        css.ClaimsUpdate(claimid, viewmodel.Claim.ClaimNumber, headCompanyId, viewmodel.Claim.LOBId, clientPointOfContactUserId, viewmodel.Claim.PolicyNumber, viewmodel.Claim.PolicyType, null, null, viewmodel.Claim.SeverityLevel, viewmodel.Claim.ServiceOffering, viewmodel.Claim.TotalDeductible, viewmodel.Claim.CauseTypeOfLoss, viewmodel.Claim.DateofLoss, viewmodel.Claim.InsuredFirstName, viewmodel.Claim.InsuredLastName, "", "", viewmodel.Claim.InsuredAddress1, viewmodel.Claim.InsuredAddress2, viewmodel.Claim.InsuredCity, viewmodel.Claim.InsuredState, viewmodel.Claim.InsuredZip, viewmodel.Claim.PropertyAddress1, viewmodel.Claim.PropertyAddress2, viewmodel.Claim.PropertyCity, viewmodel.Claim.PropertyState, viewmodel.Claim.PropertyZip, "", viewmodel.Claim.InsuredHomePhone, viewmodel.Claim.InsuredBusinessPhone, viewmodel.Claim.InsuredMobilePhone, viewmodel.Claim.InsuredEmail, "", viewmodel.Claim.CatCode, 0, "", "", viewmodel.Claim.PolicyInceptionDate, viewmodel.Claim.PolicyEffectiveDate, viewmodel.Claim.PolicyExpirationDate, viewmodel.Claim.PropertyContactName, viewmodel.Claim.PropertyContactPhone, viewmodel.Claim.IsSingleDeductible.Value);
                        css.SaveChanges();
                        //css.PropertyAssignmentUpdate(viewmodel.propertyassignment.AutoAssignmentId, claimid, viewmodel.propertyassignment.AutoAssignmentDate, viewmodel.propertyassignment.AutoAssignmentDescription, viewmodel.propertyassignment.CSSPointofContactUserId, viewmodel.propertyassignment.CSSQAAgentUserId, viewmodel.Claim.ServiceOffering);
                        css.PropertyAssignmentUpdate(viewmodel.propertyassignment.AutoAssignmentId, claimid, viewmodel.propertyassignment.AutoAssignmentDate, viewmodel.propertyassignment.AutoAssignmentDescription, viewmodel.propertyassignment.CSSPointofContactUserId, viewmodel.propertyassignment.CSSQAAgentUserId);
                        //CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                        //AssignmentStatusUpdate(viewmodel.propertyassignment.AssignmentId, viewmodel.propertyassignment.FileStatus.Value, loggedInUser.UserId);
                        //Utility.ExportClaim(viewmodel.Claim, headCompanyId);

                        #region ClaimCoverages Table
                        //Code for inserting Data of dynamically created Rows into DB 


                        //identify records which are present in the db table but not on the form
                        //such records have been deleted by the user on the form
                        foreach (ClaimCoverage coverage in css.ClaimCoverages.Where(x => x.ClaimId == viewmodel.Claim.ClaimId))
                        {
                            bool isDeleted = true;

                            //find whether ClaimCoverageId record is present in the view model
                            foreach (ClaimCoverage tempCoverage in viewmodel.Claim.ClaimCoverages)
                            {
                                if (tempCoverage.ClaimCoverageId == coverage.ClaimCoverageId)
                                {
                                    //ClaimCoverageId not present, hence delete it.
                                    isDeleted = false;
                                    break;
                                }
                            }

                            if (isDeleted)
                            {
                                css.ClaimCoveragesDelete(coverage.ClaimCoverageId);
                            }

                        }

                        //identify whether any record exists which needs to be inserted
                        ClaimCoverage cc = viewmodel.Claim.ClaimCoverages.ElementAt(0);
                        bool hasClaimCoverageData = false;
                        if (cc != null)
                        {
                            if (!String.IsNullOrEmpty(cc.CoverageName))
                            {
                                hasClaimCoverageData = true;
                            }

                        }
                        if (hasClaimCoverageData)
                        {

                            ObjectParameter outClaimCoverageId = new ObjectParameter("ClaimCoverageId", DbType.Int64);
                            foreach (ClaimCoverage coverage in viewmodel.Claim.ClaimCoverages)
                            {
                                if (coverage.ClaimCoverageId != 0)
                                {
                                    css.ClaimCoveragesUpdate(coverage.ClaimCoverageId, viewmodel.Claim.ClaimId, coverage.CoverageName, coverage.CoverageType, coverage.CoverageLimit, coverage.CoverageDeductible, coverage.COV_Reserves, 0, 0, coverage.ExpenseReserve);
                                }
                                else
                                {
                                    css.ClaimCoveragesInsert(outClaimCoverageId, viewmodel.Claim.ClaimId, coverage.CoverageName, coverage.CoverageType, coverage.CoverageLimit, coverage.CoverageDeductible, coverage.COV_Reserves, 0, 0, coverage.ExpenseReserve);
                                }


                            }
                        }


                        css.SaveChanges();
                        #endregion

                        #region ClaimMortgages Table
                        //Code for inserting Data of dynamically created Rows into DB 


                        //identify records which are present in the db table but not on the form
                        //such records have been deleted by the user on the form
                        foreach (ClaimMortgage mortgage in css.ClaimMortgages.Where(x => x.ClaimId == viewmodel.Claim.ClaimId))
                        {
                            bool isMortgageDeleted = true;

                            //find whether ClaimMortgageId record is present in the view model
                            foreach (ClaimMortgage tempMortgage in viewmodel.Claim.ClaimMortgages)
                            {
                                if (tempMortgage.ClaimMortgageId == mortgage.ClaimMortgageId)
                                {
                                    //ClaimMortgageId not present, hence delete it.
                                    isMortgageDeleted = false;
                                    break;
                                }
                            }

                            if (isMortgageDeleted)
                            {
                                css.usp_ClaimMortgagesDelete(mortgage.ClaimMortgageId);
                            }

                        }

                        //identify whether any record exists which needs to be inserted
                        ClaimMortgage cm = viewmodel.Claim.ClaimMortgages.ElementAt(0);
                        bool hasClaimMortgageData = false;
                        if (cm != null)
                        {
                            if (!String.IsNullOrEmpty(cm.MortgageHolder))
                            {
                                hasClaimMortgageData = true;
                            }

                        }
                        if (hasClaimMortgageData)
                        {

                            foreach (ClaimMortgage mortgage in viewmodel.Claim.ClaimMortgages)
                            {
                                if (mortgage.ClaimMortgageId != 0)
                                {
                                    css.usp_ClaimMortgagesUpdate(mortgage.ClaimMortgageId, mortgage.MortgageHolder, mortgage.LoanNumber);
                                }
                                else
                                {
                                    css.usp_ClaimMortgagesInsert(viewmodel.Claim.ClaimId, mortgage.MortgageHolder, mortgage.LoanNumber);
                                }


                            }
                        }


                        css.SaveChanges();
                        #endregion


                    }

                    TempData["UseMasterPageLayout"] = viewmodel.UseMasterPageLayout;
                    if (Request.IsAjaxRequest())
                    {
                        var successmsg = new { id = 1, errorList = "" };
                        return Json(successmsg, JsonRequestBehavior.AllowGet);
                    }
                    if (viewmodel.isRedirectToTriage)
                    {
                        return RedirectToAction("Details", "PropertyAssignment", new { ClaimId = Cypher.EncryptString(claimid.ToString()), ActiveMainTab = "Triage" });
                    }
                    else
                    {
                        return RedirectToAction("AutoSearch", "AutoAssignment");
                    }
                }
                else
                {
                    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                    BLL.User LoggedInUser = css.Users.Find(loggedInUser.UserId);
                    if (LoggedInUser.UserTypeId.HasValue)
                    {

                        if (LoggedInUser.HeadCompanyId.HasValue)
                        {
                            foreach (var company in viewmodel.InsuranceCompaniesList)
                            {
                                if (company.Value == LoggedInUser.HeadCompanyId + "")
                                {
                                    if (LoggedInUser.UserTypeId != 2)
                                    {
                                        viewmodel.Claim.HeadCompanyId = LoggedInUser.HeadCompanyId;
                                        company.Selected = true;
                                        break;
                                    }
                                }
                            }
                        }
                        viewmodel.ClientPointofContactList = getClientPointOfContactDDL(viewmodel.Claim.HeadCompanyId);

                        bool RedirectToSpMap = Convert.ToBoolean(form["RedirectToSpMap"]);
                        if (Request.IsAjaxRequest())
                        {
                            List<object> errorlist = new List<object>();
                            foreach (var modelStateKey in ViewData.ModelState.Keys)
                            {
                                var modelStateVal = ViewData.ModelState[modelStateKey];

                                foreach (var error in modelStateVal.Errors)
                                {

                                    var newError = new { key = modelStateKey.Substring(modelStateKey.IndexOf(".") + 1), Message = error.ErrorMessage };
                                    errorlist.Add(newError);
                                    // You may log the errors if you want
                                }
                            }
                            var returnObj = new { id = 0, errorList = errorlist };
                            return Json(returnObj, JsonRequestBehavior.AllowGet);


                        }


                    }
                }
                List<LineOfBusinessGetList_Result> LObList = css.LineOfBusinessGetList(viewmodel.Claim.HeadCompanyId).ToList();
                foreach (var lob in LObList)
                {
                    viewmodel.LineOfBusinessList.Add(new SelectListItem { Text = lob.LOBDescription, Value = lob.LOBId + "" });
                }

            }
            catch (Exception ex)
            {
                ViewBag.ExceptionErrorMsg = ex.Message + "<br/>" + ex.InnerException + "<br/>" + ex.StackTrace;
            }
            return View(viewmodel);
        }
        public bool isFormValid(PropertyAssignmentViewModel viewmodel, Int64? claimid)
        {
            bool IsValid = true;
            if (viewmodel.Claim.ClaimCoverages.Count == 1)
            {
                ClaimCoverage cc = viewmodel.Claim.ClaimCoverages.ElementAt(0);
                bool clearClaimCoverageErrors = true;
                if (cc != null)
                {
                    if (!String.IsNullOrEmpty(cc.CoverageName))
                    {
                        clearClaimCoverageErrors = false;
                    }

                }
                if (clearClaimCoverageErrors)
                {
                    ModelState.Remove("Claim.ClaimCoverages[0].CoverageName");
                    ModelState.Remove("Claim.ClaimCoverages[0].CoverageType");
                    ModelState.Remove("Claim.ClaimCoverages[0].CoverageLimit");
                    ModelState.Remove("Claim.ClaimCoverages[0].CoverageDeductible");
                    ModelState.Remove("Claim.ClaimCoverages[0].COV_Reserves");
                }
            }
            //claimInfo Duplicate check
            if (!String.IsNullOrEmpty(viewmodel.Claim.ClaimNumber) && claimid == -1)
            {
                if (css.Claims.Where(x => x.ClaimNumber.Trim() == viewmodel.Claim.ClaimNumber.Trim()).Count() > 0)
                {
                    IsValid = false;
                    ModelState.AddModelError("ClaimNumber", "Claim Number is already Exists.");
                }
            }
            if (viewmodel.Claim.ClaimMortgages.Count == 1)
            {
                ClaimMortgage cm = viewmodel.Claim.ClaimMortgages.ElementAt(0);
                bool clearClaimMortgagesErrors = true;
                if (cm != null)
                {
                    if (!String.IsNullOrEmpty(cm.MortgageHolder))
                    {
                        clearClaimMortgagesErrors = false;
                    }

                }
                if (clearClaimMortgagesErrors)
                {
                    ModelState.Remove("Claim.ClaimMortgages[0].MortgageHolder");
                    ModelState.Remove("Claim.ClaimMortgages[0].LoanNumber");
                }
            }
            //Should contain atleast one Phone number
            if (String.IsNullOrEmpty(viewmodel.Claim.InsuredBusinessPhone) && String.IsNullOrEmpty(viewmodel.Claim.InsuredHomePhone) && String.IsNullOrEmpty(viewmodel.Claim.InsuredMobilePhone))
            {
                IsValid = false;
                ModelState.AddModelError("InsuredMobilePhone", "One Insured Phone is required.");
            }
            if (viewmodel.Claim.InsuredState == "0")
            {
                IsValid = false;
                ModelState.AddModelError("InsuredState", "Insured State is required.");
            }
            if (viewmodel.Claim.PropertyState == "0")
            {
                IsValid = false;
                ModelState.AddModelError("PropertyState", "Property State is required.");
            }
            if (viewmodel.Claim.DateofLoss.HasValue)
            {
                if (viewmodel.Claim.DateofLoss.Value.Date > DateTime.Now.Date)
                {
                    IsValid = false;
                    ModelState.AddModelError("DateofLoss", "Loss Date cannot be a future date.");
                }
            }
            if (!String.IsNullOrEmpty(viewmodel.Claim.InsuredZip))
            {
                if (!Utility.isZipCodeValid(viewmodel.Claim.InsuredZip))
                {
                    IsValid = false;
                    ModelState.AddModelError("InsuredZip", "Invalid Insured Zip.");
                }
            }
            if (!String.IsNullOrEmpty(viewmodel.Claim.PropertyZip))
            {
                if (!Utility.isZipCodeValid(viewmodel.Claim.PropertyZip))
                {
                    IsValid = false;
                    ModelState.AddModelError("PropertyZip", "Invalid Property Zip.");
                }
            }
            if (!ModelState.IsValid)
            {
                IsValid = false;
            }


            return IsValid;
        }
        public ActionResult ClaimCorrespondenceDocument(Int64 assignmentId, short documentTypeId)
        {
            CSS.Models.User user = CSS.AuthenticationUtility.GetUser();

            ClaimCorrespondencesViewModel viewModel = new ClaimCorrespondencesViewModel();
            viewModel.AssignmentId = assignmentId;
            viewModel.DocumentTypeId = documentTypeId;
            viewModel.UploadedByUserId = user.UserId;
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult ClaimCorrespondenceDocument(ClaimCorrespondencesViewModel viewModel, FormCollection form)
        {
            //get list of fields
            List<CorrespondenceFormField> CorrespondenceFormFieldList = css.CorrespondenceFormFields.Where(x => x.CorrespondenceFormId == viewModel.CorrespondenceFormId).ToList();
            viewModel.CorrespondenceFormFieldList = CorrespondenceFormFieldList;
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult UploadClaimCorrespondenceDocument(ClaimCorrespondencesViewModel viewModel, FormCollection form)
        {
            Claim claim = new Claim();
            PropertyAssignment propertyassignment = new PropertyAssignment();
            try
            {
                CorrespondenceForm correspondenceForm = css.CorrespondenceForms.Find(viewModel.CorrespondenceFormId);
                List<CorrespondenceFormField> CorrespondenceFormFieldList = css.CorrespondenceFormFields.Where(x => x.CorrespondenceFormId == viewModel.CorrespondenceFormId).ToList();
                foreach (CorrespondenceFormField field in CorrespondenceFormFieldList)
                {
                    if (form[field.FormFieldId + ""] != null)
                    {
                        string fieldValue = form[field.FormFieldId + ""];
                        ObjectParameter outClaimCorrespondenceId = new ObjectParameter("ClaimCorrespondenceId", DbType.Int64);
                        css.ClaimCorrespondencesInsert(outClaimCorrespondenceId, viewModel.AssignmentId, viewModel.CorrespondenceFormId, field.FormFieldId, fieldValue);
                    }
                }

                var claimid = (from pa in css.PropertyAssignments
                               where pa.AssignmentId == viewModel.AssignmentId
                               select pa.ClaimId).First();

                claim = css.Claims.Find(claimid);
                string InsuredName = claim.InsuredFirstName + " " + " " + claim.InsuredLastName;

                var company = (from comp in css.Companies where comp.CompanyId == claim.HeadCompanyId select comp.CompanyName).FirstOrDefault();

                propertyassignment = css.PropertyAssignments.Find(viewModel.AssignmentId);

                var serviceprovider = (from sp in css.Users
                                       where sp.UserId == propertyassignment.OAUserID
                                       select sp.FirstName + sp.LastName).FirstOrDefault();

                //Construct HTML
                string htmlData = "<table><tr><td>Dear " + InsuredName + "</td></tr><tr><td><br/></td></tr><tr><td>" + claim.InsuredAddress1 + "</td></tr><tr><td>" + claim.InsuredCity + ", " + claim.InsuredState + claim.InsuredZip + "</td></tr><tr><td><br/><br/><br/></td></tr>";
                htmlData += "<tr><td>This letter is to inform you that we have concluded the investigation of your claim. Based on this investigation, </td></tr><tr><td>we have determined your loss payment will be as follows</td></tr><tr><td><br/><br/></td></tr><tr><td><table>";
                foreach (CorrespondenceFormField field in CorrespondenceFormFieldList)
                {
                    htmlData += "<tr>";
                    htmlData += "<td>";
                    htmlData += field.FormFieldName;
                    htmlData += "</td>";
                    htmlData += "<td>";
                    if (form[field.FormFieldId + ""] != null)
                    {
                        htmlData += form[field.FormFieldId + ""];
                    }
                    htmlData += "</td>";
                    htmlData += "</tr>";
                }
                htmlData += "</table></td></tr><tr><td><br/><br/></td></tr><tr><td>The above figures were arrived at based on the enclosed property damage estimate. If you feel that we have missed any items or have a dispute</td></tr>";
                htmlData += "<tr><td>in these amounts please do not hesitate to call us. In case of a dispute please do not perform any repairs before these disputes are brought to our attention. </td></tr>";
                htmlData += "<tr><td><br/><br/></td></tr><tr><td>Once your property is repaired you are entitled to the recoverable depreciation amount listed above. To claim this amount please provide us proof that the</td></tr><tr><td>property has been repaired.</td></tr>";
                htmlData += "<tr><td><br/><br/></td></tr><tr><td>Thank You</td></tr><tr><td><br/></td></tr><tr><td>" + company + "</td></tr><tr><td><br/><br/></td></tr><tr><td>" + serviceprovider + "</td></tr></table>";
                BLL.Document document = new BLL.Document();
                document.AssignmentId = viewModel.AssignmentId;
                document.DocumentTypeId = viewModel.DocumentTypeId;
                document.Title = correspondenceForm.CorrespondenceForm1;
                document.DocumentUploadedByUserId = viewModel.UploadedByUserId;
                document.DocumentUploadedDate = DateTime.Now;

                byte[] pdfData = Utility.ConvertHTMLStringToPDF(htmlData);
                #region Cloud Storage
                // extract only the fielname
                string fileName = DateTime.Now.ToString("MMddyyyy_HHmm") + ".pdf";
                //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                string assignmentId = document.AssignmentId + "";
                string documentTypeId = document.DocumentTypeId + "";

                // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                string relativeFileName = assignmentId + "/" + documentTypeId + "/" + fileName;

                string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                CloudStorageUtility.StoreFile(containerName, relativeFileName, pdfData);
                #endregion

                #region Local File System
                //// extract only the fielname
                //string fileName = DateTime.Now.ToString("MMddyyyy_HHmm") + ".pdf";
                //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                //string assignmentId = document.AssignmentId + "";
                //string documentTypeId = document.DocumentTypeId + "";
                ////check whether the folder with user's userid exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId));
                //}
                ////check whether the destination folder depending on the type of document being uploaded exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId));
                //}

                //// store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                //string path = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), fileName);

                //Utility.StoreBytesAsFile(pdfData, path);
                #endregion





                ObjectParameter outDocumentId = new ObjectParameter("DocumentId", DbType.Int64);
                css.DocumentsInsert(outDocumentId, document.AssignmentId, null, document.Title, fileName, fileName, document.DocumentUploadedByUserId, document.DocumentTypeId);

                return RedirectToAction("UploadClaimDocumentSuccess");
            }
            catch (Exception ex)
            {
                RedirectToAction("ClaimCorrespondenceDocument", new { assignmentId = viewModel.AssignmentId, documentTypeId = viewModel.DocumentTypeId });
            }
            return View(viewModel);
        }
        public ActionResult DeleteClaimDocument(Int64 claimDocumentId = 0)
        {
            int valueToReturn = 0;//1-Success
            try
            {
                if (claimDocumentId != 0)
                {
                    BLL.Document claimDoc = css.Documents.Find(claimDocumentId);
                    css.DocumentsDelete(claimDocumentId);

                    //delete physical file 
                    string fileName = new FileInfo(claimDoc.DocumentPath).Name;
                    #region Cloud Delete
                    string assignmentId = claimDoc.AssignmentId + "";
                    string documentTypeId = claimDoc.DocumentTypeId + "";
                    string relativeFileName = assignmentId + "/" + documentTypeId + "/" + fileName;
                    string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                    CloudStorageUtility.DeleteFile(containerName, relativeFileName);
                    #endregion
                    #region local file system
                    //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                    //string assignmentId = claimDoc.AssignmentId + "";
                    //string documentTypeId = claimDoc.DocumentTypeId + "";
                    //string filePath = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), fileName);

                    //if (System.IO.File.Exists(filePath))
                    //{
                    //    try
                    //    {
                    //        System.IO.File.Delete(filePath);
                    //    }
                    //    catch (IOException ex)
                    //    {
                    //    }
                    //}
                    #endregion

                    valueToReturn = 1;
                }
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn);
        }

        public ActionResult ClaimDocumentsList(Int64 claimId, Int64 assignmentId, short documentTypeId = 0, byte? statusId = 4)
        {
            DocumentsListViewModel viewModel = new BLL.ViewModels.DocumentsListViewModel(claimId, assignmentId, documentTypeId);
            viewModel.StatusList = new List<SelectListItem>();
            viewModel.StatusId = (byte)4;
            viewModel.StatusList.Add(new SelectListItem() { Text = "--Select--", Value = "4" });//Default
            viewModel.StatusList.Add(new SelectListItem() { Text = "Approved", Value = "1" });
            viewModel.StatusList.Add(new SelectListItem() { Text = "Rejected", Value = "0" });
            viewModel.StatusList.Add(new SelectListItem() { Text = "Pending Approval", Value = "2" });//Database value for Pending Approval is NULL
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            int? headCompanyId = css.Claims.Find(claimId).HeadCompanyId;
            if (headCompanyId.HasValue)
            {
                viewModel.IsFTPDocExportEnabled = css.Companies.Find(headCompanyId.Value).EnableFTPDocExport ?? false;
            }

            viewModel.DocumentsList = css.DocumentsList(claimId, documentTypeId).ToList();
            viewModel.DocumentsPreviousList = css.usp_DocumentsPreviousGetList(claimId, documentTypeId).ToList();
            viewModel.DocumentsRejectedList = css.RejectedDocumentsList(claimId, documentTypeId).ToList();
               if (loggedInUser.UserTypeId == 1 || loggedInUser.UserTypeId == 8)
               {
                   viewModel.DocumentsList.RemoveAll(x => x.DocumentTypeId == 10);
                   viewModel.DocumentsPreviousList.RemoveAll(x => x.DocumentTypeId == 10);
                   viewModel.DocumentsRejectedList.RemoveAll(x => x.DocumentTypeId == 10);



               }
          //  CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            //Client POC should see only the approved documents -17-01-2014
            if (loggedInUser.UserTypeId == 11)
            {
                List<DocumentsList_Result> tempList = viewModel.DocumentsList.ToList();
                tempList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);

                viewModel.DocumentsList = tempList.ToList();
                viewModel.DocumentsPreviousList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
            }


            //Search Filter
            if (statusId == 1)//Doc Status: Approved 
            {
                List<DocumentsList_Result> tempList = viewModel.DocumentsList.ToList();
                tempList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);

                    viewModel.DocumentsList = tempList.ToList();
                viewModel.DocumentsPreviousList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
                //viewModel.DocumentsRejectedList.ToList().RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
              //  viewModel.DocumentsRejectedList.ToList().RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
                viewModel.DocumentsRejectedList = null;
            }
            else if (statusId == 0)//Doc Status: Rejected
            {
                List<DocumentsList_Result> tempList = viewModel.DocumentsList.ToList();
                tempList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : true) == true);

                       viewModel.DocumentsList = tempList.ToList();
                viewModel.DocumentsPreviousList.RemoveAll(x => (x.Status.HasValue ? x.Status.Value : true) == true);
              //  viewModel.DocumentsRejectedList.ToList().RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
            }
            else if (statusId == 2)//Doc Status: Pending Approval
            {
               // IEnumerable<DocumentsList_Result> obj = (IEnumerable<DocumentsList_Result>)viewModel.DocumentsList;
                List<DocumentsList_Result> tempList = (List<DocumentsList_Result>)viewModel.DocumentsList.ToList();
                tempList.RemoveAll(x => x.Status.HasValue == true);

                    viewModel.DocumentsList = tempList.ToList();
                viewModel.DocumentsPreviousList.RemoveAll(x => x.Status.HasValue == true);
                viewModel.DocumentsRejectedList = null;
                //viewModel.DocumentsRejectedList.ToList().RemoveAll(x => (x.Status.HasValue ? x.Status.Value : false) == false);
                //viewModel.DocumentsRejectedList.ToList().RemoveAll(x => x.Status.HasValue == true);
            }

            viewModel.ClaimDocumentType = css.ClaimDocumentTypes.Find(documentTypeId);
            PropertyAssignment pa = css.PropertyAssignments.Find(assignmentId);

            List<GetClaimParticipants_Result> claimParticipantsList = null;
            if (pa != null)
            {
                claimParticipantsList = css.GetClaimParticipants(pa.ClaimId).ToList();
            }
            string participantsEmails = "";
            foreach (GetClaimParticipants_Result participant in claimParticipantsList)
            {
                if (!String.IsNullOrEmpty(participant.Email))
                {
                    if (participant.UserType != "Insured")
                    {
                        if (participantsEmails.Length != 0)
                        {
                            participantsEmails += ", ";
                        }
                        participantsEmails += participant.Email;
                    }
                }
            }
            viewModel.ClaimParticipantsEmailAddresses = participantsEmails;
            if (viewModel.ClaimDocumentType == null)
            {
                viewModel.ClaimDocumentType = new ClaimDocumentType();
            }

            //return View(viewModel);
            var jsonResult = Json(RenderViewToString("ClaimDocumentsList", viewModel), JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        [HttpPost]
        public ActionResult ClaimDocumentsList(DocumentsListViewModel viewModel)
        {
            return View(viewModel);
        }


        public ActionResult ClaimEmailDocuments(Int64 claimId, Int64 assignmentId, string documentIds, short documentTypeId = 0, byte? statusId = 4)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            string[] documentIdList = documentIds.Split(',');

            DocumentsListViewModel viewModel = new BLL.ViewModels.DocumentsListViewModel(claimId, assignmentId, documentTypeId);
         
            List<DocumentsListResult> doclist = new List<DocumentsListResult>();

            foreach (string documentId in documentIdList)
            {
                DocumentsListResult docselected = new DocumentsListResult();
                BLL.Document document = css.Documents.Find(Convert.ToInt64(documentId));
                docselected.DocumentId = document.DocumentId;
                docselected.AssignmentId = document.AssignmentId;
                docselected.Title = document.Title + "_" + document.DocumentPath;
                doclist.Add(docselected);
            }
            viewModel.DocumentsListMail = doclist;

            PropertyAssignment pa = css.PropertyAssignments.Find(assignmentId);

            List<GetClaimParticipants_Result> claimParticipantsList = null;
            if (pa != null)
            {
                claimParticipantsList = css.GetClaimParticipants(pa.ClaimId).ToList();
            }
            string participantsEmails = "";
            foreach (GetClaimParticipants_Result participant in claimParticipantsList)
            {
                if (!String.IsNullOrEmpty(participant.Email))
                {
                    if (participant.UserType != "Insured")
                    {
                        if (participantsEmails.Length != 0)
                        {
                            participantsEmails += ", ";
                        }
                        participantsEmails += participant.Email;
                    }
                }
            }
            viewModel.ClaimParticipantsEmailAddresses = participantsEmails;
            return Json(RenderViewToString("ClaimEmailDocuments", viewModel), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ClaimEmailDocuments(DocumentsListViewModel viewModel)
        {
            return View(viewModel);
        }


        public ActionResult ClaimFtpDocuments(Int64 claimId, Int64 assignmentId, string documentIds, short documentTypeId = 0, byte? statusId = 4)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            string[] documentIdList = documentIds.Split(',');

            DocumentsListViewModel viewModel = new BLL.ViewModels.DocumentsListViewModel(claimId, assignmentId, documentTypeId);

            List<DocumentsListResult> doclist = new List<DocumentsListResult>();

            foreach (string documentId in documentIdList)
            {
                DocumentsListResult docselected = new DocumentsListResult();
                BLL.Document document = css.Documents.Find(Convert.ToInt64(documentId));
                docselected.DocumentId = document.DocumentId;
                docselected.AssignmentId = document.AssignmentId;
                docselected.Title = document.Title + "_" + document.DocumentPath;
                doclist.Add(docselected);
            }
            viewModel.DocumentsListMail = doclist;
            //PropertyAssignment pa = css.PropertyAssignments.Find(assignmentId);

            //List<GetClaimParticipants_Result> claimParticipantsList = null;
            //if (pa != null)
            //{
            //    claimParticipantsList = css.GetClaimParticipants(pa.ClaimId).ToList();
            //}
            //string participantsEmails = "";
            //foreach (GetClaimParticipants_Result participant in claimParticipantsList)
            //{
            //    if (!String.IsNullOrEmpty(participant.Email))
            //    {
            //        if (participantsEmails.Length != 0)
            //        {
            //            participantsEmails += ", ";
            //        }
            //        participantsEmails += participant.Email;
            //    }
            //}
            //viewModel.ClaimParticipantsEmailAddresses = participantsEmails;
            return Json(RenderViewToString("ClaimFtpDocuments", viewModel), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ClaimFtpDocuments(DocumentsListViewModel viewModel)
        {
            return View(viewModel);
        }
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult UploadClaimDocument(Int64 assignmentId, short documentTypeId)
        {
            Response.CacheControl = "no-cache";
            ClaimDocumentViewModel viewModel = new ClaimDocumentViewModel();
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            viewModel.Document.DocumentUploadedByUserId = loggedInUser.UserId;
            viewModel.Document.AssignmentId = assignmentId;
            viewModel.Document.DocumentTypeId = documentTypeId;
            return View(viewModel);

        }

        [HttpPost]
        public ActionResult UploadClaimDocument(ClaimDocumentViewModel viewModel, HttpPostedFileBase uploadFile)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (uploadFile != null && uploadFile.ContentLength > 0)
                    {
                        if (viewModel.Document != null)
                        {
                            UploadDocument(viewModel.Document, uploadFile);
                            return RedirectToAction("UploadClaimDocumentSuccess");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return View(viewModel);
        }

        public ActionResult UploadClaimDocumentSuccess()
        {
            return View();
        }

        public ActionResult GetFinacialList(Int64 claimid)
        {
            XACTClaimsInfoViewModel viewModel = new BLL.ViewModels.XACTClaimsInfoViewModel();
            viewModel.PropertyAssignment = css.PropertyAssignments.Where(x => x.ClaimId == claimid).First();
            viewModel.PropertyInvoiceList = css.usp_PropertyInvoiceSummaryGetList(claimid).ToList();
            Int64 assignmentId = viewModel.PropertyAssignment.AssignmentId;
            //viewModel.ClaimCoveragesBasedonClaimid = css.ClaimCoveragesBasedonClaimIdGetList(claimid).ToList();
            viewModel.ClaimReserves = css.usp_ClaimReservesGetList(assignmentId).ToList();
            viewModel.ReserveListBasedonCoverageid = null;

            return PartialView("_AssignmentDetailsReserves", viewModel);
        }

        public ActionResult GetReserveList(Int64 ClaimCoverageId)
        {
            XACTClaimsInfoViewModel viewModel = new BLL.ViewModels.XACTClaimsInfoViewModel();
            viewModel.ReserveListBasedonCoverageid = css.ClaimReservesBasedonCoverageIdGetList(ClaimCoverageId).ToList();

            return PartialView("_AssignmentDetailsReservesBasedonCoverageId", viewModel);
        }
        public JsonResult GetClaimReservesModify(Int64 claimReserveId)
        {

            ClaimReserve claimReserve = css.ClaimReserves.Find(claimReserveId);
            IndemnityReserveModifyViewModel viewModel = new IndemnityReserveModifyViewModel();
            viewModel.ClaimReserveChange = new ClaimReservesHistory();
            viewModel.ClaimReserveChange.ClaimReserveId = claimReserveId;
            viewModel.ClaimReserveHistoryList = css.usp_ClaimReservesHistoryGetList(claimReserveId).ToList();
            viewModel.AssignmentId = css.ClaimReserves.Find(claimReserveId).AssignmentId;
            viewModel.ClaimId = css.PropertyAssignments.Find(viewModel.AssignmentId).ClaimId.Value;
            viewModel.CoverageName = claimReserve.ClaimCoverage.CoverageName;

            return Json(RenderPartialViewToString("_IndemnityReserveModify", viewModel), JsonRequestBehavior.AllowGet);
        }
        public JsonResult ClaimReservesUpdate(Int64 claimReserveId, decimal actualPaid, decimal expenseReserve)
        {
            int valueToReturn = 0;
            try
            {
                css.usp_ClaimReservesUpdate(claimReserveId, actualPaid, expenseReserve);
                valueToReturn = 1;
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }

            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ClaimReserveHistoryInsert(Int64 claimReserveId, decimal amountChanged, string comment)
        {
            int valueToReturn = 0;
            try
            {
                css.usp_ClaimReservesHistoryInsert(claimReserveId, amountChanged, comment);
                valueToReturn = 1;
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }

            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult OrderBydiaries(Int64? assignmentId, string OrderByvalue, string LastOrderByValue, string Flag)
        {

            List<SelectListItem> ParticipantList = new List<SelectListItem>();
            XACTClaimsInfoViewModel viewModel = new BLL.ViewModels.XACTClaimsInfoViewModel();
            viewModel.PropertyAssignment.ClaimId = css.PropertyAssignments.Find(assignmentId).ClaimId;
            foreach (GetClaimParticipants_Result participant in css.GetClaimParticipants(viewModel.PropertyAssignment.ClaimId))
            {
                ParticipantList.Add(new SelectListItem { Text = participant.ShortDesc, Value = Convert.ToString(participant.UserId) });
            }
            viewModel.ParticipantList = ParticipantList;

            if ((Flag == "0" && LastOrderByValue == OrderByvalue) || (LastOrderByValue != OrderByvalue))
            {
                switch (OrderByvalue)
                {
                    case "DiaryCategoryId":
                        viewModel.DiaryDetail = css.GetDiaryItems(assignmentId).OrderBy(x => x.DiaryCategoryDesc).ToList();
                        break;
                    case "CreatedByUserId":
                        viewModel.DiaryDetail = css.GetDiaryItems(assignmentId).OrderBy(x => x.Username).ToList();
                        break;
                    case "CreatedDate":
                        viewModel.DiaryDetail = css.GetDiaryItems(assignmentId).OrderBy(x => x.CreatedDate).ToList();
                        break;
                    case "DueDate":
                        viewModel.DiaryDetail = css.GetDiaryItems(assignmentId).OrderBy(x => x.DueDate).ToList();
                        break;
                    case "StatusId":
                        viewModel.DiaryDetail = css.GetDiaryItems(assignmentId).ToList();
                        viewModel.DiaryDetail = viewModel.DiaryDetail.OrderBy(x => x.StatusId).ToList();
                        break;
                }
                Flag = "1";
            }
            else if (Flag == "1" && LastOrderByValue == OrderByvalue)
            {
                switch (OrderByvalue)
                {
                    case "DiaryCategoryId":
                        viewModel.DiaryDetail = css.GetDiaryItems(assignmentId).OrderByDescending(x => x.DiaryCategoryDesc).ToList();
                        break;
                    case "CreatedByUserId":
                        viewModel.DiaryDetail = css.GetDiaryItems(assignmentId).OrderByDescending(x => x.Username).ToList();
                        break;
                    case "CreatedDate":
                        viewModel.DiaryDetail = css.GetDiaryItems(assignmentId).OrderByDescending(x => x.CreatedDate).ToList();
                        break;
                    case "DueDate":
                        viewModel.DiaryDetail = css.GetDiaryItems(assignmentId).OrderByDescending(x => x.DueDate).ToList();
                        break;
                    case "StatusId":
                        viewModel.DiaryDetail = css.GetDiaryItems(assignmentId).ToList();
                        viewModel.DiaryDetail = viewModel.DiaryDetail.OrderByDescending(x => x.StatusId).ToList();
                        break;
                }
                Flag = "0";
            }
            viewModel.PropertyAssignment.AssignmentId = assignmentId.Value;
            viewModel.claim.ClaimNumber = css.Claims.Find(viewModel.PropertyAssignment.ClaimId).ClaimNumber;
            ViewBag.LastOrderByDiaryItem = OrderByvalue;
            ViewBag.flag = Flag;
            return Json(RenderViewToString("ClaimDiaryList", viewModel), JsonRequestBehavior.AllowGet);
        }

        public ActionResult checkClaimID(string claimNumber)
        {
            int Count = 0;
            Count = css.Claims.Where(x => x.ClaimNumber == claimNumber).Count();
            return Json(Count, JsonRequestBehavior.AllowGet);
        }
        public ActionResult checkPolicyID(string policynumber)
        {
            int Count = 0;
            Count = css.Claims.Where(x => x.PolicyNumber == policynumber).Count();
            return Json(Count, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult AssignmentStatusUpdate(Int64 assignmentId, Int64 claimId, short statusId, string statusChangeNote, bool retainSP, int SPRank,bool RejectionMark)
        {
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                Int64 spid = 0;
                ClaimCycleTimeDetail c = null;
                if (SPRank != 0)
                {

                    spid = css.PropertyAssignments.Single(x => x.AssignmentId == assignmentId && x.ClaimId == claimId).OAUserID.Value;
                    c = css.ClaimCycleTimeDetails.Single(x => x.SPId == spid && x.ClaimId == claimId);
                }
                   
                if (statusId == 7)
                {
                    css.usp_GenerateSPRank(spid, claimId, statusChangeNote, SPRank, null, statusId);
                }

                if (statusId == 8)
                {
                    css.usp_GenerateSPRank(spid, claimId, null, null, RejectionMark, statusId);
                }
          
                css.UpdateAssignmentStatus(assignmentId, statusId, loggedInUser.UserId, null, 0);

                //Add Note Entry
                AssignmentStatu assignmentStatu = css.AssignmentStatus.Find(statusId);
                ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                if (statusId != 7)
                {
                    css.usp_NotesInsert(outNoteId, assignmentId, "Status changed to " + assignmentStatu.StatusDescription, statusChangeNote, DateTime.Now, false, true, loggedInUser.UserId, null, null, null, null, null, null);
                }
                if (retainSP && statusId == 11)//Consider Retain SP only if the claim is being reopened
                {
                    //When a reopen is performed a new PropertyAssignment record is created without the SP
                    Int64? existingSPUserId = css.PropertyAssignments.Find(assignmentId).OAUserID;

                    //Find the most recent PropertyAssignment for the claim which got created after a reopen
                    Int64 newAssignmentId = css.PropertyAssignments.Where(x => x.ClaimId == claimId).OrderByDescending(x => x.AssignmentId).First().AssignmentId;
                    if (existingSPUserId.HasValue)
                    {
                        css.AssignServiceProviderToAssignment(newAssignmentId, existingSPUserId, loggedInUser.UserId);
                    }
                }

            }
            catch (Exception ex)
            {
            }
            //return RedirectToAction("Details", new { claimId = claimId });
            return Json(1);
        }

        [HttpPost]
        public JsonResult IsCoverageReserveOrExpenseAvailable(Int64 claimId = 0)
        {
            int valueToReturn = 0;
            try
            {

                if (css.ClaimCoverages.Where(x => x.ClaimId == claimId).ToList().Count > 0)
                {
                    valueToReturn = 1;
                }

            }
            catch (Exception ex)
            {
            }

            return Json(valueToReturn);

        }

        [HttpPost]
        public ActionResult UpdateFileStatus(Int64 AssignmentId, Byte Filestatus)
        {

            try
            {

                if (Filestatus > 0)
                {
                    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                    AssignmentStatusUpdate(AssignmentId, Filestatus, loggedInUser.UserId);

                }

            }
            catch (Exception ex)
            {
            }

            return RedirectToAction("UpdateSuccess");

        }
        private void AssignmentStatusUpdate(Int64 AssignmentId, Byte Filestatus, Int64 loggedInUserId)
        {
            css.UpdateAssignmentStatus(AssignmentId, Filestatus, loggedInUserId, null, 0);
            //Add Note Entry
            AssignmentStatu assignmentStatu = css.AssignmentStatus.Find(Filestatus);
            ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
            css.usp_NotesInsert(outNoteId, AssignmentId, "Status changed to " + assignmentStatu.StatusDescription, "Status changed to " + assignmentStatu.StatusDescription, DateTime.Now, false, true, loggedInUserId, null, null, null, null,null,null);

            //css.FileStatusUpdate(AssignmentId, Filestatus, loggedInUserId);
        }
        public ActionResult UpdateSuccess()
        {
            return View();
        }

        //Payroll
        public ActionResult PayrollMaster(Int64 claimId)
        {
            PayrollViewModel viewModel = new PayrollViewModel();
            viewModel.ClaimId = claimId;
            if (css.PropertyAssignments.Where(x => x.ClaimId == claimId && x.DateReOpened != null).ToList().Count != 0)
            {
                viewModel.HasReopen = true;
            }
            viewModel.SPPayrollAndAdjList = css.usp_SPPayrollAndAdjGetList(claimId).ToList();
            return Json(RenderPartialViewToString("_AssignmentDetailsPayroll", viewModel), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SubmitClaimDiaryItems(string diaryid, string claimId, string assignmentid, string DueDate, string AssigntoUserId, string DiaryCategoryId, string Title, string DiaryDesc, bool EmailTaskDescription, bool IsExternalContact, bool IsBillable, double? NoOfHours)
        {
            try
            {
                XACTClaimsInfoViewModel viewmodel = new XACTClaimsInfoViewModel();
                DateTime dtDueDate;
                if (!String.IsNullOrEmpty(DueDate))
                {
                    dtDueDate = Convert.ToDateTime(DueDate);
                }
                else
                {
                    dtDueDate = DateTime.Now.AddDays(1);
                }

                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                byte StatusId;
                Int64 DiaryId;
                if (diaryid == "0")
                {
                    StatusId = 1;
                    if (dtDueDate < DateTime.Now)
                    {
                        StatusId = 3;
                    }
                    ObjectParameter outDiaryId = new ObjectParameter("DiaryId", DbType.Int64);
                    DiaryId = css.DiaryItemsInsert(outDiaryId, Convert.ToInt64(assignmentid), Convert.ToByte(DiaryCategoryId), Convert.ToInt64(AssigntoUserId), loggedInUser.UserId, System.DateTime.Now, dtDueDate, null, null, Title, DiaryDesc, StatusId, null, null, IsExternalContact, IsBillable, NoOfHours);

                    //string assignedToName = "";
                    //if (!String.IsNullOrEmpty(AssigntoUserId))
                    //{
                    //    if (AssigntoUserId != "0")
                    //    {
                    //        if (IsExternalContact == false)
                    //        {
                    //            User tempUser = css.Users.Find(Convert.ToInt64(AssigntoUserId));
                    //            assignedToName = tempUser.FirstName + " " + tempUser.LastName;

                    //        }
                    //        else
                    //        {
                    //            assignedToName = css.ClaimContacts.Find(Convert.ToInt64(AssigntoUserId)).ContactName;

                    //        }
                    //    }
                    //}
                    //string comment = (NoOfHours.HasValue ? "No of Hours: " + NoOfHours.Value : "") + " Due Date: " + DueDate + "  Assigned To:" + assignedToName + "    " + System.Environment.NewLine + DiaryDesc;

                    //ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int64);
                    //css.usp_NotesInsert(outNoteId, Convert.ToInt64(assignmentid), Title, comment, DateTime.Now, true, true, loggedInUser.UserId, null,null,null,null);
                }
                else
                {
                    DiaryId = css.DiaryItemsUpdate(Convert.ToInt64(diaryid), Convert.ToInt64(assignmentid), Convert.ToByte(DiaryCategoryId), Convert.ToInt64(AssigntoUserId), loggedInUser.UserId, System.DateTime.Now, dtDueDate, null, null, Title, DiaryDesc, null, null, IsExternalContact, IsBillable, NoOfHours);
                }

                if (EmailTaskDescription)
                {
                    if (!String.IsNullOrEmpty(AssigntoUserId))
                    {
                        if (AssigntoUserId != "0")
                        {
                            User user = css.Users.Find(Convert.ToInt64(AssigntoUserId));
                            if (user != null)
                            {
                                string body = "Due Date: " + dtDueDate.ToString("MM/dd/yyyy") + "<br/><br/>";
                                body += DiaryDesc;
                                List<string> emailToList = new List<string>();
                                emailToList.Add(user.Email);
                                Utility.sendEmail(emailToList, loggedInUser.Email, Title, body);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return Json(1);
        }

        public ActionResult ClaimDiariesList(Int64? assignmentId)
        {
            List<SelectListItem> ParticipantList = new List<SelectListItem>();

            XACTClaimsInfoViewModel viewModel = new BLL.ViewModels.XACTClaimsInfoViewModel();
            viewModel.PropertyAssignment.ClaimId = css.PropertyAssignments.Find(assignmentId).ClaimId;
            foreach (GetClaimParticipants_Result participant in css.GetClaimParticipants(viewModel.PropertyAssignment.ClaimId))
            {
                ParticipantList.Add(new SelectListItem { Text = participant.ShortDesc, Value = Convert.ToString(participant.UserId) });
            }
            viewModel.ParticipantList = ParticipantList;


            viewModel.DiaryDetail = css.GetDiaryItems(viewModel.PropertyAssignment.ClaimId).ToList();

            viewModel.PropertyAssignment.AssignmentId = assignmentId.Value;
            viewModel.claim.ClaimNumber = css.Claims.Find(viewModel.PropertyAssignment.ClaimId).ClaimNumber;
            return Json(RenderViewToString("ClaimDiaryList", viewModel), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetExternalClaimParticipantsList(Int64 claimId)
        {
            List<SelectListItem> ParticipantList = new List<SelectListItem>();
            ParticipantList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (GetExternalClaimParticipants_Result participant in css.GetExternalClaimParticipants(claimId))
            {
                ParticipantList.Add(new SelectListItem { Text = participant.ShortDesc, Value = Convert.ToString(participant.ClaimContactId) });
            }

            return Json(ParticipantList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetInternalClaimParticipantsList(Int64 claimId)
        {
            List<SelectListItem> ParticipantList = new List<SelectListItem>();
            ParticipantList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (GetClaimParticipants_Result participant in css.GetClaimParticipants(claimId))
            {
                ParticipantList.Add(new SelectListItem { Text = participant.ShortDesc, Value = Convert.ToString(participant.UserId) });
            }

            return Json(ParticipantList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddClaimContact(ClaimContact claimContact, FormCollection form)
        {
            int valueToReturn = 0;
            try
            {
                ObjectParameter outClaimContactId = new ObjectParameter("ClaimContactId", DbType.Int64);
                css.ClaimContactsInsert(outClaimContactId, claimContact.ClaimId, claimContact.ContactType, claimContact.ContactName, claimContact.ContactEmail);
                valueToReturn = 1;
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }

            return Json(valueToReturn, JsonRequestBehavior.AllowGet);

            //return PartialView("_ClaimContacts",claimContact);
        }
        public ActionResult NoteUpdate(Int64 noteId, bool isBillable, double? noOfHours)
        {
            int valueToReturn = 0;
            try
            {
                css.usp_NoteUpdate(noteId, isBillable, noOfHours);
                valueToReturn = 1;
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }

            return Json(valueToReturn, JsonRequestBehavior.AllowGet);

        }
        //public ActionResult GetFinacialList(Int64 Claimid)
        //{
        //    XACTClaimsInfoViewModel viewModel = new BLL.ViewModels.XACTClaimsInfoViewModel();
        //    viewModel.ClaimCoverages = css.ClaimCoveragesGetList();

        //    return PartialView("_AssignmentDetailsPayments", viewModel);
        //}
        public ActionResult GetFinacialpaymentList(Int64? ClaimID)
        {
            XACTClaimsInfoViewModel viewModel = new BLL.ViewModels.XACTClaimsInfoViewModel();
            viewModel.ClaimFinanacialPayment = css.GetClaimFinancialPayment(ClaimID).ToList();
            return PartialView("_AssignmentDetailsPayments", viewModel);
        }
        public ActionResult TriageDetails(Int64 assignmentId, bool? isReadOnly = false)
        {
            TriageViewModel viewModel = new TriageViewModel();
            viewModel.isReadOnly = isReadOnly.Value;
            List<Triage> triageResult = css.Triages.Where(x => x.AssignmentId == assignmentId).ToList();
            if (!isReadOnly.Value)
            {
                Int64 claimId = css.PropertyAssignments.Find(assignmentId).ClaimId.Value;
                viewModel.ClaimParticipantsList = css.GetClaimParticipants(claimId).ToList();
            }
            if (triageResult.Count != 0)
            {
                viewModel.Triage = triageResult[0];
                viewModel.StateList.Select(x => x.Value == viewModel.Triage.DamageState);
            }
            else
            {
                viewModel.Triage = new Triage();
                if (assignmentId != null)
                {

                    viewModel.Triagedetails = css.usp_GetTriageDetails(Convert.ToInt64(assignmentId)).ToList();
                    if (viewModel.Triagedetails.Count > 0)
                    {
                        //Triage TgDetails = new Triage();
                        viewModel.Triage.PartyContactName = viewModel.Triagedetails[0].PartyContactName;
                        viewModel.Triage.DamageStreetAddress = viewModel.Triagedetails[0].DamageStreetAddress;
                        viewModel.Triage.DamageState = viewModel.Triagedetails[0].DamageState;
                        viewModel.Triage.DamangeCity = viewModel.Triagedetails[0].DamangeCity;
                        viewModel.Triage.DamageZip = viewModel.Triagedetails[0].DamageZip;
                        viewModel.Triage.DamangesReview = viewModel.Triagedetails[0].DamangesReview;
                        viewModel.Triage.Email = viewModel.Triagedetails[0].Email;
                        viewModel.Triage.HomePhone = viewModel.Triagedetails[0].HomePhone;
                        viewModel.Triage.MobilePhone = viewModel.Triagedetails[0].MobilePhone;
                        viewModel.Triage.SeverityLevelId = viewModel.Triagedetails[0].SeverityLevelId;
                    }
                    viewModel.Triage.PreferEmailOverPhone = true;
                    viewModel.Triage.DwellingAffectedAreaName = "";

                }
                else
                {


                }

                viewModel.Triage.TriageId = -1;
                viewModel.Triage.AssignmentId = assignmentId;
            }
            try
            {


            }
            catch (Exception ex)
            {
            }
            return Json(RenderPartialViewToString("_TriageDetails", viewModel), JsonRequestBehavior.AllowGet);

        }
        public ActionResult SaveTriageDetails(TriageViewModel viewModel, FormCollection form)
        {
            Int64 valueToReturn = 0;
            try
            {
                Triage triage = new Triage();
                triage.TriageId = Convert.ToInt64(form["triageId"]);

                triage.AssignmentId = Convert.ToInt64(form["assignmentId"]);
                triage.PartyContactName = Convert.ToString(form["partyContactName"]);
                triage.DamageStreetAddress = Convert.ToString(form["damageStreetAddress"]);
                triage.DamangeCity = Convert.ToString(form["damangeCity"]);
                triage.DamageState = Convert.ToString(form["damageState"]);
                triage.DamageZip = Convert.ToString(form["damageZip"]);
                triage.DwellingAffectedAreaName = Convert.ToString(form["dwellingAffectedAreaName"]);
                triage.HomePhone = Convert.ToString(form["homePhone"]);
                triage.WorkPhone = Convert.ToString(form["workPhone"]);
                triage.MobilePhone = Convert.ToString(form["mobilePhone"]);
                triage.Email = Convert.ToString(form["email"]);
                triage.PreferEmailOverPhone = Convert.ToBoolean(form["preferEmailOverPhone"]);
                triage.SeverityLevelId = Convert.ToByte(form["severityLevelId"]);
                triage.DamangesReview = Convert.ToString(form["damangesReview"]);

                if (triage.TriageId == -1)
                {
                    ObjectParameter outTriageId = new ObjectParameter("TriageId", DbType.Int64);
                    css.usp_TriageInsert(outTriageId, triage.AssignmentId, triage.PartyContactName, triage.DamageStreetAddress, triage.DamangeCity, triage.DamageState, triage.DamageZip, triage.DwellingAffectedAreaName, triage.HomePhone, triage.WorkPhone, triage.MobilePhone, triage.Email, triage.PreferEmailOverPhone, triage.SeverityLevelId, triage.DamangesReview);
                    valueToReturn = Convert.ToInt64(outTriageId.Value);
                }
                else
                {
                    css.usp_TriageUpdate(triage.TriageId, triage.AssignmentId, triage.PartyContactName, triage.DamageStreetAddress, triage.DamangeCity, triage.DamageState, triage.DamageZip, triage.DwellingAffectedAreaName, triage.HomePhone, triage.WorkPhone, triage.MobilePhone, triage.Email, triage.PreferEmailOverPhone, triage.SeverityLevelId, triage.DamangesReview);
                    valueToReturn = Convert.ToInt64(triage.TriageId);
                }

            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }

            return Json(valueToReturn, JsonRequestBehavior.AllowGet);

            //return PartialView("_ClaimContacts",claimContact);
        }
        public ActionResult DocumentAcess(Int64 AssignmentId = 0, string fileurl = "", string filename = "")
        {
            string Fileurl ="" ;
           // Cypher cypher = new Cypher();
            //if (Fileurl.Contains("[msl]"))
            //{
            //    Fileurl = Cypher.DecryptString(fileurl.Replace("[msl]", "/"));
            //}
            //else
            //{
            //    Fileurl = Cypher.DecryptString(fileurl);
            //}
            Fileurl = Cypher.DecryptString(Cypher.ReplaceCharcters(fileurl));

            //if (Session["AuthorizeSearch"] != null)
            //{
            //    XactAssignmentSearchViewModel viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["AuthorizeSearch"].ToString());

                
            //     Int64? claimid=css.PropertyAssignments.Where(x => x.AssignmentId == AssignmentId).Select(x=>x.ClaimId).FirstOrDefault();



            //     //if (viewModel.ClaimsInfo.Where(x => x.ClaimId == claimid).Count() <= 0)
            //     //{

            //     //    return View("AuthorizationError");
            //     //}


            //}
            //else
            //{
            //    return View("AuthorizationError");
            //}
            //filename = "PHOTO_SHEET.PDF";
            //fileurl = "https://optdata.blob.core.windows.net/opt-propertyassignmentdocs/270616/4/PHOTO_SHEET.PDF";
            try
            {
                //Fileurl = Server.MapPath(Fileurl);

                //Fileurl = Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString() + "" + Fileurl);
                //return new ReportResult(fileurl, filename);


                string contentType = string.Empty;

                if (filename.Contains(".pdf"))
                {
                    contentType = "application/pdf";
                }

                else if (filename.Contains(".docx"))
                {
                    contentType = "application/docx";
                }
                else if (filename.Contains(".png"))
                {
                    contentType = "application/png";
                }
                else if (filename.Contains(".jpeg"))
                {
                    contentType = "application/jpeg";
                }
                else if (filename.Contains(".jpg"))
                {
                    contentType = "application/jpg";
                }
                else if (filename.Contains(".xlsx"))
                {
                    contentType = "application/xlsx";
                }

                return new ReportResult(Fileurl, filename, contentType);
              //  return FilePath(Fileurl, contentType, filename);  
    
            }
            catch (Exception ex)
            {

            }
            return File(fileurl, "application/pdf");

        }
        public ActionResult GetNotesList(Int64? Claimid, int displayType = 0)
        {
            //DisplayType = 0 - Hide System Generated
            //DisplayType = 1 - View All
            XACTClaimsInfoViewModel viewModel = new BLL.ViewModels.XACTClaimsInfoViewModel();


            try
            {

                viewModel.NotesDisplayType = displayType + "";
                viewModel.NotesDisplayTypeList = getNotesDisplayTypeList();

                bool? isSystemGenerated = null;
                if (displayType == 1
                    )
                {
                    isSystemGenerated = false;
                }

                viewModel.PropertyAssignment = css.PropertyAssignments.Where(x => x.ClaimId == Claimid).OrderByDescending(x => x.AssignmentId).FirstOrDefault();
                viewModel.NoteHistory = css.usp_NotesHistoryGetList(Claimid, isSystemGenerated).ToList();
                viewModel.RunningTime = css.usp_NoteRunningTotalGet(Claimid).First();
                List<usp_PropertyInvoiceSummaryGetList_Result> invoiceSummaryList = css.usp_PropertyInvoiceSummaryGetList(Claimid).ToList();
                List<usp_PropertyInvoiceSummaryGetList_Result> invoiceWithBilledNotesSummaryList = new List<usp_PropertyInvoiceSummaryGetList_Result>();

                foreach (var invoice in invoiceSummaryList)
                {
                    if (css.Notes.Where(x => x.BilledInvoiceId == invoice.InvoiceId).ToList().Count > 0)
                    {
                        invoiceWithBilledNotesSummaryList.Add(invoice);
                    }
                }
                viewModel.PropertyInvoiceList = invoiceWithBilledNotesSummaryList;

                viewModel.Pager = new BLL.Models.Pager();
                viewModel.Pager.Page = 1;
                viewModel.Pager.RecsPerPage = 20;
                viewModel.Pager.FirstPageNo = 1;
                viewModel.Pager.IsAjax = true;
                viewModel.Pager.FormName = "NotesHistoryRF";
                viewModel.Pager.AjaxParam = Claimid.ToString();
                viewModel.Pager.TotalCount = viewModel.NoteHistory.Count().ToString();
                viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();

                viewModel.NoteHistory = viewModel.NoteHistory.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();
                viewModel.ClaimParticipantslist = css.GetClaimParticipants(Claimid).ToList();
            }

            catch (Exception ex)
            {
            }

            return PartialView("_AssignmentDetailsNotesHistory", viewModel);
        }

        public XACTClaimsInfoViewModel setPager(XACTClaimsInfoViewModel viewModel, FormCollection form, Int64? Claimid)
        {
            viewModel.Pager = new BLL.Models.Pager();
            if (form == null)
            {
                viewModel.Pager.Page = 1;
                viewModel.Pager.FirstPageNo = 1;
            }
            else
            {
                if ((form["hdnCurrentPage"]) != "")
                {
                    viewModel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);
                }
                else
                {
                    viewModel.Pager.Page = 1;
                }
                if (form["hdnstartPage"] != null)
                {
                    if (!String.IsNullOrEmpty(form["hdnstartPage"]))
                    {
                        viewModel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
                    }
                    else
                    {
                        viewModel.Pager.FirstPageNo = 1;
                    }
                }
            }
            viewModel.Pager.IsAjax = true;
            viewModel.Pager.FormName = "NotesHistoryRF";
            viewModel.Pager.AjaxParam = Claimid.ToString();
            //+ "," + viewModel.Pager.FirstPageNo;
            viewModel.Pager.RecsPerPage = 20;
            viewModel.Pager.TotalCount = viewModel.Pager.TotalCount = viewModel.NoteHistory.Count().ToString();
            viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
            viewModel.NoteHistory = viewModel.NoteHistory.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();

            return viewModel;
        }

        private List<SelectListItem> getNotesDisplayTypeList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list.Add(new SelectListItem() { Text = "View All", Value = "0" });
            list.Add(new SelectListItem() { Text = "Hide System Generated", Value = "1" });

            return list;
        }

        [HttpPost]
        public ActionResult GetNotesList(FormCollection form, Int64? Claimid, string FirstPage, string CurrentPage, int displayType = 0)
        {
            XACTClaimsInfoViewModel viewModel = new BLL.ViewModels.XACTClaimsInfoViewModel();

            try
            {
                viewModel.NotesDisplayType = displayType + "";
                viewModel.NotesDisplayTypeList = getNotesDisplayTypeList();

                bool? isSystemGenerated = null;
                if (displayType == 1)
                {
                    isSystemGenerated = false;
                }

                viewModel.PropertyAssignment = css.PropertyAssignments.Where(x => x.ClaimId == Claimid).FirstOrDefault();
                viewModel.NoteHistory = css.usp_NotesHistoryGetList(Claimid, isSystemGenerated).ToList();
                viewModel.RunningTime = css.usp_NoteRunningTotalGet(Claimid).First();

                viewModel.PropertyInvoiceList = css.usp_PropertyInvoiceSummaryGetList(Claimid).ToList();
                viewModel.Pager = new BLL.Models.Pager();
                viewModel.Pager.IsAjax = true;
                viewModel.Pager.FormName = "NotesHistoryRF";
                viewModel.Pager.AjaxParam = Claimid.ToString();

                if ((CurrentPage) != "")
                {
                    viewModel.Pager.Page = Convert.ToInt32(CurrentPage);
                }
                else
                {
                    viewModel.Pager.Page = 1;
                }
                viewModel.Pager.RecsPerPage = 20;
                if (FirstPage != "")
                {
                    if (!String.IsNullOrEmpty(FirstPage))
                    {
                        viewModel.Pager.FirstPageNo = Convert.ToInt16(FirstPage);
                    }
                    else
                    {
                        viewModel.Pager.FirstPageNo = 1;
                    }
                }
                viewModel.Pager.TotalCount = viewModel.NoteHistory.Count().ToString();
                viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
                viewModel.NoteHistory = viewModel.NoteHistory.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();

                //if (Session["XactClaimsInfo"] != null)
                //{
                //    Session["XactClaimsInfo"] = viewModel;
                //}
                //else
                //{
                //    Session.Add("XactClaimsInfo", viewModel);
                //}


                viewModel.ClaimParticipantslist = css.GetClaimParticipants(Claimid).ToList();
            }

            catch (Exception ex)
            {
            }

            return PartialView("_AssignmentDetailsNotesHistory", viewModel);
        }

        public ActionResult AssignmentDetailsBasic(Int64 claimid)
        {
            XACTClaimsInfoViewModel claiminfoviewModel = new XACTClaimsInfoViewModel(claimid);
            return PartialView("_AssignmentDetailsBasic", claiminfoviewModel);
        }

        public ActionResult SendmailToParticipant(Int64 assignmentId, string participantid, int fromUserid, string Subject, string Comment, DateTime DateCreated)
        {
            Int64 Userid = 0;
            Int64 claimId = css.PropertyAssignments.Find(assignmentId).ClaimId.Value;
            Claim claim = css.Claims.Find(claimId);
            string emailSubject = "Note - Insured Name: " + claim.InsuredFirstName + " " + claim.InsuredLastName + " - Claim #: " + claim.ClaimNumber;

            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            List<string> Emailid = new List<string>();
            BLL.User user;
            string Body = "";
            Body += "<p>Subject: " + Subject + "</p>";
            Body += "<br />";
            Body += "<p>Comment: " + Comment + "</p>";
            Body += "<br />";
            Body += "<p>Date :" + DateCreated + "</p>";
            Body += "<br />";
            string[] useridtosendemail = participantid.Split(',');
            foreach (string userid in useridtosendemail)
            {
                Userid = Convert.ToInt64(userid);
                user = css.Users.Where(x => x.UserId == Userid).FirstOrDefault();
                Emailid.Add(user.Email);
            }
            user = css.Users.Where(x => x.UserId == loggedInUser.UserId).FirstOrDefault();
            Utility.sendEmail(Emailid, user.Email, emailSubject, Body);
            return Json(1);
        }
        public ActionResult SendTriageEmailToParticipant(Int64 assignmentId, string participantid)
        {
            string valueToReturn = "0";
            try
            {
                Int64 Userid = 0;
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                PropertyAssignment pa = css.PropertyAssignments.Find(assignmentId);
                Claim claim = css.Claims.Find(pa.ClaimId);

                List<string> Emailid = new List<string>();
                BLL.User user;
                string subject = "Triage - Insured Name: " + claim.InsuredFirstName + " " + claim.InsuredLastName + " - Claim #: " + claim.ClaimNumber;
                string body = "";
                body += "Claim #: " + claim.ClaimNumber;
                body += "<br /><br />";
                body += ((JsonResult)TriageDetails(assignmentId, true)).Data.ToString();

                string[] useridtosendemail = participantid.Split(',');
                foreach (string userid in useridtosendemail)
                {
                    Userid = Convert.ToInt64(userid);
                    user = css.Users.Where(x => x.UserId == Userid).FirstOrDefault();
                    Emailid.Add(user.Email);
                }
                user = css.Users.Where(x => x.UserId == loggedInUser.UserId).FirstOrDefault();
                Utility.sendEmail(Emailid, user.Email, subject, body);
                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EmailUtility(string emailto, string emailfrom, int setReplyToEmail, string subject, string Content, Int64? assignmentid)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            string replyTo = String.Empty;
            if (setReplyToEmail == 1)
            {
                replyTo = loggedInUser.Email;
            }
            int NoteId;
            ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
            List<string> Emailid = new List<string>();
            Emailid.Add(emailto);

            Int64? SPUserId = css.PropertyAssignments.Where(x => x.AssignmentId == assignmentid).FirstOrDefault().OAUserID;//not used currently whenever we want to add signature for SP it is used.  

            User LoggedInUser = css.Users.Where(x => x.UserId == loggedInUser.UserId).FirstOrDefault();
            Content = Content + "\n\n\nThanks\n" + LoggedInUser.FirstName + " " + LoggedInUser.LastName;


            string data = HttpUtility.HtmlDecode(Content);
            data = Regex.Replace(data, "\n", "<br>");
            Utility.sendEmail(Emailid, emailfrom, replyTo, subject, data);
            CSS.Models.User user = CSS.AuthenticationUtility.GetUser();
            string emailbody = "TO: " + emailto + "\nSUBJECT: " + subject + "\n\nMESSAGE: " + Content;
            NoteId = css.usp_NotesInsert(outNoteId, assignmentid, (subject == null ? "" : subject), (emailbody == null ? "" : emailbody), DateTime.Now, false, false, user.UserId, null, null, null, null,null,null);
            return Json(1);
        }


        [HttpPost]
        public ActionResult EmailDocuments(Int64 assignmentId, string documentIds, int setReplyToEmail, string toEmails, string subject, string body, string mergedoc)
        {
            int valueToReturn = 0;
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            string fromEmail = System.Configuration.ConfigurationManager.AppSettings["DocumentAttachmentFromEmailAddress"].ToString();
             Int64? claimid = css.PropertyAssignments.FirstOrDefault(x => x.AssignmentId == assignmentId).ClaimId;
             string claimNumber = css.Claims.FirstOrDefault(x => x.ClaimId == claimid).ClaimNumber;
             string newFile1 = "";
             List<string> files = null;


               
                string replyToEmail = String.Empty;
                if (setReplyToEmail == 1)
                {
                    replyToEmail = loggedInUser.Email;
                }
               
                string[] documentIdList = documentIds.Split(',');
                string[] tempToEmailList = toEmails.Split(',');
                string emailbody = "TO: " + toEmails + "\nSUBJECT: " + subject + "\n\nMESSAGE: " + body;
                List<string> attachmentFilePathList = new List<string>();
                List<string> toEmailList = new List<string>();
                List<PdfReader> readerList = new List<PdfReader>();
               
              
                string containerName = "";
               
                long attachedfileslenth = 0;
                bool flag = false;
                string faileddocumentids = "";
                long f = 0;
                 string docpath = "";
             try
             {

                if (mergedoc == "1")
                {
                     files = new List<string>();
                    foreach (string documentId in documentIdList)
                    {
                        BLL.Document document = css.Documents.Find(Convert.ToInt64(documentId));
                        //#region Local File
                        //string filePath = Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString() + "" + document.AssignmentId + "/" + document.DocumentTypeId + "/" + document.DocumentPath);
                        #region Cloud File

                        containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                        string relativeFileName = CloudStorageUtility.GetAbsoluteFileURL(containerName, document.AssignmentId + "/" + document.DocumentTypeId + "/" + document.DocumentPath);

                         try
                         {

                        f = getDocFileSize(relativeFileName);
                        attachedfileslenth += f / 1024;
                          docpath = document.DocumentPath.Substring(document.DocumentPath.LastIndexOf('.') + 1);

                        if (docpath == "pdf" || docpath=="PDF")
                        {
                            if (attachedfileslenth <= Convert.ToInt32(ConfigurationManager.AppSettings["TotalEmailSize"].ToString()))
                            {
                            PdfReader pdfReader = new PdfReader(FileToByteArray(relativeFileName, true));
                            readerList.Add(pdfReader);
                                     faileddocumentids += documentId + ',';
                            }
                            else
                            {
                                flag = true;
                                break;
                            }

                        }
                        else
                        {
                            if (attachedfileslenth <= Convert.ToInt32(ConfigurationManager.AppSettings["TotalEmailSize"].ToString()))
                            {

                                 //attachmentFilePathList.Add(relativeFileName);
                                 docpath = document.DocumentPath.Substring(document.DocumentPath.LastIndexOf('/') + 1);
                                 ReadCloudFile(relativeFileName, docpath, ref attachmentFilePathList, ref files);
                                         faileddocumentids += documentId + ',';
                            }
                            else
                            {
                                flag = true;
                                break;
                            }
                        }
                         }
                         catch (Exception ex)
                         {
                         }
                        #endregion
                    }
                    if (flag == true)
                    {
                        return Json(2);
                    }
                    newFile1 = Server.MapPath(ConfigurationManager.AppSettings["DocumentTempPath"].ToString() + Claim.ClaimId.ToString() + DateTime.Now.ToString("MMddyyymmss") + ".pdf");
                    iTextSharp.text.Document Idoc = new iTextSharp.text.Document(PageSize.A4, 0, 0, 0, 0);
                    PdfCopy pdfcopyprovider = new PdfCopy(Idoc, new FileStream(newFile1, FileMode.Create));
                    Idoc.Open();

                    foreach (PdfReader reader in readerList)
                    {
                        for (int i = 1; i <= reader.NumberOfPages; i++)
                        {
                            PdfImportedPage page = pdfcopyprovider.GetImportedPage(reader, i);
                           
                            pdfcopyprovider.AddPage(page);
                        }
                    }
                    Idoc.Close();
                    attachmentFilePathList.Add(newFile1);

                }
                else
                {
                     files = new List<string>();
                    foreach (string documentId in documentIdList)
                    {
                        BLL.Document document = css.Documents.Find(Convert.ToInt64(documentId));
                        #region Cloud File
                        containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                        string relativeFileName = CloudStorageUtility.GetAbsoluteFileURL(containerName, document.AssignmentId + "/" + document.DocumentTypeId + "/" + document.DocumentPath);
                         try
                         {
                        f = getDocFileSize(relativeFileName);
                       
                        attachedfileslenth += f / 1024;
                        #endregion

                         docpath = document.DocumentPath.Substring(document.DocumentPath.LastIndexOf('/') + 1);
                        if (attachedfileslenth <= Convert.ToInt32(ConfigurationManager.AppSettings["TotalEmailSize"].ToString()))
                        {

                                ReadCloudFile(relativeFileName, docpath, ref attachmentFilePathList, ref files);
                                 faileddocumentids += documentId + ',';
                        }
                        else
                        {
                            flag = true;
                            break;
                        }
                    }
                         catch (Exception ex)
                         {

                         }
                         // attachmentFilePathList.Add(newFile);
                     }
                    if (flag == true)
                    {
                        return Json(2);
                    }
                }
                foreach (string email in tempToEmailList)
                {
                    toEmailList.Add(email);
                }

                body = Regex.Replace(body, "\n", "<br>");
                 if (attachmentFilePathList.Count > 0)
                 {
                Utility.sendEmail(toEmailList, fromEmail, replyToEmail, subject, body, attachmentFilePathList);
                 }
                 //17-03-2015 gaurav
                 // deletinf files from tempdoc
                    if (System.IO.File.Exists(newFile1) && newFile1 != "")
                     {

                         System.IO.File.Delete(newFile1);
                     }
                     foreach (var file in files)
                     {
                         if (System.IO.File.Exists(file))
                         {
                             System.IO.File.Delete(file);
                         }

                     }
                 ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
                 css.usp_NotesInsert(outNoteId, assignmentId, subject, emailbody, DateTime.Now, false, false, loggedInUser.UserId, null, null, null, null, toEmails, documentIds);

                 valueToReturn = 1;
             }
             catch (TimeoutException ex)
             {
                 bool merge=false;
                 bool replytoemail = false;
                 string attachmentlist="";
                 string[] attachmentslist=attachmentFilePathList.ToArray(); 
                 if(mergedoc=="1")
                 {
                     merge=true;

                 }
                 if (setReplyToEmail == 1)
                 {
                     replytoemail = true;
                 }
                 attachmentlist=String.Join(",",attachmentslist);
                 css.FailedEmailLogInsert(toEmails, faileddocumentids.Remove(faileddocumentids.LastIndexOf(','), 1), replyToEmail, subject, body, merge, claimNumber, assignmentId, attachmentlist, loggedInUser.UserId, replytoemail);
                if (System.IO.File.Exists(newFile1) && newFile1 != "")
                {

                    System.IO.File.Delete(newFile1);
                }

                     foreach (var file in files)
                     {
                         if (System.IO.File.Exists(file))
                         {
                             System.IO.File.Delete(file);
                         }

                     }

               //  valueToReturn = -1;
            }
            catch (Exception ex)
            {
                 if (System.IO.File.Exists(newFile1) && newFile1 != "")
                 {

                     System.IO.File.Delete(newFile1);
                 }
                 foreach (var file in files)
                 {
                     if (System.IO.File.Exists(file))
                     {
                         System.IO.File.Delete(file);
                     }

                 }
                if (!String.IsNullOrEmpty(loggedInUser.Email))
                {
                    Utility.sendEmail(loggedInUser.Email, fromEmail, loggedInUser.Email, "Email Sending Failed", "Email sending failed due to some error for claim " + claimNumber);
                        
                }
                //valueToReturn = -1;
            }
            return Json(valueToReturn + "");
        }
        public byte[] FileToByteArray(string fileName, bool fromcloud = false)
        {
            byte[] fileContent = null;
            try
            {
            if (fromcloud)
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(fileName);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream ReceiveStream = response.GetResponseStream();
                System.IO.BinaryReader binaryReader = new System.IO.BinaryReader(ReceiveStream);
                long byteLength = response.ContentLength;
                fileContent = binaryReader.ReadBytes((Int32)byteLength);
                ReceiveStream.Close();
                ReceiveStream.Dispose();
                binaryReader.Close();
            }
            else
            {
                System.IO.FileStream fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                System.IO.BinaryReader binaryReader = new System.IO.BinaryReader(fs);
                long byteLength = new System.IO.FileInfo(fileName).Length;
                fileContent = binaryReader.ReadBytes((Int32)byteLength);
                fs.Close();
                fs.Dispose();
                binaryReader.Close();
            }
            }
            catch (Exception ex)
            {
                throw;
            }
            return fileContent;
        }
        [HttpPost]
        public ActionResult ChangeDocumentStatus(string documentIds, byte newStatusId)
        {
            int valueToReturn = 0;
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            try
            {
                string[] documentIdList = documentIds.Split(',');

                foreach (string documentId in documentIdList)
                {
                    css.usp_DocumentsStatusUpdate(Convert.ToInt64(documentId), newStatusId == 1 ? true : false, loggedInUser.UserId);
                }


                valueToReturn = 1;
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult TransferDocToFTP(Int64 claimId, string documentIds, string mergedoc)
        {
            string valueToReturn = "0";
            //try
            //{
            //    Claim claim = css.Claims.Find(claimId);
            //    int headCompanyId = claim.HeadCompanyId.Value;//Will always have a value as FTP feature as this action is reached only if company has FTP Export Enabled
            //    Company company = css.Companies.Find(headCompanyId);



            //    string[] documentIdList = documentIds.Split(',');
            //    List<string> fileNames = new List<string>();
            //    string destfilename = "";
            //    string indexfilename = "";

            //    string documentid1 = "";
            //    string documentid2 = "";

            //    MemoryStream memorystream = null;

            //    List<PdfReader> readerList = new List<PdfReader>();

            //    List<PdfReader> readerList1 = new List<PdfReader>();



            //    if (mergedoc == "1")
            //    {
            //        foreach (string documentId in documentIdList)
            //        {



            //            //Build Destination File Name

            //            //gaurav 11-07-2014

            //            BLL.Document document = css.Documents.Find(Convert.ToInt64(documentId));

            //            string docpath = document.DocumentPath.Substring(document.DocumentPath.IndexOf('.') + 1);

            //            string docType = "0261";//Documents: All Other Documents
            //            if ((document.DocumentTypeId ?? 0) == 1 || (document.DocumentTypeId ?? 0) == 3 || (document.DocumentTypeId ?? 0) == 8 || document.Title.ToUpper().StartsWith("INVOICE"))
            //            {
            //                //Documents: General Loss Report, Invoice
            //                docType = "0265";
            //                if (docpath == "pdf" || docpath == "PDF")
            //                {
            //                    documentid2 = Convert.ToString(document.DocumentId);
            //                }
            //            }
            //            else
            //            {
            //                if (docpath == "pdf" || docpath == "PDF")
            //                {
            //                    documentid1 = Convert.ToString(document.DocumentId);
            //                }
            //            }
            //            #region Cloud File
            //            string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
            //            string relativeFileName = CloudStorageUtility.GetAbsoluteFileURL(containerName, document.AssignmentId + "/" + document.DocumentTypeId + "/" + document.DocumentPath);
            //            if ((docpath == "pdf" || docpath == "PDF") && docType == "0261")
            //            {
            //                PdfReader pdfReader = new PdfReader(FileToByteArray(relativeFileName, true));
            //                readerList.Add(pdfReader);
            //            }
            //            else if ((docpath == "pdf" || docpath == "PDF") && docType == "0265")
            //            {
            //                PdfReader pdfReader = new PdfReader(FileToByteArray(relativeFileName, true));
            //                readerList1.Add(pdfReader);
            //            }
            //            else
            //            {
            //                BLL.Document doc = css.Documents.Find(Convert.ToInt64(documentId));

            //                destfilename = "";
            //                indexfilename = "";
            //                getFileName(claimId, documentId, ref destfilename, ref indexfilename, ref memorystream);
            //                string fileURL = CloudStorageUtility.GetAbsoluteFileURL(ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString(), doc.AssignmentId + "/" + doc.DocumentTypeId + "/" + doc.DocumentPath);
            //                FTPUtility.UploadFile(fileURL, destfilename, company.FTPUserName, company.FTPPassword);
            //                FTPUtility.UploadFile(memorystream.ToArray(), indexfilename, company.FTPUserName, company.FTPPassword);
                            
            //            }
            //            #endregion
            //        }

            //        if (readerList.Count > 0)
            //        {
            //            MergeToPdf(readerList, company, claimId, documentid1, ref memorystream);
            //        }
            //        if (readerList1.Count > 0)
            //        {
            //            MergeToPdf(readerList1, company, claimId, documentid2, ref memorystream);

            //        }
            //        //DateTime CSTTimeStamp = css.usp_GetLocalDateTime().First().Value;
            //        //string docTimeStamp = CSTTimeStamp.ToString("MMddyyyyHHmmssFFF");



            //        //Create Index File
            //        //MemoryStream memoryStream = new MemoryStream();
            //        //TextWriter tw = new StreamWriter(memoryStream);
            //        //tw.WriteLine("polnum=" + policyNo);
            //        //tw.WriteLine("claimnum=" + claimNo);
            //        //tw.WriteLine("slevel=0261");
            //        //tw.WriteLine("lname=" + claim.InsuredLastName);
            //        //tw.WriteLine("fname=" + claim.InsuredFirstName);
            //        //tw.WriteLine("docstat=T");
            //        //tw.WriteLine("sloc=B");
            //        //tw.WriteLine("sdate=" + CSTTimeStamp.ToString("yyyyMMdd"));
            //        //tw.WriteLine("apptype=02");
            //        //tw.WriteLine("pdlname=" + claim.InsuredLastName);
            //        //tw.WriteLine("pdfname=" + claim.InsuredFirstName);
            //        //tw.WriteLine("docdate=#IMPDATE");




            //        //Upload Document to FTP
            //        //        PdfImportedPage page = writer1.GetImportedPage(reader, i);
            //    }
            //    else
            //    {
            //        foreach (string documentId in documentIdList)
            //        {
            //            BLL.Document doc = css.Documents.Find(Convert.ToInt64(documentId));
            //            destfilename = "";
            //            indexfilename = "";
            //            getFileName(claimId, documentId, ref destfilename, ref indexfilename, ref memorystream);
            //            #region Cloud File
            //            string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
            //            string relativeFileName = CloudStorageUtility.GetAbsoluteFileURL(containerName, doc.AssignmentId + "/" + doc.DocumentTypeId + "/" + doc.DocumentPath);
            //            #endregion
            //            string fileURL = CloudStorageUtility.GetAbsoluteFileURL(ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString(), doc.AssignmentId + "/" + doc.DocumentTypeId + "/" + doc.DocumentPath);
            //            FTPUtility.UploadFile(fileURL, destfilename, company.FTPUserName, company.FTPPassword);

            //            //Upload Index File to FTP

            //            FTPUtility.UploadFile(memorystream.ToArray(), indexfilename, company.FTPUserName, company.FTPPassword);


                        

            //        }
            //    }

            //    //Create a Note
            //    string noteBody = String.Empty;
            //    foreach (var fileName in fileNames)
            //    {
            //        if (!String.IsNullOrEmpty(noteBody))
            //        {
            //            noteBody += ", ";
            //        }
            //        noteBody += fileName;
            //    }

            //    noteBody = "Documents " + noteBody;
            //    noteBody += " were transfered to the FTP account.";

            //    CSS.Models.User user = CSS.AuthenticationUtility.GetUser();
            //    string subject = "FTP Transfer";
            //    Int64 assignmentId = css.PropertyAssignments.Where(x => x.ClaimId == claimId).OrderByDescending(x => x.AssignmentId).First().AssignmentId;
            //    ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int32);
            //    css.usp_NotesInsert(outNoteId, assignmentId, subject, noteBody, DateTime.Now, true, true, user.UserId, null, null, null, null,null,null);

            //    valueToReturn = "1";
            //}
            //catch (Exception ex)
            //{
            //    valueToReturn = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            //}

            //return Json(valueToReturn, JsonRequestBehavior.AllowGet);

            CSS.Models.User user = CSS.AuthenticationUtility.GetUser();

            ObjectParameter outFTPPushLogID = new ObjectParameter("FTPPushLogID", DbType.UInt64);

            if (mergedoc == "1")
            {
                css.usp_FTPPushInsert(outFTPPushLogID, claimId, documentIds, true, "Inserted for Push", false, DateTime.Now, user.UserId);
            }
            else
            {
                css.usp_FTPPushInsert(outFTPPushLogID, claimId, documentIds, false, "Inserted for Push", false, DateTime.Now, user.UserId);
            }

            valueToReturn = "1";
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        private void MergeToPdf(List<PdfReader> readerList, Company company, Int64 claimId, string documentId, ref MemoryStream memorystream)
        {
            string newFile = "";
            string destfilename = "";
            string indexfilename = "";
            newFile = Server.MapPath(ConfigurationManager.AppSettings["DocumentTempPath"].ToString() + Claim.ClaimId.ToString() + DateTime.Now.ToString("MMddyyymmss") + ".pdf");
            iTextSharp.text.Document Idoc = new iTextSharp.text.Document(PageSize.A4, 0, 0, 0, 0);
            PdfCopy pdfcopyprovider = new PdfCopy(Idoc, new FileStream(newFile, FileMode.Create));
            Idoc.Open();
            foreach (PdfReader reader in readerList)
            {
                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    PdfImportedPage page = pdfcopyprovider.GetImportedPage(reader, i);

                    pdfcopyprovider.AddPage(page);
                }
            }
            Idoc.Close();
            getFileName(claimId, documentId, ref destfilename, ref indexfilename, ref memorystream);
            FTPUtility.UploadFile(newFile, destfilename, company.FTPUserName, company.FTPPassword);
            if (System.IO.File.Exists(newFile) && newFile != "")
            {

                System.IO.File.Delete(newFile);
            }
            FTPUtility.UploadFile(memorystream.ToArray(), indexfilename, company.FTPUserName, company.FTPPassword);
        }
        private void getFileName(Int64 claimId, string DocumentId, ref string destfilename, ref string indexfilename, ref MemoryStream memorystream)
        {


            Claim claim = css.Claims.Find(claimId);
            BLL.Document doc = css.Documents.Find(Convert.ToInt64(DocumentId));
            Regex rgx = new Regex("[^a-zA-Z0-9]");
            string docType = "";

            //Build Destination File Name
            string policyNo = claim.PolicyNumber;
            policyNo = rgx.Replace(policyNo, "");

            string claimNo = claim.ClaimNumber;
            claimNo = rgx.Replace(claimNo, "");

            docType = "0261";//Documents: All Other Documents
            if ((doc.DocumentTypeId ?? 0) == 1 || (doc.DocumentTypeId ?? 0) == 3 || (doc.DocumentTypeId ?? 0) == 8 || doc.Title.ToUpper().StartsWith("INVOICE"))
            {
                //Documents: General Loss Report, Invoice
                docType = "0265";
            }

            DateTime CSTTimeStamp = css.usp_GetLocalDateTime().First().Value;
            string docTimeStamp = CSTTimeStamp.ToString("MMddyyyyHHmmssFFF");
            string fileExtension = doc.DocumentPath.Substring(doc.DocumentPath.LastIndexOf('.'), doc.DocumentPath.Length - doc.DocumentPath.LastIndexOf('.'));


            //Create Index File
            MemoryStream memoryStream = new MemoryStream();
            TextWriter tw = new StreamWriter(memoryStream);
            tw.WriteLine("polnum=" + policyNo);
            tw.WriteLine("claimnum=" + claimNo);
            tw.WriteLine("slevel=" + docType);
            if (claim.InsuredLastName.Contains("&"))
            {
                claim.InsuredLastName = claim.InsuredLastName.Replace("&", String.Empty);

            }

            tw.WriteLine("lname=" + claim.InsuredLastName);
            if (claim.InsuredFirstName.Contains("&"))
            {
                claim.InsuredFirstName = claim.InsuredFirstName.Replace("&", String.Empty);

            }
            tw.WriteLine("fname=" + claim.InsuredFirstName);
            tw.WriteLine("docstat=T");
            tw.WriteLine("sloc=B");
            tw.WriteLine("sdate=" + CSTTimeStamp.ToString("yyyyMMdd"));
            tw.WriteLine("apptype=02");
            tw.WriteLine("pdlname=" + claim.InsuredLastName);
            tw.WriteLine("pdfname=" + claim.InsuredFirstName);
            tw.WriteLine("docdate=#IMPDATE");
            tw.Flush();
            memoryStream.Flush();

            memorystream = memoryStream;

            //Upload Document to FTP
            destfilename = policyNo + "_" + claimNo + "_" + docType + "_" + docTimeStamp + fileExtension;

            //Upload Index File to FTP

            indexfilename = policyNo + "_" + claimNo + "_" + docType + "_" + docTimeStamp + ".idx";



        }
        public ActionResult AddCatCode(string catCode, string description)
        {
            int valueToReturn = 0;
            try
            {
                //check if the catcode already exists
                if (css.CatCodes.Where(x => x.CatCode1 == catCode).ToList().Count == 0)
                {
                    css.usp_CatCodesInsert(catCode, description);
                    valueToReturn = 1;
                }
                else
                {
                    valueToReturn = 2;
                }

            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddClientPOC(int headCompanyId, string firstName, string middleInitial, string lastName, string email)
        {
            Int64 valueToReturn = 0;
            try
            {
                string userName = firstName.Substring(0, 1).ToLower() + lastName.Replace(" ", "").ToLower();
                string password = "Password123";

                //check if the User Name already exists
                if (css.Users.Where(x => x.UserName == userName).ToList().Count != 0)
                {
                    valueToReturn = -2;//User Name already exists
                }
                else if (css.Users.Where(x => x.Email == email).ToList().Count != 0)
                {
                    valueToReturn = -3;//Email already exists
                }
                else
                {


                    ObjectParameter outUserID = new ObjectParameter("UserId", DbType.Int64);
                    css.usp_NewClientPOC(outUserID, headCompanyId, userName, password, firstName, middleInitial, lastName, email);
                    valueToReturn = Convert.ToInt64(outUserID.Value);
                }

            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCatCodesList()
        {
            List<SelectListItem> catCodesList = new List<SelectListItem>();
            catCodesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            try
            {
                foreach (CatCode catCode in css.CatCodes)
                {
                    catCodesList.Add(new SelectListItem { Text = catCode.CatCode1, Value = catCode.CatCode1 });
                }
            }
            catch (Exception ex)
            {

            }
            return Json(catCodesList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCSSPOCList()
        {
            List<SelectListItem> cssPOCList = new List<SelectListItem>();
            cssPOCList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            try
            {
                foreach (var cssPOC in css.CSSPointOfContactList().ToList())
                {
                    cssPOCList.Add(new SelectListItem { Text = cssPOC.UserFullName, Value = cssPOC.UserId + "" });
                }
            }
            catch (Exception ex)
            {

            }
            return Json(cssPOCList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetClientPOCList()
        {
            List<SelectListItem> cssPOCList = new List<SelectListItem>();
            cssPOCList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            try
            {
                foreach (var cssPOC in css.CSSPointOfContactList().ToList())
                {
                    cssPOCList.Add(new SelectListItem { Text = cssPOC.UserFullName, Value = cssPOC.UserId + "" });
                }
            }
            catch (Exception ex)
            {

            }
            return Json(cssPOCList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SplitSPPayGetView(Int64 claimId, Int64 invoiceId)
        {

            SplitSPPayViewModel viewModel = new SplitSPPayViewModel();
            viewModel.ClaimId = claimId;
            viewModel.InvoiceId = invoiceId;
            viewModel.SPList = new List<SplitSPPay>();
            //Get most recent assignment two SPs
            List<PropertyAssignment> paList = css.PropertyAssignments.Where(x => x.ClaimId == claimId).ToList();
            foreach (var pa in paList)
            {
                SplitSPPay sp = new SplitSPPay();
                sp.SPId = pa.OAUserID.Value;
                User user = css.Users.Find(sp.SPId);
                sp.SPFullName = user.FirstName + ' ' + user.LastName;

                //find the most recent invoice for this assignment
                PropertyInvoice invoice = css.PropertyInvoices.Where(x => x.AssignmentId == pa.AssignmentId).OrderByDescending(x => x.InvoiceId).First();
                sp.CurrentSPPayPercent = invoice.SPServiceFeePercent.Value;
                sp.CurrentSPPay = invoice.SPServiceFee.Value;
                if (invoice.InvoiceId == invoiceId)
                {
                    viewModel.SplitAmount = invoice.SPServiceFee.Value;
                }
                viewModel.SPList.Add(sp);
            }

            return Json(RenderPartialViewToString("_SplitSPPay", viewModel), JsonRequestBehavior.AllowGet);
        }

        public ActionResult SplitSPPay(SplitSPPayViewModel viewModel)
        {
            int valueToReturn = 0;
            try
            {
                css.usp_SplitSPPay(viewModel.SplitAmount, viewModel.ClaimId, viewModel.SPList[0].SPId, viewModel.SPList[0].AdjustedSPPayPercent, viewModel.SPList[1].SPId, viewModel.SPList[1].AdjustedSPPayPercent);
            }
            catch (Exception ex)
            {
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult AssigTimestampUpdate(Int64 claimId, Int64 assignmentId, string updateFieldCode, string note)
        {
            string valueToReturn = "0";
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            try
            {
                switch (updateFieldCode)
                {
                    case "SRR":
                        css.usp_AssigTimestampUpdate(assignmentId, updateFieldCode, null, note, loggedInUser.UserId);
                        break;
                }
                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Financials(Int64 claimid)
        {
            XACTClaimsInfoViewModel claiminfoviewModel = new XACTClaimsInfoViewModel(claimid);

            return Json(RenderPartialViewToString("_AssignmentDetailsFinancials", claiminfoviewModel), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult BulkAssignCSSPOC(string assignmentIds, Int64 CSSPOCUserId)
        {
            string valueToReturn = "0";
            try
            {
                foreach (string assignmentId in assignmentIds.Split(','))
                {
                    css.usp_AssignCSSPOC(Convert.ToInt64(assignmentId), CSSPOCUserId);
                }
                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult BulkAssignLOB(string assignmentIds, int LOBId = 0)
        {
            string valueToReturn = "0";

            try
            {
                foreach (string assignmentId in assignmentIds.Split(','))
                {
                    Int64 AssignmentId = Convert.ToInt64(assignmentId);
                    var claim = (from pd in css.Claims
                                 join od in css.PropertyAssignments on pd.ClaimId equals od.ClaimId
                                 where od.AssignmentId == AssignmentId
                                 select new { pd.ClaimId }).ToList();
                    if (claim.Count() > 0)
                    {

                        css.usp_ClaimLOBUpdate(claim[0].ClaimId, LOBId);
                    }
                }
                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetLOBList(Int64 CompanyId)
        {

            try
            {


                XactAssignmentSearchViewModel viewModel = new BLL.ViewModels.XactAssignmentSearchViewModel();

                viewModel.LOBList = css.GetLOBList(CompanyId).ToList();
                if (viewModel.LOBList.Count == 0)
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    IEnumerable<SelectListItem> LOBList = new List<SelectListItem>();

                    LOBList = (from m in viewModel.LOBList select m).AsEnumerable().Select(m => new SelectListItem() { Text = m.LOBDescription, Value = m.lobid.ToString() });
                    var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                    return Json(new SelectList(LOBList, "Value", "Text"), JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult BulkMarkNotesBillable(string noteIds)
        {
            string returnCode = "0";
            string returnData = String.Empty;

            try
            {
                foreach (string noteId in noteIds.Split(','))
                {
                    css.usp_NoteIsBillableUpdate(Convert.ToInt64(noteId), true);
                }
                returnCode = "1";
            }
            catch (Exception ex)
            {
                returnCode = "-1";
                returnData = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(new { Code = returnCode, Data = returnData }, JsonRequestBehavior.AllowGet);
        }
        private void getTemplateData( Int16 AuthorId,ref User usr,Int64 ClaimId)
        {

            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
           
            if (AuthorId == 1)
            {

                usr = css.Users.Find(loggedInUser.UserId);
                

            }
            else if (AuthorId == 2)
            {
                var userid = (from c in css.PropertyAssignments where c.ClaimId == ClaimId select c.CSSPointofContactUserId).FirstOrDefault();
                if (userid == null || userid == 0)
                {

                    usr = css.Users.Find(Convert.ToInt64(loggedInUser.UserId));
                }
                else
                {
                    usr = css.Users.Find(Convert.ToInt64(userid));
                }
               
            }
            else if (AuthorId == 3)
            {
                var userid = (from c in css.PropertyAssignments where c.ClaimId == ClaimId select c.CSSQAAgentUserId).FirstOrDefault();

                if (userid == null || userid == 0)
                {

                    usr = css.Users.Find(Convert.ToInt64(loggedInUser.UserId));
                }
                else
                {
                    usr = css.Users.Find(Convert.ToInt64(userid));
                }

               
            }
            else if (AuthorId == 4)
            {
                var userid = (from c in css.PropertyAssignments where c.ClaimId == ClaimId select c.OAUserID).FirstOrDefault();
                if (userid == null || userid == 0)
                {

                    usr = css.Users.Find(Convert.ToInt64(loggedInUser.UserId));
                }
                else
                {
                    usr = css.Users.Find(Convert.ToInt64(userid));
                }


                
            }
            else
            {
                Int64? userid = css.Claims.Find(ClaimId).ClientPointOfContactUserId.HasValue ? css.Claims.Find(ClaimId).ClientPointOfContactUserId.Value : 0;
                if (userid == null || userid == 0)
                {

                    usr = css.Users.Find(Convert.ToInt64(loggedInUser.UserId));
                }
                else
                {
                    usr = css.Users.Find(Convert.ToInt64(userid));
                }


                
            }
        }
        public ActionResult TemplateContent(Int32 TemplateId, Int64 ClaimId,Int16 AuthorId)
        {
            string data="";
            if (TemplateId != 0)
            {

                XACTClaimsInfoViewModel claiminfoviewModel = new XACTClaimsInfoViewModel(ClaimId);
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
              //  DateTime dt = new DateTime(2008, 3, 9, 16, 5, 7, 123);

               // string var = String.Format("{0:MM/dd/yyyy}", dt); 
              //  DateTime dt = new DateTime(Convert.ToInt64(claiminfoviewModel.claim.DateofLoss.ToString()));
                string AuthorName = "";
                User usr = null;
                List<string> PlaceHolderList = new List<string>();
                string[] arr1 = new string[] { "[ClaimNumber]", "[PolicyNumber]", "[DateofLoss]", "[InsuredFirstName]", 
                "[InsuredLastName]", "[InsuredAddress1]", "[InsuredAddress2]", 
                "[InsuredCity]", "[InsuredState]", "[InsuredZip]","[InsuredEmail]", "[PropertyAddress1]","[PropertyAddress2]","[PropertyCity]",
                "[PropertyState]","[PropertyZip]","[Signature]","[CurrentDate]","[DateReceived]" };
                PlaceHolderList.AddRange(arr1);

                List<string> ActualValueList = new List<string>();
                ActualValueList.Add(claiminfoviewModel.claim.ClaimNumber);
                ActualValueList.Add(claiminfoviewModel.claim.PolicyNumber);
                ActualValueList.Add(String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(claiminfoviewModel.claim.DateofLoss)));
               // ActualValueList.Add(claiminfoviewModel.claim.DateofLoss.ToString());
                ActualValueList.Add(claiminfoviewModel.claim.InsuredFirstName);
                ActualValueList.Add(claiminfoviewModel.claim.InsuredLastName);
                ActualValueList.Add(claiminfoviewModel.claim.InsuredAddress1);
                ActualValueList.Add(claiminfoviewModel.claim.InsuredAddress2);
                ActualValueList.Add(claiminfoviewModel.claim.InsuredCity);
                ActualValueList.Add(claiminfoviewModel.claim.InsuredState);
                ActualValueList.Add(claiminfoviewModel.claim.InsuredZip);
                ActualValueList.Add(claiminfoviewModel.claim.InsuredEmail);
                ActualValueList.Add(claiminfoviewModel.claim.PropertyAddress1);
                ActualValueList.Add(claiminfoviewModel.claim.PropertyAddress2);
                ActualValueList.Add(claiminfoviewModel.claim.PropertyCity);
                ActualValueList.Add(claiminfoviewModel.claim.PropertyState);
                ActualValueList.Add(claiminfoviewModel.claim.PropertyZip);
                getTemplateData(AuthorId, ref usr, ClaimId);
                   ActualValueList.Add(usr.FirstName + " " + usr.LastName);
                   ActualValueList.Add(String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(DateTime.Now)));
                   ActualValueList.Add(String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(claiminfoviewModel.claim.ClaimCreatedDate)));

                EmailTemplate emailtemplate = css.EmailTemplates.Find(TemplateId);

                 data = HttpUtility.HtmlDecode(emailtemplate.TemplateBody.ToString());

                for (int i = 0; i < PlaceHolderList.Count(); i++)
                {

                    data = data.Replace(PlaceHolderList[i], ActualValueList[i]);

                }
            }
            else
            {
                data = null;

            }
            return Json(data);
        }
        [HttpPost]
        public ActionResult SendEmailOrPdf(string EmailPdf, string Title, string To, string TempBody, string AssignmentId, string ClaimId, Int16 AuthorId, string LogoStatus)
        {
            string emailpdf = EmailPdf;
            string filename1 = "";
            string relativefilename = "";
            string userhome = "";
            string usermobile = "";
            string useremail = "";
            string useraddresspo = "";
            string useraddressline = "";
            string userstreetaddress = "";
            string cmplogo = "";
            string path1 = "";
            Claim clm = css.Claims.Find(Convert.ToInt64(ClaimId));
            Company cmp = css.Companies.Find(clm.HeadCompanyId);
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

            User user = null;
            getTemplateData(AuthorId, ref user, Convert.ToInt64(ClaimId));
            UserType usertype = css.UserTypes.Find(user.UserTypeId);
            if (!string.IsNullOrEmpty(LogoStatus))
            {
                if (LogoStatus == "2")
                {
                    path1 = Server.MapPath("~/Content/Images/CSSLogo01.gif");
                }
                else
                {
                    if (!string.IsNullOrEmpty(cmp.Logo))
                    {
                        cmplogo = cmp.Logo;

                    }
                }
            }
            if (!string.IsNullOrEmpty(user.HomePhone))
            {
                userhome = "HomePhone:" + user.HomePhone+"<br/>";
            }

            if (!string.IsNullOrEmpty(user.MobilePhone))
            {
                usermobile = "MobilePhone:" + user.MobilePhone+"<br/>";
            }

            if (!string.IsNullOrEmpty(user.Email))
            {
                useremail = "Email:" + user.Email+"<br/>";
            }
            if (!string.IsNullOrEmpty(user.AddressPO))
            {
                useraddresspo = user.AddressPO+"<br/>";

            }
            if (!string.IsNullOrEmpty(user.AddressLine1))
            {
                useraddressline = user.AddressLine1+"<br/>";
            }
            if (!string.IsNullOrEmpty(user.StreetAddress))
            {
                userstreetaddress = user.StreetAddress+"<br/>";
            }
            string baseFolder1 = System.Configuration.ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
            if (!string.IsNullOrEmpty(cmplogo))
            {
                string absoluteThumnailFileName = cmp.CompanyId + "/" + cmp.Logo;
                string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
                path1 = CloudStorageUtility.GetAbsoluteFileURL(containerName, absoluteThumnailFileName);
            }

           

            //string strlogo = "<table style='width:100%'><tr><tbody><td width='50%'>";
            //strlogo += "<img height='170px' width='200px' src=\"D:\\Projects\\Sajal choicesolution\\gaurav\\MVC4App\\CSS\\CSS\\Content\\Images\\CSSLogo01.gif\"></img>";
            //strlogo += "</td><td valign='top' style='text-align:right'> gaurav amarseda,<br />Charkop,Kandivli,<br />Sector-08,Room No:C35,<br /> Durga Society,<br />";
            //strlogo += "Mumbai-400067</td></tr></tbody></table><hr style='color:red'/>";

            string strlogo = "<table style='width:100%'><tr><tbody><td width='50%'>";
            strlogo += "<img src="+"'"+path1+"'"+"></img>";
            strlogo += "</td><td valign='top' style='text-align:right;font-size:17px;font-family:arial'></br>" + user.FirstName + " " + user.LastName + "<br />" + cmp.CompanyName + "<br />" + useraddresspo  + useraddressline  + userstreetaddress;
            strlogo += userhome + usermobile + useremail + "</td></tr></tbody></table><hr style='color:red'/>";


          //  string strlogo = "<img height='180px' width='230px' src=\"D:\\Projects\\Sajal choicesolution\\gaurav\\MVC4App\\CSS\\CSS\\Content\\Images\\CSSLogo01.gif\"></img>";
            
            TempBody = strlogo + TempBody;
            if (emailpdf == "1")
            {


               

                string htmlData = HttpUtility.HtmlDecode(TempBody);
                string cssfile = @"<style type='text/css'>.lnht5 {
    line-height: 5px;
    display:block;
}

.lnht10 {
    
    line-height: 10px;
    display:block;
}

.lnht15 {
    
    line-height: 15px;
    display:block;
}

.lnht20 {
  
    line-height: 20px;
    display:block;
}

.lnht25 {
    
    line-height: 25px;
    display:block;
}

.lnht30 {
    
    line-height: 30px;
    display:block;
}


body {
        padding-top: 5px;
    }</style>";
                htmlData = cssfile + htmlData;
                htmlData = htmlData.Replace("></p>", "></br></p>");
                htmlData = "<div style='width:100%;word-wrap:break-word;'>" + htmlData + "</div>";
                byte[] pdfData = Utility.ConvertHTMLStringToPDF(htmlData,50,50);
                #region Cloud Storage
                // extract only the fielname
                string fileName = DateTime.Now.ToString("MMddyyyy_HHmm") + ".pdf";
                //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                string assignmentId = AssignmentId + "";


                // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                relativefilename = AssignmentId + "/" + 1 + "/" + "Letter_" + assignmentId + "_" + loggedInUser.UserId + "_" + fileName;
                filename1 = "Letter_" + assignmentId + "_" + loggedInUser.UserId + "_" + fileName;

                string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                CloudStorageUtility.StoreFile(containerName, relativefilename, pdfData);
                #endregion


                //#region Local File System
                //// extract only the fielname
                //string fileName = DateTime.Now.ToString("MMddyyyy_HHmm") + ".pdf";
                //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                //string assignmentId = AssignmentId + "";
                //// string documentTypeId = document.DocumentTypeId + "";
                ////check whether the folder with user's userid exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId));
                //}
                ////check whether the destination folder depending on the type of document being uploaded exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId + "/" + 7)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId + "/" + 7));
                //}

                //filename1 = "Letter_" + assignmentId + "_" + loggedInUser.UserId + "_" + fileName;
                //// store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                //string path = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + 7), filename1);

                //Utility.StoreBytesAsFile(pdfData, path);
                //#endregion

                ObjectParameter outDocumentId = new ObjectParameter("DocumentId", DbType.Int64);
                css.DocumentsInsert(outDocumentId, Convert.ToInt64(AssignmentId), null, Title, filename1, filename1, loggedInUser.UserId, 1);

                return RedirectToAction("ClaimDocumentsList", new { claimId = ClaimId, assignmentId = AssignmentId, documentTypeId = 1 });
                    
            }
            else
            {

               // return RedirectToAction("EmailUtility", new { emailto = To, emailfrom = "", setReplyToEmail = 1, subject = Title, Content = TempBody, assignmentid = Convert.ToInt64(AssignmentId) });

                return EmailUtility(To, "", 1, Title, TempBody, Convert.ToInt64(AssignmentId));
                        
            }

            

        }
        public ActionResult GetNotesDocuments(Int64 NoteId)
        {
            try
            {
                return Json(css.usp_GetNotesEmailDocuments(NoteId).ToList(),JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex) { return Json(0,JsonRequestBehavior.AllowGet);; }
        }
        private long getDocFileSize(string urlpath)
        {
            Stream f=null;
            MemoryStream ms=null;
            long filesize = 0;
            try
            {
                 f = new WebClient().OpenRead(urlpath);
                 ms = new MemoryStream();

                f.CopyTo(ms);

                filesize = ms.Length;
            }
            catch (Exception ex)
            {
                filesize = 0;  
                throw;
                
            }
            finally
            {
              
                f.Dispose();
                ms.Dispose();
            }
            

          
            return filesize;

        }
        private static void ReadWriteStream(Stream readStream, Stream writeStream)
        {
            int Length = 25600000;
            Byte[] buffer = new Byte[Length];
            int bytesRead = readStream.Read(buffer, 0, Length);
            // FileStream outFile = null;
            // write the required bytes
            while (bytesRead > 0)
            {
                writeStream.Write(buffer, 0, bytesRead);
                bytesRead = readStream.Read(buffer, 0, Length);
            }
            readStream.Dispose();
            //  outFile = (FileStream)writeStream;
            //  writeStream.Close();
            writeStream.Dispose();

            //    return outFile;

        }
        public void ReadCloudFile(string relativefilepath, string docpath, ref List<string> attachmentFilePathList, ref List<string> files)
        {
            try
        {
            var request = (HttpWebRequest)WebRequest.Create(relativefilepath);

            using (var response = request.GetResponse())
            {

                using (Stream responseStream = response.GetResponseStream())
                {

                   string saveTo = Server.MapPath(ConfigurationManager.AppSettings["DocumentTempPath"].ToString() + docpath.ToString());
                 
                    // create a write stream
                    FileStream writeStream = new FileStream(saveTo, FileMode.Create, FileAccess.Write);
                    // write to the stream
                    ReadWriteStream(responseStream, writeStream);
                    attachmentFilePathList.Add(saveTo);
                    files.Add(saveTo);


                }
            }

        }
            catch (Exception ex)
            {
                throw;
            }
           

        }

        [HttpPost]
        public JsonResult ClaimExist(string Number, Int32 QuickSearchType = 0, string SearchString = "")
        {
            bool result = false;
            Int64 ClaimId = 0;
            string Claimnumber = "";

            Claim navigateclaim = new Claim();
            navigateclaim = css.Claims.Where(x => x.ClaimNumber.Equals(Number)).FirstOrDefault();

            Claim QuickSearch = new Claim();

            QuickSearch = css.Claims.Where(x => x.ClaimNumber.Equals(Number)).FirstOrDefault();

            if (!string.IsNullOrEmpty(SearchString))
            {
                Number = SearchString;
            }



            //if (QuickSearchType == 0)
            //{
            //    QuickSearch = css.Claims.Where(x => x.ClaimNumber.Equals(Number)).FirstOrDefault();
            //}
            //else if (QuickSearchType == 1)
            //{
            //    QuickSearch = css.Claims.Where(x => x.PolicyNumber.Equals(Number)).FirstOrDefault();
            //}
            //else if (QuickSearchType == 2)
            //{
            //    QuickSearch = (from claim in css.Claims
            //                  join assignment in css.PropertyAssignments on claim.ClaimId equals assignment.ClaimId
            //                  join invoice in css.PropertyInvoices on assignment.AssignmentId equals invoice.AssignmentId
            //                  where invoice.InvoiceNo.Equals(Number)
            //                  select claim).FirstOrDefault();

            //}
            //else if (QuickSearchType == 3)
            //{
            //    string[] Name = Number.Split(' ');
            //    if (Name.Length > 1)
            //    {
            //        string FirstName = Name[0];
            //        string LastName = Name[1];
            //        QuickSearch = css.Claims.Where(x => x.InsuredFirstName.Equals(FirstName) && x.InsuredLastName.Equals(LastName)).FirstOrDefault();
            //    }
            //    else
            //    {
            //        string FirstName = Name[0];
            //        QuickSearch = css.Claims.Where(x => x.InsuredFirstName.Equals(FirstName)).FirstOrDefault();
            //    }

            //}

            ClaimId = QuickSearch.ClaimId;
            Claimnumber = QuickSearch.ClaimNumber;
            result = true;
            if (QuickSearch != null && TempData["IsQuickSearch"] != null)
            {

                if (TempData["IsQuickSearch"].ToString() == "1")
                {
                    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                    if (QuickSearchType == 0)
                        css.usp_ClaimReviewHistoryInsert(ClaimId, Number, null, null, null, loggedInUser.UserId);
                    else if (QuickSearchType == 1)
                        css.usp_ClaimReviewHistoryInsert(ClaimId, null, Number, null, null, loggedInUser.UserId);
                    else if (QuickSearchType == 2)
                        css.usp_ClaimReviewHistoryInsert(ClaimId, null, null, Number, null, loggedInUser.UserId);
                    else if (QuickSearchType == 3)
                        css.usp_ClaimReviewHistoryInsert(ClaimId, null, null, null, Number, loggedInUser.UserId);

                    TempData["IsQuickSearch"] = "0";
                }
            }


            string claimNumber = Cypher.EncryptString(navigateclaim.ClaimId.ToString());
            return Json(new { ClaimIdNo = claimNumber, resultDisplay = result, QuickSearchTypes = QuickSearchType }, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public JsonResult BindQuickSearchList(int QuickSearchType = 0)
        {

            if (TempData["tmpQuickSearchtype"] != null)
            {
                QuickSearchType = Convert.ToInt16(TempData["tmpQuickSearchtype"].ToString());
            }
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            Int64 UserID = loggedInUser.UserId;
            if (QuickSearchType == 0)
            {
                //var QuickSearchList = (from c in css.ClaimReviewHistories
                //                       where c.UserId == UserID && c.ClaimNumber != null
                //                       orderby c.ClaimReviewHistoryId descending
                //                       select c.ClaimNumber).Distinct().Take(5).ToList();

                var QuickSearchList = css.ClaimReviewHistories.Where(a => a.UserId == UserID && a.ClaimNumber != null).Distinct().OrderByDescending(a => a.ClaimReviewHistoryId).Take(5).Select(c => c.ClaimNumber).ToList().Distinct();

                return Json(QuickSearchList, JsonRequestBehavior.AllowGet);

            }
            else if (QuickSearchType == 1)
            {
                //var QuickSearchList = (from c in css.ClaimReviewHistories
                //                       where c.UserId==UserID && c.PolicyNumber != null
                //                       orderby c.ClaimReviewHistoryId descending
                //                       select c.PolicyNumber).Distinct().Take(5).ToList();

                var QuickSearchList = css.ClaimReviewHistories.Where(a => a.UserId == UserID && a.PolicyNumber != null).Distinct().OrderByDescending(a => a.ClaimReviewHistoryId).Take(5).Select(c => c.PolicyNumber).ToList().Distinct();

                return Json(QuickSearchList, JsonRequestBehavior.AllowGet);
            }
            else if (QuickSearchType == 2)
            {
                //var QuickSearchList = (from c in css.ClaimReviewHistories
                //                       where c.UserId == UserID && c.InvoiceNumber != null
                //                       orderby c.ClaimReviewHistoryId descending
                //                       select c.InvoiceNumber).Distinct().Take(5).ToList();

                var QuickSearchList = css.ClaimReviewHistories.Where(a => a.UserId == UserID && a.InvoiceNumber != null).Distinct().OrderByDescending(a => a.ClaimReviewHistoryId).Take(5).Select(c => c.InvoiceNumber).ToList().Distinct();
                return Json(QuickSearchList, JsonRequestBehavior.AllowGet);
            }
            else if (QuickSearchType == 3)
            {
                //var QuickSearchList = (from c in css.ClaimReviewHistories
                //                       where c.UserId == UserID && c.InsuredName != null
                //                       orderby c.ClaimReviewHistoryId descending
                //                       select c.InsuredName).Distinct().Take(5).ToList();

                var QuickSearchList = css.ClaimReviewHistories.Where(a => a.UserId == UserID && a.InsuredName != null).Distinct().OrderByDescending(a => a.ClaimReviewHistoryId).Take(5).Select(c => c.InsuredName).ToList().Distinct();
                return Json(QuickSearchList, JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult BindClaimSearchList(Int32 QuickSearchType = 0)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            Int64 UserID = loggedInUser.UserId;
            if (QuickSearchType == 0)
            {
                var QuickSearchList1 = (from c in css.ClaimReviewHistories
                                        join cl in css.Claims on c.ClaimId equals cl.ClaimId
                                        where c.UserId == UserID && c.ClaimNumber != null
                                        orderby c.ClaimReviewHistoryId descending
                                        select cl).Distinct().Take(10).ToList().Distinct();


                var QuickSearchList = css.ClaimReviewHistories.Where(a => a.UserId == UserID && a.ClaimNumber != null).Distinct().OrderByDescending(a => a.ClaimReviewHistoryId).Take(10).Select(c => c.ClaimNumber).ToList().Distinct();
                return Json(QuickSearchList, JsonRequestBehavior.AllowGet);


            }
            else if (QuickSearchType == 1)
            {
                var QuickSearchList = (from c in css.ClaimReviewHistories
                                       where c.UserId == UserID && c.PolicyNumber != null
                                       orderby c.ClaimReviewHistoryId descending
                                       select c.PolicyNumber).Distinct().Take(5).ToList();

                return Json(QuickSearchList, JsonRequestBehavior.AllowGet);
            }
            else if (QuickSearchType == 2)
            {
                var QuickSearchList = (from c in css.ClaimReviewHistories
                                       where c.UserId == UserID && c.InvoiceNumber != null
                                       orderby c.ClaimReviewHistoryId descending
                                       select c.InvoiceNumber).Distinct().Take(5).ToList();
                return Json(QuickSearchList, JsonRequestBehavior.AllowGet);
            }
            else if (QuickSearchType == 3)
            {
                var QuickSearchList = (from c in css.ClaimReviewHistories
                                       where c.UserId == UserID && c.InsuredName != null
                                       orderby c.ClaimReviewHistoryId descending
                                       select c.InsuredName).Distinct().Take(5).ToList();
                return Json(QuickSearchList, JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public ActionResult _LastClaimsSearchList()
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            Int64 UserID = loggedInUser.UserId;
            TempData["IsQuickSearch"] = "1";
            return PartialView("_LastClaimsSearchList", css.GetLastClaimsSearched(UserID).ToList());
        }
        public ActionResult QuickSearch(string type = "", string QuickSearchType = "", string Number = "")
        {
            XactAssignmentSearchViewModel viewModel = new XactAssignmentSearchViewModel();
            TempData["IsQuickSearch"] = "1";
            TempData["tmpNumber"] = Number;
            TempData["tmpQuickSearchtype"] = QuickSearchType;
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            try
            {

                if (Session["XactAssignmentSearch"] != null)
                {
                    viewModel = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<XactAssignmentSearchViewModel>(Session["XactAssignmentSearch"].ToString());
                    //viewModel = (XactAssignmentSearchViewModel)Session["XactAssignmentSearch"];
                }
                else
                {
                    viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.GENERAL;
                }
                if (loggedInUser.UserTypeId == 11)
                {
                    //Client POC should only see their company. 17-01-2014
                    if (loggedInUser.HeadCompanyId.HasValue)
                    {
                        viewModel.InsuranceCompanies.RemoveAll(x => x.Value != loggedInUser.HeadCompanyId + "");
                    }
                    else
                    {
                        viewModel.InsuranceCompanies.RemoveAll(x => x.Value != "0");
                    }
                }
                if (!String.IsNullOrEmpty(type))
                {
                    if (type == "QAAgent_Review")
                    {
                        viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE;
                    }
                    else
                    {
                        if (viewModel.SearchMode == XactAssignmentSearchViewModel.ViewMode.QAAGENT_REVIEW_QUEUE)
                        {
                            //Reset the file status to select incase the user is coming back to claim search from QA Agent review queue
                            viewModel.FileStatus = 0;
                        }
                        viewModel.SearchMode = BLL.ViewModels.XactAssignmentSearchViewModel.ViewMode.GENERAL;
                    }
                }
                if (viewModel.Pager == null)
                {
                    viewModel.Pager = new BLL.Models.Pager();
                    viewModel.Pager.Page = 1;
                    viewModel.Pager.RecsPerPage = 20;
                    viewModel.Pager.FirstPageNo = 1;
                    viewModel.Pager.IsAjax = false;
                    viewModel.Pager.FormName = "formSearch";
                }

                if (QuickSearchType == "0")
                {
                    viewModel.ClaimNumber = Number;
                }
                if (QuickSearchType == "1")
                {
                    viewModel.PolicyNumber = Number;
                }
                if (QuickSearchType == "2")
                {
                    viewModel.InvoiceNumber = Number;
                }
                if (QuickSearchType == "3")
                {
                    viewModel.InsureName = Number;
                }
                if (QuickSearchType == "")
                {
                    viewModel.ClaimNumber = Number;
                }
                //viewModel.ClaimNumber = "";
                //viewModel.PolicyNumber = "";  
                //viewModel.ClaimNumber = Number;

                viewModel.ClaimsInfo = getAssignmentSearchResult(css.Users.Find(loggedInUser.UserId), viewModel).ToList();
                //if (viewModel.ClaimsInfo.Count() > 0)
                //{

                //    if (QuickSearchType == "0")
                //        css.usp_ClaimReviewHistoryInsert(1, Number, null, null, null, loggedInUser.UserId);

                //    else if (QuickSearchType == "1")
                //        css.usp_ClaimReviewHistoryInsert(1, null, Number, null, null, loggedInUser.UserId);

                //    else if (QuickSearchType == "2")
                //        css.usp_ClaimReviewHistoryInsert(1, null, null, Number, null, loggedInUser.UserId);

                //    else if (QuickSearchType == "3")
                //        css.usp_ClaimReviewHistoryInsert(1, null, null, null, Number, loggedInUser.UserId);
                //}



                viewModel.Pager.TotalCount = viewModel.ClaimsInfo[0].TotalRecords.ToString();
                //Session["XactAssignmentSearch"] = null;

                viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();

            }
            catch (Exception ex)
            {
                string abc = ex.Message;
            }
            ModelState.Remove("PolicyNumber");
            viewModel.PolicyNumber = "";
            ModelState.Remove("ClaimNumber");
            viewModel.ClaimNumber = null;
            ModelState.Remove("LastName");
            viewModel.LastName = null;
            ModelState.Remove("City");
            viewModel.City = null;
            ModelState.Remove("Zip");
            viewModel.Zip = null;
            ModelState.Remove("Distance");
            viewModel.Distance = null;
            ModelState.Remove("State");
            viewModel.State = "0";
            ModelState.Remove("CatCode");
            viewModel.CatCode = null;
            ModelState.Remove("LossType");
            viewModel.LossType = "0";
            ModelState.Remove("FileStatus");
            viewModel.FileStatus = 0;
            ModelState.Remove("DateFrom");
            viewModel.DateFrom = null;
            ModelState.Remove("DateTo");
            viewModel.DateTo = null;
            ModelState.Remove("InsuranceCompany");
            viewModel.InsuranceCompany = 0;
            ModelState.Remove("CSSPOCUserId");
            viewModel.CSSPOCUserId = 0;
            ModelState.Remove("CSSQAAgentUserId");
            viewModel.CSSQAAgentUserId = 0;
            ModelState.Remove("DateReceivedFrom");
            viewModel.DateReceivedFrom = null;
            ModelState.Remove("DateReceivedTo");
            viewModel.DateReceivedTo = null;
            ModelState.Remove("IsTriageAvailable");
            viewModel.IsTriageAvailable = false;
            ModelState.Remove("SPName");
            viewModel.SPName = null;
            ModelState.Remove("SPName");
            viewModel.InvoiceNumber = null;
            ModelState.Remove("InvoiceNumber");

            return View("Search", viewModel);
        }
    }
}
