﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BLL.ViewModels;
using System.Data.Objects;
using System.Data;
using System.IO;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Web.UI;
using LumenWorks.Framework.IO.Csv;

using System.Data.OleDb;
using System.Configuration;
using System.Data.SqlClient;
using CSS;
namespace CSS.Controllers
{
    public class ClaimImportController : Controller
    {
        //
        // GET: /ClaimImport/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ClaimImport()
        {
            return View();
        }

        public ActionResult UploadSuccess()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file1)
        {

            if (file1 != null && file1.ContentLength > 0)
            {
                CSS.Models.User user = CSS.AuthenticationUtility.GetUser();
                // extract only the fielname
                string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(file1.FileName);
                string baseFolder = ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
                string userId = user.UserId + "";
                //string documentTypeDesc = Url.Encode(css.DocumentTypes.Find(viewModel.ServiceProviderDocument.DTId).DocumentDesc.Replace(" ", "_"));
                //check whether the folder with user's userid exists
                if (!Directory.Exists(Server.MapPath(baseFolder + "" + userId)))
                {
                    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + userId));
                }
                //check whether the destination folder depending on the type of document being uploaded exists
                if (!Directory.Exists(Server.MapPath(baseFolder + "" + user.UserId)))
                {
                    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + userId));
                }

                // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                string path = Path.Combine(Server.MapPath(baseFolder + "" + userId), fileName);
                file1.SaveAs(path);
                ClaimImportViewModel claimimportviewmodel = new ClaimImportViewModel();

                CSV(path, fileName, ref claimimportviewmodel);

                return View("UploadSuccess", claimimportviewmodel);

               // return RedirectToAction("UploadSuccess",);

            }
            else
            {

            }

            return RedirectToAction("ClaimImport");

        }

        public void CSV(string path, string filename, ref ClaimImportViewModel claimimportviewmodel)
        {

            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            ObjectParameter outClaimId = new ObjectParameter("ClaimId", DbType.Int64);
            ObjectParameter outAssignmentId = new ObjectParameter("AssignmentId", DbType.Int64);
            ObjectParameter outStatusFlag = new ObjectParameter("StatusFlag", DbType.Int64);
            CSS.Models.User user = CSS.AuthenticationUtility.GetUser();
            try
            {

                string Con = ConfigurationManager.ConnectionStrings["ChoiceSolutionsEntities"].ToString();
                CsvReader objReader = new CsvReader(new StreamReader(path), true);

                while (objReader.ReadNextRecord())
                {

                    int spaceAt;
                    spaceAt = objReader[4].IndexOf(' ');

                    int deduct = 0; //
                    string firstname = "";
                    string lastname = "";//objReader[5].Substring(0, spaceAt);
                    if (spaceAt > 0)
                    {
                        deduct = spaceAt + 1;
                        int lenghtstr = objReader[4].Length - deduct;
                        firstname = objReader[4].Substring(0, spaceAt);
                        lastname = objReader[4].Substring(spaceAt + 1, lenghtstr);
                    }
                    else
                    {
                        firstname = objReader[4];
                        lastname = "";
                    }

                    css.ClaimsInsertCSV(outClaimId, outAssignmentId, objReader[2], objReader[0], Convert.ToDateTime(objReader[10]), Convert.ToDateTime(objReader[9]), DateTime.Now, user.UserId, objReader[1], firstname, lastname, objReader[12], objReader[13], objReader[11], objReader[3], objReader[15], objReader[16], outStatusFlag);

                    claimimportviewmodel.statusflag = Convert.ToInt32(outStatusFlag.Value);
                    if (Convert.ToInt32(claimimportviewmodel.statusflag) == 1 || Convert.ToInt32(claimimportviewmodel.statusflag) == 2)
                    {
                        break;
                    }
                }

            }
            catch (Exception e)
            {

            }

        }



    }
}
