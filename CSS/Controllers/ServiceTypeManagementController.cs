﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BLL.Models;
using BLL.ViewModels;
using System.Data;
using System.Data.SqlClient;
using System.Data.Linq.SqlClient;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Data.Objects;


namespace CSS.Controllers
{
    public class ServiceTypeManagementController : CustomController
    {
        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public ActionResult ServiceTypes()
        {
            ServiceTypesManagementViewModel viewmodel = new ServiceTypesManagementViewModel();
            viewmodel.ServiceTypes = (from c in css.ServiceOfferings
                                      where c.IsDeleted == false
                                      orderby c.ServiceDescription
                                      select c).ToList();

            if (viewmodel.Pager == null)
            {
                viewmodel.Pager = new BLL.Models.Pager();
                viewmodel.Pager.Page = 1;
                viewmodel.Pager.RecsPerPage = 15;
                viewmodel.Pager.FirstPageNo = 1;
                viewmodel.Pager.IsAjax = false;
                viewmodel.Pager.FormName = "frmServiceTypesMgt";
            }

            viewmodel.searchTypesListSearchResult = RenderPartialViewToString("_ServiceTypeList", viewmodel);

            return View(viewmodel);
        }


        public string AddNewServiceType(string serviceTypeName, string process)
        {
            using(ChoiceSolutionsEntities css = new ChoiceSolutionsEntities())
            {
                if(TempData["ServiceTypeId"] != null && process == "U")
                {
                    int serviceTypeId = Convert.ToInt32(TempData["ServiceTypeId"]);
                    ServiceOffering objToUpdated = css.ServiceOfferings.Find(serviceTypeId);
                    objToUpdated.ServiceDescription = serviceTypeName;
                    objToUpdated.UpdatedOn = DateTime.Now;
                    css.SaveChanges();
                }
                else
                {
                    //ServiceId is not Identity column and hence we need to pass ServiceId explicitly
                    //As we not modify existing column to make it identity just pass next possible ServiceId
                    Int32 nextServiceId = css.ServiceOfferings.Max(m => m.ServiceId);

                    ServiceOffering obj = new ServiceOffering();
                    obj.ServiceId = nextServiceId + 1;
                    obj.CreatedOn = DateTime.Now;
                    obj.IsDeleted = false;
                    obj.ServiceDescription = serviceTypeName;
                    css.ServiceOfferings.Add(obj);
                    css.SaveChanges();
                }
                

                return GetSearchServiceListString();
            }
        }

        public ActionResult Edit(string serviceTypeId)
        {
            using (ChoiceSolutionsEntities css = new ChoiceSolutionsEntities())
            {
                int intServiceTypeId = Convert.ToInt32(serviceTypeId);
                ServiceOffering obj = (
                                            from c in css.ServiceOfferings
                                            where c.ServiceId == intServiceTypeId && c.IsDeleted == false
                                            select c
                                      ).FirstOrDefault();
                if (obj != null)
                {
                    TempData["ServiceTypeId"] = obj.ServiceId;
                }
                else
                {
                    TempData["ServiceTypeId"] = 0;
                }
                return Json(obj.ServiceDescription, JsonRequestBehavior.AllowGet);
            }
        }

        public string Delete(string serviceTypeId)
        {
            using (ChoiceSolutionsEntities css = new ChoiceSolutionsEntities())
            {
                ServiceOffering obj = css.ServiceOfferings.Find(Convert.ToInt32(serviceTypeId));
                obj.IsDeleted = true;
                obj.UpdatedOn = DateTime.Now;
                css.SaveChanges();

                return GetSearchServiceListString();
            }
        }

        private string GetSearchServiceListString()
        {
            using (ChoiceSolutionsEntities css = new ChoiceSolutionsEntities())
            {
                ServiceTypesManagementViewModel viewmodel = new ServiceTypesManagementViewModel();
                viewmodel.ServiceTypes = (from c in css.ServiceOfferings
                                          where c.IsDeleted == false
                                          orderby c.ServiceDescription
                                          select c).ToList();

                if (viewmodel.Pager == null)
                {
                    viewmodel.Pager = new BLL.Models.Pager();
                    viewmodel.Pager.Page = 1;
                    viewmodel.Pager.RecsPerPage = 15;
                    viewmodel.Pager.FirstPageNo = 1;
                    viewmodel.Pager.IsAjax = false;
                    viewmodel.Pager.FormName = "frmServiceTypesMgt";
                }

                viewmodel.searchTypesListSearchResult = RenderPartialViewToString("_ServiceTypeList", viewmodel);
                return viewmodel.searchTypesListSearchResult;
            }
            return "";
        }
    }
}