﻿using System.Xml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ViewModels;
using System.Xml.Serialization;
using System.IO;
using CSS.CssXactAssignmentService;
using System.Data.Objects;
using BLL.ViewModels;
using BLL;
using System.Data;
using System.Net;
namespace CSS.Controllers
{
    public class XactAssignmentController : CustomController
    {

        [Authorize]
        public ActionResult ExportAssignment()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult ExportAssignment(ExportAssignmentViewModel model, HttpPostedFileBase fu1)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.ResponseErrorCode = "";
                    model.ResponseErrorDesc = "";
                    string webServiceURL = System.Configuration.ConfigurationManager.AppSettings["CSSXactWebServiceURL"].ToString();
                    string assignmentUploadPath = System.Configuration.ConfigurationManager.AppSettings["XactAssignmentExportPath"].ToString();
                    string filename = DateTime.Now.ToString("MMddyyyy_HHmmss_") + new FileInfo(fu1.FileName).Name; 
                    string completeFilepath = Server.MapPath(assignmentUploadPath) + "" + filename;
                    fu1.SaveAs(completeFilepath);

                    //XmlSerializer mySerializer = new XmlSerializer(typeof(CssXactAssignmentService.XACTDOC));
                    //Stream myReader = new FileStream(completeFilepath, FileMode.Open);
                    //CssXactAssignmentService.XACTDOC objXACTDOC = (CssXactAssignmentService.XACTDOC)mySerializer.Deserialize(myReader);
                    //myReader.Close();

                    XmlDocument myXmlDoc = new XmlDocument();
                    Stream myReader = new FileStream(completeFilepath, FileMode.Open);
                    myXmlDoc.Load(myReader);
                    myReader.Close();

                    //XACTServiceSoapClient qs = new XACTServiceSoapClient();
                    //AuthHeader Credentials = new AuthHeader();
                    //Credentials.UserName = model.WebServiceUID;
                    //Credentials.Password = CryptographyUtility.Encrypt(model.WebServicePWD);


                    ////ExportAssignmentResult response = new ExportAssignmentResult();
                    ////response = qs.ExportAssignment(Credentials, objXactDocExport);

                    //ExportAssignmentAsXmlDocumentResult response = new ExportAssignmentAsXmlDocumentResult();
                    //response = qs.ExportAssignmentAsXmlDocument(Credentials, myXmlDoc);

                    //model.ResponseErrorCode = response.XactAnalysisExportResult.errorCode;
                    //model.ResponseErrorDesc = response.XactAnalysisExportResult.errorDescription;

                    WebRequest webReq = WebRequest.Create(webServiceURL);
                    //webReq.Headers["Application"] = "ExportAssignmentPOST";

                    string authInfo;
                    authInfo = model.WebServiceUID + ":" + model.WebServicePWD;
                    authInfo = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(authInfo));
                    webReq.Headers["Authorization"] = "Basic " + authInfo;
                    webReq.ContentType = "text/xml";
                    webReq.Method = "POST";
                    StringWriter xmlStringWriter = new StringWriter();
                    XmlTextWriter xmlTextWriter = new XmlTextWriter(xmlStringWriter);
                    myXmlDoc.WriteTo(xmlTextWriter);

                    byte[] bytes = System.Text.Encoding.ASCII.GetBytes(xmlStringWriter.ToString());
                    webReq.ContentLength = bytes.Length;
                    Stream os = webReq.GetRequestStream();
                    os.Write(bytes, 0, bytes.Length);
                    os.Close();

                    WebResponse webResp = webReq.GetResponse();

                    System.IO.StreamReader sr = new System.IO.StreamReader(webResp.GetResponseStream());

                    string response = sr.ReadToEnd().ToString();
                    XmlDocument xmlResponse = new XmlDocument();
                    xmlResponse.LoadXml(response);
                    XmlNodeList xmlNodes = xmlResponse.SelectNodes("XactAnalysisExportResponse/XactAnalysisExportResult/errorCode");
                    model.ResponseErrorCode = xmlNodes[0].InnerText;
                    xmlNodes = xmlResponse.SelectNodes("XactAnalysisExportResponse/XactAnalysisExportResult/errorDescription");
                    model.ResponseErrorDesc = xmlNodes[0].InnerText;


                }
            }
            catch (Exception ex)
            {
                model.ResponseErrorCode = "Application";
                model.ResponseErrorDesc = ex.Message;
            }
            return View(model);
        }

        [Authorize]
        public ActionResult ImportAssignment()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult ImportAssignment(FormCollection form)
        {
            TempData["Response"] = null;
            #region Generate XML File
            string claimNumber = form["txtClaimNumber"];
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            Claim claim = css.Claims.Where(x => x.ClaimNumber == claimNumber).FirstOrDefault();
            XACTClaimsInfo xactClaimsInfo = css.XACTClaimsInfoes.Where(x => x.ClaimId == claim.ClaimId).FirstOrDefault();
            XACTDOC myXACTDOC = new XACTDOC();
            myXACTDOC.ADM = new XACTDOCADM();
            myXACTDOC.ADM.COVERAGE_LOSS = new XACTDOCADMCOVERAGE_LOSS();
            myXACTDOC.ADM.COVERAGE_LOSS.claimNumber = claim.ClaimNumber;
            myXACTDOC.XACTNET_INFO = new XACTDOCXACTNET_INFO();
            myXACTDOC.XACTNET_INFO.transactionId = xactClaimsInfo.transactionId;

            XmlSerializer myXMLSerializer = new XmlSerializer(myXACTDOC.GetType());
            StringWriter myStringWriter = new StringWriter();
            myXMLSerializer.Serialize(myStringWriter,myXACTDOC);

            string xmlFile = myStringWriter.ToString();
            #endregion

            #region Push XML File to XACTAnalysis
            //WebRequest webReq = WebRequest.Create("https://testedi.xactnet.com:443");
            WebRequest webReq = WebRequest.Create("http://testedi.xactnet.com:80");
            webReq.Headers["Application"] = "ImportAssignment";
            //webReq.Headers["Application"] = "ExportAssignment";

            string authInfo;
            authInfo = "ChoiceSolution" + ":" + "choice2012";
            authInfo = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(authInfo));
            webReq.Headers["Authorization"] = "Basic " + authInfo;
            webReq.ContentType = "text/xml";
            webReq.Method = "POST";

            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(xmlFile);
            webReq.ContentLength = bytes.Length;
            Stream os = webReq.GetRequestStream();
            os.Write(bytes, 0, bytes.Length);
            os.Close();

            WebResponse webResp = webReq.GetResponse();

            System.IO.StreamReader sr = new System.IO.StreamReader(webResp.GetResponseStream());

            string webResponseContent = sr.ReadToEnd().Trim();
            TempData["Response"] = webResponseContent;
            #endregion

            return View();
        }

    }
}
