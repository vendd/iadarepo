﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BLL.Models;
using BLL.ViewModels;
using System.Data.Objects;
using System.Data;
using System.IO;
using System.Configuration;

using System.Drawing;
using System.IO;
using System.Web.UI;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Drawing.Imaging;
namespace CSS.Controllers
{
    [System.Web.Mvc.Authorize]
    public class ServiceProviderRegistrationController : Controller
    {

        #region Basic Functionality

        #region Objects & Variables

        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
      //  ClaimType claimtype = new ClaimType();
      //  User objUser = new User();
      //  ServiceProviderDetail objServiceProviderDetails = new ServiceProviderDetail();
     
        #endregion
      
        #region Actions
        [AllowAnonymous]
        public ActionResult Submit(Int64? SPID=-1,String SPType=null)
        {
            ServiceProviderRegistrationViewModel viewModel;
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            //User Loggin as SP and is non admin
            if (SPType == "SP")
                SPID = loggedInUser.UserId;

            if (SPID == -1)
            {
                //The user is about to register
                viewModel = new ServiceProviderRegistrationViewModel();
            }
            else
            {
                //The user is editing an existing record
                viewModel = new ServiceProviderRegistrationViewModel(SPID.Value);

                //Branch Location should pull the address of Main Locaion
                if (viewModel.user.User1 != null)
                {
                    viewModel.user.StreetAddress = viewModel.user.User1.StreetAddress; 
                }
                //if User login as a Service Provider and who not an admin user
                if (loggedInUser != null && loggedInUser.UserTypeId != 2 && SPType=="SP")
                {
                    viewModel.user.City = "";
                    viewModel.user.State = "";
                    viewModel.user.Zip = "";
                    viewModel.user.UserId = -1;
                    viewModel.user.UserName = "";
                    viewModel.user.Password = "";
                    viewModel.user.MgrId = SPID;
                }
                else
                {
                    viewModel.user.Password = Cypher.DecryptString(Cypher.ReplaceCharcters(viewModel.user.Password));
                }
              
             }
            return View(viewModel);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Submit(FormCollection collection, HttpPostedFileBase profileImage, ServiceProviderRegistrationViewModel viewModel, Int64 ID = -1)
        {
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                viewModel.user.UserId = ID;
                if (isFormValid(viewModel, collection, ID))
                {
                    if (ID == -1)
                    {
                        #region Users Table

                        int hdnResiRowCount = Convert.ToInt32(collection["hdnResiRowCount"]);
                        viewModel.user.Password = Cypher.EncryptString(collection["user.Password"].ToString());
                        viewModel.user.BusinessDesc = collection["user.Company1.BusinessDesc"];
                        ObjectParameter outUserid = new ObjectParameter("UserId", DbType.Int32);

                        css.SPRegistrationUsersInsert(outUserid, viewModel.user.UserName, viewModel.user.Password, 1, viewModel.user.FirstName, viewModel.user.MiddleInitial,
                             viewModel.user.LastName, viewModel.user.StreetAddress, viewModel.user.City, viewModel.user.State, viewModel.user.Zip, "",
                             viewModel.user.Email, viewModel.user.Twitter, viewModel.user.Facebook, viewModel.user.SSN, viewModel.user.HomePhone, "",
                             viewModel.user.MobilePhone, "", "", true, viewModel.user.GooglePlus, viewModel.user.AddressLine1, viewModel.user.AddressPO,
                             viewModel.user.NetworkProviderId, viewModel.user.BusinessDesc, viewModel.user.LinkedInAddress, viewModel.user.JobTitle, viewModel.user.HeadCompanyId);


                        viewModel.user.UserId = ID = Convert.ToInt64(outUserid.Value);

                        css.SaveChanges();
                        #endregion

                        #region ServiceProviderDetails Table

                        //string QbVendorid = null;                         
                        //User user = css.Users.Find(viewModel.user.UserId);                   
                        //if(user.MgrId != null && user.MgrId != 0)
                        //{                            
                        //    ServiceProviderDetail spd = css.ServiceProviderDetails.Find(user.MgrId);
                        //    QbVendorid = spd.QBVendorId;
                        //}

                        css.usp_ServiceProviderDetailsNewInsert(Convert.ToInt64(outUserid.Value), viewModel.user.ServiceProviderDetail.IsKnowByOtherNames, viewModel.user.ServiceProviderDetail.KnowByOtherNames, viewModel.user.ServiceProviderDetail.DBA_LLC_Firm_Corp,
                            viewModel.user.ServiceProviderDetail.TaxID, viewModel.user.ServiceProviderDetail.MobileHome, viewModel.user.ServiceProviderDetail.Rank, viewModel.user.ServiceProviderDetail.DriversLicenceNumber,
                            viewModel.user.ServiceProviderDetail.DriversLicenceState, isDatetime(viewModel.user.ServiceProviderDetail.DOB), viewModel.user.ServiceProviderDetail.IsDBA, viewModel.user.ServiceProviderDetail.FederalTaxClassification,
                            viewModel.user.ServiceProviderDetail.LLCTaxClassification, viewModel.user.ServiceProviderDetail.AppraiserLicense, viewModel.user.ServiceProviderDetail.AdjusterLicense, viewModel.user.ServiceProviderDetail.BusinessLicense,
                            viewModel.user.ServiceProviderDetail.CompanyAppraisersCount, viewModel.user.ServiceProviderDetail.AppraiserType, viewModel.user.ServiceProviderDetail.IsProvideTrainingOfAppraisers, viewModel.user.ServiceProviderDetail.HasOfficeReviewCompleted, isFloat(viewModel.user.ServiceProviderDetail.ReviewedPercentage),
                            viewModel.user.ServiceProviderDetail.IsProvideApprHandlingInsts, isFloat(viewModel.user.ServiceProviderDetail.ReInspectionPercentage), isInteger(viewModel.user.ServiceProviderDetail.ATATimeOfAssignment), viewModel.user.ServiceProviderDetail.TechTrainingCerti, isFloat(viewModel.user.ServiceProviderDetail.AudatexEST),
                            isFloat(viewModel.user.ServiceProviderDetail.MitchellEST), isFloat(viewModel.user.ServiceProviderDetail.CCCEST), viewModel.user.ServiceProviderDetail.OtherDesc, isFloat(viewModel.user.ServiceProviderDetail.OtherEST), isInteger(viewModel.user.ServiceProviderDetail.CompanyLast12monthEST), viewModel.user.ServiceProviderDetail.AppraisalManagementDesc);

                        css.SaveChanges();
                        #endregion

                        #region Serviceofferings

                        foreach (var serviceID in viewModel.SelectedServiceTypes ?? new int[0])
                        {
                            css.usp_SPServiceOfferingsInsert(Convert.ToInt64(outUserid.Value), serviceID);
                        }

                        #endregion


                        #region Send Welcome email and notification email to Admin
                        //List<string> to = new List<string>();
                        //to.Add(user.Email);
                        //string mailBody = "Dear " + user.FirstName + " " + user.MiddleInitial + " " + user.LastName + ",";
                        //mailBody += "<br />";
                        //mailBody += "<br />Welcome to IADA Claim Solutions Inc. Your account has now been created.";
                        //mailBody += "<br />";
                        //mailBody += "<br />You can now login to your account using the following credentials.";
                        //mailBody += "<br />";
                        //mailBody += "<br />Website Address: " + ConfigurationManager.AppSettings["WebAppURL"].ToString();
                        //mailBody += "<br />User Name: " + user.UserName;
                        //mailBody += "<br />Password: " + user.Password;
                        //mailBody += "<br />";
                        //mailBody += "<br />";
                        //mailBody += "<br />";
                        //mailBody += "<br />";
                        //mailBody += "<br />Thanking you,";
                        //mailBody += "<br />";
                        //mailBody += "<br />Support,";
                        //mailBody += "<br />IADA Claim Solutions Inc.";
                        //Utility.sendEmail(to, MvcApplication.AdminMail, "Welcome to IADA Claim Solutions Inc.", mailBody);

                        //Admin Notification
                        var adminEmailList = css.Users.Where(x => x.UserTypeId == 2).Select(y => y.Email).ToList();
                        if (adminEmailList.Count > 0)
                        {
                            List<string> toEmails = new List<string>();
                            foreach (var email in adminEmailList)
                            {
                                if (!string.IsNullOrEmpty(email))
                                {
                                    toEmails.Add(email);
                                }
                            }
                            if (toEmails.Count > 0)
                            {
                         string mailBody = "<br />";
                                mailBody += "DO NOT REPLY TO THIS EMAIL.  THIS IS A SYSTEM GENERATED EMAIL.  THE EMAIL MAILBOX IS NOT MONITORED.";
                                mailBody += "<br />";
                                mailBody += "<br />";
                                mailBody += "Dear IADA Customer Support Team,";
                                mailBody += "<br />";
                                mailBody += "<br /> The following new user has been registered to IADApro.com.";
                                mailBody += "<br />";
                                mailBody += "<br />Name: " + viewModel.user.FirstName + " " + viewModel.user.MiddleInitial + " " + viewModel.user.LastName + "";
                                mailBody += "<br />User Name: " + viewModel.user.UserName;
                                mailBody += "<br />Company: " + viewModel.user.Company1.CompanyName;
                                mailBody += "<br />Address: " + viewModel.user.City + ", " + viewModel.user.State + " " + viewModel.user.Zip;
                                mailBody += "<br />Email: " + viewModel.user.Email;
                                mailBody += "<br />Phone: " + viewModel.user.MobilePhone;
                                mailBody += "<br />";
                                mailBody += "<br />";
                                mailBody += "<br />";
                                mailBody += "<br />Thank you,";
                                mailBody += "<br />";
                                mailBody += "IADA Customer Support Team";                               
                                Utility.sendEmail(toEmails, MvcApplication.AdminMail, "New User Registered - " + viewModel.user.Company1.CompanyName,mailBody);
                            }
                        }

                        #endregion



                    }
                    else
                    {
                        viewModel.user.UserId = ID;

                        #region Users Table

                        int hdnResiRowCount = Convert.ToInt32(collection["hdnResiRowCount"]);
                        viewModel.user.Password = Cypher.EncryptString(collection["user.Password"].ToString());

                        string website = collection["user.Company1.WebsiteURL"];
                        string CompanyName = collection["user.Company1.CompanyName"];
                        viewModel.user.BusinessDesc = collection["user.Company1.BusinessDesc"];
                        css.SPRegistrationUsersUpdate(ID, viewModel.user.UserName, viewModel.user.Password, viewModel.user.FirstName, viewModel.user.MiddleInitial,
                        viewModel.user.LastName, viewModel.user.StreetAddress, viewModel.user.City, viewModel.user.State, viewModel.user.Zip, "",
                        viewModel.user.Email, viewModel.user.Twitter, viewModel.user.Facebook, viewModel.user.SSN, viewModel.user.HomePhone, viewModel.user.WorkPhone,
                        viewModel.user.MobilePhone, viewModel.user.Pager, viewModel.user.OtherPhone, viewModel.user.GooglePlus, viewModel.user.AddressLine1, loggedInUser.UserId, DateTime.Now,
                        viewModel.user.AddressPO, viewModel.user.NetworkProviderId, viewModel.user.BusinessDesc, viewModel.IsIadaMember, viewModel.user.LinkedInAddress, viewModel.user.JobTitle,
                        viewModel.user.HeadCompanyId, website, CompanyName);


                        #endregion


                        #region ServiceProviderDetails Table

                        css.usp_ServiceProviderDetailsUpdateNew(ID, viewModel.user.ServiceProviderDetail.IsKnowByOtherNames, viewModel.user.ServiceProviderDetail.KnowByOtherNames, viewModel.user.ServiceProviderDetail.DBA_LLC_Firm_Corp,
                           viewModel.user.ServiceProviderDetail.TaxID, 1, viewModel.user.ServiceProviderDetail.MobileHome, viewModel.user.ServiceProviderDetail.Rank,
                            isDatetime(viewModel.user.ServiceProviderDetail.DOB), viewModel.user.ServiceProviderDetail.IsDBA, viewModel.user.ServiceProviderDetail.FederalTaxClassification,
                           viewModel.user.ServiceProviderDetail.LLCTaxClassification, null, loggedInUser.UserId, viewModel.user.ServiceProviderDetail.AppraiserLicense, viewModel.user.ServiceProviderDetail.AdjusterLicense, viewModel.user.ServiceProviderDetail.BusinessLicense,
                           viewModel.user.ServiceProviderDetail.CompanyAppraisersCount, viewModel.user.ServiceProviderDetail.AppraiserType, viewModel.user.ServiceProviderDetail.IsProvideTrainingOfAppraisers, viewModel.user.ServiceProviderDetail.HasOfficeReviewCompleted, isFloat(viewModel.user.ServiceProviderDetail.ReviewedPercentage),
                           viewModel.user.ServiceProviderDetail.IsProvideApprHandlingInsts, isFloat(viewModel.user.ServiceProviderDetail.ReInspectionPercentage), isInteger(viewModel.user.ServiceProviderDetail.ATATimeOfAssignment), viewModel.user.ServiceProviderDetail.TechTrainingCerti, isFloat(viewModel.user.ServiceProviderDetail.AudatexEST),
                           isFloat(viewModel.user.ServiceProviderDetail.MitchellEST), isFloat(viewModel.user.ServiceProviderDetail.CCCEST), viewModel.user.ServiceProviderDetail.OtherDesc, isFloat(viewModel.user.ServiceProviderDetail.OtherEST), isInteger(viewModel.user.ServiceProviderDetail.CompanyLast12monthEST), viewModel.user.ServiceProviderDetail.AppraisalManagementDesc,
                           viewModel.user.ServiceProviderDetail.DriversLicenceNumber, viewModel.user.ServiceProviderDetail.DriversLicenceState);

                        css.SaveChanges();

                        #endregion



                        #region Serviceofferings

                        css.usp_SPServiceOfferingsDelete(ID);

                        foreach (var serviceID in viewModel.SelectedServiceTypes ?? new int[0])
                        {
                            css.usp_SPServiceOfferingsInsert(Convert.ToInt64(ID), serviceID);
                        }

                        #endregion

                        #region Update is IADA Member in Company table

                        //bool IsIadaMember = Convert.ToBoolean(collection["chkIADAMember"]);                       
                        //objUser.HeadCompanyId = userExistingData.HeadCompanyId;
                        //css.UpdateCompanyIsIADA(objUser.HeadCompanyId, IsIadaMember);

                        #endregion

                    }
                    #region Company Profile Image Process and upload

                    if (profileImage != null && profileImage.ContentLength > 0 && viewModel.user.HeadCompanyId != null && ID > -1)
                    {
                        string imagePath = String.Empty;
                        UploadDocument(viewModel.user, profileImage, out imagePath);
                    }
                    #endregion

                    #region Redirect to Appropriate page
                    if (loggedInUser == null)
                    {
                        return RedirectToAction("Login", "Acount");
                    }
                    else if (loggedInUser.UserId == viewModel.user.UserId)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else if (loggedInUser != null && loggedInUser.UserTypeId != 2)
                    {
                        return RedirectToAction("Search", "SPMapSearch", new { sptype = "act" });
                    }
                    else
                    {
                        return RedirectToAction("Search", "ServiceProviders");
                    }
                    #endregion
                }
               
            }
            catch (Exception ex)
            {
                ViewBag.ExceptionErrorMsg = ex.Message + "<br/>" + ex.InnerException + "<br/>" + ex.StackTrace;
            }
            return View(new ServiceProviderRegistrationViewModel(viewModel.user));

        }
        private Int32? isInteger(object value)
        {
            Int32 RValue;
            if (value!=null&&Int32.TryParse(value.ToString(),out RValue))
            {
                return RValue;
            }
            return null;
        }
        private Double? isFloat(object value)
        {
            float RValue;
            if (value != null && float.TryParse(value.ToString(), out RValue))
            {
                return RValue;
            }
            return null;
        }
        private DateTime? isDatetime(object value)
        {
            DateTime RValue;
            if (value != null && DateTime.TryParse(value.ToString(), out RValue))
            {
                return RValue;
            }
            return null;
        }
        public ActionResult EmailMarketingSPRegistration()
        {
            return View();
        }

        private int getProfileProgressValue(User user)
        {
            int percentageCompleted = Utility.getProfileProgressValue(user);

            return percentageCompleted;
        }

        public ActionResult SubmitSuccess()
        {
            return View();
        }
        public ActionResult UpdateSuccess()
        {
            return View();
        }

        public ActionResult DocumentsList(Int64 userId, int documentTypeId, int maxDocsCount, int displayMode, bool isReadOnly)
        {
            ServiceProviderDocumentsViewModel documentViewModel = new ServiceProviderDocumentsViewModel(userId, documentTypeId, maxDocsCount, displayMode, isReadOnly);
            documentViewModel.ServiceProviderDocument.Title = css.DocumentTypes.Find(documentTypeId).DocumentDesc;

            return View(documentViewModel);
        }

        [HttpPost]
        public ActionResult UploadFile(ServiceProviderDocumentsViewModel viewModel, HttpPostedFileBase file1)
        {

            if (file1 != null && file1.ContentLength > 0)
            {
                #region CloudStorage
                //extract only the fielname
                string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(file1.FileName);
                string userId = viewModel.ServiceProviderDocument.UserId + "";
                string documentTypeDesc = Url.Encode(css.DocumentTypes.Find(viewModel.ServiceProviderDocument.DTId).DocumentDesc.Replace(" ", "_"));

                // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                //store the file inside the container. The file name would include directory information starting after the container name. 
                string relativeFileName = userId + "/" + documentTypeDesc + "/" + fileName;

                MemoryStream bufferStream = new System.IO.MemoryStream();
                file1.InputStream.CopyTo(bufferStream);
                byte[] buffer = bufferStream.ToArray();

                string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
                CloudStorageUtility.StoreFile(containerName, relativeFileName, buffer);

                if (viewModel.DisplayMode == 1)
                {
                    //If an image is being uploaded
                    Image originalImage = Image.FromStream(file1.InputStream);
                    Image thumbnail = CSS.Utility.generateThumbnail(originalImage);
                    FileInfo file = new FileInfo(fileName);

                    string thumbnailFileName = Utility.getThumbnailFileName(file.Name);
                    string thumbnailRelativeFileName = userId + "/" + documentTypeDesc + "/" + thumbnailFileName;

                    MemoryStream thumbnailStream = new MemoryStream();
                    thumbnail.Save(thumbnailStream, System.Drawing.Imaging.ImageFormat.Jpeg);

                    byte[] bufferThumbnail = thumbnailStream.ToArray();
                    CloudStorageUtility.StoreFile(containerName, thumbnailRelativeFileName, bufferThumbnail);

                }
                #endregion
                #region FileSystem
                ////extract only the fielname
                //string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(file1.FileName);
                //string baseFolder = ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
                //string userId = viewModel.ServiceProviderDocument.UserId + "";
                //string documentTypeDesc = Url.Encode(css.DocumentTypes.Find(viewModel.ServiceProviderDocument.DTId).DocumentDesc.Replace(" ", "_"));

                ////check whether the folder with user's userid exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + userId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + userId));
                //}
                ////check whether the destination folder depending on the type of document being uploaded exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + viewModel.ServiceProviderDocument.UserId + "/" + documentTypeDesc)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc));
                //}

                //// store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                //string path = Path.Combine(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc), fileName);
                //file1.SaveAs(path);

                //if (viewModel.DisplayMode == 1)
                //{
                //    //If an image is being uploaded
                //    Image originalImage = Image.FromFile(path);
                //    Image thumbnail = CSS.Utility.generateThumbnail(originalImage);
                //    FileInfo file = new FileInfo(path);

                //    string thumbnailFileName = Utility.getThumbnailFileName(file.Name);
                //    string thumbnailPath = Path.Combine(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc), thumbnailFileName);

                //    thumbnail.Save(thumbnailPath, System.Drawing.Imaging.ImageFormat.Jpeg);

                //}
                #endregion
                ServiceProviderDocument document = new ServiceProviderDocument();
                document.UserId = viewModel.ServiceProviderDocument.UserId;
                document.DTId = viewModel.ServiceProviderDocument.DTId;
                document.Title = viewModel.ServiceProviderDocument.Title;
                if (document.Title == null || document.Title.Trim().Length == 0)
                {
                    document.Title = css.DocumentTypes.Find(viewModel.ServiceProviderDocument.DTId).DocumentDesc;
                }
                document.Path = fileName;
                document.UploadedDate = DateTime.Now;
                css.ServiceProviderDocuments.Add(document);
                css.SaveChanges();

            }

            return RedirectToAction("DocumentsList", new { userId = viewModel.ServiceProviderDocument.UserId, documentTypeId = viewModel.ServiceProviderDocument.DTId, maxDocsCount = viewModel.MaxDocsCount, displayMode = viewModel.DisplayMode, isReadOnly = viewModel.IsReadOnly });
        }

        public ActionResult DeleteFile(Int64 SPDId, Int64 userId, int documentTypeId, int maxDocsCount, int displayMode, bool isReadOnly)
        {
            ServiceProviderDocument document = css.ServiceProviderDocuments.Where(x => x.SPDId == SPDId).First();


            //delete physical file 
            #region Cloud Storage
            string documentTypeDesc = Url.Encode(css.DocumentTypes.Find(documentTypeId).DocumentDesc.Replace(" ", "_"));
            string fileName = document.Path;
            string relativeFileName = userId + "/" + documentTypeDesc + "/" + fileName;
            string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
            CloudStorageUtility.DeleteFile(containerName, relativeFileName);

            if (displayMode == 1)
            {
                string thumbnailFileName = Utility.getThumbnailFileName(fileName);
                string thumbnailPath = document.Path.Replace(fileName, thumbnailFileName);
                string thumbnailFilePath = userId + "/" + documentTypeDesc + "/" + thumbnailFileName;
                CloudStorageUtility.DeleteFile(containerName, thumbnailFileName);
            }
            #endregion

            #region Local File System

            //string baseFolder = ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
            //string documentTypeDesc = Url.Encode(css.DocumentTypes.Find(documentTypeId).DocumentDesc.Replace(" ", "_"));
            //string fileName = document.Path;
            //string filePath = Path.Combine(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc), fileName);
            //if (System.IO.File.Exists(filePath))
            //{
            //    try
            //    {
            //        System.IO.File.Delete(filePath);
            //    }
            //    catch (IOException ex)
            //    {
            //    }
            //}

            //if (displayMode == 1)
            //{
            //    string thumbnailFileName = Utility.getThumbnailFileName(fileName);
            //    string thumbnailPath = document.Path.Replace(fileName, thumbnailFileName);
            //    string thumbnailFilePath = Path.Combine(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc), thumbnailFileName);
            //    if (System.IO.File.Exists(thumbnailPath))
            //    {
            //        try
            //        {
            //            System.IO.File.Delete(thumbnailPath);
            //        }
            //        catch (IOException ex)
            //        {
            //        }
            //    }
            //}

            #endregion
            //delete database record

            css.ServiceProviderDocuments.Remove(document);
            css.SaveChanges();
            return RedirectToAction("DocumentsList", new { userId = userId, documentTypeId = documentTypeId, maxDocsCount = maxDocsCount, displayMode = displayMode, isReadOnly = isReadOnly });
        }

        public ActionResult ProcessESigningDoc(Int64 userId, int documentTypeID)
        {
            DocumentType docType = css.DocumentTypes.Find(documentTypeID);
            string docLink = Utility.getESigningDocumentLink(userId, documentTypeID);
            ProcessESigningDocViewModel model = new ProcessESigningDocViewModel(userId, documentTypeID, docType.DocumentDesc, docLink);
            return View(model);
        }


        #endregion

        #region Methods

        public ActionResult EditInternalContact(Int64 UserID)
        {
            ServiceProviderRegistrationViewModel viewModel = new ServiceProviderRegistrationViewModel();            
            if(UserID != -1)
            {
                BLL.User user = new User();
                viewModel.user = css.Users.Where(x => x.UserId == UserID).FirstOrDefault();
            }
            return PartialView("_InternalContactRegistration", viewModel);
        }

        public ActionResult SaveInternalContact(FormCollection Form, Boolean? IsDefault)
        {
            Int64? UserID = Utility.IsValidInt64(Form["user.userId"]); //Convert.ToInt64(Form["user.userId"]);
            string FirstName = (Form["user.FirstName"]);
            string LastName = (Form["user.LastName"]);
            string MiddleName = (Form["user.MiddleInitial"]);
            string Email = (Form["user.Email"]);
            string Mobile = (Form["user.MobilePhone"]);
            Int64 HeadCompanyID = Convert.ToInt64(Form["user.HeadCompanyId"]);
            string UserName = FirstName + LastName;
            string Password = UserName + "123";
            //string IsDefault1 = (Form["IsDefault"]);
            bool isFormValid=true;
            string errorsList="<ul>";
            if (string.IsNullOrEmpty(FirstName))
            {
                isFormValid = false;
                errorsList = errorsList + "<li>First Name Is Required!</li>";
            }
            else if (string.IsNullOrEmpty(LastName))
            {
                isFormValid = false;
                errorsList = errorsList + "<li>Last Name Is Required!</li>";
            }
            else if (string.IsNullOrEmpty(Email))
            {
                isFormValid = false;
                errorsList = errorsList + "<li>Email Is Required!</li>";
            }
            errorsList = errorsList + "</ul>";
            if (isFormValid==false)
            {
                return Json(new { success = 0, Errorlist = errorsList }, JsonRequestBehavior.AllowGet);
            }

            if (UserID == null || UserID == -1)
            {
                ObjectParameter outUserid = new ObjectParameter("UserID", DbType.Int32);
                css.InternalContactInsert(outUserid, UserName, Password, 1011, HeadCompanyID, FirstName, MiddleName, LastName, Email, Mobile, (IsDefault??false));
                //1011 is Internal Contact
            }
            else
            {
                css.InternalContactUpdate(UserID, FirstName, MiddleName, LastName, Email, Mobile, (IsDefault ?? false));
            }

            //if(IsDefault1 == "true,false")
            //{
            //    css.InternalContactIsDefaultUpdate(UserID, HeadCompanyID);
            //}
            return Json(new { success = 1, Errorlist = "" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetInternalContactList(Int64 CompanyID)
        {
             ServiceProviderRegistrationViewModel viewModel = new ServiceProviderRegistrationViewModel();
             viewModel.InternalContactGetList = css.InternalContactGetList(CompanyID).ToList();

             return PartialView("_InternalContactList",viewModel);
        }

        public ActionResult RejectInternalContact(Int64 UserID)
        {
            if (UserID != null || UserID != 0)
            {
                //BLL.Company company = css.Companies.Where(x => x.IsDefault == UserID).FirstOrDefault();
                BLL.AutoAssignment autoAssignment = css.AutoAssignments.Where(x => x.CSSPointofContactUserId == UserID).FirstOrDefault();
                if (autoAssignment == null)
                {
                    //ObjectParameter outIsReject = new ObjectParameter("IsReject", DbType.Int64);
                    //css.RejectClaimRep(outIsReject, UserID);
                    css.InternalContactReject(UserID);
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(1, JsonRequestBehavior.AllowGet);                 
                }
            }
            return View();
        }

        public bool isFormValid(ServiceProviderRegistrationViewModel viewModel, FormCollection collection, Int64 id = -1)
        {
            bool isValid = true;
            //DBA_LLC_Firm_Corp and FederalTaxClassification check
                  if (viewModel.user.ServiceProviderDetail.IsDBA == true)
                    {
                        if (String.IsNullOrEmpty(viewModel.user.ServiceProviderDetail.DBA_LLC_Firm_Corp))
                        {
                            ModelState.AddModelError("user.ServiceProviderDetail.DBA_LLC_Firm_Corp", "Please Enter LLC/Corp Name.");
                        }
                        if (viewModel.user.ServiceProviderDetail.FederalTaxClassification == "0")
                        {
                            ModelState.AddModelError("user.ServiceProviderDetail.FederalTaxClassification", "Please select Federal Tax Classification.");
                        }
                    }
              
            //User Name Already Exists
            if (collection["user.UserName"].Trim().Length != 0)
            {
                ObjectParameter outStatus = new ObjectParameter("statusFlag", DbType.Binary);
                css.IsUserNameInUse(outStatus, collection["user.UserName"]);

                if ((Convert.ToBoolean(outStatus.Value) == true && id == -1))
                {
                    isValid = false;
                    ModelState.AddModelError("UserName", "User Name already exists.");
                }
            }


           
            //Password = Confirm Password ?
            if (collection["user.Password"] != collection["user.PasswordConfirm"])
            {
                isValid = false;
                ModelState.AddModelError("Password", "Password & Confirm Password do not match.");
            }

            //City,Zip,STate Validation
            if (Utility.IsEmptyFormField(collection["user.Zip"]))
            {
                   isValid = false;
                   ModelState.AddModelError("Zip", "Zip Code Is Required!");
            }
            if (Utility.IsEmptyFormField(collection["user.City"]))
            {
                isValid = false;
                ModelState.AddModelError("City", "City Is Required!");
            }
            if (Utility.IsEmptyFormField(collection["user.State"]))
            {
                isValid = false;
                ModelState.AddModelError("State", "State Is Required!");
            }

            //LLCTaxClassification Left Blank
            bool isLLCTaxClassificationValid = true;
            if (viewModel.user.ServiceProviderDetail.FederalTaxClassification == "L")
            {
                isLLCTaxClassificationValid = false;
                if (!String.IsNullOrEmpty(viewModel.user.ServiceProviderDetail.LLCTaxClassification))
                {
                    if (viewModel.user.ServiceProviderDetail.LLCTaxClassification != "0")
                    {
                        isLLCTaxClassificationValid = true;
                    }
                }
            }
            if (!isLLCTaxClassificationValid)
            {
                isValid = false;
                ModelState.AddModelError("LLCTaxClassification", "LLC Tax Classification is required.");
            }

            //Attribute Driven Validation
            ModelState.Remove("user.Company1.Email");
            if (viewModel.user.HeadCompanyId == null || viewModel.user.HeadCompanyId == 0)
            {
                ModelState.Remove("user.Company1.CompanyName");
            }

            if (ModelState.IsValid == false)
            {
                isValid = false;
            }
           
            //if (collection["user.Email"]!=null&&collection["user.Email"].Trim().Length != 0)
            //{
            //    string Email = collection["user.Email"];
            //    var objemail = css.Users.Where(x => (x.Email == Email) && (x.UserId != id)).ToList().Count;
            //    if (Convert.ToInt32(objemail) > 0)
            //    {
            //        isValid = false;
            //        ModelState.AddModelError("Email", "Email already exists.");
            //    }
            //}

            //if (collection["user.SSN"].Trim().Length != 0)
            //{
            //    string SSN = collection["user.SSN"];
            //    var objssn = css.Users.Where(x => (x.SSN == SSN.Replace("-", "")) && (x.UserId != id)).ToList().Count;
            //    if (Convert.ToInt32(objssn) > 0)
            //    {
            //        isValid = false;
            //        ModelState.AddModelError("SSN", "SSN already exists.");
            //    }
            //}

            return isValid;
        }

        public ActionResult SSNUnique(string SSNValue)
        {
            var obj = from user in css.Users
                      where user.SSN == SSNValue
                      select user.SSN;
            if (Convert.ToInt32(obj.Count()) <= 0)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult EmailUnique(string Emailtext)
        {
            var obj = from user in css.Users
                      where user.Email == Emailtext
                      select user.Email;
            if (Convert.ToInt32(obj.Count()) <= 0)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult LicenseDocsUpload(Int64 userId)
        {
            LicenseDocsUploadViewModel viewModel = new LicenseDocsUploadViewModel(userId);
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult LicenseDocsUpload(LicenseDocsUploadViewModel viewModel, HttpPostedFileBase licenseFile)
        {
            LicenseDocsUploadViewModel newViewModel = new LicenseDocsUploadViewModel();
            ServiceProviderLicens spLicense = null;
            if (viewModel.SPLId != null)
            {
                if (viewModel.SPLId != 0)
                {
                    spLicense = css.ServiceProviderLicenses.Find(viewModel.SPLId);
                }
            }

            newViewModel.UserId = viewModel.UserId;
            newViewModel.SPLId = viewModel.SPLId;
            newViewModel.License = spLicense;
            newViewModel.populateLists();
            bool isDocUploadedFlag = false;
            if (spLicense != null)
            {
                if (spLicense.IsDocUploaded.HasValue)
                {
                    if (spLicense.IsDocUploaded.Value)
                    {
                        isDocUploadedFlag = true;
                    }
                }
            }


            //upload document
            if (licenseFile != null && licenseFile.ContentLength > 0 && viewModel.SPLId != 0 && isDocUploadedFlag == false)
            {
                #region Cloud Storage
                // extract only the fielname
                string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(licenseFile.FileName);
                string userId = viewModel.UserId + "";
                string documentTypeDesc = ConfigurationManager.AppSettings["ServiceProviderLicenseDocsFolderName"].ToString();

                // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                string relativeFileName = userId + "/" + documentTypeDesc + "/" + fileName;

                MemoryStream bufferStream = new System.IO.MemoryStream();
                licenseFile.InputStream.CopyTo(bufferStream);
                byte[] buffer = bufferStream.ToArray();

                string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
                CloudStorageUtility.StoreFile(containerName, relativeFileName, buffer);

                #endregion
                #region Local File System

                //// extract only the fielname
                //string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(licenseFile.FileName);
                //string baseFolder = ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
                //string userId = viewModel.UserId + "";
                ////string documentTypeDesc = Url.Encode(css.DocumentTypes.Find(viewModel.ServiceProviderDocument.DTId).DocumentDesc.Replace(" ", "_"));
                //string documentTypeDesc = ConfigurationManager.AppSettings["ServiceProviderLicenseDocsFolderName"].ToString();

                ////check whether the folder with user's userid exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + userId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + userId));
                //}
                ////check whether the destination folder depending on the type of document being uploaded exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc));
                //}

                //// store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                //string path = Path.Combine(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc), fileName);
                //licenseFile.SaveAs(path);
                #endregion
                spLicense.FileName = fileName;
                spLicense.IsDocUploaded = true;
                css.Entry(spLicense).State = EntityState.Modified;
                css.SaveChanges();
            }


            return View(newViewModel);
        }
        public ActionResult LicenseDocsDelete(Int64 SPLId)
        {
            ServiceProviderLicens license = css.ServiceProviderLicenses.Find(SPLId);


            //delete physical file 

            #region Cloud Storage
            string documentTypeDesc = ConfigurationManager.AppSettings["ServiceProviderLicenseDocsFolderName"].ToString();
            string fileName = license.FileName;
            Int64 userId = license.UserId.Value;
            string relativeFileName = userId + "/" + documentTypeDesc + "/" + fileName;
            string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
            CloudStorageUtility.DeleteFile(containerName, relativeFileName);

            #endregion

            #region Local File System

            //string baseFolder = ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
            //string documentTypeDesc = ConfigurationManager.AppSettings["ServiceProviderLicenseDocsFolderName"].ToString();
            //string fileName = license.FileName;
            //Int64 userId = license.UserId.Value;
            //string filePath = Path.Combine(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc), fileName);
            //if (System.IO.File.Exists(filePath))
            //{
            //    try
            //    {
            //        System.IO.File.Delete(filePath);
            //    }
            //    catch (IOException ex)
            //    {
            //    }
            //}
            #endregion

            license.IsDocUploaded = false;
            license.FileName = null;
            css.Entry(license).State = EntityState.Modified;
            css.SaveChanges();
            LicenseDocsUploadViewModel viewModel = new LicenseDocsUploadViewModel(license.UserId.Value);
            return RedirectToAction("LicenseDocsUpload", new { userId = license.UserId.Value });
        }

        public ActionResult CertificationDocsUpload(Int64 userId)
        {
            CertificationDocsUploadViewModel viewModel = new CertificationDocsUploadViewModel(userId);
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult CertificationDocsUpload(CertificationDocsUploadViewModel viewModel, HttpPostedFileBase certificationFile)
        {
            CertificationDocsUploadViewModel newViewModel = new CertificationDocsUploadViewModel();
            ServiceProviderCertification spCertification = null;
            if (viewModel.SPCId != null)
            {
                if (viewModel.SPCId != 0)
                {
                    spCertification = css.ServiceProviderCertifications.Find(viewModel.SPCId);
                }
            }

            newViewModel.UserId = viewModel.UserId;
            newViewModel.SPCId = viewModel.SPCId;
            newViewModel.Certification = spCertification;
            newViewModel.populateLists();
            bool isDocUploadedFlag = false;
            if (spCertification != null)
            {
                if (spCertification.IsDocUploaded.HasValue)
                {
                    if (spCertification.IsDocUploaded.Value)
                    {
                        isDocUploadedFlag = true;
                    }
                }
            }


            //upload document
            if (certificationFile != null && certificationFile.ContentLength > 0 && viewModel.SPCId != 0 && isDocUploadedFlag == false)
            {
                #region Cloud Storage
                // extract only the fielname
                string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(certificationFile.FileName);
                string userId = viewModel.UserId + "";
                string documentTypeDesc = ConfigurationManager.AppSettings["ServiceProviderCertificationDocsFolderName"].ToString();

                string relativeFileName = userId + "/" + documentTypeDesc + "/" + fileName;

                MemoryStream bufferStream = new System.IO.MemoryStream();
                certificationFile.InputStream.CopyTo(bufferStream);
                byte[] buffer = bufferStream.ToArray();

                string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
                CloudStorageUtility.StoreFile(containerName, relativeFileName, buffer);
                #endregion

                #region Local File Sytem

                //// extract only the fielname
                //string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(certificationFile.FileName);
                //string baseFolder = ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
                //string userId = viewModel.UserId + "";
                ////string documentTypeDesc = Url.Encode(css.DocumentTypes.Find(viewModel.ServiceProviderDocument.DTId).DocumentDesc.Replace(" ", "_"));
                //string documentTypeDesc = ConfigurationManager.AppSettings["ServiceProviderCertificationDocsFolderName"].ToString();

                ////check whether the folder with user's userid exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + userId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + userId));
                //}
                ////check whether the destination folder depending on the type of document being uploaded exists
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc));
                //}

                //// store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                //string path = Path.Combine(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc), fileName);
                //certificationFile.SaveAs(path);
                #endregion

                spCertification.DocFileName = fileName;
                spCertification.IsDocUploaded = true;
                spCertification.DocUploadedDate = DateTime.Now;
                css.Entry(spCertification).State = EntityState.Modified;
                css.SaveChanges();
            }


            return View(newViewModel);
        }
        public ActionResult CertificationDocsDelete(Int64 SPCId)
        {
            ServiceProviderCertification certification = css.ServiceProviderCertifications.Find(SPCId);


            //delete physical file 
            #region Cloud Storage

            string documentTypeDesc = ConfigurationManager.AppSettings["ServiceProviderCertificationDocsFolderName"].ToString();
            string fileName = certification.DocFileName;
            Int64 userId = certification.UserId.Value;
            string relativeFileName = userId + "/" + documentTypeDesc + "/" + fileName;
            string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
            CloudStorageUtility.DeleteFile(containerName, relativeFileName);

            #endregion

            #region Local File System

            //string baseFolder = ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
            //string documentTypeDesc = ConfigurationManager.AppSettings["ServiceProviderCertificationDocsFolderName"].ToString();
            //string fileName = certification.DocFileName;
            //Int64 userId = certification.UserId.Value;
            //string filePath = Path.Combine(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc), fileName);
            //if (System.IO.File.Exists(filePath))
            //{
            //    try
            //    {
            //        System.IO.File.Delete(filePath);
            //    }
            //    catch (IOException ex)
            //    {
            //    }
            //}

            #endregion

            certification.IsDocUploaded = false;
            certification.DocFileName = null;
            certification.DocUploadedDate = null;
            css.Entry(certification).State = EntityState.Modified;
            css.SaveChanges();
            CertificationDocsUploadViewModel viewModel = new CertificationDocsUploadViewModel(certification.UserId.Value);
            return RedirectToAction("CertificationDocsUpload", new { userId = certification.UserId.Value });
        }
        #endregion

        [HttpPost]
        public JsonResult GetLLCFirm(string taxid)
        {
            var DBALLCFirmCorp = from u in css.ServiceProviderDetails
                                 where (u.TaxID == taxid)
                                 select u.DBA_LLC_Firm_Corp;
            if (DBALLCFirmCorp.ToList().Count != 0)
            {
                return Json(DBALLCFirmCorp.First());
            }
            else
                return Json("");
        }

        [HttpPost]
        public JsonResult GetProgressIndicatorValueAsJSON(Int64 userId)
        {
            int progressValue = 0;
            try
            {
                User user = css.Users.Find(userId);
                progressValue = Utility.getProfileProgressValue(user);
            }
            catch (Exception ex)
            {
                progressValue = -1;
            }
            return Json(progressValue);
        }
        [HttpPost]
        public ActionResult DocumentStatusUpdate(int UserId, int doctypeid)
        {

            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                int c = css.ServiceProviderDocumentReset(UserId, doctypeid,loggedInUser.UserId);
            }
            catch (Exception c)
            {
            }
            return Json(1);
        }
        #endregion
        public ActionResult UnQualifiedSP(string SPid,string Companyid)
        {
            int valueToReturn = 0;
            try
            {
                //check if the CompanyCode already exists
                Int32 cmpID=0; 
                if(!string.IsNullOrEmpty(Companyid))
                {
                    cmpID = Convert.ToInt32(Companyid);
                }
                Int32 SPID = 0;
                if (!string.IsNullOrEmpty(SPid))
                {
                    SPID = Convert.ToInt32(SPid);
                }
                if (css.UnQualifiedServiceProviders.Where(x=>x.SPId==SPID && x.CompanyId == cmpID).ToList().Count == 0)
                {
                    css.usp_UnQualifiedSPInsert(Convert.ToInt32(SPid), Convert.ToInt32(Companyid));
                    valueToReturn = 1;
                }
                else
                {
                    valueToReturn = 2;
                }

            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UnQualifiedSPByCompanyLOB(string SPid, string Companyid,string LOBId)
        {
            int valueToReturn = 0;
            try
            {
                //check if the CompanyCode already exists
                Int32 cmpID = 0;
                if (!string.IsNullOrEmpty(Companyid))
                {
                    cmpID = Convert.ToInt32(Companyid);
                }
                Int32 SPID = 0;
                if (!string.IsNullOrEmpty(SPid))
                {
                    SPID = Convert.ToInt32(SPid);
                }
                Int32 LOBID = 0;
                if (!string.IsNullOrEmpty(LOBId))
                {
                    LOBID = Convert.ToInt32(LOBId);
                }
                if (css.UnQualifiedServiceProviders.Where(x => x.SPId == SPID && x.CompanyId == cmpID && x.LOBId == LOBID).ToList().Count == 0)
                {
                    css.usp_UnQualifiedSPLOBInsert(Convert.ToInt32(SPid), Convert.ToInt32(Companyid),Convert.ToInt32(LOBId));
                    valueToReturn = 1;
                }
                else
                {
                    valueToReturn = 2;
                }

            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UnQualifiedSPByCompanyClientPOC(string SPid, string Companyid, string ClientPOCId)
        {
            int valueToReturn = 0;
            try
            {
                //check if the CompanyCode already exists
                Int32 cmpID = 0;
                if (!string.IsNullOrEmpty(Companyid))
                {
                    cmpID = Convert.ToInt32(Companyid);
                }
                Int32 SPID = 0;
                if (!string.IsNullOrEmpty(SPid))
                {
                    SPID = Convert.ToInt32(SPid);
                }
                Int32 CLIENTPOCID = 0;
                if (!string.IsNullOrEmpty(ClientPOCId))
                {
                    CLIENTPOCID = Convert.ToInt32(ClientPOCId);
                }
                if (css.UnQualifiedServiceProviders.Where(x => x.SPId == SPID && x.CompanyId == cmpID && x.ClientPOCId == CLIENTPOCID).ToList().Count == 0)
                {
                    css.usp_UnQualifiedSPClientPOCInsert(Convert.ToInt32(SPid), Convert.ToInt32(Companyid), Convert.ToInt32(ClientPOCId));
                    valueToReturn = 1;
                }
                else
                {
                    valueToReturn = 2;
                }

            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }
        public ActionResult _UnQualifiedList(string SPID)
        {
          if(string.IsNullOrEmpty(SPID))
          {
              SPID="0";
          }
          return PartialView("_UnQualifiedSP", css.GetUnQualifiedList(Convert.ToInt32(SPID)).ToList());
        }

        public ActionResult _UnQualifiedSPByLOBList(string SPID)
        {
            if (string.IsNullOrEmpty(SPID))
            {
                SPID = "0";
            }
            return PartialView("_UnQualifiedLOBSP", css.GetUnQualifiedSPByLOBList(Convert.ToInt32(SPID)).ToList());
        }
        public ActionResult _UnQualifiedSPByClientPOCList(string SPID)
        {
            if (string.IsNullOrEmpty(SPID))
            {
                SPID = "0";
            }
            return PartialView("_UnQualifiedClientPOCSP", css.GetUnQualifiedSPByClientPOCList(Convert.ToInt32(SPID)).ToList());
        }
        public ActionResult _QualifiedSP(string SPID,string Companyid)
        {
            if (string.IsNullOrEmpty(SPID))
            {
                SPID = "0";
            }
           
            css.QualifiedSP(Convert.ToInt32(SPID), Convert.ToInt32(Companyid));
            return PartialView("_UnQualifiedSP", css.GetUnQualifiedList(Convert.ToInt32(SPID)).ToList());
        }
        public ActionResult _QualifiedSPByLOB(string SPID, string Companyid,string LOBId)
        {
            if (string.IsNullOrEmpty(SPID))
            {
                SPID = "0";
            }
            if (string.IsNullOrEmpty(Companyid))
            {
                Companyid = "0";
            }
            if (string.IsNullOrEmpty(LOBId))
            {
                LOBId = "0";
            }
            css.QualifiedSPByLOB(Convert.ToInt32(SPID), Convert.ToInt32(Companyid),Convert.ToInt32(LOBId));
            return PartialView("_UnQualifiedLOBSP", css.GetUnQualifiedSPByLOBList(Convert.ToInt32(SPID)).ToList());
        }
        public ActionResult _QualifiedSPByClientPOC(string SPID, string Companyid, string ClientPOCId)
        {
            if (string.IsNullOrEmpty(SPID))
            {
                SPID = "0";
            }
            if (string.IsNullOrEmpty(Companyid))
            {
                Companyid = "0";
            }
            if (string.IsNullOrEmpty(ClientPOCId))
            {
                ClientPOCId = "0";
            }
            css.QualifiedSPByClientPOC(Convert.ToInt32(SPID), Convert.ToInt32(Companyid), Convert.ToInt32(ClientPOCId));
            return PartialView("_UnQualifiedClientPOCSP", css.GetUnQualifiedSPByClientPOCList(Convert.ToInt32(SPID)).ToList());
        }
        public JsonResult getLOBListJson(Int64 companyId)
        {
            return Json(getLOBList(companyId));
        }
        public SelectList getLOBList(Int64 companyId)
        {
            IEnumerable<LineOfBusinessGetList_Result> LOBList1 = new List<LineOfBusinessGetList_Result>();
            IEnumerable<SelectListItem> LOBList = new List<SelectListItem>();
            if (companyId!=0)
            {


                LOBList1 = css.LineOfBusinessGetList(companyId).ToList();

                LOBList=(from m in LOBList1 select m).AsEnumerable().Select(m => new SelectListItem() { Text = m.LOBDescription, Value = m.LOBId.ToString() });
             
               
            }
            return new SelectList(LOBList, "Value", "Text");
        }
        public JsonResult getClientPOCListJson(Int64 companyId)
        {
            return Json(getClientPOCList(companyId));
        }
        public SelectList getClientPOCList(Int64 companyId)
        {
           
            IEnumerable<SelectListItem> ClientPOCList = new List<SelectListItem>();
            if (companyId != 0)
            {
                ClientPOCList = (from m in css.Users where m.UserTypeId == 11 && m.HeadCompanyId == companyId select m).AsEnumerable().Select(m => new SelectListItem() { Text = m.FirstName + ' ' + m.LastName, Value = m.UserId.ToString() });
              
            }
            return new SelectList(ClientPOCList, "Value", "Text");
        }
       [AllowAnonymous]
        public JsonResult GetHeadCompanyDetails(Int64 HeadCompanyId)
        {            
            if (HeadCompanyId != 0)
            {
                var headCompanyDet = css.GetHeadCompanyDetails(HeadCompanyId).FirstOrDefault();
                headCompanyDet.imagePath = Utility.GetClaimServiceProviderAbsoluteDocPath(HeadCompanyId, headCompanyDet.imagePath);
                return Json(headCompanyDet, JsonRequestBehavior.AllowGet);
            }
            return Json(1);
        }

       #region Company Profile Image upload
      [NonAction]
      public void UploadDocument(User viewModel, HttpPostedFileBase uploadFile, out string imagePath)
       {
           imagePath = "";
           if (uploadFile != null && uploadFile.ContentLength > 0)
           {
               CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
               if (MvcApplication.UseAzureCloudStorage == "1")
               {
                   #region Cloud Storage

                   // extract only the fielname
                   string fileName = new FileInfo(uploadFile.FileName).Name;
                   string storeAsFileName = Guid.NewGuid() + "" + Regex.Replace(fileName, "[^0-9a-zA-Z-_.]+", "");
                   string HeadCompanyId = viewModel.HeadCompanyId + "";
                   //check whether the folder with user's userid exists

                   // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                   string relativeFileName = HeadCompanyId + "/" + storeAsFileName;

                   MemoryStream bufferStream = new System.IO.MemoryStream();
                   uploadFile.InputStream.CopyTo(bufferStream);
                   byte[] buffer = bufferStream.ToArray();
                   string containerName = MvcApplication.CompanyDocsContainerName;
                   string ThumbnailcontainerName = MvcApplication.CompanyThumbnailContainerName;
                   //remove existing file
                   try
                   {
                       int companyid = viewModel.HeadCompanyId ?? 0;
                       String imagePathDB = css.Companies.Where(x => x.CompanyId == companyid).Select(y => y.imagePath).FirstOrDefault();
                       if (!String.IsNullOrEmpty(imagePathDB))
                       {
                           string deleteFileName = HeadCompanyId + "/" + imagePathDB;
                           CloudStorageUtility.DeleteFile(containerName, deleteFileName);
                           CloudStorageUtility.DeleteFile(ThumbnailcontainerName, deleteFileName);
                       }
                   }
                   catch (Exception ex) { }


                   //Original Image storage 

                   CloudStorageUtility.StoreFile(containerName, relativeFileName, buffer);
                   bufferStream.Close();
                   bufferStream.Dispose();
                   //Thumbnail Image storage  


                   MemoryStream TbufferStream = new System.IO.MemoryStream();
                   // Load image.
                   Image image = Image.FromStream(uploadFile.InputStream);

                   // Compute thumbnail size.
                   Size thumbnailSize =Utility.GetThumbnailSize(image);

                   // Get thumbnail.
                   Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                       thumbnailSize.Height, null, IntPtr.Zero);
                   ImageFormat Iformat = new ImageFormat(image.RawFormat.Guid);
                   thumbnail.Save(TbufferStream, Iformat);

                   // If you're going to read from the stream, you may need to reset the position to the start
                   TbufferStream.Position = 0;
                   byte[] Tbuffer = TbufferStream.ToArray();
                   CloudStorageUtility.StoreFile(ThumbnailcontainerName, relativeFileName, Tbuffer);
                   TbufferStream.Close();
                   TbufferStream.Dispose();
                   #endregion
                   imagePath = storeAsFileName;
                   //Update Company Logo Image
                   if (viewModel.HeadCompanyId != null)
                   {
                       css.usp_companyProfilePictureUpload(viewModel.UserId, viewModel.HeadCompanyId, storeAsFileName);
                   }
               }
               else
               {
                   #region Local File System

                   // extract only the fielname
                   string fileName = new FileInfo(uploadFile.FileName).Name;
                   string storeAsFileName = Guid.NewGuid() + "" + Regex.Replace(fileName, "[^0-9a-zA-Z-_.]+", "");
                   string baseFolder = MvcApplication.ClaimServiceProviderDocPath;
                   string HeadCompanyId = viewModel.HeadCompanyId + "";

                   //check whether the folder with user's userid exists
                   if (!Directory.Exists(Server.MapPath(baseFolder + "OriginalImage/" + HeadCompanyId)))
                   {
                       Directory.CreateDirectory(Server.MapPath(baseFolder + "OriginalImage/" + HeadCompanyId));
                   }

                   if (!Directory.Exists(Server.MapPath(baseFolder + "ThumbnailImage/" + HeadCompanyId)))
                   {
                       Directory.CreateDirectory(Server.MapPath(baseFolder + "ThumbnailImage/" + HeadCompanyId));
                   }

                   // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                   string path = Path.Combine(Server.MapPath(baseFolder + "OriginalImage/" + HeadCompanyId), storeAsFileName);
                   uploadFile.SaveAs(path);
                   Utility.ThumbnailImage(path, Path.Combine(Server.MapPath(baseFolder + "ThumbnailImage/" + HeadCompanyId), storeAsFileName));

                   //   loadFormData(uploadFile, path);


                   //  css.usp_userProfilePictureUpload(viewModel.UserId, storeAsFileName);
                   //Update Company Logo Image
                   if (viewModel.HeadCompanyId != null)
                   {
                       css.usp_companyProfilePictureUpload(viewModel.UserId, viewModel.HeadCompanyId, storeAsFileName);
                   }
                   #endregion
                   imagePath = storeAsFileName;
               }

           }
       }
      
        #endregion


        [HttpGet]
        public int QBVendorIdUpdate(string QBVendorId,int HeadCompanyId)
        {
            try
            {
                css.usp_UpdateQBVendorId(HeadCompanyId, QBVendorId);
                return 1;
            }
            catch
            {
                return 0;
            }
    
        }
    }
}

