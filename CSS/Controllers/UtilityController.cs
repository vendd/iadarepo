﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
//using Winnovative.WnvHtmlConvert;
using System.Configuration;

namespace CSS.Controllers
{
    public class ZipInfo
    {
        public string City { get; set; }
        public string State { get; set; }

    }

    public class UtilityController : Controller
    {
        [HttpPost]
        public JsonResult GetZipCodeInfo(string ZipCode)
        {
            try
            {

                ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
                StateProvince objstateprovince = new StateProvince();
                ZipInfo objzipinfo = new ZipInfo();
                tbl_ZipUSA objZipUSA = css.tbl_ZipUSA.Where(x => x.ZipCode.Trim() == ZipCode.Trim()).First();
                string state = objZipUSA.State;

                objstateprovince = css.StateProvinces.Where(p => p.StateProvinceCode == state).First();
                objzipinfo.City = objZipUSA.LLCity;
                objzipinfo.State = objstateprovince.StateProvinceCode.Trim();

                if (objZipUSA != null)
                {
                    return Json(objzipinfo);
                }
            }
            catch
            {
            }

            return Json(-1);

        }

    }
}
