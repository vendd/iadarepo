﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ViewModels;
using BLL.Models;
using BLL;
using Newtonsoft.Json;
using System.IO;
namespace CSS.Controllers
{
    [Authorize]
    public class SPMapSearchController : Controller
    {
        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public IEnumerable<ServiceOfferingsNew> ServiceOfferings { get; set; }

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Search(string sptype)
        {
          //  SPMapSearchViewModel search = new SPMapSearchViewModel();
         //   search = LoadSASearch(search, "", 0);
            //search = LoadSPSearch(search, "", 0);
        //    search.Distance = 20;

            //LoadSPPager(search, null, "1", "1");

            //for Create Auto Assignment
            Session["SPTYPE"] = "";
            if(sptype == "fav")
            {
                Session["SPTYPE"] = "fav";
            }
            else if(sptype == "act")
            {
                Session["SPTYPE"] = "act";
            }
                //AutoAssignmentViewModel viewModel;
                        
                //viewModel = new AutoAssignmentViewModel();
                //viewModel.UseMasterPageLayout = true;
                //viewModel.claim.IsSingleDeductible = false;//Default to SingeDeductible
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
              //  BLL.User LoggedInUser = css.Users.Find(loggedInUser.UserId);            
            //

                return View(loggedInUser);
        }
       public string isSessionValid(object value)
       {
           if (value==null)
           {
               return "";
           }
           string sessionValue ="";
           if (!string.IsNullOrEmpty(Session["SPTYPE"].ToString()))
           {
               sessionValue = Session["SPTYPE"].ToString();
           }
           return sessionValue;
       }
        public JsonResult InitSPMapSearch()
        {
            SPMapSearchNewViewModel search = new SPMapSearchNewViewModel();
             search.Distance = 100;


             if (isSessionValid(Session["SPTYPE"]) == "fav" || isSessionValid(Session["SPTYPE"]) == "act")
             {
                
               search = LoadSPSearch(search, ",0", search.Distance,0);//INPSW-32
             }

             return Json(search, JsonRequestBehavior.AllowGet);
        }

        public SPMapSearchViewModel LoadSASearch(SPMapSearchViewModel search, string Location, int distance)
        {
            if (search.CatCode != null && search.CatCode.Trim() == "0") search.CatCode = null;
            //if (search.SeverityLevel == 0) search.SeverityLevel = null;
            //if (search.LossType == 0) search.LossType = null;
            //if (search.InsuranceCompany == 0) search.InsuranceCompany = null;
            //SPMapSearchViewModel search = new SPMapSearchViewModel();

            if (Location == ",0") Location = "";
            search.ClaimList = css.usp_SearchDispatchClaimsGetList(Location, distance, search.CatCode, search.SeverityLevel, search.LossType, search.InsuranceCompany).ToList();
            var Filteredzip = (from a in search.ClaimList
                               select new { a.PropertyZip, a.Lat, a.Lng }
                        ).Distinct().ToList();
            search.spfilteredZip = new List<SAfilteredZip>();
            foreach (var z in Filteredzip)
            {
                SAfilteredZip FilteredZp = new SAfilteredZip();
                FilteredZp.Zip = z.PropertyZip.ToString();
                FilteredZp.Latitude = z.Lat.ToString();
                FilteredZp.Lontitude = z.Lng.ToString();
                FilteredZp.SPAssignments = new List<SMAssignments>();
                List<usp_SearchDispatchClaimsGetList_Result> clForZip = search.ClaimList.Where(a => a.PropertyZip == z.PropertyZip.ToString()).ToList();
                foreach (usp_SearchDispatchClaimsGetList_Result c in clForZip)
                {
                    SMAssignments sma = new SMAssignments();
                    Claim cl = new Claim();
                    cl.ClaimNumber = c.ClaimNumber;
                    cl.ClaimId = c.ClaimId;
                    cl.ClaimNumber = c.ClaimNumber;
                    cl.PolicyNumber = c.PolicyNumber;
                    cl.InsuredFirstName = c.InsuredFirstName;
                    cl.InsuredLastName = c.InsuredLastName;
                    cl.PropertyCity = c.PropertyCity;
                    cl.PropertyZip = c.PropertyZip;
                    cl.SeverityLevel = c.SeverityLevel;
                    cl.ClaimCreatedDate = c.AssignmentDate; //shoud be claim created date
                    cl.LOBId = c.LOBId;
                    cl.ClientPointOfContactUserId = c.ClientPointOfContactUserId;
                    //Add
                    Company comp = new Company();
                    comp.CompanyName = c.CompanyName;
                    comp.CompanyId = c.CompanyId; //new change
                    cl.Company = comp;
                    sma.CauseType = c.LossType;
                    sma.AssignmentId = c.AssignmentId;
                    sma.AssignmentDate = c.AssignmentDate.ToShortDateString();
                    //cl.FileStatus = c.FileStatus;    
                    sma.ClaimDetail = cl;
                    FilteredZp.SPAssignments.Add(sma);
                }
                search.spfilteredZip.Add(FilteredZp);
            }
            if (Session["ZipList"] != null)
            {
                Session["ZipList"] = search.spfilteredZip;
            }
            else
            {
                Session.Add("ZipList", search.spfilteredZip);
            }

            return search;
        }

        public SPMapSearchNewViewModel LoadSPSearch(SPMapSearchNewViewModel search, string Location, int distance,Int64 Autoassignmentid)//INPSW-32
        {
            if (Location == ",0")
            {
                Location = "";
            }
            search.IsLoggedIN = true;
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            search.SPMapSearchtype = SPMapSearchType.MainSearch;
            if (isSessionValid(Session["SPTYPE"]) == "fav")
            {
                search.SPMapSearchtype = SPMapSearchType.Fevorite;
                search.Splist = css.AutoSearchDispatchSPGetList(Location, distance, search.Rank, search.SPName, search.ServiceType, loggedInUser.UserId, 1, 0, search.SPActiveStatus, Autoassignmentid).ToList();//INPSW-32
                search.IsActiveSP = true;
            }
            else if (isSessionValid(Session["SPTYPE"]) == "act")
            {
                search.SPMapSearchtype = SPMapSearchType.ActiveLoc;
                search.Splist = css.AutoSearchDispatchSPGetList(Location, distance, search.Rank, search.SPName, search.ServiceType, loggedInUser.UserId, 0, loggedInUser.HeadCompanyId, search.SPActiveStatus, Autoassignmentid).ToList(); //INPSW-32
                search.IsActiveSP = false;
            }
            else
            {
                search.Splist = css.AutoSearchDispatchSPGetList(Location, distance, search.Rank, search.SPName, search.ServiceType, loggedInUser.UserId, 0, 0, search.SPActiveStatus, Autoassignmentid).ToList();//INPSW-32
                search.IsActiveSP = true;
            }
            foreach (AutoSearchDispatchSPGetList_Result s in search.Splist)
            {
                s.imagePath = Utility.GetClaimServiceProviderAbsoluteDocPath((Int32)s.HeadCompanyID, s.imagePath);
            }
           
          //Commented for IADA 
            //changes for unqualified service provider
            //if (search.InsuranceCompany != 0)
            //{
            //    List<usp_AutoSearchDispatchSPGetList_Result> tempSP = new List<usp_AutoSearchDispatchSPGetList_Result>();
            //    foreach (usp_AutoSearchDispatchSPGetList_Result s in search.Splist)
            //    {
            //        if (css.UnQualifiedServiceProviders.Where(x => x.SPId == s.UserId && x.CompanyId == search.InsuranceCompany && x.LOBId==null && x.ClientPOCId==null).ToList().Count() == 0)
            //        {
            //            tempSP.Add(s);
            //        }
            //    }
            //    search.Splist = tempSP;
            //}

            //search.SPServiceproviders = new List<BLL.User>();
            ////List<usp_SearchDispatchSPGetList_Result> spForZip = search.Splist.Where(a => a.Zip == z.PropertyZip.ToString()).ToList();
            //foreach (usp_AutoSearchDispatchSPGetList_Result s in search.Splist)
            //{
            //    User u = new BLL.User();
            //    u.City = s.City;
            //    u.FirstName = s.Firstname;
            //    u.LastName = s.LastName;
            //    u.StreetAddress = s.StreetAddress;
            //    u.City = s.City;
            //    u.State = s.State;
            //    u.Zip = s.Zip;
            //    u.Email = s.Email;
            //    u.HomePhone = s.HomePhone;
            //    u.WorkPhone = s.WorkPhone;
            //    u.MobilePhone = s.MobilePhone;
            //    u.UserId = Convert.ToInt64(s.UserId);
            //    ServiceProviderDetail spd = new ServiceProviderDetail();
            //    spd.TypeOfClaimsPref = s.TypeOfClaimsPref;
            //    u.ServiceProviderDetail = spd;
            //    search.SPServiceproviders.Add(u);
            //}
            //if (Session["SPList"] != null)
            //{
            //    Session["SPList"] = search.SPServiceproviders;
            //}
            //else
            //{
            //    Session.Add("SPList", search.SPServiceproviders);
            //}
            return search;
        }

        public SPMapSearchViewModel LoadSessionSAData(SPMapSearchViewModel search)
        {
            search.spfilteredZip = new List<SAfilteredZip>();
            List<SAfilteredZip> SALIst = new List<SAfilteredZip>();

            if (Session["ZipList"] != null)
            {
                SALIst = (List<SAfilteredZip>)Session["ZipList"];
            }

            foreach (SAfilteredZip SA in SALIst)
            {
                search.spfilteredZip.Add(SA);
            }
            //sarch.SPServiceproviders = Splist;
            return search;
        }

        public SPMapSearchViewModel LoadSessionSPData(SPMapSearchViewModel search)
        {
            search.SPServiceproviders = new List<BLL.User>();
            List<User> Splist = new List<BLL.User>();
            if (Session["SPList"] != null)
            {
                Splist = (List<User>)Session["SPList"];
            }
            foreach (User u in Splist)
            {
                search.SPServiceproviders.Add(u);
            }
            return search;

        }

       [HttpPost]
        public ActionResult MapSearch(SPMapSearchNewViewModel search)
        {
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            search.Distance = 100;

            if (search.AddressDetails.Zip == null) search.AddressDetails.Zip = "";
            if (search.AddressDetails.Zip.Trim() != "")
            {
                search = LoadSPSearch(search, search.AddressDetails.Zip, search.Distance, 0);//inpsw-32
                // search = LoadSASearch(search, search.Zip, search.Distance);
            }
            else
            {
                search = LoadSPSearch(search, search.AddressDetails.City + "," + search.AddressDetails.State, search.Distance, 0);//inpsw-32
                //   search = LoadSASearch(search, search.City + "," + search.State, search.Distance);
            }
            TempData["SPMapSearchNewViewModel"] = search;
            return View("Search");
        }
        [HttpPost]
        public JsonResult SPMapSearch(SPMapSearchNewViewModel search)
        {
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            search.IsLoggedIN = true;
            if (loggedInUser==null)
            {
                search.IsLoggedIN = false;
                return Json(search, JsonRequestBehavior.AllowGet);
            }
            if (search.AddressDetails==null)
            {
                search = LoadSPSearch(search, "", search.Distance, search.AutoassignmentId);//inpsw-32
                return Json(search, JsonRequestBehavior.AllowGet);
            }
            if (search.AddressDetails.Zip == null) search.AddressDetails.Zip = "";
            if (search.AddressDetails.Zip.Trim() != "")
            {
                search = LoadSPSearch(search, search.AddressDetails.Zip, search.Distance, search.AutoassignmentId);//inpsw-32
               // search = LoadSASearch(search, search.Zip, search.Distance);
            }
            else
            {
                search = LoadSPSearch(search, search.AddressDetails.City + "," + search.AddressDetails.State, search.Distance,search.AutoassignmentId);//inpsw-32
             //   search = LoadSASearch(search, search.City + "," + search.State, search.Distance);
            }
            return Json(search, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ManageServiceRate(Int64 UserID)
        {
           return PartialView("_SPServiceRatesPopup");
        }

        public JsonResult GetSPServiceRateDetails(Int64 SPID)
        {
            SPMoreInfoViewModel vm = new SPMoreInfoViewModel();

        //    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            if (SPID != 0)
            {
                vm.UserId = SPID;
                 vm.SPServiceRate = css.usp_GetServiceProviderServiceRates(SPID).ToList();
                return Json(vm, JsonRequestBehavior.AllowGet);
            }
            return Json("0", JsonRequestBehavior.AllowGet);
        }
       
        public JsonResult GetSPMapDetails(Int64 SPID)
        {
            SPMoreInfoViewModel vm = new SPMoreInfoViewModel();

            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
           if (SPID!=0)
           {
               vm.UserId = SPID;
               vm.result = css.usp_GetSPMapDetails(SPID, loggedInUser.UserId).FirstOrDefault();
               vm.result.imagePath = Utility.GetClaimServiceProviderAbsoluteDocPath((Int32)vm.result.HeadCompanyId, vm.result.imagePath);
            //   vm.SPServiceRate = css.usp_GetServiceProviderServiceRates(SPID).ToList();
               return Json(vm, JsonRequestBehavior.AllowGet);
           }
            return Json("0", JsonRequestBehavior.AllowGet);
        }
      

        [HttpPost]
        public ActionResult updateSPServiceRates(SPMoreInfoViewModel VM)
        { 
        if(VM.SPServiceRate!=null)
        {
            css.usp_SPServiceRatesDelete(VM.UserId);
            foreach(usp_GetServiceProviderServiceRates_Result SPServiceRate in VM.SPServiceRate)
            {
                css.usp_SPServiceRatesInsert(VM.UserId, SPServiceRate.ServiceId, Utility.isInt16(SPServiceRate.FreeMiles), Utility.isDecimal(SPServiceRate.ServiceRate), Utility.isDecimal(SPServiceRate.TandERatePerHour));
            }
           
        }
        return Json("1", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Reject(Int64 hdnRejectUserId, string txtRejectRemarks)
        {
            css.usp_DeleteSPUser(hdnRejectUserId, true);
            return RedirectToAction("InitSPMapSearch");
        }

        //[HttpPost]
        //public JsonResult Search(SPMapSearchViewModel search, FormCollection form)
        //{
        //    ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        //    //string strIsQuickAssign = form["hdnIsQuickAssign"];
        //    //string strSubmit = form["hdnsubmitType"];

        //    string strIsQuickAssign =search.hdnIsQuickAssignVal.ToString();
        //    string strSubmit = search.hdnsubmitTypeVal;

        //    if (!String.IsNullOrEmpty(search.SPName))
        //    {
        //        //When SP Name is specified, reset all other search filters
        //        search.Zip = String.Empty;
        //        search.State = "0";
        //        search.City = String.Empty;
        //        search.Distance = 20;
        //        search.Rank = 0;
        //        search.CatCode = "0";
        //        search.LossType = 0;
        //        search.SeverityLevel = 0;
        //        search.InsuranceCompany = 0;
        //    }

        //    //Uncomment if u require view model//viewmodel = LoadSessionData(viewmodel);
        //    if (strIsQuickAssign.Trim() == "1")
        //    {
        //        search = LoadSessionSAData(search);
        //        search = LoadSessionSPData(search);
        //        CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
        //        //string spId = form["hdnSpid"];
        //        //string ASGId = form["hdnASGId"];

        //        string spId = search.hdnSpidVal;
        //        string ASGId = search.hdnASGIdVal;
        //        try
        //        {
        //            //css.SPMapSearchUpdate(ASGId, Convert.ToInt64(spId), loggedInUser.UserId);
        //            //Instead of the above line css.SPMapSearchUpdate() make use of the existing action
        //            NewAssignmentController controller = new NewAssignmentController();
        //            controller.ControllerContext = this.ControllerContext;
        //            controller.AssignServiceProvider(Convert.ToInt64(ASGId), Convert.ToInt64(spId));
        //        }
        //        catch (Exception ex)
        //        {
        //            //Error
        //        }
        //    }

        //    if (strSubmit != null && strSubmit != "")
        //    {

        //        if (strSubmit.Trim() == "ALL")
        //        {
        //            search = LoadSASearch(search, "", 0);
        //            if (Session["SPList"] != null)
        //            {
        //                Session["SPList"] = null;
        //            }
        //            search.Distance = 20;
        //        }
        //        if (strSubmit.Trim() == "LOC") //location filter
        //        {
        //            //get the zip list from the sp
        //            string strCatcode = "";
        //            byte SeverityLevel = 0;
        //            int iLosstype = 0, iInsuranceComp = 0;

        //            //search.CatCode,search.SeverityLevel,search.LossType,search.InsuranceCompany).ToList();

        //            //System.Data.Objects.ObjectParameter objZiplist = new System.Data.Objects.ObjectParameter("StrZipList", "");
        //            //css.usp_GetZipList(search.Zip, search.City, search.Distance, objZiplist);
        //            //strZiplist = objZiplist.Value.ToString();


        //            if (search.Zip == null) search.Zip = "";
        //            if (search.Zip.Trim() != "")
        //            {
        //                search = LoadSPSearch(search, search.Zip, search.Distance);
        //                search = LoadSASearch(search, search.Zip, search.Distance);
        //            }
        //            else
        //            {
        //                search = LoadSPSearch(search, search.City + "," + search.State, search.Distance);
        //                search = LoadSASearch(search, search.City + "," + search.State, search.Distance);
        //            }
        //        }
        //        else if (strSubmit.Trim() == "SA") //location filter
        //        {
        //            string str = "";

        //            //System.Data.Objects.ObjectParameter objZiplist = new System.Data.Objects.ObjectParameter("StrZipList", "");
        //            //css.usp_GetZipList(search.Zip, search.City, search.Distance, objZiplist);
        //            //strZiplist = objZiplist.Value.ToString();
        //            //if (search.Zip == null) search.Zip = "";
        //            //if (search.Zip.Trim() != "")
        //            //{
        //            //search = LoadSPSearch(search, search.Zip, search.Distance);
        //            search = LoadSessionSPData(search);
        //            search = LoadSASearch(search, search.Zip, search.Distance);
        //            //}
        //            //else
        //            //{
        //            //    search = LoadSPSearch(search, search.City + "," + search.State, search.Distance);
        //            //    search = LoadSASearch(search, search.City + "," + search.State, search.Distance);
        //            //}
        //        }
        //    }


        //    search = LoadSessionSAData(search);
        //    search = LoadSessionSPData(search);
        //    string curPage = form["hdnCurrentPage"];
        //    string firstPage = form["hdnstartPage"];

        //    //search = LoadSPPager(search, form, curPage, firstPage);
        //    //code to refresh the view model

        //    return Json(search, JsonRequestBehavior.AllowGet);
        //}


        public ActionResult AssignSp(string Ziplist, string Spid)
        {
            SPMapSearchViewModel search = new SPMapSearchViewModel();
            search = LoadSessionSAData(search);
            search = LoadSessionSPData(search);

            if (Spid.Trim() != "" && Spid.Trim() != "0")
            {
                //List<SPfilteredZip> splist = search.spfilteredZip.Where(a => a.Zip == a.SPServiceproviders.Where(b => b.UserId.ToString() == Spid));
                //foreach (var obj in search.SPServiceproviders)
                //{
                var usr = search.SPServiceproviders.Where(a => a.UserId.ToString() == Spid).ToList();
                if (usr.Count() > 0)
                {
                    TempData["SpName"] = usr[0].FirstName + " " + usr[0].LastName;
                    TempData["SpIid"] = usr[0].UserId.ToString();
                }
                //}
            }
            else
            {
                TempData["SpName"] = "";
                TempData["SpIid"] = "";
            }

            if (Ziplist.Trim() != "")
            {
                List<SAfilteredZip> result = search.spfilteredZip.Where(a => a.Zip == Ziplist).ToList();
                search.spfilteredZip = result;
            }

            //get details of srevice provider from spid
            //get details of zip

            //Uncomment  for js pop up 
            //  return Json(RenderViewToString("AssignSp", search), JsonRequestBehavior.AllowGet);
            return View(search);
        }

        //not used
        [HttpPost]
        public Boolean AssignSp(string strAsglist, string spId, string post)
        //(SPMapSearchViewModel viewmodel, FormCollection Form)
        {
            Boolean Istatus = false;
            string spID = spId;//Form["hdnSpID"];
            string lstAssignmentId = strAsglist; //Form["AsgIdList"];
            //call function to save data 
            if (lstAssignmentId != null && lstAssignmentId.Trim() != "" && spID != null && spID.Trim() != null)
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                string[] AsgIdlist = strAsglist.Split(',');
                foreach (string AsgId in AsgIdlist)
                {
                    try
                    {
                        //css.SPMapSearchUpdate(lstAssignmentId, Convert.ToInt64(spID), loggedInUser.UserId);
                        NewAssignmentController controller = new NewAssignmentController();
                        controller.ControllerContext = this.ControllerContext;
                        controller.AutoAssignServiceProvider(Convert.ToInt64(AsgId), Convert.ToInt64(spId));
                        Istatus = true;
                    }
                    catch (Exception ex)
                    {
                        //Error
                    }
                }
            }

            SPMapSearchViewModel search = new SPMapSearchViewModel();
            // return Json(RenderViewToString("AssignSp", search), JsonRequestBehavior.AllowGet);
            return Istatus;

        }

        public ActionResult BrowseMapLocation(string hdnlistType, string hdnParam, string isSort, string filterParam, int pgNO, FormCollection form)
        {
            MapLocationSearchViewModel search = new MapLocationSearchViewModel();

            string listtype = string.IsNullOrEmpty(hdnlistType) ? "0" : hdnlistType; //form["hdnlistType"];
            string parameter = string.IsNullOrEmpty(hdnParam) ? "" : hdnParam;//["hdnParam"];
            search.FilterAlphabet = filterParam;
            search.Pager1 = new BLL.Models.Pager();
            if (isSort == "0")
            {
                if (parameter.Trim() == "")
                {
                    listtype = "0"; //incase no parameter it woill return city list
                }
                switch (listtype)
                {
                    case "0":
                        search.populateLists();
                        search.listtype = 0;
                        break;
                    case "1":
                        search.populateCountylist(parameter);
                        search.listtype = 1;

                        break;
                    case "2":
                        search.populateCitylist(parameter);
                        search.listtype = 2;
                        break;
                    case "3":
                        search.populateZiplist(parameter);
                        search.listtype = 3;
                        break;
                    case "4":
                        //close window and populate map using the zip code.
                        break;
                }
                if (Session["filterList"] == null)
                {
                    Session.Add("filterList", search.FilterList);
                }
                else
                {
                    Session["filterList"] = search.FilterList;
                }
                search.populatelinks();
                search = loadLocationPager(search, form, "", "");

            }
            else if (isSort == "1")//sort by alphabet
            {
                search.listtype = Convert.ToInt16(listtype);
                search.FilterList = (List<LinkList>)Session["filterList"];
                search.populatelinks();
                var lst = from LinkList elements in search.FilterList where elements.Text.StartsWith(filterParam) select elements;
                List<LinkList> results = lst.ToList<LinkList>();
                search.FilterList = results;
                search.FilterAlphabet = filterParam;
                //search = loadPager(search, form);
            }
            else if (isSort == "2") //Pager 
            {
                search.listtype = Convert.ToInt16(listtype);
                search.FilterList = (List<LinkList>)Session["filterList"];
                search.populatelinks();
                search = loadLocationPager(search, form, pgNO.ToString(), filterParam);
            }

            return View(search);
        }

        //public void loadsplist(SPMapSearchViewModel search)
        //{
        //    search.SPServiceproviders
        //}
        public SPMapSearchViewModel LoadSPPager(SPMapSearchViewModel search, FormCollection form, string CurPg, string FirstPg)
        {
            search.Pager = new BLL.Models.Pager();
            if (CurPg != "")
            {
                search.Pager.Page = Convert.ToInt32(CurPg);
            }
            else
            {
                search.Pager.Page = 1;
            }
            search.Pager.RecsPerPage = 5;
            if (FirstPg != null && FirstPg != "")
            {
                if (!String.IsNullOrEmpty(FirstPg))
                {
                    search.Pager.FirstPageNo = Convert.ToInt16(FirstPg);
                }
                else
                {
                    search.Pager.FirstPageNo = 1;
                }
            }
            else
            {
                search.Pager.FirstPageNo = 1;
            }
            search.Pager.IsAjax = false;
            search.Pager.TotalCount = search.SPServiceproviders.Count.ToString();  // getAssignmentSearchResult(loggedInUser, viewModel).Count().ToString();

            if (Convert.ToInt32(search.Pager.TotalCount) % Convert.ToInt32(search.Pager.RecsPerPage) != 0)
            {
                search.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(search.Pager.TotalCount) / Convert.ToInt32(search.Pager.RecsPerPage)))).ToString();
            }
            else
            {
                search.Pager.NoOfPages = ((Convert.ToInt32(search.Pager.TotalCount) / Convert.ToInt32(search.Pager.RecsPerPage))).ToString();
            }
            search.Pager.FormName = "spSearch";
            //search.Pager.AjaxParam = search.listtype.ToString();

            int pgcnt = 0;
            if (search.Pager.Page > 1)
            {
                pgcnt = search.Pager.Page - 1;
            }
            search.SPServiceproviders = search.SPServiceproviders.Skip(pgcnt * search.Pager.RecsPerPage).Take(search.Pager.RecsPerPage).ToList();

            return search;
        }


        public MapLocationSearchViewModel loadLocationPager(MapLocationSearchViewModel search, FormCollection form, string CurPg, string FirstPg)
        {
            if (CurPg != "")
            {
                search.Pager1.Page = Convert.ToInt32(CurPg);
            }
            else
            {
                search.Pager1.Page = 1;
            }
            search.Pager1.RecsPerPage = 5;
            if (FirstPg != null && FirstPg != "")
            {
                if (!String.IsNullOrEmpty(FirstPg))
                {
                    search.Pager1.FirstPageNo = Convert.ToInt16(FirstPg);
                }
                else
                {
                    search.Pager1.FirstPageNo = 1;
                }
            }
            else
            {
                search.Pager1.FirstPageNo = 1;
            }
            search.Pager1.IsAjax = true;
            search.Pager1.TotalCount = search.FilterList.Count.ToString();  // getAssignmentSearchResult(loggedInUser, viewModel).Count().ToString();


            if (Convert.ToInt32(search.Pager1.TotalCount) % Convert.ToInt32(search.Pager1.RecsPerPage) != 0)
            {
                search.Pager1.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(search.Pager1.TotalCount) / Convert.ToInt32(search.Pager1.RecsPerPage)))).ToString();
            }
            else
            {
                search.Pager1.NoOfPages = ((Convert.ToInt32(search.Pager1.TotalCount) / Convert.ToInt32(search.Pager1.RecsPerPage))).ToString();
            }
            search.Pager1.FormName = "ReloadPager";
            search.Pager1.AjaxParam = search.listtype.ToString();

            int pgcnt = 0;
            if (search.Pager1.Page > 1)
            {
                pgcnt = search.Pager1.Page - 1;
            }
            search.FilterList = search.FilterList.Skip(pgcnt * search.Pager1.RecsPerPage).Take(search.Pager1.RecsPerPage).ToList();

            return search;
        }
        //[HttpPost]
        //public ActionResult BrowseMapLocation()
        //{
        //    MapLocationSearchViewModel search = new MapLocationSearchViewModel();
        //    return View(search);
        //}


        public ActionResult SpDetails(string strSpID, string Zip)
        {
            SPMapSearchViewModel SpPopup = new SPMapSearchViewModel();
            SpPopup = LoadSessionSAData(SpPopup);
            SpPopup = LoadSessionSPData(SpPopup);

            if (strSpID != null && strSpID != "0")
            {
                TempData.Clear();
                if (TempData["SPID"] != null)
                    TempData["SPID"] = strSpID;
                else
                    TempData.Add("SPID", strSpID);

                if (TempData["ZIP"] != null)
                    TempData["ZIP"] = strSpID;
                else
                    TempData.Add("ZIP", strSpID);
                // SPMapSearchViewModel SpPopup = new SPMapSearchViewModel();

                #region MyRegion
                //var objZip = SpPopup.spfilteredZip.Where(a => a.Zip == Zip);
                //SAfilteredZip spZip = objZip.SingleOrDefault();
                //var objsp = spZip.SPServiceproviders.Where(a => a.UserId.ToString() == strSpID);
                //User selectedSP = objsp.SingleOrDefault();
                //spZip.SPServiceproviders.Clear();
                //spZip.SPServiceproviders.Add(selectedSP);
                //SpPopup.spfilteredZip.Clear();
                //SpPopup.spfilteredZip.Add(spZip); 
                #endregion
                var objsp = SpPopup.SPServiceproviders.Where(a => a.UserId.ToString() == strSpID);
                User selectedSP = objsp.SingleOrDefault();
                SpPopup.SPServiceproviders.Clear();
                SpPopup.SPServiceproviders.Add(selectedSP);
                return View(SpPopup);
            }
            else
            {
                TempData["SpName"] = "";
                TempData["SpIid"] = "";

            }

            return View(SpPopup);
        }


        public ActionResult ClaimDetails(string strClaimId, string Zip)
        {
            SPMapSearchViewModel SpPopup = new SPMapSearchViewModel();
            SpPopup = LoadSessionSAData(SpPopup);
            SpPopup = LoadSessionSPData(SpPopup);
            if (strClaimId != null && strClaimId != "0")
            {
                TempData.Clear();
                if (TempData["strClaimId"] != null)
                    TempData["strClaimId"] = strClaimId;
                else
                    TempData.Add("SPID", strClaimId);
                // SPMapSearchViewModel SpPopup = new SPMapSearchViewModel();

                var objZip = SpPopup.spfilteredZip.Where(a => a.Zip == Zip);
                SAfilteredZip spZip = objZip.SingleOrDefault();
                var objCL = spZip.SPAssignments.Where(a => a.ClaimDetail.ClaimId.ToString() == strClaimId);
                SMAssignments selectedClaim = objCL.SingleOrDefault();
                spZip.SPAssignments.Clear();
                spZip.SPAssignments.Add(selectedClaim);
                SpPopup.spfilteredZip.Clear();
                SpPopup.spfilteredZip.Add(spZip);
                return View(SpPopup);
            }

            return View(SpPopup);
        }
        public ActionResult CheckUnQualifiedSP(string SPid, string Companyid, string LOBId, string ClientPOCId)
        {
            int valueToReturn = 0;
            try
            {
                //check if the CompanyCode already exists
                Int32 cmpID = 0;
                if (!string.IsNullOrEmpty(Companyid))
                {
                    cmpID = Convert.ToInt32(Companyid);
                }
                Int32 SPID = 0;
                if (!string.IsNullOrEmpty(SPid))
                {
                    SPID = Convert.ToInt32(SPid);
                }

                Int32 LOBid = 0;
                if (!string.IsNullOrEmpty(LOBId))
                {
                    LOBid = Convert.ToInt32(LOBId);
                }
                Int32 ClientPOCid = 0;
                if (!string.IsNullOrEmpty(ClientPOCId))
                {
                    ClientPOCid = Convert.ToInt32(ClientPOCId);
                }
             //   if (css.UnQualifiedServiceProviders.Where(x => x.SPId == SPID && x.CompanyId == cmpID).ToList().Count == 0)
                if (css.UnQualifiedServiceProviders.Where(x => x.SPId == SPID && x.CompanyId == cmpID && x.LOBId == null && x.ClientPOCId == null || ((x.SPId == SPID && x.CompanyId == cmpID && x.LOBId == LOBid && x.ClientPOCId == null) || (x.SPId == SPID && x.CompanyId == cmpID && x.ClientPOCId == ClientPOCid && x.LOBId == null))).ToList().Count() == 0)
                {
                  
                    valueToReturn = 1;
                }
                else
                {
                    valueToReturn = 2;
                }

            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }


        public ActionResult SelectServiceProvider(Int64 assignmentId)
        {
      
            SPMapSearchNewViewModel search = new SPMapSearchNewViewModel();
            try
            {
               

                Claim claim = new Claim();

                if (assignmentId == null)
                {
                    return Json("0", JsonRequestBehavior.AllowGet);
                }

                AutoAssignment AS = css.AutoAssignments.Find(assignmentId);

                

                string ZipCode=String.Empty;
                if (AS != null)
                {
                    if (AS.ClaimId.HasValue)
                    {
                        claim = css.Claims.Find(AS.ClaimId);
                        if (claim != null)
                        {
                            if (!String.IsNullOrEmpty(claim.PropertyZip))
                            {
                                ZipCode = claim.PropertyZip;
                            }
                            search.SelectedServiceIdM = claim.ServiceTypeID??0;
                            search.ServiceType = AS.ServiceId ?? 0;
                        }
                    }
                }
                if (ZipCode != String.Empty)
                {
                    search.Distance = 100;
                    search = LoadSPSearch(search, ZipCode, search.Distance, assignmentId);//inpsw -32
                    search.assignmentSearchDet = ZipCode;
                   
                    
                    
                }
                else
                {
                    return Json("0", JsonRequestBehavior.AllowGet);
                }                
            }
            catch(Exception ex)
            {
                string message = ex.Message;
            }

            return Json(search, JsonRequestBehavior.AllowGet);
        }
        public ActionResult updateUserfavourite(Int64 SPid, Boolean isFavourite)
        {
            int valueToReturn = 0;
            try
            {
                //css.ServiceProviderIsFavouriteUpdate(SPid, isFavourite);
                //valueToReturn = 1;
                 CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();                
                if(isFavourite == true)
                {
                    css.ServiceProviderIsFavouriteSave(SPid, loggedInUser.UserId);
                    valueToReturn = 1;

                }else 
                {
                    css.ServiceProviderIsFavouriteDelete(SPid, loggedInUser.UserId);
                    valueToReturn = 1;
                }
            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult test(Int64 ServiceTypeId, Int64 assignmentId = 0)
        {
            Claim claim = new Claim();
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            int SetSelectedServiceId;
            int HeadCompanyId = loggedInUser.HeadCompanyId??0;
            int ServiceTypeIdInt = Convert.ToInt16(ServiceTypeId);
            AutoAssignment AS = css.AutoAssignments.Find(assignmentId);
            int companyid = 0;
            if (AS != null)
            {
                if (AS.ClaimId.HasValue)
                {
                    claim = css.Claims.Find(AS.ClaimId);
                    companyid = claim.HeadCompanyId ?? 0;
                }
                 SetSelectedServiceId = AS.ServiceId??0; 
            }
            else
            {
                 SetSelectedServiceId = 0;
            }

            bool QRPActive;
            if (companyid > 0)
            {
                QRPActive = css.Companies.Where(y => y.CompanyId == companyid).Select(x => x.QualityRepairProgram).FirstOrDefault() ?? false;
            }
            else
            {
                
                QRPActive = css.Companies.Where(y => y.CompanyId == HeadCompanyId).Select(x => x.QualityRepairProgram).FirstOrDefault() ?? false;
                if (loggedInUser.UserTypeId == 2)//admin
                {
                    QRPActive = true;
                }
            }
            if (QRPActive)
            {
                ServiceOfferings = (from sType in css.ServiceOfferings
                                    where sType.ServiceTypeId == ServiceTypeIdInt && sType.IsDeleted == false
                                    orderby sType.ServiceId
                                    select new ServiceOfferingsNew()
                                    {
                                        ServiceId = sType.ServiceId,
                                        ServiceDescription = sType.ServiceDescription,
                                        selectedServiceId = SetSelectedServiceId
                                    }).ToList();
            }
            else
            {
                ServiceOfferings = (from sType in css.ServiceOfferings
                                    where sType.ServiceTypeId == ServiceTypeIdInt && sType.IsDeleted == false && sType.ServiceId!=13
                                    orderby sType.ServiceId
                                    select new ServiceOfferingsNew()
                                    {
                                        ServiceId = sType.ServiceId,
                                        ServiceDescription = sType.ServiceDescription,
                                        //selectedServiceId = AS.ServiceId ?? 0 //Comment to solve INPSW - 34
                                        selectedServiceId = SetSelectedServiceId//INPSW -34
                                    }).ToList();
            }

        
            //if(AS!=null)
            //{
            //    ServiceOfferings = (from sType in css.ServiceOfferings
            //                        where sType.ServiceTypeId == ServiceTypeIdInt && sType.IsDeleted == false
            //                        orderby sType.ServiceId
            //                        select new ServiceOfferingsNew()
            //                        {
            //                            ServiceId = sType.ServiceId,
            //                            ServiceDescription = sType.ServiceDescription,
            //                            selectedServiceId = AS.ServiceId ?? 0
            //                        }).ToList();

            return this.Json(ServiceOfferings, JsonRequestBehavior.AllowGet);
        }
    }

    public class ServiceOfferingsNew
    {
        public int ServiceId { get; set; }
        public string ServiceDescription { get; set; }
        public int selectedServiceId { get; set; }
    }
}
