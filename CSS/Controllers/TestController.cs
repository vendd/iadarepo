﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ViewModels;
using BLL;
using System.IO;
using System.Data.Objects;
using System.Data;

namespace CSS.Controllers
{
    [Authorize]
    public class TestController : Controller
    {
        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public ActionResult SertifiTest1()
        {
            
            return View();
        }
        public ActionResult XACTXMLRead()
        {
           


            return View();
        }
        public ActionResult UIControls()
        {
            return View();
        }
        public ActionResult ProgressStatus()
        {
            return View();
        }

        public ActionResult PopulateZipData()
        {
            return View();
        }
        //
        // GET: /Test/

        public ActionResult Index()
        {
            ServiceProviderRegistrationViewModel obj = new ServiceProviderRegistrationViewModel();
            return View(obj);
        }
        public JsonResult GetState(string Sel_Country)
        {
            tbl_ZipUSA db = new tbl_ZipUSA();
            JsonResult result = new JsonResult();

            var vStateList = css.tbl_ZipUSA.Find("NY");
            //result.Data = vStateList.ToList();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return result;
        }

        public ActionResult Submit()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Submit(HttpPostedFileBase file)
        {
            // Verify that the user selected a file
            if (file != null && file.ContentLength > 0)
            {
                // extract only the fielname
                var fileName = Path.GetFileName(file.FileName);
                // store the file inside ~/App_Data/uploads folder
                var path = Path.Combine(Server.MapPath("~/App_Data/Uploads"), fileName);
                file.SaveAs(path);
            }

           

          
            return RedirectToAction("Index");
        }

        public JsonResult UploadFile(HttpPostedFileBase file)
        {
            // Verify that the user selected a file
            if (file != null && file.ContentLength > 0)
            {
                // extract only the fielname
                var fileName = Path.GetFileName(file.FileName);
                // store the file inside ~/App_Data/uploads folder
                var path = Path.Combine(Server.MapPath("~/App_Data/Uploads"), fileName);
                file.SaveAs(path);
            }



            if (true)
            {
                return Json(0);
            }
            else
            {
                return Json(1);
            }
            // redirect back to the index action to show the form once again
           
        }
        public ActionResult RefreshItems()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RefreshItems(string a) {
        try {
            return View();
            // Fill List/Model

            // Return Partial
            //return PartialView("_ItemList");
        }
        catch (Exception ex) {

            return RedirectToAction("Index");
        }
        }

        public ActionResult TakeFile()
        {
            return View();
        }

        [HttpPost]
        public ActionResult TakeFile(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                // extract only the fielname
                var fileName = Path.GetFileName(file.FileName);
                // store the file inside ~/App_Data/uploads folder
                var path = Path.Combine(Server.MapPath("~/App_Data/Uploads"), fileName);
                file.SaveAs(path);
                return Json(new { fileName = file.FileName });
            }
            else
                return Json(new { fileName = "" });
            
        }

        public ActionResult Fupload()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Fupload(IEnumerable<HttpPostedFileBase> files)
        {
            foreach (var file in files)
            {
                var filename = Path.Combine(Server.MapPath("~/App_Data/Uploads"), file.FileName);
                file.SaveAs(filename);
            }
            return Json(files.Select(x => new { name = x.FileName }));
        }

        public ActionResult Ajax()
        {
            return View();
        }

        public ActionResult AjaxContent()
        {
            return View();
        }
       
    
    }
}
