﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BLL.Models;
using BLL.ViewModels;
using System.Data;
using System.Data.Objects;
using System.IO;
using System.Configuration;
using System.Net;
using System.Net.Mail;
namespace CSS.Controllers
{
   
    public class NewAssignmentController : CustomController
    {
        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        //
        // GET: /NewAssignment/

        public ActionResult Index()
        {
            CreateAssignmentViewModel viewModel = new CreateAssignmentViewModel();
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Index(CreateAssignmentViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                //return RedirectToAction("SelectServiceProvider", "NewAssignment", new { location = viewModel.Location });
                return RedirectToAction("PropertyAssignment", "PropertyAssignment", new { location = viewModel.Location });
            }
            return View(viewModel);
        }

        public ActionResult SelectServiceProvider(Int64 assignmentId)
        {
            AssignmentServiceProviderSearchViewModel viewModel = new AssignmentServiceProviderSearchViewModel();
            try
            {
                viewModel.Distance = 100;
                Claim claim = new Claim();

                if (assignmentId != null)
                {
                    viewModel.AssignmentId = assignmentId;
                }

                PropertyAssignment pa = css.PropertyAssignments.Find(assignmentId);
                int? companyID = null;
                if (pa != null)
                {
                    if (pa.ClaimId.HasValue)
                    {
                         claim = css.Claims.Find(pa.ClaimId);
                        if (claim != null)
                        {
                            if (!String.IsNullOrEmpty(claim.PropertyZip))
                            {
                                viewModel.Location = claim.PropertyZip;
                            }
                            if(claim.HeadCompanyId.HasValue)
                            {
                                if(claim.HeadCompanyId.Value > 0)
                                {
                                    companyID = claim.HeadCompanyId.Value;
                                }
                            }
                        }
                    }
                }


                viewModel.ServiceProvidersList = css.AssignmentSPSearch(viewModel.SPName, viewModel.Location, viewModel.Rank, viewModel.Experience, viewModel.Distance, companyID,claim.LOBId).ToList();
                List<GoogleMapLocation> markerList = new List<GoogleMapLocation>();
                foreach (var record in viewModel.ServiceProvidersList)
                {
                    if (record.RowNumber.HasValue && record.Lat.HasValue && record.Lng.HasValue)
                    {
                        GoogleMapLocation location = new GoogleMapLocation(record.RowNumber.Value, (!String.IsNullOrEmpty(record.Company) ? record.Company : ""), record.Lat.Value, record.Lng.Value);
                        markerList.Add(location);
                    }
                }
                GoogleMapViewModel googleMapViewModel = new GoogleMapViewModel();
                googleMapViewModel.MarkerLocationList = markerList;
                viewModel.GoogleMapViewModel = googleMapViewModel;
                string prevurl = Request.UrlReferrer.ToString();
                Session["claimid"] = pa.ClaimId.ToString();
                Session["PreviousUrl"] = Request.UrlReferrer.ToString();

            }
            catch (Exception ex)
            {
                string message = ex.Message;
            }
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult SelectServiceProvider(AssignmentServiceProviderSearchViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                        PropertyAssignment pa = css.PropertyAssignments.Find(viewModel.AssignmentId);
                        Claim claim = new Claim();

                    if (!String.IsNullOrEmpty(viewModel.SPName))
                    {
                        if (pa != null)
                        {
                            if (pa.ClaimId.HasValue)
                            {
                                 claim = css.Claims.Find(pa.ClaimId);
                                if (claim != null)
                                {
                                    if (!String.IsNullOrEmpty(claim.PropertyZip))
                                    {
                                        viewModel.Location = claim.PropertyZip;
                                    }
                                    if (claim.HeadCompanyId.HasValue)
                                    {
                                        if (claim.HeadCompanyId.Value > 0)
                                        {
                                            Company company = css.Companies.Find(claim.HeadCompanyId.Value);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    int? companyID = null;
                    if (pa != null)
                    {
                        if (pa.ClaimId.HasValue)
                        {
                            claim = css.Claims.Find(pa.ClaimId);
                            if (claim != null)
                            {
                              
                                if (claim.HeadCompanyId.HasValue)
                                {
                                    if (claim.HeadCompanyId.Value > 0)
                                    {
                                        companyID = claim.HeadCompanyId.Value;
                                        
                                    }
                                }
                            }
                        }
                    }
                    viewModel.ServiceProvidersList = css.AssignmentSPSearch(viewModel.SPName, viewModel.Location, viewModel.Rank, viewModel.Experience, viewModel.Distance,companyID,claim.LOBId).ToList();
                    List<GoogleMapLocation> markerList = new List<GoogleMapLocation>();
                    foreach (var record in viewModel.ServiceProvidersList)
                    {
                        if (record.RowNumber.HasValue && record.Lat.HasValue && record.Lng.HasValue)
                        {
                            GoogleMapLocation location = new GoogleMapLocation(record.RowNumber.Value, (!String.IsNullOrEmpty(record.Company) ? record.Company : ""), record.Lat.Value, record.Lng.Value);
                            markerList.Add(location);
                        }
                    }
                    GoogleMapViewModel googleMapViewModel = new GoogleMapViewModel();
                    googleMapViewModel.MarkerLocationList = markerList;
                    viewModel.GoogleMapViewModel = googleMapViewModel;
                }
                catch (Exception ex)
                {
                }
            }

            return View(viewModel);
        }

        public ActionResult AutoAssignServiceProvider(Int64 assignmentId, Int64 OAUserId, string IsInvoiceMgmt = "0")//INPSW -32 IsInvoiceMgmt
        {
            AssignServiceProviderViewModel viewModel = new AssignServiceProviderViewModel();
            viewModel.hasBeenAssignedSuccessfully = false;
            try
            {
                if (assignmentId != null && OAUserId != null)
                {
                    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                    AutoAssignment AA = css.AutoAssignments.Find(assignmentId);
                    Claim cl = AA.Claim;
                  
                    // Claim cl = css.Claims.Find(AA.ClaimId);
                    bool isReassign = false;
                    if (AA.OAUserID.HasValue)
                    {
                        if (AA.OAUserID.Value != 0)
                        {
                            isReassign = true;
                        }
                    }
                    if (!isReassign)
                    {
                        css.AssignServiceProviderToAutoAssignment(assignmentId, OAUserId, loggedInUser.UserId,IsInvoiceMgmt);//INPSW -32 IsInvoiceMgmt
                    }
                    else
                    {   
                        AutoAssignOldSPCancelMail(AA.OAUserID, cl.ClaimNumber, assignmentId); //INPSW -54                         
						css.ReAssignServiceProviderToAutoAssignment(assignmentId, OAUserId, loggedInUser.UserId);
                    }

                    viewModel.OAUserId = OAUserId;
                    viewModel.AssignmentId = assignmentId;
                    viewModel.hasBeenAssignedSuccessfully = true;
                    css.SaveChanges();
                    byte SPPayPertentage;
                    byte HoldBackPertentage;

                    //Makes use of Percentage values stored in the most recent Property Assignment 
                    if (AA.SPPayPercent.HasValue)
                    {
                        SPPayPertentage = AA.SPPayPercent.Value;
                    }
                    else if (cl.CauseTypeOfLoss == 8)
                    {
                        SPPayPertentage = Convert.ToByte("65");
                    }
                    else
                    {
                        SPPayPertentage = Convert.ToByte(System.Configuration.ConfigurationManager.AppSettings["DefaultSPPayPercent"].ToString());
                    }
                    if (AA.HoldBackPercent.HasValue)
                    {
                        HoldBackPertentage = AA.HoldBackPercent.Value;
                    }
                    else
                    {
                        HoldBackPertentage = Convert.ToByte(System.Configuration.ConfigurationManager.AppSettings["DefaultHoldbackPercent"].ToString());
                    }

                    SetupAutoSPPayPercentage(assignmentId, OAUserId, SPPayPertentage, HoldBackPertentage);
                }
            }
            catch (Exception ex)
            {
                viewModel.hasBeenAssignedSuccessfully = false;
                Utility.LogException("method AutoAssignServiceProvider", ex, "An Error occured while assigning SP:" + OAUserId + " to Assignment:" + assignmentId);
            }
            return View(viewModel);
        }
        //INPSW -54
        private void AutoAssignOldSPCancelMail(Int64? spUserId, string ClaimNumber, Int64 autoAssignmentId)
        {

            string spEmail = String.Empty;
            string fromEmail = MvcApplication.AdminMail;
            string subject;
            string mailBody = "";
            subject = "Claim #: " + ClaimNumber + " - Assignment Canceled";
            try
            {
                if (spUserId.HasValue)
                {
                    User sp = css.Users.Find(spUserId);
                    spEmail = sp.Email;
                    if (String.IsNullOrEmpty(sp.Email) && sp.User1 != null)
                        spEmail = sp.User1.Email;
                    string spFullName = sp.FirstName + " " + sp.LastName;
                    mailBody = "<br /><br/>";
                    mailBody += "DO NOT REPLY TO THIS EMAIL. THIS IS A SYSTEM GENERATED EMAIL. THE EMAIL MAILBOX IS NOT MONITORED.";
                    mailBody += "<br /><br />";
                    mailBody += "Dear " + spFullName + ",<br/><br/>";
                    mailBody += "The assignment for the following claim number has been canceled.";
                    mailBody += "<br/><br/ > Claim Number:  " + ClaimNumber;
                    mailBody += "<br /><br/>Thank you,";
                    mailBody += "<br />IADA Customer Support Team";

                }
                bool successFlag = Utility.sendEmail(spEmail, fromEmail, null, subject, mailBody);
                if (!successFlag)
                    css.FailedEmailLogInsert(spEmail, "ClaimInfo", MvcApplication.AdminMail, subject, mailBody, null, ClaimNumber, autoAssignmentId, "", spUserId, null);
                else
                    css.usp_ExceptionLogInsert("Success ", spEmail, subject, ClaimNumber + " " + autoAssignmentId, "");
            }
            catch (Exception ex)
            {

            }
        }

        public ActionResult AssignServiceProvider(Int64 assignmentId, Int64 OAUserId)
        {
            AssignServiceProviderViewModel viewModel = new AssignServiceProviderViewModel();
            viewModel.hasBeenAssignedSuccessfully = false;
            try
            {
                if (assignmentId != null && OAUserId != null)
                {
                    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                    PropertyAssignment pa = css.PropertyAssignments.Find(assignmentId);
                    Claim cl = css.Claims.Find(pa.ClaimId);
                    bool isReassign = false;
                    if (pa.OAUserID.HasValue)
                    {
                        if (pa.OAUserID.Value != 0)
                        {
                            isReassign = true;
                        }
                    }
                    if (!isReassign)
                    {
                        css.AssignServiceProviderToAssignment(assignmentId, OAUserId, loggedInUser.UserId);
                    }
                    else
                    {
                        css.usp_ReAssignServiceProviderToAssignment(assignmentId, OAUserId, loggedInUser.UserId);
                    }

                    viewModel.OAUserId = OAUserId;
                    viewModel.AssignmentId = assignmentId;
                    viewModel.hasBeenAssignedSuccessfully = true;
                    css.SaveChanges();
                    byte SPPayPertentage;
                    byte HoldBackPertentage;

                    //Makes use of Percentage values stored in the most recent Property Assignment 
                    if (pa.SPPayPercent.HasValue)
                    {
                        SPPayPertentage = pa.SPPayPercent.Value;
                    }
                    else if (cl.CauseTypeOfLoss == 8)
                    {
                        SPPayPertentage = Convert.ToByte("65");
                    }
                    else
                    {
                        SPPayPertentage = Convert.ToByte(System.Configuration.ConfigurationManager.AppSettings["DefaultSPPayPercent"].ToString());
                    }
                    if (pa.HoldBackPercent.HasValue)
                    {
                        HoldBackPertentage = pa.HoldBackPercent.Value;
                    }
                    else
                    {
                        HoldBackPertentage = Convert.ToByte(System.Configuration.ConfigurationManager.AppSettings["DefaultHoldbackPercent"].ToString());
                    }

                    SetupSPPayPercentage(assignmentId, OAUserId, SPPayPertentage, HoldBackPertentage);
                }
            }
            catch (Exception ex)
            {
                viewModel.hasBeenAssignedSuccessfully = false;
                Utility.LogException("method AssignServiceProvider", ex, "An Error occured while assigning SP:" + OAUserId + " to Assignment:" + assignmentId);

                //viewModel.errorMessage = ex.InnerException.Message;
            }
            return View(viewModel);
        }

        private string generateExhibitDocs(Int64 loggedInUserId, string claimNumber, DateTime cstDateTime, Int64 assignmentId, byte spPayPercentage, byte holdbackPercentage)
        {
            byte[] fileBytes;

            //Generate PDF Bytes
            PropertyAssignmentController controller = new PropertyAssignmentController();


            //Triage
            //controller.ControllerContext = this.ControllerContext;
            //string triageHtml = ((JsonResult)controller.TriageDetails(assignmentId, true)).Data.ToString();
            //triageHtml += "<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
            //triageHtml += "<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
            //triageHtml += "<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
            //triageHtml += "<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
            //triageHtml += "<br/><br/><br/><br/><br/><br/><br/>";

            //Exhibit B
            ExhibitBDocViewModel vmExhibitB = new ExhibitBDocViewModel();
            vmExhibitB.AssignmentId = assignmentId;
            vmExhibitB.SPPayPercent = spPayPercentage;
            string exhibitBHtml = RenderPartialViewToString("_ExhibitBDoc", vmExhibitB);
            exhibitBHtml += "<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
            exhibitBHtml += "<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";
            exhibitBHtml += "<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";

            //Exhibit C
            ExhibitCDocViewModel vmExhibitC = new ExhibitCDocViewModel();
            vmExhibitC.AssignmentId = assignmentId;
            vmExhibitC.HoldBackPercent = holdbackPercentage;
            string exhibitCHtml = RenderPartialViewToString("_ExhibitCDoc", vmExhibitC);

            fileBytes = Utility.ConvertHTMLStringToPDF(exhibitBHtml + exhibitCHtml, 10, 10, 10, 20, true, "Claim #: " + claimNumber + "                                                                                                                                         Document Generated On: " + cstDateTime.ToString("MM/dd/yyyy"), 20, false, "");


            //Store PDF Bytes

            #region Cloud Storage
            string documentTypeId = 9 + "";
            string originalFileName = "Exhibit.pdf";
            Int64 claimId = css.PropertyAssignments.Find(assignmentId).ClaimId.Value;
            List<Int64> assignmentIdsList = css.PropertyAssignments.Where(x => x.ClaimId == claimId).Select(x => x.AssignmentId).ToList();
            int revisionCount = css.Documents.Where(x => assignmentIdsList.Contains(x.AssignmentId) == true && (x.DocumentTypeId == 9) && (x.OriginalFileName == originalFileName)).ToList().Count;
            string fileName = "Exhibit" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
            string relativeFileName = assignmentId + "/" + documentTypeId + "/" + fileName;

            string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
            CloudStorageUtility.StoreFile(containerName, relativeFileName, fileBytes);
            string absoluteFileURL = CloudStorageUtility.GetAbsoluteFileURL(containerName, relativeFileName);
            #endregion

            #region Local File System
            //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
            //string documentTypeId = 9 + "";
            //string originalFileName = "Exhibit.pdf";
            //Int64 claimId = css.PropertyAssignments.Find(assignmentId).ClaimId.Value;
            //List<Int64> assignmentIdsList = css.PropertyAssignments.Where(x => x.ClaimId == claimId).Select(x => x.AssignmentId).ToList();
            //int revisionCount = css.Documents.Where(x => assignmentIdsList.Contains(x.AssignmentId) == true && (x.DocumentTypeId == 9) && (x.OriginalFileName == originalFileName)).ToList().Count;
            //string fileName = "Exhibit" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
            //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId)))
            //{
            //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId));
            //}
            //path = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), fileName);
            //Utility.StoreBytesAsFile(fileBytes, path);
            #endregion


            ObjectParameter outDocumentId = new ObjectParameter("DocumentId", DbType.Int64);
            css.DocumentsInsert(outDocumentId, assignmentId, DateTime.Now, "Agreement Exhibit" + (revisionCount > 0 ? " (Revision " + revisionCount + ")" : ""), originalFileName, fileName, loggedInUserId, 9);

            return absoluteFileURL;
        }
        private string generateRequirementsHTML(Int64 loggedInUserId, Int64 assignmentId)
        {

            //Generate PDF Bytes
            PropertyAssignmentController controller = new PropertyAssignmentController();
            controller.ControllerContext = this.ControllerContext;
            RequirementViewModel viewModel = new RequirementViewModel();
            string requirementsHtml = ((JsonResult)controller.RequiermentsMail(assignmentId, true)).Data.ToString();

            return requirementsHtml;
        }

        public string SetupSPPayPercentage(Int64 assignmentId, Int64? spUserId, byte spPayPercentage, byte holdbackPercentage)
        {
            string valueToReturn = "0";
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                PropertyAssignment pa = css.PropertyAssignments.Find(assignmentId);
                Claim claim = css.Claims.Find(pa.ClaimId);
                string claimCompany = "";
                if (claim.HeadCompanyId.HasValue)
                {
                    Company company = css.Companies.Find(claim.HeadCompanyId);
                    claimCompany = company.CompanyName;
                }

                css.usp_AssignmentSPPayPercentageUpdate(assignmentId, spPayPercentage, holdbackPercentage, loggedInUser.UserId);
                DateTime cstDateTime = css.usp_GetLocalDateTime().First().Value;

                string attachmentPath = generateExhibitDocs(loggedInUser.UserId, claim.ClaimNumber, cstDateTime, assignmentId, spPayPercentage, holdbackPercentage);

                //Send an email to the SP only if an SP is assigned to this claim: 29/05/2013
                if (spUserId.HasValue)
                {
                    User sp = css.Users.Find(spUserId);
                    string requirementsHTML = generateRequirementsHTML(loggedInUser.UserId, assignmentId);
                    string assignmentdescription = css.PropertyAssignments.SingleOrDefault(x => x.AssignmentId == assignmentId).AssignmentDescription;
                  
                    string subject;
                    string spFullName = sp.FirstName + " " + sp.LastName;
                    string spEmail = sp.Email;
                    string fromEmail = System.Configuration.ConfigurationManager.AppSettings["SupportEmailAddress"] + "";
                    if (!string.IsNullOrEmpty(claimCompany))
                        subject = "Claim #: " + claim.ClaimNumber + " Client: " + claimCompany + " assigned";
                    else
                        subject = "Claim #: " + claim.ClaimNumber + " assigned";
                    string mailBody = "Dear " + spFullName + ",<br/><br/>";
                    if (!string.IsNullOrEmpty(claimCompany))
                        mailBody += "<b>Claim #: " + claim.ClaimNumber + " Client: " + claimCompany + "</b> has been assigned to you.<br /><br/>";
                    else
                        mailBody += "<b>Claim #: " + claim.ClaimNumber + "</b> has been assigned to you.<br /><br/>";

                    #region Additional Info
                    if (assignmentdescription != null)
                    {

                        mailBody = mailBody + "<br/><b><u>Claim Description :</u></b><br/><br/> ";
                        mailBody = mailBody + assignmentdescription + "<br/><br/>";
                    }

                    mailBody += "<br/><b><u>*SEE FULL REQUIREMENTS FOR THIS CLAIM*</u></b><br/>";
                    ClaimRequirement Commentreq = css.ClaimRequirements.Where(x => x.AssignmentId == assignmentId).FirstOrDefault();

                    if (Commentreq != null)
                    {
                        if (Commentreq.Comments != null)
                        {
                            mailBody += "<b><u>Comments or Other Instructions :</u></b><br/><br/> " + Commentreq.Comments + "<br /><br />";
                        }
                    }


                    if ((claim.InsuredFirstName != null || claim.InsuredLastName != null) && claim.PropertyAddress1 != null)
                    {
                        if (!string.IsNullOrEmpty(claim.InsuredFirstName) && (!string.IsNullOrEmpty(claim.InsuredLastName)))
                        {
                            if (!string.IsNullOrEmpty(claim.InsuredMobilePhone))
                            {
                                


                                if (!string.IsNullOrEmpty(claim.PropertyContactName) && !string.IsNullOrEmpty(claim.PropertyContactPhone))
                                {
                                    if (claim.PropertyContactName.CompareTo(claim.InsuredFirstName + " " + claim.InsuredLastName) != 0)
                                    {
                                        mailBody += "<b>Insured Name : </b>" + claim.InsuredFirstName + " " + claim.InsuredLastName + "<br /><b> Insured Phone : </b>" + claim.InsuredMobilePhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><b> Contact Name : </b>" + claim.PropertyContactName + "<br /><b> Contact Phone : </b>" + claim.PropertyContactPhone + "<br/><br/>";
                                    }
                                    else
                                    {
                                        mailBody += "<b>Insured Name : </b>" + claim.InsuredFirstName + " " + claim.InsuredLastName + "<br /><b> Insured Phone : </b>" + claim.InsuredMobilePhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><br/>";

                                    }

                                }
                                else
                                {
                                    mailBody += "<b>Insured Name : </b>" + claim.InsuredFirstName + " " + claim.InsuredLastName + "<br /><b> Insured Phone : </b>" + claim.InsuredMobilePhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><br/>";
                                }
                            }
                            if (!string.IsNullOrEmpty(claim.InsuredHomePhone))
                            {
                                //mailBody += "<br/><b><u>*SEE FULL REQUIREMENTS FOR THIS CLAIM*</u></b><br/>";
                                if (!string.IsNullOrEmpty(claim.PropertyContactName) && !string.IsNullOrEmpty(claim.PropertyContactPhone))
                                {
                                    if (claim.PropertyContactName.CompareTo(claim.InsuredFirstName + " " + claim.InsuredLastName) != 0)
                                    {
                                        mailBody += "<b>Insured Name : </b>" + claim.InsuredFirstName + " " + claim.InsuredLastName + "<br /><b> Insured Phone : </b>" + claim.InsuredHomePhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><b> Contact Name : </b>" + claim.PropertyContactName + "<br /><b> Contact Phone : </b>" + claim.PropertyContactPhone + "<br/><br/>";
                                    }
                                    else
                                    {
                                        mailBody += "<b>Insured Name : </b>" + claim.InsuredFirstName + " " + claim.InsuredLastName + "<br /><b> Insured Phone : </b>" + claim.InsuredHomePhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><br/>";

                                    }

                                }
                                else
                                {
                                    mailBody += "<b>Insured Name : </b>" + claim.InsuredFirstName + " " + claim.InsuredLastName + "<br /><b> Insured Phone : </b>" + claim.InsuredHomePhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><br/>";
                                }

                            }
                            if (!string.IsNullOrEmpty(claim.InsuredBusinessPhone))
                            {
                                //mailBody += "<br/><b><u>*SEE FULL REQUIREMENTS FOR THIS CLAIM*</u></b><br/>";
                                if (!string.IsNullOrEmpty(claim.PropertyContactName) && !string.IsNullOrEmpty(claim.PropertyContactPhone))
                                {
                                    if (claim.PropertyContactName.CompareTo(claim.InsuredFirstName + " " + claim.InsuredLastName) != 0)
                                    {
                                        mailBody += "<b>Insured Name : </b>" + claim.InsuredFirstName + " " + claim.InsuredLastName + "<br /><b> Insured Phone : </b>" + claim.InsuredBusinessPhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><b> Contact Name : </b>" + claim.PropertyContactName + "<br /><b> Contact Phone : </b>" + claim.PropertyContactPhone + "<br/><br/>";
                                    }
                                    else
                                    {
                                        mailBody += "<b>Insured Name : </b>" + claim.InsuredFirstName + " " + claim.InsuredLastName + "<br /><b> Insured Phone : </b>" + claim.InsuredBusinessPhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><br/>";

                                    }

                                }
                                else
                                {
                                    mailBody += "<b>Insured Name : </b>" + claim.InsuredFirstName + " " + claim.InsuredLastName + "<br /><b> Insured Phone : </b>" + claim.InsuredBusinessPhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><br/>";
                                }

                            }


                        }
                        else if (!string.IsNullOrEmpty(claim.InsuredFirstName))
                        {
                            if (!string.IsNullOrEmpty(claim.InsuredMobilePhone))
                            {
                                //mailBody += "<br/><b><u>*SEE FULL REQUIREMENTS FOR THIS CLAIM*</u></b><br/>";
                                if (!string.IsNullOrEmpty(claim.PropertyContactName) && !string.IsNullOrEmpty(claim.PropertyContactPhone))
                                {
                                    if (claim.PropertyContactName.CompareTo(claim.InsuredFirstName) != 0)
                                    {
                                        mailBody += "<b>Insured Name : </b>" + claim.InsuredFirstName + "<br /><b> Insured Phone : </b>" + claim.InsuredMobilePhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><b> Contact Name : </b>" + claim.PropertyContactName + "<br /><b> Contact Phone : </b>" + claim.PropertyContactPhone + "<br/><br/>";
                                    }
                                    else
                                    {
                                        mailBody += "<b>Insured Name : </b>" + claim.InsuredFirstName + "<br /><b> Insured Phone : </b>" + claim.InsuredMobilePhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><br/>";

                                    }

                                }
                                else
                                {
                                    mailBody += "<b>Insured Name : </b>" + claim.InsuredFirstName + "<br /><b> Insured Phone : </b>" + claim.InsuredMobilePhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><br/>";
                                }
                            }
                            if (!string.IsNullOrEmpty(claim.InsuredHomePhone))
                            {
                                //mailBody += "<br/><b><u>*SEE FULL REQUIREMENTS FOR THIS CLAIM*</u></b><br/>";
                                if (!string.IsNullOrEmpty(claim.PropertyContactName) && !string.IsNullOrEmpty(claim.PropertyContactPhone))
                                {
                                    if (claim.PropertyContactName.CompareTo(claim.InsuredFirstName) != 0)
                                    {
                                        mailBody += "<b>Insured Name : </b>" + claim.InsuredFirstName + "<br /><b> Insured Phone : </b>" + claim.InsuredHomePhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><b> Contact Name : </b>" + claim.PropertyContactName + "<br /><b> Contact Phone : </b>" + claim.PropertyContactPhone + "<br/><br/>";
                                    }
                                    else
                                    {
                                        mailBody += "<b>Insured Name : </b>" + claim.InsuredFirstName + "<br /><b> Insured Phone : </b>" + claim.InsuredHomePhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><br/>";

                                    }

                                }
                                else
                                {
                                    mailBody += "<b>Insured Name : </b>" + claim.InsuredFirstName + "<br /><b> Insured Phone : </b>" + claim.InsuredHomePhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><br/>";
                                }

                            }
                            if (!string.IsNullOrEmpty(claim.InsuredBusinessPhone))
                            {
                                //mailBody += "<br/><b><u>*SEE FULL REQUIREMENTS FOR THIS CLAIM*</u></b><br/>";
                                if (!string.IsNullOrEmpty(claim.PropertyContactName) && !string.IsNullOrEmpty(claim.PropertyContactPhone))
                                {
                                    if (claim.PropertyContactName.CompareTo(claim.InsuredFirstName) != 0)
                                    {
                                        mailBody += "<b>Insured Name : </b>" + claim.InsuredFirstName + "<br /><b> Insured Phone : </b>" + claim.InsuredBusinessPhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><b> Contact Name : </b>" + claim.PropertyContactName + "<br /><b> Contact Phone : </b>" + claim.PropertyContactPhone + "<br/><br/>";
                                    }
                                    else
                                    {
                                        mailBody += "<b>Insured Name : </b>" + claim.InsuredFirstName + "<br /><b> Insured Phone : </b>" + claim.InsuredBusinessPhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><br/>";

                                    }

                                }
                                else
                                {
                                    mailBody += "<b>Insured Name : </b>" + claim.InsuredFirstName + "<br /><b> Insured Phone : </b>" + claim.InsuredBusinessPhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><br/>";
                                }

                            }
                        }
                        else if (!string.IsNullOrEmpty(claim.InsuredLastName))
                        {
                            if (!string.IsNullOrEmpty(claim.InsuredMobilePhone))
                            {
                                //mailBody += "<br/><b><u>*SEE FULL REQUIREMENTS FOR THIS CLAIM*</u></b><br/>";
                                if (!string.IsNullOrEmpty(claim.PropertyContactName) && !string.IsNullOrEmpty(claim.PropertyContactPhone))
                                {
                                    if (claim.PropertyContactName.CompareTo(claim.InsuredLastName) != 0)
                                    {
                                        mailBody += "<b>Insured Name : </b>" + claim.InsuredLastName + "<br /><b> Insured Phone : </b>" + claim.InsuredMobilePhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><b> Contact Name : </b>" + claim.PropertyContactName + "<br /><b> Contact Phone : </b>" + claim.PropertyContactPhone + "<br/><br/>";
                                    }
                                    else
                                    {
                                        mailBody += "<b>Insured Name : </b>" + claim.InsuredLastName + "<br /><b> Insured Phone : </b>" + claim.InsuredMobilePhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><br/>";

                                    }

                                }
                                else
                                {
                                    mailBody += "<b>Insured Name : </b>" + claim.InsuredLastName + "<br /><b> Insured Phone : </b>" + claim.InsuredMobilePhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><br/>";
                                }

                            }
                            if (!string.IsNullOrEmpty(claim.InsuredHomePhone))
                            {
                                //mailBody += "<br/><b><u>*SEE FULL REQUIREMENTS FOR THIS CLAIM*</u></b><br/>";
                                if (!string.IsNullOrEmpty(claim.PropertyContactName) && !string.IsNullOrEmpty(claim.PropertyContactPhone))
                                {
                                    if (claim.PropertyContactName.CompareTo(claim.InsuredLastName) != 0)
                                    {
                                        mailBody += "<b>Insured Name : </b>" + claim.InsuredLastName + "<br /><b> Insured Phone : </b>" + claim.InsuredHomePhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><b> Contact Name : </b>" + claim.PropertyContactName + "<br /><b> Contact Phone : </b>" + claim.PropertyContactPhone + "<br/><br/>";
                                    }
                                    else
                                    {
                                        mailBody += "<b>Insured Name : </b>" + claim.InsuredLastName + "<br /><b> Insured Phone : </b>" + claim.InsuredHomePhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><br/>";

                                    }

                                }
                                else
                                {
                                    mailBody += "<b>Insured Name : </b>" + claim.InsuredLastName + "<br /><b> Insured Phone : </b>" + claim.InsuredHomePhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><br/>";
                                }

                            }
                            if (!string.IsNullOrEmpty(claim.InsuredBusinessPhone))
                            {
                                //mailBody += "<br/><b><u>*SEE FULL REQUIREMENTS FOR THIS CLAIM*</u></b><br/>";
                                if (!string.IsNullOrEmpty(claim.PropertyContactName) && !string.IsNullOrEmpty(claim.PropertyContactPhone))
                                {
                                    if (claim.PropertyContactName.CompareTo(claim.InsuredLastName) != 0)
                                    {
                                        mailBody += "<b>Insured Name : </b>" + claim.InsuredLastName + "<br /><b> Insured Phone : </b>" + claim.InsuredBusinessPhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><b> Contact Name : </b>" + claim.PropertyContactName + "<br /><b> Contact Phone : </b>" + claim.PropertyContactPhone + "<br/><br/>";
                                    }
                                    else
                                    {
                                        mailBody += "<b>Insured Name : </b>" + claim.InsuredLastName + "<br /><b> Insured Phone : </b>" + claim.InsuredBusinessPhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><br/>";

                                    }

                                }
                                else
                                {
                                    mailBody += "<b>Insured Name : </b>" + claim.InsuredLastName + "<br /><b> Insured Phone : </b>" + claim.InsuredBusinessPhone + "<br /><b> Address of loss : </b>" + claim.PropertyAddress1 + "<br /><br/>";
                                }

                            }

                        }

                    }
                    


                    #endregion

                    mailBody += "<br /><br /> A document with the following information has been attached.<br/>";
                    mailBody += " 1. Agreement Exhibit B<br/>";
                    mailBody += " 2. Agreement Exhibit C<br/><br/>";
                    mailBody += "<b>Claim Requirements</b>";
                    mailBody += requirementsHTML;
                    mailBody += "<br />";
                    mailBody += "<br />";
                    mailBody += "<br />Thanking you,";
                    mailBody += "<br />";
                    mailBody += "<br />Support,";
                    mailBody += "<br />IADA Claim Solutions Inc.";
                    List<string> attachmentList = new List<string>();
                    List<string> files=new List<string>();
                   // attachmentList.Add(attachmentPath);


                   // PropertyAssignmentController PAC = new PropertyAssignmentController();
                    ReadCloudFile(attachmentPath, "Exhibit.pdf", ref attachmentList, ref files);

                    Utility.sendEmail(spEmail, fromEmail, subject, mailBody, attachmentList);


                    foreach (var file in files)
                    {
                        if (System.IO.File.Exists(file))
                        {
                            System.IO.File.Delete(file);
                        }

                    }


                }
                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                Utility.LogException("Method SetupSPPayPercentage 1", ex, "An error occured while sending mail to Assignment:" + assignmentId);
                valueToReturn = ex.Message + "" + ex.InnerException != null ? "  " + ex.InnerException.Message + "" : "";
            }
            return valueToReturn;
        }

        public string SetupAutoSPPayPercentage( Int64 autoassignmentId, Int64? spUserId, byte spPayPercentage, byte holdbackPercentage)
        {
            string valueToReturn = "0";
            Stream claimInfoData = null;
             string spEmail =String.Empty;
            try
            {               
                    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                    AutoAssignment pa = css.AutoAssignments.Find(autoassignmentId);
                    Claim claim = pa.Claim;
                    string claimCompany = "";
                    int ServiceId = 0;
                    if (claim.Company!=null)
                    {
                      //  Company company = css.Companies.Find(claim.HeadCompanyId);
                        claimCompany = claim.Company.CompanyName;
                        ServiceId = Convert.ToInt16(pa.ServiceId);
                    }

                    css.usp_AutoAssignmentSPPayPercentageUpdate(autoassignmentId, spPayPercentage, holdbackPercentage, loggedInUser.UserId);

                    DateTime cstDateTime = css.usp_GetLocalDateTime().First().Value;

                    // string attachmentPath = generateExhibitDocs(loggedInUser.UserId, claim.ClaimNumber, cstDateTime, autoassignmentId, spPayPercentage, holdbackPercentage);

                    //Send an email to the SP only if an SP is assigned to this claim: 29/05/2013
                    if (spUserId.HasValue)
                    {
                        User sp = css.Users.Find(spUserId);
                        //User sp = pa.User;
                        
                        spEmail = sp.Email;
                        if (String.IsNullOrEmpty(sp.Email) && sp.User1 != null)
                        {
                            spEmail = sp.User1.Email;
                        }
                        //string requirementsHTML = generateRequirementsHTML(loggedInUser.UserId, autoassignmentId);
                        // string assignmentdescription = css.AutoAssignments.SingleOrDefault(x => x.AutoAssignmentId == autoassignmentId).AutoAssignmentDescription;                                       
                        string fromEmail = MvcApplication.AdminMail;
                        string subject;
                        string mailBody = "";
                        Attachment attachment = null;
                        if (ServiceId != 13)
                        {
                           
                            string spFullName = sp.FirstName + " " + sp.LastName;


                            if (!string.IsNullOrEmpty(claimCompany))
                                subject = "Claim #: " + claim.ClaimNumber + " - New Assignment Notification";
                            else
                                subject = "Claim #: " + claim.ClaimNumber + " - New Assignment Notification";

                         mailBody = "<br />";
                         mailBody += "DO NOT REPLY TO THIS EMAIL. THIS IS A SYSTEM GENERATED EMAIL. THE EMAIL MAILBOX IS NOT MONITORED.";
                         mailBody += "<br /><br />";   
                         mailBody += "Dear " + spFullName + ",<br/><br/>";                      
                         mailBody += "You have been assigned an assignment <b>(see attached)</b>.";
                         mailBody += " Log into <a href=\"" + ConfigurationManager.AppSettings["IadaLoginUrl"] + "\">www.IADApro.com</a> and click the green check mark to accept the assignment or click the red X to reject the assignment.";
                         mailBody += " Upon completion of the task, please upload documentation and invoice the assignment at <a href=\"" + ConfigurationManager.AppSettings["IadaLoginUrl"] + "\">www.IADApro.com</a><br /><br/>";                      
                         mailBody += "<br />Thank you,";
                         mailBody += "<br />IADA Customer Support Team";                       
                        string mimeType;
                        string ReportPath = Server.MapPath(MvcApplication.ReportPath + "ClaimDetailsPrint.rdlc");
                        byte[] renderedBytes = Utility.ConvertRDLCToByteArray(claim.ClaimId,  out mimeType, pa.ServiceId.Value);
                        StoreClaimInfoPDF(Convert.ToInt64(autoassignmentId), loggedInUser, renderedBytes);
                        claimInfoData = new MemoryStream(renderedBytes);
                        attachment = new Attachment(claimInfoData, "Claim_Info.pdf");                           
                        }
                        else
                        {

                           
                            string spFullName = sp.FirstName + " " + sp.LastName;
                            subject = "Claim #:" + claim.ClaimNumber + " - QRP Assignment Notification";
                            mailBody = "<br />";
                            mailBody += "DO NOT REPLY TO THIS EMAIL. THIS IS A SYSTEM GENERATED EMAIL. THE EMAIL MAILBOX IS NOT MONITORED.";
                            mailBody += "<br /><br />";
                            mailBody += "Dear Repair Manager,"+"<br/><br/>";
                            mailBody += "You have been assigned a <b>Quality Repair Program - Vehicle Inspection</b> assignment (<b>see attached</b>).  As part of the <b>Assurant Quality Repair Program</b>, please follow these guidelines:" + "<br/>";
                            mailBody += "<br/><br/>";
                            mailBody += "<ul>";
                            mailBody += "<li>Please <b>contact the Vehicle Owner within 2 hours</b> to schedule and time for this vehicle inspection.</li>";
                            mailBody += "<li>As OE Guidelines dictate<b>, PRE and POST diagnostic scans will be required</b> prior to vehicle release.</li>";
                            mailBody += "<li>Follow recommended <b>Manufacturer Repair Methods</b> utilizing industry tools such as <b>ALLDATA</b>.</li>";
                            mailBody += "<li>Please document all <b>Unrelated Prior Damage (UPD) separately from this Damage Estimate</b> for this claim.</li>";
                            mailBody += "<li>If the vehicle is an <b>obvious Total Loss please complete a CCC BCIF for conditioning and submit details with the Damage Estimate.</b></li>";
                            mailBody += "<li>Please contact the <b>Claim Representative</b> listed in the assignment if there are any issues to be addressed on inspection or <b>difficulty scheduling with Vehicle Owner</b>. </li>";
                            mailBody += "</ul>";
                            mailBody += "Log into <a href=\"" + ConfigurationManager.AppSettings["IadaLoginUrl"] + "\">www.IADApro.com</a> to accept the assignment and upload the estimate documentation.  A username and password have been sent separately along with access to the mobile app for direct Peer to Peer video review with our Auto Specialist team.  They will be available online for Direct Supplement Approvals and Estimate Damage issues.  We look forward to working with you.";
                            mailBody += "<br/><br/>";
                            mailBody += "Thank you,"+"<br/><br/>";
                            mailBody += "The Assurant Auto Support Team  (QRP)"+"<br/>";
                            mailBody += "Phone: 855-876-0367";    
                            string mimeType;
                            string ReportPath = Server.MapPath(MvcApplication.ReportPath + "ClaimDetailsPrint.rdlc");
                            byte[] renderedBytes = Utility.ConvertRDLCToByteArray(claim.ClaimId, out mimeType, pa.ServiceId.Value);
                            StoreClaimInfoPDF(Convert.ToInt64(autoassignmentId), loggedInUser, renderedBytes);
                            claimInfoData = new MemoryStream(renderedBytes);
                             attachment = new Attachment(claimInfoData, "Claim_Info.pdf");
                         }
                       
                        bool successFlag = Utility.sendEmail(spEmail, fromEmail, null, subject, mailBody, attachment);

                        if (!successFlag)
                        {
                            css.FailedEmailLogInsert(spEmail, "ClaimInfo", MvcApplication.AdminMail, subject, mailBody, null, claim.ClaimNumber, pa.AutoAssignmentId, "", sp.UserId, null);
                        }
                        else
                        {
                            css.usp_ExceptionLogInsert("Success ", spEmail, subject, claim.ClaimNumber + " " + pa.AutoAssignmentId, "");
                        }
                       

                    }
                    valueToReturn = "1";

            }
            catch (Exception ex)
            {
                Utility.LogException("Method SetupAutoSPPayPercentage 2", ex, "An error occured while sending mail to SP:" + spEmail);
                valueToReturn = ex.Message + "" + ex.InnerException != null ? "  " + ex.InnerException.Message + "" : "";
            }
            finally {
                claimInfoData.Close();
                claimInfoData.Dispose();
            }

         
            return valueToReturn;
        }
               private static void ReadWriteStream(Stream readStream, Stream writeStream)
        {
            int Length = 25600000;
            Byte[] buffer = new Byte[Length];
            int bytesRead = readStream.Read(buffer, 0, Length);
            // FileStream outFile = null;
            // write the required bytes
            while (bytesRead > 0)
            {
                writeStream.Write(buffer, 0, bytesRead);
                bytesRead = readStream.Read(buffer, 0, Length);
            }
            readStream.Dispose();
            //  outFile = (FileStream)writeStream;
            //  writeStream.Close();
            writeStream.Dispose();

            //    return outFile;

        }
        private void ReadCloudFile(string relativefilepath, string docpath, ref List<string> attachmentFilePathList, ref List<string> files)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(relativefilepath);

                using (var response = request.GetResponse())
                {

                    using (Stream responseStream = response.GetResponseStream())
                    {

                        string saveTo = Server.MapPath(ConfigurationManager.AppSettings["DocumentTempPath"].ToString() + docpath.ToString());

                        // create a write stream
                        FileStream writeStream = new FileStream(saveTo, FileMode.Create, FileAccess.Write);
                        // write to the stream
                        ReadWriteStream(responseStream, writeStream);
                        attachmentFilePathList.Add(saveTo);
                        files.Add(saveTo);


                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }
       }

        public void StoreClaimInfoPDF(Int64 autoAssignmentId, CSS.Models.User loggedInUser, byte[] renderedBytes)
        {
            var assignment = css.AutoAssignments.Where(x => x.AutoAssignmentId == autoAssignmentId).FirstOrDefault();

            if (assignment != null)
            {
                #region Export to Pdf
                string originalFileName = "Claim_Info.pdf";
                int revisionCount = css.AutoDocuments.Where(x => (x.AutoAssignmentId == assignment.AutoAssignmentId) && (x.DocumentTypeId == 8) && (x.OriginalFileName == originalFileName)).ToList().Count;

                ObjectParameter outDocumentid = new ObjectParameter("Documentid", DbType.Int64);
                string fileName = string.Empty;
                if (MvcApplication.UseAzureCloudStorage == "1")
                {
                    #region Cloud Storage

                    string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                    string assignmentId = autoAssignmentId + "";
                    string documentTypeId = 8 + "";

                    fileName = "Invoice" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
                    string relativeFileName = assignmentId + "/" + documentTypeId + "/" + fileName;

                    string containerName = MvcApplication.AutoAssignDocsContainerName;
                    CloudStorageUtility.StoreFile(containerName, relativeFileName, renderedBytes);

                    #endregion
                }
                else
                {
                    #region Local File System

                    string baseFolder = System.Configuration.ConfigurationManager.AppSettings["AutoAssignmentDocsPath"].ToString();
                    string assignmentId = autoAssignmentId + "";
                    string documentTypeId = 8 + "";


                    fileName = "Invoice" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
                    if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId)))
                    {
                        Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId));
                    }
                    string path = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), fileName);

                    Utility.StoreBytesAsFile(renderedBytes, path);
                    #endregion

                }

                //Generation of a monthly T&E Invoice created an incorrect note which would read as Revised Invoice. Make use of a generalised note
                css.AutoDocumentsInsert(outDocumentid, autoAssignmentId, DateTime.Now, "Invoice", originalFileName, fileName, loggedInUser.UserId, 8);
                #endregion
            }
        }
    }
}
