﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ViewModels;
using System.Data;
using System.Data.Objects;
using BLL;

namespace CSS.Controllers
{
    public class ServiceLOBPricingController:Controller
    {
        //
        // GET: /CompanyLOBPricing/
        BLL.ChoiceSolutionsEntities css = new BLL.ChoiceSolutionsEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PricingSchedule(Int32 CompanyId, Int64 CompanyLOBPricingId = -1)
        {
            PricingScheduleViewModel viewmodel = new PricingScheduleViewModel(CompanyId);
            try
            {


                //if (CompanyLOBPricingId == -1)
                //{
                //    viewmodel = new PricingScheduleViewModel(CompanyId);
                //    viewmodel.pricingschedule = css.CompanyLineofBusinessGetList(CompanyId).ToList();
                //}
                //else
                //{
                //    viewmodel = new PricingScheduleViewModel(CompanyId, CompanyLOBPricingId);
                //    viewmodel.pricingschedule = css.CompanyLineofBusinessGetList(CompanyId).ToList();
                //}
            }
            catch (Exception ex)
            {

            }
            return View(viewmodel);


        }

        [HttpPost]
        public ActionResult PricingSchedule(Int32 CompanyId, Int64 companylobpricingid, Int32 lobid, double startrange, double endrange, double baseservicefee, double spservicefee, double rcvpercent)
        {
            int valueToReturn = 0;
            try
            {

                //if (companylobpricingid <= 0)
                //{

                //    ObjectParameter outCompanyLOBPricingId = new ObjectParameter("CompanyLOBPricingId", DbType.Int32);
                //    css.CompanyLOBPricingInsert(outCompanyLOBPricingId, CompanyId, lobid, startrange, endrange, baseservicefee, spservicefee, rcvpercent);
                //}
                //if (companylobpricingid > 0)
                //{
                //    css.CompanyLOBPricingUpdate(companylobpricingid, CompanyId, lobid, startrange, endrange, baseservicefee, spservicefee, rcvpercent);


                //}
                valueToReturn = 1;

            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }

            return Json(valueToReturn);


        }

        public ActionResult DeletePricingSchedule(Int32 CompanyId, Int64 CompanyLOBPricingId)
        {
            try
            {
                if (CompanyLOBPricingId > 0)
                {
                    CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                    ObjectParameter outResult = new ObjectParameter("Result", DbType.Int32);
                    css.CompanyLOBPricingDelete(CompanyLOBPricingId, loggedInUser.UserId, outResult);
                    int companylobpricingid = Convert.ToInt32(outResult.Value);
                    if (companylobpricingid <= 0)
                    {
                        TempData["Error"] = "Cannot delete this Pricing as Invoice exists for this Pricing.";
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return RedirectToAction("PricingSchedule", "CompanyLOBPricing", new { CompanyId = CompanyId });


        }


        public ActionResult isValidRange(double startrange, double endrange, Int32 companyid, Int32 lobid, Int64 companylobpricingid)
        {
            int valueToReturn = 0;
            bool isStartRangeValid = false;
            bool isEndRangeValid = false;
            try
            {

                if (startrange >= 0 && lobid > 0 && companyid > 0)
                {
                    ObjectParameter outStartStatus = new ObjectParameter("statusFlag", DbType.Binary);
                    css.IsStartRangeInUse(outStartStatus, startrange, lobid, companyid, companylobpricingid);

                    if ((Convert.ToBoolean(outStartStatus.Value) == false))
                        isStartRangeValid = true;


                }
                if (endrange >= 0 && lobid > 0 && companyid > 0)
                {
                    ObjectParameter outEndStatus = new ObjectParameter("statusFlag", DbType.Binary);
                    css.IsEndRangeInUse(outEndStatus, endrange, lobid, companyid, companylobpricingid);

                    if ((Convert.ToBoolean(outEndStatus.Value) == false))
                        isEndRangeValid = true;

                }
                if (endrange <= startrange)
                {
                    isStartRangeValid = false;
                }
                if (isStartRangeValid && isEndRangeValid)
                {
                    valueToReturn = 1;
                }
                else if (!isStartRangeValid && !isEndRangeValid)
                {
                    valueToReturn = 0;
                }
                else if (!isStartRangeValid)
                {

                    valueToReturn = 2;
                }
                else if (!isEndRangeValid)
                {
                    valueToReturn = 3;
                }


            }
            catch (Exception ex)
            {

            }

            return Json(valueToReturn);


        }

        public ActionResult LOBPricing(int ServiceTypeId, int LobID)
        {
            NuGenPricingScheduleViewModel viewmodel = new NuGenPricingScheduleViewModel(ServiceTypeId);
            if (ServiceTypeId != 0 && LobID != 0)
            {
                try
                {
                    viewmodel = new NuGenPricingScheduleViewModel(ServiceTypeId);
                    viewmodel.PopulateLOBlist(LobID);
                    viewmodel.LobID = LobID;
                    viewmodel.strLobDesc = viewmodel.LineofBusinessList.Where(a => a.Value == LobID.ToString()).Select(x => x.Text).SingleOrDefault();
                }
                catch (Exception ex)
                {

                }
            }
            return View(viewmodel);
        }

        [HttpPost]
        public ActionResult LOBPricing(NuGenPricingScheduleViewModel viewModel, FormCollection form)
        {
            viewModel.ValidateLobDetails();
            if (viewModel.ValidationSummary == "") 
            {
                try
                { 
                    ObjectParameter outServiceLOBPricingId = new ObjectParameter("ServiceLOBPricingId", DbType.Int64);
                    if (viewModel.LobPricingList != null)
                    {
                        foreach (ServiceLOBPricing pricing in viewModel.LobPricingList)
                        {
                            if (pricing.ServiceLOBPricingId != 0)
                            {
                                css.ServiceLOBPricingUpdate
                                    (
                                        pricing.ServiceLOBPricingId, 
                                        viewModel.ServceTypeId, 
                                        viewModel.LobID, 
                                        pricing.StartRange, 
                                        pricing.EndRange, 
                                        pricing.BaseServiceFee, 
                                        pricing.SPServiceFee, 
                                        pricing.RCVPercent, 
                                        pricing.SPRCVPercent, 
                                        pricing.SPXPercent,
                                        pricing.HourlyRate,
                                        pricing.FreeMile,
                                        pricing.DollarPerMile,
                                        pricing.TotalLossFee,
                                        pricing.IsTimeNExpance,
                                        pricing.SPServicePercent
                                    );
                            }
                            else
                            {
                                css.ServiceLOBPricingInsert
                                    (
                                        outServiceLOBPricingId, 
                                        viewModel.ServceTypeId, 
                                        viewModel.LobID, 
                                        pricing.StartRange, 
                                        pricing.EndRange, 
                                        pricing.BaseServiceFee, 
                                        pricing.SPServiceFee, 
                                        pricing.RCVPercent, 
                                        pricing.SPRCVPercent,
                                        pricing.SPXPercent,
                                        pricing.HourlyRate,
                                        pricing.FreeMile,
                                        pricing.DollarPerMile,
                                        pricing.TotalLossFee,
                                        pricing.IsTimeNExpance,
                                         pricing.SPServicePercent
                                     );
                            }
                        }
                    }
                    string strhdnDelLobprcIDlst = form["hdnDelLobprcIDlst"];
                    if (strhdnDelLobprcIDlst.Trim() != "")
                    {
                        strhdnDelLobprcIDlst = strhdnDelLobprcIDlst.Substring(0, strhdnDelLobprcIDlst.Length - 1);
                        string[] ServiceLOBPricingIds = strhdnDelLobprcIDlst.Split(',');
                        foreach (string strCLPID in ServiceLOBPricingIds)
                        {
                            if (strCLPID.Trim() != "")
                            {
                                //delete code here
                                try
                                {
                                    css.usp_DeleteServiceLOBPricing(Convert.ToInt32(strCLPID.Trim()));
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                        }
                    }
                    viewModel.ValidationSummary = " Save successful";
                }
                catch (Exception ex)
                {

                }

            }
            else
            {
                viewModel.Delstring = form["hdnDelLobprcIDlst"];
                viewModel.ValidationSummary = " Error Summary <br/> " + viewModel.ValidationSummary;
            }

            viewModel.FillServiceTypeDetails(viewModel.ServceTypeId);
            viewModel.populateLists(viewModel.ServceTypeId);
            viewModel.strLobDesc = viewModel.LineofBusinessList.Where(a => a.Value == viewModel.LobID.ToString()).Select(x => x.Text).SingleOrDefault();

            viewModel.PopulateLOBlist(viewModel.LobID);
            return View(viewModel);
        }


        public ActionResult ICompanyLobPricing(int? CompanyId, int? LobID)
        {
            ViewBag.CompanyId = CompanyId;
            ViewBag.LobID = LobID;

            return View();
        }



        [HttpPost]
        public ActionResult ICompanyLobPricing(FormCollection form)
        {
            return View();
        }
    }
}