﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BLL.ViewModels;
using System.Data;
using System.Data.SqlClient;
using System.Data.Linq.SqlClient;
using System.Collections;
using System.Data.Objects;
using System.Drawing;
using System.IO;
using System.Configuration;
using System.Threading.Tasks;
//using CSSCRM.BLL;
namespace CSS.Controllers
{
    public class InsuranceSetUpController : CustomController
    {
        //
        // GET: /InsuranceSetUp/
        #region Basic Functionality

        #region Objects & Variables

        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();


        #endregion


        #region Methods
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CompaniesList(Int64 compid = -1)
        {
            if (!String.IsNullOrEmpty(TempData["ErrorMsg"] != null ? TempData["ErrorMsg"] + "" : ""))
            {
                ViewBag.ErrorMsg = TempData["ErrorMsg"];
                TempData["ErrorMsg"] = null;
            }

            InsuranceTypeSearchViewModel viewmodel = new InsuranceTypeSearchViewModel();
            viewmodel.active = "1";
            GetCompanyList(viewmodel,null);
         

            //viewmodel.HeadCompanyID = Convert.ToInt64(CSS.AuthenticationUtility.GetUser().HeadCompanyId);
            return View(viewmodel);
        }
        private void GetCompanyList(InsuranceTypeSearchViewModel viewmodel,FormCollection form)
        {
            IEnumerable<Company> CompanyObj = css.Companies.Where(x => x.CompanyTypeId == 1 && x.IntegrationTypeId != 0).ToList();
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            BLL.User LoggedInUser = css.Users.Find(loggedInUser.UserId);
            if (LoggedInUser.UserTypeId == 3)
            {
                CompanyObj = CompanyObj.Where(x => x.CompanyId == LoggedInUser.HeadCompanyId).ToList();
            }
            if (viewmodel.active == "0")
            {
                CompanyObj = CompanyObj.Where(p => (p.IsActive??false) == false).ToList();
            }
            else if (viewmodel.active == "1")
            {
                CompanyObj = CompanyObj.Where(p => (p.IsActive??false) == true).ToList();
            }
            else
            {
                CompanyObj = CompanyObj.ToList();
            }
            if (viewmodel.CompanyName != null)
            {
                if (viewmodel.CompanyName.Trim().Length != 0)
                {
                    CompanyObj = CompanyObj.Where(c => c.CompanyName.ToLower().Contains(viewmodel.CompanyName.ToLower())).ToList();
                }
            }
            if (viewmodel.IntegrationTypeId != 0)
            {
                CompanyObj = CompanyObj.Where(c => c.IntegrationTypeId == viewmodel.IntegrationTypeId).ToList();
            }
          //Sorting
            if (viewmodel.sortBy == "City")
            {
                if (viewmodel.sortString == "DSC")
                {
                    CompanyObj = CompanyObj.OrderByDescending(x => x.City);

                }
                else if (viewmodel.sortString == "ASC")
                {
                    CompanyObj = CompanyObj.OrderBy(x => x.City);
                }
            }
            else if (viewmodel.sortBy == "State")
            {
                if (viewmodel.sortString == "DSC")
                {
                    CompanyObj = CompanyObj.OrderByDescending(x => x.State);

                }
                else if (viewmodel.sortString == "ASC")
                {
                    CompanyObj = CompanyObj.OrderBy(x => x.State);
                }
            }
            else if (viewmodel.sortBy == "RepsCount")
            {
                if (viewmodel.sortString == "DSC")
                {
                    CompanyObj = CompanyObj.OrderByDescending(x => x.Users1.Count());

                }
                else if (viewmodel.sortString == "ASC")
                {
                    CompanyObj = CompanyObj.OrderBy(x => x.Users1.Count());
                }
            }

            viewmodel.company = CompanyObj;
            viewmodel = setPager(viewmodel, form);
        }
        public InsuranceTypeSearchViewModel setPager(InsuranceTypeSearchViewModel viewModel, FormCollection form)
        {
            viewModel.Pager = new BLL.Models.Pager();
            if (form == null)
            {
                viewModel.Pager.Page = 1;
                viewModel.Pager.FirstPageNo = 1;
            }
            else
            {
                if ((form["hdnCurrentPage"]) != "")
                {
                    viewModel.Pager.Page = Convert.ToInt32(form["hdnCurrentPage"]);
                }
                else
                {
                    viewModel.Pager.Page = 1;
                }
                if (form["hdnstartPage"] != null)
                {
                    if (!String.IsNullOrEmpty(form["hdnstartPage"]))
                    {
                        viewModel.Pager.FirstPageNo = Convert.ToInt16(form["hdnstartPage"]);
                    }
                    else
                    {
                        viewModel.Pager.FirstPageNo = 1;
                    }
                }
            }
            viewModel.Pager.IsAjax = true;
            viewModel.Pager.FormName = "frmCompanySearch";
            viewModel.Pager.RecsPerPage = 20;
            viewModel.Pager.TotalCount = viewModel.Pager.TotalCount = viewModel.company.Count().ToString();
            viewModel.Pager.NoOfPages = (Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(viewModel.Pager.TotalCount) / Convert.ToInt32(viewModel.Pager.RecsPerPage)))).ToString();
            viewModel.company = viewModel.company.Skip((viewModel.Pager.Page - 1) * viewModel.Pager.RecsPerPage).Take(viewModel.Pager.RecsPerPage).ToList();
            return viewModel;
        }
        [HttpPost]
        public ActionResult CompaniesList(InsuranceTypeSearchViewModel viewmodel, FormCollection form)
        {

            //List<Company> Company = css.Companies.Where(x => x.CompanyTypeId == 1 && x.IntegrationTypeId != 0).ToList();
            //InsuranceTypeSearchViewModel viewModel = new InsuranceTypeSearchViewModel();
            //if (viewmodel.active == "0")
            //{
            //    Company = Company.Where(p => (p.IsActive == false) || (p.IsActive == null)).ToList();
            //}
            //else if (viewmodel.active == "1")
            //{
            //    Company = Company.Where(p => p.IsActive == true).ToList();
            //}
            //else
            //{
            //    Company = Company.ToList();
            //}
            //if (viewmodel.CompanyName != null)
            //{
            //    if (viewmodel.CompanyName.Trim().Length != 0)
            //    {
            //        Company = Company.Where(c => c.CompanyName.ToLower().Contains(viewmodel.CompanyName.ToLower())).ToList();
            //    }
            //}
            //if (viewmodel.IntegrationTypeId != 0)
            //{
            //    List<Company> company = new List<Company>();
            //    Company = Company.Where(c => c.IntegrationTypeId == viewmodel.IntegrationTypeId).ToList();
            //}
            //List<Companies> objcomp = new List<Companies>();
            //viewmodel.company = Company;
            GetCompanyList(viewmodel, form);
            return View(viewmodel);
        }

        public ActionResult submit(Int64 compid = -1)
        {
            InsuranceTypeSearchViewModel viewModel;
            if (compid == -1)
            {
                viewModel = new InsuranceTypeSearchViewModel();
                viewModel.RequirementsViewModel = new RequirementViewModel();

            }
            else
            {
                viewModel = new InsuranceTypeSearchViewModel(compid);
                viewModel.RequirementsViewModel = new RequirementViewModel();
                List<ClaimRequirement> lstClmRequirements = css.ClaimRequirements.Where(a => (a.HeadCompanyId == compid) && (a.AssignmentId == null) && (a.LOBId == null)).ToList();
                if (lstClmRequirements.Count > 0)
                {
                    ClaimRequirement Requirementdetails = lstClmRequirements[0];

                    viewModel.RequirementsViewModel.ClaimRequirementId = Convert.ToInt64(Requirementdetails.ClaimRequirementId);

                    if (Requirementdetails.SoftwareRequired.HasValue)
                    {
                        viewModel.RequirementsViewModel.SoftwareRequired = Convert.ToInt32(Requirementdetails.SoftwareRequired);
                    }

                    if (Requirementdetails.ContactWithin.HasValue)
                    {
                        viewModel.RequirementsViewModel.ContactWithin = Convert.ToInt32(Requirementdetails.ContactWithin);
                    }


                    if (Requirementdetails.InspectWithin.HasValue)
                    {
                        viewModel.RequirementsViewModel.InspectWithin = Convert.ToInt32(Requirementdetails.InspectWithin);
                    }


                    if (Requirementdetails.FirstReportDue.HasValue)
                    {
                        viewModel.RequirementsViewModel.FirstReportDue = Convert.ToInt32(Requirementdetails.FirstReportDue);
                    }


                    if (Requirementdetails.ReservesDue.HasValue)
                    {
                        viewModel.RequirementsViewModel.ReservesDue = Convert.ToInt32(Requirementdetails.ReservesDue);
                    }


                    if (Requirementdetails.SatusReports.HasValue)
                    {
                        viewModel.RequirementsViewModel.SatusReports = Convert.ToInt32(Requirementdetails.SatusReports);
                    }


                    if (Requirementdetails.FinalReportDue.HasValue)
                    {
                        viewModel.RequirementsViewModel.FinalReportDue = Convert.ToInt32(Requirementdetails.FinalReportDue);
                    }


                    if (Requirementdetails.BaseServiceCharges.HasValue)
                    {
                        viewModel.RequirementsViewModel.BaseServiceCharges = Convert.ToInt32(Requirementdetails.BaseServiceCharges);
                    }


                    if (Requirementdetails.InsuredToValueRequired.HasValue)
                    {
                        viewModel.RequirementsViewModel.InsuredToValueRequired = Convert.ToInt32(Requirementdetails.InsuredToValueRequired);
                    }


                    if (Requirementdetails.NeighborhoodCanvas.HasValue)
                    {
                        viewModel.RequirementsViewModel.NeighborhoodCanvas = Convert.ToInt32(Requirementdetails.NeighborhoodCanvas);
                    }


                    if (Requirementdetails.ITELRequired.HasValue)
                    {
                        viewModel.RequirementsViewModel.ITELRequired = Convert.ToInt32(Requirementdetails.ITELRequired);
                    }


                    if (Requirementdetails.MinimumCharges.HasValue)
                    {
                        viewModel.RequirementsViewModel.MinimumCharges = Convert.ToInt32(Requirementdetails.MinimumCharges);
                    }


                    if (Requirementdetails.RequiredDocuments != null)
                    {
                        viewModel.RequirementsViewModel.RequiredDocuments = Requirementdetails.RequiredDocuments;
                    }


                    if (Requirementdetails.EstimateContents.HasValue)
                    {
                        viewModel.RequirementsViewModel.EstimateContents = Convert.ToInt32(Requirementdetails.EstimateContents);
                    }


                    if (Requirementdetails.ObtainAgreedSorP.HasValue)
                    {
                        viewModel.RequirementsViewModel.ObtainAgreedSorP = Convert.ToInt32(Requirementdetails.ObtainAgreedSorP);
                    }


                    if (Requirementdetails.DiscussCoveragNScopewithClaimant.HasValue)
                    {
                        viewModel.RequirementsViewModel.DiscussCoveragNScopewithClaimant = Convert.ToInt32(Requirementdetails.DiscussCoveragNScopewithClaimant);
                    }


                    if (Requirementdetails.RoofSketchRequired.HasValue)
                    {
                        viewModel.RequirementsViewModel.RoofSketchRequired = Convert.ToInt32(Requirementdetails.RoofSketchRequired);
                    }


                    if (Requirementdetails.PhotoSGorPGRequired.HasValue)
                    {
                        viewModel.RequirementsViewModel.PhotoSGorPGRequired = Convert.ToInt32(Requirementdetails.PhotoSGorPGRequired);
                    }


                    if (Requirementdetails.DepreciacionType.HasValue)
                    {
                        viewModel.RequirementsViewModel.DepreciacionType = Convert.ToInt32(Requirementdetails.DepreciacionType);
                    }


                    if (Requirementdetails.MaxDepr.HasValue)
                    {
                        viewModel.RequirementsViewModel.MaxDepr = Convert.ToInt32(Requirementdetails.MaxDepr);
                    }


                    if (Requirementdetails.AppOfOveheadNProfit.HasValue)
                    {
                        viewModel.RequirementsViewModel.AppOfOveheadNProfit = Convert.ToInt32(Requirementdetails.AppOfOveheadNProfit);
                    }

                    if (Requirementdetails.Comments != null)
                    {
                        viewModel.RequirementsViewModel.Comments = Requirementdetails.Comments;
                    }
                }
            }
            return View(viewModel);
        }


        [HttpPost]
        public ActionResult submit(InsuranceTypeSearchViewModel company, RequirementViewModel requirementsViewModel, string submit, HttpPostedFileBase file1, int compid = -1)
        {
            InsuranceTypeSearchViewModel viewModel;
            int regions = 0;
            string regionid = "";
            try
            {
                if (isFormValid(company, compid))
                {
                    ObjectParameter outcompid = new ObjectParameter("CompanyId", DbType.Int64);
                    if (company.companyid == 0)
                    {
                        viewModel = new InsuranceTypeSearchViewModel();

                        ObjectParameter outregionid = new ObjectParameter("CompanyRegionId", DbType.Int64);
                        //Insert Company
                        if (viewModel != null)
                        {
                            css.CompaniesInsert(outcompid, 1, company.CompanyName, null, "", company.TaxID, company.Address, company.City, company.State, company.Zip, company.PhoneNumber, company.WebsiteURL, "", company.EnableFTPDocExport, "", company.FTPUserName, company.FTPPassword, "", "", "", "", "", 0, company.CarriedId, Convert.ToByte(company.IntegrationTypeId), company.IsActive, company.Email, company.Fax, company.UsesSharedDataset, company.XACTBusinessUnit,company.HasDocumentBridge,company.IsAutoConfigureInvoice);
                            compid = Convert.ToInt32(outcompid.Value);
                            if (company.regions != null)
                            {
                                foreach (string region in company.regions)
                                {
                                    regionid += region + " ,";
                                }
                                regions = css.upCompanyRegionsInsert(outregionid, Convert.ToInt32(outcompid.Value), Convert.ToByte(company.RegionId), regionid);
                            }
                            css.SaveChanges();
                            int taskCompanyId = compid;
                   //commented for IADA Temporarly
                            //Task.Factory.StartNew(() =>
                            //{
                            //    Utility.QBUpdateCustomer(taskCompanyId);
                            //}, TaskCreationOptions.LongRunning);

                            //if (System.Configuration.ConfigurationManager.AppSettings["CSSCRMIntegrationEnabled"].ToString() == "1")
                            //{
                            //    CSSCRM.Model.Account crmAccount = new CSSCRM.Model.Account();
                            //    crmAccount.Name = company.CompanyName;
                            //    crmAccount.AddressLine1 = company.Address;
                            //    crmAccount.City = company.City;
                            //    crmAccount.State = company.State;
                            //    crmAccount.Zip = company.Zip;
                            //    crmAccount.PrimaryPhone = company.PhoneNumber;
                            //    crmAccount.Fax = company.Fax;
                            //    crmAccount.Website = company.WebsiteURL;
                            //    crmAccount.RegionIdsCovered = regionid;

                            //    crmAccount.OwnerId = 1;
                            //    crmAccount.CreatedBy = 1;
                            //    crmAccount.ModifiedBy = 1;

                            //    Int64 crmAccountId = CSSCRM.BLL.AccountsService.CreateAndLinkToOpt(crmAccount, compid);
                            //    css.usp_CompanyCRMAccountIdUpdate(compid, crmAccountId);
                            //}
                        }
                    }
                    else
                    {
                        ObjectParameter outregionid = new ObjectParameter("CompanyRegionId", DbType.Int64);
                        //Update Company
                        if (company != null)
                        {
                            css.CompaniesUpdate(company.companyid, 1, company.CompanyName, null, "", company.TaxID, company.Address, company.City, company.State, company.Zip, company.PhoneNumber, company.WebsiteURL, "", company.EnableFTPDocExport, "", company.FTPUserName, company.FTPPassword, "", "", "", "", "", 0, company.CarriedId, Convert.ToByte(company.IntegrationTypeId), company.IsActive, company.Email, company.Fax, company.UsesSharedDataset, company.XACTBusinessUnit, company.HasDocumentBridge, company.IsAutoConfigureInvoice, company.IsQRPServiceActive,company.QBCustomerId);//INPSW-39
                            if (company.regions != null)
                            {
                                foreach (string region in company.regions)
                                {
                                    regionid += region + " ,";
                                }
                                regions = css.upCompanyRegionsInsert(outregionid, Convert.ToInt32(company.companyid), Convert.ToByte(company.RegionId), regionid);
                            }

                            css.SaveChanges();
                            int taskCompanyId = company.companyid;
                       //commented for IADA Temporarly
                            //Task.Factory.StartNew(() =>
                            //{
                            //    Utility.QBUpdateCustomer(taskCompanyId);
                            //}, TaskCreationOptions.LongRunning);

                            //Update Information On CRM
                            //if (System.Configuration.ConfigurationManager.AppSettings["CSSCRMIntegrationEnabled"].ToString() == "1")
                            //{
                            //    Int64? crmAccountId = css.Companies.Find(company.companyid).CRMAccountId;
                            //    if (crmAccountId.HasValue)
                            //    {
                            //        CSSCRM.Model.Account crmAccount = CSSCRM.BLL.AccountsService.GetByAccountId(crmAccountId.Value);
                            //        crmAccount.Name = company.CompanyName;
                            //        crmAccount.AddressLine1 = company.Address;
                            //        crmAccount.City = company.City;
                            //        crmAccount.State = company.State;
                            //        crmAccount.Zip = company.Zip;
                            //        crmAccount.PrimaryPhone = company.PhoneNumber;
                            //        crmAccount.Fax = company.Fax;
                            //        crmAccount.Website = company.WebsiteURL;
                            //        crmAccount.RegionIdsCovered = regionid;
                            //        crmAccount.ModifiedBy = 1;


                            //        CSSCRM.BLL.AccountsService.Update(crmAccount);
                            //    }
                            //}

                            compid = company.companyid;
                            outcompid.Value = company.companyid;
                        }

                    }

                    //Insert/Update Requirements
//commented for IADA Temporarly
                    //css.usp_insertClaimRequirements(requirementsViewModel.ClaimRequirementId, compid, null, null, requirementsViewModel.SoftwareRequired, requirementsViewModel.ContactWithin,
                    //requirementsViewModel.InspectWithin, requirementsViewModel.FirstReportDue, requirementsViewModel.ReservesDue, requirementsViewModel.SatusReports, requirementsViewModel.FinalReportDue,
                    //requirementsViewModel.BaseServiceCharges, requirementsViewModel.InsuredToValueRequired, requirementsViewModel.NeighborhoodCanvas, requirementsViewModel.ITELRequired, requirementsViewModel.MinimumCharges,
                    //requirementsViewModel.RequiredDocuments, requirementsViewModel.EstimateContents, requirementsViewModel.ObtainAgreedSorP, requirementsViewModel.DiscussCoveragNScopewithClaimant,
                    //requirementsViewModel.RoofSketchRequired, requirementsViewModel.PhotoSGorPGRequired, requirementsViewModel.DepreciacionType, requirementsViewModel.MaxDepr, requirementsViewModel.AppOfOveheadNProfit,
                    //requirementsViewModel.Comments);

                    if (file1 != null && file1.ContentLength > 0)
                    {
                        #region Cloud Storage
                        //// extract only the fielname
                        //string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(file1.FileName);
                        //string companid = outcompid.Value + "";
                        //string relativeFileName = companid + "/" + fileName;

                        //MemoryStream bufferStream = new System.IO.MemoryStream();
                        //file1.InputStream.CopyTo(bufferStream);
                        //byte[] buffer = bufferStream.ToArray();

                        //string containerName = ConfigurationManager.AppSettings["SPDocsContainerName"].ToString();
                        //CloudStorageUtility.StoreFile(containerName, relativeFileName, buffer);

                        //Image originalImage = Image.FromStream(file1.InputStream);
                        //Image thumbnail = CSS.Utility.generateThumbnail(originalImage);
                        //FileInfo file = new FileInfo(fileName);
                        //string thumbnailFileName = Utility.getThumbnailFileName(file.Name);
                        //string thumbnailRelativeFileName = companid + "/" + thumbnailFileName;

                        //MemoryStream thumbnailStream = new MemoryStream();
                        //thumbnail.Save(thumbnailStream, System.Drawing.Imaging.ImageFormat.Jpeg);

                        //byte[] bufferThumbnail = thumbnailStream.ToArray();
                        //CloudStorageUtility.StoreFile(containerName, thumbnailRelativeFileName, bufferThumbnail);


                        #endregion

                        #region Local File System
                        // extract only the fielname
                        string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(file1.FileName);
                        string baseFolder = ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
                        string companid = outcompid.Value + "";
                        //check whether the folder with user's userid exists
                        if (!Directory.Exists(Server.MapPath(baseFolder + "" + companid)))
                        {
                            Directory.CreateDirectory(Server.MapPath(baseFolder + "" + companid));
                        }
                        //check whether the destination folder depending on the type of document being uploaded exists
                        if (!Directory.Exists(Server.MapPath(baseFolder + "" + company.companyid)))
                        {
                            Directory.CreateDirectory(Server.MapPath(baseFolder + "" + companid));
                        }
                        // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
                        string path = Path.Combine(Server.MapPath(baseFolder + "" + companid), fileName);
                        file1.SaveAs(path);

                        //if (viewModel.DisplayMode == 1)
                        //{
                        //If an image is being uploaded
                        Image originalImage = Image.FromFile(path);
                        Image thumbnail = CSS.Utility.generateThumbnail(originalImage);
                        FileInfo file = new FileInfo(path);
                        string thumbnailFileName = Utility.getThumbnailFileName(file.Name);
                        string thumbnailPath = Path.Combine(Server.MapPath(baseFolder + "" + companid), thumbnailFileName);
                        thumbnail.Save(thumbnailPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                        //}
                        #endregion

                        css.CompaniesUpdate(Convert.ToInt32(outcompid.Value), 1, company.CompanyName, null, "", company.TaxID, company.Address, company.City, company.State, company.Zip, company.PhoneNumber, company.WebsiteURL, fileName, company.EnableFTPDocExport, "", company.FTPUserName, company.FTPPassword, "", "", "", "", "", 0, company.CarriedId, Convert.ToByte(company.IntegrationTypeId), company.IsActive, company.Email, company.Fax, company.UsesSharedDataset, company.XACTBusinessUnit, company.HasDocumentBridge, company.IsAutoConfigureInvoice, company.IsQRPServiceActive, company.QBCustomerId);//INPSW-39
                        css.SaveChanges();
                    }
                    return RedirectToAction("CompaniesList", "InsuranceSetUp", new { compid = compid });
                }
            }
            catch (Exception ex)
            {
                ViewBag.ExceptionErrorMsg = ex.Message + "<br/>" + ex.InnerException + "<br/>" + ex.StackTrace;

            }
            company.RequirementsViewModel = requirementsViewModel;
            return View(company);
        }
        public ActionResult DeleteCompany(bool? Status,Int32 compid = -1)
        {
            ObjectParameter outResult = new ObjectParameter("Result", DbType.Byte); ;
            if (compid != 0 && compid != null)
            {
                css.CompaniesDelete(compid, !(Status??false), outResult);
                if (Convert.ToByte(outResult.Value) == 0)
                {
                    TempData["ErrorMsg"] = "Cannot delete. Insurance company is in use by one or more claims.";
                }
                else if (Convert.ToByte(outResult.Value) == 2)
                {
                    TempData["ErrorMsg"] = "Cannot delete. LOB exists for this insurance company.";
                }
            }

            return RedirectToAction("CompaniesList", "InsuranceSetUp", new { compid = compid });
            //return View();
        }
        public bool isFormValid(InsuranceTypeSearchViewModel company, Int64 compid = -1)
        {
            bool valueToReturn = true;
            if (!ModelState.IsValid)
            {
                valueToReturn = false;
            }
            if (company.IntegrationTypeId == 0)
            {
                ModelState.AddModelError("Integration type", "Integration Type is required.");
                valueToReturn = false;
            }
            else
            {
                if (company.IntegrationTypeId == 1 && String.IsNullOrEmpty(company.CarriedId))
                {
                    //Xactnet
                    ModelState.AddModelError("Carrier Id", "Carrier Id is required.");
                    valueToReturn = false;
                }
            }
            if (company.EnableFTPDocExport)
            {
                if (String.IsNullOrEmpty(company.FTPUserName))
                {
                    ModelState.AddModelError("FTPUserName", "FTP User Name is required.");
                    valueToReturn = false;
                }
                if (!String.IsNullOrEmpty(company.FTPUserName) && !String.IsNullOrEmpty(company.FTPPassword))
                {
                    if (company.FTPPassword != (company.FTPConfirmPassword ?? ""))
                    {
                        ModelState.AddModelError("FTPPassword", "Incorrect FTP Confirm Password.");
                        valueToReturn = false;
                    }
                }
            }

            return valueToReturn;
        }
        //public ActionResult CompanyLogo(Int64 CompId, int documentTypeId, int maxDocsCount, int displayMode, bool isReadOnly)
        //{
        //    InsuranceCompanyLogoViewModel documentViewModel = new InsuranceCompanyLogoViewModel(CompId, displayMode, isReadOnly);
        //    //documentViewModel.logo = css.DocumentTypes.Find(documentTypeId).DocumentDesc;
        //    return View(documentViewModel);
        //}

        //[HttpPost]
        //public ActionResult UploadFile(InsuranceCompanyLogoViewModel viewModel, HttpPostedFileBase file1)
        //{

        //    if (file1 != null && file1.ContentLength > 0)
        //    {
        //        // extract only the fielname
        //        string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + Path.GetExtension(file1.FileName);
        //        string baseFolder = ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
        //        string compid = viewModel.companyid + "";
        //        //check whether the folder with user's userid exists
        //        if (!Directory.Exists(Server.MapPath(baseFolder + "" + compid)))
        //        {
        //            Directory.CreateDirectory(Server.MapPath(baseFolder + "" + compid));
        //        }
        //        //check whether the destination folder depending on the type of document being uploaded exists
        //        if (!Directory.Exists(Server.MapPath(baseFolder + "" + viewModel.companyid)))
        //        {
        //            Directory.CreateDirectory(Server.MapPath(baseFolder + "" + compid));
        //        }
        //        // store the file inside ~/UserUploads/ServiceProviderDocuments/<UserID>/<DocumentTypeDesc> folder
        //        string path = Path.Combine(Server.MapPath(baseFolder + "" + compid), fileName);
        //        file1.SaveAs(path);

        //        if (viewModel.DisplayMode == 1)
        //        {
        //            //If an image is being uploaded
        //            Image originalImage = Image.FromFile(path);
        //            Image thumbnail = CSS.Utility.generateThumbnail(originalImage);
        //            FileInfo file = new FileInfo(path);
        //            string thumbnailFileName = Utility.getThumbnailFileName(file.Name);
        //            string thumbnailPath = Path.Combine(Server.MapPath(baseFolder + "" + compid), thumbnailFileName);
        //            thumbnail.Save(thumbnailPath, System.Drawing.Imaging.ImageFormat.Jpeg);
        //        }
        //        ServiceProviderDocument document = new ServiceProviderDocument();
        //        document.UserId = viewModel.companyid;
        //        document.Path = fileName;
        //        document.UploadedDate = DateTime.Now;
        //        //css.ServiceProviderDocuments.Add(document);
        //        //css.SaveChanges();
        //    }

        //    return RedirectToAction("DocumentsList", new { userId = viewModel.companyid, maxDocsCount = viewModel.MaxDocsCount, displayMode = viewModel.DisplayMode, isReadOnly = viewModel.IsReadOnly });
        //}

        //public ActionResult DeleteFile(Int64 SPDId, Int64 userId, int documentTypeId, int maxDocsCount, int displayMode, bool isReadOnly)
        //{
        //    ServiceProviderDocument document = css.ServiceProviderDocuments.Where(x => x.SPDId == SPDId).First();


        //    //delete physical file 

        //    string baseFolder = ConfigurationManager.AppSettings["ServiceProviderDocsPath"].ToString();
        //    string documentTypeDesc = Url.Encode(css.DocumentTypes.Find(documentTypeId).DocumentDesc.Replace(" ", "_"));
        //    string fileName = document.Path;
        //    string filePath = Path.Combine(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc), fileName);
        //    if (System.IO.File.Exists(filePath))
        //    {
        //        try
        //        {
        //            System.IO.File.Delete(filePath);
        //        }
        //        catch (IOException ex)
        //        {
        //        }
        //    }

        //    if (displayMode == 1)
        //    {
        //        string thumbnailFileName = Utility.getThumbnailFileName(fileName);
        //        string thumbnailPath = document.Path.Replace(fileName, thumbnailFileName);
        //        string thumbnailFilePath = Path.Combine(Server.MapPath(baseFolder + "" + userId + "/" + documentTypeDesc), thumbnailFileName);
        //        if (System.IO.File.Exists(thumbnailPath))
        //        {
        //            try
        //            {
        //                System.IO.File.Delete(thumbnailPath);
        //            }
        //            catch (IOException ex)
        //            {
        //            }
        //        }
        //    }
        //    //delete database record

        //    css.ServiceProviderDocuments.Remove(document);
        //    css.SaveChanges();
        //    return RedirectToAction("DocumentsList", new { userId = userId, documentTypeId = documentTypeId, maxDocsCount = maxDocsCount, displayMode = displayMode, isReadOnly = isReadOnly });
        //}

        public ActionResult ClientPOCList(int companyId=0)
        {
            ClientPOCListViewModel viewModel = new ClientPOCListViewModel();
           
            viewModel.ClientPOCList = css.usp_ClientPOCGetList(companyId).ToList();

            if (Request.IsAjaxRequest())
            {
                return Json(RenderPartialViewToString("_CompanyCPOCList", viewModel), JsonRequestBehavior.AllowGet);
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult ClientPOCList(ClientPOCListViewModel viewModel)
        {
            //ClientPOCListViewModel viewModel = new ClientPOCListViewModel();
            // viewModel.CompanyId = companyId;
            // viewModel.CompanyName = "";
            viewModel.ClientPOCList = css.usp_ClientPOCGetList(viewModel.CompanyId).ToList();

            if (Request.IsAjaxRequest())
            {
                return Json(RenderPartialViewToString("_CompanyCPOCList", viewModel), JsonRequestBehavior.AllowGet);
            }

            return View(viewModel);
        }

        public ActionResult ClientPOCEdit(Int64 userId = 0)
        {
            ClientPOCDetailsEditViewModel viewModel = new ClientPOCDetailsEditViewModel();
            if (userId > 0)
            {
                viewModel.UserId = userId;
                viewModel.CPOC = css.Users.Find(userId);
                viewModel.CPOC.Password = String.Empty;//Do not display Password. When a password is entered for an existing user, it means the existing password needs to be changed.
            }
            else
            {
                viewModel.CPOC = new User();
                viewModel.UserId = 0;
                viewModel.CPOC.UseHeadCompanyContactDetails = true;
            }
            return Json(RenderPartialViewToString("_ClientPOCDetailsEdit", viewModel), JsonRequestBehavior.AllowGet);
        }
        private bool isCSSPOCFormValid(ClientPOCDetailsEditViewModel viewModel)
        {
            bool valueToReturn = true;
            ModelState.Clear();

            if (String.IsNullOrEmpty(viewModel.CPOC.FirstName))
            {
                ModelState.AddModelError("CPOC.FirstName", "First Name is required.");
                valueToReturn = false;
            }

            if (String.IsNullOrEmpty(viewModel.CPOC.LastName))
            {
                ModelState.AddModelError("CPOC.LastName", "Last Name is required.");
                valueToReturn = false;
            }
            if (String.IsNullOrEmpty(viewModel.CPOC.Email))
            {
                ModelState.AddModelError("CPOC.Email", "Email is required.");
                valueToReturn = false;
            }
            if (viewModel.UserId == 0)
            {
                if (String.IsNullOrEmpty(viewModel.CPOC.UserName))
                {
                    ModelState.AddModelError("CPOC.UserName", "User Name is required.");
                    valueToReturn = false;
                }
                else
                {
                    if (css.Users.Where(x => x.UserName == viewModel.CPOC.UserName.Trim()).ToList().Count != 0)
                    {
                        ModelState.AddModelError("CPOC.UserName", "User Name already exists.");
                        valueToReturn = false;
                    }
                }
                if (String.IsNullOrEmpty(viewModel.CPOC.Password))
                {
                    ModelState.AddModelError("CPOC.Password", "Passwords do not match.");
                    valueToReturn = false;
                }
                if (css.Users.Where(x => x.Email == viewModel.CPOC.Email.Trim()).ToList().Count != 0)
                {
                    ModelState.AddModelError("CPOC.Email", "Email already exists.");
                    valueToReturn = false;
                }
            }
            if (!String.IsNullOrEmpty(viewModel.CPOC.Password))
            {
                if (viewModel.CPOC.Password != viewModel.ConfirmPassword)
                {
                    ModelState.AddModelError("CPOC.Password", "Passwords do not match.");
                    valueToReturn = false;
                }
            }
            if (!String.IsNullOrEmpty(viewModel.CPOC.ExternalMappingIdentifier))
            {
                User user = null;
                user = css.Users.Where(x => x.HeadCompanyId == viewModel.CPOC.HeadCompanyId && x.ExternalMappingIdentifier == viewModel.CPOC.ExternalMappingIdentifier).SingleOrDefault();

                if (user != null)
                {
                    ModelState.AddModelError("CPOC.ExternalMappingIdentifier", "ExternalIdentifier already exists");
                    valueToReturn = false;
                }
            }
            return valueToReturn;
        }
        [HttpPost]
        public ActionResult ClientPOCEdit(ClientPOCDetailsEditViewModel viewModel)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            string code = "0";
            string data = "";
            try
            {
                if (isCSSPOCFormValid(viewModel))
                {
                    string Password = Cypher.EncryptString(viewModel.CPOC.Password);
                    if (viewModel.UserId == 0)
                    {
                     
                        ObjectParameter outUserId = new ObjectParameter("UserId", DbType.Int64);
                        css.usp_ClientPOCInsert(outUserId, loggedInUser.UserId, viewModel.CPOC.HeadCompanyId, viewModel.CPOC.FirstName, viewModel.CPOC.MiddleInitial, viewModel.CPOC.LastName, viewModel.CPOC.Email, viewModel.CPOC.UserName, Password, !viewModel.CPOC.UseHeadCompanyContactDetails, viewModel.CPOC.MobilePhone, viewModel.CPOC.WorkPhone, viewModel.CPOC.AddressPO, viewModel.CPOC.AddressLine1, viewModel.CPOC.StreetAddress, viewModel.CPOC.Zip, viewModel.CPOC.State, viewModel.CPOC.City, viewModel.CPOC.ExternalMappingIdentifier);
                        data = Convert.ToInt64(outUserId.Value) + "";
                    }
                    else
                    {
                        css.usp_ClientPOCUpdate(viewModel.UserId, loggedInUser.UserId, viewModel.CPOC.FirstName, viewModel.CPOC.MiddleInitial, viewModel.CPOC.LastName, viewModel.CPOC.Email, Password, !viewModel.CPOC.UseHeadCompanyContactDetails, viewModel.CPOC.MobilePhone, viewModel.CPOC.WorkPhone, viewModel.CPOC.AddressPO, viewModel.CPOC.AddressLine1, viewModel.CPOC.StreetAddress, viewModel.CPOC.Zip, viewModel.CPOC.State, viewModel.CPOC.City, viewModel.CPOC.ExternalMappingIdentifier);
                        data = viewModel.UserId + "";
                    }
                    code = "1";

                }
                else
                {
                    code = "-1";
                    data = RenderPartialViewToString("_ClientPOCDetailsEdit", viewModel);
                }
            }
            catch (Exception ex)
            {
                code = "-3";
                code = ex.Message + (ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return Json(new { Code = code, Data = data }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult ClientPOCDelete(Int64 userId)
        {
            string returnCode = "0";
            string returnData = String.Empty;
            try
            {
                if (css.Claims.Where(x => x.ClientPointOfContactUserId == userId).ToList().Count == 0)
                {
                    css.usp_UsersDelete(userId);
                    returnCode = "1";
                }
                else
                {
                    returnCode = "-2";
                    returnData = "Cannot delete as this Client POC is assigned to one or more claims.";
                }
            }
            catch (Exception ex)
            {
                returnCode = "-1";
                returnData = ex.Message + (ex.InnerException != null ? " " + ex.InnerException.Message : "");
            }
            return Json(new { Code = returnCode, Data = returnData }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteLogo(Int32 CompanyId)
        {

            try
            {

                css.UpdateCompanyLogo(CompanyId);


                //Company cmp = css.Companies.Find(CompanyId);
                //cmp.Logo = "fff";
                ////(from c in css.Companies
                //// where c.CompanyId==CompanyId
                //// select c).First();
                //css.SaveChanges();

            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("submit", new { compid = CompanyId });
        }
        #endregion
        #endregion
        [AllowAnonymous]
        public ActionResult CreateCompany()
        {
            InsuranceTypeSearchViewModel viewmodel = new InsuranceTypeSearchViewModel();
            return View("CreateInsuranceCompany",viewmodel);
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult CreateCompany(FormCollection Form, InsuranceTypeSearchViewModel viewmodel)
        {
            try
            {
                if (validateCompanyForm(Form))
                {
                    string FirstName = Form["FirstName"];
                    string LastName = Form["LastName"];
                    string UserName = Form["UserName"];
                    string Password = Form["Password"];
                    string CompanyName = Form["CompanyName"];
                    string Title = Form["Title"];
                    string PhoneNo = Form["PhoneNumber"];
                    string Address = Form["Address"];
                    string City = Form["City"];
                    string State = Form["State"];
                    string Zip = Form["Zip"];
                    string Fax = Form["Fax"];
                    string WebSiteUrl = Form["WebSiteUrl"];

                    //ObjectParameter outCompanyID = new ObjectParameter("CompanyRegionId", DbType.Int64);
                    Password = Cypher.EncryptString(Password);

                    css.InsertCompanyRegistration(FirstName, LastName, UserName, CompanyName, WebSiteUrl, Password, Fax, Address, City, State, Zip, PhoneNo, Title).ToString();
                }
                else {
               
                    return View("CreateInsuranceCompany", viewmodel);
                }
           }
            catch(Exception Ex)
            {

            }

            return RedirectToAction("Login", "Account");
        }
        public bool validateCompanyForm(FormCollection Form)
        {
            bool valid = true;
           
            if (string.IsNullOrEmpty(Form["Password"]))
            {
                valid = false;
                ModelState.AddModelError("", "Password is Required!");
            }
            if (string.IsNullOrEmpty(Form["UserName"]))
            {
                valid = false;
                ModelState.AddModelError("", "UserName is Required!");
            }
            return valid;
        }
        public ActionResult ClaimRepList (Int64? CompanyID)
        {
            List<ClaimRepList_Result> ClaimRepList = css.ClaimRepList(CompanyID).ToList();
             if (Request.IsAjaxRequest())
            {
                return Json(RenderPartialViewToString("_ClaimRepList",ClaimRepList), JsonRequestBehavior.AllowGet);
            }
             return View(); 
        }


        public ActionResult CreateClaimRepr(InsuranceTypeSearchViewModel viewmodel, FormCollection Form)
        {
            int UserTypeId = 3;

            int ID = Convert.ToInt16(Form["ClaimRepID"]);
            string FirstName = Form["FirstName"];
            string MiddleName = Form["MiddleName"];
            string LastName = Form["LastName"];
            string UserName = Form["UserName"];
            string Password = Form["Password"];            
            string MobileNo = Form["MobileNumber"];
            string Email = Form["EmailAddress"];
            string ClaimsRepresentativeManger = Form["CRManager"];

            if (ClaimsRepresentativeManger == "true")
            {
                UserTypeId = 12;
            }
            Password = Cypher.EncryptString(Password).ToString();

            if (ID == 1)
            {
                Int64 CompanyID = Convert.ToInt64(Form["ClaimRepCompanyID"]);
                #region ClaimRep Insert

                css.ClaimReprInsert(UserName, Password, FirstName, MiddleName, LastName, Email, MobileNo, CompanyID, UserTypeId);

                #endregion 
            }
            else
            {
                Int64 UserID = Convert.ToInt64(Form["UserID"]); 
                #region ClaimRep Update

                css.ClaimRepUpdate(UserID, UserName, FirstName, MiddleName, LastName, MobileNo, Email, Password, UserTypeId);

                #endregion 
            }            
            return View();
        }

        public ActionResult RejectClaimRep(Int64 UserID)
        {
            if(UserID != null || UserID != 0)
            {
                BLL.AutoAssignment autoassignment = css.AutoAssignments.Find(UserID);
                if(autoassignment == null)
                {
                    ObjectParameter outIsReject = new ObjectParameter("IsReject", DbType.Int64);
                    css.RejectClaimRep(outIsReject, UserID);
                    return Json(0, JsonRequestBehavior.AllowGet);                    
                }
                else
                {
                    if (autoassignment.RepresentativeID == null)
                    {
                        ObjectParameter outIsReject = new ObjectParameter("IsReject", DbType.Int64);
                        css.RejectClaimRep(outIsReject, UserID);
                        return Json(0, JsonRequestBehavior.AllowGet);
                    }else
                    {
                        return Json(1, JsonRequestBehavior.AllowGet);
                    }                    
                }            
            }
            return View();
        }
    }
}

