﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BLL.Models;
using BLL.ViewModels;
using System.Data;
using System.Data.Objects;
using System.Threading.Tasks;

namespace CSS.Controllers
{
    public class ReportingController : Controller
    {
        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

        private void populateESReportDDLs(ref ESReportViewModel viewModel)
        {
            //Companies List
            viewModel.CompanyList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            viewModel.SearchFilterList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                viewModel.CompanyList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }

            //States List
            viewModel.StateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                viewModel.StateList.Add(new SelectListItem { Text = state.Name, Value = state.StateProvinceCode });
            }

            //Cat. Codes List 
            viewModel.CatCodeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (CatCode catcode in css.CatCodes.ToList())
            {
                viewModel.CatCodeList.Add(new SelectListItem { Text = catcode.CatCode1, Value = catcode.CatCode1 });
            }

            //Loss Types List
            viewModel.LossTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (LossType losstype in css.LossTypes.ToList())
            {
                //LossTypesList.Add(losstype.LossTypeId, losstype.LossType1);
                viewModel.LossTypeList.Add(new SelectListItem { Text = losstype.LossType1, Value = Convert.ToString(losstype.LossTypeId) });
            }
            viewModel.SearchFilterList.Add(new SelectListItem { Text = "Client", Value = "1", Selected = true });
            viewModel.SearchFilterList.Add(new SelectListItem { Text = "SPX(Examiner)", Value = "2" });
            viewModel.SearchFilterList.Add(new SelectListItem { Text = "ClientPOC", Value = "3" });
            viewModel.SearchFilterList.Add(new SelectListItem { Text = "CSSPOC", Value = "4" });
        }
        private void populateKPIReportDDLs(ref KPIReportViewModel viewModel)
        {
            //Pivot Category List
            viewModel.PivotCategoryList.Add(new SelectListItem { Text = "CSS POC", Value = "1" });
            viewModel.PivotCategoryList.Add(new SelectListItem { Text = "SPX", Value = "2" });
            viewModel.PivotCategoryList.Add(new SelectListItem { Text = "SP", Value = "3" });
            viewModel.PivotCategoryList.Add(new SelectListItem { Text = "Cat Code", Value = "4" });
            viewModel.PivotCategoryList.Add(new SelectListItem { Text = "Loss State", Value = "5" });
            viewModel.PivotCategoryList.Add(new SelectListItem { Text = "Loss Type", Value = "6" });
            viewModel.PivotCategoryList.Add(new SelectListItem { Text = "Received Date", Value = "7" });
            viewModel.PivotCategoryList.Add(new SelectListItem { Text = "Loss Date", Value = "8" });
            viewModel.PivotCategoryList.Add(new SelectListItem { Text = "Client", Value = "9" });


            //Companies List
            viewModel.CompanyList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                viewModel.CompanyList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }

            //States List
            viewModel.StateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                viewModel.StateList.Add(new SelectListItem { Text = state.Name, Value = state.StateProvinceCode });
            }

            //Cat. Codes List
            viewModel.CatCodeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (CatCode catcode in css.CatCodes.ToList())
            {
                viewModel.CatCodeList.Add(new SelectListItem { Text = catcode.CatCode1, Value = catcode.CatCode1 });
            }

            //Loss Types List
            viewModel.LossTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (LossType losstype in css.LossTypes.ToList())
            {
                //LossTypesList.Add(losstype.LossTypeId, losstype.LossType1);
                viewModel.LossTypeList.Add(new SelectListItem { Text = losstype.LossType1, Value = Convert.ToString(losstype.LossTypeId) });
            }

            //SPX
            viewModel.QAAgentList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            List<GetCSSQAAgentList_Result> cssQAAgentList = css.GetCSSQAAgentList().ToList();
            foreach (var cssQAAgent in cssQAAgentList)
            {
                viewModel.QAAgentList.Add(new SelectListItem { Text = cssQAAgent.UserFullName, Value = Convert.ToString(cssQAAgent.UserId) });
            }
        }
        private void populateProdReportDDLs(ref ProdReportViewModel viewModel)
        {
            //Companies List
            viewModel.CompanyList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            viewModel.SearchFilterList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                viewModel.CompanyList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }

            //Dataset
            viewModel.DataSetList.Add(new SelectListItem { Text = "Claim Count", Value = "0" });
            viewModel.DataSetList.Add(new SelectListItem { Text = "Revenue", Value = "1" });
            viewModel.DataSetList.Add(new SelectListItem { Text = "Both", Value = "2" });
            viewModel.SearchFilterList.Add(new SelectListItem { Text = "Client", Value = "1", Selected = true });
            viewModel.SearchFilterList.Add(new SelectListItem { Text = "SPX(Examiner)", Value = "2" });
            viewModel.SearchFilterList.Add(new SelectListItem { Text = "ClientPOC", Value = "3" });
            viewModel.SearchFilterList.Add(new SelectListItem { Text = "CSSPOC", Value = "4" });
        }
        private void populateTotalCycleByRCVReportDDLs(ref TotalCycleByRCVReportViewModel viewModel)
        {
            //Companies List
            viewModel.CompanyList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                viewModel.CompanyList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }

            //States List
            viewModel.StateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                viewModel.StateList.Add(new SelectListItem { Text = state.Name, Value = state.StateProvinceCode });
            }

            //Cat. Codes List
            viewModel.CatCodeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (CatCode catcode in css.CatCodes.ToList())
            {
                viewModel.CatCodeList.Add(new SelectListItem { Text = catcode.CatCode1, Value = catcode.CatCode1 });
            }

            //Loss Types List
            viewModel.LossTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (LossType losstype in css.LossTypes.ToList())
            {
                viewModel.LossTypeList.Add(new SelectListItem { Text = losstype.LossType1, Value = Convert.ToString(losstype.LossTypeId) });
            }

            //CSS POC
            viewModel.CSSPOCList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            List<CSSPointOfContactList_Result> cssPOCList = css.CSSPointOfContactList().ToList();
            foreach (var poc in cssPOCList)
            {
                viewModel.CSSPOCList.Add(new SelectListItem { Text = poc.UserFullName, Value = Convert.ToString(poc.UserId) });
            }

            //SPX
            viewModel.QAAgentList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            List<GetCSSQAAgentList_Result> cssQAAgentList = css.GetCSSQAAgentList().ToList();
            foreach (var cssQAAgent in cssQAAgentList)
            {
                viewModel.QAAgentList.Add(new SelectListItem { Text = cssQAAgent.UserFullName, Value = Convert.ToString(cssQAAgent.UserId) });
            }

        }

        [Authorize]
        public ActionResult ProcessedPayrollReport()
        {
            ProcessedPayrollReportViewModel viewModel = new ProcessedPayrollReportViewModel();

            viewModel.EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
            viewModel.StartDate = viewModel.EndDate.Value.AddMonths(-6);

            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            Int64? spId = null;
            if (loggedInUser.UserTypeId == 1)
            {
                spId = loggedInUser.UserId;
            }
            ObjectResult<DateTime?> payrollDateList = css.usp_ProcessedPayrollDateHistoryGetList(spId, viewModel.StartDate, viewModel.EndDate);
            List<string> model = new List<string>();
            foreach (var payrollDate in payrollDateList)
            {
                model.Add(payrollDate.Value.ToString("MM/dd/yyyy hh:mm tt"));
            }
            viewModel.PayrollDates = model;
            return View(viewModel);
        }
        [Authorize]
        [HttpPost]
        public ActionResult ProcessedPayrollReport(ProcessedPayrollReportViewModel viewModel)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            Int64? spId = null;
            if (loggedInUser.UserTypeId == 1)
            {
                spId = loggedInUser.UserId;
            }

            ObjectResult<DateTime?> payrollDateList = css.usp_ProcessedPayrollDateHistoryGetList(spId, viewModel.StartDate, viewModel.EndDate);
            List<string> model = new List<string>();
            foreach (var payrollDate in payrollDateList)
            {
                model.Add(payrollDate.Value.ToString("MM/dd/yyyy hh:mm tt"));
            }
            viewModel.PayrollDates = model;
            return View(viewModel);
        }
        [Authorize]
        public ActionResult ClaimsReport()
        {
            return View();
        }
        [Authorize]
        public ActionResult ExpenseCommissionReport()
        {
            return View();
        }
        [Authorize]
        public ActionResult RejectedClaimsReport()
        {

            string urlParameters = "";
            RejectedClaimReportViewModel viewModel = new RejectedClaimReportViewModel();
            viewModel.ReportURL = "~/Reports/RejectedClaimsReport.aspx";
            DateTime today = DateTime.Today;

            viewModel.StartDate=new DateTime(today.Year,today.Month,1);
            viewModel.EndDate = new DateTime(today.Year, today.Month, 1).AddMonths(1).AddDays(-1);
            urlParameters = "BeginDate=" + viewModel.StartDate.Value.ToString("MM-dd-yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.Value.ToString("MM-dd-yyyy");
            viewModel.ReportURL += "?" + urlParameters;
            return View(viewModel);
        }
        [Authorize]
        [HttpPost]
        public ActionResult RejectedClaimsReport(RejectedClaimReportViewModel viewModel)
        {

            string urlParameters = "";
           // RejectedClaimReportViewModel viewModel = new RejectedClaimReportViewModel();
            viewModel.ReportURL = "~/Reports/RejectedClaimsReport.aspx";
            //DateTime today = DateTime.Today;

            //viewModel.StartDate = new DateTime(today.Year, today.Month, 1);
            //viewModel.EndDate = new DateTime(today.Year, today.Month, 1).AddMonths(1).AddDays(-1);
            urlParameters = "BeginDate=" + viewModel.StartDate.Value.ToString("MM-dd-yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.Value.ToString("MM-dd-yyyy");
            viewModel.ReportURL += "?" + urlParameters;
            return View(viewModel);
        }
        public ActionResult SPPaymentSummary()
        {
            List<usp_SPPaymentSummaryGetList_Result> model = css.usp_SPPaymentSummaryGetList().ToList();
            return View(model);
        }

        [Authorize]
        public ActionResult ARReport()
        {
            string urlParameters = "";
            string reportURL = "~/Reports/ARReport.aspx";
            ARReportViewModel viewModel = new ARReportViewModel();
            viewModel.ReportURL = reportURL;
            viewModel.IncludeClientPOC = true;
            viewModel.PivotDate = css.usp_GetLocalDateTime().First().Value.Date;

            viewModel.CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                viewModel.CompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }

            //viewModel.StartDate = css.usp_GetLocalDateTime().First().Value.AddMonths(-1).Date;
            //viewModel.EndDate = css.usp_GetLocalDateTime().First().Value.Date;

            urlParameters += "HeadCompanyId=" + viewModel.HeadCompanyId;
            urlParameters += "&PivotDate=" + viewModel.PivotDate.ToString("MM-dd-yyyy");
            //urlParameters += "&StartDate=" + viewModel.StartDate.ToString("MM-dd-yyyy");
            //urlParameters += "&EndDate=" + viewModel.EndDate.ToString("MM-dd-yyyy");
            viewModel.ReportURL = reportURL + "?" + urlParameters;
            return View(viewModel);
        }
        [HttpPost]
        [Authorize]
        public ActionResult ARReport(ARReportViewModel viewModel)
        {
            viewModel.CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                viewModel.CompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }



            string reportURL = "~/Reports/ARReport.aspx";
            string urlParameters = "";

            if (!viewModel.IncludeClientPOC)
            {
                reportURL = "~/Reports/ARReport_NoCPOC.aspx";
            }
            if (viewModel.PivotDate <= DateTime.MinValue)
            {
                viewModel.PivotDate = css.usp_GetLocalDateTime().First().Value.Date;
            }
            urlParameters += "HeadCompanyId=" + viewModel.HeadCompanyId;
            urlParameters += "&PivotDate=" + viewModel.PivotDate.ToString("MM-dd-yyyy");
            //urlParameters += "&StartDate=" + viewModel.StartDate.ToString("MM/dd/yyyy");
            //urlParameters += "&EndDate=" + viewModel.EndDate.ToString("MM/dd/yyyy");


            viewModel.ReportURL = reportURL + "?" + urlParameters;

            return View(viewModel);
        }

        [Authorize]
        public ActionResult ARReportSimple()
        {
            string urlParameters = "";
            string reportURL = "~/Reports/ARReportSimple.aspx";
            ARReportViewModel viewModel = new ARReportViewModel();
            viewModel.ReportURL = reportURL;
            viewModel.IncludeClientPOC = true;
            viewModel.PivotDate = css.usp_GetLocalDateTime().First().Value.Date;

            viewModel.CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                viewModel.CompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }

            //viewModel.StartDate = css.usp_GetLocalDateTime().First().Value.AddMonths(-1).Date;
            //viewModel.EndDate = css.usp_GetLocalDateTime().First().Value.Date;

            urlParameters += "HeadCompanyId=" + viewModel.HeadCompanyId;
            urlParameters += "&PivotDate=" + viewModel.PivotDate.ToString("MM-dd-yyyy");
            //urlParameters += "&StartDate=" + viewModel.StartDate.ToString("MM-dd-yyyy");
            //urlParameters += "&EndDate=" + viewModel.EndDate.ToString("MM-dd-yyyy");
            viewModel.ReportURL = reportURL + "?" + urlParameters;
            return View(viewModel);
        }
        [HttpPost]
        [Authorize]
        public ActionResult ARReportSimple(ARReportViewModel viewModel)
        {
            viewModel.CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                viewModel.CompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }



            string reportURL = "~/Reports/ARReportSimple.aspx";
            string urlParameters = "";

            //if (!viewModel.IncludeClientPOC)
            //{
            //    reportURL = "~/Reports/ARReport_NoCPOC.aspx";
            //}
            if (viewModel.PivotDate <= DateTime.MinValue)
            {
                viewModel.PivotDate = css.usp_GetLocalDateTime().First().Value.Date;
            }
            urlParameters += "HeadCompanyId=" + viewModel.HeadCompanyId;
            urlParameters += "&PivotDate=" + viewModel.PivotDate.ToString("MM-dd-yyyy");
            //urlParameters += "&StartDate=" + viewModel.StartDate.ToString("MM/dd/yyyy");
            //urlParameters += "&EndDate=" + viewModel.EndDate.ToString("MM/dd/yyyy");


            viewModel.ReportURL = reportURL + "?" + urlParameters;

            return View(viewModel);
        }
        [Authorize]
        public ActionResult ESReport()
        {
            string urlParameters = "";
            ESReportViewModel viewModel = new ESReportViewModel();
            ViewBag.SearchList = new List<SelectListItem> {
            new SelectListItem {Text="--Select--",Value="0",Selected=true}
            };
            viewModel.ReportURL = "~/Reports/ESReport.aspx";
            populateESReportDDLs(ref viewModel);
            viewModel.SearchBy = 1;
            viewModel.SearchfilterValue = 0;
            urlParameters += "&SearchParameterId=" + viewModel.SearchBy;
            urlParameters += "&SearchParameterValue=" + viewModel.SearchFilter;

            viewModel.ReportURL += "?" + urlParameters;
            return View(viewModel);
        }
        [HttpPost]
        [Authorize]
        public ActionResult ESReport(ESReportViewModel viewModel)
        {
            viewModel.ReportURL = "~/Reports/ESReport.aspx";
            populateESReportDDLs(ref viewModel);
            string urlParameters = "";
            viewModel.SearchfilterValue = viewModel.SearchFilter;
            ViewBag.SearchList = new List<SelectListItem> {
            new SelectListItem {Text="--Select--",Value="0",Selected=true}
            };

            if (viewModel.StartDate.HasValue && viewModel.EndDate.HasValue)
            {
                urlParameters += "StartDate=" + viewModel.StartDate.Value.ToString("MM-dd-yyyy") + "&EndDate=" + viewModel.EndDate.Value.ToString("MM-dd-yyyy");
            }
            else
            {
                viewModel.StartDate = null;
                viewModel.EndDate = null;
            }
            if (!String.IsNullOrEmpty(urlParameters)) urlParameters += "&";
            urlParameters += "SPName=" + (String.IsNullOrEmpty(viewModel.SPName) ? "" : viewModel.SPName.Trim());
            urlParameters += "&HeadCompanyId=" + viewModel.HeadCompanyId;
            urlParameters += "&State=" + viewModel.State;
            urlParameters += "&CatCode=" + viewModel.CatCode;
            urlParameters += "&LossTypeId=" + viewModel.LossTypeId;
            urlParameters += "&SearchParameterId=" + viewModel.SearchBy;
            urlParameters += "&SearchParameterValue=" + viewModel.SearchFilter;

            viewModel.ReportURL += "?" + urlParameters;

            return View(viewModel);
        }

        [Authorize]
        public ActionResult KPIReport()
        {
            KPIReportViewModel viewModel = new KPIReportViewModel();
            viewModel.ReportURL = "~/Reports/KPIReport.aspx";
            string urlParameters = "";
            populateKPIReportDDLs(ref viewModel);
            viewModel.PivotCategoryId = 1;

            viewModel.ReportURL += "?PivotCategoryId=" + viewModel.PivotCategoryId;
            return View(viewModel);
        }
        [HttpPost]
        [Authorize]
        public ActionResult KPIReport(KPIReportViewModel viewModel)
        {
            viewModel.ReportURL = "~/Reports/KPIReport.aspx";
            populateKPIReportDDLs(ref viewModel);
            string urlParameters = "";

            urlParameters += "PivotCategoryId=" + viewModel.PivotCategoryId;
            if (viewModel.LossDateFrom.HasValue)
            {
                urlParameters += "&LossDateFrom=" + viewModel.LossDateFrom.Value.ToString("MM-dd-yyyy");
            }
            if (viewModel.LossDateTo.HasValue)
            {
                urlParameters += "&LossDateTo=" + viewModel.LossDateTo.Value.ToString("MM-dd-yyyy");
            }
            if (viewModel.DateReceivedFrom.HasValue)
            {
                urlParameters += "&DateReceivedFrom=" + viewModel.DateReceivedFrom.Value.ToString("MM-dd-yyyy");
            }
            if (viewModel.DateReceivedTo.HasValue)
            {
                urlParameters += "&DateReceivedTo=" + viewModel.DateReceivedTo.Value.ToString("MM-dd-yyyy");
            }

            urlParameters += "&LossTypeId=" + viewModel.LossTypeId;
            urlParameters += "&LossState=" + viewModel.LossState;
            urlParameters += "&CatCode=" + viewModel.CatCode;
            urlParameters += "&SPName=" + (String.IsNullOrEmpty(viewModel.SPName) ? "" : viewModel.SPName.Trim());
            urlParameters += "&QAAgentUserId=" + viewModel.QAAgentUserId;
            urlParameters += "&HeadCompanyId=" + viewModel.HeadCompanyId;

            viewModel.ReportURL += "?" + urlParameters;

            return View(viewModel);
        }

        [Authorize]
        public ActionResult ProdReport()
        {
            string urlParameters = "";

            ProdReportViewModel viewModel = new ProdReportViewModel();
            ViewBag.SearchList = new List<SelectListItem> {
            new SelectListItem {Text="--Select--",Value="0",Selected=true}
            };
            populateProdReportDDLs(ref viewModel);
            viewModel.ReportURL = "~/Reports/ProdReport.aspx";
            viewModel.EndDate = css.usp_GetLocalDateTime().First().Value;
            viewModel.StartDate = new DateTime(viewModel.EndDate.Value.Year, viewModel.EndDate.Value.Month, 1);
            viewModel.IncludeYoY = true;
            viewModel.DataSetType = 2;
            viewModel.SearchBy = 1;
            viewModel.HeadCompanyId = 0;
            viewModel.SearchfilterValue = 0;

            urlParameters = "StartDate=" + viewModel.StartDate.Value.ToString("MM-dd-yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.Value.ToString("MM-dd-yyyy");
            urlParameters += "&HeadCompanyId=" + viewModel.HeadCompanyId;
            urlParameters += "&DataSetType=" + viewModel.DataSetType;
            urlParameters += "&IncludeYoY=" + viewModel.IncludeYoY;
            urlParameters += "&SearchParameterId=" + viewModel.SearchBy;
            urlParameters += "&SearchParameterValue=" + viewModel.SearchFilter;

            viewModel.ReportURL += "?" + urlParameters;

            return View(viewModel);
        }
        [HttpPost]
        [Authorize]
        public ActionResult ProdReport(ProdReportViewModel viewModel)
        {
            viewModel.ReportURL = "~/Reports/ProdReport.aspx";
            populateProdReportDDLs(ref viewModel);
            string urlParameters = "";
            viewModel.SearchfilterValue = viewModel.SearchFilter;
         //   viewModel.SearchfilterValue = viewModel.SearchFilter;
            ViewBag.SearchList = new List<SelectListItem> {
            new SelectListItem {Text="--Select--",Value="0"}
            };
            if (!viewModel.StartDate.HasValue || !viewModel.EndDate.HasValue)
            {
                viewModel.EndDate = css.usp_GetLocalDateTime().First().Value;
                viewModel.StartDate = new DateTime(viewModel.EndDate.Value.Year,viewModel.EndDate.Value.Month, 1);
            }

            urlParameters = "StartDate=" + viewModel.StartDate.Value.ToString("MM-dd-yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.Value.ToString("MM-dd-yyyy");
           
            urlParameters += "&DataSetType=" + viewModel.DataSetType;
            urlParameters += "&IncludeYoY=" + viewModel.IncludeYoY;
            urlParameters += "&SearchParameterId=" + viewModel.SearchBy;
            urlParameters += "&SearchParameterValue=" + viewModel.SearchFilter;

            viewModel.ReportURL += "?" + urlParameters;

            return View(viewModel);
        }

        [Authorize]
        public ActionResult TotalCycleByRCVReport()
        {
            string urlParameters = "";

            TotalCycleByRCVReportViewModel viewModel = new TotalCycleByRCVReportViewModel();
            populateTotalCycleByRCVReportDDLs(ref viewModel);
            viewModel.ReportURL = "~/Reports/TotalCycleByRCVReport.aspx";
            viewModel.EndDate = css.usp_GetLocalDateTime().First().Value;
            viewModel.StartDate = new DateTime(viewModel.EndDate.Value.Year, 1, 1);

            urlParameters = "StartDate=" + viewModel.StartDate.Value.ToString("MM-dd-yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.Value.ToString("MM-dd-yyyy");

            viewModel.ReportURL += "?" + urlParameters;

            return View(viewModel);
        }
        [HttpPost]
        [Authorize]
        public ActionResult TotalCycleByRCVReport(TotalCycleByRCVReportViewModel viewModel)
        {
            viewModel.ReportURL = "~/Reports/TotalCycleByRCVReport.aspx";
            populateTotalCycleByRCVReportDDLs(ref viewModel);
            string urlParameters = "";

            if (!viewModel.StartDate.HasValue || !viewModel.EndDate.HasValue)
            {
                viewModel.EndDate = css.usp_GetLocalDateTime().First().Value;
                viewModel.StartDate = new DateTime(viewModel.EndDate.Value.Year, 1, 1);
            }

            urlParameters = "StartDate=" + viewModel.StartDate.Value.ToString("MM-dd-yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.Value.ToString("MM-dd-yyyy");
            urlParameters += "&HeadCompanyId=" + viewModel.HeadCompanyId;
            urlParameters += "&QAAgent=" + viewModel.QAAgent;
            urlParameters += "&SPName=" + viewModel.SPName;
            urlParameters += "&CSSPOCUserId=" + viewModel.CSSPOCUserId;
            urlParameters += "&LossState=" + viewModel.LossState;
            urlParameters += "&LossTypeId=" + viewModel.LossTypeId;
            urlParameters += "&CatCode=" + viewModel.CatCode;

            viewModel.ReportURL += "?" + urlParameters;

            return View(viewModel);
        }

        [Authorize]
        public ActionResult PayrollSummaryReport()
        {
            PayrollSummaryReportViewModel viewModel = getPayrollSummaryReportViewModel(DateTime.Now, 0);

            return View(viewModel);
        }
        [Authorize]
        [HttpPost]
        public ActionResult PayrollSummaryReport(PayrollSummaryReportViewModel viewModel)
        {
            if (viewModel.EndDate == DateTime.MinValue)
            {
                viewModel.EndDate = DateTime.Now;
            }
            viewModel = getPayrollSummaryReportViewModel(viewModel.EndDate, viewModel.UserTypeId);
            return View(viewModel);
        }
        public ActionResult SPDetailReport()
        {

            string urlParameters = "";
            SPDetailReportViewModel viewModel = new SPDetailReportViewModel();

            viewModel.CompanyId = 0;
            viewModel.TypeOfLossId = 0;
            DateTime today = DateTime.Today;

            viewModel.StartDate = new DateTime(today.Year, today.Month, 1);
            viewModel.EndDate = new DateTime(today.Year, today.Month, 1).AddMonths(1).AddDays(-1);
            viewModel.ReportURL = "~/Reports/SPDetailReport.aspx";
            viewModel.CompanyList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (InsuranceCompaniesGetList_Result insurancecompany in css.InsuranceCompaniesGetList())
            {
                viewModel.CompanyList.Add(new SelectListItem { Text = insurancecompany.CompanyName, Value = insurancecompany.CompanyId.ToString() });
            }
            viewModel.TypeOfLossList.Add(new SelectListItem { Text = "--Select--", Value = "0" });


        

            foreach (var losstype in css.LossTypes.ToList())
            {
                viewModel.TypeOfLossList.Add(new SelectListItem { Text=losstype.LossType1,Value=losstype.LossTypeId.ToString()});
            }
            urlParameters = "CompanyID=" + viewModel.CompanyId;
            urlParameters += "&LossTypeID=" + viewModel.TypeOfLossId;
            urlParameters+="&StartDate=" + viewModel.StartDate.Value.ToString("MM-dd-yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.Value.ToString("MM-dd-yyyy");
            viewModel.ReportURL += "?" + urlParameters;
            return View(viewModel);
        }
        [Authorize]
        [HttpPost]
        public ActionResult SPDetailReport(SPDetailReportViewModel viewModel)
        {

            string urlParameters = "";
            //SPDetailReportViewModel viewModel = new SPDetailReportViewModel();
            //viewModel.CompanyId = 0;
            viewModel.ReportURL = "~/Reports/SPDetailReport.aspx";
            viewModel.CompanyList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (InsuranceCompaniesGetList_Result insurancecompany in css.InsuranceCompaniesGetList())
            {
                viewModel.CompanyList.Add(new SelectListItem { Text = insurancecompany.CompanyName, Value = insurancecompany.CompanyId.ToString() });
            }
            viewModel.TypeOfLossList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            foreach (var losstype in css.LossTypes.ToList())
            {
                viewModel.TypeOfLossList.Add(new SelectListItem { Text = losstype.LossType1, Value = losstype.LossTypeId.ToString() });
            }
            urlParameters = "CompanyID=" + viewModel.CompanyId;
            urlParameters += "&LossTypeID=" + viewModel.TypeOfLossId;
            urlParameters += "&StartDate=" + viewModel.StartDate.Value.ToString("MM-dd-yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.Value.ToString("MM-dd-yyyy");
            viewModel.ReportURL += "?" + urlParameters;
            return View(viewModel);
        }
        [Authorize]
        [HttpPost]
        public ActionResult PayrollSummaryReportProcess(PayrollSummaryReportViewModel viewModel)
        {
            ObjectResult<DateTime?> cstLocalTime = css.usp_GetLocalDateTime();
            DateTime payrollDateTime = cstLocalTime.First().Value;
            payrollDateTime = new DateTime(payrollDateTime.Year, payrollDateTime.Month, payrollDateTime.Day, payrollDateTime.Hour, payrollDateTime.Minute, 0);//retain time info till minute

            foreach (var master in viewModel.PayrollSummaryReport.Where(x => x.isSelected == true))
            {
                foreach (var detail in master.Detail)
                {
                    css.usp_SPPayrollAndAdjustmentsReleasePayment(detail.PAId, detail.PaymentType, payrollDateTime);
                }
                List<usp_PayrollSummaryDetailReport_Result> taskDetail = master.Detail;
                Int64 taskUserId = master.Master.Companyid.Value;//svp
                decimal taskUserPaymentAmount = master.Master.PaymentAmount.Value;
                DateTime taskEndDate = viewModel.EndDate;
                DateTime taskPayrollDateTime = payrollDateTime;



                // Task.Factory.StartNew(() =>
                // {
                try
                {
                    ChoiceSolutionsEntities taskCSS = new ChoiceSolutionsEntities();
                    //string qbBillId = Utility.QBUpdateBill(taskUserId, taskUserPaymentAmount, taskPayrollDateTime, taskEndDate, taskDetail);
                    foreach (var detail in taskDetail)
                    {
                        //taskCSS.usp_SPPayrollAndAdjUpdateQBBillId(detail.PAId, detail.PaymentType, qbBillId);
                    }
                }
                catch (Exception ex)
                {
                    Utility.LogException("Reporting/PayrollSummaryReportProcess", ex, taskUserId + "|" + taskUserPaymentAmount);
                }
                // }, TaskCreationOptions.LongRunning);
            }
            return View("PayrollSummaryReportSuccess", payrollDateTime);
        }
        [Authorize]
        public ActionResult PayrollSummaryReportSuccess()
        {
            return View();
        }
        private PayrollSummaryReportViewModel getPayrollSummaryReportViewModel(DateTime invoiceEndDate, int userTypeId)
        {
            PayrollSummaryReportViewModel viewModel = new PayrollSummaryReportViewModel();
            viewModel.EndDate = invoiceEndDate;
            viewModel.UserTypeId = userTypeId;
            
            
            
            //********************************** TEMP CHANGES **********************************************************************************
            //List<usp_PayrollSummaryMasterReport_Result> masterRecords = css.usp_PayrollSummaryMasterReport(invoiceEndDate, userTypeId).ToList();
            List<usp_PayrollSummaryMasterReport_Result> masterRecords = css.usp_PayrollSummaryMasterReport(invoiceEndDate, userTypeId,null).ToList();
            //**********************************************************************************************************************************
            
            
            
            
            List<PayrollSummaryReportModel> report = new List<PayrollSummaryReportModel>();
            viewModel.PayrollSummaryReport = report;
            foreach (var master in masterRecords)
            {
                PayrollSummaryReportModel record = new PayrollSummaryReportModel();
                record.Master = master;



                //********************************** TEMP CHANGES **********************************************************************************    
                //record.Detail = css.usp_PayrollSummaryDetailReport(master.SPId, invoiceEndDate).ToList();
                record.Detail = css.usp_PayrollSummaryDetailReport(master.Companyid, invoiceEndDate,null).ToList();//svp
                //********************************** TEMP CHANGES **********************************************************************************

                if (master.PaymentAmount >= 0)
                {
                    record.isSelectable = true;
                }
                report.Add(record);
            }
            return viewModel;
        }
        public JsonResult getSearchFilterJson(string searchId)
        {
            return Json(getSearchFilter(searchId));
        }
        public SelectList getSearchFilter(string searchId)
        {
            IEnumerable<SelectListItem> SearchList = new List<SelectListItem>();
            if (!string.IsNullOrEmpty(searchId))
            {

                if (searchId == "1")
                {

                    SearchList = (from m in css.Companies select m).AsEnumerable().Select(m => new SelectListItem() { Text = m.CompanyName, Value = m.CompanyId.ToString() });
                }
                if (searchId == "3")
                {
                    SearchList = (from m in css.Users where m.UserTypeId == 11 select m).AsEnumerable().Select(m => new SelectListItem() { Text = m.FirstName + ' ' + m.LastName, Value = m.UserId.ToString() });
                }
                if (searchId == "2")
                {
                    
                    //var query = (from q in css.Users
                    //              join f in css.UserAssignedRights on q.UserId equals f.UserId into fg
                    //              from fgi in fg.DefaultIfEmpty()
                    //              where (q.UserTypeId == 8 && q.Active == true) || (fgi.UserRightId == 1 && fgi.IsActive == true)
                    //              select new
                    //              {
                    //                  UserId = q.UserId,
                    //                  UserName = q.FirstName +" "+ q.LastName
                    //              }).Distinct();
                    var query = (from q in css.PropertyAssignments
                                 join f in css.Users on q.CSSQAAgentUserId equals f.UserId
                                 select new
                                 {
                                     UserId = f.UserId,
                                     UserName = f.FirstName + " " + f.LastName
                                 }).Distinct();

                    SearchList = (from m in query select m).AsEnumerable().Select(m => new SelectListItem() { Text = m.UserName, Value = m.UserId.ToString() });
                    

                    

                }
                if (searchId == "4")
                {
                    var query = (from q in css.PropertyAssignments
                                 join f in css.Users on q.CSSPointofContactUserId equals f.UserId
                                 select new
                                 {
                                     UserId = f.UserId,
                                     UserName = f.FirstName + " " + f.LastName
                                 }).Distinct();

                    SearchList = (from m in query select m).AsEnumerable().Select(m => new SelectListItem() { Text = m.UserName, Value = m.UserId.ToString() });
                }
            }
            return new SelectList(SearchList, "Value", "Text");
        }
        [Authorize]
        public ActionResult QuarterlyReport()
        {
            string urlParameters = "";
            string reportURL = "~/Reports/QuarterlyReport.aspx";
            QuarterlyReportViewModel viewModel = new QuarterlyReportViewModel();
            viewModel.ReportURL = reportURL;
          

            viewModel.CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                viewModel.CompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }

            //viewModel.StartDate = css.usp_GetLocalDateTime().First().Value.AddMonths(-3).Date;
            viewModel.EndDate = css.usp_GetLocalDateTime().First().Value.Date;
           // viewModel.EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
            viewModel.StartDate = new DateTime(viewModel.EndDate.Year, 1, 1);

            urlParameters += "HeadCompanyId=" + viewModel.HeadCompanyId;
       
            urlParameters += "&StartDate=" + viewModel.StartDate.ToString("MM-dd-yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.ToString("MM-dd-yyyy");
            viewModel.ReportURL = reportURL + "?" + urlParameters;
            return View(viewModel);
        }
       
        [HttpPost]
        [Authorize]
        public ActionResult QuarterlyReport(QuarterlyReportViewModel viewModel)
        {
            viewModel.CompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var company in css.GetCompaniesList(1))
            {
                viewModel.CompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = company.CompanyId + "" });
            }



            string reportURL = "~/Reports/QuarterlyReport.aspx";
            string urlParameters = "";

           
            urlParameters += "HeadCompanyId=" + viewModel.HeadCompanyId;
            
            urlParameters += "&StartDate=" + viewModel.StartDate.ToString("MM/dd/yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.ToString("MM/dd/yyyy");


            viewModel.ReportURL = reportURL + "?" + urlParameters;

            return View(viewModel);
        }

        [Authorize]
        public ActionResult SPLicenseExpiryReport()
        {
            string urlParameters = "";
            string reportURL = "~/Reports/SPLicenseExpiryReport.aspx";
            SPLicenseExpiryReportViewModel viewModel = new SPLicenseExpiryReportViewModel();
            viewModel.StatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            viewModel.StatusList.Add(new SelectListItem { Text = "Active", Value = "Active" });
            viewModel.StatusList.Add(new SelectListItem { Text = "Expired", Value = "Expired" });

            viewModel.SPName = "";
            viewModel.Status = "0";
            try
            {
                //  viewModel.ToExpiryDate = css.usp_GetLocalDateTime().First().Value.Date;
                // viewModel.FromExpiryDate = new DateTime(viewModel.ToExpiryDate.Value.Year, viewModel.ToExpiryDate.Value.Month, 1); 
                //viewModel.ToExpiryDate =new DateTime(2015,03,10);
                //viewModel.FromExpiryDate = new DateTime(2012, 03, 01);
                DateTime today = DateTime.Today;
                viewModel.FromExpiryDate = new DateTime(today.Year, today.Month, 1);
                viewModel.ToExpiryDate = new DateTime(today.Year, today.Month, 1).AddMonths(1).AddDays(-1);

                // DateTime today = DateTime.Today;
                // viewModel.FromExpiryDate = null;
                //  viewModel.ToExpiryDate = null;
            }
            catch (Exception ex)
            {

                ///  viewModel.ToExpiryDate = css.usp_GetLocalDateTime().First().Value.Date;
                //viewModel.FromExpiryDate= css.usp_GetLocalDateTime().First().Value.Date;
            }
            urlParameters += "SPName=" + viewModel.SPName;
            urlParameters += "&Status=" + viewModel.Status;
            urlParameters += "&FromExpiryDate=" + viewModel.FromExpiryDate.Value.ToString("MM-dd-yyyy");
            urlParameters += "&ToExpiryDate=" + viewModel.ToExpiryDate.Value.ToString("MM-dd-yyyy");
            viewModel.ReportURL = reportURL + "?" + urlParameters;
            return View(viewModel);
        }
        [HttpPost]
        [Authorize]
        public ActionResult SPLicenseExpiryReport(SPLicenseExpiryReportViewModel viewModel)
        {
            string urlParameters = "";
            string reportURL = "~/Reports/SPLicenseExpiryReport.aspx";

            viewModel.StatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            viewModel.StatusList.Add(new SelectListItem { Text = "Active", Value = "Active" });
            viewModel.StatusList.Add(new SelectListItem { Text = "Expired", Value = "Expired" });



            urlParameters += "SPName=" + viewModel.SPName;
            urlParameters += "&Status=" + viewModel.Status;
            if (viewModel.FromExpiryDate != null)
            {
                urlParameters += "&FromExpiryDate=" + viewModel.FromExpiryDate.Value.ToString("MM-dd-yyyy");
            }
            else
            {
                urlParameters += "&FromExpiryDate=''";
            }
            if (viewModel.ToExpiryDate != null)
            {
                urlParameters += "&ToExpiryDate=" + viewModel.ToExpiryDate.Value.ToString("MM-dd-yyyy");
            }
            else
            {
                urlParameters += "&ToExpiryDate=''";
            }
            viewModel.ReportURL = reportURL + "?" + urlParameters;
            return View(viewModel);

        }


        [Authorize]
        public ActionResult FailedEmailReport()
        {
            string urlParameters = "";
            string reportURL = "~/Reports/FailedEmailReport.aspx";
            FailedEmailReportViewModel viewModel = new FailedEmailReportViewModel();
            viewModel.StatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            viewModel.StatusList.Add(new SelectListItem { Text = "Failed", Value = "2" });
            viewModel.StatusList.Add(new SelectListItem { Text = "Sent", Value = "1" });

            viewModel.ClaimNumber = null;
            viewModel.Status = "0";
            try
            {
                //  viewModel.ToExpiryDate = css.usp_GetLocalDateTime().First().Value.Date;
                // viewModel.FromExpiryDate = new DateTime(viewModel.ToExpiryDate.Value.Year, viewModel.ToExpiryDate.Value.Month, 1); 
                //viewModel.ToExpiryDate =new DateTime(2015,03,10);
                //viewModel.FromExpiryDate = new DateTime(2012, 03, 01);
                DateTime today = DateTime.Today;
                viewModel.StartDate = new DateTime(today.Year, today.Month, 1);
                viewModel.EndDate = new DateTime(today.Year, today.Month, 1).AddMonths(1).AddDays(-1);
            }
            catch (Exception ex)
            {

                ///  viewModel.ToExpiryDate = css.usp_GetLocalDateTime().First().Value.Date;
                //viewModel.FromExpiryDate= css.usp_GetLocalDateTime().First().Value.Date;
            }
            urlParameters += "StartDate=" + viewModel.StartDate.ToString("MM-dd-yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.ToString("MM-dd-yyyy");
            urlParameters += "&ClaimNumber=" + viewModel.ClaimNumber;
            urlParameters += "&Status=" + viewModel.Status;

            viewModel.ReportURL = reportURL + "?" + urlParameters;
            return View(viewModel);


        }
        [HttpPost]
        [Authorize]
        public ActionResult FailedEmailReport(FailedEmailReportViewModel viewModel)
        {
            string urlParameters = "";
            string reportURL = "~/Reports/FailedEmailReport.aspx";


            viewModel.StatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            viewModel.StatusList.Add(new SelectListItem { Text = "Failed", Value = "2" });
            viewModel.StatusList.Add(new SelectListItem { Text = "Sent", Value = "1" });

            urlParameters += "StartDate=" + viewModel.StartDate.ToString("MM-dd-yyyy");
            urlParameters += "&EndDate=" + viewModel.EndDate.ToString("MM-dd-yyyy");
            urlParameters += "&ClaimNumber=" + viewModel.ClaimNumber;
            urlParameters += "&Status=" + viewModel.Status;

            viewModel.ReportURL = reportURL + "?" + urlParameters;
            return View(viewModel);
        }


        [HttpGet]
        public ActionResult SPRankCalculationReport()
        {
            SPRankCalculationReportViewModel viewModel = new SPRankCalculationReportViewModel();
            viewModel.SPName = null;
            List<SPRankCalculationReportMaster_Result> masterrecords = css.SPRankCalculationReportMaster(viewModel.SPName).ToList();
            viewModel.SPRankCalculationReport = new List<SPRankCalculationReportModel>();

            foreach (var master in masterrecords)
            {
                SPRankCalculationReportModel record = new SPRankCalculationReportModel();
                record.Master = master;
                record.Detail = css.SPRankCalculationReportDetail(master.UserId).ToList();
                viewModel.SPRankCalculationReport.Add(record);
            }
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult SPRankCalculationReport(SPRankCalculationReportViewModel viewModel)
        {

            List<SPRankCalculationReportMaster_Result> masterrecords = css.SPRankCalculationReportMaster(viewModel.SPName).ToList();
            viewModel.SPRankCalculationReport = new List<SPRankCalculationReportModel>();

            foreach (var master in masterrecords)
            {
                SPRankCalculationReportModel record = new SPRankCalculationReportModel();
                record.Master = master;
                record.Detail = css.SPRankCalculationReportDetail(master.UserId).ToList();
                viewModel.SPRankCalculationReport.Add(record);

            }
            return View(viewModel);

        }

    }
}
