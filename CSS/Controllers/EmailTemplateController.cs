﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ViewModels;
using System.Data.Objects;
using System.Data;

namespace CSS.Controllers
{
    public class EmailTemplateController : Controller
    {
        //
        // GET: /EmailTemplate/

        //public ActionResult Index()
        //{
        //    return View();
        //}

        BLL.ChoiceSolutionsEntities css = new BLL.ChoiceSolutionsEntities();
        [HttpGet]
        public ActionResult EmailTemplate(Int32 HeadCompanyId, Int32 TemplateId,Int32 StatusId)
        {
            EmailTemplateViewModel ViewModel = new EmailTemplateViewModel(HeadCompanyId);
            
            if (TemplateId == -1)
            {
                ViewModel.TemplateGetList = css.EmailTemplateGetList(HeadCompanyId).ToList();
                ViewModel.emailtemplate.TemplateId = 0;
            }
            else
            {
                ViewModel = new EmailTemplateViewModel(HeadCompanyId, TemplateId);
                ViewModel.TemplateGetList = css.EmailTemplateGetList(HeadCompanyId).ToList();
                ViewModel.emailtemplate.TemplateId = TemplateId;
               
            }

            ViewModel.headCompanyId = HeadCompanyId;
            ViewModel.StatusId = StatusId;
            return View(ViewModel);
        }

        [HttpPost]
        public ActionResult EmailTemplate(EmailTemplateViewModel ViewModel,FormCollection form)
        {

            bool flag = false;
             
            try
            {
               
                if (isFormValid(ViewModel, form))
                {
                     string templatebody = Convert.ToString(form["txtTemplateBody"]);
                     ViewModel.StatusId = 0;
                    if (ViewModel.emailtemplate.TemplateId <= 0)
                    {
                        ObjectParameter outTemplateId = new ObjectParameter("TemplateId", DbType.Int32);
                        ObjectParameter outStatusId = new ObjectParameter("StatusId", DbType.Int32);
                        css.InsertEmailTemplates(outTemplateId, ViewModel.emailtemplate.TemplateName, templatebody, ViewModel.headCompanyId, outStatusId);

                        if (Convert.ToInt32(outStatusId.Value) == 1)
                        {
                            ViewModel.StatusId = 1;
                            ViewModel.TemplateGetList = css.EmailTemplateGetList(ViewModel.headCompanyId).ToList();
                            ViewModel.emailtemplate.TemplateBody = form["txtTemplateBody"];
                            return View(ViewModel);
                        }
                        flag = false;
                        
                        

                    }
                    else
                    {

                        css.UpdateEmailTemplates(ViewModel.emailtemplate.TemplateId, ViewModel.emailtemplate.TemplateName, templatebody);
                        flag = true;

                    }

                    

                }
                else
                {
                    ViewModel.TemplateGetList = css.EmailTemplateGetList(ViewModel.headCompanyId).ToList();
                    ViewModel.emailtemplate.TemplateBody = form["txtTemplateBody"];
                    return View(ViewModel);
                }
             

            }
            catch(Exception ex)
            {

            }
            if (flag == true)
            {
                return RedirectToAction("EmailTemplate", "EmailTemplate", new { HeadCompanyId = ViewModel.headCompanyId, TemplateId = ViewModel.emailtemplate.TemplateId,StatusId=2 });
            }
            else
            {
                return RedirectToAction("EmailTemplate", "EmailTemplate", new { HeadCompanyId = ViewModel.headCompanyId, TemplateId = -1, StatusId = 2 });
            }
        }

        public ActionResult EmailTemplateDelete(EmailTemplateViewModel ViewModel,int TemplateId)
        {
            try
            {
                if (TemplateId > 0)
                {
                    css.DeleteEmailTemplates(TemplateId);
                    //ViewModel.TemplateGetList = css.EmailTemplateGetList(ViewModel.headCompanyId).ToList();
                }

            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("EmailTemplate", "EmailTemplate", new { HeadCompanyId = ViewModel.headCompanyId, TemplateId = -1, StatusId = 3 });
        }

        private bool isFormValid(EmailTemplateViewModel viewModel, FormCollection form)
        {
            bool valueToReturn = true;
            if (viewModel.emailtemplate.TemplateName == "" || viewModel.emailtemplate.TemplateName == null)
            {

                ModelState.AddModelError("emailtemplate.TemplateName", "Please Enter Template Name");
                valueToReturn = false;
            }
            //LOBPricing should not contain non Time and Expense Pricing if T&E monthly billing day is specified
            if (form["txtTemplateBody"] == "" || form["txtTemplateBody"] == null)
            {
                ModelState.AddModelError("txtTemplateBody", "Please Enter TemplateBody");
                valueToReturn = false;
            }

            return valueToReturn;
        }
    }
}
