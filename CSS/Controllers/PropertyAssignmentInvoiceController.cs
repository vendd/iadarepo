﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL.ViewModels;
using BLL;
using System.Data.Objects;
using System.Data;
using System.IO;
using BLL.ViewModels;
using System.Threading.Tasks;
using System.Configuration;

namespace CSS.Controllers
{
    public class PropertyAssignmentInvoiceController : CustomController
    {
        //
        // GET: /PropertyAssignmentInvoice/

        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(Int64? assignmentid, string fileOpenMode, Int64? invoiceId)
        {
            PropertyInvoiceViewModel viewModel = new PropertyInvoiceViewModel();
            try
            {
                css = new ChoiceSolutionsEntities();
                viewModel.viewType = "EDIT";//EDIT -> New or Edit
                viewModel.invoiceDetail = css.usp_PropertyInvoiceGetDetails(assignmentid, invoiceId).First();
                viewModel.claim = css.Claims.Find(viewModel.invoiceDetail.ClaimId);
                viewModel.propertyAssignment = css.PropertyAssignments.Find(viewModel.invoiceDetail.AssignmentId);
                viewModel.invoiceDetail.MileageCharges = Convert.ToDecimal(viewModel.invoiceDetail.RatePerMile);
                viewModel.invoiceDetail.IncludedMiles = Convert.ToInt16(viewModel.invoiceDetail.FreeMiles);
                viewModel.invoiceDetail.PhotoCharge = Convert.ToDecimal(viewModel.invoiceDetail.RatePerPhoto);
                viewModel.invoiceDetail.PhotosIncluded = Convert.ToByte(viewModel.invoiceDetail.NoOfPhotosIncluded);
                viewModel.invoiceDetail.TaxID = System.Configuration.ConfigurationManager.AppSettings["CSSTaxId"].ToString();
                viewModel.TotalBalanceDue = css.usp_ClaimTotalBalanceDueGet(viewModel.claim.ClaimId).First().Value;
                if (viewModel.invoiceDetail.FeeType == 1)
                {
                    viewModel.ShowBilledNotesExportOption = true;
                }
                if (invoiceId == -1)
                {
                    viewModel.invoiceDetail.PhotoCount = (short?)viewModel.propertyAssignment.PhotoCount;

                    bool useDefaultBillableTime = true;
                    if (viewModel.claim.LOBId.HasValue)
                    {
                        LineOfBusiness lob = css.LineOfBusinesses.Find(viewModel.claim.LOBId);
                        if (lob.TEMonthlyBillingDay.HasValue)//If TEMonthlyBillingDay is specified then a Time and Expense Interim/Final invoice is being created. Monthly billing of Time and Expense is required.
                        {
                            viewModel.invoiceDetail.TE1NoOfHours = css.usp_NotesUnbilledHoursGet(viewModel.claim.ClaimId).First();
                            useDefaultBillableTime = false;
                            viewModel.invoiceDetail.InvoiceType = (byte)2;
                            viewModel.invoiceDetail.FeeType = 1;//Default to Time & Expense
                            viewModel.ShowBilledNotesExportOption = true;
                        }
                    }
                    if (useDefaultBillableTime == true)
                    {
                        ObjectResult<usp_NoteRunningTotalGet_Result> noteRunningTotalGet = css.usp_NoteRunningTotalGet(assignmentid);
                        viewModel.invoiceDetail.TE1NoOfHours = noteRunningTotalGet.First().BillableTime;
                    }

                }


                viewModel.TotalAmountReceived = css.usp_PropertyInvoiceTotalSum(viewModel.invoiceDetail.InvoiceId).FirstOrDefault();
                if (css.PropertyInvoices.Where(x => x.AssignmentId == viewModel.invoiceDetail.AssignmentId).Count() == 0)
                {
                    viewModel.invoiceDetail.SPOtherTravelPercent = 100;
                    viewModel.invoiceDetail.SPTollsPercent = 100;
                    viewModel.invoiceDetail.SPTotalMileagePercent = 100;
                }
                //if (invoiceId != -1 && css.PropertyInvoices.Where(x => x.AssignmentId == viewModel.invoiceDetail.AssignmentId).Count() != 0)
                //{
                //    try
                //    {
                //        var LobPricing = css.usp_PropertyInvoiceServiceFeeGetDetail(viewModel.invoiceDetail.HeadCompanyId, viewModel.invoiceDetail.LOBId, Convert.ToDouble(viewModel.invoiceDetail.RCV)).First();
                //        //Identify if BaseServiceFee and SPServiceFee is specified
                //        ViewBag.hdnSPServiceFee = LobPricing.SPServiceFee;
                //    }
                //    catch (Exception ex)
                //    {
                //    }
                //}
                var lineOfBussinessId = css.LineOfBusinesses.Where(x => x.HeadCompanyId == viewModel.invoiceDetail.HeadCompanyId);
                foreach (var Lob in lineOfBussinessId)
                {
                    viewModel.LineOfBussinessList.Add(new SelectListItem { Text = Lob.LOBDescription, Value = Lob.LOBId.ToString() });
                }
                //TempData["invoiceNo"] = viewModel.invoiceDetail.InvoiceNo;
                //TempData["assignmentId"] = viewModel.invoiceDetail.AssignmentId;
                //TempData["claimNumber"] = viewModel.invoiceDetail.ClaimNumber;
                //TempData["feetype"] = viewModel.feetype;
                TempData["ServiceOffering"] = viewModel.invoiceDetail.ServiceOffering;

            }
            catch (Exception ex)
            { }
            bool isOAUserIDSpecified = true;
            if (viewModel.propertyAssignment.OAUserID.HasValue == false)
            {
                isOAUserIDSpecified = false;
                ModelState.AddModelError("OAUserId", "Service Provider has not been assigned to this assignment.");
            }
            bool isQAAgentSpecified = true;
            if (viewModel.propertyAssignment.CSSQAAgentUserId.HasValue == false)
            {
                isQAAgentSpecified = false;

            }
            if (viewModel.propertyAssignment.CSSQAAgentUserId.HasValue == true && viewModel.propertyAssignment.CSSQAAgentUserId.Value == 0)
            {
                isQAAgentSpecified = false;
            }
            if (!isQAAgentSpecified)
            {
                ModelState.AddModelError("CSSQAAgentUserId", "QA Agent has not been assigned to this assignment.");
            }
            if (!isQAAgentSpecified || !isOAUserIDSpecified)
            {
                viewModel.allowSubmit = false;
            }
            else
            {
                viewModel.allowSubmit = true;
            }
            if (fileOpenMode.ToUpper() == "NEW" || fileOpenMode.ToUpper() == "EDIT")
            {
                return PartialView("_InvoiceDetails", viewModel);
            }
            else if (fileOpenMode.ToUpper() == "READ")
            {
                return PartialView("_GenerateInvoicePdf", viewModel);
            }
            else { return PartialView("_InvoiceDetails", viewModel); }
        }

        public ActionResult GetLObPricingDetails(Int64? claimId, Int64? invoiceId, byte? invoiceType, byte? feeType, Int32? LobId, Int32? HeadCompanyId, float RCVValue)
        {

            try
            {
                css = new ChoiceSolutionsEntities();
                var LobPricingCount = css.usp_PropertyInvoiceServiceFeeGetDetail(HeadCompanyId, LobId, RCVValue).Count();

                if (LobPricingCount > 0)
                {
                    var LobPricing = css.usp_PropertyInvoiceServiceFeeGetDetail(HeadCompanyId, LobId, RCVValue).ToList();

                    //Identify whether a supplement invoice with the fee type set to scheduled fee is being created 
                    if (invoiceType == 3 && feeType == 3)
                    {
                        //Identify whether existing invoices exist
                        List<Int64> assignmentIds = css.PropertyAssignments.Where(x => x.ClaimId == claimId).Select(x => x.AssignmentId).ToList();
                        List<PropertyInvoice> existingPropertyInvoices = css.PropertyInvoices.Where(x => assignmentIds.Any(y => y == x.AssignmentId) && x.InvoiceId != invoiceId).ToList();
                        if (existingPropertyInvoices.Count > 0)
                        {
                            PropertyInvoice mostRecentInvoice = existingPropertyInvoices.OrderByDescending(x => x.InvoiceId).First();

                            if (mostRecentInvoice.FeeType == 3)
                            {
                                float prevRCVValue = (float)mostRecentInvoice.RCV.Value;
                                var prevLOBPricing = css.usp_PropertyInvoiceServiceFeeGetDetail(HeadCompanyId, LobId, prevRCVValue).First();
                                LobPricing[0].BaseServiceFee = LobPricing[0].BaseServiceFee - prevLOBPricing.BaseServiceFee;
                                LobPricing[0].SPServiceFee = LobPricing[0].SPServiceFee - prevLOBPricing.SPServiceFee;
                            }
                        }
                    }
                    return Json(LobPricing);
                }
                else
                {
                    return Json(1);
                }

            }
            catch (Exception ex)
            {
                return Json(0);
            }
        }

        public ActionResult GetCompanyLObPricingDetails(Int32? LobId, Int32? HeadCompanyId, float RCVValue)
        {
            try
            {
                css = new ChoiceSolutionsEntities();
                var CompanyLobpricingCount = css.LineOfBusinesses.Where(x => x.HeadCompanyId == HeadCompanyId).Count();
                var CompanyLobpricing = css.usp_LineOfBusinessGetCompanyLOB(HeadCompanyId, LobId);
                if (CompanyLobpricingCount > 0)
                {
                    return Json(CompanyLobpricing);
                }
                else
                {
                    return Json(1);
                }

            }
            catch (Exception ex)
            {
                return Json(0);
            }
        }

        public ActionResult SubmitPropertyInvoice(Int64? invoiceid = -1)
        {

            return View();
        }

        [HttpPost]
        public ActionResult SubmitPropertyInvoice(PropertyInvoiceViewModel ViewModel, string returnToPage, FormCollection form)
        {

            css = new ChoiceSolutionsEntities();


            Int64? claimid = css.PropertyAssignments.Find(ViewModel.invoiceDetail.AssignmentId).ClaimId;
            try
            {


                #region Insert/Update Property Invoice
                Int32 count = css.PropertyInvoices.Where(x => x.AssignmentId == ViewModel.invoiceDetail.AssignmentId).ToList().Count;

                if (ViewModel.invoiceDetail.InvoiceId == -1)
                {
                    ObjectParameter outInvoiceid = new ObjectParameter("InvoiceId", DbType.Int64);
                    css.usp_PropertyInvoiceInsert(outInvoiceid, ViewModel.invoiceDetail.InvoiceNo, ViewModel.invoiceDetail.AssignmentId, ViewModel.invoiceDetail.InvoiceDate, ViewModel.invoiceDetail.OriginalInvoiceDate, ViewModel.invoiceDetail.InvoiceType, ViewModel.invoiceDetail.InvoiceStatus, ViewModel.invoiceDetail.TotalService, ViewModel.invoiceDetail.FieldStaffPercent, ViewModel.invoiceDetail.CSSPOCPercent, ViewModel.invoiceDetail.PhotoCharge, ViewModel.invoiceDetail.PhotoCount, ViewModel.invoiceDetail.PhotosIncluded, ViewModel.invoiceDetail.TotalPhotosCharges, ViewModel.invoiceDetail.AirFarePercent, ViewModel.invoiceDetail.MiscPercent, ViewModel.invoiceDetail.ContentsCountPercent, ViewModel.invoiceDetail.MileageCharges, ViewModel.invoiceDetail.ActualMiles, ViewModel.invoiceDetail.IncludedMiles, ViewModel.invoiceDetail.TotalMiles, ViewModel.invoiceDetail.TotalMileage, ViewModel.invoiceDetail.Tolls, ViewModel.invoiceDetail.Airfare, ViewModel.invoiceDetail.OtherTravelCharge, ViewModel.invoiceDetail.EDIFee, ViewModel.invoiceDetail.TotalAdditionalCharges, ViewModel.invoiceDetail.ServicesCharges, ViewModel.invoiceDetail.OfficeFee, ViewModel.invoiceDetail.FileSetupFee, ViewModel.invoiceDetail.ReInspectionFee, ViewModel.invoiceDetail.OtherFees, ViewModel.invoiceDetail.GrossLoss, ViewModel.invoiceDetail.SubTotal, ViewModel.invoiceDetail.IsTaxApplicable, ViewModel.invoiceDetail.Tax, ViewModel.invoiceDetail.GrandTotal, ViewModel.invoiceDetail.QAAgentFeePercent, ViewModel.invoiceDetail.QAAgentFee, ViewModel.invoiceDetail.SPServiceFeePercent, ViewModel.invoiceDetail.SPServiceFee, ViewModel.invoiceDetail.HoldBackPercent, ViewModel.invoiceDetail.HoldBackFee, ViewModel.invoiceDetail.SPEDICharge, ViewModel.invoiceDetail.SPAerialCharge, Convert.ToDouble(form["invoiceDetail.TotalSPPayPercent"]), ViewModel.invoiceDetail.TotalSPPay, Convert.ToDouble(form["invoiceDetail.CSSPortionPercent"]), ViewModel.invoiceDetail.CSSPortion, ViewModel.invoiceDetail.Notes, ViewModel.invoiceDetail.FeeType, ViewModel.invoiceDetail.RCV, ViewModel.invoiceDetail.SalesCharges, ViewModel.invoiceDetail.SPTotalMileagePercent, ViewModel.invoiceDetail.SPTollsPercent, ViewModel.invoiceDetail.SPOtherTravelPercent, ViewModel.invoiceDetail.BalanceDue, ViewModel.invoiceDetail.SPTotalPhotoPercent, ViewModel.invoiceDetail.Misc, ViewModel.invoiceDetail.MiscComment, ViewModel.invoiceDetail.ContentCount, ViewModel.invoiceDetail.AierialImageFee, ViewModel.invoiceDetail.TE1NoOfHours, ViewModel.invoiceDetail.TE1SPLevel, ViewModel.invoiceDetail.TE1SPHourlyRate);
                    ViewModel.invoiceDetail.InvoiceId = Convert.ToInt64(outInvoiceid.Value);
                    ViewModel.invoiceDetail.InvoiceNo = "INVC-" + ViewModel.invoiceDetail.InvoiceId;
                    Int64 invoiceid = Convert.ToInt64(outInvoiceid.Value);
                    if (ViewModel.invoiceDetail.FeeType == 1)
                    {
                        //If time and expense invoice
                        css.usp_MarkUnbilledNotesAsBilled(claimid, invoiceid);
                    }
                    Task.Factory.StartNew(() =>
                    {
                        Utility.QBUpdateInvoice(invoiceid);
                    }, TaskCreationOptions.LongRunning);
                }
                else
                {
                    css.usp_PropertyInvoiceUpdate(ViewModel.invoiceDetail.InvoiceId, ViewModel.invoiceDetail.InvoiceNo, ViewModel.invoiceDetail.AssignmentId, ViewModel.invoiceDetail.OriginalInvoiceDate, ViewModel.invoiceDetail.InvoiceType, ViewModel.invoiceDetail.InvoiceStatus, ViewModel.invoiceDetail.TotalService, ViewModel.invoiceDetail.FieldStaffPercent, ViewModel.invoiceDetail.CSSPOCPercent, ViewModel.invoiceDetail.PhotoCharge, ViewModel.invoiceDetail.PhotoCount, ViewModel.invoiceDetail.PhotosIncluded, ViewModel.invoiceDetail.TotalPhotosCharges, ViewModel.invoiceDetail.AirFarePercent, ViewModel.invoiceDetail.MiscPercent, ViewModel.invoiceDetail.ContentsCountPercent, ViewModel.invoiceDetail.MileageCharges, ViewModel.invoiceDetail.ActualMiles, ViewModel.invoiceDetail.IncludedMiles, ViewModel.invoiceDetail.TotalMiles, ViewModel.invoiceDetail.TotalMileage, ViewModel.invoiceDetail.Tolls, ViewModel.invoiceDetail.Airfare, ViewModel.invoiceDetail.OtherTravelCharge, ViewModel.invoiceDetail.EDIFee, ViewModel.invoiceDetail.TotalAdditionalCharges, ViewModel.invoiceDetail.ServicesCharges, ViewModel.invoiceDetail.OfficeFee, ViewModel.invoiceDetail.FileSetupFee, ViewModel.invoiceDetail.ReInspectionFee, ViewModel.invoiceDetail.OtherFees, ViewModel.invoiceDetail.GrossLoss, ViewModel.invoiceDetail.SubTotal, ViewModel.invoiceDetail.IsTaxApplicable, ViewModel.invoiceDetail.Tax, ViewModel.invoiceDetail.GrandTotal, ViewModel.invoiceDetail.QAAgentFeePercent, ViewModel.invoiceDetail.QAAgentFee, ViewModel.invoiceDetail.SPServiceFeePercent, ViewModel.invoiceDetail.SPServiceFee, ViewModel.invoiceDetail.HoldBackPercent, ViewModel.invoiceDetail.HoldBackFee, ViewModel.invoiceDetail.SPEDICharge, ViewModel.invoiceDetail.SPAerialCharge, Convert.ToDouble(form["invoiceDetail.TotalSPPayPercent"]), ViewModel.invoiceDetail.TotalSPPay, Convert.ToDouble(form["invoiceDetail.CSSPortionPercent"]), ViewModel.invoiceDetail.CSSPortion, ViewModel.invoiceDetail.Notes, ViewModel.invoiceDetail.FeeType, ViewModel.invoiceDetail.RCV, ViewModel.invoiceDetail.SalesCharges, ViewModel.invoiceDetail.SPTotalMileagePercent, ViewModel.invoiceDetail.SPTollsPercent, ViewModel.invoiceDetail.SPOtherTravelPercent, ViewModel.invoiceDetail.BalanceDue, ViewModel.invoiceDetail.SPTotalPhotoPercent, ViewModel.invoiceDetail.Misc, ViewModel.invoiceDetail.MiscComment, ViewModel.invoiceDetail.ContentCount, ViewModel.invoiceDetail.AierialImageFee, ViewModel.invoiceDetail.TE1NoOfHours, ViewModel.invoiceDetail.TE1SPLevel, ViewModel.invoiceDetail.TE1SPHourlyRate);
                    Int64 invoiceid = Convert.ToInt64(ViewModel.invoiceDetail.InvoiceId);
                    Task.Factory.StartNew(() =>
                    {
                        Utility.QBUpdateInvoice(invoiceid);
                    }, TaskCreationOptions.LongRunning);
                }
                css.usp_ClaimLOBUpdate(claimid, ViewModel.invoiceDetail.LOBId);
                css.usp_ClaimRCVUpdate(claimid, ViewModel.invoiceDetail.RCV);

                #endregion
                #region Export to Pdf
                string originalFileName = "Invoice.pdf";
                int revisionCount = css.Documents.Where(x => (x.AssignmentId == ViewModel.invoiceDetail.AssignmentId) && (x.DocumentTypeId == 8) && (x.OriginalFileName == originalFileName)).ToList().Count;
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                string thisPageURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
                thisPageURL = thisPageURL.Replace("SubmitPropertyInvoice", "GeneratePdfInvoice");


                ViewModel.viewType = "READ";
                ObjectParameter outDocumentid = new ObjectParameter("Documentid", DbType.Int64);

                string str = RenderViewToString("GeneratePdfInvoice", ViewModel);

                byte[] pdfarray = Utility.ConvertHTMLStringToPDFwithImages(str, thisPageURL);
                #region Cloud Storage

                string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                string assignmentId = ViewModel.invoiceDetail.AssignmentId + "";
                string documentTypeId = 8 + "";

                string fileName = "Invoice" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
                string relativeFileName = assignmentId + "/" + documentTypeId + "/" + fileName;

                string containerName = ConfigurationManager.AppSettings["PropAssignDocsContainerName"].ToString();
                CloudStorageUtility.StoreFile(containerName, relativeFileName, pdfarray);

                #endregion

                #region Local File System

                //string baseFolder = System.Configuration.ConfigurationManager.AppSettings["PropertyAssignmentDocsPath"].ToString();
                //string assignmentId = ViewModel.invoiceDetail.AssignmentId + "";
                //string documentTypeId = 8 + "";


                //string fileName = "Invoice" + (revisionCount > 0 ? "_R" + revisionCount : "") + ".pdf";
                //if (!Directory.Exists(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId)))
                //{
                //    Directory.CreateDirectory(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId));
                //}
                //string path = Path.Combine(Server.MapPath(baseFolder + "" + assignmentId + "/" + documentTypeId), fileName);

                //Utility.StoreBytesAsFile(pdfarray, path);
                #endregion



                //Generation of a monthly T&E Invoice created an incorrect note which would read as Revised Invoice. Make use of a generalised note
                css.DocumentsInsert(outDocumentid, ViewModel.invoiceDetail.AssignmentId, DateTime.Now, "Invoice", originalFileName, fileName, loggedInUser.UserId, 8);

                ObjectParameter outNoteId = new ObjectParameter("NoteId", DbType.Int64);
                css.usp_NotesInsert(outNoteId, ViewModel.invoiceDetail.AssignmentId, "Invoice", "Invoice has been generated.", null, false, true, loggedInUser.UserId, null, null, null, null,null,null);

                ///css.DocumentsInsert(outDocumentid, ViewModel.invoiceDetail.AssignmentId, DateTime.Now, "Invoice" + (revisionCount > 0 ? " (Revision " + revisionCount + ")" : ""), originalFileName, fileName, loggeduser.UserId, 8);
                //if (revisionCount == 0)
                //{

                //    css.usp_NotesInsert(outNoteId, ViewModel.invoiceDetail.AssignmentId, "New Invoice", "New invoice has been generated.", null, false, true, loggeduser.UserId, null, null, null, null);

                //}
                //else
                //{
                //    css.usp_NotesInsert(outNoteId, ViewModel.invoiceDetail.AssignmentId, "Revised Invoice", "A revised invoice has been generated.", null, false, true, loggeduser.UserId, null, null, null, null);

                //}

                #endregion

            }
            catch (Exception ex)
            {
                string exp = ex.Message;
            }
            if (returnToPage == "ManageStatus")
            {
                return RedirectToAction("Details", "PropertyAssignment", new { claimid = Cypher.EncryptString(claimid.ToString()) });
            }
            else if (returnToPage == "ManageStatus")
            {
                return RedirectToAction("Search", "PropertyAssignment", new { claimid = claimid });
            }
            else if (returnToPage == "financial")
            {
                ViewModel.assignmentTab = "7";
                return RedirectToAction("Details", "PropertyAssignment", new { claimid = Cypher.EncryptString(claimid.ToString()) });
            }
            else if (returnToPage == "none")
            {
                return Json(new { InvoiceId = ViewModel.invoiceDetail.InvoiceId }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return RedirectToAction("Search", "PropertyAssignment", new { claimid = claimid });
            }
        }

        public ActionResult GeneratePdfInvoice(PropertyInvoiceViewModel ViewModel)
        {

            return View();
        }
        public ActionResult GetPaymentDetails(Int64 claimId, Int64 assignmentid, Int64 invoiceId)
        {
            PropertyInvoiceViewModel ViewModel = new PropertyInvoiceViewModel();
            css = new ChoiceSolutionsEntities();
            try
            {
                assignmentid = css.usp_PropertyInvoiceSummaryGetList(claimId).OrderByDescending(x => x.InvoiceId).First().AssignmentId.Value;
                ViewModel.PropertyInvoiceList = css.usp_PropertyInvoiceSummaryGetList(claimId).ToList();
                ViewModel.invoiceDetail = css.usp_PropertyInvoiceGetDetails(assignmentid, invoiceId).First();
                ViewModel.paymentslist = css.usp_PaymentGetList(claimId).ToList();
                ViewModel.propertyAssignment = css.PropertyAssignments.Find(assignmentid);
                ViewModel.TotalBalanceDue = css.usp_ClaimTotalBalanceDueGet(claimId).First().Value;
            }
            catch (Exception ex)
            {
                string err = ex.Message;
            }
            //ViewModel.invoiceDetail = css.usp_PropertyInvoiceGetDetails(assignmentid, invoiceId).First();
            return PartialView("_PaymentsDetails", ViewModel);
        }

        public ActionResult PaymentDetailsInsert(Int64 PaymentId, Int64 InvoiceId, DateTime ReceivedDate, float AmountReceived, string CheckNumber)
        {
            string valueToReturn = "0";
            PropertyInvoiceViewModel ViewModel = new PropertyInvoiceViewModel();
            css = new ChoiceSolutionsEntities();
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

                var Assignmentid = css.PropertyInvoices.Where(x => x.InvoiceId == InvoiceId).First().AssignmentId;
                PropertyAssignment pa = css.PropertyAssignments.Find(Assignmentid);
                if (PaymentId == 0)
                {
                    ObjectParameter outPaymentId = new ObjectParameter("PaymentId", DbType.Int64);
                    css.usp_paymentInsert(outPaymentId, InvoiceId, ReceivedDate, AmountReceived, CheckNumber, loggedInUser.UserId, System.DateTime.Now);
                    PaymentId = Convert.ToInt64(outPaymentId.Value);
                }
                else
                {
                    css.usp_paymentUpdate(PaymentId, InvoiceId, ReceivedDate, AmountReceived, CheckNumber, loggedInUser.UserId, System.DateTime.Now);
                }
                css.usp_balanceDueUpdate(InvoiceId, AmountReceived);
                css.usp_AutoCloseInvoice(InvoiceId);
                //ViewModel.invoiceDetail = css.usp_PropertyInvoiceGetDetails(Assignmentid, InvoiceId).First();
                //ViewModel.paymentslist = css.usp_PaymentGetList(Assignmentid).ToList();
                Int64 taskPaymentId = PaymentId;
                Task.Factory.StartNew(() =>
                {
                    Utility.QBUpdatePayment(taskPaymentId);
                }, TaskCreationOptions.LongRunning);

                valueToReturn = "1";
            }
            catch (Exception ex)
            {
                valueToReturn = ex.Message + " " + ex.InnerException != null ? ex.InnerException.Message : "";
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
            //return PartialView("_PaymentList", ViewModel);
            //return RedirectToAction("GetPaymentDetails", new { assignmentid = ViewModel.invoiceDetail.AssignmentId});
        }
        public ActionResult DeletePaymentDetail(Int64 PaymentId, Int64 InvoiceId)
        {
            PropertyInvoiceViewModel ViewModel = new PropertyInvoiceViewModel();
            css = new ChoiceSolutionsEntities();
            try
            {
                CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
                Int64 Assignmentid = css.PropertyInvoices.Where(x => x.InvoiceId == InvoiceId).First().AssignmentId.Value;
                string qbPaymentId = css.Payments.Find(PaymentId).QBPaymentId;
                css.usp_PaymentDelete(PaymentId,loggedInUser.UserId);
                //css.usp_balanceDueUpdate(InvoiceId);
                ViewModel.invoiceDetail = css.usp_PropertyInvoiceGetDetails(Assignmentid, InvoiceId).First();
                ViewModel.paymentslist = css.usp_PaymentGetList(InvoiceId).ToList();
                if (!String.IsNullOrEmpty(qbPaymentId))
                {
                    Task.Factory.StartNew(() =>
                    {
                        Utility.QBDeletePayment(qbPaymentId);
                    }, TaskCreationOptions.LongRunning);

                }
                return Json(1);
            }
            catch (Exception ex)
            { return Json(0); }

        }

        public ActionResult CloseInvoice(Int64 InvoiceId, string Comment, Int32 Reason, bool IsLossIncurred, Byte SpLossPercent, bool IsInvoiceClose)
        {
            PropertyInvoiceViewModel ViewModel = new PropertyInvoiceViewModel();
            css = new ChoiceSolutionsEntities();
            try
            {
                css.usp_PropertyInvoiceClose(InvoiceId, Comment, Reason, IsLossIncurred, SpLossPercent, IsInvoiceClose);
            }
            catch (Exception ex)
            { }
            return Json(1);
        }
        [Authorize]
        public JsonResult SPPayrollAndAdjIsClosedUpdate(Int64 PAId)
        {
            int valueToReturn = 0;
            try
            {
                css.usp_SPPayrollAndAdjIsClosedUpdate(PAId, true);
                valueToReturn = 1;

            }
            catch (Exception ex)
            {
                valueToReturn = -1;
            }
            return Json(valueToReturn, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public JsonResult AssignmentInvoiceSummaryList(Int64 claimId, Int64 assignmentId, bool isEditable = false, bool displayHeader = true)
        {

            PropertyInvoiceSummaryListViewModel viewModel = new PropertyInvoiceSummaryListViewModel();

            try
            {
                viewModel.AssignmentId = assignmentId;
                viewModel.IsEditable = isEditable;
                viewModel.DisplayHeader = displayHeader;
                List<usp_PropertyInvoiceSummaryGetList_Result> result = new List<usp_PropertyInvoiceSummaryGetList_Result>();
                result = css.usp_PropertyInvoiceSummaryGetList(claimId).ToList();
                viewModel.PropertyInvoiceSummaryList = result;
                viewModel.ClaimNumber = css.Claims.Find(claimId).ClaimNumber;

            }
            catch (Exception ex)
            {

            }
            return Json(RenderPartialViewToString("_AssignmentInvoiceSummaryList", viewModel), JsonRequestBehavior.AllowGet);
        }
        private InvoiceBilledNotesViewModel getInvoiceBilledNotesViewModel(Int64 claimId, Int64 invoiceId)
        {
            InvoiceBilledNotesViewModel viewModel = new BLL.ViewModels.InvoiceBilledNotesViewModel();

            viewModel.NotesList = css.usp_BilledNotesForInvoiceExportGetList(claimId, invoiceId).ToList();
            viewModel.InvoiceId = invoiceId;
            viewModel.TotalHours = viewModel.NotesList.Sum(x => x.Hours.HasValue?x.Hours.Value:0);

            if (claimId > 0)
            {
                //When a new invoice is being created make use of ClaimId as an invoice does not exist in the table yet
                viewModel.ClaimId = claimId;
                viewModel.ClaimNumber = css.Claims.Find(claimId).ClaimNumber;
                viewModel.InvoiceNumber = "INVC-" + invoiceId;
            }
            else
            {
                //An existing invoice exists
                Int64 assignmentId = css.PropertyInvoices.Find(invoiceId).AssignmentId.Value;
                viewModel.ClaimId = css.PropertyAssignments.Find(assignmentId).ClaimId.Value;
                viewModel.ClaimNumber = css.Claims.Find(viewModel.ClaimId).ClaimNumber;
                viewModel.InvoiceNumber = css.PropertyInvoices.Find(invoiceId).InvoiceNo;
            }
            return viewModel;
        }
        public ActionResult InvoiceBilledNotesList(Int64 claimId, Int64 invoiceId)
        {

            InvoiceBilledNotesViewModel viewModel = getInvoiceBilledNotesViewModel(claimId, invoiceId);

            string html = RenderPartialViewToString("_InvoiceBilledNotes", viewModel);
            string thisPageURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;

            byte[] pdfData = Utility.ConvertHTMLStringToPDFwithImages(html, thisPageURL, 0, 0, 0, 0, false, string.Empty, 0, false, string.Empty);

            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=BilledNotes_" + viewModel.InvoiceNumber + ".pdf");
            Response.ContentType = "application/pdf";
            Response.OutputStream.Write(pdfData, 0, pdfData.Length);
            Response.Flush();
            Response.End();

            return View();
        }

    }
}
