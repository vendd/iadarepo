﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BLL.ViewModels;
using System.Data;
using System.Data.Objects;
using System.Web.UI;

namespace CSS.Controllers
{
    public class NuGenLineOfBusinessController:Controller
    {
        BLL.ChoiceSolutionsEntities css = new BLL.ChoiceSolutionsEntities();
        public ActionResult Index()
        {
            return View();
        }
        private RequirementViewModel getCompanyLevelRequirementsViewModel(int companyId)
        {
            //Display Default Company level Requirements
            RequirementViewModel viewModel = new RequirementViewModel();
            List<ClaimRequirement> lstClmRequirements = css.ClaimRequirements.Where(a => (a.HeadCompanyId == companyId) && (a.AssignmentId == null) && (a.LOBId == null)).ToList();
            if (lstClmRequirements.Count > 0)
            {
                ClaimRequirement Requirementdetails = lstClmRequirements[0];

                if (Requirementdetails.SoftwareRequired.HasValue)
                {
                    viewModel.SoftwareRequired = Convert.ToInt32(Requirementdetails.SoftwareRequired);
                }
                if (Requirementdetails.ContactWithin.HasValue)
                {
                    viewModel.ContactWithin = Convert.ToInt32(Requirementdetails.ContactWithin);
                }
                if (Requirementdetails.InspectWithin.HasValue)
                {
                    viewModel.InspectWithin = Convert.ToInt32(Requirementdetails.InspectWithin);
                }
                if (Requirementdetails.FirstReportDue.HasValue)
                {
                    viewModel.FirstReportDue = Convert.ToInt32(Requirementdetails.FirstReportDue);
                }
                if (Requirementdetails.ReservesDue.HasValue)
                {
                    viewModel.ReservesDue = Convert.ToInt32(Requirementdetails.ReservesDue);
                }
                if (Requirementdetails.SatusReports.HasValue)
                {
                    viewModel.SatusReports = Convert.ToInt32(Requirementdetails.SatusReports);
                }
                if (Requirementdetails.FinalReportDue.HasValue)
                {
                    viewModel.FinalReportDue = Convert.ToInt32(Requirementdetails.FinalReportDue);
                }
                if (Requirementdetails.BaseServiceCharges.HasValue)
                {
                    viewModel.BaseServiceCharges = Convert.ToInt32(Requirementdetails.BaseServiceCharges);
                }
                if (Requirementdetails.InsuredToValueRequired.HasValue)
                {
                    viewModel.InsuredToValueRequired = Convert.ToInt32(Requirementdetails.InsuredToValueRequired);
                }
                if (Requirementdetails.NeighborhoodCanvas.HasValue)
                {
                    viewModel.NeighborhoodCanvas = Convert.ToInt32(Requirementdetails.NeighborhoodCanvas);
                }
                if (Requirementdetails.ITELRequired.HasValue)
                {
                    viewModel.ITELRequired = Convert.ToInt32(Requirementdetails.ITELRequired);
                }
                if (Requirementdetails.MinimumCharges.HasValue)
                {
                    viewModel.MinimumCharges = Convert.ToInt32(Requirementdetails.MinimumCharges);
                }
                if (Requirementdetails.RequiredDocuments != null)
                {
                    viewModel.RequiredDocuments = Requirementdetails.RequiredDocuments;
                }
                if (Requirementdetails.EstimateContents.HasValue)
                {
                    viewModel.EstimateContents = Convert.ToInt32(Requirementdetails.EstimateContents);
                }
                if (Requirementdetails.ObtainAgreedSorP.HasValue)
                {
                    viewModel.ObtainAgreedSorP = Convert.ToInt32(Requirementdetails.ObtainAgreedSorP);
                }
                if (Requirementdetails.DiscussCoveragNScopewithClaimant.HasValue)
                {
                    viewModel.DiscussCoveragNScopewithClaimant = Convert.ToInt32(Requirementdetails.DiscussCoveragNScopewithClaimant);
                }
                if (Requirementdetails.RoofSketchRequired.HasValue)
                {
                    viewModel.RoofSketchRequired = Convert.ToInt32(Requirementdetails.RoofSketchRequired);
                }
                if (Requirementdetails.PhotoSGorPGRequired.HasValue)
                {
                    viewModel.PhotoSGorPGRequired = Convert.ToInt32(Requirementdetails.PhotoSGorPGRequired);
                }
                if (Requirementdetails.DepreciacionType.HasValue)
                {
                    viewModel.DepreciacionType = Convert.ToInt32(Requirementdetails.DepreciacionType);
                }
                if (Requirementdetails.MaxDepr.HasValue)
                {
                    viewModel.MaxDepr = Convert.ToInt32(Requirementdetails.MaxDepr);
                }
                if (Requirementdetails.AppOfOveheadNProfit.HasValue)
                {
                    viewModel.AppOfOveheadNProfit = Convert.ToInt32(Requirementdetails.AppOfOveheadNProfit);
                }
                if (Requirementdetails.Comments != null)
                {
                    viewModel.Comments = Requirementdetails.Comments;
                }
            }
            return viewModel;
        }
        public ActionResult NuGenLineOfBusiness(Int32 ServiceTypeId, Int32 LOBId)
        {

            NuGenLineOfBusinessViewModel viewmodel = new NuGenLineOfBusinessViewModel(ServiceTypeId);
            try
            {
                if (LOBId == -1)
                {
                    viewmodel.LOBGetList = css.ServiceTypewiseLineOfBusinessGetList(ServiceTypeId).ToList();
                    viewmodel.LOB.LOBId = 0;
                    
                    
                    viewmodel.RequirementsViewModel = getCompanyLevelRequirementsViewModel(0);//Set temporarily to zero . It is from previous development

                }
                else
                {
                    viewmodel = new NuGenLineOfBusinessViewModel(ServiceTypeId, LOBId);
                    viewmodel.LOBGetList = css.ServiceTypewiseLineOfBusinessGetList(ServiceTypeId).ToList();
                    viewmodel.LOB.LOBId = LOBId;
                    viewmodel.HasBridge = viewmodel.LOB.DenyBridgeFlag ?? false;
                    //Display Default Company level Requirements
                    viewmodel.RequirementsViewModel = new RequirementViewModel();
                    List<ClaimRequirement> lstClmRequirements = css.ClaimRequirements.Where(a => (a.HeadCompanyId == null) && (a.AssignmentId == null) && (a.LOBId == LOBId)).ToList();
                    if (lstClmRequirements.Count > 0)
                    {
                        ClaimRequirement Requirementdetails = lstClmRequirements[0];
                        viewmodel.RequirementsViewModel.ClaimRequirementId = Convert.ToInt64(Requirementdetails.ClaimRequirementId);
                        if (Requirementdetails.SoftwareRequired.HasValue)
                        {
                            viewmodel.RequirementsViewModel.SoftwareRequired = Convert.ToInt32(Requirementdetails.SoftwareRequired);
                        }
                        if (Requirementdetails.ContactWithin.HasValue)
                        {
                            viewmodel.RequirementsViewModel.ContactWithin = Convert.ToInt32(Requirementdetails.ContactWithin);
                        }
                        if (Requirementdetails.InspectWithin.HasValue)
                        {
                            viewmodel.RequirementsViewModel.InspectWithin = Convert.ToInt32(Requirementdetails.InspectWithin);
                        }
                        if (Requirementdetails.FirstReportDue.HasValue)
                        {
                            viewmodel.RequirementsViewModel.FirstReportDue = Convert.ToInt32(Requirementdetails.FirstReportDue);
                        }
                        if (Requirementdetails.ReservesDue.HasValue)
                        {
                            viewmodel.RequirementsViewModel.ReservesDue = Convert.ToInt32(Requirementdetails.ReservesDue);
                        }
                        if (Requirementdetails.SatusReports.HasValue)
                        {
                            viewmodel.RequirementsViewModel.SatusReports = Convert.ToInt32(Requirementdetails.SatusReports);
                        }
                        if (Requirementdetails.FinalReportDue.HasValue)
                        {
                            viewmodel.RequirementsViewModel.FinalReportDue = Convert.ToInt32(Requirementdetails.FinalReportDue);
                        }
                        if (Requirementdetails.BaseServiceCharges.HasValue)
                        {
                            viewmodel.RequirementsViewModel.BaseServiceCharges = Convert.ToInt32(Requirementdetails.BaseServiceCharges);
                        }
                        if (Requirementdetails.InsuredToValueRequired.HasValue)
                        {
                            viewmodel.RequirementsViewModel.InsuredToValueRequired = Convert.ToInt32(Requirementdetails.InsuredToValueRequired);
                        }
                        if (Requirementdetails.NeighborhoodCanvas.HasValue)
                        {
                            viewmodel.RequirementsViewModel.NeighborhoodCanvas = Convert.ToInt32(Requirementdetails.NeighborhoodCanvas);
                        }
                        if (Requirementdetails.ITELRequired.HasValue)
                        {
                            viewmodel.RequirementsViewModel.ITELRequired = Convert.ToInt32(Requirementdetails.ITELRequired);
                        }
                        if (Requirementdetails.MinimumCharges.HasValue)
                        {
                            viewmodel.RequirementsViewModel.MinimumCharges = Convert.ToInt32(Requirementdetails.MinimumCharges);
                        }
                        if (Requirementdetails.RequiredDocuments != null)
                        {
                            viewmodel.RequirementsViewModel.RequiredDocuments = Requirementdetails.RequiredDocuments;
                        }
                        if (Requirementdetails.EstimateContents.HasValue)
                        {
                            viewmodel.RequirementsViewModel.EstimateContents = Convert.ToInt32(Requirementdetails.EstimateContents);
                        }
                        if (Requirementdetails.ObtainAgreedSorP.HasValue)
                        {
                            viewmodel.RequirementsViewModel.ObtainAgreedSorP = Convert.ToInt32(Requirementdetails.ObtainAgreedSorP);
                        }
                        if (Requirementdetails.DiscussCoveragNScopewithClaimant.HasValue)
                        {
                            viewmodel.RequirementsViewModel.DiscussCoveragNScopewithClaimant = Convert.ToInt32(Requirementdetails.DiscussCoveragNScopewithClaimant);
                        }
                        if (Requirementdetails.RoofSketchRequired.HasValue)
                        {
                            viewmodel.RequirementsViewModel.RoofSketchRequired = Convert.ToInt32(Requirementdetails.RoofSketchRequired);
                        }
                        if (Requirementdetails.PhotoSGorPGRequired.HasValue)
                        {
                            viewmodel.RequirementsViewModel.PhotoSGorPGRequired = Convert.ToInt32(Requirementdetails.PhotoSGorPGRequired);
                        }
                        if (Requirementdetails.DepreciacionType.HasValue)
                        {
                            viewmodel.RequirementsViewModel.DepreciacionType = Convert.ToInt32(Requirementdetails.DepreciacionType);
                        }
                        if (Requirementdetails.MaxDepr.HasValue)
                        {
                            viewmodel.RequirementsViewModel.MaxDepr = Convert.ToInt32(Requirementdetails.MaxDepr);
                        }
                        if (Requirementdetails.AppOfOveheadNProfit.HasValue)
                        {
                            viewmodel.RequirementsViewModel.AppOfOveheadNProfit = Convert.ToInt32(Requirementdetails.AppOfOveheadNProfit);
                        }
                        if (Requirementdetails.Comments != null)
                        {
                            viewmodel.RequirementsViewModel.Comments = Requirementdetails.Comments;
                        }
                    }
                }
                viewmodel.ServiceTypeId = ServiceTypeId;
            }
            catch (Exception ex)
            {

            }
            return View(viewmodel);


        }

        [HttpPost]
        public ActionResult NuGenLineOfBusiness(NuGenLineOfBusinessViewModel viewmodel, RequirementViewModel requirementsViewModel, FormCollection form)
        {
            try
            {
                if (isFormValid(viewmodel) == true)
                {
                    if (viewmodel.LOB.LOBId <= 0)
                    {
                        ObjectParameter outLOBIdId = new ObjectParameter("LOBId", DbType.Int32);
                        css.NuGenLineOfBusinessInsert(outLOBIdId, null, viewmodel.LOB.LOBType, viewmodel.LOB.CatType, viewmodel.LOB.Other, viewmodel.ServiceTypeId, viewmodel.LOB.StandardAdjuster, viewmodel.LOB.GeneralAdjuster, viewmodel.LOB.ExecutiveGeneralAdjuster, viewmodel.LOB.FreeMiles, viewmodel.LOB.RatePerMile, viewmodel.LOB.NoOfPhotosIncluded, viewmodel.LOB.RatePerPhoto, viewmodel.LOB.TEMonthlyBillingDay, viewmodel.HasBridge);
                        if (Convert.ToInt32(outLOBIdId.Value) > 0)
                        {
                            viewmodel.LOB.LOBId = Convert.ToInt32(outLOBIdId.Value);
                        }
                        else
                        {
                            TempData["Error"] = "Line Of Business Description already exists.";
                        }
                    }
                    else if (viewmodel.LOB.LOBId > 0)
                    {
                        css.NuGenLineOfBusinessUpdate(viewmodel.LOB.LOBId, null, viewmodel.LOB.LOBType, viewmodel.LOB.CatType, viewmodel.LOB.Other, viewmodel.ServiceTypeId, viewmodel.LOB.StandardAdjuster, viewmodel.LOB.GeneralAdjuster, viewmodel.LOB.ExecutiveGeneralAdjuster, viewmodel.LOB.FreeMiles, viewmodel.LOB.RatePerMile, viewmodel.LOB.NoOfPhotosIncluded, viewmodel.LOB.RatePerPhoto, viewmodel.LOB.TEMonthlyBillingDay, viewmodel.HasBridge);
                    }


                    if (viewmodel.LOB.LOBId > 0) //viewmodel.LOB.LOBId will contain a positive value incase the Insert/Update action was successful
                    {
                        //Insert/Update Requirements
                        css.usp_insertClaimRequirements(requirementsViewModel.ClaimRequirementId, null, viewmodel.LOB.LOBId, null, requirementsViewModel.SoftwareRequired, requirementsViewModel.ContactWithin,
                        requirementsViewModel.InspectWithin, requirementsViewModel.FirstReportDue, requirementsViewModel.ReservesDue, requirementsViewModel.SatusReports, requirementsViewModel.FinalReportDue,
                        requirementsViewModel.BaseServiceCharges, requirementsViewModel.InsuredToValueRequired, requirementsViewModel.NeighborhoodCanvas, requirementsViewModel.ITELRequired, requirementsViewModel.MinimumCharges,
                        requirementsViewModel.RequiredDocuments, requirementsViewModel.EstimateContents, requirementsViewModel.ObtainAgreedSorP, requirementsViewModel.DiscussCoveragNScopewithClaimant,
                        requirementsViewModel.RoofSketchRequired, requirementsViewModel.PhotoSGorPGRequired, requirementsViewModel.DepreciacionType, requirementsViewModel.MaxDepr, requirementsViewModel.AppOfOveheadNProfit,
                        requirementsViewModel.Comments);
                    }
                }
                else
                {
                    viewmodel.populateLists();
                    viewmodel.LOBGetList = css.ServiceTypewiseLineOfBusinessGetList(viewmodel.ServiceTypeId).ToList();
                    viewmodel.RequirementsViewModel = requirementsViewModel;
                    return View(viewmodel);
                }
            }
            catch (Exception ex)
            {

            }

            return RedirectToAction("NuGenLineOfBusiness", "NuGenLineOfBusiness", new { ServiceTypeId = viewmodel.ServiceTypeId, LOBId = -1 });
        }
        private bool isFormValid(NuGenLineOfBusinessViewModel viewModel)
        {
            bool valueToReturn = true;
            if (viewModel.LOB.TEMonthlyBillingDay.HasValue)
            {
                if (viewModel.LOB.TEMonthlyBillingDay.Value < 1 || viewModel.LOB.TEMonthlyBillingDay.Value > 30)
                {
                    ModelState.AddModelError("NuGenLineOfBusinessViewModel.LOB.TEMonthlyBillingDay", "T&E Monthly Billing Day is invalid.");
                    valueToReturn = false;
                }
                //LOBPricing should not contain non Time and Expense Pricing if T&E monthly billing day is specified
                if (css.CompanyLOBPricings.Where(x => x.LOBId == viewModel.LOB.LOBId && (x.IsTimeNExpance ?? false) == false).ToList().Count > 0)
                {
                    ModelState.AddModelError("NuGenLineOfBusinessViewModel.LOB.TEMonthlyBillingDay", "T&E Monthly Billing Day cannot be specified as the LOB Pricing includes non T&E pricing.");
                    valueToReturn = false;
                }
            }
            return valueToReturn;
        }
        public ActionResult NuGenLineOfBusinessDelete(NuGenLineOfBusinessViewModel viewmodel, FormCollection form, Int64 ServiceTypeId, Int32 LOBId)
        {
            try
            {

                if (LOBId > 0)
                {

                    ObjectParameter outResult = new ObjectParameter("Result", DbType.Int32);
                    css.LineOfBusinessDelete(LOBId, outResult);
                    int lobid = Convert.ToInt32(outResult.Value);
                    if (lobid <= 0)
                    {
                        TempData["Error"] = "Cannot delete this LOB as Pricing exists for this LOB.";



                    }

                }
            }
            catch (Exception ex)
            {

            }
            return RedirectToAction("NuGenLineOfBusiness", "NuGenLineOfBusiness", new { ServiceTypeId = ServiceTypeId, LOBId = -1 });



        }
    }
}