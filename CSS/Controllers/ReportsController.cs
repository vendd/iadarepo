﻿using BLL;
using BLL.Models;
using BLL.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CSS.Controllers
{
    public class ReportsController : Controller
    {
        BLL.ChoiceSolutionsEntities css = new BLL.ChoiceSolutionsEntities();
        #region  action methods
        //[Authorize]
        [HttpGet]
        public ActionResult InvoiceTrackerReport()
        {
            string reportURL = "";
            InvoiceTrackerViewModel Model = new InvoiceTrackerViewModel();
            Model.RptFilterDisplay = true;
            Model.RptViewerDisplay = false;
            Model.ReportViewer = new ReportViewer() { ReportTitle = "Report Preview", ReportURL = reportURL, BackButtonDisplay = false };

            return View(Model);
        }

        //[Authorize]
        [HttpPost]
        public ActionResult InvoiceTrackerReport(InvoiceTrackerViewModel viewModel)
        {
            string urlParameters = "";
            viewModel.ReportURL = "~/Reports/InvoiceTrackerReport.aspx";
            urlParameters = "InsuranceCompany=" + viewModel.InsuranceCompany;
            urlParameters += "&ClaimRepresentativeID=" + viewModel.ClaimRepresentativeID;
            urlParameters += "&StartDate=" + (viewModel.StartDate.HasValue?viewModel.StartDate.Value.ToString("MM-dd-yyyy"):null);
            urlParameters += "&EndDate=" + (viewModel.EndDate.HasValue?viewModel.EndDate.Value.ToString("MM-dd-yyyy"):null);
            urlParameters += "&AssignmentMonth=" + (viewModel.AssignmentMonth.HasValue ? viewModel.AssignmentMonth.Value.Month.ToString() : null);
            urlParameters += "&AssignmentYear=" + (viewModel.AssignmentYear.HasValue ? viewModel.AssignmentYear.Value.Year.ToString() : null);
            viewModel.ReportURL += "?" + urlParameters;

            viewModel.RptFilterDisplay = false;
            viewModel.RptViewerDisplay = true;
            viewModel.ReportViewer = new ReportViewer() { ReportTitle = "Report Preview", ReportURL = viewModel.ReportURL, BackButtonDisplay = true };
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult GetClaimRepresentativeList(int? CompanyId)
        {
            int UserTypeId = 2; //2 is admin 
            List<SelectListItem> ClaimRepresentativeList = new List<SelectListItem>();
            //ClaimRepresentativeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            if (CompanyId != null)
            {
                List<ClaimRepresentativeGetList_Result> ClaimRepreList = css.ClaimRepresentativeGetList(UserTypeId, CompanyId).ToList();
                ClaimRepresentativeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                foreach (var ClaimRepresentative in ClaimRepreList)
                {
                    ClaimRepresentativeList.Add(new SelectListItem { Text = ClaimRepresentative.UserFullName, Value = ClaimRepresentative.UserId + "" });
                }
                //if (ClaimRepreList.Count == 0)
                //{
                //    ClaimRepresentativeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                //}
            }
            return Json(ClaimRepresentativeList);
        }


        [Authorize]
        public ActionResult PayrollSummaryReport()
        {
            PayrollSummaryReportViewModel viewModel = getPayrollSummaryReportViewModel(DateTime.Now, 0,0);

            return View(viewModel);
        }
        [Authorize]
        [HttpPost]
        public ActionResult PayrollSummaryReport(PayrollSummaryReportViewModel viewModel)
        {
            if (viewModel.EndDate == DateTime.MinValue)
            {
                viewModel.EndDate = DateTime.Now;
            }
            viewModel = getPayrollSummaryReportViewModel(viewModel.EndDate, viewModel.UserTypeId,viewModel.ServiceTypeID);
            return View(viewModel);
        }
        private PayrollSummaryReportViewModel getPayrollSummaryReportViewModel(DateTime invoiceEndDate, int userTypeId,int ServiceTypeId)
        {
            PayrollSummaryReportViewModel viewModel = new PayrollSummaryReportViewModel();
            viewModel.EndDate = invoiceEndDate;
            viewModel.UserTypeId = userTypeId;
            List<usp_PayrollSummaryMasterReport_Result> masterRecords = css.usp_PayrollSummaryMasterReport(invoiceEndDate, userTypeId, ServiceTypeId).ToList();
            List<PayrollSummaryReportModel> report = new List<PayrollSummaryReportModel>();
            viewModel.PayrollSummaryReport = report;
            foreach (var master in masterRecords)
            {
                PayrollSummaryReportModel record = new PayrollSummaryReportModel();
                record.Master = master;
                record.Detail = css.usp_PayrollSummaryDetailReport(master.Companyid, invoiceEndDate, ServiceTypeId).ToList();
                
                if (master.PaymentAmount >= 0)
                {
                    record.isSelectable = true;
                }
                report.Add(record);
            }
            return viewModel;
        }
        [Authorize]
        [HttpPost]
        public ActionResult PayrollSummaryReportProcess(PayrollSummaryReportViewModel viewModel)
        {
            ObjectResult<DateTime?> cstLocalTime = css.usp_GetLocalDateTime();
            DateTime payrollDateTime = cstLocalTime.First().Value;
            payrollDateTime = new DateTime(payrollDateTime.Year, payrollDateTime.Month, payrollDateTime.Day, payrollDateTime.Hour, payrollDateTime.Minute, 0);//retain time info till minute
            string strAccessToken = "";
            foreach (var master in viewModel.PayrollSummaryReport.Where(x => x.isSelected == true))
            {   
                foreach (var detail in master.Detail.Where(x => x.isAssignmentSelected == true))
                {
                    css.usp_SPPayrollAndAdjustmentsReleasePayment(detail.PAId, detail.PaymentType, payrollDateTime);

                //markper
                // List<usp_PayrollSummaryDetailReport_Result> taskDetail = master.Detail;
                List<usp_PayrollSummaryDetailReport_Result> taskDetail = new List<usp_PayrollSummaryDetailReport_Result>();
                decimal taskUserPaymentAmount = 0;

                    if (detail.isAssignmentSelected)
                    {
                        detail.InvoiceNo = detail.ClaimNumber + " " + detail.InvoiceNo;
                        taskDetail.Add(detail);
                        taskUserPaymentAmount += detail.PaymentAmount;
                    }
                    Int64 taskUserId = Convert.ToInt64(detail.SPID);
                    DateTime taskEndDate = viewModel.EndDate;
                    DateTime taskPayrollDateTime = payrollDateTime;

                    try
                    {
                        //Check for OAuth acceessKeyToken

                        //Set first parameter flat QBManageTokens stored procedure as
                        //0. false -> Get
                        //1. true  -> Update

                    QBManageTokens_Result objQBManageToken = css.QBManageTokens(false, null, null).FirstOrDefault();
                    if(objQBManageToken != null)
                    {
                        if (string.IsNullOrEmpty(objQBManageToken.AccessToken))
                        {
                            AccountController obj = new AccountController();
                            strAccessToken = obj.setQuickBookDetail();
                            
                            if(strAccessToken.Contains("Error"))
                            {
                                Utility.LogException("Reports/PayrollSummaryReportProcess", new Exception(), taskUserId + "|" + taskUserPaymentAmount + "|" + strAccessToken);
                            }
            
                            //if (HttpContext.Session["QuickBookDetails"] != null)
                            //{
                            //    QuickBookDetails obj_QuickBookDetail = new QuickBookDetails();
                            //    obj_QuickBookDetail = (QuickBookDetails)System.Web.HttpContext.Current.Session["QuickBookDetails"];
                            //    strAccessToken = obj_QuickBookDetail.accessToken;
                            //    HttpContext.Session["QuickBookDetails"] = obj_QuickBookDetail;
                            //}
                        }
                        else
                        {
                            strAccessToken = objQBManageToken.AccessToken;
                        }
                    }

                    ChoiceSolutionsEntities taskCSS = new ChoiceSolutionsEntities();
                    string qbBillId = Utility.QBUpdateBill(taskUserId, taskUserPaymentAmount, taskPayrollDateTime, taskEndDate, taskDetail, strAccessToken);                    
                                        
                    //foreach (var detail in taskDetail)
                    //{
                    //    taskCSS.usp_SPPayrollAndAdjUpdateQBBillId(detail.PAId, detail.PaymentType, qbBillId);
                    //}
                }
                catch (Exception ex)
                {
                    Utility.LogException("Reports/PayrollSummaryReportProcess", ex, taskUserId + "|" + taskUserPaymentAmount);
                    }
                }
            }
            return View("PayrollSummaryReportSuccess", payrollDateTime);
        }
        [Authorize]
        public ActionResult PayrollSummaryReportSuccess()
        {
            return View();
        }
        [Authorize]
        public ActionResult PayrollHistoryReport()
        {
            ProcessedPayrollReportViewModel viewModel = new ProcessedPayrollReportViewModel();

            viewModel.EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
            viewModel.StartDate = viewModel.EndDate.Value.AddMonths(-6);
            //viewModel.adjusterType = AdjusterType.IA;
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            Int64? spId = null;
            if (loggedInUser.UserTypeId == 1)
            {
                spId = loggedInUser.UserId;
            }
            ObjectResult<DateTime?> payrollDateList = css.usp_ProcessedPayrollDateHistoryGetList(spId, viewModel.StartDate, viewModel.EndDate);
            List<string> model = new List<string>();
            foreach (var payrollDate in payrollDateList)
            {
                model.Add(payrollDate.Value.ToString("MM/dd/yyyy hh:mm tt"));
            }
            viewModel.PayrollDates = model;
            return View(viewModel);
        }
        [Authorize]
        [HttpPost]
        public ActionResult PayrollHistoryReport(ProcessedPayrollReportViewModel viewModel)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            Int64? spId = null;
            if (loggedInUser.UserTypeId == 1)
            {
                spId = loggedInUser.UserId;
            }
          
           // viewModel = new ProcessedPayrollReportViewModel();
            ObjectResult<DateTime?> payrollDateList = css.usp_ProcessedPayrollDateHistoryGetList(spId, viewModel.StartDate, viewModel.EndDate);
            List<string> model = new List<string>();
            foreach (var payrollDate in payrollDateList)
            {
                model.Add(payrollDate.Value.ToString("MM/dd/yyyy hh:mm tt"));
            }
            viewModel.PayrollDates = model;
            return View(viewModel);
        }


        [Authorize]
        public ActionResult BulkInvoiceHistoryReport()
        {
            BulkInvoiceHistoryReportViewModel viewModel = new BulkInvoiceHistoryReportViewModel();

            viewModel.EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
            viewModel.StartDate = viewModel.EndDate.Value.AddMonths(-6);
            //viewModel.adjusterType = AdjusterType.IA;
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            Int64? spId = null;
            if (loggedInUser.UserTypeId == 1)
            {
                spId = loggedInUser.UserId;
            }
            ObjectResult<DateTime?> BulkInvoiceDateList = css.usp_BulkInvoiceHistoryGetList(spId, viewModel.StartDate, viewModel.EndDate);
            List<string> model = new List<string>();
            foreach (var BulkInvoiceDate in BulkInvoiceDateList)
            {
                model.Add(BulkInvoiceDate.Value.ToString("MM/dd/yyyy hh:mm tt"));
            }
            viewModel.BulkInvoiceHistoryDates = model;
            return View(viewModel);
        }
        [Authorize]
        [HttpPost]
        public ActionResult BulkInvoiceHistoryReport(BulkInvoiceHistoryReportViewModel viewModel)
        {
            CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();
            Int64? spId = null;
            if (loggedInUser.UserTypeId == 1)
            {
                spId = loggedInUser.UserId;
            }

            // viewModel = new ProcessedPayrollReportViewModel();
            ObjectResult<DateTime?> BulkInvoiceDateList = css.usp_BulkInvoiceHistoryGetList(spId, viewModel.StartDate, viewModel.EndDate);
            List<string> model = new List<string>();
            foreach (var BulkInvoiceDate in BulkInvoiceDateList)
            {
                model.Add(BulkInvoiceDate.Value.ToString("MM/dd/yyyy hh:mm tt"));
            }
            viewModel.BulkInvoiceHistoryDates = model;
            return View(viewModel);
        }
        #endregion

    }
}