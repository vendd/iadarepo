﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using BLL;
using BLL.ViewModels;

namespace CSS.Controllers
{
    public class HomeController : CustomController
    {
        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        CSS.Models.User loggedInUser = CSS.AuthenticationUtility.GetUser();

        public ActionResult Index()
        {
            ViewBag.Message = "Home: Index";
            HomeViewModel viewModel = new HomeViewModel();
            viewModel.Level1GroupId = 0;
            viewModel.Level2GroupId = 1;
            viewModel.Level3GroupId = 2;
            populateHomeViewModelLists(ref viewModel);

            return View(viewModel);
        }

        #region IADA Home Screen
        [System.Web.Mvc.AllowAnonymous]
        public ActionResult Home()
        {

            return View();
        }
         [System.Web.Mvc.AllowAnonymous]
        public ActionResult faq()
        {
            return View("faq/Index");
        }
         [System.Web.Mvc.AllowAnonymous]
         public ActionResult newsEvent()
         {
             return View("iada-news-and-events/Index");
         }
         [System.Web.Mvc.AllowAnonymous]
         public ActionResult contact()
         {
             return View("contact/Index");
         }

         [System.Web.Mvc.AllowAnonymous]
         public ActionResult helpSaveLife()
         {
             return View("help-iada-save-a-life/Index");
         }
         [System.Web.Mvc.AllowAnonymous]
         public ActionResult newIADALogo()
         {
             return View("introducing-the-new-iada-logo/Index");
         }
         [System.Web.Mvc.AllowAnonymous]
         public ActionResult vehicleRepairConf()
         {
             return View("the-47th-iada-vehicle-repair-conference-was-a-huge-success/Index");
         }
       #endregion

    

        private void populateHomeViewModelLists(ref HomeViewModel viewModel)
        {
            viewModel.Level1GroupList = new List<SelectListItem>();
            viewModel.Level3GroupList = viewModel.Level2GroupList = viewModel.Level1GroupList;
            viewModel.Level1GroupList.Add(new SelectListItem() { Value = "0", Text = "Company" });
            viewModel.Level1GroupList.Add(new SelectListItem() { Value = "1", Text = "Internal Contact" });
            viewModel.Level1GroupList.Add(new SelectListItem() { Value = "2", Text = "Service Provider" });


        }
        public ActionResult AutoClaimFilter()
        {
            ViewBag.Message = "Your app description page.";
            XactAutoAssignmentSearchViewModel viewModel = new XactAutoAssignmentSearchViewModel();
            return PartialView("_AutoClaimFilter", viewModel);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }
        public ActionResult DiarySummaryAdmin(HomeViewModel viewModel)
        {
            populateHomeViewModelLists(ref viewModel);
            return Json(RenderPartialViewToString("_DiarySummaryAdminV2", viewModel), JsonRequestBehavior.AllowGet);
        }
        //public ActionResult Contact()
        //{
        //    ViewBag.Message = "Your contact page.";

        //    return View();
        //}

        [System.Web.Mvc.Authorize]
        public ActionResult DiarySummaryOverDueClaimsList(int diaryCategoryId, string diaryCategoryDesc, int headCompanyId, string companyName, Int64 CSSPOCUserId, string CSSPOCName, Int64 SPUserId, string SPName)
        {
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

            DiarySummaryOverDueClaimsListViewModel viewModel = new DiarySummaryOverDueClaimsListViewModel();

            viewModel.DiaryCategoryId = diaryCategoryId;
            viewModel.DiaryCategoryDesc = diaryCategoryDesc;

            viewModel.HeadCompanyId = headCompanyId;
            viewModel.CompanyName = companyName;

            viewModel.CSSPOCUserId = CSSPOCUserId;
            viewModel.CSSPOCName = CSSPOCName;

            viewModel.SPUserId = SPUserId;
            viewModel.SPName = SPName;

            viewModel.ClaimsList = css.usp_DiarySummaryOverDueClaimsList(diaryCategoryId, headCompanyId, CSSPOCUserId, SPUserId);

            return Json(RenderPartialViewToString("_DiarySummaryOverdueClaimsList", viewModel), JsonRequestBehavior.AllowGet);
        }
        public ActionResult DiarySummaryLevelOverDueClaimsList(int diaryCategoryId, string diaryCategoryDesc, byte level1GroupID, string level1Group_Desc, Int64 level1_ID, string level1_Desc, byte level2GroupID, string level2Group_Desc, Int64 level2_ID, string level2_Desc, byte level3GroupID, string level3Group_Desc, Int64 level3_ID, string level3_Desc)
        {
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            DiarySummaryLevelOverDueClaimsListViewModel viewModel = new DiarySummaryLevelOverDueClaimsListViewModel();

            viewModel.DiaryCategoryId = diaryCategoryId;
            viewModel.DiaryCategoryDesc = diaryCategoryDesc;

            viewModel.Level1GroupId = level1GroupID;
            viewModel.Level1Group_Desc = level1Group_Desc;
            viewModel.Level1_ID = level1_ID;
            viewModel.Level1_Desc = level1_Desc;

            viewModel.Level2GroupId = level2GroupID;
            viewModel.Level2Group_Desc = level2Group_Desc;
            viewModel.Level2_ID = level2_ID;
            viewModel.Level2_Desc = level2_Desc;

            viewModel.Level3GroupId = level3GroupID;
            viewModel.Level3Group_Desc = level3Group_Desc;
            viewModel.Level3_ID = level3_ID;
            viewModel.Level3_Desc = level3_Desc;

            viewModel.ClaimsList = css.usp_DiarySummaryLevelOverDueClaimsList(diaryCategoryId, level1GroupID, level1_ID, level2GroupID, level2_ID, level3GroupID, level3_ID);

            return Json(RenderPartialViewToString("_DiarySummaryLevelOverdueClaimsList", viewModel), JsonRequestBehavior.AllowGet);
        }

        public ActionResult NotificationList()
        {
            List<NotificationList_Result> NoteList = css.NotificationList(loggedInUser.UserId).ToList();            
            if (Request.IsAjaxRequest())
            {
                return Json(RenderPartialViewToString("_NotificationList", NoteList), JsonRequestBehavior.AllowGet);
            }
            return View(); 
        }

        public ActionResult NotificationView(Int64 AutoNoteID)
        {
            List<NotificationList_Result> NoteList = css.NotificationList(loggedInUser.UserId).ToList();
            NoteList = NoteList.Where(x => x.AutoNoteID == AutoNoteID).ToList();
            if (Request.IsAjaxRequest())
            {
                return Json(RenderPartialViewToString("_NotificationView", NoteList), JsonRequestBehavior.AllowGet);
            }
            return View();
        }

        public ActionResult NotificationIsViewUpdate(Int64 AutoNoteID)
        {
            css.AutoNotesIsViewUpdate(AutoNoteID);
            if (Request.IsAjaxRequest())
            {
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            return View();
        }       
    }
}
