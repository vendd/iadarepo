//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    
    public partial class usp_PropInvcShortPaymentReport_Result
    {
        public long InvoiceId { get; set; }
        public string CompanyName { get; set; }
        public Nullable<System.DateTime> OriginalInvoiceDate { get; set; }
        public string InvoiceNo { get; set; }
        public string ClaimNumber { get; set; }
        public Nullable<bool> IsInvoiceClose { get; set; }
        public Nullable<decimal> Invoice_Total { get; set; }
        public Nullable<decimal> Payments_Received { get; set; }
        public Nullable<decimal> Short_Payment_Amount { get; set; }
    }
}
