//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    
    public partial class NotificationList_Result
    {
        public long AutoNoteID { get; set; }
        public Nullable<long> AutoAssignmentID { get; set; }
        public string Subject { get; set; }
        public string Comment { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
        public Nullable<long> FromUserId { get; set; }
        public Nullable<long> ToUserId { get; set; }
        public string AutoDocumentsID { get; set; }
        public Nullable<bool> IsSystemGenerated { get; set; }
        public Nullable<long> ClaimID { get; set; }
        public string ClaimNumber { get; set; }
        public string FromUserName { get; set; }
        public string ToUserName { get; set; }
        public string DocumentType { get; set; }
    }
}
