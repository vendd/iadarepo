//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    
    public partial class GetQBBridgeFailedPayments_Result
    {
        public long PaymentId { get; set; }
        public Nullable<long> InvoiceId { get; set; }
        public Nullable<System.DateTime> ReceivedDate { get; set; }
        public Nullable<double> AmountReceived { get; set; }
        public string CheckNumber { get; set; }
        public Nullable<long> AddedBy { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; }
        public string QBPaymentId { get; set; }
    }
}
