﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Models
{
    [Serializable]
    public class PayrollSummaryReportModel
    {
        public bool isSelected { get; set; }
        public bool isSelectable { get; set; }
        public usp_PayrollSummaryMasterReport_Result Master { get; set; }
        public List<usp_PayrollSummaryDetailReport_Result> Detail { get; set; }
    }
}
