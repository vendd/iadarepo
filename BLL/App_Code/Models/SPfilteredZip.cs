﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Models
{
    [Serializable]
    public  class SAfilteredZip
    {
        public List<SMAssignments> SPAssignments;
        public string Zip { get; set; }
        public string Lontitude { get; set; }
        public string Latitude { get; set; }
    }
}
