﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web.Script.Serialization;
namespace BLL.Models
{
    [Serializable]
    public class UserProfile
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public Nullable<int> HeadCompanyId { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Email { get; set; }
        public string Twitter { get; set; }
        public string Facebook { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string MobilePhone { get; set; }
        public Nullable<long> MgrId { get; set; }
        public string imagePath { get; set; }
      
        public virtual Company Company1 { get; set; }
    }
    public class ClaimCompany
    {
        public int CompanyId { get; set; }
        public Nullable<int> CompanyTypeId { get; set; }
        public string CompanyName { get; set; }
        public Nullable<int> HeadCompanyId { get; set; }
        public string LineOfBusiness { get; set; }
        public string TaxID { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string PhoneNumber { get; set; }
        public string WebsiteURL { get; set; }
        public string Logo { get; set; }
        public string FTPAddress { get; set; }
        public string FTPPassword { get; set; }
        public string LetterLibray { get; set; }
        public string SpecialInstructions { get; set; }
        public string NeededDocuments { get; set; }
        public string ContractDocuments { get; set; }
        public string MilestoneDatesComments { get; set; }
        public Nullable<int> ParentCompanyId { get; set; }
        public string CarriedId { get; set; }
        public Nullable<byte> IntegrationTypeId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string SymbilityHeadOfficeCompanyId { get; set; }
        public Nullable<bool> UsesSharedDataset { get; set; }
        public Nullable<long> CRMAccountId { get; set; }
        public string XACTBusinessUnit { get; set; }
        public string QBCustomerId { get; set; }
        public Nullable<bool> EnableFTPDocExport { get; set; }
        public string FTPUserName { get; set; }
        public Nullable<bool> DenyDocumentBridgeFlag { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<bool> IsIadaMember { get; set; }
        public string imagePath { get; set; }
        public string BusinessDesc { get; set; }
        public Nullable<long> IsDefault { get; set; }

    }
}
