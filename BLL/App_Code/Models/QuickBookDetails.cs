﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    [Serializable]
    public class QuickBookDetails
    {
        public string clientId { get; set; }
        public string clientSecret { get; set; }
        public string callbackURL { get; set; }
        public string authorizationCode { get; set; }
        public string realmId { get; set; }
        public string identityToken { get; set; }
        public string refreshToken { get; set; }
        public string accessToken { get; set; }
        public string discoveryAuthority { get; set; }
        public string discoveryUrl { get; set; }
        public string qboBaseUrl { get; set; }
        public string state { get; set; }
        public string error { get; set; }
    }
}
