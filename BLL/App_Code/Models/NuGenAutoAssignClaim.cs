﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.App_Code.Models
{
    class NuGenAutoAssignClaim
    {
        public string CompanyName { get; set; }
        public string ClaimNumber { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string ServiceProvider { get; set; }
        public string Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string DateOfLoss { get; set; }
        public DateTime AssignDate { get; set; }
    }
}
