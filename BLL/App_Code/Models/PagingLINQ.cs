﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.App_Code.Models
{
    [Serializable]
    public class PagingLINQ
    {
        public string FormName { get; set; }
        public int Page { get; set; }
        public string TotalCount { get; set; }
        public string NoOfPages { get; set; }
        public Int16 FirstPageNo { get; set; }
        public Int16 PageCount = 10;
        public Int32 RecsPerPage;
    }
}
