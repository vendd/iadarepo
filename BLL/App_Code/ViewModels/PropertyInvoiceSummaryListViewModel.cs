﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
    [Serializable]
    public class PropertyInvoiceSummaryListViewModel
    {
        public Int64 AssignmentId;
        public string ClaimNumber;
        public bool IsEditable;
        public bool DisplayHeader;
        public List<usp_PropertyInvoiceSummaryGetList_Result> PropertyInvoiceSummaryList = new List<usp_PropertyInvoiceSummaryGetList_Result>();
        public PropertyInvoiceSummaryListViewModel()
        {
        }
    }
}
