﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    public class SPLicenseExpiryReportViewModel
    {
        public string ReportURL { get; set; }
        public string SPName { get; set; }
        public string Status { get; set; }
        public DateTime? FromExpiryDate { get; set; }
        public DateTime? ToExpiryDate { get; set; }

        public List<SelectListItem> StatusList = new List<SelectListItem>();
    }
}
