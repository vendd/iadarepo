﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    public class AutoAssignmentViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

        public Claim claim { get; set; }
        //////////////////////////////////////////////////////////////////////////  new change
        public bool IsEditOrSearchAssign { get; set; }
        public int ServiceTypeId { get; set; }
        ///////////////////////////////////////////////////////////////////////////
      
        public VehicleClaimDetail vehicleClaimDetail { get; set; }
        public CatCodeViewModel CatCodeViewModel { get; set; }
        public ClaimReferrer claimReferrer { get; set; }
        //public int claimId
        //{
        //    get
        //    {
        //        return (int)HttpContext.Current.Session["ClaimId"];
        //    }
        //    set
        //    {
        //        HttpContext.Current.Session["ClaimId"] = value;
        //    }
        //}
        public bool isInsuredInfoSameAsLossInfo { get; set; }
        public bool UseMasterPageLayout { get; set; }
        public bool isRedirectToTriage { get; set; }
        public bool SameAsOwnerAddress { get; set; }
        public bool isNuGenClaim { get; set; }
        public bool isManualInvoiceClaim { get; set; } //INPSW -32
        public bool IsAvsnAssignment { get; set; } //INPSW -32
        public AutoAssignment autoassignment { get; set; }
        public AutoAssignment aua = new AutoAssignment();
        public List<SelectListItem> ApplyDeductibleList = new List<SelectListItem>();
        public List<SelectListItem> DrivableList = new List<SelectListItem>();
        public List<SelectListItem> StateList = new List<SelectListItem>();
        public List<SelectListItem> InsuredStateList = new List<SelectListItem>();
        public List<SelectListItem> CatCodesList = new List<SelectListItem>();
        public List<SelectListItem> SeverityLevelsList = new List<SelectListItem>();
        public List<SelectListItem> ServiceOfferingsList = new List<SelectListItem>();
        public List<SelectListItem> LineOfBusinessList = new List<SelectListItem>();
        public List<SelectListItem> LossTypesList = new List<SelectListItem>();
        public List<SelectListItem> PointofContactList = new List<SelectListItem>();
        public List<SelectListItem> CSSQAAgentList = new List<SelectListItem>();
        public List<SelectListItem> InsuranceCompaniesList = new List<SelectListItem>();
        public List<SelectListItem> ClientPointofContactList = new List<SelectListItem>();
        public List<SelectListItem> CoverageTypesList = new List<SelectListItem>();
        public List<SelectListItem> FileStatusList = new List<SelectListItem>();
        public List<SelectListItem> ReferrerTypeList = new List<SelectListItem>();
        public List<SelectListItem> ClaimTypeList = new List<SelectListItem>();
        public List<SelectListItem> OwnerStateList = new List<SelectListItem>();
        public List<SelectListItem> VehicleLocationList = new List<SelectListItem>();
        public List<SelectListItem> ServiceTypeList = new List<SelectListItem>();
        public List<SelectListItem> ClaimRepresentativeList = new List<SelectListItem>();
        public List<SelectListItem> InternalContactList = new List<SelectListItem>();
        
        public AutoAssignmentViewModel()
        {
            this.claim = new Claim();
            this.claim.ClaimId = -1;
            this.autoassignment = new AutoAssignment();

            //autoassignment.Claim.InsuredLastName = "cjvhs";
            //autoassignment.Claim.InsuredAddress1 = "sdhbvh";
            //autoassignment.Claim.InsuredZip = "123456";
            //autoassignment.Claim.InsuredState = "ABC";
            //autoassignment.Claim.InsuredCity = "XYZ";
            //autoassignment.Claim.SeverityLevel = 2;

            populateLists();
        }

        public AutoAssignmentViewModel(Int64 claimid)
        {
            // this.claimId =(int)claimid;
            claim = css.Claims.Find(claimid);
            #region  //Apply Deductible 
            claimReferrer = (from c in css.ClaimReferrers
                             where c.ClaimId == claim.ClaimId
                             select c).FirstOrDefault();
            #endregion
            

            this.autoassignment = new AutoAssignment();
            //autoassignment = css.AutoAssignments.Find(claimid);
            Int64 autoassignmentID = (from pass in css.AutoAssignments
                                      where pass.ClaimId == claimid
                                      orderby pass.AutoAssignmentId descending
                                      select pass.AutoAssignmentId).First();
            if(autoassignmentID == null || autoassignmentID == 0)
            {
                autoassignment = null;
            }
            else
            {
                autoassignment = css.AutoAssignments.Find(autoassignmentID);
            }
            vehicleClaimDetail = (from v in css.VehicleClaimDetails
                                  where v.AssignmentID == autoassignmentID
                                  select v).FirstOrDefault();
            populateLists();
            foreach (var i in DrivableList)
            {
                if (i.Value.ToString().Substring(0, 1) == autoassignment.IsDriveable) i.Selected = true;
            }
            if (claimReferrer != null)
            {
                foreach (var i in ApplyDeductibleList)
                {
                    if (i.Value.ToString().Substring(0, 1) == claimReferrer.ReferrerDeductibleStatus) i.Selected = true;
                }
            }
            if (vehicleClaimDetail != null)
            {
                foreach (var i in StateList)
                {
                    if (i.Value.ToString().Substring(0, 1) == vehicleClaimDetail.PlateState) i.Selected = true;
                }
            }

        }

        public AutoAssignmentViewModel(AutoAssignmentViewModel viewmodel)
        {
            claim = viewmodel.claim;
            autoassignment = viewmodel.autoassignment;
            populateLists();
        }

        private void populateLists()
        {
            ApplyDeductibleList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (ApplyDeductible e in Enum.GetValues(typeof(ApplyDeductible)))
            {
                ApplyDeductibleList.Add(new SelectListItem { Text = e.ToString(), Value = e.ToString().Substring(0, 1) });
            }

            DrivableList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (Drivable e in Enum.GetValues(typeof(Drivable)))
            {
                DrivableList.Add(new SelectListItem { Text = e.ToString(), Value = e.ToString().Substring(0, 1) });
            }

            //Internal Contact is retrieved dynamically
            InternalContactList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            //Claim Representative is retrieved dynamically
            ClaimRepresentativeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });            

            //Vehicle Location
            VehicleLocationList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach(VehicleLocation vehiclelocation in css.VehicleLocations.ToList())
            {
                VehicleLocationList.Add(new SelectListItem { Text = vehiclelocation.VehicleLocationName, Value = Convert.ToString(vehiclelocation.VehicleLocationID) });
            }

            //Claim Types
            ClaimTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (ClaimType claimtype in css.ClaimTypes.ToList())
            {
                ClaimTypeList.Add(new SelectListItem { Text = claimtype.Description, Value = Convert.ToString(claimtype.ClaimTypeId) });
            }

            //Claim For
            ReferrerTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach(ReferrerType claimfor in css.ReferrerTypes.ToList())
            {
                ReferrerTypeList.Add(new SelectListItem { Text = claimfor.ReferrerType1, Value = Convert.ToString(claimfor.ReferrerTypeId) });
            }


            StateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                //StateList.Add(Convert.ToString(state.StateProvinceCode), state.Name);
                StateList.Add(new SelectListItem { Text = state.Name, Value = Convert.ToString(state.StateProvinceCode).Trim() });
            }

            InsuredStateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                //InsuredStateList.Add(Convert.ToString(state.StateProvinceCode), state.Name);
                InsuredStateList.Add(new SelectListItem { Text = state.Name, Value = Convert.ToString(state.StateProvinceCode).Trim() });
            }

            OwnerStateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                //StateList.Add(Convert.ToString(state.StateProvinceCode), state.Name);
                OwnerStateList.Add(new SelectListItem { Text = state.Name, Value = Convert.ToString(state.StateProvinceCode).Trim() });
            }


            CatCodesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            foreach (CatCode catcode in css.CatCodes.ToList())
            {
                CatCodesList.Add(new SelectListItem { Text = catcode.CatCode1, Value = catcode.CatCode1 });
            }
            //if the CatCode stored in Claims table is not available in the DDL then add it to the list
            if (claim != null)
            {
                if (!String.IsNullOrEmpty(claim.CatCode))
                {
                    if (CatCodesList.Where(x => x.Value == claim.CatCode).ToList().Count == 0)
                    {
                        CatCodesList.Add(new SelectListItem { Text = claim.CatCode, Value = claim.CatCode });
                    }
                }
            }


            SeverityLevelsList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (SeverityLevel severitylevel in css.SeverityLevels.ToList())
            {
                //SeverityLevelsList.Add(severitylevel.SeverityLevelId, severitylevel.SeverityLevel1);
                SeverityLevelsList.Add(new SelectListItem { Text = severitylevel.SeverityLevel1, Value = Convert.ToString(severitylevel.SeverityLevelId) });
            }
            ServiceOfferingsList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (ServiceOffering serviceoffering in css.ServiceOfferings.ToList())
            {
                //ServiceOfferingsList.Add(serviceoffering.ServiceId, serviceoffering.ServiceDescription);
                ServiceOfferingsList.Add(new SelectListItem { Text = serviceoffering.ServiceDescription, Value = Convert.ToString(serviceoffering.ServiceId) });
            }
            LineOfBusinessList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            //List < LineOfBusinessGetList_Result > LObList = css.LineOfBusinessGetList(Claim.HeadCompanyId).ToList();
            //foreach (var lob in LObList)
            //{
            //    LineOfBusinessList.Add(new SelectListItem { Text = lob.LOBDescription, Value = lob.LOBId + "" });
            //}
            //foreach (LineOfBusiness lineofbusiness in css.LineOfBusinesses.ToList())
            //{
            //    //LineOfBusinessList.Add(lineofbusiness.LOBId, lineofbusiness.LOBDescription);
            //    LineOfBusinessList.Add(new SelectListItem { Text = lineofbusiness.LOBDescription, Value = Convert.ToString(lineofbusiness.LOBId) });
            //}
            LossTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (LossType losstype in css.LossTypes.ToList())
            {
                //LossTypesList.Add(losstype.LossTypeId, losstype.LossType1);
                LossTypesList.Add(new SelectListItem { Text = losstype.LossType1, Value = Convert.ToString(losstype.LossTypeId) });
            }
            PointofContactList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            List<CSSPointOfContactList_Result> cssPOCList = css.CSSPointOfContactList().ToList();
            foreach (var poc in cssPOCList)
            {
                //PointofContactList.Add(Convert.ToInt32(p.ID),p.UserType);
                PointofContactList.Add(new SelectListItem { Text = poc.UserFullName, Value = Convert.ToString(poc.UserId) });
            }

            //InsuranceCompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            //List<GetCompaniesList_Result> insuranceCompaniesList = css.GetCompaniesList(1).ToList();
            //foreach (var company in insuranceCompaniesList)
            //{
            //    //PointofContactList.Add(Convert.ToInt32(p.ID),p.UserType);
            //    InsuranceCompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = Convert.ToString(company.CompanyId) });
            //}

            CoverageTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            List<CoverageType> coverageTypesList = css.CoverageTypes.ToList();
            foreach (var coverageType in coverageTypesList)
            {

                CoverageTypesList.Add(new SelectListItem { Text = coverageType.CoverageType1, Value = Convert.ToString(coverageType.CoverageTypeId) });
            }

            //ClientPOC is retrieved dynamically
            ClientPointofContactList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            CSSQAAgentList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            List<GetCSSQAAgentList_Result> cssQAAgentList = css.GetCSSQAAgentList().ToList();
            foreach (var cssQAAgent in cssQAAgentList)
            {
                CSSQAAgentList.Add(new SelectListItem { Text = cssQAAgent.UserFullName, Value = Convert.ToString(cssQAAgent.UserId) });
            }

            FileStatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (FileStatusGetList_Result filestatus in css.FileStatusGetList())
            {
                FileStatusList.Add(new SelectListItem { Text = filestatus.StatusDescription, Value = filestatus.StatusId.ToString() });
            }

        }
        enum ApplyDeductible
        {
            Unknown = 1,
            Yes = 2,
            No = 3
        }
        enum Drivable
        {
            Unknown = 1,
            Waived = 2,
            Yes = 3,
            No = 4,

        }
        public enum AssignmentType
        { 
        AutoAssignment=1,
        PropertyAssignment=9  
        }
    }
}
