﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
    [Serializable]
    public class IndemnityReserveModifyViewModel
    {
        public string CoverageName { get; set; }
        public Int64 ClaimId { get; set; }
        public Int64 AssignmentId { get; set; }
        public ClaimReservesHistory ClaimReserveChange { get; set; }
        public List<usp_ClaimReservesHistoryGetList_Result> ClaimReserveHistoryList { get; set; }
    }
}
