﻿using BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
   [Serializable]
   public class UserProfileViewModel
    {
       private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
       public List<CustomeDDL> StateList = new List<CustomeDDL>();
       public UserProfile UserProfile { get; set; }
       public User user = new User();
       public Nullable<Int64> UserId { get; set; }
       public Nullable<Int32> HeadCompanyId { get; set; }
       public string CurrentPassword { get; set; }
       public string Password { get; set; }
       public String Error { get; set; }
       public UserProfileViewModel()
       {
         
       }
       public UserProfileViewModel(Int64 userId)
        {
        
            UserProfile = (from userObj in css.Users
                   where userObj.UserId == userId
                    select new UserProfile()
                   {
                       UserId = userObj.UserId,
                       FirstName = userObj.FirstName,
                       LastName = userObj.LastName,
                       UserName = userObj.UserName,
                       Email = userObj.Email,
                       StreetAddress = userObj.StreetAddress,
                       City = userObj.City,
                       State = userObj.State,
                       Zip = userObj.Zip,
                       MobilePhone = userObj.MobilePhone,
                       Twitter=userObj.Twitter,
                       Facebook = userObj.Facebook,
                       HeadCompanyId = userObj.HeadCompanyId,
                      
                       //Company1=userObj.Company1!=null? new ClaimCompany{
                       //    CompanyName =userObj.Company1.CompanyName,
                       //    Address = userObj.Company1.CompanyName,
                       //    City = userObj.Company1.City,
                       //    State = userObj.Company1.State,
                       //    Zip = userObj.Company1.Zip,
                       //    PhoneNumber = userObj.Company1.PhoneNumber,
                       //    CompanyId = userObj.Company1.CompanyId,
                       //}:null


                   }).Single();
            css.Configuration.ProxyCreationEnabled = false;
            UserProfile.Company1 = css.Companies.Find(UserProfile.HeadCompanyId);
            populateLists();
       }
       private void populateLists()
       {
           StateList.Add(new CustomeDDL { Text = "--Select--", Value = "0"});

           foreach (StateProvince state in css.StateProvinces.ToList())
           {
               StateList.Add(new CustomeDDL { Text = state.Name, Value = Convert.ToString(state.StateProvinceCode) });

           }
           
       }
       public class CustomeDDL
       {
           public String Value { get; set; }
           public String Text { get; set; }
           public Boolean IsMark { get; set; }
       }
    }
}
