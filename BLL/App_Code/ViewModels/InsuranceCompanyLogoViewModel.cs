﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
    [Serializable]
    public class InsuranceCompanyLogoViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public int companyid { get; set; }
        public string logo { get; set; }
        public bool IsReadOnly { get; set; }
        public int DisplayMode { get; set; }//LIST = 0, IMAGE = 1         
        public int MaxDocsCount { get; set; } // 0 - unlimited

         public InsuranceCompanyLogoViewModel(Int64 compid, int displayMode, bool isReadOnly)
        {
            var obj = from p in css.Companies
                      where p.CompanyId == compid
                      select p;
            if (obj != null)
            {
                foreach (var u in obj)
                {
                    logo = u.Logo.ToString();
                    companyid = u.CompanyId;
                }
            }
        }

        public InsuranceCompanyLogoViewModel()
        {
            
            MaxDocsCount = 0;
            DisplayMode = 0;
            IsReadOnly = true;
        }
    }
}
