﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using BLL.App_Code.Validations;
namespace BLL.ViewModels
{
    [Serializable]
    public class LoginViewModel
    {
        [Required]
        [DisplayName("User Name")]
        public string UserName { get; set; }
        [Required]
        [DisplayName("Password")]
        public string Password { get; set; }
       
        public Boolean RememberMe { get; set; }
        public String UserEmail { get; set; }
     
    }

    [Serializable]
    public class ResetPasswordViewModel
    {
        [Required]
        [DisplayName("Password")]
        public string Password { get; set; }
        [Required]
        [System.Web.Mvc.Compare("Password", ErrorMessage = "Password and Confirm Password do not match")]
        public string ConfirmPassword { get; set; }
      
        public String resetKey { get; set; }
    }
}
