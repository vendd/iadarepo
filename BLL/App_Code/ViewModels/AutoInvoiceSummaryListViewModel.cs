﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
    [Serializable]
    public class AutoInvoiceSummaryListViewModel
    {
        public Int64 AssignmentId;
        public string ClaimNumber;
        public bool IsEditable;
        public bool DisplayHeader;
        public List<usp_AutoInvoiceSummaryGetList_Result> AutoInvoiceSummaryList = new List<usp_AutoInvoiceSummaryGetList_Result>();
        public AutoInvoiceSummaryListViewModel()
        {
        }
    }
}
