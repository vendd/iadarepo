﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Data.Objects;
using System.Data.SqlClient;
using BLL.Models;

namespace BLL.ViewModels
{
    public class XactAutoAssignmentSearchViewModel
    {
        #region Properties
        public enum ViewMode { GENERAL, QAAGENT_REVIEW_QUEUE };
        public ViewMode SearchMode { get; set; }
        public string AutoClaimNumber { get; set; }
        public string AutoPolicyNumber { get; set; }
        public string AutoInvoiceNumber { get; set; }
        public string CatCode { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string LossType { get; set; }
        public Int32 ClaimType { get; set; }
        public string WorkFlowStatus { get; set; }
        public Int16 FileStatus { get; set; }
        public string JobType { get; set; }
        public string City { get; set; }
        public string LastName { get; set; }
        public string SPName { get; set; }
        public Int64 InsuranceCompany { get; set; }
        public string Distance { get; set; }
        public bool IsApproved { get; set; }
        public bool IsTriageAvailable { get; set; }
        //public int Page { get; set; }
        //public Int64 RecsPerPage { get; set; }
        //public string TotalCount { get; set; }
        //public string NoOfPages { get; set; }
        public bool OrderDirection { get; set; }
        public string OrderByField { get; set; }
        public Int64 CSSPOCUserId { get; set; }
        public Int64 CSSQAAgentUserId { get; set; }
        public Int64 LOBId { get; set; }
        public int ServiceId { get; set; }

        public int SearchOrExport { get; set; }
        public string InsureName { get; set; } //added for quick search
        public string ClientContactName { get; set; }//added for search based on client contact

        //public Int16 FirstPageNo { get; set; }
        //public Int16 PageCount = 10;
        public Pager Pager;
        public DateTime? DateFrom
        {
            get;
            set;
            //get
            //{
            //    if (DateFrom == DateTime.MinValue)
            //        return Convert.ToDateTime(null);
            //    else
            //        return DateFrom;
            //}
            //set
            //{
            //    if (value == DateTime.MinValue)
            //        DateFrom = Convert.ToDateTime(null);
            //    else
            //        DateFrom = value;
            //}
        }
        public DateTime? DateTo
        {
            get;
            set;
            //get
            //{
            //    if (DateTo == DateTime.MinValue)
            //        return Convert.ToDateTime(null);
            //    else
            //        return DateTo;
            //}
            //set
            //{
            //    if (value == DateTime.MinValue)
            //        DateTo = Convert.ToDateTime(null);
            //    else
            //        DateTo = value;
            //}
        }
        public DateTime? DateReceivedFrom { get; set; }
        public DateTime? DateReceivedTo { get; set; }

        //Lists
        public List<SelectListItem> StateList = new List<SelectListItem>();
        public List<SelectListItem> LossTypeList = new List<SelectListItem>();
        public List<SelectListItem> ClaimTypeList = new List<SelectListItem>();
        public List<SelectListItem> WorkFlowStatusList = new List<SelectListItem>();
        public List<SelectListItem> FileStatusList = new List<SelectListItem>();
        public List<SelectListItem> JobTypeList = new List<SelectListItem>();
        public List<SelectListItem> DocumentList = new List<SelectListItem>();  //Auto Document Type
        public IEnumerable<SearchExportedAssignments_Result> lst;
        //public List<AutoClaimsGetListNew_Result> AutoClaimsInfo { get; set; }
        public List<AutoClaimsSearch_Result> AutoClaimsInfo { get; set; }
        public IEnumerable<Claim> Claim { get; set; }
        public List<SelectListItem> InsuranceCompanies = new List<SelectListItem>();
        public List<SelectListItem> CSSPOCList = new List<SelectListItem>();
        public List<SelectListItem> CSSQAAgentList = new List<SelectListItem>();
        public List<GetLOBList_Result> LOBList = new List<GetLOBList_Result>();
        public List<SelectListItem> ServiceOfferingsList = new List<SelectListItem>();

        public ClaimDocumentType ClaimDocumentType { get; set; }

        public List<usp_AutoInvoiceSummaryGetList_Result> AutoInvoiceList { get; set; }

        #endregion


        //Auto Claim Search Result
        public String autoClaimSearchResult { get; set; }
        public XactAutoAssignmentSearchViewModel()
        {
            populateLists();
        }

        private void populateLists()
        {
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            
            DocumentList.Add(new SelectListItem { Text = "--Select--",Value = "0"});
            foreach (ClaimDocumentType ClaimDocumentType in css.ClaimDocumentTypes.ToList())
            {
                DocumentList.Add(new SelectListItem { Text = ClaimDocumentType.DocumentTypeDesc, Value = Convert.ToString(ClaimDocumentType.DocumentTypeId) });
            }

            ClaimTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (ClaimType claimtype in css.ClaimTypes.ToList())
            {
                ClaimTypeList.Add(new SelectListItem { Text = claimtype.Description, Value = Convert.ToString(claimtype.ClaimTypeId) });
            }
            
            StateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                StateList.Add(new SelectListItem { Text = state.Name, Value = state.StateProvinceCode });
            }

            LossTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (LossType losstype in css.LossTypes.ToList())
            {
                LossTypeList.Add(new SelectListItem { Text = losstype.LossType1, Value = losstype.LossTypeId.ToString() });
            }

            InsuranceCompanies.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (InsuranceCompaniesGetList_Result insurancecompany in css.InsuranceCompaniesGetList().OrderBy(x=>x.CompanyName))
            {
                InsuranceCompanies.Add(new SelectListItem { Text = insurancecompany.CompanyName, Value = insurancecompany.CompanyId.ToString() });
            }

            CSSPOCList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (CSSPointOfContactList_Result cssPOC in css.CSSPointOfContactList().ToList())
            {
                CSSPOCList.Add(new SelectListItem { Text = cssPOC.UserFullName, Value = cssPOC.UserId.ToString() });
            }

            CSSQAAgentList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (GetCSSQAAgentList_Result qaagent in css.GetCSSQAAgentList())
            {
                CSSQAAgentList.Add(new SelectListItem { Text = qaagent.UserFullName, Value = qaagent.UserId.ToString() });
            }
            FileStatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (FileStatusGetList_Result filestatus in css.FileStatusGetList())
            {
                FileStatusList.Add(new SelectListItem { Text = filestatus.StatusDescription, Value = filestatus.StatusId.ToString() });
            }
            //
            ServiceOfferingsList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (ServiceOffering serviceoffering in css.ServiceOfferings.ToList())
            {
                ServiceOfferingsList.Add(new SelectListItem { Text = serviceoffering.ServiceDescription, Value = Convert.ToString(serviceoffering.ServiceId) });
            }

            WorkFlowStatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            WorkFlowStatusList.Add(new SelectListItem { Text = "None", Value = "1" });
            WorkFlowStatusList.Add(new SelectListItem { Text = "Ready For Assignment", Value = "2" });
            WorkFlowStatusList.Add(new SelectListItem { Text = "Contacted", Value = "3" });
            WorkFlowStatusList.Add(new SelectListItem { Text = "Inspected", Value = "4" });
            WorkFlowStatusList.Add(new SelectListItem { Text = "Job Started", Value = "5" });
            WorkFlowStatusList.Add(new SelectListItem { Text = "Job Completed", Value = "6" });
            WorkFlowStatusList.Add(new SelectListItem { Text = "Job Not Sold", Value = "7" });


            JobTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            JobTypeList.Add(new SelectListItem { Text = "General", Value = "1" });
            JobTypeList.Add(new SelectListItem { Text = "Emergency", Value = "2" });
        }
    }
}
