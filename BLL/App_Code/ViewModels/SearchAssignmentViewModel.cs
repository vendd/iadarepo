﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
namespace BLL.ViewModels
{
    [Serializable]
    public class SearchAssignmentViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();


        public string ClaimNumber { get; set; }

        public string State { get; set; }
        public string LossType { get; set; }
        public string WorkFlowStatus { get; set; }
        public string FileStatus { get; set; }

        //Lists
        public List<SelectListItem> StateList = new List<SelectListItem>();
        public List<SelectListItem> LossTypeList = new List<SelectListItem>();
        public List<SelectListItem> WorkFlowStatusList = new List<SelectListItem>();
        public List<SelectListItem> FileStatusList = new List<SelectListItem>();

        public SearchAssignmentViewModel()
        {
            populateLists();
        }
        private void populateLists()
        {


            StateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                StateList.Add(new SelectListItem { Text = state.Name, Value = state.StateProvinceCode });
            }

            LossTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            WorkFlowStatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            FileStatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

        }
    }
}
