﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using BLL.Models;

namespace BLL.ViewModels
{
    public class NuGenAutoClaimInfoViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

        public string ActiveMainTab { get; set; }
        public string VehicleLocationName { get; set; }
        public Claim claim = new Claim();
        public AutoAssignment AutoAssignment = new AutoAssignment();
        public Note note = new Note();
        public Note Note { get; set; }
        public string ApprovalComment { get; set; }
        public Nullable<int> Rank { get; set; }
        public List<AutoNotesHistoryGetList_Result> AutoNoteHistory { get; set; }
        public List<usp_AutoInvoiceSummaryGetList_Result> AutoInvoiceList { get; set; }

        public List<SelectListItem> FileStatusList = new List<SelectListItem>();
        public AutoAssignment pa { get; set; }

        public Nullable<bool> RejectionMark { get; set; }
        public Nullable<int> RejectionCount { get; set; }
        public Nullable<int> CheckApprovedDate { get; set; }
        public Nullable<Double> DaysToContact { get; set; }
        public Nullable<Double> DaysToInspect { get; set; }
        public Nullable<Double> DaysToUpload { get; set; }
        public NuGenAutoClaimInfoViewModel()
        {
            PopulateLists();
        }

        public NuGenAutoClaimInfoViewModel(Int64 claimid)
        {
            claim = css.Claims.Find(claimid);
            VehicleLocationName = css.VehicleLocations.Where(x => x.VehicleLocationID == claim.VehicleLocationID).Select(x=>x.VehicleLocationName).FirstOrDefault();
            AutoAssignment = css.AutoAssignments.Where(x => x.ClaimId == claimid).OrderByDescending(x => x.AutoAssignmentId).FirstOrDefault();
            AutoInvoiceList = css.usp_AutoInvoiceSummaryGetList(claimid).ToList();

            pa = css.AutoAssignments.Where(x => x.ClaimId == claimid).OrderByDescending(x => x.AutoAssignmentId).FirstOrDefault();
            AutoAssignment pa1 = null; ;
            if (css.AutoAssignments.Where(x => x.ClaimId == claimid).Count() > 1)
            {
                pa1 = css.AutoAssignments.Where(x => x.ClaimId == claimid).OrderByDescending(x => x.AutoAssignmentId).Skip(1).FirstOrDefault();
            }

            ClaimCycleTimeDetail cctd = css.ClaimCycleTimeDetails.Where(x => x.ClaimId == claimid && x.SPId == pa.OAUserID).FirstOrDefault();
            if (cctd != null)
            {

                this.Rank = cctd.Rank.HasValue ? cctd.Rank.Value : 5;
                this.ApprovalComment = cctd.ApprovalNote;

                if (pa1 != null)
                {
                    if (pa.OAUserID != null && pa1.OAUserID != null)
                    {
                        if (pa1.OAUserID == pa.OAUserID || pa.DateApproved.HasValue)
                        {
                            //this.CheckApprovedDate = pa.DateApproved.HasValue ? 1 : 0;
                            this.CheckApprovedDate = 1;
                        }
                        else
                        {
                            this.CheckApprovedDate = 0;
                        }
                    }
                    else
                    {
                        if (pa.DateApproved.HasValue)
                        {
                            this.CheckApprovedDate = 1;
                        }
                        else
                        {
                            this.CheckApprovedDate = 0;
                        }

                    }


                }
                else
                {
                    if (pa.DateApproved.HasValue)
                    {
                        this.CheckApprovedDate = 1;
                    }
                    else
                    {
                        this.CheckApprovedDate = 0;
                    }

                }



                this.DaysToContact = cctd.AcceptedToContacted.HasValue ? cctd.AcceptedToContacted.Value : 0.0;
                this.DaysToInspect = cctd.ContactedToInspected.HasValue ? cctd.ContactedToInspected.Value : 0.0;
                this.DaysToUpload = cctd.ReceivedToEstimateReturned.HasValue ? cctd.ReceivedToEstimateReturned.Value : 0.0;
                this.RejectionMark = cctd.RejectionMark.HasValue ? cctd.RejectionMark.Value : true;
                this.RejectionCount = cctd.RejectionCount.HasValue ? cctd.RejectionCount.Value : 0;
            }
            else
            {

                this.Rank = 5;
                this.ApprovalComment = "";
                this.CheckApprovedDate = 0;

                this.DaysToContact = 0.0;
                this.DaysToInspect = 0.0;
                this.DaysToUpload = 0.0;
                this.RejectionMark = true;
                this.RejectionCount = 0;
            }
            PopulateLists();
        }

        public void PopulateLists()
        {
            FileStatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (FileStatusGetList_Result filestatus in css.FileStatusGetList())
            {
                FileStatusList.Add(new SelectListItem { Text = filestatus.StatusDescription, Value = filestatus.StatusId.ToString() });
            }
        }
    }
}
