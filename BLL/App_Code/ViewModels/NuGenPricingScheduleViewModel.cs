﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    [Serializable]
    public class NuGenPricingScheduleViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public CompanyLOBPricing CompanyLOBPricing { get; set; }
        public Int32 ServceTypeId { get; set; }
        public Int64 CompanyLOBPricingid { get; set; }
        public ServiceOffering ServiceType { get; set; }
        public string ddlServiceType { get; set; }
        public string ddlLob { get; set; }
        public string ValidationSummary { get; set; }
        public string Delstring { get; set; }
        public NuGenGetClaimDetails_Result NuGenClaimDetailResult { get; set; }

        
        public int LobID { get; set; }
        public string strLobDesc { get; set; }

        public string pricingClaimId { get; set; }
        public string pricingServiceTypeId
        {
            get;
            set;
        }
        public string pricingPrevServiceTypeId { get; set; }
        public string pricingSPReferenceInvoiceNo { get; set; }
        public string pricingLineofBusinessId { get; set; }
        public string PricingRCVAmount { get; set; }
        public string pricingServiceBaseFee { get; set; }
        public string pricingTotalNoHour { get; set; }
        public string pricingHourlyRate { get; set; }
        public string pricingTotalNoHourCalculate { get; set; }
        public string pricingTotalNoMile { get; set; }
        public string pricingFreeMiles { get; set; }
        public string pricingDollarPerMile { get; set; }
        public string pricingTotalNoMileCalculated { get; set; }
        public string pricingIsTotalLoss { get; set; }
        public string prcingTotalLossFee { get; set; }
        public string pricingMiscellaneous { get; set; }
        public string pricignMiscComment { get; set; }
        public string pricingSalesTaxPercent { get; set; }
        public string pricingSalesTaxAmout { get; set; }
        public string pricingTotalInvoiceAmount { get; set; }
        public string SpInvoiceNumber { get; set; } //INPSW -40

        //Done By Heta Dt:-03-07-2018
        public bool IsQBInvoice { get; set; }


        


        
        //public int 

        
        //Entities
        private ServiceLOBPricing _LOBPricingDetail;
        public ServiceLOBPricing LOBPricingDetail 
        {
            get
            {
                if (_LOBPricingDetail == null)
                    return new ServiceLOBPricing();
                else
                    return _LOBPricingDetail;
            } 
            set
            {
                _LOBPricingDetail = value;
            }
        }

        //SpResults
        public usp_AutoInvoiceGetDetails_Result AutoInvoiceDetails { get; set; }

        //Lists
        public List<CompanyLineofBusinessGetList_Result> pricingschedule { get; set; }
        public List<SelectListItem> LineofBusinessList = new List<SelectListItem>();
        public List<SelectListItem> ServiceTypesList = new List<SelectListItem>();
        public List<ServiceLOBPricing> LobPricingList { get; set; } //List of LOBs associated with service type
        public List<SelectListItem> TotalLossList = new List<SelectListItem>();
        
        public NuGenPricingScheduleViewModel()
        {
            
        
        }


        //Constructors
        public NuGenPricingScheduleViewModel(Int32 ClaimId, bool flag)
        {
            AutoInvoiceDetails = new usp_AutoInvoiceGetDetails_Result();
            Int64 AutoAssignmentId = (from c in css.AutoAssignments
                                      orderby c.AutoAssignmentId descending
                                      where c.ClaimId == ClaimId
                                      select c.AutoAssignmentId).FirstOrDefault();

            bool ?IsNugen = (from c in css.Claims
                            where c.ClaimId == ClaimId
                            select c.IsNugeAssignment).FirstOrDefault();   

            if (css.AutoInvoices.Where(a => a.AutoAssignmentId == AutoAssignmentId).Any())
            {
                //This is for Edit
                Int64 intAutoInvoiceId = (from c in css.AutoInvoices
                                          where c.AutoAssignmentId == AutoAssignmentId
                                          select c.AutoInvoiceId).FirstOrDefault();

                AutoInvoiceDetails = css.usp_AutoInvoiceGetDetails(AutoAssignmentId, intAutoInvoiceId).FirstOrDefault();

                //ServiceTypes
                pricingClaimId = ClaimId.ToString();
                pricingServiceTypeId = AutoInvoiceDetails.ServiceOffering.HasValue?AutoInvoiceDetails.ServiceOffering.Value.ToString():"0";  //Svp1
                SpInvoiceNumber = AutoInvoiceDetails.SpInvoiceNumber;//INPSW -40
               // pricingServiceTypeId = AutoInvoiceDetails.ServiceId.HasValue ? AutoInvoiceDetails.ServiceId.Value.ToString() : "0";
                pricingPrevServiceTypeId = pricingServiceTypeId;
                pricingLineofBusinessId = AutoInvoiceDetails.LOBId.HasValue ? AutoInvoiceDetails.LOBId.Value.ToString() : "0";
                pricingIsTotalLoss = AutoInvoiceDetails.TotalIsTotalLoss.HasValue ? (AutoInvoiceDetails.TotalIsTotalLoss.Value==true?"1":"0") : "2";
                ServiceTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                var listServiceTypes = (from c in css.ServiceOfferings
                                        where c.IsDeleted == false
                                        select c).ToList();

                foreach (var item in listServiceTypes)
                {
                    if (item.ServiceId == AutoInvoiceDetails.ServiceOffering)
                        //if (item.ServiceId == AutoInvoiceDetails.ServiceId) svp 2
                        ServiceTypesList.Add(new SelectListItem { Text = item.ServiceDescription, Value = item.ServiceId.ToString(), Selected = true });
                    else
                        ServiceTypesList.Add(new SelectListItem { Text = item.ServiceDescription, Value = item.ServiceId.ToString() });
                }

                //LOBs
                LineofBusinessList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

                Int32 lobId = (from c in css.Claims
                               where c.ClaimId == ClaimId
                               select (Int32?)c.LOBId.Value).FirstOrDefault() ?? 0;

                foreach (NuGenLineOfBusinessGetList_Result lineofbusiness in css.NuGenLineOfBusinessGetList(AutoInvoiceDetails.ServiceOffering))// svp 3
               // foreach (NuGenLineOfBusinessGetList_Result lineofbusiness in css.NuGenLineOfBusinessGetList(AutoInvoiceDetails.ServiceId))
                {
                    if (lobId == lineofbusiness.LOBId)
                        LineofBusinessList.Add(new SelectListItem { Text = lineofbusiness.LOBDescription, Value = lineofbusiness.LOBId.ToString(), Selected = true });
                    else
                        LineofBusinessList.Add(new SelectListItem { Text = lineofbusiness.LOBDescription, Value = lineofbusiness.LOBId.ToString() });
                }
                
                TotalLossList.Add(new SelectListItem { Text = "--Select--", Value = "" });
                TotalLossList.Add(new SelectListItem { Text = "Yes", Value = "1", Selected = AutoInvoiceDetails.TotalIsTotalLoss.Value });
                TotalLossList.Add(new SelectListItem { Text = "No", Value = "0" });
            }
            else
            {
               //Nugen claim have not service type so bysefault we set 1
                Int32? ServiceId = 0;
                ServiceId = (from c in css.AutoAssignments
                             where c.AutoAssignmentId == AutoAssignmentId
                             select c.ServiceId).FirstOrDefault();
              if(IsNugen == true)
              {
                  if (ServiceId == null || ServiceId == 0)
                  {
                      ServiceId = 1;
                  }
                  
              }
                //svp 10
              //else
              //{
              //     ServiceId = (from c in css.AutoAssignments
              //                 where c.AutoAssignmentId == AutoAssignmentId
              //                 select c.ServiceId).FirstOrDefault();
              //}
                 
                pricingClaimId = ClaimId.ToString();
                ServiceTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                var listServiceType = (from c in css.ServiceOfferings
                                       where c.IsDeleted == false
                                       select c).ToList();

                foreach (var item in listServiceType)
                {
                    if (item.ServiceId == ServiceId)
                    ServiceTypesList.Add(new SelectListItem { Text = item.ServiceDescription, Value = item.ServiceId.ToString(),Selected = true });
                    else
                    ServiceTypesList.Add(new SelectListItem { Text = item.ServiceDescription, Value = item.ServiceId.ToString()});

                }
                pricingServiceTypeId = "" + ServiceId??"0";
                LineofBusinessList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
                foreach (NuGenLineOfBusinessGetList_Result lineofbusiness in css.NuGenLineOfBusinessGetList(ServiceId))
                {
                        LineofBusinessList.Add(new SelectListItem { Text = lineofbusiness.LOBDescription, Value = lineofbusiness.LOBId.ToString()});
                }

                TotalLossList.Add(new SelectListItem { Text = "--Select--", Value = "" });
                TotalLossList.Add(new SelectListItem { Text = "Yes", Value = "1" });
                TotalLossList.Add(new SelectListItem { Text = "No", Value = "0" });
            }
            this.NuGenClaimDetailResult = (NuGenGetClaimDetails_Result)css.NuGenGetClaimDetails(ClaimId).FirstOrDefault();
        }
        public NuGenPricingScheduleViewModel(Int32 ServiceTypeId)
        {
            ServiceType = css.ServiceOfferings.Find(ServiceTypeId);
            ServceTypeId = ServiceTypeId;
            populateLists(ServiceTypeId);
        }
        public NuGenPricingScheduleViewModel(NuGenPricingScheduleViewModel viewmodel)
        {
            ServiceType = css.ServiceOfferings.Find(viewmodel.ServceTypeId);
            populateLists(viewmodel.ServceTypeId);
        }


        //Get details of LOB pricing
        public void NuGenLOBPricingDetail(Int32 LobID, Int32 ClaimId)
        {
            this.NuGenClaimDetailResult = (NuGenGetClaimDetails_Result)css.NuGenGetClaimDetails(ClaimId).FirstOrDefault();

            LOBPricingDetail = (from c in css.ServiceLOBPricings
                               where c.LOBId == LobID
                               select c).FirstOrDefault();
        }

        public void populateLists(Int32 ServiceTypeId)
        {
            LineofBusinessList = new List<SelectListItem>();
            LineofBusinessList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            foreach (NuGenLineOfBusinessGetList_Result lineofbusiness in css.NuGenLineOfBusinessGetList(ServiceTypeId))
            {
                LineofBusinessList.Add(new SelectListItem { Text = lineofbusiness.LOBDescription, Value = lineofbusiness.LOBId.ToString() });
            }

            NuGenClaimDetailResult = (NuGenGetClaimDetails_Result)css.NuGenGetClaimDetails(Convert.ToInt32(pricingClaimId)).FirstOrDefault();

            TotalLossList.Add(new SelectListItem { Text = "--Select--", Value = "" });
            TotalLossList.Add(new SelectListItem { Text = "Yes", Value = "1" });
            TotalLossList.Add(new SelectListItem { Text = "No", Value = "0" });
        }


        //Added 24-08-2017
        public void populateLOBAndServiceTypes(Int32 ClaimId)
        {
            AutoInvoiceDetails = new usp_AutoInvoiceGetDetails_Result();
            Int64 AutoAssignmentId = (from c in css.AutoAssignments
                                      where c.ClaimId == ClaimId
                                      select c.AutoAssignmentId).FirstOrDefault();

            Int64 intAutoInvoiceId = (from c in css.AutoInvoices
                                      where c.AutoAssignmentId == AutoAssignmentId
                                      select c.AutoInvoiceId).FirstOrDefault();

            AutoInvoiceDetails = css.usp_AutoInvoiceGetDetails(AutoAssignmentId, intAutoInvoiceId).FirstOrDefault();

            ServiceTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            var listServiceTypes = (from c in css.ServiceOfferings
                                    where c.IsDeleted == false
                                    select c).ToList();

            foreach (var item in listServiceTypes)
            {
                if (item.ServiceId == AutoInvoiceDetails.ServiceOffering)
                    //if (item.ServiceId == AutoInvoiceDetails.ServiceId)svp 4
                    ServiceTypesList.Add(new SelectListItem { Text = item.ServiceDescription, Value = item.ServiceId.ToString(), Selected = true });
                else
                    ServiceTypesList.Add(new SelectListItem { Text = item.ServiceDescription, Value = item.ServiceId.ToString() });
            }

            //LOBs
            LineofBusinessList = new List<SelectListItem>();
            LineofBusinessList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            Int32 lobId = (from c in css.Claims
                           where c.ClaimId == ClaimId
                           select (Int32?)c.LOBId.Value).FirstOrDefault() ?? 0;

            foreach (NuGenLineOfBusinessGetList_Result lineofbusiness in css.NuGenLineOfBusinessGetList(AutoInvoiceDetails.ServiceOffering))// svp 5
           // foreach (NuGenLineOfBusinessGetList_Result lineofbusiness in css.NuGenLineOfBusinessGetList(AutoInvoiceDetails.ServiceId)) 
            {
                if (lobId == lineofbusiness.LOBId)
                    LineofBusinessList.Add(new SelectListItem { Text = lineofbusiness.LOBDescription, Value = lineofbusiness.LOBId.ToString(), Selected = true });
                else
                    LineofBusinessList.Add(new SelectListItem { Text = lineofbusiness.LOBDescription, Value = lineofbusiness.LOBId.ToString() });
            }
        }

        public void getNuGenClaimDetailResult(string ClaimId)
        {
            NuGenClaimDetailResult = (NuGenGetClaimDetails_Result)css.NuGenGetClaimDetails(Convert.ToInt32(ClaimId)).FirstOrDefault();
            pricingClaimId = ClaimId.ToString();
            ServiceTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            var listServiceTypes = (from c in css.ServiceOfferings
                                    where c.IsDeleted == false
                                    select c).ToList();

            foreach (var item in listServiceTypes)
            {
                ServiceTypesList.Add(new SelectListItem { Text = item.ServiceDescription, Value = item.ServiceId.ToString() });
            }

        }

        //public void populateServieTypeLOB(Int32 ServiceTypeId)
        //{
        //    LineofBusinessList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

        //    foreach (NuGenLineOfBusinessGetList_Result lineofbusiness in css.NuGenLineOfBusinessGetList(ServiceTypeId))
        //    {
        //        LineofBusinessList.Add(new SelectListItem { Text = lineofbusiness.LOBDescription, Value = lineofbusiness.LOBId.ToString() });
        //    }
        //}

        public void PopulateLOBlist(int lobID)
        {
            LobPricingList = css.ServiceLOBPricings.Where(a => a.LOBId == lobID).ToList();
        }

        public void FillServiceTypeDetails(Int32 serviceTypeId)
        {
            this.ServiceType = css.ServiceOfferings.Find(serviceTypeId);
        }

        public void ValidateLobDetails()
        {
            //bool IsdataValid = true;
            this.ValidationSummary = "";
            if (LobPricingList != null)
            {
                for (int i = 0; i < LobPricingList.Count; i++)
                {
                    StringBuilder strBvalidation = new StringBuilder();
                    ServiceLOBPricing Clob = LobPricingList[i];
                    if (!(Clob.StartRange.HasValue || (Clob.StartRange.HasValue && Clob.StartRange == 0))) { strBvalidation.Append(" Start Range required </br>"); }
                    if (!(Clob.EndRange.HasValue || (Clob.EndRange.HasValue && Clob.StartRange == 0))) { strBvalidation.Append(" End Range required </br>"); }
                    if (Clob.StartRange.HasValue && Clob.EndRange.HasValue && (Clob.StartRange > Clob.EndRange))
                    {
                        strBvalidation.Append(" End range must be greater then the start range");
                    }

                    bool isTimeExepenses = Clob.IsTimeNExpance.HasValue ? Clob.IsTimeNExpance.Value : false;


                    if (Clob.BaseServiceFee.HasValue || Clob.SPServiceFee.HasValue)
                    {
                        if (Clob.RCVPercent.HasValue || Clob.SPRCVPercent.HasValue)
                        {
                            strBvalidation.Append(" Please specify either service fee value or percentage </br>  ");
                        }
                        else
                        {
                            if (Clob.IsTimeNExpance == null || Clob.IsTimeNExpance != true)
                            {
                                if (!(Clob.BaseServiceFee.HasValue || (Clob.BaseServiceFee.HasValue && Clob.BaseServiceFee == 0))) { strBvalidation.Append(" Base Service Fee value required </br>"); }

                                //if (!(Clob.SPServiceFee.HasValue || (Clob.SPServiceFee.HasValue && Clob.SPServiceFee == 0))) { strBvalidation.Append(" SP Service Fee value required </br>"); }
                            }
                        }
                    }
                    else
                    {
                        if (Clob.IsTimeNExpance == null || Clob.IsTimeNExpance != true)
                        {
                            if (!(Clob.RCVPercent.HasValue || (Clob.RCVPercent.HasValue && Clob.RCVPercent == 0))) { strBvalidation.Append(" RCV Percent required </br>"); }
                            else
                            {
                                if (Clob.RCVPercent.Value > 100 || Clob.RCVPercent.Value < 0) strBvalidation.Append(" RCV Percent should be between 0  to 100 </br>");
                            }
                            //if (!(Clob.SPRCVPercent.HasValue || (Clob.SPRCVPercent.HasValue && Clob.SPRCVPercent == 0))) { strBvalidation.Append(" SP RCV Percent required </br>"); }
                            //else
                            //{
                            //    if (Clob.SPRCVPercent.Value > 100 || Clob.SPRCVPercent.Value < 0) strBvalidation.Append(" RCV Percent should be between 0  to 100 </br>");
                            //}
                        }

                    }

                    if (strBvalidation.ToString().Trim() == "")
                    {
                        for (int j = 0; j < LobPricingList.Count; j++)
                        {
                            if (j != i)
                            {
                                ServiceLOBPricing OtherCol = LobPricingList[j];
                                if ((OtherCol.StartRange <= Clob.StartRange) && (OtherCol.EndRange >= Clob.StartRange))
                                {
                                    strBvalidation.Append("Invalid range : Range  vaues overlap with values in " + j + " row </br>");
                                }
                            }
                        }
                    }

                    if (Clob.SPXPercent.HasValue && (Clob.SPXPercent.Value > 100 || Clob.SPXPercent.Value < 0))
                    {
                        strBvalidation.Append(" SPX Percent should be between 0  to 100 </br>");
                    }


                    if (strBvalidation.ToString().Trim() != "")
                    {
                        this.ValidationSummary += " Row No : " + (i + 1) + " </br>";
                        this.ValidationSummary += strBvalidation.ToString();
                    }
                }
                if (css.LineOfBusinesses.Find(LobID).TEMonthlyBillingDay.HasValue)
                {
                    if (LobPricingList.Where(x => (x.IsTimeNExpance ?? false) == false).ToList().Count > 0)
                    {
                        this.ValidationSummary += "Only T&E pricing can be added as T&E Monthly Billing Day has been specified.";
                    }
                }

            }
            //this.ValidationSummary
        }
    }
}
