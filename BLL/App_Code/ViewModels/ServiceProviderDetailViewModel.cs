﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
    [Serializable]
    public class ServiceProviderDetailViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public User user = new User();
        public Dictionary<int, string> ClaimTypeList = new Dictionary<int, string>();
        public bool IsSertifiAgreementSigned { get; set; }
        public string SertifiAgreementSignedURL { get; set; }
        public bool IsSertifiW9Signed { get; set; }
        public string SertifiW9SignedURL { get; set; }
        public bool IsSertifiDDFSigned { get; set; }
        public string SertifiDDFSignedURL { get; set; }
        public string NetworkProvider { get; set; }

        public IEnumerable<GetUnQualifiedList_Result> UnQualifiedList {get; set;}
        public IEnumerable<GetUnQualifiedSPByLOBList_Result> UnQualifiedSPCompanyLOBList { get; set; }
        public IEnumerable<GetUnQualifiedSPByClientPOCList_Result> UnQualifiedSPCompanyClientPOCList { get; set; }
        public ServiceProviderDetailViewModel(Int64 userId)
        {

        }
    }
}
