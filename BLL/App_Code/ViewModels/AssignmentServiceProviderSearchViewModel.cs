﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;

using System.ComponentModel.DataAnnotations;

namespace BLL.ViewModels
{
    [Serializable]
    public class AssignmentServiceProviderSearchViewModel
    {
        public string SPName { get; set; }
        public string Location { get; set; }
        [Required]
        public int Rank { get; set; }
        [Required]
        public int Experience { get; set; }
        [Required]
        public int Distance { get; set; }
        public Int64 AssignmentId { get; set; }
        public List<AssignmentSPSearch_Result> ServiceProvidersList { get; set; }
        public GoogleMapViewModel GoogleMapViewModel { get; set; }
        public AssignmentServiceProviderSearchViewModel()
        {
            Rank = 0;
            Experience = 0;
            Distance = 0;
            GoogleMapViewModel = new GoogleMapViewModel();
        }


    }
}
