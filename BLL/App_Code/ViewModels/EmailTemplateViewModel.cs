﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
    [Serializable]
    public class EmailTemplateViewModel
    {
         private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public EmailTemplate emailtemplate { get; set; }
        public string TemplateName { get; set; }
        public string TemplateBody { get; set; }
        public int headCompanyId { get; set; }

        public int StatusId { get; set; }
        public List<EmailTemplateGetList_Result> TemplateGetList { get; set; }

        public EmailTemplateViewModel()
        {

        }
        public EmailTemplateViewModel(int HeadCompanyId)
        {
            headCompanyId = HeadCompanyId;
            this.emailtemplate = new BLL.EmailTemplate();
            emailtemplate.TemplateId = -1;

        }
        public EmailTemplateViewModel(int HeadCompanyId, int TemplateId)
        {
           emailtemplate=css.EmailTemplates.Find(TemplateId);
        }
    }

    
}
