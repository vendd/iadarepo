﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    [Serializable]
    public class AutoInvoiceViewModel
    {

        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public bool ShowBilledNotesExportOption { get; set; }
        public bool allowSubmit { get; set; }
        public string feetype { get; set; }
        public string invoiceStatus { get; set; }
        public string viewType { get; set; }
        public string assignmentTab { get; set; }
        public decimal? TotalAmountReceived { get; set; }
        ServiceProviderDetail serviceProviderDetails = new ServiceProviderDetail();
        public ServiceProviderDetail serviceProviderDetail { get; set; }
        public PropertyInvoice propertyInvoice { get; set; }
        public AutoAssignment autoAssignment { get; set; }
        public AutoAssignment autoAssignments = new AutoAssignment();
        public Claim claim { get; set; }
        public Claim claims = new Claim();
        public usp_AutoInvoiceGetDetails_Result INvoiceResult = new usp_AutoInvoiceGetDetails_Result();
        public usp_AutoInvoiceGetDetails_Result invoiceDetail { get; set; }
        public List<usp_AutoPaymentGetList_Result> paymentslist { get; set; }
        public Payment payment { get; set; }
        public Int64 InvoiceId { get; set; }
        public Int64 AutoInvoiceId { get; set; }
        public decimal TotalBalanceDue { get; set; }

        //List
        public List<SelectListItem> InvoiceTypeList = new List<SelectListItem>();
        public List<SelectListItem> ServiceOfferingList = new List<SelectListItem>();
        public List<SelectListItem> InvoiceStatusList = new List<SelectListItem>();
        public List<SelectListItem> LineOfBussinessList = new List<SelectListItem>();
        public List<SelectListItem> FeeTypeList = new List<SelectListItem>();
        public List<SelectListItem> IsTaxApplicable = new List<SelectListItem>();
        public List<SelectListItem> ReasonTyptList = new List<SelectListItem>();
        public List<SelectListItem> SPLevelList = new List<SelectListItem>();
        public List<usp_AutoInvoiceSummaryGetList_Result> AutoInvoiceList = new List<usp_AutoInvoiceSummaryGetList_Result>();

        public AutoInvoiceViewModel()
        {
            this.paymentslist = new List<usp_AutoPaymentGetList_Result>();
            this.payment = new Payment();
            //this.invoiceDetail = new usp_PropertyInvoiceGetDetails_Result();
            //this.claim = new Claim();
            //this.propertyAssignment = new PropertyAssignment();
            //this.propertyInvoice = new PropertyInvoice();
            PopulateList();
        }

        private void PopulateList()
        {
            InvoiceTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            InvoiceTypeList.Add(new SelectListItem { Text = "Final", Value = "1" });
            InvoiceTypeList.Add(new SelectListItem { Text = "Interim", Value = "2", Selected = true });
            InvoiceTypeList.Add(new SelectListItem { Text = "Supplement", Value = "3" });

            ServiceOfferingList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (var offering in css.ServiceOfferings)
            {
                ServiceOfferingList.Add(new SelectListItem { Text = offering.ServiceDescription, Value = offering.ServiceId.ToString() });
            }
            InvoiceStatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            InvoiceStatusList.Add(new SelectListItem { Text = "Open", Value = "1", Selected = true });
            InvoiceStatusList.Add(new SelectListItem { Text = "Short Paid", Value = "2" });
            InvoiceStatusList.Add(new SelectListItem { Text = "Over Paid", Value = "3" });
            InvoiceStatusList.Add(new SelectListItem { Text = "Reopened", Value = "4" });
            InvoiceStatusList.Add(new SelectListItem { Text = "Pending", Value = "5" });

            LineOfBussinessList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            IsTaxApplicable.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            IsTaxApplicable.Add(new SelectListItem { Text = "Y", Value = "1" });
            IsTaxApplicable.Add(new SelectListItem { Text = "N", Value = "2" });

            FeeTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            //FeeTypeList.Add(new SelectListItem { Text = "Time And Expense", Value = "1" });
            //FeeTypeList.Add(new SelectListItem { Text = "Flat Fee", Value = "2" });
            FeeTypeList.Add(new SelectListItem { Text = "Scheduled Services", Value = "3", Selected = true });

            ReasonTyptList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            ReasonTyptList.Add(new SelectListItem { Text = "RCV lower than Captured", Value = "1" });
            ReasonTyptList.Add(new SelectListItem { Text = "Billed Incorrectly", Value = "2" });
            ReasonTyptList.Add(new SelectListItem { Text = "Management Decision", Value = "3" });
            ReasonTyptList.Add(new SelectListItem { Text = "Wrong Line of Business used", Value = "4" });

            SPLevelList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            SPLevelList.Add(new SelectListItem { Text = "Standard Adjuster", Value = "1" });
            SPLevelList.Add(new SelectListItem { Text = "General Adjuster", Value = "2" });
            SPLevelList.Add(new SelectListItem { Text = "Executive Adjuster", Value = "3" });
        }
    }
}
