﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
    public class NuGenInvoiceViewModel
    {
        public string CompanyName { get; set; }
        public decimal? TotalAmountReceived { get; set; }
        public usp_AutoInvoiceGetDetails_Result invoiceDetail { get; set; }

        public bool IsQBInvoice { get; set; }
    }
}
