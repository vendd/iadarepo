﻿using BLL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
    [Serializable]
    public class NuGenAutoAssignmentSearchViewModel
    {
        public List<NuGenClaimsGetList_Result> NuGenAutoAssignmentSearch { get; set; }
        public Pager Pager { get; set; }
        public string strNuGenAutoAssignmentResult { get; set; }
        public XactAutoAssignmentSearchViewModel objXactAutoAssignmentSearchViewModel {get;set;}
        public NuGenAutoAssignmentSearchViewModel() {
            objXactAutoAssignmentSearchViewModel = new XactAutoAssignmentSearchViewModel();
        }
    }
}
