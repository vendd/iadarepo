﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    [Serializable]
    public class NuGenLineOfBusinessViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public LineOfBusiness LOB { get; set; }
        public Int32 ServiceTypeId { get; set; }

        public bool HasBridge { get; set; }
        public RequirementViewModel RequirementsViewModel { get; set; }
        //Lists
        public List<ServiceTypewiseLineOfBusinessGetList_Result> LOBGetList { get; set; }
        public List<SelectListItem> LOBTypeList = new List<SelectListItem>();
        public List<SelectListItem> CatTypeList = new List<SelectListItem>();

        public NuGenLineOfBusinessViewModel()
        {

        }

        public NuGenLineOfBusinessViewModel(Int32 serviceTypeId)
        {
            ServiceTypeId = serviceTypeId;
            this.LOB = new LineOfBusiness();
            this.LOB.LOBId = -1;

            this.HasBridge = false;
            populateLists();
        }

        public NuGenLineOfBusinessViewModel(Int32 serviceTypeId, Int32 LOBId)
        {
            ServiceTypeId = serviceTypeId;
            LOB = css.LineOfBusinesses.Find(LOBId);

            populateLists();
        }

        public NuGenLineOfBusinessViewModel(NuGenLineOfBusinessViewModel viewmodel, Int32 serviceTypeId)
        {
            ServiceTypeId = serviceTypeId;
            LOB = viewmodel.LOB;
            LOBGetList = viewmodel.LOBGetList;
            populateLists();
        }



        public void populateLists()
        {

            LOBTypeList.Add(new SelectListItem { Text = "Personal", Value = "PL" });
            LOBTypeList.Add(new SelectListItem { Text = "Commercial", Value = "CL" });
            LOBTypeList.Add(new SelectListItem { Text = "STANDARD", Value = "ST" });
            LOBTypeList.Add(new SelectListItem { Text = "HEAVY EQUIPMENT", Value = "HE" });
            LOBTypeList.Add(new SelectListItem { Text = "WY/MT/ND/SD", Value = "WD" });

            CatTypeList.Add(new SelectListItem { Text = "CAT", Value = "CT" });
            CatTypeList.Add(new SelectListItem { Text = "Daily", Value = "DL" });
            CatTypeList.Add(new SelectListItem { Text = "Auto", Value = "AT" });
            CatTypeList.Add(new SelectListItem { Text = "SPECIALTY", Value = "ST" });
            CatTypeList.Add(new SelectListItem { Text = "HEAVY", Value = "HV" });
        }
    }
}
