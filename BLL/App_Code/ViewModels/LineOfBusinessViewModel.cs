﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    [Serializable]
    public class LineOfBusinessViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public LineOfBusiness LOB { get; set; }
        public Int32 headcompanyid { get; set; }

        public bool HasBridge { get; set; }
        public RequirementViewModel RequirementsViewModel { get; set; }
        //Lists
        public List<CompanywiseLineOfBusinessGetList_Result> LOBGetList { get; set; }
        public List<SelectListItem> LOBTypeList = new List<SelectListItem>();
        public List<SelectListItem> CatTypeList = new List<SelectListItem>();

        public LineOfBusinessViewModel()
        {

        }

        public LineOfBusinessViewModel(Int32 HeadCompanyId)
        {
            headcompanyid = HeadCompanyId;
            this.LOB = new LineOfBusiness();
            this.LOB.LOBId = -1;

            this.HasBridge = false;
            populateLists();
        }

        public LineOfBusinessViewModel(Int32 HeadCompanyId, Int32 LOBId)
        {
            headcompanyid = HeadCompanyId;
            LOB = css.LineOfBusinesses.Find(LOBId);

            populateLists();
        }

        public LineOfBusinessViewModel(LineOfBusinessViewModel viewmodel, Int32 HeadCompanyId)
        {
            headcompanyid = HeadCompanyId;
            LOB = viewmodel.LOB;
            LOBGetList = viewmodel.LOBGetList;
            populateLists();
        }



        public void populateLists()
        {

            LOBTypeList.Add(new SelectListItem { Text = "Personal", Value = "PL" });
            LOBTypeList.Add(new SelectListItem { Text = "Commercial", Value = "CL" });
            LOBTypeList.Add(new SelectListItem { Text = "STANDARD", Value = "ST" });
            LOBTypeList.Add(new SelectListItem { Text = "HEAVY EQUIPMENT", Value = "HE" });

            CatTypeList.Add(new SelectListItem { Text = "CAT", Value = "CT" });
            CatTypeList.Add(new SelectListItem { Text = "Daily", Value = "DL" });
            CatTypeList.Add(new SelectListItem { Text = "Auto", Value = "AT" });
            CatTypeList.Add(new SelectListItem { Text = "SPECIALTY", Value = "ST" });
        }

    }
}
