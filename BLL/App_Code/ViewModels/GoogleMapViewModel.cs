﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Models;
namespace BLL.ViewModels
{
    [Serializable]
    public class GoogleMapViewModel
    {
        public List<GoogleMapLocation> MarkerLocationList { get; set; }
        
        public GoogleMapViewModel()
        {
        }

    }
}
