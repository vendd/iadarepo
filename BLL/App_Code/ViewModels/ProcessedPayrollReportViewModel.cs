﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
    public class ProcessedPayrollReportViewModel
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public List<string> PayrollDates { get; set; }

    }
}
