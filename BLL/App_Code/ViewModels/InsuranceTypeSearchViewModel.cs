﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BLL.ViewModels;
using BLL.Models;
namespace BLL.ViewModels
{
    [Serializable]
    public class InsuranceTypeSearchViewModel 
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

        //Search Criteria
        public string ClaimsRepresentativeManger { get; set; }
        public int companyid { get; set; }
        public int CompanyTypeId { get; set; }
        [Required]
        [DisplayName("Company Name")]
        public string CompanyName { get; set; }
        public int IntegrationTypeId { get; set; }
        public bool UsesSharedDataset { get; set; }
        public string XACTBusinessUnit { get; set; }
        public bool IsActive { get; set; }
        public bool IsQRPServiceActive { get; set; }
        public string QBCustomerId { get; set; } //INPSW -39
        public string CarriedId { get; set; }
        public string LineOfBusiness { get; set; }
        [DisplayName("Tax Id")]
        public string TaxID { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string PhoneNumber { get; set; }
        public string WebsiteURL { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public int RegionId { get; set; }
        public string[] regions { get; set; }
        public string logo { get; set; }
        public string active { get; set; }
        public string region { get; set; }
        public bool EnableFTPDocExport { get; set; }
        public string FTPUserName { get; set; }
        public string FTPPassword { get; set; }
        public string FTPConfirmPassword { get; set; }
        public bool HasDocumentBridge { get; set; }

        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        //public string UserName { get; set; }

        public string sortBy { get; set; }
        public string sortString { get; set; }
        public bool IsAutoConfigureInvoice { get; set;}
        //Head Companyid for validation for loggin Company
        public Int64 HeadCompanyID { get; set; }
        public Pager Pager;
        public User Users { get; set; }
        public RequirementViewModel RequirementsViewModel { get; set; }
        //List
        public IEnumerable<InsuranceTypeSearchViewModel> companymodel { get; set; }
        //public Company company = new Company();
        //public IntegrationType integrationtype = new IntegrationType();
        //public CompanyRegion companyregion = new CompanyRegion();
        public IEnumerable<Company> company { get; set; }
        public IEnumerable<Companies> Companylist { get; set; }
        public List<SelectListItem> IntegrationTypeList = new List<SelectListItem>();
        public List<SelectListItem> IsActiveList = new List<SelectListItem>();
        public List<SelectListItem> StateList = new List<SelectListItem>();
        public MultiSelectList RegionTypeList;


        //        List<string> myList = new List<string>();
        //IEnumerable<string> myEnumerable = myList;


        public InsuranceTypeSearchViewModel()
        {
            //Companylist = css.Companies.Where(x => x.CompanyTypeId == 1);
            //IEnumerable<Companies> objcomp = from p in css.Companies
            //                                 join i in css.IntegrationTypes on p.IntegrationTypeId equals i.IntegrationTypeId
            //                                 where (p.CompanyTypeId == 1)
            //                                 select new Companies
            //                                 {
            //                                     CompanyId = p.CompanyId,
            //                                     CompanyName = p.CompanyName,
            //                                     City = p.City,
            //                                     State = p.State,
            //                                     Zip = p.Zip,
            //                                     WebsiteURL = p.WebsiteURL,
            //                                     IsActive = p.IsActive,
            //                                     PhoneNumber = p.PhoneNumber,
            //                                     TaxID = p.TaxID,
            //                                     Address = p.Address,
            //                                     Fax = p.Fax,
            //                                     Email = p.Email

            //                                 };
            //Companylist = objcomp;
            //IEnumerable<Company> objcoma = css.Companies.Where(x => x.CompanyTypeId == 1 && x.IntegrationTypeId != 0).ToList();
            //company = objcoma;
            //foreach (Companies comp in Companylist)
            //{
            //    companyid = comp.CompanyId;
            //    CompanyName = comp.CompanyName;
            //    CarriedId = comp.CarriedId;
            //    TaxID = comp.TaxID;
            //    Address = comp.Address;
            //    City = comp.City;
            //    State = comp.State;
            //    Zip = comp.Zip;
            //    PhoneNumber = comp.PhoneNumber;
            //    WebsiteURL = comp.WebsiteURL;
            //    IntegrationTypeId = Convert.ToInt32(comp.IntegrationTypeId);
            //    Email = comp.Email;
            //    Fax = comp.Fax;
            //    IsActiveId = Convert.ToBoolean(comp.IsActive);
            //}

            FillDetails();
        }

        public InsuranceTypeSearchViewModel(Int64 compid)
        {
            //Companylist = css.Companies.Where(x => (x.CompanyTypeId == 1) && (x.CompanyId == compid));
            IEnumerable<Companies> objcomp = from p in css.Companies
                                             join i in css.IntegrationTypes on p.IntegrationTypeId equals i.IntegrationTypeId
                                             where (p.CompanyTypeId == 1 && p.CompanyId == compid)
                                             select new Companies
                                             {
                                                 CompanyId = p.CompanyId,
                                                 CompanyName = p.CompanyName,
                                                 City = p.City,
                                                 State = p.State,
                                                 Zip = p.Zip,
                                                 WebsiteURL = p.WebsiteURL,
                                                 IntegrationType = i.IntegrationCompany,
                                                 IsActive = p.IsActive,
                                                 CarriedId = p.CarriedId,
                                                 TaxID = p.TaxID,
                                                 Address = p.Address,
                                                 Fax = p.Fax,
                                                 Email = p.Email,
                                                 PhoneNumber = p.PhoneNumber,
                                                 IntegrationTypeId = p.IntegrationTypeId,
                                                 Logo = p.Logo,
                                                 UsesSharedDataset = p.UsesSharedDataset,
                                                 XACTBusinessUnit = p.XACTBusinessUnit,
                                                 EnableFTPDocExport = p.EnableFTPDocExport,
                                                 FTPUserName = p.FTPUserName,
                                                 HasDocumentBridge=p.DenyDocumentBridgeFlag,
                                                 IsAutoConfigureInvoice = p.IsAutoConfigureInvoice,
                                                 IsQRPServiceActive = p.QualityRepairProgram,
                                                 QBCustomerId = p.QBCustomerId//INPSW-39
                                                 



                                             };
            Companylist = objcomp;
            foreach (Companies comp in Companylist)
            {
                companyid = comp.CompanyId;
                CompanyName = comp.CompanyName;
                CarriedId = comp.CarriedId;
                TaxID = comp.TaxID;
                Address = comp.Address;
                City = comp.City;
                State = comp.State;
                Zip = comp.Zip;
                PhoneNumber = comp.PhoneNumber;
                WebsiteURL = comp.WebsiteURL;
                IntegrationTypeId = Convert.ToInt32(comp.IntegrationTypeId);
                Email = comp.Email;
                Fax = comp.Fax;
                if (comp.IsActive.HasValue)
                    IsActive = comp.IsActive.Value;
                else
                    IsActive = false;
                if (comp.IsAutoConfigureInvoice.HasValue)
                    IsAutoConfigureInvoice = comp.IsAutoConfigureInvoice.Value;
                else
                    IsAutoConfigureInvoice = false;
                if (comp.IsQRPServiceActive.HasValue)
                    IsQRPServiceActive = comp.IsQRPServiceActive.Value;
                else
                    IsQRPServiceActive = false;
                QBCustomerId = comp.QBCustomerId;//INPSW-39
                logo = comp.Logo;
                UsesSharedDataset = comp.UsesSharedDataset.HasValue ? comp.UsesSharedDataset.Value : false;
                XACTBusinessUnit = comp.XACTBusinessUnit;
                EnableFTPDocExport = comp.EnableFTPDocExport ?? false;
                HasDocumentBridge = comp.HasDocumentBridge ?? false;
                FTPUserName = comp.FTPUserName;

            }
            var objselecedregion = from p in css.CompanyRegions
                                   where (p.CompanyId == compid)
                                   select p;


            foreach (var items in objselecedregion)
            {
                region += ',' + items.RegionId.ToString();
            }
            FillDetails();
        }

        private void FillDetails()
        {
            StateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                //StateList.Add(Convert.ToString(state.StateProvinceCode), state.Name);
                StateList.Add(new SelectListItem { Text = state.Name, Value = Convert.ToString(state.StateProvinceCode).Trim() });
            }


            IntegrationTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (IntegrationType integrationtype in css.IntegrationTypes.ToList())
            {
                IntegrationTypeList.Add(new SelectListItem { Text = integrationtype.IntegrationCompany, Value = integrationtype.IntegrationTypeId.ToString() });
            }

            IsActiveList.Add(new SelectListItem { Text = "--Select--", Value = "--Select--" });
            IsActiveList.Add(new SelectListItem { Text = "All", Value = "" });
            IsActiveList.Add(new SelectListItem { Text = "Yes", Value = "1" });
            IsActiveList.Add(new SelectListItem { Text = "No", Value = "0" });

            //foreach (Region regions in css.Regions.ToList())
            //{
            //    //fill from region table                
            //    RegionTypeList.Add(new SelectListItem { Text = regions.Region1, Value = regions.RegionId.ToString() });
            //}
            RegionTypeList = new MultiSelectList(css.Regions.ToList(), "RegionId", "Region1");
        }
    }

    public class Companies
    {
        public int CompanyId { get; set; }
        public Nullable<int> CompanyTypeId { get; set; }
        public string CompanyName { get; set; }
        public Nullable<int> HeadCompanyId { get; set; }
        public string LineOfBusiness { get; set; }
        public string TaxID { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string PhoneNumber { get; set; }
        public string WebsiteURL { get; set; }
        public string Logo { get; set; }

        public bool? EnableFTPDocExport { get; set; }
        public string FTPAddress { get; set; }
        public string FTPUserName { get; set; }
        public string FTPPassword { get; set; }
        public string LetterLibray { get; set; }
        public string SpecialInstructions { get; set; }
        public string NeededDocuments { get; set; }
        public string ContractDocuments { get; set; }
        public string MilestoneDatesComments { get; set; }
        public Nullable<int> ParentCompanyId { get; set; }
        public string CarriedId { get; set; }
        public Nullable<byte> IntegrationTypeId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsQRPServiceActive { get; set; }
        public string QBCustomerId { get; set; }//INPSW-39
        public string IntegrationType { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public int RegionID { get; set; }
        public string RegionType { get; set; }
        public Nullable<bool> UsesSharedDataset { get; set; }
        public string XACTBusinessUnit { get; set; }
        public Nullable<decimal> StandardAdjusterRate { get; set; }
        public Nullable<decimal> GeneralAdjusterRate { get; set; }
        public Nullable<decimal> ExecutiveGeneralAdjusterRate { get; set; }
        public Nullable<int> FreeMiles { get; set; }
        public Nullable<decimal> RatePerMile { get; set; }
        public Nullable<int> NoOfPhotosIncluded { get; set; }
        public Nullable<decimal> RatePerPhoto { get; set; }
        public bool? HasDocumentBridge { get; set; }

        public bool? IsAutoConfigureInvoice { get; set; }
    }
}
