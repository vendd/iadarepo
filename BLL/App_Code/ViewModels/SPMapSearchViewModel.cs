﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Models;
using System.Web.Mvc;

namespace BLL.ViewModels
{
     [Serializable]
    public class SPMapSearchViewModel
    {
        public List<usp_SearchDispatchClaimsGetList_Result> ClaimList;
        public List<usp_SearchDispatchSPGetList_Result> Splist;
        public List<SAfilteredZip> spfilteredZip;
        public List<User> SPServiceproviders;
        //public string City { get; set; }
        //public string County { get; set; }
        public string AlphSelected { get; set; }

        //Pager for sp list
        public Pager Pager;

        //Location
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public int Distance { get; set; }
        public List<dropDownCls> StateList = new List<dropDownCls>();
        public string SPName { get; set; }
        public int Rank { get; set; }

        //Assignment 
        public byte SeverityLevel { get; set; }
        public int LossType { get; set; }
        public string CatCode { get; set; }
        public int InsuranceCompany { get; set; }

        public List<dropDownCls> SeverityLevelsList = new List<dropDownCls>();
        public List<dropDownCls> LossTypesList = new List<dropDownCls>();
        public List<dropDownCls> CatCodesList = new List<dropDownCls>();
        public List<dropDownCls> InsuranceCompaniesList = new List<dropDownCls>();

         //Hidden fields
        public string hdnSpidVal { get; set; }
        public string hdnASGIdVal { get; set; }
        public int hdnIsQuickAssignVal { get; set; }
        public string hdnsubmitTypeVal { get; set; }

       
        public SPMapSearchViewModel()
        {
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

            StateList.Add(new dropDownCls { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                StateList.Add(new dropDownCls { Text = state.Name, Value = state.StateProvinceCode.Trim() });
            }

            SeverityLevelsList.Add(new dropDownCls { Text = "--Select--", Value = "0" });
            foreach (SeverityLevel severitylevel in css.SeverityLevels.ToList())
            {
                //SeverityLevelsList.Add(severitylevel.SeverityLevelId, severitylevel.SeverityLevel1);
                SeverityLevelsList.Add(new dropDownCls { Text = severitylevel.SeverityLevel1, Value = Convert.ToString(severitylevel.SeverityLevelId) });
            }

            LossTypesList.Add(new dropDownCls { Text = "--Select--", Value = "0" });
            foreach (LossType losstype in css.LossTypes.ToList())
            {
                //LossTypesList.Add(losstype.LossTypeId, losstype.LossType1);
                LossTypesList.Add(new dropDownCls { Text = losstype.LossType1, Value = Convert.ToString(losstype.LossTypeId) });
            }

            CatCodesList.Add(new dropDownCls { Text = "--Select--", Value = "0" });

            foreach (CatCode catcode in css.CatCodes.ToList())
            {
                CatCodesList.Add(new dropDownCls { Text = catcode.CatCode1, Value = catcode.CatCode1 });
            }

            InsuranceCompaniesList.Add(new dropDownCls { Text = "--Select--", Value = "0" });
            List<GetCompaniesList_Result> insuranceCompaniesList = css.GetCompaniesList(1).ToList();
            foreach (var company in insuranceCompaniesList)
            {
                //PointofContactList.Add(Convert.ToInt32(p.ID),p.UserType);
                InsuranceCompaniesList.Add(new dropDownCls { Text = company.CompanyName, Value = Convert.ToString(company.CompanyId) });
            }
            //LoadDummyData();
        }
           

        public void LoadDummyData()
        {
            this.spfilteredZip = new List<SAfilteredZip>();
            SAfilteredZip zipDetails = new SAfilteredZip();
            zipDetails.SPAssignments = new List<SMAssignments>();
            SPServiceproviders = new List<User>();
            for (int j = 0; j < 3; j++)
            {
                User SP = new User();
                Claim cl = new Claim();

                cl.CarrierID = 10 + j;
                cl.PolicyNumber = "3" + j;
                cl.InsuredState = "NY";
                cl.ClaimNumber = "10" + j;


                switch (j)
                {
                    case 0:
                        SP.FirstName = "Jovan";
                        SP.LastName = " Macdonald";
                        cl.InsuredFirstName = "Andy";
                        cl.InsuredLastName = "Pandya ";
                        cl.ClaimCreatedDate = DateTime.Now.AddHours(2);
                        break;
                    case 1:
                        SP.FirstName = "Elian";
                        SP.LastName = " wills";
                        cl.InsuredFirstName = "Arjun";
                        cl.InsuredLastName = "Wenger ";
                        cl.ClaimCreatedDate = DateTime.Now.AddHours(1);
                        break;
                    case 2:
                        SP.FirstName = "jerry";
                        SP.LastName = " Austyn";
                        cl.InsuredFirstName = "jeremy";
                        cl.InsuredLastName = " j. ";
                        cl.ClaimCreatedDate = DateTime.Now.AddHours(3);
                        break;

                }
                cl.Company = new Company();
                cl.Company.CompanyName = "Choice Solutions Services Inc";
                cl.ClaimId = 10 + j;
                cl.InsuredZip = "10001";
                SP.Zip = "10001";
                SP.UserId = 11 + j;

                ServiceProviderDetail SPDetails = new ServiceProviderDetail();
                SPDetails.HAAGCertified = true;
                SPDetails.HAAGCertificationNumber = "HAAGCrtNo";
                SPDetails.HAAGExpirationDate = DateTime.Now.AddMonths(2);
                SP.ServiceProviderDetail = SPDetails;


                SMAssignments SMA = new SMAssignments();
                SMA.AssignmentId = 10 + j;
                SMA.ClaimDetail = cl;
                zipDetails.SPAssignments.Add(SMA);

                SPServiceproviders.Add(SP);
            }
            zipDetails.Latitude = "40.752050";
            zipDetails.Lontitude = "-73.994517";
            zipDetails.Zip = "10001";
            this.spfilteredZip.Add(zipDetails);

            SAfilteredZip zipDetails1 = new SAfilteredZip();
            zipDetails1.SPAssignments = new List<SMAssignments>();
            SPServiceproviders = new List<User>();
            for (int j = 0; j < 3; j++)
            {
                User SP = new User();
                Claim cl = new Claim();

                cl.PolicyNumber = "1" + j;
                cl.InsuredState = "NY";
                cl.CarrierID = 10 + j;
                cl.ClaimId = 10 + j;
                cl.InsuredZip = "00501";
                cl.ClaimNumber = "11" + j;
                SP.UserId = 10 + j;
                SP.Zip = "00501";

                switch (j)
                {
                    case 0:
                        SP.FirstName = "Ashton";
                        SP.LastName = " donald";
                        cl.InsuredFirstName = "Kaleb";
                        cl.InsuredLastName = "E'to ";
                        cl.ClaimCreatedDate = DateTime.Now.AddHours(2);
                        break;
                    case 1:
                        SP.FirstName = "Elian";
                        SP.LastName = " Dewills";
                        cl.InsuredFirstName = "Emmanuel";
                        cl.InsuredLastName = "Wenger ";
                        cl.ClaimCreatedDate = DateTime.Now.AddHours(5);

                        break;
                    case 2:
                        SP.FirstName = "Samir";
                        SP.LastName = "Nasri";
                        cl.InsuredFirstName = "David";
                        cl.InsuredLastName = "Backham";
                        cl.ClaimCreatedDate = DateTime.Now.AddHours(3);
                        break;

                }

                ServiceProviderDetail SPDetails = new ServiceProviderDetail();
                SPDetails.HAAGCertified = true;
                SPDetails.HAAGCertificationNumber = "HAAGCrtNo";
                SPDetails.HAAGExpirationDate = DateTime.Now.AddMonths(2);
                SP.ServiceProviderDetail = SPDetails;

                cl.Company = new Company();
                cl.Company.CompanyName = "Choice Solutions Services Inc";
                SMAssignments SMA = new SMAssignments();
                SMA.AssignmentId = 10 + j;
                SMA.ClaimDetail = cl;
                zipDetails1.SPAssignments.Add(SMA);
                SPServiceproviders.Add(SP);
            }

            zipDetails1.Latitude = "40.813078";
            zipDetails1.Lontitude = "-73.046388";
            zipDetails1.Zip = "00501";
            this.spfilteredZip.Add(zipDetails1);




            SAfilteredZip zipDetails2 = new SAfilteredZip();
            zipDetails2.SPAssignments = new List<SMAssignments>();
            SPServiceproviders = new List<User>();
            for (int j = 0; j < 3; j++)
            {
                User SP = new User();
                Claim cl = new Claim();

                cl.CarrierID = 10 + j;
                cl.PolicyNumber = "2" + j;
                cl.InsuredState = "NY";
                cl.ClaimId = 10 + j;
                cl.InsuredZip = "10002";
                cl.ClaimNumber = "12" + j;


                SP.UserId = 21 + j;
                SP.Zip = "10002";

                switch (j)
                {
                    case 0:
                        SP.FirstName = "Rayen";
                        SP.LastName = " Gigs";
                        cl.InsuredFirstName = "Andrew";
                        cl.InsuredLastName = "Flintoff ";
                        cl.ClaimCreatedDate = DateTime.Now.AddHours(2);

                        break;
                    case 1:
                        SP.FirstName = "Thierry";
                        SP.LastName = " Henry";
                        cl.InsuredFirstName = "Robert";
                        cl.InsuredLastName = "Pires ";
                        cl.ClaimCreatedDate = DateTime.Now.AddHours(2);

                        break;
                    case 2:
                        SP.FirstName = "Jack";
                        SP.LastName = "W.";
                        cl.InsuredFirstName = "Theo";
                        cl.InsuredLastName = "Chamberlin";
                        cl.ClaimCreatedDate = DateTime.Now.AddHours(2);

                        break;

                }

                cl.Company = new Company();
                cl.Company.CompanyName = "Choice Solutions Services Inc";
                ServiceProviderDetail SPDetails = new ServiceProviderDetail();
                SPDetails.HAAGCertified = true;
                SPDetails.HAAGCertificationNumber = "HAAGCrtNo";
                SPDetails.HAAGExpirationDate = DateTime.Now.AddMonths(2);
                SP.ServiceProviderDetail = SPDetails;


                SMAssignments SMA = new SMAssignments();
                SMA.AssignmentId = 10 + j;
                SMA.ClaimDetail = cl;
                zipDetails2.SPAssignments.Add(SMA);
                SPServiceproviders.Add(SP);
            }
            zipDetails2.Latitude = "40.715523";
            zipDetails2.Lontitude = "-73.988379";
            zipDetails2.Zip = "10002";

            this.spfilteredZip.Add(zipDetails2);
        }
    }


     public class dropDownCls
     {
         public string Text { get; set; }
         public string Value { get; set; }
         public bool Selected { get; set; }
     }


}

