﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Models;
using System.Web.Mvc;
namespace BLL.ViewModels
{
    [Serializable]
    public class ServiceProviderRegistrationViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public User user { get; set; }
            
        public Dictionary<string, string> CityList = new Dictionary<string, string>();
        public Dictionary<string, string> StateList = new Dictionary<string, string>();

        public List<ServiceOffering> ServiceOfferings = new List<ServiceOffering>();

        public List<CustomeDDL> HeadCompanyList = new List<CustomeDDL>();

        public int[] SelectedServiceTypes { get; set; }

        public List<InternalContactGetList_Result> InternalContactGetList = new List<InternalContactGetList_Result>();

        public int LobId { get; set; }
        public int ClientPOCId { get; set; }
        public int NetworkProvider { get; set; }
        public Boolean IsIadaMember { get; set; }
        public String QBVendorId { get; set; }//INPSW-39

        public Boolean IsDefault { get; set; }
        public ServiceProviderRegistrationViewModel()
        {
            populateLists();

            #region Default Initialization
            user = new User();
            user.UserId = -1;
            user.ServiceProviderDetail = new ServiceProviderDetail();
            user.ServiceProviderDetail.IsKnowByOtherNames = false;
            user.ServiceProviderDetail.HasValidPassport = false;
            user.ServiceProviderDetail.Rank = 5;
            user.Company1 = new Company();
            IsIadaMember = false;

            #endregion
        }
        public ServiceProviderRegistrationViewModel(Int64 userId)
        {
            css.Configuration.ProxyCreationEnabled = true;
            user = css.Users.Find(userId);

            if (user.ServiceProviderDetail == null)
                user.ServiceProviderDetail = css.ServiceProviderDetails.Find(userId);
                var ServiceProviderDetails = css.ServiceProviderDetails.Find(userId);
                QBVendorId = ServiceProviderDetails.QBVendorId;//INPSW-39
 
            if (user.ServiceProviderServiceofferings == null)
                user.ServiceProviderServiceofferings = css.ServiceProviderServiceofferings.Where(x => x.UserId == userId).ToList();
            
             SelectedServiceTypes = user.ServiceProviderServiceofferings.Select(x => x.ServiceId.Value).ToArray();
              
            populateLists();
        }
   
        public ServiceProviderRegistrationViewModel(User user)
        {
            this.user = user;
            populateLists();
    }
        private void populateLists()
        {
            StateList.Add("0", "--Select--");
            HeadCompanyList.Clear();
            HeadCompanyList.Add(new CustomeDDL { Text = "--Select--", Value = "0", IsMark = false });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                StateList.Add(Convert.ToString(state.StateProvinceCode), state.Name);
            }

            foreach (Company Company in css.Companies.Where(x => x.HeadCompanyId == null && (x.CompanyTypeId ?? 0) == 3 && (x.IsActive ?? false) == true).ToList().OrderBy(x => x.CompanyName))
            {
                HeadCompanyList.Add(new CustomeDDL { Text = Company.CompanyName, Value = Company.CompanyId.ToString(), IsMark = Company.IsIadaMember??false });

            }
            //foreach (ServiceOffering ServiceOffering in css.ServiceOfferings.ToList())
            //{
            //    ServiceOfferings.Add(new SelectListItem { Text = ServiceOffering.ServiceDescription, Value = ServiceOffering.ServiceId.ToString() });
            //}
            css.Configuration.ProxyCreationEnabled = false;
            ServiceOfferings = css.ServiceOfferings.ToList();
         
        }
    }
    public class CustomeDDL
    {
        public String Value { get; set; }
        public String Text { get; set; }
        public Boolean IsMark { get; set; }
    }
}
