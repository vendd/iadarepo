﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Models;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace BLL.ViewModels
{
    [Serializable]
    public class PayrollSummarySelection
    {
        public string PAId;
        public bool IsSelected;
    }
     [Serializable]
    public class PayrollSummaryReportViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
         public List<SelectListItem> ServiceOfferingsList = new List<SelectListItem>();
        [DisplayName("End Date")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime EndDate {get;set;}
        public int UserTypeId { get; set; } //0->Both 1-> Service Provider 8->QA Agent(Examiner)
        public int ServiceTypeID { get; set; }
        public List<PayrollSummaryReportModel> PayrollSummaryReport { get; set; }
        public List<PayrollSummarySelection> PayrollSummarySelection { get; set; }

        public PayrollSummaryReportViewModel()
        {

            ServiceTypeID = 0;

            ServiceOfferingsList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (ServiceOffering serviceoffering in css.ServiceOfferings.ToList())
            {
                //ServiceOfferingsList.Add(serviceoffering.ServiceId, serviceoffering.ServiceDescription);
                ServiceOfferingsList.Add(new SelectListItem { Text = serviceoffering.ServiceDescription, Value = Convert.ToString(serviceoffering.ServiceId) });
            }
         }
    }
}
