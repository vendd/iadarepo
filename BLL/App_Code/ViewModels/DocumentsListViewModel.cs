﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL;
using System.Web.Mvc;
namespace BLL.ViewModels
{
    [Serializable]
    public class DocumentsListViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public IEnumerable<ClaimDocumentTypesRS> ClaimDocumentTypeList { get; set; }
        public Int64 ClaimId { get; set; }
        public Int64 AssignmentId { get; set; }
        public ClaimDocumentType ClaimDocumentType { get; set; }
        public short DocumentTypeId { get; set; }
        public string ClaimParticipantsEmailAddresses { get; set; }
        public bool IsFTPDocExportEnabled { get; set; }
        public List<DocumentsList_Result> DocumentsList { get; set; }
        public List<DocumentsListResult> DocumentsListMail { get; set; }
        public List<usp_DocumentsPreviousGetList_Result> DocumentsPreviousList { get; set; }
        public List<RejectedDocumentsList_Result> DocumentsRejectedList { get; set; }
        public List<SelectListItem> StatusList { get; set; }
        public byte StatusId { get; set; }
        public string FirstDocument { get; set; }
        BLL.Claim claim { get; set; }
        public byte[] FirstFilebyte { get; set; }
        //email doc
        public string subject { get; set; }
        public string Body { get; set; }//INPSW-11
        public string authReplayUser { get; set; }

        public DocumentsListViewModel()
        {
        }
        public DocumentsListViewModel(Int64 claimId, Int64 assignmentId, short documentTypeId)
        {
            ClaimId = claimId;
            AssignmentId = assignmentId;
            DocumentTypeId = documentTypeId;
            //ClaimDocumentTypeList = css.ClaimDocumentTypes.ToList();
            ClaimDocumentTypeList = from docType in css.ClaimDocumentTypes
                                    select new ClaimDocumentTypesRS()
                                    {
                                        DocumentTypeId = docType.DocumentTypeId,
                                        DocumentTypeDesc = docType.DocumentTypeDesc,
                                        GenerateForm = docType.GenerateForm,
                                       // DocCount = css.AutoDocuments.Where(x => x.AutoAssignmentId == assignmentId && x.DocumentTypeId == docType.DocumentTypeId).Count()
                                        DocCount = (from AD in css.AutoDocuments join AA in css.AutoAssignments on AD.AutoAssignmentId equals AA.AutoAssignmentId where (AA.ClaimId == ClaimId) && (AD.DocumentTypeId == docType.DocumentTypeId) select AD.AutoDocumentId).Count()

                                    };

            //ClaimDocumentTypeList = from docType in css.ClaimDocumentTypes
            //            select new ClaimDocumentTypesRS()
            //            {
            //                 DocCount = from AD in css.AutoDocuments
            //                 join AA in css.AutoAssignments on AD.AutoAssignmentId equals  AA.AutoAssignmentId
            //                 where(AA.claimid == ClaimId) && (AD.DocumentTypeId == 5;
            //            }

        }

    }
        public class DocumentsListResult
        {
            public long DocumentId { get; set; }
            public long AssignmentId { get; set; }
            public string Title { get; set; }

            public Nullable<Boolean> isselctet { get; set; }

        }
        public class ClaimDocumentTypesRS
        {
            public short DocumentTypeId { get; set; }
            public string DocumentTypeDesc { get; set; }
            public Nullable<bool> GenerateForm { get; set; }
            public int DocCount { get; set; }

        }


    
}
