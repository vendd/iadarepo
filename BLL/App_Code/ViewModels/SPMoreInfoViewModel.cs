﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
   [Serializable]
   public class SPMoreInfoViewModel
    {
      public long UserId { get; set; }
      public usp_GetSPMapDetails_Result result { get; set; }
      public IEnumerable<usp_GetServiceProviderServiceRates_Result> SPServiceRate { get; set; }
    }
}
