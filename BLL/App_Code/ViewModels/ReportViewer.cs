﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BLL.ViewModels
{
    [Serializable]
    public class ReportViewer
    {
        public String ReportURL { get; set; }
        public String ReportTitle { get; set; }
        public Boolean BackButtonDisplay { get; set; }
    }
}