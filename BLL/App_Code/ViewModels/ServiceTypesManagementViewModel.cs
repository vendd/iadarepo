﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL.Models;

namespace BLL.ViewModels
{
    public class ServiceTypesManagementViewModel
    {
        public List<ServiceOffering> ServiceTypes { get; set; }
        public Pager Pager { get; set; }
        public string searchTypesListSearchResult { get; set; }
    }
}
