﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    [Serializable]
    public class InvoiceTrackerViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public String ReportURL { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? AssignmentMonth { get; set; }
        public DateTime? AssignmentYear { get; set; }
        public ReportViewer ReportViewer { get; set; }
        public Boolean RptFilterDisplay { get; set; }
        public Boolean RptViewerDisplay { get; set; }

        public int? InsuranceCompany { get; set; }
        public int? ClaimRepresentativeID { get; set; }
        public List<SelectListItem> ClaimRepresentativeList = new List<SelectListItem>();
        public List<SelectListItem> InsuranceCompaniesList = new List<SelectListItem>();

        public InvoiceTrackerViewModel()
        {
            populateLists();
        }
        private void populateLists()
        {
            //Claim Representative is retrieved dynamically
            ClaimRepresentativeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            InsuranceCompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            List<GetCompaniesList_Result> insuranceCompaniesList = css.GetCompaniesList(1).ToList();
            foreach (var company in insuranceCompaniesList)
            {
                //PointofContactList.Add(Convert.ToInt32(p.ID),p.UserType);
                InsuranceCompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = Convert.ToString(company.CompanyId) });
            }
        }
    }
}
