﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
   [Serializable]
   public class MultiFileUploadViewModel
    {
       public long AssignmentID { get; set; }
       public int DocTypeID { get; set; }

       public IEnumerable<ClaimDocumentTypes> DocumentList { get; set; }
       public MultiFileUploadViewModel()
       {
           ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

           DocumentList = from sType in css.ClaimDocumentTypes
                          select new ClaimDocumentTypes()
                              {
                                  ClaimDocId = sType.DocumentTypeId,
                                  ClaimDocDescription = sType.DocumentTypeDesc,

                              };
       }
    }
   public class ClaimDocumentTypes
   {
       public short ClaimDocId { get; set; }
       public string ClaimDocDescription { get; set; }
   }
}
