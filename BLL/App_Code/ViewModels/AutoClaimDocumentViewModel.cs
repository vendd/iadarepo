﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BLL.ViewModels
{
    public class AutoClaimDocumentViewModel
    {
        public AutoDocument AutoDocument { get; set; }
        public Int64 AutoAssignmentId{get;set;}
        public Nullable<short> DocumentTypeId { get; set; }
        public Int64 ClaimId { get; set; }
        public string documentIds{get;set;}
        public byte newStatusId{get;set;}
        public AutoClaimDocumentViewModel()
        {
            this.AutoDocument = new AutoDocument();
        }
    }
      
}
