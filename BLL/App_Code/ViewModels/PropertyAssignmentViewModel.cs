﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
namespace BLL.ViewModels
{
    [Serializable]
    public class PropertyAssignmentViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        //  public User user { get; set; }
        public Claim Claim { get; set; }
        public CatCodeViewModel CatCodeViewModel { get; set; }
        public bool isInsuredInfoSameAsLossInfo { get; set; }
        public bool UseMasterPageLayout { get; set; }
        public bool isRedirectToTriage { get; set; }
        //public Claim Claim = new Claim();
        public AutoAssignment propertyassignment { get; set; }
        public PropertyAssignment pa = new PropertyAssignment();
        public List<SelectListItem> StateList = new List<SelectListItem>();
        public List<SelectListItem> InsuredStateList = new List<SelectListItem>();
        public List<SelectListItem> CatCodesList = new List<SelectListItem>();
        public List<SelectListItem> SeverityLevelsList = new List<SelectListItem>();
        public List<SelectListItem> ServiceOfferingsList = new List<SelectListItem>();
        public List<SelectListItem> LineOfBusinessList = new List<SelectListItem>();
        public List<SelectListItem> LossTypesList = new List<SelectListItem>();
        public List<SelectListItem> PointofContactList = new List<SelectListItem>();
        public List<SelectListItem> CSSQAAgentList = new List<SelectListItem>();
        public List<SelectListItem> InsuranceCompaniesList = new List<SelectListItem>();
        public List<SelectListItem> ClientPointofContactList = new List<SelectListItem>();
        public List<SelectListItem> CoverageTypesList = new List<SelectListItem>();
        public List<SelectListItem> FileStatusList = new List<SelectListItem>();
        public List<SelectListItem> ClaimTypeList = new List<SelectListItem>();
        public PropertyAssignmentViewModel()
        {
            this.Claim = new Claim();
            this.Claim.ClaimId = -1;
            this.CatCodeViewModel = new CatCodeViewModel();
            this.propertyassignment = new AutoAssignment();
            if (Claim.CauseTypeOfLoss == null || Claim.CauseTypeOfLoss <= 0)
            {

                Claim.CauseTypeOfLoss = 0;
            }
            if (Claim.LOBId == null || Claim.LOBId <= 0)
            {

                Claim.LOBId = 0;
            }
            if (Claim.ServiceOffering == null || Claim.ServiceOffering <= 0)
            {

                Claim.ServiceOffering = 0;
            }
            if (Claim.SeverityLevel == null || Claim.SeverityLevel <= 0)
            {

                Claim.SeverityLevel = 0;
            }
            if (propertyassignment.CSSPointofContactUserId == null || propertyassignment.CSSPointofContactUserId <= 0)
            {

                propertyassignment.CSSPointofContactUserId = 0;
            }
            if (propertyassignment.CSSQAAgentUserId == null || propertyassignment.CSSQAAgentUserId <= 0)
            {

                propertyassignment.CSSQAAgentUserId = 0;

            }
            //if (propertyassignment.OAUserID == null || propertyassignment.OAUserID <= 0)
            //{

            //        propertyassignment.OAUserID = 0;
            //}
            populateLists();

        }
        public PropertyAssignmentViewModel(Int64 claimid)
        {
            this.CatCodeViewModel = new CatCodeViewModel();
            Claim = css.Claims.Find(claimid);
            if (String.IsNullOrEmpty(Claim.CatCode))
                Claim.CatCode = "0";
            if (Claim.CauseTypeOfLoss == null || Claim.CauseTypeOfLoss <= 0)
            {

                Claim.CauseTypeOfLoss = 0;
            }
            if (Claim.LOBId == null || Claim.LOBId <= 0)
            {

                Claim.LOBId = 0;
            }
            if (Claim.ServiceOffering == null || Claim.ServiceOffering <= 0)
            {

                Claim.ServiceOffering = 0;
            }
            if (Claim.SeverityLevel == null || Claim.SeverityLevel <= 0)
            {

                Claim.SeverityLevel = 0;
            }
            //if (Claim.DateofLoss != null)
            //{

            //    Claim.DateofLoss = Convert.ToDateTime(Claim.DateofLoss);
            //}

            pa.AssignmentId = (from pass in css.AutoAssignments
                               where pass.ClaimId == Claim.ClaimId
                               orderby pass.AutoAssignmentId descending
                               select pass.AutoAssignmentId).First();
            propertyassignment = css.AutoAssignments.Find(pa.AssignmentId);
            if (propertyassignment.CSSPointofContactUserId == null || propertyassignment.CSSPointofContactUserId <= 0)
            {

                propertyassignment.CSSPointofContactUserId = 0;
            }
            if (propertyassignment.CSSQAAgentUserId == null || propertyassignment.CSSQAAgentUserId <= 0)
            {

                propertyassignment.CSSQAAgentUserId = 0;

            }
            //if (propertyassignment.AssignmentDate != null)
            //{

            //    propertyassignment.AssignmentDate = Convert.ToDateTime(propertyassignment.AssignmentDate.Value.ToString("MM/dd/yyyy")) ;
            //}
            //if (propertyassignment.OAUserID == null || propertyassignment.OAUserID <= 0)
            //{

            //    propertyassignment.OAUserID = 0;
            //}

            //user = css.Users.Find(spUserId);


            //propertyassignment1.AssignmentId = (from pa in css.PropertyAssignments
            //                                    where pa.OAUserID == spUserId
            //                                    select pa.AssignmentId).ToList().First();
            //propertyassignment = css.PropertyAssignments.Find(propertyassignment1.AssignmentId);
            //claim1.ClaimId = (from claims in css.Claims
            //                  where claims.ClaimId == propertyassignment.ClaimId
            //                  select claims.ClaimId).ToList().First();
            //claim = css.Claims.Find(claim1.ClaimId);

            populateLists();


        }

        public PropertyAssignmentViewModel(PropertyAssignmentViewModel viewmodel)
        {
            Claim = viewmodel.Claim;
            propertyassignment = viewmodel.propertyassignment;
            populateLists();
        }


        private void populateLists()
        {

            StateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                //StateList.Add(Convert.ToString(state.StateProvinceCode), state.Name);
                StateList.Add(new SelectListItem { Text = state.Name, Value = Convert.ToString(state.StateProvinceCode).Trim() });
            }
            InsuredStateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                //InsuredStateList.Add(Convert.ToString(state.StateProvinceCode), state.Name);
                InsuredStateList.Add(new SelectListItem { Text = state.Name, Value = Convert.ToString(state.StateProvinceCode).Trim() });
            }

            CatCodesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            foreach (CatCode catcode in css.CatCodes.ToList())
            {
                CatCodesList.Add(new SelectListItem { Text = catcode.CatCode1, Value = catcode.CatCode1 });
            }
            //if the CatCode stored in Claims table is not available in the DDL then add it to the list
            if (Claim != null)
            {
                if (!String.IsNullOrEmpty(Claim.CatCode))
                {
                    if (CatCodesList.Where(x => x.Value == Claim.CatCode).ToList().Count == 0)
                    {
                        CatCodesList.Add(new SelectListItem { Text = Claim.CatCode, Value = Claim.CatCode });
                    }
                }
            }

            //Claim Types
            ClaimTypeList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (ClaimType claimtype in css.ClaimTypes.ToList())
            {
                ClaimTypeList.Add(new SelectListItem { Text = claimtype.Description, Value = Convert.ToString(claimtype.ClaimTypeId) });
            }

            SeverityLevelsList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (SeverityLevel severitylevel in css.SeverityLevels.ToList())
            {
                //SeverityLevelsList.Add(severitylevel.SeverityLevelId, severitylevel.SeverityLevel1);
                SeverityLevelsList.Add(new SelectListItem { Text = severitylevel.SeverityLevel1, Value = Convert.ToString(severitylevel.SeverityLevelId) });
            }
            ServiceOfferingsList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (ServiceOffering serviceoffering in css.ServiceOfferings.ToList())
            {
                //ServiceOfferingsList.Add(serviceoffering.ServiceId, serviceoffering.ServiceDescription);
                ServiceOfferingsList.Add(new SelectListItem { Text = serviceoffering.ServiceDescription, Value = Convert.ToString(serviceoffering.ServiceId) });
            }
            LineOfBusinessList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            //List < LineOfBusinessGetList_Result > LObList = css.LineOfBusinessGetList(Claim.HeadCompanyId).ToList();
            //foreach (var lob in LObList)
            //{
            //    LineOfBusinessList.Add(new SelectListItem { Text = lob.LOBDescription, Value = lob.LOBId + "" });
            //}
            //foreach (LineOfBusiness lineofbusiness in css.LineOfBusinesses.ToList())
            //{
            //    //LineOfBusinessList.Add(lineofbusiness.LOBId, lineofbusiness.LOBDescription);
            //    LineOfBusinessList.Add(new SelectListItem { Text = lineofbusiness.LOBDescription, Value = Convert.ToString(lineofbusiness.LOBId) });
            //}
            LossTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (LossType losstype in css.LossTypes.ToList())
            {
                //LossTypesList.Add(losstype.LossTypeId, losstype.LossType1);
                LossTypesList.Add(new SelectListItem { Text = losstype.LossType1, Value = Convert.ToString(losstype.LossTypeId) });
            }
            PointofContactList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            List<CSSPointOfContactList_Result> cssPOCList = css.CSSPointOfContactList().ToList();
            foreach (var poc in cssPOCList)
            {
                //PointofContactList.Add(Convert.ToInt32(p.ID),p.UserType);
                PointofContactList.Add(new SelectListItem { Text = poc.UserFullName, Value = Convert.ToString(poc.UserId) });
            }

            InsuranceCompaniesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            List<GetCompaniesList_Result> insuranceCompaniesList = css.GetCompaniesList(1).ToList();
            foreach (var company in insuranceCompaniesList)
            {
                //PointofContactList.Add(Convert.ToInt32(p.ID),p.UserType);
                InsuranceCompaniesList.Add(new SelectListItem { Text = company.CompanyName, Value = Convert.ToString(company.CompanyId) });
            }

            CoverageTypesList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            List<CoverageType> coverageTypesList = css.CoverageTypes.ToList();
            foreach (var coverageType in coverageTypesList)
            {

                CoverageTypesList.Add(new SelectListItem { Text = coverageType.CoverageType1, Value = Convert.ToString(coverageType.CoverageTypeId) });
            }

            //ClientPOC is retrieved dynamically
            ClientPointofContactList.Add(new SelectListItem { Text = "--Select--", Value = "0" });

            CSSQAAgentList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            List<GetCSSQAAgentList_Result> cssQAAgentList = css.GetCSSQAAgentList().ToList();
            foreach (var cssQAAgent in cssQAAgentList)
            {
                CSSQAAgentList.Add(new SelectListItem { Text = cssQAAgent.UserFullName, Value = Convert.ToString(cssQAAgent.UserId) });
            }

            FileStatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (FileStatusGetList_Result filestatus in css.FileStatusGetList())
            {
                FileStatusList.Add(new SelectListItem { Text = filestatus.StatusDescription, Value = filestatus.StatusId.ToString() });
            }

        }
    }
}
