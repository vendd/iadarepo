﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
    public class SPMapSearchNewViewModel
    {
        ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public int Distance { get; set; }
        public string SPName { get; set; }
        public int Rank { get; set; }
        public int ServiceType { get; set; }
        public int InsuranceCompany { get; set; }
        public bool IsLoggedIN { get; set; }
        public string assignmentSearchDet { get; set; }
        public bool IsActiveSP { get; set; }
        public bool SPActiveStatus { get; set; }      
        public int SelectedServiceIdM { get; set; }//to set servicetype
        public Int64  AutoassignmentId { get; set; }//Set autoassignmentid  Inpsw-32
        public IEnumerable<ServiceOfferingsNew> ServiceOfferings { get; set; }

        ///////////////////////////////////////////////////////////////////  New Change
        public IEnumerable<ServiceTypesNew> ServiceTypes { get; set; }
        ///////////////////////////////////////////////////////////////////

        public IEnumerable<CustomeDDL> HeadCompanyList { get; set; }
        public List<AutoSearchDispatchSPGetList_Result> Splist;
        public List<User> SPServiceproviders;
        public AddressDetails AddressDetails { get; set; }

        public string favissue { get; set; }
        public SPMapSearchType SPMapSearchtype { get; set; }

        public dynamic CompaniesList { get; set; }
        //public List<CustomeDDL> HeadCompanyList = new List<CustomeDDL>();
        public SPMapSearchNewViewModel()
        {
            SPActiveStatus = true;
            fillList();
            ServiceType = 0;
        }
        public void fillList()
        { 
        ServiceOfferings=from sType in css.ServiceOfferings
                         orderby sType.ServiceDescription
                        select new ServiceOfferingsNew()
                        {
                            ServiceId = sType.ServiceId,
                            ServiceDescription = sType.ServiceDescription,
                                 
                            };

        HeadCompanyList = css.Companies.Where(x => x.HeadCompanyId == null && (x.CompanyTypeId ?? 0) == 3 && (x.IsIadaMember ?? false) == true && (x.IsActive ?? false) == true).ToList().OrderBy(x => x.CompanyName).Select(y => new CustomeDDL { Text = y.CompanyName, Value = y.CompanyId.ToString() });

     
        //HeadCompanyList.Clear();
        //HeadCompanyList.Add(new CustomeDDL { Text = "--Select--", Value = "0", IsMark = false });
        //foreach (Company Company in css.Companies.Where(x => x.HeadCompanyId == null &&(x.IsIadaMember??false)==true).ToList().OrderBy(x => x.CompanyName))
        //{
        //    HeadCompanyList.Add(new CustomeDDL { Text = Company.CompanyName, Value = Company.CompanyId.ToString() });
        //}

        /////////////////////////////////////////////////////////////////////// New Change
        ServiceTypes = from sType in css.ServiceTypes
                        orderby sType.ServiceTypeId
                        select new ServiceTypesNew()
                        {
                            ServiceTypeId = sType.ServiceTypeId,
                            ServiceName = sType.ServiceName,
                        };
        /////////////////////////////////////////////////////////////////////////
        }
    }
    public class AddressDetails
    {
        public String State { get; set; }
        public String StateLong { get; set; }
        public String country { get; set; }
        public String countryShort { get; set; }
        public String latitude { get; set; }
        public String City { get; set; }
        public String longitude { get; set; }
        public String Zip { get; set; }
    }
    public class ServiceOfferingsNew {
        public int ServiceId { get; set; }
        public string ServiceDescription { get; set; }
     }

    ///////////////////////////////////////////////////////////////////// New Change
     public class ServiceTypesNew
    {
        public int ServiceTypeId { get; set; }
        public string ServiceName { get; set; }
    }   
    //////////////////////////////////////////////////////////////////////////////// 

    public enum SPMapSearchType
    { 
       MainSearch=0,
       ActiveLoc=1,
       Fevorite=2
    }
  
}
