﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BLL.Models;

namespace BLL.ViewModels
{
    [Serializable]
    public class TriageViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public bool isReadOnly { get; set; }
        public Triage Triage = new Triage();
        public List<SelectListItem> StateList = new List<SelectListItem>();
        public List<SelectListItem> SeverityLevelsList = new List<SelectListItem>();
        public List<GetClaimParticipants_Result> ClaimParticipantsList { get; set; }

        public List<usp_GetTriageDetails_Result> Triagedetails;
        public TriageViewModel()
        {
            populateLists();
        }
        private void populateLists()
        {
            StateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                StateList.Add(new SelectListItem { Text = state.Name, Value = state.StateProvinceCode.Trim() });
            }

            SeverityLevelsList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (SeverityLevel severitylevel in css.SeverityLevels.ToList())
            {
                SeverityLevelsList.Add(new SelectListItem { Text = severitylevel.SeverityLevel1, Value = Convert.ToString(severitylevel.SeverityLevelId) });
            }

        }
    }

}
