﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.ViewModels
{
    [Serializable]
    public class ServiceProviderDocumentsViewModel
    {
        public bool IsReadOnly { get; set; }
        public int DisplayMode { get; set; }//LIST = 0, IMAGE = 1 
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
        public ServiceProviderDocument ServiceProviderDocument { get; set; }
        public int MaxDocsCount { get; set; } // 0 - unlimited

        public IEnumerable<ServiceProviderDocument> ServiceProvidersDocuments { get; set; }
        public ServiceProviderDocumentsViewModel(Int64 userId, int documentTypeId, int maxDocsCount, int displayMode, bool isReadOnly)
        {
            ServiceProviderDocument = new BLL.ServiceProviderDocument();
            ServiceProviderDocument.UserId = userId;
            ServiceProviderDocument.DTId = documentTypeId;
            ServiceProvidersDocuments = css.ServiceProviderDocuments.Where(x => (x.UserId == userId) && (x.DTId == documentTypeId));
            MaxDocsCount = maxDocsCount;
            DisplayMode = displayMode;
            IsReadOnly = isReadOnly;
        }
        public ServiceProviderDocumentsViewModel()
        {
            ServiceProviderDocument = new BLL.ServiceProviderDocument();
            MaxDocsCount = 0;
            DisplayMode = 0;
            IsReadOnly = true;
        }
    }
}
