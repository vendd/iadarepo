﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BLL;
namespace BLL.ViewModels
{
    [Serializable]
    public class PayrollViewModel
    {
        public Int64 ClaimId { get; set; }
        public List<usp_SPPayrollAndAdjGetList_Result> SPPayrollAndAdjList = new List<usp_SPPayrollAndAdjGetList_Result>();
        public List<usp_SPPayrollAndAdjAutoGetList_Result> SPPayrollandAdjAutoList = new List<usp_SPPayrollAndAdjAutoGetList_Result>();//INPSW -41
        public bool HasReopen { get; set; }
        public PayrollViewModel()
        {

        }
    }
}
