﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BLL.Models;
using Newtonsoft.Json;
namespace BLL.ViewModels
{
    [Serializable]
    public class ServiceProviderSearchViewModel
    {
        public string ActiveMainTab { get; set; }

        //Company
        public string Status { get; set; }
        public string UserName { get; set; }
        public string ContactName { get; set; }
        public int Rank { get; set; }
        public string Email { get; set; }
        public string MobilePhone { get; set; }
     
        //Location
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public int Distance { get; set; }
        public string IsIADAMember { get; set; }
        public string sortBy { get; set; }
        public string sortString { get; set; }
        public Int64 RecordCount { get; set; }

        public DateTime? RegisteredDateFrom { get; set; }
        public DateTime? RegisteredDateTo { get; set; }
        public DateTime? ApprovedDateFrom { get; set; }
        public DateTime? ApprovedDateTo { get; set; }

        public Pager Pager;
        public SPSearchMapViewModel MapViewModel { get; set; }

        public Int64 HeadCompanyID { get; set; }
        public List<CustomeDDL> HeadCompanyList = new List<CustomeDDL>();
        //Lists
        [JsonIgnore]
        public List<ServiceProviderDetail> ServiceProviders { get; set; }
        public List<usp_ServiceProviderSearch_Result> ServiceProvidersList { get; set; }
        public List<SelectListItem> StateList = new List<SelectListItem>();
  

        public ServiceProviderSearchViewModel()
        {
            Status = "Active";
            UserName = "";
            ContactName = "";
            Rank = 0;
            Email = "";
            MobilePhone = "";
            City = "";
            State = "";
            Zip = "";
            Distance = 0;
            IsIADAMember = "";
            populateLists();
        }
        public void populateLists()
        {
            ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();
            StateList.Clear();
            HeadCompanyList.Clear();
            StateList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
          
            foreach (StateProvince state in css.StateProvinces.ToList())
            {
                StateList.Add(new SelectListItem { Text = state.Name, Value = state.StateProvinceCode });
            }
            HeadCompanyList.Add(new CustomeDDL { Text = "--Select--", Value = "0" });
            foreach (Company Company in css.Companies.Where(x => x.HeadCompanyId == null && (x.CompanyTypeId ?? 0) == 3 && (x.IsActive??false)==true).ToList().OrderBy(x => x.CompanyName))
            {
                HeadCompanyList.Add(new CustomeDDL { Text = Company.CompanyName, Value = Company.CompanyId.ToString(), IsMark = Company.IsIadaMember ?? false });

            }

  //Commented for IADA 10-18-2016
            //ExperienceClaimTypeList.Clear();
            //ExperienceClaimTypeList.Add(new DDListItem { Text = "--Select--", Value = "0" });
            //foreach (ClaimType claimType in css.ClaimTypes.ToList())
            //{
            //    ExperienceClaimTypeList.Add(new DDListItem { Text = claimType.Description, Value = claimType.ClaimTypeId.ToString() });
            //}
            //FloodClaimTypeList.Clear();
            //FloodClaimTypeList.Add(new DDListItem { Text = "--Select--", Value = "0" });
            //foreach (ClaimType claimType in css.ClaimTypes.ToList())
            //{
            //    FloodClaimTypeList.Add(new DDListItem { Text = claimType.Description, Value = claimType.ClaimTypeId.ToString() });
            //}

            //SoftwareExperienceList.Clear();
            //SoftwareExperienceList.Add(new DDListItem { Text = "MSB", Value = "MSB" });
            //SoftwareExperienceList.Add(new DDListItem { Text = "Xactimate", Value = "Xactimate" });
            //SoftwareExperienceList.Add(new DDListItem { Text = "Symbility", Value = "Symbility" });
            //SoftwareExperienceList.Add(new DDListItem { Text = "Simsol", Value = "Simsol" });
            //SoftwareExperienceList.Add(new DDListItem { Text = "Other/word/excel", Value = "Other" });

            //CapabilityToClimbSteepList.Clear();
            //CapabilityToClimbSteepList.Add(new DDListItem { Text = "N/A", Value = "0", Selected = true });
            //CapabilityToClimbSteepList.Add(new DDListItem { Text = "6/12", Value = "6/12" });
            //CapabilityToClimbSteepList.Add(new DDListItem { Text = "8/12", Value = "8/12" });
            //CapabilityToClimbSteepList.Add(new DDListItem { Text = "10/12", Value = "10/12" });
            //CapabilityToClimbSteepList.Add(new DDListItem { Text = "12/12+", Value = "12/12+" });

            //CapabilityToClimbList.Clear();
            //CapabilityToClimbList.Add(new DDListItem { Text = "N/A", Value = "0", Selected = true });
            //CapabilityToClimbList.Add(new DDListItem { Text = "1 Story", Value = "1 Story" });
            //CapabilityToClimbList.Add(new DDListItem { Text = "2 Story", Value = "2 Story" });
            //CapabilityToClimbList.Add(new DDListItem { Text = "Above 2 Story", Value = "Above 2 Story" });
        }
    }
}
