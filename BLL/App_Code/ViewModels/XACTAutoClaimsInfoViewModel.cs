﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using BLL.Models;

namespace BLL.ViewModels
{
    public class XACTAutoClaimsInfoViewModel
    {
        private ChoiceSolutionsEntities css = new ChoiceSolutionsEntities();

        public string ActiveMainTab { get; set; }
        public string VehicleLocationName { get; set; }
        public Claim claim = new Claim();
        public AutoAssignment AutoAssignment = new AutoAssignment();
        public Note note = new Note();
        public Note Note { get; set; }
        public string ApprovalComment { get; set; }
        public Nullable<int> Rank { get; set; }
        public List<AutoNotesHistoryGetList_Result> AutoNoteHistory { get; set; }
        public List<usp_AutoInvoiceSummaryGetList_Result> AutoInvoiceList { get; set; }

        public VehicleClaimDetail VehicleClaimDetail = new VehicleClaimDetail();
        public string NugenVehicleLocationName { get; set; }

        public List<SelectListItem> FileStatusList = new List<SelectListItem>();
        public AutoAssignment pa { get; set; }

        public Nullable<bool> RejectionMark { get; set; }
        public Nullable<int> RejectionCount { get; set; }
        public Nullable<int> CheckApprovedDate { get; set; }
        public Nullable<Double> DaysToContact { get; set; }
        public Nullable<Double> DaysToInspect { get; set; }
        public Nullable<Double> DaysToUpload { get; set; }

        public bool IsNuGenClaim { get; set; }
        public bool IsNonNugenIntegratedClaim { get; set; }

        public long SPID { get; set; }
        public string DisableFileStatus { get; set; } //INPSW - 27
        public XACTAutoClaimsInfoViewModel()
        {
            PopulateLists(); 
        }

        public XACTAutoClaimsInfoViewModel(Int64 claimid)
        {
           

            claim = css.Claims.Find(claimid);


            var claimReferrer = (from c in css.ClaimReferrers
                                 where c.ClaimId == claimid && c.ReferrerTypeId == claim.ReferrerTypeId
                                 select c).FirstOrDefault();


            VehicleLocationName = css.VehicleLocations.Where(x => x.VehicleLocationID == claim.VehicleLocationID).Select(x=>x.VehicleLocationName).FirstOrDefault();
            AutoAssignment = css.AutoAssignments.Where(x => x.ClaimId == claimid).OrderByDescending(x => x.AutoAssignmentId).FirstOrDefault();
            AutoInvoiceList = css.usp_AutoInvoiceSummaryGetList(claimid).ToList();

            pa = css.AutoAssignments.Where(x => x.ClaimId == claimid).OrderByDescending(x => x.AutoAssignmentId).FirstOrDefault();
            if (pa.OAUserID != null)
            {
                SPID = (long)pa.OAUserID;
            }
            AutoAssignment pa1 = null; ;
            if (css.AutoAssignments.Where(x => x.ClaimId == claimid).Count() > 1)
            {
                pa1 = css.AutoAssignments.Where(x => x.ClaimId == claimid).OrderByDescending(x => x.AutoAssignmentId).Skip(1).FirstOrDefault();
                if (pa.OAUserID != null)
                {
                    SPID = (long)pa.OAUserID;
                }
            }

            ClaimCycleTimeDetail cctd = css.ClaimCycleTimeDetails.Where(x => x.ClaimId == claimid && x.SPId == pa.OAUserID).FirstOrDefault();
            if (cctd != null)
            {

                this.Rank = cctd.Rank.HasValue ? cctd.Rank.Value : 5;
                this.ApprovalComment = cctd.ApprovalNote;

                if (pa1 != null)
                {
                    if (pa.OAUserID != null && pa1.OAUserID != null)
                    {
                        if (pa1.OAUserID == pa.OAUserID || pa.DateApproved.HasValue)
                        {
                            //this.CheckApprovedDate = pa.DateApproved.HasValue ? 1 : 0;
                            this.CheckApprovedDate = 1;
                        }
                        else
                        {
                            this.CheckApprovedDate = 0;
                        }
                    }
                    else
                    {
                        if (pa.DateApproved.HasValue)
                        {
                            this.CheckApprovedDate = 1;
                        }
                        else
                        {
                            this.CheckApprovedDate = 0;
                        }

                    }


                }
                else
                {
                    if (pa.DateApproved.HasValue)
                    {
                        this.CheckApprovedDate = 1;
                    }
                    else
                    {
                        this.CheckApprovedDate = 0;
                    }

                }



                this.DaysToContact = cctd.AcceptedToContacted.HasValue ? cctd.AcceptedToContacted.Value : 0.0;
                this.DaysToInspect = cctd.ContactedToInspected.HasValue ? cctd.ContactedToInspected.Value : 0.0;
                this.DaysToUpload = cctd.ReceivedToEstimateReturned.HasValue ? cctd.ReceivedToEstimateReturned.Value : 0.0;
                this.RejectionMark = cctd.RejectionMark.HasValue ? cctd.RejectionMark.Value : true;
                this.RejectionCount = cctd.RejectionCount.HasValue ? cctd.RejectionCount.Value : 0;
            }
            else
            {

                this.Rank = 5;
                this.ApprovalComment = "";
                this.CheckApprovedDate = 0;

                this.DaysToContact = 0.0;
                this.DaysToInspect = 0.0;
                this.DaysToUpload = 0.0;
                this.RejectionMark = true;
                this.RejectionCount = 0;
            }
            //INPSW- 27
             var CurrentAssignmentStatus = css.AutoAssignments.Where(x => x.ClaimId == claimid).OrderByDescending(x => x.FileStatus).FirstOrDefault();// chk Current claim status
             var CountInvoice = css.AutoInvoices.Where(x => x.AutoAssignmentId == AutoAssignment.AutoAssignmentId).Count();//chk current claim is Invoiced
             var CountAssignments = css.AutoAssignments.Where(x => x.ClaimId == claimid).Count();//chk for reopen

            if (CurrentAssignmentStatus.FileStatus == 18 || CurrentAssignmentStatus.FileStatus == 19 || CurrentAssignmentStatus.FileStatus == 20 || CurrentAssignmentStatus.FileStatus == 10 || CountInvoice > 0 || CountAssignments > 1)
            {
                DisableFileStatus = "21";
            }
            PopulateLists();
        }

        public void getNugetVehicleDetails(Int64 claimid)
        {
            Int64 assignmentId = (from a in css.AutoAssignments
                                 where a.ClaimId == claimid
                                 select a.AutoAssignmentId).FirstOrDefault();
            if(assignmentId != null)
            {
                VehicleClaimDetail = (from c in css.VehicleClaimDetails
                                      where c.AssignmentID == assignmentId
                                      select c).FirstOrDefault();
                if (VehicleClaimDetail != null)
                    NugenVehicleLocationName = css.VehicleLocations.Where(x => x.VehicleLocationID == VehicleClaimDetail.VehicleLocation).Select(x => x.VehicleLocationName).FirstOrDefault();
                else {
                    VehicleClaimDetail = new VehicleClaimDetail();
                }
            }
        }

        public void PopulateLists()
        {
            FileStatusList.Add(new SelectListItem { Text = "--Select--", Value = "0" });
            foreach (FileStatusGetList_Result filestatus in css.FileStatusGetList())
            {              
               FileStatusList.Add(new SelectListItem { Text = filestatus.StatusDescription, Value = filestatus.StatusId.ToString() });                              
            }
        }
    }
}
