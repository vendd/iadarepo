﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using BLL.CustomValidations;

namespace BLL
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.ServiceProviderAddressValidation))]
    public partial class ServiceProviderAddress { }
}
namespace BLL.Models.Validations
{
    [Bind(Exclude = "SPAId")]
    class ServiceProviderAddressValidation
    {
        //[System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.ServiceProviderAddressValidation))] 
        //The above commented code is to be put before the BLL.ServiceProviderAddress class to make this class its buddy class
        public long SPAId { get; set; }
        public long UserId { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        [DisplayName("Date To")]
        [DateValidation(DateValidationAttribute.ValidationType.Compare, "List your residences: \"Date To\" should be greater than \"Date From\"", "DateFrom")]
        public Nullable<System.DateTime> DateTo { get; set; }
        [DisplayName("Date From")]
        public Nullable<System.DateTime> DateFrom { get; set; }

        public virtual User User { get; set; }

    }
}
