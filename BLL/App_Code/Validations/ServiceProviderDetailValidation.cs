﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BLL
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.ServiceProviderDetailValidation))]
    public partial class ServiceProviderDetail { }
}
namespace BLL.Models.Validations
{
    [Bind(Exclude = "UserId")]
    class ServiceProviderDetailValidation
    {
        //[System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.ServiceProviderDetailValidation))] 
        //The above commented code is to be put before the BLL.User class to make this class its buddy class

        public long UserId { get; set; }
        public Nullable<bool> IsKnowByOtherNames { get; set; }
        public string KnowByOtherNames { get; set; }
        public Nullable<bool> HasBeenPublicAdjuster { get; set; }
        public Nullable<System.DateTime> PublicAdjusterWhen { get; set; }
        public string PublicAdjusterWhere { get; set; }
        public Nullable<bool> RoofClimbingRequired { get; set; }
        public string CapabilityToClimb { get; set; }
        public string TypeOfClaimsPref { get; set; }
        public string TypeOfClaimsPrefOthers { get; set; }
        public Nullable<bool> ConsiderWorkingInCat { get; set; }
        public Nullable<System.DateTime> ExperienceWorkingInCatWhen { get; set; }
        public string CatCompany { get; set; }
        public string CatDuties { get; set; }
        public Nullable<bool> PositionRequiredFidelityBond { get; set; }
        public string BondClaimsDetails { get; set; }
        public Nullable<bool> BondRevoked { get; set; }
        public string BondRevokedDetails { get; set; }
        public Nullable<bool> VocationalLicenseEverRevoked { get; set; }
        public string VocationalLicenseEverRevokedDetails { get; set; }
        public Nullable<bool> AnyLicenseEverRevoked { get; set; }
        public string AnyLicenseEverRevokedDetails { get; set; }
        public string InsurerStakeholderList { get; set; }
        public string InsurerStakeholderStockPledged { get; set; }
        public Nullable<bool> HasDWIDUIConvections { get; set; }
        public Nullable<bool> HasFelonyConvictions { get; set; }
        public string FelonyConvictionsDetails { get; set; }
        public Nullable<bool> BecameInsolvent { get; set; }
        public Nullable<bool> WasCompanyEverSuspended { get; set; }
        public string CompanySuspendedDetails { get; set; }
        public string OtherLanguages { get; set; }
        public Nullable<bool> HasValidPassport { get; set; }
        public Nullable<System.DateTime> PassportExpiryDate { get; set; }
        public Nullable<bool> FormerEmployerBeContacted { get; set; }
        public Nullable<bool> HasWorkedInCS { get; set; }
        public Nullable<System.DateTime> WorkedInCSWhen { get; set; }
        public string WorkedInCSWhere { get; set; }
        public Nullable<bool> CSAskedYouToWork { get; set; }
        public string CapabilityToClimbSteep { get; set; }
        public Nullable<System.DateTime> LastDateCatastropheWorked { get; set; }
        public string CatastropheForWhom { get; set; }
        public Nullable<bool> HasExperienceWorkingInCat { get; set; }
        public Nullable<bool> HasRopeHarnessEquip { get; set; }
        public Nullable<bool> HasRoofAssistProgram { get; set; }
        public int ProfileProgress { get; set; }
        public Nullable<bool> IsAcceptingAssignments { get; set; }
        public Nullable<bool> IsAvailableForDeployment { get; set; }
        public Nullable<bool> IsDeployed { get; set; }
        [RegularExpression("(^[0-9]{5}$)|(^[0-9]{5}-[0-9]{4}$)", ErrorMessage = "Invalid Deployed Zip")]
        public string DeployedZip { get; set; }
        public string DeployedState { get; set; }
        public string DeployedCity { get; set; }

        public virtual User User { get; set; }
    }
}
