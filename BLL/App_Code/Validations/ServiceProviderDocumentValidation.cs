﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BLL
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.ServiceProviderDocumentValidation))]
    public partial class ServiceProviderDocument { }
}
namespace BLL.Models.Validations
{
    [Bind(Exclude = "SPDId")]
    class ServiceProviderDocumentValidation
    {
        //[System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.ServiceProviderDocumentValidation))] 
        public long SPDId { get; set; }
        public int DTId { get; set; }
        public long UserId { get; set; }
        public string Title { get; set; }
        public string Path { get; set; }
        public System.DateTime UploadedDate { get; set; }
        public string SertifiDocumentId { get; set; }

        public virtual DocumentType DocumentType { get; set; }
        public virtual User User { get; set; }
    }
}
