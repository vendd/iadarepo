﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BLL
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.UserValidation))]
    public partial class User { }
}
namespace BLL.Models.Validations
{
    [Bind(Exclude="UserId")]
    public class UserValidation
    {
        //[System.ComponentModel.DataAnnotations.MetadataType(typeof(BLL.Models.Validations.UserValidation))] 
        //The above commented code is to be put before the BLL.User class to make this class its buddy class
        public long UserId { get; set; }

        [Required]
        [DisplayName("First Name")]
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        [DisplayName("Last Name")]
        [Required]
        public string LastName { get; set; }

        [Required]
        [DisplayName("User Name")]
        public string UserName { get; set; }

        [Required]
        //[StringLength(15, MinimumLength = 6, ErrorMessage = "Password should be atleast 6 characters.")]
        //[RegularExpression("([a-zA-Z0-9!@#$%^&*]{1,})", ErrorMessage = "Password should contain atleast one alphabet and a number")]
        public string Password { get; set; }

        [Required]
        [DisplayName("Email")]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Invalid Email Address")]
        //[RegularExpression("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        //[Required]
        //[DisplayName("Street Number")]
        //public string AddressLine1 { get; set; }
        //[Required]
        //[DisplayName("Street Name")]
        //public string StreetAddress { get; set; }
        //[RegularExpression("(^[0-9]{5}$)|(^[0-9]{5}-[0-9]{4}$)", ErrorMessage = "Invalid Zip Code")]
        public string Zip { get; set; }

        [DisplayName("Social Security Number")]
        public string SSN { get; set; }


        public Nullable<int> UserTypeId { get; set; }
        public Nullable<int> ParentCompanyId { get; set; }
        public Nullable<int> HeadCompanyId { get; set; }
        [Required]
        [DisplayName("Doing Business As")]
        private string DBA_LLC_Firm_Corp { get; set; }
        private string TaxID { get; set; }
       
        
        public string City { get; set; }
        public string State { get; set; }
       
        public string Country { get; set; }
       
        public string Twitter { get; set; }
        public string Facebook { get; set; }
      
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string MobilePhone { get; set; }
        public string Pager { get; set; }
        public string OtherPhone { get; set; }
        public Nullable<bool> Active { get; set; }
        public Nullable<bool> AcceptingClaims { get; set; }
        public Nullable<bool> Deployed { get; set; }
        public Nullable<bool> AvailibleForDeployment { get; set; }
        public string AlternativeZip { get; set; }
        public Nullable<System.DateTime> ReturnDate { get; set; }
        public Nullable<bool> Vacation { get; set; }
        public string XactnetAddress { get; set; }
        public Nullable<System.DateTime> XactnetExpirySubscription { get; set; }
        public string SymbilityID { get; set; }
        private Nullable<int> YearsExperience { get; set; }
        private string CL { get; set; }
        private string PL { get; set; }
        private string GL { get; set; }
        private string Equip { get; set; }
        private string MobileHome { get; set; }
        private string Enviromental { get; set; }
        private string SoftwareUsed { get; set; }
        private Nullable<System.DateTime> ContractExpires { get; set; }
        private Nullable<int> Rank { get; set; }
        private Nullable<double> DefaultService { get; set; }
        private Nullable<double> DefaultHurricaneService { get; set; }
        private Nullable<bool> BackgroundCheck { get; set; }
        private Nullable<bool> DirectDeposit { get; set; }
        private string LiceseNumber { get; set; }
        public string NotesSection { get; set; }
        public string DocumentsSection { get; set; }
        private Nullable<bool> HAAGCertified { get; set; }
        private Nullable<System.DateTime> HAAGExpirationDate { get; set; }
        private Nullable<bool> EQCertified { get; set; }
        private Nullable<bool> CertifiedQualifiedForClients { get; set; }
        private Nullable<bool> IsFloodCertified { get; set; }
        private string FloodCertificationNumber { get; set; }
        private Nullable<System.DateTime> FloodCertificationExpiry { get; set; }
        private string DriversLicenceNumber { get; set; }
        private string DriversLicenceState { get; set; }
        private Nullable<System.DateTime> DriversLicenceExpiry { get; set; }
        private string ListProfesionalOrganization { get; set; }
        private string PrimaryLanguage { get; set; }
        private Nullable<bool> EOHasInsurance { get; set; }
        private string EOPoliyNumber { get; set; }
        private string EOCarrier { get; set; }
        private Nullable<System.DateTime> EOInception { get; set; }
        private Nullable<System.DateTime> EOExpiry { get; set; }
        private Nullable<double> EOLimit { get; set; }
        private Nullable<double> EODeductible { get; set; }
        private string AutoPolicyNumber { get; set; }
        private string AutoCarrier { get; set; }
        private Nullable<System.DateTime> AutoExpiry { get; set; }
        private Nullable<double> AutoLimit { get; set; }
        private string EmergencyContactPersonDetails { get; set; }
        public string LevelOfUserRights { get; set; }
        private Nullable<System.DateTime> DOB { get; set; }
        private string PlaceOfBirth { get; set; }
        public string GooglePlus { get; set; }
        private string EQCertificationNumber { get; set; }
        private Nullable<System.DateTime> EQExpirationDate { get; set; }
        private string HAAGCertificationNumber { get; set; }

        public virtual ServiceProviderDetail ServiceProviderDetail { get; set; }
        public virtual ICollection<ServiceProviderExperience> ServiceProviderExperiences { get; set; }
        public virtual UserType UserType { get; set; }
        public virtual ICollection<ServiceProviderAddress> ServiceProviderAddresses { get; set; }
        public virtual ICollection<ServiceProviderEmployer> ServiceProviderEmployers { get; set; }
        public virtual ICollection<ServiceProviderDesignation> ServiceProviderDesignations { get; set; }
        public virtual ICollection<ServiceProviderReference> ServiceProviderReferences { get; set; }
        public virtual ICollection<ServiceProviderEducation> ServiceProviderEducations { get; set; }
        public virtual ICollection<ServiceProviderSoftwareExperience> ServiceProviderSoftwareExperiences { get; set; }
        public virtual ICollection<ServiceProviderLicens> ServiceProviderLicenses { get; set; }
    }
}
