//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    
    public partial class GetDiaryItems_Result
    {
        public long DiaryId { get; set; }
        public Nullable<long> AssignmentId { get; set; }
        public Nullable<byte> DiaryCategoryId { get; set; }
        public Nullable<long> AssignedToUserId { get; set; }
        public Nullable<long> CreatedByUserId { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> DueDate { get; set; }
        public Nullable<System.DateTime> FollowupDate { get; set; }
        public Nullable<System.DateTime> CompletedDate { get; set; }
        public string Title { get; set; }
        public string DiaryDesc { get; set; }
        public Nullable<byte> StatusId { get; set; }
        public Nullable<System.DateTime> ReminderDate { get; set; }
        public Nullable<bool> IsSystemGenerated { get; set; }
        public string DiaryCategoryDesc { get; set; }
        public string Username { get; set; }
        public bool IsExternalContact { get; set; }
        public Nullable<bool> IsBillable { get; set; }
        public Nullable<double> NoOfHours { get; set; }
    }
}
