//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    using System.Collections.Generic;
    
    public partial class WeGoLookLogDetail
    {
        public int Id { get; set; }
        public string ClaimNumber { get; set; }
        public string CustomMessage { get; set; }
        public string ExceptionMessage { get; set; }
        public string ExceptionStackTrace { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<bool> HasResolved { get; set; }
    }
}
