//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    
    public partial class DocumentsList_Result
    {
        public int DisplayRank { get; set; }
        public long AutoDocumentId { get; set; }
        public long AutoAssignmentId { get; set; }
        public System.DateTime AutoDocumentUploadedDate { get; set; }
        public string Title { get; set; }
        public string OriginalFileName { get; set; }
        public string AutoDocumentPath { get; set; }
        public long AutoDocumentUploadedByUserId { get; set; }
        public string DocumentUploadedByUser { get; set; }
        public Nullable<short> DocumentTypeId { get; set; }
        public string DocumentTypeDesc { get; set; }
        public Nullable<bool> Status { get; set; }
        public string StatusDesc { get; set; }
    }
}
