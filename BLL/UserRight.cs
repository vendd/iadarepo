//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BLL
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserRight
    {
        public UserRight()
        {
            this.UserAssignedRights = new HashSet<UserAssignedRight>();
        }
    
        public byte UserRightId { get; set; }
        public string UserRightDesc { get; set; }
    
        public virtual ICollection<UserAssignedRight> UserAssignedRights { get; set; }
    }
}
